'use strict';
/**
 * Usage:
 *
 * 1. Install Jeditable: http://www.appelsiini.net/projects/jeditable
 * 2. Add the code below to your javascript.
 * 3. Call it like this:
 *
 * $('p').editable('/edit', {
 *   type:   'checkbox',
 *   cancel: 'Cancel',
 *   submit: 'OK',
 *   checkbox: { trueValue: 'Yes', falseValue: 'No' }
 * });
 *
 * Upon clicking on the <p>, it's content will be replaced by a checkbox.
 * If the text within the paragraph is 'Yes', the checkbox will be checked
 * by default, otherwise it will be unchecked.
 *
 * trueValue is submitted when the checkbox is checked and falseValue otherwise.
 *
 * Have fun!
 *
 * Peter Bücker (spam.naag@gmx.net)
 * http://www.pastie.org/893364
 */ 

jQuery.editable.addInputType('checkbox', {
  element: function(settings, original) {
    jQuery(this).append('<input type="checkbox"/>');
    var hidden = jQuery('<input type="hidden"/>');
    jQuery(this).append(hidden);
    return(hidden);
  },

  submit: function(settings, original) {
    settings = jQuery.extend({ checkbox: {
      trueValue: '1',
      falseValue: '0'
    }}, settings);

    if (jQuery(':checkbox', this).is(':checked')) {
      jQuery(':hidden', this).val(settings.checkbox.trueValue);
    } else {
      jQuery(':hidden', this).val(settings.checkbox.falseValue);
    }
  },

  content: function(data, settings, original) {
    settings = jQuery.extend({ checkbox: {
      trueValue: '1',
      falseValue: '0'
    }}, settings);

    if (data == settings.checkbox.trueValue) {
      jQuery(':checkbox', this).attr('checked', 'checked');
    } else if (data === settings.checkbox.falseValue) {
      jQuery(':checkbox', this).removeAttr('checked');
    } else if (jQuery(data).is('input[type="checkbox"]')) {
      if (jQuery(data).is(':checked')) {
        jQuery(':checkbox', this).attr('checked', 'checked');
      } else {
        jQuery(':checkbox', this).removeAttr('checked');
      }
    } else {
      jQuery(':checkbox', this).removeAttr('checked');
    }
  }
});