'use strict';
 
(function ($) { 
  $.fn.lazyLoadingButton = function(options) {
    if (options.ajax && options.callback && options.loadingText && options.data) { 
      var oldText = ($(this).html());  
      var self = this;
      $(self).on('click',function(e) { 
          if (options.validation && !options.validation()) {
            if (options.onValidateError) {
              options.onValidateError();
            }
            return;
          }
          $(self).html(options.loadingText); 
          $.ajax({
            url: options.ajax,
            data: JSON.stringify(options.data(self)),
            type: 'POST', 
            dataType: 'json',
            contentType : 'application/json',
            error : function(jqXHR, textStatus, errorThrown) {
                $(self).html(oldText);
                options.callback({
                  jqXHR : jqXHR,
                  message: textStatus,
                  error: errorThrown
                }, textStatus);
            },
            success : function(data, textStatus, jqXHR) {
                $(self).html(oldText);
                options.callback(null, textStatus);
            }
        });
      });
    } else { 
      console.error('Invalid lazy button parameters.');
    }
  };
})(jQuery);