'use strict';

function TensAPI(jQuery, baseUrl) {
  var self = this;
  self.jq = jQuery;
  self.base_url = baseUrl;
  self.getCustomer = function(customerId, successCallback, failureCallback) { 
    jQuery.ajax({
        url: self.base_url + 'customers/api/getCustomer',
        data: JSON.stringify({customerId : customerId}),
        type: 'POST', 
        dataType: 'json',
        contentType : 'application/json',
        error : function(jqXHR, textStatus, errorThrown) {
            failureCallback(textStatus);
        },
        success : function(data, textStatus, jqXHR) {
            successCallback(data);
        }
    });
  }
 
  self.confirmRefund = function(params, successCallback, failureCallback) {
    if (params.orderId && params.refundDate && params.total && params.reference && params.payMethodId && 
      params.terminalId) {
      jQuery.ajax({
        url: self.base_url + 'refunds/api/confirmRefund',
        data: JSON.stringify(params),
        type: 'POST', 
        dataType: 'json',
        contentType : 'application/json',
        error : function(jqXHR, textStatus, errorThrown) {
          console.log('Failure');
            failureCallback(textStatus);
        },
        success : function(data, textStatus, jqXHR) {
          console.log('Success');
            successCallback(data);
        }
      });
    } else {
      console.error('Invalid parameters');
      failureCallback(new Error('Invalid parameters'));
    }
  }
}