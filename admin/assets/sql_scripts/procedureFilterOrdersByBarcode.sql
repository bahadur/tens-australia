
DELIMITER //
CREATE PROCEDURE filterOrdersByBarcode(IN barcodeVar VARCHAR(18))
BEGIN
SELECT orderdetail.OrderDetailID, hospital.HospitalName, product.ProductName, orderdetail.Quantity, stockmovedetail.Barcode, orderdetail.UnitPrice, orderdetail.GST, stockmove.MoveDate, CONCAT(customer.FirstName, " ", customer.LastName, " ", customer.BusinessName) as "Customer_Name" 
FROM (productcategory INNER JOIN product ON productcategory.ProductCategoryID = product.ProductCategoryID) 
INNER JOIN (((hospital INNER JOIN (customer INNER JOIN `order` ON customer.CustomerID = order.CustomerID) ON hospital.HospitalID = order.HospitalID) 
             INNER JOIN (orderdetail INNER JOIN stockmovedetail ON orderdetail.OrderDetailID = stockmovedetail.OrderDetailID) ON order.OrderID = orderdetail.OrderID) 
            INNER JOIN stockmove ON (stockmove.StockMoveID = stockmovedetail.StockMoveID) AND (order.OrderID = stockmove.ToOrderID)) ON (product.ProductID = stockmovedetail.ProductID) AND (product.ProductID = orderdetail.ProductID) WHERE Barcode = barcodeVar;
END //
DELIMITER ;			