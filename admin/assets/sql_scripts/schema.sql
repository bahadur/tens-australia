-- MySQL dump 10.13  Distrib 5.6.23, for Win64 (x86_64)
--
-- Host: localhost    Database: tens_australia
-- ------------------------------------------------------
-- Server version	5.6.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accountcode`
--

DROP TABLE IF EXISTS `accountcode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountcode` (
  `AccountCode` varchar(255) NOT NULL,
  `Description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`AccountCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `areamanager`
--

DROP TABLE IF EXISTS `areamanager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `areamanager` (
  `AreaManagerID` int(10) NOT NULL,
  `BusinessName` varchar(255) DEFAULT NULL,
  `ABN` varchar(255) DEFAULT NULL,
  `Firstname` varchar(255) DEFAULT NULL,
  `Lastname` varchar(255) DEFAULT NULL,
  `Position` varchar(255) DEFAULT NULL,
  `Address` varchar(255) DEFAULT NULL,
  `Suburb` varchar(255) DEFAULT NULL,
  `State` varchar(255) DEFAULT NULL,
  `Postcode` varchar(255) DEFAULT NULL,
  `Country` varchar(255) DEFAULT NULL,
  `Phone` varchar(255) DEFAULT NULL,
  `Fax` varchar(255) DEFAULT NULL,
  `MobilePhone` varchar(255) DEFAULT NULL,
  `Email` longtext,
  `ReferralFee` decimal(19,4) DEFAULT NULL,
  `StartDate` datetime DEFAULT NULL,
  `Active` tinyint(1) NOT NULL,
  PRIMARY KEY (`AreaManagerID`),
  UNIQUE KEY `EducatorID` (`AreaManagerID`),
  KEY `postcode1` (`Postcode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `areamanagerpostcode`
--

DROP TABLE IF EXISTS `areamanagerpostcode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `areamanagerpostcode` (
  `AreaManagerPostcodeID` int(10) NOT NULL,
  `AreaManagerID` int(10) DEFAULT NULL,
  `Postcode` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`AreaManagerPostcodeID`),
  KEY `Postcode` (`Postcode`),
  KEY `AreaManagerID` (`AreaManagerPostcodeID`),
  KEY `AreaManagerAreaManagerPostcode` (`AreaManagerID`),
  KEY `AreaManagerID1` (`AreaManagerID`),
  CONSTRAINT `FK_areamanagerpostcode_AreaManager` FOREIGN KEY (`AreaManagerID`) REFERENCES `areamanager` (`AreaManagerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config` (
  `ConfigID` tinyint(3) unsigned NOT NULL,
  `DefaultPriceGroupID` int(10) DEFAULT NULL,
  `GST_Percent` double DEFAULT NULL,
  `DefaultPickLocationID` int(10) DEFAULT NULL,
  `DefaultHireReturnLocationID` int(10) DEFAULT NULL,
  `SearchRightCharsInBarcode` smallint(5) DEFAULT NULL,
  `ReturnExpectedDuePlusDays` smallint(5) DEFAULT NULL,
  `SendingSMS` varchar(38) DEFAULT NULL,
  `SendSMS_Time` datetime DEFAULT NULL,
  `SMS_SentDate` datetime DEFAULT NULL,
  `RequiredWarningDueMinusDays` smallint(5) DEFAULT NULL,
  `InvoiceEmailSubject` longtext,
  `InvoiceEmailBody` longtext,
  `RefundEmailSubject` longtext,
  `RefundEmailBody` longtext,
  `AdminPassword` varchar(255) DEFAULT NULL,
  `ClassReminderMinusDays` tinyint(3) unsigned DEFAULT NULL,
  `ClassReminderMessageText` varchar(255) DEFAULT NULL,
  `CommonEmailFooter` longtext,
  PRIMARY KEY (`ConfigID`),
  KEY `DefaultPriceGroupID` (`DefaultPriceGroupID`),
  KEY `DefaultHireReturnLocationID` (`DefaultHireReturnLocationID`),
  KEY `DefaultPickLocationID` (`DefaultPickLocationID`),
  KEY `SearchRightCharsInBarcode` (`SearchRightCharsInBarcode`),
  KEY `ConfigID` (`ConfigID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `CustomerID` int(10) NOT NULL,
  `CustomerTypeID` int(10) DEFAULT NULL,
  `BusinessName` varchar(255) DEFAULT NULL,
  `ABN` varchar(255) DEFAULT NULL,
  `Firstname` varchar(255) DEFAULT NULL,
  `LastName` varchar(255) DEFAULT NULL,
  `BillingAddress` varchar(255) DEFAULT NULL,
  `BillingSuburb` varchar(255) DEFAULT NULL,
  `BillingState` varchar(255) DEFAULT NULL,
  `BillingPostcode` varchar(4) DEFAULT NULL,
  `BillingCountry` varchar(255) DEFAULT NULL,
  `ShippingSameAsBilling` tinyint(1) NOT NULL,
  `ShippingAddress` varchar(255) DEFAULT NULL,
  `ShippingSuburb` varchar(255) DEFAULT NULL,
  `ShippingState` varchar(255) DEFAULT NULL,
  `ShippingPostcode` varchar(4) DEFAULT NULL,
  `ShippingCountry` varchar(255) DEFAULT NULL,
  `HomePhone` varchar(255) DEFAULT NULL,
  `BusinessPhone` varchar(255) DEFAULT NULL,
  `Fax` varchar(255) DEFAULT NULL,
  `MobilePhone` varchar(255) DEFAULT NULL,
  `Email` longtext,
  `Website` longtext,
  `Comments` longtext,
  `Active` tinyint(1) NOT NULL,
  `InsuranceProviderID` int(10) DEFAULT NULL,
  `InsuranceMembershipNo` varchar(255) DEFAULT NULL,
  `PriceGroupID` int(10) DEFAULT NULL,
  `PartnersName` varchar(255) DEFAULT NULL,
  `PartnersMobilePhone` varchar(255) DEFAULT NULL,
  `PartnersBusinessPhone` varchar(255) DEFAULT NULL,
  `CustomerProblemFlagID` int(10) DEFAULT NULL,
  `Identifiers` varchar(255) DEFAULT NULL,
  `ImportID` int(10) DEFAULT NULL,
  `PriceList` tinyint(1) NOT NULL,
  PRIMARY KEY (`CustomerID`),
  KEY `BillingPostcode` (`ShippingPostcode`),
  KEY `CustomerProblemFlagID` (`CustomerProblemFlagID`),
  KEY `Identifiers` (`Identifiers`),
  KEY `PriceGroupCustomer` (`PriceGroupID`),
  KEY `postcode1` (`BillingPostcode`),
  KEY `ImportID` (`ImportID`),
  KEY `insProvID` (`InsuranceProviderID`),
  KEY `PriceGroupID` (`PriceGroupID`),
  KEY `InsuranceProviderCustomer` (`InsuranceProviderID`),
  CONSTRAINT `fk_customer_PriceGroup` FOREIGN KEY (`PriceGroupID`) REFERENCES `pricegroup` (`PriceGroupID`),
  CONSTRAINT `fk_customer_insuranceprovider` FOREIGN KEY (`InsuranceProviderID`) REFERENCES `insuranceprovider` (`InsuranceProviderID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customernote`
--

DROP TABLE IF EXISTS `customernote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customernote` (
  `CustomerNoteID` int(10) NOT NULL,
  `CustomerID` int(10) DEFAULT NULL,
  `OrderID` int(10) DEFAULT NULL,
  `DateTime` datetime DEFAULT NULL,
  `StaffID` int(10) DEFAULT NULL,
  `CustomerProblemFlagID` int(10) DEFAULT NULL,
  `Note` longtext,
  PRIMARY KEY (`CustomerNoteID`),
  KEY `OrderID` (`OrderID`),
  KEY `OrderCustomerNote` (`OrderID`),
  KEY `CustomerProblemFlagID` (`CustomerProblemFlagID`),
  KEY `CustomerID` (`CustomerID`),
  KEY `CustomerCustomerNote` (`CustomerID`),
  KEY `StaffID` (`StaffID`),
  KEY `{603A9FF6-E28D-46D4-B236-7FFD07E3C7F7}` (`CustomerProblemFlagID`),
  KEY `CustomerNotesID` (`CustomerNoteID`),
  CONSTRAINT `FK_customernote_Customer` FOREIGN KEY (`CustomerID`) REFERENCES `customer` (`CustomerID`),
  CONSTRAINT `FK_customernote_CustomerProblemFlag` FOREIGN KEY (`CustomerProblemFlagID`) REFERENCES `customerproblemflag` (`CustomerProblemFlagID`),
  CONSTRAINT `FK_customernote_order` FOREIGN KEY (`OrderID`) REFERENCES `order` (`OrderID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customerproblemflag`
--

DROP TABLE IF EXISTS `customerproblemflag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customerproblemflag` (
  `CustomerProblemFlagID` int(10) NOT NULL,
  `Flag` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`CustomerProblemFlagID`),
  KEY `CustomerProblemFlagID` (`CustomerProblemFlagID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `ID` int(10) NOT NULL,
  `Co/Last Name` varchar(255) DEFAULT NULL,
  `First Name` varchar(255) DEFAULT NULL,
  `Addr 1 - Line 1` varchar(255) DEFAULT NULL,
  `Addr Line 4` varchar(255) DEFAULT NULL,
  `City` varchar(255) DEFAULT NULL,
  `State` varchar(255) DEFAULT NULL,
  `Postcode` varchar(255) DEFAULT NULL,
  `Country` varchar(255) DEFAULT NULL,
  `Mobile` varchar(255) DEFAULT NULL,
  `Telephone` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Notes` varchar(255) DEFAULT NULL,
  `Identifiers` varchar(255) DEFAULT NULL,
  UNIQUE KEY `ID` (`ID`),
  KEY `- Postcode` (`Postcode`),
  KEY `Identifiers` (`Identifiers`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customertype`
--

DROP TABLE IF EXISTS `customertype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customertype` (
  `CustomerTypeID` int(10) NOT NULL,
  `CustomerType` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`CustomerTypeID`),
  KEY `CustomerTypeID` (`CustomerTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `educator`
--

DROP TABLE IF EXISTS `educator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `educator` (
  `EducatorID` int(10) NOT NULL,
  `BusinessName` varchar(255) DEFAULT NULL,
  `ABN` varchar(255) DEFAULT NULL,
  `Firstname` varchar(255) DEFAULT NULL,
  `Lastname` varchar(255) DEFAULT NULL,
  `Position` varchar(255) DEFAULT NULL,
  `Address` varchar(255) DEFAULT NULL,
  `Suburb` varchar(255) DEFAULT NULL,
  `State` varchar(255) DEFAULT NULL,
  `Postcode` varchar(255) DEFAULT NULL,
  `Country` varchar(255) DEFAULT NULL,
  `Phone` varchar(255) DEFAULT NULL,
  `Fax` varchar(255) DEFAULT NULL,
  `MobilePhone` varchar(255) DEFAULT NULL,
  `Email` longtext,
  `ReferralFee` decimal(19,4) DEFAULT NULL,
  `ClassMinNo` smallint(5) DEFAULT NULL,
  `ClassMinFee` decimal(19,4) DEFAULT NULL,
  `Active` tinyint(1) NOT NULL,
  PRIMARY KEY (`EducatorID`),
  UNIQUE KEY `eduID` (`EducatorID`),
  KEY `postcode1` (`Postcode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `educatorclass`
--

DROP TABLE IF EXISTS `educatorclass`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `educatorclass` (
  `EduClassID` int(10) NOT NULL,
  `EduLocationID` int(10) DEFAULT NULL,
  `ClassDate` datetime DEFAULT NULL,
  `StartTime` datetime DEFAULT NULL,
  `Duration` int(10) DEFAULT NULL,
  `EducatorID` int(10) DEFAULT NULL,
  `HospitalID` int(10) DEFAULT NULL,
  `CourseID` int(10) DEFAULT NULL,
  PRIMARY KEY (`EduClassID`),
  KEY `HospitalID` (`HospitalID`),
  KEY `HospitalEducatorClass` (`HospitalID`),
  KEY `CourseID` (`CourseID`),
  KEY `EducatorClassCourseEducatorClass` (`CourseID`),
  KEY `EducatorEducatorClass` (`EducatorID`),
  KEY `LocationID` (`EduLocationID`),
  KEY `EducatorLocationEducatorClass` (`EduLocationID`),
  CONSTRAINT `FK_educatorclass_Hospital` FOREIGN KEY (`HospitalID`) REFERENCES `hospital` (`HospitalID`),
  CONSTRAINT `FK_educatorclass_educator` FOREIGN KEY (`EducatorID`) REFERENCES `educator` (`EducatorID`),
  CONSTRAINT `FK_educatorclass_educatorclasscourse` FOREIGN KEY (`CourseID`) REFERENCES `educatorclasscourse` (`CourseID`),
  CONSTRAINT `FK_educatorclass_educatorlocation` FOREIGN KEY (`EduLocationID`) REFERENCES `educatorlocation` (`EduLocationID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `educatorclasscourse`
--

DROP TABLE IF EXISTS `educatorclasscourse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `educatorclasscourse` (
  `CourseID` int(10) NOT NULL,
  `CourseName` varchar(255) DEFAULT NULL,
  `CourseNameHTML` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`CourseID`),
  KEY `CourseID` (`CourseID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `educatorclasscustomer`
--

DROP TABLE IF EXISTS `educatorclasscustomer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `educatorclasscustomer` (
  `EducatorClassCustomerID` int(10) NOT NULL,
  `EduClassID` int(10) DEFAULT NULL,
  `CustomerID` int(10) DEFAULT NULL,
  `NumberAttending` tinyint(3) unsigned DEFAULT NULL,
  `BookingDateTime` datetime DEFAULT NULL,
  `Cancelled` tinyint(1) NOT NULL,
  `CancelledDateTime` datetime DEFAULT NULL,
  PRIMARY KEY (`EducatorClassCustomerID`),
  KEY `EducatorClassEducatorClassCustomer` (`EduClassID`),
  KEY `NumberAttending` (`NumberAttending`),
  KEY `CustomerID` (`CustomerID`),
  KEY `CustomerEducatorClassCustomer` (`CustomerID`),
  KEY `EduClassID` (`EduClassID`),
  CONSTRAINT `FK_educatorclasscustomer_Customer` FOREIGN KEY (`CustomerID`) REFERENCES `customer` (`CustomerID`),
  CONSTRAINT `FK_educatorclasscustomer_educatorclass` FOREIGN KEY (`EduClassID`) REFERENCES `educatorclass` (`EduClassID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `educatorclasscustomerremindersent`
--

DROP TABLE IF EXISTS `educatorclasscustomerremindersent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `educatorclasscustomerremindersent` (
  `EducatorClassCustomerID` int(10) NOT NULL,
  `SentDateTime` datetime DEFAULT NULL,
  PRIMARY KEY (`EducatorClassCustomerID`),
  UNIQUE KEY `{7FC33C63-C80D-4FD8-8A4D-CF4880D8DD17}` (`EducatorClassCustomerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `educatorlocation`
--

DROP TABLE IF EXISTS `educatorlocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `educatorlocation` (
  `EduLocationID` int(10) NOT NULL,
  `LocName` varchar(255) DEFAULT NULL,
  `LocAddress` varchar(255) DEFAULT NULL,
  `LocSuburb` varchar(255) DEFAULT NULL,
  `LocState` varchar(255) DEFAULT NULL,
  `LocPostcode` varchar(255) DEFAULT NULL,
  `LocContact` varchar(255) DEFAULT NULL,
  `LocTelephone` varchar(255) DEFAULT NULL,
  `Active` tinyint(1) NOT NULL,
  `Instructions` varchar(255) DEFAULT NULL,
  `EducatorPatientsID` int(10) DEFAULT NULL,
  `Fee` int(10) DEFAULT NULL,
  `ClassSize` smallint(5) DEFAULT NULL,
  `LocMapURL` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`EduLocationID`),
  KEY `LocPostcode` (`LocPostcode`),
  KEY `EducatorPatientsID` (`EducatorPatientsID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `educatorpatients`
--

DROP TABLE IF EXISTS `educatorpatients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `educatorpatients` (
  `EducatorPatientsID` int(10) NOT NULL,
  `PatientsInvited` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`EducatorPatientsID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `emailtemplate`
--

DROP TABLE IF EXISTS `emailtemplate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emailtemplate` (
  `EmailTemplateID` int(10) NOT NULL,
  `TemplateName` varchar(255) DEFAULT NULL,
  `Subject` varchar(255) DEFAULT NULL,
  `Body` longtext,
  PRIMARY KEY (`EmailTemplateID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gateapplicationrequest`
--

DROP TABLE IF EXISTS `gateapplicationrequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gateapplicationrequest` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `___GateInput___x` bigint(20) NOT NULL DEFAULT '0',
  `DocumentId` bigint(20) NOT NULL,
  `TopicId` bigint(20) NOT NULL,
  `Priority` float NOT NULL DEFAULT '0',
  `GateApplicationUrl` longtext,
  `TestHtml` longtext,
  `DateCreated` datetime DEFAULT NULL,
  `___GateOutput___x` datetime DEFAULT NULL,
  `ApplyGateProcessIdLock` bigint(20) DEFAULT NULL,
  `DateExecuted` datetime DEFAULT NULL,
  `ExecutionTimeInSeconds` int(10) DEFAULT NULL,
  `GateOutputXml` longtext CHARACTER SET utf8,
  `___PostGateProcessing___x` char(1) DEFAULT NULL,
  `PostGateProcessIdLock` bigint(20) DEFAULT NULL,
  `DatePostGateProcessed` datetime DEFAULT NULL,
  `GateResultsSerialized` longtext,
  `HadError` tinyint(1) NOT NULL,
  `ErrorMessage` longtext,
  `RequestId` bigint(20) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `DocumentId` (`DocumentId`),
  KEY `TopicId` (`TopicId`),
  KEY `ProcessIdLock` (`ApplyGateProcessIdLock`,`DateExecuted`,`Priority`),
  KEY `HadError` (`HadError`),
  KEY `ExecutionTimeInSeconds` (`ExecutionTimeInSeconds`),
  KEY `RequestId` (`RequestId`),
  KEY `GetIdLock` (`DateExecuted`,`DatePostGateProcessed`,`PostGateProcessIdLock`,`Priority`),
  KEY `GateOutputXml` (`GateOutputXml`(2)),
  KEY `DateCreated` (`DateCreated`),
  KEY `DatePostGateProcessed` (`DatePostGateProcessed`),
  KEY `DateExecuted` (`DateExecuted`),
  KEY `PostGateProcessIdLock` (`PostGateProcessIdLock`),
  KEY `PostGateQueue` (`PostGateProcessIdLock`,`DatePostGateProcessed`,`DateExecuted`,`HadError`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPRESSED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hasbarcode`
--

DROP TABLE IF EXISTS `hasbarcode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hasbarcode` (
  `HasBarcodeID` tinyint(3) unsigned NOT NULL,
  `HasBarcode` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`HasBarcodeID`),
  KEY `HasBarcode` (`HasBarcode`),
  KEY `HasBarcodeID` (`HasBarcodeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hospital`
--

DROP TABLE IF EXISTS `hospital`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hospital` (
  `HospitalID` int(10) NOT NULL,
  `HospitalGroupID` int(10) DEFAULT NULL,
  `HospitalName` varchar(255) DEFAULT NULL,
  `Address` varchar(255) DEFAULT NULL,
  `Suburb` varchar(255) DEFAULT NULL,
  `State` varchar(255) DEFAULT NULL,
  `Country` varchar(255) DEFAULT NULL,
  `Postcode` varchar(4) DEFAULT NULL,
  `Phone` varchar(255) DEFAULT NULL,
  `Fax` varchar(255) DEFAULT NULL,
  `Email` longtext,
  `Website` longtext,
  `Maternity` tinyint(1) NOT NULL,
  `Rehab` tinyint(1) NOT NULL,
  `OnHold` tinyint(1) NOT NULL,
  PRIMARY KEY (`HospitalID`),
  KEY `HospitalGroupHospital` (`HospitalGroupID`),
  KEY `Postcode` (`Postcode`),
  KEY `HospitalGroupID` (`HospitalGroupID`),
  KEY `HospitalID` (`HospitalID`),
  CONSTRAINT `FK_hospital_hospitalgroup` FOREIGN KEY (`HospitalGroupID`) REFERENCES `hospitalgroup` (`HospitalGroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hospitalcontact`
--

DROP TABLE IF EXISTS `hospitalcontact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hospitalcontact` (
  `HospitalContactID` int(10) NOT NULL,
  `HospitalID` int(10) DEFAULT NULL,
  `FirstName` varchar(255) DEFAULT NULL,
  `LastName` varchar(255) DEFAULT NULL,
  `Position` varchar(255) DEFAULT NULL,
  `Department` varchar(255) DEFAULT NULL,
  `Phone` varchar(255) DEFAULT NULL,
  `Fax` varchar(255) DEFAULT NULL,
  `MobilePhone` varchar(255) DEFAULT NULL,
  `Email` longtext,
  `Primary` tinyint(1) NOT NULL,
  PRIMARY KEY (`HospitalContactID`),
  UNIQUE KEY `HospitalContactID` (`HospitalContactID`),
  KEY `HospitalID` (`HospitalID`),
  KEY `HospitalHospitalContact` (`HospitalID`),
  CONSTRAINT `FK_hospitalcontact_hospital` FOREIGN KEY (`HospitalID`) REFERENCES `hospital` (`HospitalID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hospitalgroup`
--

DROP TABLE IF EXISTS `hospitalgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hospitalgroup` (
  `HospitalGroupID` int(10) NOT NULL,
  `HospitalGroup` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`HospitalGroupID`),
  KEY `HospitalGroupID` (`HospitalGroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `insuranceprovider`
--

DROP TABLE IF EXISTS `insuranceprovider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `insuranceprovider` (
  `InsuranceProviderID` int(10) NOT NULL,
  `CompanyName` varchar(255) DEFAULT NULL,
  `ProviderNo` varchar(255) DEFAULT NULL,
  `Address` varchar(255) DEFAULT NULL,
  `Suburb` varchar(255) DEFAULT NULL,
  `Postcode` varchar(4) DEFAULT NULL,
  `State` varchar(255) DEFAULT NULL,
  `Country` varchar(255) DEFAULT NULL,
  `Website` longtext,
  PRIMARY KEY (`InsuranceProviderID`),
  KEY `Postcode1` (`Postcode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `kit`
--

DROP TABLE IF EXISTS `kit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kit` (
  `KitID` int(10) NOT NULL,
  `KitName` varchar(255) DEFAULT NULL,
  `Notes` longtext,
  PRIMARY KEY (`KitID`),
  KEY `KitID` (`KitID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `kitproduct`
--

DROP TABLE IF EXISTS `kitproduct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kitproduct` (
  `KitProductID` int(10) NOT NULL,
  `KitID` int(10) DEFAULT NULL,
  `ProductID` int(10) DEFAULT NULL,
  `Quantity` smallint(5) DEFAULT NULL,
  KEY `KitKitProduct` (`KitID`),
  KEY `ProductKitProduct` (`ProductID`),
  KEY `KitProductID1` (`KitID`),
  KEY `KitProductID` (`KitProductID`),
  KEY `ProductID` (`ProductID`),
  CONSTRAINT `FK_kitproduct_KitID` FOREIGN KEY (`KitID`) REFERENCES `kit` (`KitID`),
  CONSTRAINT `FK_kitproduct_Product` FOREIGN KEY (`ProductID`) REFERENCES `product` (`ProductID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location` (
  `LocationID` int(10) NOT NULL,
  `LocationTypeID` int(10) DEFAULT NULL,
  `LocationDescription` varchar(255) DEFAULT NULL,
  `Address` varchar(255) DEFAULT NULL,
  `Suburb` varchar(255) DEFAULT NULL,
  `State` varchar(255) DEFAULT NULL,
  `Postcode` varchar(4) DEFAULT NULL,
  `Country` varchar(255) DEFAULT NULL,
  `PickStock` tinyint(1) NOT NULL,
  `HireReturns` tinyint(1) NOT NULL,
  PRIMARY KEY (`LocationID`),
  KEY `LocationTypeID` (`LocationTypeID`),
  KEY `LocationTypeLocation` (`LocationTypeID`),
  KEY `LocationID` (`LocationID`),
  KEY `Postcode` (`Postcode`),
  CONSTRAINT `FK_location_LocationTypeID` FOREIGN KEY (`LocationTypeID`) REFERENCES `locationtype` (`LocationTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `locationtype`
--

DROP TABLE IF EXISTS `locationtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locationtype` (
  `LocationTypeID` int(10) NOT NULL,
  `LocationType` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`LocationTypeID`),
  KEY `LocationTypeID` (`LocationTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order` (
  `OrderID` int(10) NOT NULL,
  `ExtendOrderID` int(10) DEFAULT NULL,
  `CustomerID` int(10) DEFAULT NULL,
  `PriceGroupID` int(10) DEFAULT NULL,
  `BillingAddress` varchar(255) DEFAULT NULL,
  `BillingSuburb` varchar(255) DEFAULT NULL,
  `BillingState` varchar(255) DEFAULT NULL,
  `BillingPostcode` varchar(4) DEFAULT NULL,
  `BillingCountry` varchar(255) DEFAULT NULL,
  `ShippingSameAsBilling` tinyint(1) NOT NULL,
  `ShippingAddress` varchar(255) DEFAULT NULL,
  `ShippingSuburb` varchar(255) DEFAULT NULL,
  `ShippingState` varchar(255) DEFAULT NULL,
  `ShippingPostcode` varchar(4) DEFAULT NULL,
  `ShippingCountry` varchar(255) DEFAULT NULL,
  `HomePhone` varchar(255) DEFAULT NULL,
  `BusinessPhone` varchar(255) DEFAULT NULL,
  `Fax` varchar(255) DEFAULT NULL,
  `MobilePhone` varchar(255) DEFAULT NULL,
  `Email` longtext,
  `OrderDate` datetime DEFAULT NULL,
  `CustomerReference` varchar(255) DEFAULT NULL,
  `RequiredDate` datetime DEFAULT NULL,
  `DueDate` datetime DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` varchar(255) DEFAULT NULL,
  `OrderSourceID` int(10) DEFAULT NULL,
  `HospitalID` int(10) DEFAULT NULL,
  `EducatorID` int(10) DEFAULT NULL,
  `AreaManagerID` int(10) DEFAULT NULL,
  `Pharmacy` varchar(255) DEFAULT NULL,
  `ShipMethodID` int(10) DEFAULT NULL,
  `ShipBarcode` varchar(255) DEFAULT NULL,
  `ReturnBarcode` varchar(255) DEFAULT NULL,
  `InvoiceDate` datetime DEFAULT NULL,
  `TempInvoiceDate` datetime DEFAULT NULL,
  PRIMARY KEY (`OrderID`),
  KEY `OrderID` (`OrderID`),
  KEY `ShipBarcode` (`ShipBarcode`),
  KEY `BillingPostcode` (`BillingPostcode`),
  KEY `ShipMethodID` (`ShipMethodID`),
  KEY `EducatorOrder` (`EducatorID`),
  KEY `ExtendOrderID` (`ExtendOrderID`),
  KEY `PriceGroupOrder` (`PriceGroupID`),
  KEY `ShipMethodOrder` (`ShipMethodID`),
  KEY `CustomerOrder` (`CustomerID`),
  KEY `ShippingPostcode` (`ShippingPostcode`),
  KEY `CustomerID` (`CustomerID`),
  KEY `AreaManagerOrder` (`AreaManagerID`),
  KEY `EducatorID` (`EducatorID`),
  KEY `HospitalID` (`HospitalID`),
  KEY `OrderSourceID` (`OrderSourceID`),
  KEY `HospitalOrder` (`HospitalID`),
  KEY `AreaManagerID` (`AreaManagerID`),
  KEY `PriceGroupID` (`PriceGroupID`),
  KEY `ReturnBarcode` (`ReturnBarcode`),
  KEY `OrderSourceOrder` (`OrderSourceID`),
  CONSTRAINT `FK_orderShipMethod` FOREIGN KEY (`ShipMethodID`) REFERENCES `shipmethod` (`ShipMethodID`),
  CONSTRAINT `FK_order_AreaManager` FOREIGN KEY (`AreaManagerID`) REFERENCES `areamanager` (`AreaManagerID`),
  CONSTRAINT `FK_order_Customer` FOREIGN KEY (`CustomerID`) REFERENCES `customer` (`CustomerID`),
  CONSTRAINT `FK_order_PriceGroup` FOREIGN KEY (`PriceGroupID`) REFERENCES `pricegroup` (`PriceGroupID`),
  CONSTRAINT `FK_order_educator` FOREIGN KEY (`EducatorID`) REFERENCES `educator` (`EducatorID`),
  CONSTRAINT `FK_order_hospital` FOREIGN KEY (`HospitalID`) REFERENCES `hospital` (`HospitalID`),
  CONSTRAINT `FK_order_ordersource` FOREIGN KEY (`OrderSourceID`) REFERENCES `ordersource` (`OrderSourceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `orderdetail`
--

DROP TABLE IF EXISTS `orderdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orderdetail` (
  `OrderDetailID` int(10) NOT NULL,
  `OrderID` int(10) DEFAULT NULL,
  `ProductID` int(10) DEFAULT NULL,
  `Quantity` smallint(5) DEFAULT NULL,
  `UnitPriceRRP` decimal(19,4) DEFAULT NULL,
  `UnitPrice` decimal(19,4) DEFAULT NULL,
  `GST` tinyint(1) NOT NULL,
  `GST_Percent` double DEFAULT NULL,
  `GST_Amount` decimal(19,4) DEFAULT NULL,
  `Hire` tinyint(1) NOT NULL,
  `ReturnExpectedDate` datetime DEFAULT NULL,
  `ReturnActualDate` datetime DEFAULT NULL,
  `CancelReminderMessages` tinyint(1) NOT NULL,
  `TempBarcode` varchar(18) DEFAULT NULL,
  `TempPicked` tinyint(1) NOT NULL,
  `TempPickQuantity` smallint(5) DEFAULT NULL,
  `TempTransactionInclude` tinyint(1) NOT NULL,
  `TempTransactionAmount` decimal(19,4) DEFAULT NULL,
  `TempReturnQuantity` int(10) DEFAULT NULL,
  `RefundProcessedDate` datetime DEFAULT NULL,
  `TempRefundQuantity` double DEFAULT NULL,
  PRIMARY KEY (`OrderDetailID`),
  KEY `OrderID` (`OrderID`),
  KEY `ProductOrderDetail` (`ProductID`),
  KEY `OrderDetailID` (`OrderDetailID`),
  KEY `OrderOrderDetail` (`OrderID`),
  KEY `TaxCodeID` (`GST`),
  KEY `TempBarcode` (`TempBarcode`),
  KEY `ProductID` (`ProductID`),
  CONSTRAINT `FK_orderdetail_Product` FOREIGN KEY (`ProductID`) REFERENCES `product` (`ProductID`),
  CONSTRAINT `FK_orderdetail_order` FOREIGN KEY (`OrderID`) REFERENCES `order` (`OrderID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ordersource`
--

DROP TABLE IF EXISTS `ordersource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordersource` (
  `OrderSourceID` int(10) NOT NULL,
  `OrderSource` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`OrderSourceID`),
  UNIQUE KEY `transSourceID` (`OrderSourceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `paymentmethod`
--

DROP TABLE IF EXISTS `paymentmethod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paymentmethod` (
  `PaymentMethodID` int(10) NOT NULL,
  `PaymentMethod` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PaymentMethodID`),
  KEY `PaymentMethodID` (`PaymentMethodID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `paymentterminal`
--

DROP TABLE IF EXISTS `paymentterminal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paymentterminal` (
  `PaymentTerminalID` int(10) NOT NULL,
  `PaymentMethodID` int(10) DEFAULT NULL,
  `TerminalID` int(10) DEFAULT NULL,
  PRIMARY KEY (`PaymentTerminalID`),
  KEY `TerminalID` (`TerminalID`),
  KEY `PaymentMethodID` (`PaymentMethodID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `postcode`
--

DROP TABLE IF EXISTS `postcode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `postcode` (
  `Postcode` varchar(4) NOT NULL,
  PRIMARY KEY (`Postcode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `postcodesuburb`
--

DROP TABLE IF EXISTS `postcodesuburb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `postcodesuburb` (
  `PostcodeID` int(10) NOT NULL,
  `Postcode` varchar(255) DEFAULT NULL,
  `Locality` varchar(255) DEFAULT NULL,
  `State` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `DeliveryOffice` varchar(255) DEFAULT NULL,
  `PresortIndicator` varchar(255) DEFAULT NULL,
  `ParcelZone` varchar(255) DEFAULT NULL,
  `BSPnumber` varchar(255) DEFAULT NULL,
  `BSPname` varchar(255) DEFAULT NULL,
  `Category` varchar(255) DEFAULT NULL,
  UNIQUE KEY `ID` (`PostcodeID`),
  KEY `Pcode` (`Postcode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pricegroup`
--

DROP TABLE IF EXISTS `pricegroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pricegroup` (
  `PriceGroupID` int(10) NOT NULL,
  `PriceGroup` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PriceGroupID`),
  KEY `PriceGroupID` (`PriceGroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `ProductID` int(10) NOT NULL,
  `ProductCategoryID` int(10) DEFAULT NULL,
  `ProductCode` varchar(255) DEFAULT NULL,
  `ProductName` varchar(255) DEFAULT NULL,
  `Active` tinyint(1) NOT NULL,
  `Notes` longtext,
  `Hire` tinyint(1) NOT NULL,
  `TaxCodeID` int(10) DEFAULT NULL,
  `LocationID` int(10) DEFAULT NULL,
  `HasBarcodeID` tinyint(3) unsigned DEFAULT NULL,
  `zzGST` tinyint(1) NOT NULL,
  `AccountCode` varchar(255) DEFAULT NULL,
  `SwapToProductID` int(10) DEFAULT NULL,
  `ImportantDatesNotification` tinyint(1) NOT NULL,
  `ShowRefundButton` tinyint(1) NOT NULL,
  `PriceList` tinyint(1) NOT NULL,
  PRIMARY KEY (`ProductID`),
  KEY `AccountCode` (`AccountCode`),
  KEY `TaxCodeID` (`TaxCodeID`),
  KEY `TaxCodeProduct` (`TaxCodeID`),
  KEY `ProductCode` (`ProductCode`),
  KEY `ProductCategoryID` (`ProductCategoryID`),
  KEY `HasBarcode` (`HasBarcodeID`),
  KEY `LocationProduct` (`LocationID`),
  KEY `HasBarcodeProduct` (`HasBarcodeID`),
  KEY `ProductCategoryProduct` (`ProductCategoryID`),
  KEY `SwapToProductID` (`SwapToProductID`),
  KEY `ProductID` (`ProductID`),
  CONSTRAINT `FK_product_HasBarcode` FOREIGN KEY (`HasBarcodeID`) REFERENCES `hasbarcode` (`HasBarcodeID`),
  CONSTRAINT `FK_product_Location` FOREIGN KEY (`LocationID`) REFERENCES `location` (`LocationID`),
  CONSTRAINT `FK_product_ProductCategory` FOREIGN KEY (`ProductCategoryID`) REFERENCES `productcategory` (`ProductCategoryID`),
  CONSTRAINT `FK_product_TaxCode` FOREIGN KEY (`TaxCodeID`) REFERENCES `taxcode` (`TaxCodeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `productaccountcode`
--

DROP TABLE IF EXISTS `productaccountcode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productaccountcode` (
  `ProductAccountCodeID` int(10) NOT NULL,
  `ProductID` int(10) DEFAULT NULL,
  `TransactionTypeID` int(10) DEFAULT NULL,
  `AccountCode` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ProductAccountCodeID`),
  KEY `TransactionTypeAccountCode` (`TransactionTypeID`),
  KEY `AccountCode` (`AccountCode`),
  KEY `AccountCodeID` (`ProductAccountCodeID`),
  KEY `ProductAccountCode` (`ProductID`),
  KEY `TransactionTypeID` (`TransactionTypeID`),
  KEY `AccountCodeProductAccountCode` (`AccountCode`),
  KEY `ProductID` (`ProductID`),
  CONSTRAINT `FK_productaccountcode_AccountCode` FOREIGN KEY (`AccountCode`) REFERENCES `accountcode` (`AccountCode`),
  CONSTRAINT `FK_productaccountcode_Product` FOREIGN KEY (`ProductID`) REFERENCES `product` (`ProductID`),
  CONSTRAINT `FK_productaccountcode_TransactionType` FOREIGN KEY (`TransactionTypeID`) REFERENCES `transactiontype` (`TransactionTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `productbarcode`
--

DROP TABLE IF EXISTS `productbarcode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productbarcode` (
  `Barcode` varchar(8) NOT NULL,
  `ProductID` int(10) DEFAULT NULL,
  `LocationID` int(10) DEFAULT NULL,
  `OrderID` int(10) DEFAULT NULL,
  PRIMARY KEY (`Barcode`),
  UNIQUE KEY `serialID` (`Barcode`),
  KEY `OrderID` (`OrderID`),
  KEY `ProductProductBarcode` (`ProductID`),
  KEY `LocationProductBarcode` (`LocationID`),
  KEY `productID` (`ProductID`),
  CONSTRAINT `FK_productbarcode_Location` FOREIGN KEY (`LocationID`) REFERENCES `location` (`LocationID`),
  CONSTRAINT `FK_productbarcode_Product` FOREIGN KEY (`ProductID`) REFERENCES `product` (`ProductID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `productcategory`
--

DROP TABLE IF EXISTS `productcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productcategory` (
  `ProductCategoryID` int(10) NOT NULL,
  `ProductCategory` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ProductCategoryID`),
  KEY `ProductCategoryID` (`ProductCategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `productlocation`
--

DROP TABLE IF EXISTS `productlocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productlocation` (
  `ProductLocationID` int(10) NOT NULL,
  `ProductID` int(10) DEFAULT NULL,
  `LocationID` int(10) DEFAULT NULL,
  `Quantity` smallint(5) DEFAULT NULL,
  PRIMARY KEY (`ProductLocationID`),
  UNIQUE KEY `barcodeID` (`ProductLocationID`),
  KEY `OrderID` (`Quantity`),
  KEY `ProductProductLocation` (`ProductID`),
  KEY `LocationProductLocation` (`LocationID`),
  KEY `productID` (`ProductID`),
  CONSTRAINT `FK_productlocation_Location` FOREIGN KEY (`LocationID`) REFERENCES `location` (`LocationID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `productprice`
--

DROP TABLE IF EXISTS `productprice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productprice` (
  `ProductPriceID` int(10) NOT NULL,
  `ProductID` int(10) DEFAULT NULL,
  `PriceGroupID` int(10) DEFAULT NULL,
  `Price` decimal(19,4) DEFAULT NULL,
  PRIMARY KEY (`ProductPriceID`),
  KEY `ProductPriceGroupID` (`PriceGroupID`),
  KEY `ProductProductPrice` (`ProductID`),
  KEY `PriceGroupProductPrice` (`PriceGroupID`),
  KEY `ProductPriceID` (`ProductPriceID`),
  KEY `ProductID` (`ProductID`),
  CONSTRAINT `FK_productprice_PriceGroupID` FOREIGN KEY (`PriceGroupID`) REFERENCES `pricegroup` (`PriceGroupID`),
  CONSTRAINT `FK_productprice_Product` FOREIGN KEY (`ProductID`) REFERENCES `product` (`ProductID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `productreorder`
--

DROP TABLE IF EXISTS `productreorder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productreorder` (
  `ProductReOrderID` int(10) NOT NULL,
  `ProductID` int(10) DEFAULT NULL,
  `LocationID` int(10) DEFAULT NULL,
  `ReOrderQuantity` smallint(5) DEFAULT NULL,
  PRIMARY KEY (`ProductReOrderID`),
  KEY `LocationProductReOrder` (`LocationID`),
  KEY `ProductReOrderID` (`ProductReOrderID`),
  KEY `ProductProductReOrder` (`ProductID`),
  KEY `LocationID` (`LocationID`),
  KEY `ProductID` (`ProductID`),
  CONSTRAINT `FK_productreorder_Location` FOREIGN KEY (`LocationID`) REFERENCES `location` (`LocationID`),
  CONSTRAINT `FK_productreorder_Product` FOREIGN KEY (`ProductID`) REFERENCES `product` (`ProductID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reminder`
--

DROP TABLE IF EXISTS `reminder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reminder` (
  `ReminderID` int(10) NOT NULL,
  `GraceDays` smallint(5) DEFAULT NULL,
  `GraceDaysProgrammerComment` varchar(255) DEFAULT NULL,
  `MessageText` longtext,
  PRIMARY KEY (`ReminderID`),
  KEY `ReminderID` (`ReminderID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `remindersent`
--

DROP TABLE IF EXISTS `remindersent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `remindersent` (
  `ReminderSentID` int(10) NOT NULL,
  `OrderDetailID` int(10) DEFAULT NULL,
  `ReminderID` int(10) DEFAULT NULL,
  `SentDateTime` datetime DEFAULT NULL,
  PRIMARY KEY (`ReminderSentID`),
  KEY `ReminderSentID` (`ReminderSentID`),
  KEY `ReminderReminderSent` (`ReminderID`),
  KEY `OrderDetailReminderSent` (`OrderDetailID`),
  KEY `OrderDetailID` (`OrderDetailID`),
  KEY `ReminderID` (`ReminderID`),
  CONSTRAINT `FK_remindersent_orderdetail` FOREIGN KEY (`OrderDetailID`) REFERENCES `orderdetail` (`OrderDetailID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shipmethod`
--

DROP TABLE IF EXISTS `shipmethod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipmethod` (
  `ShipMethodID` int(10) NOT NULL,
  `ShipMethod` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ShipMethodID`),
  KEY `ShipMethodID` (`ShipMethodID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `staff`
--

DROP TABLE IF EXISTS `staff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staff` (
  `StaffID` int(10) NOT NULL,
  `FirstName` varchar(255) DEFAULT NULL,
  `LastName` varchar(255) DEFAULT NULL,
  `Active` tinyint(1) NOT NULL,
  PRIMARY KEY (`StaffID`),
  KEY `StaffID` (`StaffID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stockmove`
--

DROP TABLE IF EXISTS `stockmove`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stockmove` (
  `StockMoveID` int(10) NOT NULL,
  `StockMoveTypeID` int(10) DEFAULT NULL,
  `MoveDate` datetime DEFAULT NULL,
  `MoveBy` varchar(255) DEFAULT NULL,
  `FromPurchaseOrderNo` varchar(255) DEFAULT NULL,
  `FromLocationID` int(10) DEFAULT NULL,
  `FromOrderID` int(10) DEFAULT NULL,
  `ToLocationID` int(10) DEFAULT NULL,
  `ToOrderID` int(10) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `FromProductID` int(10) DEFAULT NULL,
  `ToProductID` int(10) DEFAULT NULL,
  `TempMoveDate` datetime DEFAULT NULL,
  PRIMARY KEY (`StockMoveID`),
  KEY `OrderID` (`FromOrderID`),
  KEY `StockMoveTypeStockMove` (`StockMoveTypeID`),
  KEY `LocationStockMove` (`FromLocationID`),
  KEY `FromOrderID` (`ToOrderID`),
  KEY `FromLocationID` (`ToLocationID`),
  KEY `LocationID` (`FromLocationID`),
  KEY `StockMovementID` (`StockMoveID`),
  KEY `FromProductID` (`FromProductID`),
  KEY `ToProductID` (`ToProductID`),
  KEY `OrderStockMove` (`ToOrderID`),
  CONSTRAINT `FK_stockmove_Location` FOREIGN KEY (`FromLocationID`) REFERENCES `location` (`LocationID`),
  CONSTRAINT `FK_stockmove_StockMoveType` FOREIGN KEY (`StockMoveTypeID`) REFERENCES `stockmovetype` (`StockMoveTypeID`),
  CONSTRAINT `FK_stockmove_order` FOREIGN KEY (`ToOrderID`) REFERENCES `order` (`OrderID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stockmovedetail`
--

DROP TABLE IF EXISTS `stockmovedetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stockmovedetail` (
  `StockMoveDetailID` int(10) NOT NULL,
  `StockMoveID` int(10) DEFAULT NULL,
  `ProductID` int(10) DEFAULT NULL,
  `OrderDetailID` int(10) DEFAULT NULL,
  `Barcode` varchar(18) DEFAULT NULL,
  `Quantity` smallint(5) DEFAULT NULL,
  PRIMARY KEY (`StockMoveDetailID`),
  KEY `OrderDetailID` (`OrderDetailID`),
  KEY `BarcodeID` (`Barcode`),
  KEY `StockMoveStockMoveDetail` (`StockMoveID`),
  KEY `StockMoveID` (`StockMoveID`),
  KEY `ProductID` (`ProductID`),
  KEY `StockMoveDetailID` (`StockMoveDetailID`),
  KEY `ProductStockMoveDetail` (`ProductID`),
  CONSTRAINT `FK_stockmovedetail_Product` FOREIGN KEY (`ProductID`) REFERENCES `product` (`ProductID`),
  CONSTRAINT `FK_stockmovedetail_StockMove` FOREIGN KEY (`StockMoveID`) REFERENCES `stockmove` (`StockMoveID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stockmovetype`
--

DROP TABLE IF EXISTS `stockmovetype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stockmovetype` (
  `StockMoveTypeID` int(10) NOT NULL,
  `StockMoveType` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`StockMoveTypeID`),
  KEY `StockMoveTypeID` (`StockMoveTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `taxcode`
--

DROP TABLE IF EXISTS `taxcode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taxcode` (
  `TaxCodeID` int(10) NOT NULL,
  `TaxCode` varchar(255) DEFAULT NULL,
  `CalcGST` tinyint(1) NOT NULL,
  `SortOrder` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`TaxCodeID`),
  KEY `TaxCodeID` (`TaxCodeID`),
  KEY `TaxCode` (`TaxCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_tensunit`
--

DROP TABLE IF EXISTS `tbl_tensunit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_tensunit` (
  `Barcode` varchar(255) DEFAULT NULL,
  `ProductID` double DEFAULT NULL,
  `LocationID` double DEFAULT NULL,
  UNIQUE KEY `Barcode` (`Barcode`),
  KEY `LocationID` (`LocationID`),
  KEY `ProductID` (`ProductID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `terminal`
--

DROP TABLE IF EXISTS `terminal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `terminal` (
  `TerminalID` int(10) NOT NULL,
  `Terminal` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`TerminalID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction` (
  `TransactionID` int(10) NOT NULL,
  `OrderID` int(10) DEFAULT NULL,
  `TransactionTypeID` int(10) DEFAULT NULL,
  `PaymentMethodID` int(10) DEFAULT NULL,
  `TransactionDate` datetime DEFAULT NULL,
  `TransactionTotal` decimal(19,4) DEFAULT NULL,
  `TransactionBy` varchar(255) DEFAULT NULL,
  `TransactionReference` varchar(255) DEFAULT NULL,
  `TransactionCreateDateTime` datetime DEFAULT NULL,
  `TerminalID` int(10) DEFAULT NULL,
  `Comments` longtext,
  PRIMARY KEY (`TransactionID`),
  KEY `OrderID` (`OrderID`),
  KEY `OrderTransaction` (`OrderID`),
  KEY `TransactionTypeTransaction` (`TransactionTypeID`),
  KEY `PaymentMethodID` (`PaymentMethodID`),
  KEY `TerminalID` (`TerminalID`),
  KEY `PaymentID` (`TransactionID`),
  KEY `TransactionTypeID` (`TransactionTypeID`),
  KEY `PaymentMethodTransaction` (`PaymentMethodID`),
  CONSTRAINT `FK_transaction_PaymentMethod` FOREIGN KEY (`PaymentMethodID`) REFERENCES `paymentmethod` (`PaymentMethodID`),
  CONSTRAINT `FK_transaction_TransactionType` FOREIGN KEY (`TransactionTypeID`) REFERENCES `transactiontype` (`TransactionTypeID`),
  CONSTRAINT `FK_transaction_order` FOREIGN KEY (`OrderID`) REFERENCES `order` (`OrderID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transactiondetail`
--

DROP TABLE IF EXISTS `transactiondetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactiondetail` (
  `TransactionDetailID` int(10) NOT NULL,
  `TransactionID` int(10) DEFAULT NULL,
  `OrderDetailID` int(10) DEFAULT NULL,
  `Amount` decimal(19,4) DEFAULT NULL,
  `AccountCode` varchar(255) DEFAULT NULL,
  `GST_Amount` decimal(19,4) DEFAULT NULL,
  PRIMARY KEY (`TransactionDetailID`),
  KEY `AccountCode` (`AccountCode`),
  KEY `TransactionDetailID` (`TransactionDetailID`),
  KEY `OrderDetailID` (`OrderDetailID`),
  KEY `TransactionTransactionDetail` (`TransactionID`),
  KEY `TransactionID` (`TransactionID`),
  KEY `OrderDetailTransactionDetail` (`OrderDetailID`),
  CONSTRAINT `FK_transactiondetail_OrderDetail` FOREIGN KEY (`OrderDetailID`) REFERENCES `orderdetail` (`OrderDetailID`),
  CONSTRAINT `FK_transactiondetail_transaction` FOREIGN KEY (`TransactionID`) REFERENCES `transaction` (`TransactionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transactiontype`
--

DROP TABLE IF EXISTS `transactiontype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactiontype` (
  `TransactionTypeID` int(10) NOT NULL,
  `TransactionType` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`TransactionTypeID`),
  KEY `TransactionID` (`TransactionTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usysapplicationlog`
--

DROP TABLE IF EXISTS `usysapplicationlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usysapplicationlog` (
  `ID` int(10) NOT NULL,
  `SourceObject` varchar(255) DEFAULT NULL,
  `Data Macro Instance ID` varchar(255) DEFAULT NULL,
  `Error Number` int(10) DEFAULT NULL,
  `Category` varchar(255) DEFAULT NULL,
  `Object Type` varchar(255) DEFAULT NULL,
  `Description` longtext,
  `Context` varchar(255) DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zzcopy of order`
--

DROP TABLE IF EXISTS `zzcopy of order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zzcopy of order` (
  `OrderID` int(10) NOT NULL,
  `ExtendOrderID` int(10) DEFAULT NULL,
  `CustomerID` int(10) DEFAULT NULL,
  `PriceGroupID` int(10) DEFAULT NULL,
  `BillingAddress` varchar(255) DEFAULT NULL,
  `BillingSuburb` varchar(255) DEFAULT NULL,
  `BillingState` varchar(255) DEFAULT NULL,
  `BillingPostcode` varchar(4) DEFAULT NULL,
  `BillingCountry` varchar(255) DEFAULT NULL,
  `ShippingSameAsBilling` tinyint(1) NOT NULL,
  `ShippingAddress` varchar(255) DEFAULT NULL,
  `ShippingSuburb` varchar(255) DEFAULT NULL,
  `ShippingState` varchar(255) DEFAULT NULL,
  `ShippingPostcode` varchar(4) DEFAULT NULL,
  `ShippingCountry` varchar(255) DEFAULT NULL,
  `HomePhone` varchar(255) DEFAULT NULL,
  `BusinessPhone` varchar(255) DEFAULT NULL,
  `Fax` varchar(255) DEFAULT NULL,
  `MobilePhone` varchar(255) DEFAULT NULL,
  `Email` longtext,
  `OrderDate` datetime DEFAULT NULL,
  `CustomerReference` varchar(255) DEFAULT NULL,
  `RequiredDate` datetime DEFAULT NULL,
  `DueDate` datetime DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` varchar(255) DEFAULT NULL,
  `OrderSourceID` int(10) DEFAULT NULL,
  `HospitalID` int(10) DEFAULT NULL,
  `EducatorID` int(10) DEFAULT NULL,
  `AreaManagerID` int(10) DEFAULT NULL,
  `Pharmacy` varchar(255) DEFAULT NULL,
  `ShipMethodID` int(10) DEFAULT NULL,
  `ShipBarcode` varchar(255) DEFAULT NULL,
  `ReturnBarcode` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`OrderID`),
  KEY `OrderID` (`OrderID`),
  KEY `BillingPostcode` (`BillingPostcode`),
  KEY `ShipMethodID` (`ShipMethodID`),
  KEY `ExtendOrderID` (`ExtendOrderID`),
  KEY `ShippingPostcode` (`ShippingPostcode`),
  KEY `ShipBarcode` (`ShipBarcode`),
  KEY `CustomerID` (`CustomerID`),
  KEY `EducatorID` (`EducatorID`),
  KEY `HospitalID` (`HospitalID`),
  KEY `OrderSourceID` (`OrderSourceID`),
  KEY `ReturnBarcode` (`ReturnBarcode`),
  KEY `AreaManagerID` (`AreaManagerID`),
  KEY `PriceGroupID` (`PriceGroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zzorderdetail1`
--

DROP TABLE IF EXISTS `zzorderdetail1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zzorderdetail1` (
  `OrderDetailID` int(10) NOT NULL,
  `OrderID` int(10) DEFAULT NULL,
  `ProductID` int(10) DEFAULT NULL,
  `Quantity` smallint(5) DEFAULT NULL,
  `UnitPriceRRP` decimal(19,4) DEFAULT NULL,
  `UnitPrice` decimal(19,4) DEFAULT NULL,
  `GST` tinyint(1) NOT NULL,
  `GST_Percent` double DEFAULT NULL,
  `GST_Amount` decimal(19,4) DEFAULT NULL,
  `Hire` tinyint(1) NOT NULL,
  `ReturnExpectedDate` datetime DEFAULT NULL,
  `ReturnActualDate` datetime DEFAULT NULL,
  `CancelReminderMessages` tinyint(1) NOT NULL,
  `TempBarcode` varchar(18) DEFAULT NULL,
  `TempPicked` tinyint(1) NOT NULL,
  `TempPickQuantity` smallint(5) DEFAULT NULL,
  `TempTransactionInclude` tinyint(1) NOT NULL,
  `TempTransactionAmount` decimal(19,4) DEFAULT NULL,
  PRIMARY KEY (`OrderDetailID`),
  KEY `OrderID` (`OrderID`),
  KEY `TaxCodeID` (`GST`),
  KEY `OrderDetailID` (`OrderDetailID`),
  KEY `TempBarcode` (`TempBarcode`),
  KEY `ProductID` (`ProductID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-02-18 14:00:13
