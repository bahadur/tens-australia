LOCK TABLES 
    `order` WRITE,
    `customernote` WRITE,
    `stockmove` WRITE,
    `transaction` WRITE,
    `orderdetail` WRITE;# MySQL returned an empty result set (i.e. zero rows).

    /*
ALTER TABLE `customernote` DROP FOREIGN KEY FK_customernote_order;  
ALTER TABLE `orderdetail`  DROP FOREIGN KEY FK_orderdetail_order;
ALTER TABLE `transaction` DROP FOREIGN KEY FK_transaction_order;
ALTER TABLE `stockmove` DROP FOREIGN KEY FK_stockmove_order;*/
    
ALTER TABLE `order`
  CHANGE COLUMN `OrderID` `OrderID` INT(10) NOT NULL AUTO_INCREMENT,
  DROP PRIMARY KEY,
  ADD PRIMARY KEY (`OrderID`);# MySQL returned an empty result set (i.e. zero rows).

  
  ALTER TABLE `customernote`
    ADD CONSTRAINT FK_customernote_order FOREIGN KEY (`OrderID`) REFERENCES `order` (`OrderID`) ON UPDATE CASCADE ON DELETE CASCADE;# 1 row affected.
;
  ALTER TABLE `orderdetail`
    ADD CONSTRAINT FK_orderdetail_order FOREIGN KEY (`OrderID`) REFERENCES `order` (`OrderID`) ON UPDATE CASCADE ON DELETE CASCADE;# 18 rows affected.
;
  ALTER TABLE `stockmove`
    ADD CONSTRAINT FK_stockmove_order FOREIGN KEY (`ToOrderID`) REFERENCES `order` (`OrderID`) ON UPDATE CASCADE ON DELETE CASCADE;# 5 rows affected.
;
  ALTER TABLE `transaction`
    ADD CONSTRAINT FK_transaction_order FOREIGN KEY (`OrderID`) REFERENCES `order` (`OrderID`) ON UPDATE CASCADE ON DELETE CASCADE;# 4 rows affected.
;

UNLOCK TABLES;