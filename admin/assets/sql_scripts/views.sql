
DROP VIEW IF EXISTS qryFulfilmentListQtyShipped;
DROP VIEW IF EXISTS qryOrderListShippedStatus;
DROP VIEW IF EXISTS qryFulfilmentListOrderPaid;
DROP VIEW IF EXISTS qryFulfilmentListAllShipped;
DROP VIEW IF EXISTS educator_crosstab_m;
DROP FUNCTION IF EXISTS priceGroupIdVar;
DROP VIEW IF EXISTS qryProductPrice;
DROP VIEW IF EXISTS qryPaymentTransactionsDetail;
DROP VIEW IF EXISTS qryHireReturnQty; 

CREATE VIEW qryFulfilmentListQtyShipped AS (
    SELECT orderdetail.OrderID, stockmovedetail.OrderDetailID, SUM(stockmovedetail.Quantity) AS ShippedQuantity, stockmove.MoveDate
  FROM stockmove INNER JOIN stockmovedetail ON stockmove.StockMoveID = stockmovedetail.StockMoveID INNER JOIN orderdetail ON 
  orderdetail.OrderDetailID = stockmovedetail.OrderDetailID WHERE stockmove.StockMoveTypeID =2
  GROUP BY  orderdetail.OrderID, stockmovedetail.OrderDetailID
  );
 
CREATE VIEW qryOrderListShippedStatus AS (
SELECT order.OrderID, IF(SUM(orderdetail.Quantity)-SUM(qryFulfilmentListQtyShipped.ShippedQuantity)=0,'All Quantities Shipped','Pending') AS ShippedStatus FROM `order` INNER JOIN (orderdetail LEFT JOIN qryFulfilmentListQtyShipped ON orderdetail.OrderDetailID = qryFulfilmentListQtyShipped.OrderDetailID) ON order.OrderID = orderdetail.OrderID GROUP BY order.OrderID); 


CREATE VIEW qryFulfilmentListOrderPaid AS (
SELECT orderdetail.OrderID, Sum(UnitPrice*Quantity) AS SumofLineTotal, Sum(transactiondetail.Amount) AS SumOfAmount
FROM `transaction` INNER JOIN (orderdetail INNER JOIN transactiondetail ON orderdetail.OrderDetailID = transactiondetail.OrderDetailID) ON transaction.TransactionID = transactiondetail.TransactionID
WHERE (((transaction.TransactionTypeID)=1))
GROUP BY orderdetail.OrderID
);


CREATE VIEW qryFulfilmentListAllShipped AS (
SELECT OrderID, SUM(IF(AllShipped = "YES", 1, 0)) AS ProductsFullyShippedCount, SUM(IF(AllShipped = "NO", 1, 0)) AS ProductsNotFullyShippedCount, OrderedQuantity FROM `qryShippedStatuses` GROUP BY OrderID HAVING 
ProductsNotFullyShippedCount = 0);

CREATE VIEW educator_crosstab_m AS
(SELECT educator.BusinessName, 
	sum(case when educator.State = 'VIC' THEN 1 ELSE 0 end) `NULL`,
	sum(case when educator.State IS NULL THEN 1 ELSE 0 end) VIC,
	sum(case when educator.State = 'NSW' THEN 1 ELSE 0 end) NSW
FROM educator 
GROUP BY educator.BusinessName);


/* 
   A hacker's way of creating a way that accepts a parameter. ;) 
   To use it, we need to do 
   SET @priceGroupIdVar = 1; before we use the view
*/
CREATE FUNCTION priceGroupIdVar() RETURNS int(32)
  RETURN @priceGroupIdVar;

CREATE VIEW qryProductPrice AS
(SELECT productprice.ProductID, productprice.Price, PriceGroupID
FROM productprice WHERE PriceGroupID = priceGroupIdVar());


/*

qryPaymentTransactionsDetail

SELECT TransactionDetail.TransactionID, Transaction.TransactionDate, TransactionDetail.TransactionDetailID, TransactionDetail.OrderDetailID
FROM [Transaction] INNER JOIN TransactionDetail ON Transaction.TransactionID=TransactionDetail.TransactionID
WHERE (((Transaction.TransactionTypeID)=1));


*/

CREATE VIEW qryPaymentTransactionsDetail AS
(SELECT transactiondetail.TransactionID, transaction.TransactionDate, transactiondetail.TransactionDetailID, transactiondetail.OrderDetailID
FROM `transaction` INNER JOIN transactiondetail ON transaction.TransactionID=transactiondetail.TransactionID
WHERE (((transaction.TransactionTypeID)=1)));


CREATE VIEW qryHireReturnQty AS 
(SELECT StockMoveDetail.Quantity, StockMoveDetail.OrderDetailID
FROM StockMove INNER JOIN StockMoveDetail ON StockMove.StockMoveID = StockMoveDetail.StockMoveID
WHERE (((StockMove.StockMoveTypeID)=3))
);



DROP VIEW IF EXISTS qryItemsInOrders;
DROP VIEW IF EXISTS qryOrdersFullyShippedItemsCount;
DROP VIEW IF EXISTS qryShippedStatuses; 
CREATE VIEW qryItemsInOrders AS (
SELECT `order`.OrderID, Count(OrderDetailID) AS ItemsInOrder FROM orderdetail INNER JOIN `order` ON `order`.OrderID = orderdetail.OrderID 
GROUP BY `order`.OrderID
);


CREATE VIEW qryOrdersFullyShippedItemsCount AS (
    SELECT stockmovedetail.OrderDetailID, COUNT((SUM(stockmovedetail.Quantity) - orderdetail.Quantity) > 0), stockmove.MoveDate
  FROM stockmove INNER JOIN stockmovedetail ON stockmove.StockMoveID = stockmovedetail.StockMoveID INNER JOIN 
  orderdetail ON stockmovedetail.OrderDetailID = orderdetail.OrderDetailID
  WHERE stockmove.StockMoveTypeID =2
  GROUP BY stockmovedetail.OrderDetailID, stockmovedetail.OrderID 
);


CREATE VIEW qryShippedStatuses AS (
SELECT orderdetail.OrderID, orderdetail.OrderDetailID, qryFulfilmentListQtyShipped.ShippedQuantity, orderdetail.Quantity AS OrderedQuantity, 
IF( (qryFulfilmentListQtyShipped.ShippedQuantity - orderdetail.Quantity) >= 0, "YES", "NO") AS AllShipped FROM `order` INNER JOIN orderdetail ON `order`.OrderID = orderdetail.OrderID LEFT JOIN qryFulfilmentListQtyShipped ON qryFulfilmentListQtyShipped.OrderDetailID = orderdetail.OrderDetailID
);

DROP VIEW IF EXISTS qryReturnFormQuantityAlreadyShipped;
CREATE VIEW qryReturnFormQuantityAlreadyShipped AS (
  SELECT stockmovedetail.OrderDetailID, Sum(stockmovedetail.Quantity) AS SumOfQuantity
  FROM stockmove INNER JOIN stockmovedetail ON stockmove.StockMoveID = stockmovedetail.StockMoveID
  GROUP BY stockmovedetail.OrderDetailID, stockmove.StockMoveTypeID
  HAVING (((stockmove.StockMoveTypeID)=2)); 
);


DROP VIEW IF EXISTS qrySearchByLocationLastStockMoveSubquery;

DROP VIEW IF EXISTS qrySearchByLocationLastStockMove;


CREATE VIEW qrySearchByLocationLastStockMoveSubquery AS (
(SELECT stockmovedetail.ProductID, stockmovedetail.Barcode, Max(stockmove.MoveDate) AS LastMoveDate 
  FROM stockmove INNER JOIN stockmovedetail ON stockmove.StockMoveID = stockmovedetail.StockMoveID 
  GROUP BY stockmovedetail.Barcode) 
);

CREATE VIEW qrySearchByLocationLastStockMove AS (
SELECT LastMove.Barcode, LastMove.LastMoveDate  AS MoveDate, LastMove.ProductID
FROM qrySearchByLocationLastStockMoveSubquery AS LastMove
);


CREATE VIEW qryDepositRefundAmount AS (
    SELECT transactiondetail.OrderDetailID, transactiondetail.Amount, transactiondetail.GST_Amount, transactiondetail.TransactionID, 
  paymentmethod.PaymentMethod, `transaction`.TransactionDate, `transaction`.Comments
FROM (paymentmethod RIGHT JOIN `transaction` ON paymentmethod.PaymentMethodID = `transaction`.PaymentMethodID) INNER JOIN 
  transactiondetail ON `transaction`.TransactionID = transactiondetail.TransactionID
WHERE (((`transaction`.TransactionTypeID)=2))
ORDER BY transactiondetail.OrderDetailID
);


SELECT transactiondetail.OrderDetailID, transactiondetail.Amount, transactiondetail.GST_Amount, transactiondetail.TransactionID, 
  paymentmethod.PaymentMethod, `transaction`.TransactionDate, `transaction`.Comments
FROM (paymentmethod RIGHT JOIN `transaction` ON paymentmethod.PaymentMethodID = `transaction`.PaymentMethodID) INNER JOIN 
  transactiondetail ON `transaction`.TransactionID = transactiondetail.TransactionID
WHERE (((`transaction`.TransactionTypeID)=2))
ORDER BY transactiondetail.OrderDetailID;
CREATE VIEW qryOrderPickupLocation AS (
  SELECT stockmove.ToOrderID, location.LocationDescription
  FROM location RIGHT JOIN stockmove ON location.LocationID=stockmove.FromLocationID
  WHERE (((stockmove.StockMoveTypeID)=2) AND ((location.PickStock)=1))
  GROUP BY stockmove.ToOrderID, location.LocationDescription
  ORDER BY stockmove.ToOrderID
);
