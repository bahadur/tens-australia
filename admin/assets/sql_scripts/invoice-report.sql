SELECT order.OrderID, customer.Firstname, customer.LastName, `order`.BillingAddress, `order`.BillingSuburb, `order`.BillingState, 
`order`.BillingPostcode, `order`.HomePhone, `order`.MobilePhone, `order`.BusinessPhone, 
CONCAT(educator.Firstname, " ", educator.Lastname) AS Educator, 
`order`.CustomerReference, orderdetail.Quantity, product.ProductCode, 
CONCAT(ProductName, "(", SUBSTRING((SELECT Barcode AS BarcodeU FROM stockmovedetail WHERE orderdetail.OrderID=158973),0, 14),")") AS ProductDetail, 
orderdetail.UnitPrice, ROUND(orderdetail.Quantity*orderdetail.UnitPrice,2) AS `Total$`, orderdetail.GST_Amount, SumOfAmount 
AS PaymentTotal, `order`.OrderDate, IF(`order`.HomePhone IS NULL, `order`.MobilePhone, `order`.MobilePhone) 
AS CustomerTel, Hospital.HospitalName, product.hire AS HasHire, ShipMethod.ShipMethod, customer.BusinessName, 
PaymentMethods(`order`.OrderID,1) AS PaymentMethod, `order`.InvoiceDate, ImportantInformationText(`order`.OrderID) 
AS ImportantInfo, orderdetail.OrderDetailID 
	FROM ShipMethod RIGHT JOIN (product INNER JOIN ((Hospital RIGHT JOIN (Educator RIGHT JOIN (customer 
		INNER JOIN ((SELECT orderdetail.OrderID, Sum(transactiondetail.Amount) AS SumOfAmount FROM `transaction` 
			INNER JOIN (orderdetail INNER JOIN transactiondetail ON orderdetail.OrderDetailID = transactiondetail.OrderDetailID) 
				ON transaction.TransactionID = transactiondetail.TransactionID WHERE (((transaction.TransactionTypeID)=1)) 
					GROUP BY orderdetail.OrderID)  AS PaymentTotal RIGHT JOIN `order` ON PaymentTotal.OrderID = `order`.OrderID) 
						ON customer.CustomerID = `order`.CustomerID) ON Educator.EducatorID = `order`.EducatorID) 
							ON Hospital.HospitalID = `order`.HospitalID) INNER JOIN orderdetail ON `order`.OrderID = orderdetail.OrderID) 
								ON product.ProductID = orderdetail.ProductID) ON ShipMethod.ShipMethodID = `order`.ShipMethodID WHERE
									OrderID=158973; 




DLookUp("Barcode","stockmovedetail","OrderDetailID=" & orderdetail.OrderDetailID

SELECT Barcode AS BarcodeU FROM stockmovedetail WHERE (orderdetails.OrderID=158973)

;

DLookup("[UnitPrice]", "Order Details", "OrderID = 10248")

SELECT UnitPrice AS Expr1
FROM [Order Details]
WHERE ((([Order Details].OrderID)=10248));