/*
 Example call:
 CALL getProductsInKit(123, MAKEDATE(2015,1), 5, 6);
*/

DELIMITER //
CREATE PROCEDURE getProductsInKit(IN `orderId` INT(10), IN `dueDate` DATETIME, IN `kitID` INT(10), IN `priceGroupID` INT(10))
BEGIN
SELECT `orderId` AS OrderID, kitproduct.ProductID, productprice.Price, kitproduct.Quantity, taxcode.CalcGST, 
	CONVERT(ROUND(IF ( taxcode.CalcGST, config.GST_Percent, 0), 2),DECIMAL(16,2)) AS GST_Percent,
	CONVERT(
		
			ROUND(Price * Quantity *
					(1-
						(1/
							(1+ 
								ROUND(
									IF(taxcode.CalcGST = 1, config.GST_Percent, 0),4
									)
							)
						)
					)*(CalcGST),2
				) 
				, DECIMAL(16,2)
			) AS Tax_Amount, 
		
	product.Hire, 
	IF(Hire = True AND CHAR_LENGTH(`dueDate`) > 0, DATE_ADD(`dueDate`, INTERVAL ReturnExpectedDuePlusDays DAY),Null) AS ReturnExpectedDate 
	FROM config, taxcode INNER JOIN ((product INNER JOIN kitproduct ON product.ProductID = kitproduct.ProductID) 
		INNER JOIN productprice ON product.ProductID = productprice.ProductID) ON taxcode.TaxCodeID = product.TaxCodeID 
	WHERE(((kitproduct.KitID) = `kitID`) And ((productprice.PriceGroupID) = `priceGroupID`) And ((Config.ConfigID) = 1)) 
	ORDER BY kitproduct.KitProductID; 
END //
DELIMITER ;		