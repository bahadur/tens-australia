LOCK TABLES 
    `orderdetail` WRITE, 
    `transactiondetail` WRITE,
    `remindersent` WRITE;

    
ALTER TABLE `remindersent` DROP FOREIGN KEY FK_remindersent_orderdetail;  
ALTER TABLE `transactiondetail`  DROP FOREIGN KEY FK_transactiondetail_OrderDetail; 
    
ALTER TABLE `orderdetail`
  CHANGE COLUMN `OrderDetailID` `OrderDetailID` INT(10) NOT NULL AUTO_INCREMENT,
  DROP PRIMARY KEY,
  ADD PRIMARY KEY (`OrderDetailID`);# MySQL returned an empty result set (i.e. zero rows).

  
  ALTER TABLE `remindersent`
    ADD CONSTRAINT FK_remindersent_orderdetail FOREIGN KEY (`OrderDetailID`) REFERENCES `orderdetail` (`OrderDetailID`) ON UPDATE CASCADE ON DELETE CASCADE;# 1 row affected.
;
  ALTER TABLE `transactiondetail`
    ADD  CONSTRAINT `FK_transactiondetail_OrderDetail` FOREIGN KEY (`OrderDetailID`) REFERENCES `orderdetail` (`OrderDetailID`) ON UPDATE CASCADE ON DELETE CASCADE;# 18 rows affected.
; 

UNLOCK TABLES;