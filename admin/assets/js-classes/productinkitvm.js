/*
	Requires a jQuery.
*/
 

function ProductInKitVM() 
{  
}


ProductInKitVM.prototype.loadJson = function(jsonObj)
{ 
    var self = this; 
    self.orderId = jsonObj['orderId'];
    self.productId = jsonObj['productId']; 
    self.price = jsonObj['price']; 
    self.quantity = jsonObj['quantity']; 
    self.calcGst = jsonObj['calcGst']; 
    self.gstPercent = jsonObj['gstPercent']; 
    self.taxAmount = jsonObj['taxAmount']; 
    self.hire = jsonObj['hire']; 
    self.returnExpectedDate = jsonObj['returnExpectedDate'];  
}
 
ProductInKitVM.prototype.getOrderId = function()
{
	return this.orderId;
}

ProductInKitVM.prototype.getProductId = function()
{
	return this.productId;
}

ProductInKitVM.prototype.getPrice = function()
{
	return this.price;
}

ProductInKitVM.prototype.getQuantity = function()
{
	return this.quantity;
}

ProductInKitVM.prototype.getCalcGst = function()
{
	return this.calcGst;
}

ProductInKitVM.prototype.getGstPercent = function()
{
	return this.gstPercent;
}

ProductInKitVM.prototype.getTaxAmount = function()
{
	return this.taxAmount;
}

ProductInKitVM.prototype.getHire = function()
{
	return this.hire;
}

ProductInKitVM.prototype.getReturnExpectedDate = function()
{
	return this.returnExpectedDate;
}

/*  Setters  */


ProductInKitVM.prototype.setOrderId = function(value)
{
	this.orderId;
}

ProductInKitVM.prototype.setProductId = function(value)
{
	this.productId = value;
}

ProductInKitVM.prototype.setPrice = function(value)
{
	this.price = value;
}

ProductInKitVM.prototype.setQuantity = function(value)
{
	this.quantity = value;
}

ProductInKitVM.prototype.setCalcGst = function(value)
{
	this.calcGst = value;
}

ProductInKitVM.prototype.setGstPercent = function(value)
{
	this.gstPercent = value;
}

ProductInKitVM.prototype.setTaxAmount = function(value)
{
	this.taxAmount = value;
}

ProductInKitVM.prototype.setHire = function(value)
{
	this.hire = value;
}

ProductInKitVM.prototype.setReturnExpectedDate = function(value)
{
	this.returnExpectedDate = value;
}