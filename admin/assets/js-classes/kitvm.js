/*
	Requires a jQuery.
*/
 
function KitVM() 
{  
}


KitVM.prototype.loadJson = function(jsonObj)
{ 
    var self = this;
    self.kitId = jsonObj['kitId'];;
    self.kitName = jsonObj['kitName']; 
}

KitVM.prototype.getKitId = function()
{
    return this.kitId;
}
 
KitVM.prototype.getKitName = function()
{
    return this.kitName;
}


KitVM.prototype.setKitId = function(value)
{
    this.kitId = value;
}
 
KitVM.prototype.setKitName = function(value)
{
    this.kitName = value;
}