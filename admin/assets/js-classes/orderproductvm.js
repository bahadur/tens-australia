/*
	Requires a jQuery.
*/
 
function OrderProductVM() 
{ 
}


OrderProductVM.prototype.loadJson = function(jsonObj) 
{
    var self = this;
    self.productId = jsonObj['productId'];
    self.productName = jsonObj['productName'];
    self.hire = jsonObj['hire'];
    self.price = jsonObj['price'];
    self.calcGst = jsonObj['calcGst'];
    self.gstPercent = jsonObj['gstPercent'];
    self.hasBarcodeId = jsonObj['hasBarcodeId']; 
}

OrderProductVM.prototype.setProductId = function(value) 
{
    this.productId = value;
}

OrderProductVM.prototype.setProductName = function(value) 
{
    this.productName = value;
}

OrderProductVM.prototype.setHire = function(value) 
{
    this.hire = value;
}

OrderProductVM.prototype.setPrice = function(value) 
{
    this.price = value;
}

OrderProductVM.prototype.setCalcGst = function(value) 
{
    this.calcGst = value;
}

OrderProductVM.prototype.setGstPercent = function(value) 
{
    this.gstPercent = value;
}

OrderProductVM.prototype.setHasBarcodeId = function(value) 
{
    this.hasBarcodeId = value;
}

/* Getters */


OrderProductVM.prototype.getProductId = function() 
{
    return this.productId;
}

OrderProductVM.prototype.getProductName = function() 
{
    return this.productName;
}

OrderProductVM.prototype.getHire = function() 
{
    return this.hire ;
}

OrderProductVM.prototype.getPrice = function() 
{
    return this.price ;
}

OrderProductVM.prototype.getCalcGst = function() 
{
    return this.calcGst ;
}

OrderProductVM.prototype.getGstPercent = function() 
{
    return this.gstPercent ;
}

OrderProductVM.prototype.getHasBarcodeId = function() 
{
    return this.hasBarcodeId ;
}

/* Not used */
OrderProductVM.prototype.getAllProducts = function(productPriceGroupId, callback)
{
	// Do an AJAX call
	if (window.jQuery)
	{
		// Do an AJAX call
		$.ajax({            
                type: "get", 
                url: "'.base_url().'orders/getAllProducts/" + productPriceGroupId + "/",
                success: function(data)
                {
                	if (callback != null)
                	{
                    	callback(data);
                	}
                }        
            }); 
	}
}