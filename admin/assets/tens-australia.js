'use strict';

function DateTimeFormatter () {
		var self = this;
    self.backendDateFormat = 'YYYY-MM-DD HH:mm:ss A';
    self.frontendDateFormat = 'DD/MM/YYYY';
    self.frontendTimeFormat = 'hh:mm:ss A'; 
    self.getBackEndDate = function(frontEndDate) {
			return moment(frontEndDate, this.frontendDateFormat).format(this.backendDateFormat);
    };

    self.getFrontEndDate = function(backEndDate) { 
			return moment(backEndDate, this.backendDateFormat).format(this.frontendDateFormat);
    };

    self.getFrontEndTime = function(backEndDate) { 
			return moment(backEndDate, this.backendDateFormat).format(this.frontendTimeFormat);
    };

    self.toFrontEndDate = function(mysqlDate) {
    	if (mysqlDate && mysqlDate !== '') {
				return moment(mysqlDate, this.backendDateFormat).format(this.frontendDateFormat);
			} else {
				return '';
			}
    };

    self.toFrontEndNow = function() {
			return moment().format(self.frontendDateFormat);
    };
}

function formatDate(dateStr)
{ 
	if (dateStr != null && dateStr.length > 2)
	{
		var date = new Date(dateStr);
		if (typeof date.getDate === 'function')
		{
			var day = date.getDate();
			if (day < 10 && day > -10  || day.length == 1)
			{
				day = '0' + day;
			}
			var month = date.getMonth() + 1;
			if (month < 10 && month > -10 || month.length == 1)
			{
				month = '0' + month;
			}
			var year = date.getFullYear(); 
			return day + '-' + month + '-' + year;
		}
		else 
		{
			return '';
		}
	}
	else 
	{
		return null;
	}
}

function formatTime(timeStr)
{ 
	var mysqlFormat = 'YYYY-MM-DD HH:mm:ss A';
	return moment(timeStr, mysqlFormat).format('hh:mm:ss A');
}


function formatMySQLDatetime(str, defaultToNow, formatString)
{
	if (str === null || str === '0000-00-00 00:00:00' || str === '')
	{
		if (defaultToNow)
		{
			return moment().format('DD-MM-YYYY HH-mm-ss');
		}
		else 
		{
			return '';
		}
	}
	else 
	{ 
		return moment(str, 'YYYY-MM-DD HH:mm:ss').format(formatString);
	}
}

function toMySQLDate(str) {

}


function fromMySQLDate(str) {

}

function formatMySQLDate(str, defaultToNow, formatString)
{
	if (str === null || str === '0000-00-00 00:00:00' || str === '')
	{
		if (defaultToNow)
		{
			return moment().format('DD-MM-YYYY');
		}
		else 
		{
			return '';
		}
	}
	else 
	{
		// Typical MySQL DateTime format : 2015-03-15 00:00:00
		return moment(str, 'YYYY-MM-DD HH:mm:ss').format(formatString);
	}
}