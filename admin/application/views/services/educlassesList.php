<div class="col-lg-12">
                <!--***********************************************************************-->
                <!-- contents for every page starts here -->
                <div class="row">
                    <div class="col-lg-12">&nbsp;</div>
                </div>                
				<div class="row">
                    <div class="col-lg-12">
						
												
					</div>
					<div class="col-md-12" >
						<div class="panel panel-primary">
							<div class="panel-heading">
								<div class="panel-title">
										Available EduClass List 
								</div>
							</div>
												
							<div class="panel-body">
								<table class="table table-bordered" id="main-datatable">
										<thead>
											<th>Class Date</th>
											<th>Start Time</th>
											<th>Duration</th>
											<th>Patients Invited</th>
											<th>Location</th>
											<th>Address</th>
											<th>SubUrb</th>
											<th>Instructions</th>
											<th>Educator</th>
										</thead>
										<tbody>
											<?php foreach ($eduClassesResult as $result){?>
												<tr> 
													
													<td ><?php $cDate = $result->ClassDate; $cD = new DateTime($cDate);echo $cD->format('Y-m-d'); ?></td>
													<td ><?php $sT = $result->StartTime; $startTime = new DateTime($sT); echo $startTime->format('H:i');?></td>
													<td ><?php echo $result->Duration;?></td>
													<td><?php echo $result->PatientsInvited;?></td>
													<td><?php echo  $result->locName; ?></td>
													<td><?php echo $result->LocAddress;?></td>
													<td><?php echo $result->LocSuburb;?></td>
													<td><?php echo $result->Instructions;?></td>
													<td><?php echo $result->Educator;?></td>
													
												</tr>
												<?php }?>
										</tbody>
								</table>
								
							</div>
						</div>
					</div>
                </div>
            <!-- contents for every page ends here -->
            <!--***********************************************************************-->
</div>      
            
 

<script type="text/javascript">

jQuery("#main-datatable").dataTable();
</script>