<div class="col-lg-12">
                <!--***********************************************************************-->
                <!-- contents for every page starts here -->
                <div class="row">
                    <div class="col-lg-12">&nbsp;</div>
                </div>                
				<div class="row">
                    <div class="col-lg-12">
						
												
					</div>
					<div class="col-lg-12" >
						<div class="panel panel-primary"  style="width:160%">
							<div class="panel-heading">
								<div class="panel-title">
										Delivery Suite Support Program Equipment List
								</div>
							</div>
												
							<div class="panel-body">
								<table class="table table-bordered" id="main-datatable">
										<thead>
											<th>Order ID</th>
											<th>Customer</th>
											<th>Product Name</th>
											<th>Temp BarCode</th>
											<th>Invoice Date</th>
											<th>Days</th>
											<th>Return Expected Date</th>
										</thead>
										<tbody>
										
												<?php foreach ($suiteProductResult as $result){?>
													<tr>
													<td ><?php echo $result->OrderID;?></td>
													<td ><?php echo $result->Customer;?></td>
													<td ><?php echo $result->ProductName;?></td>
													<td><?php echo $result->TempBarcode;?></td>
													<td ><?php $InvoiceDate = $result->InvoiceDate; $Idate = new DateTime($InvoiceDate);echo $Idate->format('Y-m-d'); ?>"  
													</td>
													<td><?php echo $result->Days;?></td>
													<td>
														<?php echo $result->ReturnExpectedDate;?> 
													</td>
													</tr>
												<?php }?>
												
												
										</tbody>
								</table>
								
							</div>
						</div>
					</div>
                </div>
            <!-- contents for every page ends here -->
            <!--***********************************************************************-->
</div>      

<script type="text/javascript">

jQuery("#main-datatable").dataTable();
</script>

