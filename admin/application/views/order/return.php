<div class="col-md-12">  
    <h2>Create an order by entering the data below:</h2>
    <div class="container"> 
        <h2>Customer</h2>
        <select id="customers-select"> 
        </select>
        <h2>Hospital:</h2>
        <select id="hospitals-select">
        </select>
        <br/>
        <br/>
        <br/>
         <button type="button" class="btn btn-primary" area-label="Left Align" onclick="createOrder()">
            Create an order (Go to view orders)
         </button> 
    </div>  
</div>     


              
<script type="text/javascript">

function createOrder()
{
    var selectedCustomerId = jQuery("#customers-select").val();
    var selectedHospitalId = jQuery("#hospitals-select").val();
    var selectedCustomerHtml = jQuery("#customers-select").find(":selected").text();;
    var selectedHospitalHtml = jQuery("#hospitals-select").find(":selected").text();; 
    jQuery.ajax({
        url: "<?= base_url().'orders/vc/createOrder/'?>" + selectedCustomerId + "/" + selectedHospitalId,
        type: "post", // To protect sensitive data 
        success:function(response){
            // Handle the response object 
            console.log("Response:");
            console.log(response);
            var responseOrder = JSON.parse(response);
            if (responseOrder != null)
            {
            console.log(responseOrder);
                    window.location = "<?= base_url().'orders/details/'?>" + responseOrder["OrderID"];
            } 
        }
    });
} 

jQuery.ajax({
    url: "<?= base_url().'ajax/allcustomers'?>",
    type: "post", // To protect sensitive data 
    success:function(response){
        // Handle the response object 
        for (var customerId in response)
        { 
            if (response.hasOwnProperty(customerId))
            {
                var customer = response[customerId]; 
                var fullName =  customer["BusinessName"] + " " + customer["Firstname"]  + " " + customer["LastName"] ;
                jQuery("#customers-select").append("<option value=\"" + customer["CustomerID"] + "\">" + fullName + "</option>")
            }
        }
    }
});

jQuery.ajax({
    url: "<?= base_url().'ajax/allhospitals'?>",
    type: "post", // To protect sensitive data 
    success:function(response){
        // Handle the response object
        for (var hospitalId in response)
        { 
            if (response.hasOwnProperty(hospitalId))
            {
                var hospital = response[hospitalId]; 
                console.log(hospital); 
                jQuery("#hospitals-select").append("<option value=\"" + hospital["HospitalID"] + "\">" + hospital["HospitalName"] + "</option>")
            }
        }
    }
});
</script> 