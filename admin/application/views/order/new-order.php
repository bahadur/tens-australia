<link rel="stylesheet" href="<?= base_url().'assets/neon/js/select2/select2.css'?>">   
<script src="<?= base_url().'assets/neon/js/select2/select2.min.js'?>"></script> 
<script src="<?= base_url().'assets/neon/js/jquery.validate.min.js'?>"></script> 

<div class="panel panel-primary">
    <div class="panel-heading">
        <div class="panel-title">Create an order by entering the data below:</div>
    </div>
    <div class="panel-body">
        <form class="form" id="new-order-form" action="<?php echo base_url().'orders/vc/createOrder/' ?>" method="POST" onsubmit=""> 
        <div class="row">
            <div class="row col-md-10 col-md-offset-1"> 
            <h4>Customer:</h4>
                <input type="hidden" name="customerId" class="required  col-md-9" style="left:-15px" id="customerId"/>
                <button type="button" class="btn btn-primary  col-md-3" id="new-customer">New customer</button>
            </div>
            </div>
            <div class="row">
                <div class="col-md-5 col-md-offset-1">
                    <div class="row col-md-12">  
                    <h4>Hospital:</h4>
                        <select name="hospitalId" class="select2 required" id="hospitalId" data-placeholder="Please select">
                            <option></option> 
                            <?php foreach ($hospitals as $hospital) {?>
                                <option value="<?php echo $hospital->HospitalID ?>"><?php 
                                    echo $hospital->HospitalName;
                                ?></option>
                            <?php }?>
                        </select>
                    </div>

                    <div class="row col-md-12">  
                        <h4>Educator:</h4>
                        <select name="educatorId" class="select2 required" id="educatorId"
                        data-placeholder="Please select">
                            <option></option> 
                            <?php foreach ($educators as $educator) {?>
                                <option value="<?php echo $educator->EducatorID ?>"><?php 
                                    echo $educator->Firstname.' '.$educator->Lastname.' '.$educator->BusinessName;
                                ?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>

                <div class="col-md-5">
                      

                    <div class="row col-md-12">  
                        <h4>Shipping Method:</h4>
                        <select name="shipmethodId" class="select2" id="shipmethodId" data-validate="required"
                          data-placeholder="Please select a shipping method">
                            
                            <?php foreach ($shipmethods as $shipmethod) {
                                $selected = ($shipmethod->ShipMethodID == 1)?"selected":"";
                                ?>
                                <option value="<?php echo $shipmethod->ShipMethodID ?>" <?php echo $selected ?>><?php 
                                    echo $shipmethod->ShipMethod;
                                ?></option>
                            <?php }?>
                        </select>
                    </div> 

                    <div class="row col-md-12">  
                        <h4>Pharmacy:</h4>
                        <input type="text" data-placeholder="Please enter pharmacy..." class="form-control" id="pharmacyName" name="pharmacyName"></input>
                    </div> 

                </div>
            </div> 




            <div class="col-md-10 col-md-offset-1">  
             <button type="submit" class="btn btn-success col-md-offset-1 pull-right col-md-4">
                Create an order
             </button> 
            </div>  
        </form>
    </div>
</div> 
<script type="text/javascript"> 
  
jQuery("#new-order-form").validate({
    ignore: 'input[type=hidden]',
    rules:
    {
        customerId: { required: true }, 
        hospitalId: {required: true},
        educatorId: {required: true},
        pricegroupId: {required: true},
        shipmethodId: {required: true}
    },

    errorPlacement: function (error, element) { 
        var elem = jQuery(element);
        error.addClass('errorLabel');
        error.insertAfter(element);
    },
    
    success: function (label, element) {  
        var elem = jQuery(element);
        elem.siblings('label.error').remove();
    },

    submitHandler: function(form) {
        var customerData = (jQuery('#customerId').select2('data')); 
        if (customerData.CustomerProblemFlagID * 1 == 2) {
            alert('Warning - This customer is under investigation!');
            return true;
        } else if (customerData.CustomerProblemFlagID * 1 == 3) {
            alert('This customer cannot have new orders placed for him!');
            return false;
        } else if (customerData.CustomerProblemFlagID * 1 == 1) { 
            return true;
        } 
    }
});

/*
Wait until we test HUGe amount of data 
http://stackoverflow.com/questions/16862623/how-can-i-modify-this-select2-javascript-to-enable-paging
 */
jQuery('#customerId').select2({ 
    "ajax": {
        "url": "<?php echo base_url().'customers/formapi/getCustomersSelect'?>",
        "type": 'POST',  
        "delay": 250,
        "results": function(data, page) { 
            return {
                results: data
            };
        },   
        data: function (term, page) {
            return {
                "q": term,
                "page" : page
            };
        },
        "cache" : true
    },
    "placeholder": 'Select a customer'
});

function validateForm() 
{ 
    return false;
}

jQuery("#new-customer").click(function(){
    document.location = '<?php echo base_url()?>customers/vc/newCustomer' 
});

</script>






