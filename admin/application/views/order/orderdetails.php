<script type="text/javascript">
window.onpageshow = function(evt) {
    // If persisted then it is in the page cache, force a reload of the page.
    if (evt.persisted) {
        document.body.style.display = "none";
        location.reload();
    }
};
</script>
<script type="text/javascript" src="<?= base_url().'assets/js/tensApi/lazyLoadingButton.js'?>"></script> 
<script type="text/javascript" src="<?= base_url().'assets/jeditable.checkbox.js'?>"></script> 
 
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-title">Order Details</div>
            </div>
            <div class="panel-body"> 
            <?php 
                    $this->load->view('order/ui-elements/general-details');
            ?> 
                <div class="row">
                   <div class="col-md-12">  

                       <br/>
                       <br/>
                       <br/>
                       <div class="col-md-12">
                           <ul class="nav nav-tabs bordered" id="tabs">
                            <li class="active"><a href="#address" data-toggle="tab">Address, Phone, Email</a></li>
                            <li><a href="#products" data-toggle="tab">Products</a></li>
                            <li><a href="#payments" data-toggle="tab">Payment</a></li>
                            <li><a href="#stockmovehistory" data-toggle="tab">Stock Move History</a></li>
                            <li><a href="#transactionhistory" data-toggle="tab">Transaction History</a></li>
                            <li><a href="#smshistory" data-toggle="tab">SMS History</a></li>
                            <li><a href="#notes" data-toggle="tab">Notes</a></li>

                        </ul>
                        </div>
                        <div class="tab-content" id="my-tab-content">
                            <div class="tab-pane active" id="address">

                                <?php 
                                $this->load->view('order/ui-elements/contact-details-tab');
                                ?>

                            </div>

                            <div class="tab-pane " id="payments">

                                <?php 
                                $date = date("Y-m-d");
                                $data['date'] = $date;
                                $this->load->view('order/ui-elements/payments-tab',$data);
                                ?>
                            </div>

                            <div class="tab-pane " id="products">
                                <?php 
                                $this->load->view('order/ui-elements/products-tab');
                                ?>

                            </div>

                            <div class="tab-pane " id="stockmovehistory"> 
                                <?php $this->load->view('order/ui-elements/stockmove-tab'); ?>
                            </div>

                            <div class="tab-pane " id="transactionhistory">  
                                <?php $this->load->view('order/ui-elements/transactions-tab'); ?>
                            </div>

                            <div class="tab-pane" id="smshistory">  
                                <div class="col-lg-12">  
                                    <table class="table table-striped tableStyle" id="sms-table"> 
                                        <thead>
                                            <tr>
                                                <th>Sms Number</th>
                                                <th>Sent Date Time</th> 
                                            </tr>
                                        </thead>   
                                        <tbody>
                                            <?php 
                                            foreach ($smshistory as $sms)
                                            {    
                                                echo '<tr>';
                                                echo '<td>'.$sms->ReminderID.'</td>'; 
                                                echo '<td>'.$sms->SentDateTime.'</td>';  
                                                echo '</tr>';
                                            }
                                            ?>
                                        </tbody>
                                    </table> 
                                </div>
                            </div>

                            <div class="tab-pane" id="notes"> 
                                <?php  
                                $this->load->view('order/ui-elements/notes-tab');
                                ?>
                            </div>
                        </div>
                    </div> 
                </div>   
            </div>
        </div> 
    </div>
</div>
 

<script type="text/javascript">
    jQuery("#sms-table").dataTable({
        bPaginate: true,
        bFilter: false
    });
</script>

<script type="text/javascript" src="<?= base_url().'assets/bootstrap/js/bootstrap-datepicker.js'?>"></script> 
<script type="text/javascript">


var df = new DateTimeFormatter();
var transVm = new TransactionsVM();
transVm.bind();
var paymentVm = new PaymentTabVM(transVm);
paymentVm.bind();  
var productsTabVM = new ProductsTabVM();
productsTabVM.bind(paymentVm);
var notesVm = new NotesVM();
notesVm.bind();
var newNoteVm = new NewNoteVM();
newNoteVm.bind(notesVm);


jQuery(".dropdown-menu li a").click(function(){
  var selText = jQuery(this).text();
  jQuery(this).parents('.btn-group').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
});

jQuery("#btnSearch").click(function(){
    alert(jQuery('.btn-select').text()+", "+jQuery('.btn-select2').text());
});

jQuery("#duedate-input").val( 
     df.toFrontEndDate('<?php echo $orderObject->DueDate ?>')
    );
jQuery("#applicationdate").val( 
     df.toFrontEndDate('<?php echo $orderObject->OrderDate ?>')
    );
 
jQuery("#createddate").val( 
     df.toFrontEndDate('<?php echo $orderObject->CreatedDate ?>')
    );
jQuery("#requireddate").val(
    df.toFrontEndDate('<?php echo $orderObject->RequiredDate ?>')
    );


jQuery("#edit-customer").click(function(){
    window.location ='<?= base_url()."customers/vc/customerDetails/".$customerSelected->CustomerID."/".$orderObject->OrderID; ?>';
});

jQuery("input[name=CustomerField]").on("focusout",function(){
    jQuery(this).attr({disabled:"disabled"});
});
 

</script> 






