 
                <!--***********************************************************************-->
                <!-- contents for every page starts here -->
                <div class="row">
                    <div class="col-md-12">&nbsp;</div>
                </div>                
                 <div class="col-md-12">
                    <?php 
                            $articleId = null;
                            $barcode = null;
                            if (isset($_GET['articleId']))
                            { 
                                $articleId = $_GET['articleId'];
                                echo "<h2>Orders with AusPost ArticleID : ".$articleId."</h2>";
                            }
                            else if (isset($_GET['barcode']))
                            {
                                $barcode = $_GET['barcode'];
                                echo "<h2>Orders with Barcode : ".$barcode."</h2>";
                            }
                    ?>
                    <table class="table table-bordered datatable" id="orders-table">
                        <thead>
                            <tr>
                                <th>Order ID</th>
                                <th>Hospital Name</th>
                                <th>Product Name</th>
                                <th>Quantity</th>
                                <th>Barcode</th>
                                <th>Unit Price</th>
                                <th>GST</th>
                                <th>Move Date</th>
                                <th>Customer Name</th>
                            </tr>
                        </thead>

                        <?php 
                            $baseUrl = $this->config->config['base_url']; 
                            if (!is_null($orders))
                            {
                                foreach ($orders as $row)
                                {    
                                      echo "<tr>";
                                      echo "<td><a href='".$baseUrl."/orders/details/".$row->OrderID."'>".$row->OrderID."</a></td>";
                                      echo "<td>".$row->HospitalName."</td>";
                                      echo "<td>".$row->ProductName."</td>";
                                      echo "<td>".$row->Quantity."</td>";
                                      echo "<td>".$row->Barcode."</td>";
                                      echo "<td>$".number_format($row->UnitPrice, 2, '.','')."</td>";
                                      echo "<td>".$row->GST."</td>";
                                      echo "<td>".$row->MoveDate."</td>";
                                      echo "<td>".$row->Customer_Name."</td>";
                                      echo "</tr>";
                                }
                            }
                        ?>
                    </table>
                 </div> 
                
        <script type="text/javascript">
            jQuery("#orders-table").dataTable();
        </script>
<!-- add the below lines on every content pages -->            
        </div>
    </div>
        <div class="col-lg-1"></div>
    </div><!-- end of row in navbar view -->
</div><!-- end of container in navbar view -->






