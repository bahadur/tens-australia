<link rel="stylesheet" href="<?= base_url().'assets/js/select2/select2.css'?>">
<link rel="stylesheet" href="<?= base_url().'assets/js/select2/select2-bootstrap.css'?>">
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" >
            <div class="panel-heading">
                <div class="panel-title">
                    Find Customer
                </div>
            </div>
                <?php 


                        $qString = "SELECT `customer`.`CustomerID`, 
                                CONCAT(`BusinessName`, \",\", `Firstname`,\",\",  `MobilePhone`,\",\",  `BillingAddress`,\",\",  `BillingSuburb`,\",\",  `BillingPostcode`) AS CustomerName, 
                                `customer`.`BillingAddress`, 
                                `customer`.`BillingSuburb`, 
                                `customer`.`BillingState`, 
                                `customer`.`BillingPostcode`, 
                                `customer`.`BillingCountry`, 
                                `customer`.`ShippingSameAsBilling`, 
                                `customer`.`ShippingAddress`, 
                                `customer`.`ShippingSuburb`, 
                                `customer`.`ShippingState`, 
                                `customer`.`ShippingPostcode`, 
                                `customer`.`ShippingCountry`, 
                                `customer`.`HomePhone`, 
                                `customer`.`MobilePhone`, 
                                `customer`.`Fax`, 
                                `customer`.`Email`, 
                                `customer`.`PriceGroupID`,
                                LastName, FirstName, MobilePhone, BusinessName, BillingAddress, BillingSuburb, BillingPostcode
                                FROM (`customer`) ORDER BY CustomerName";
                        $query = $this->db->query($qString); 
                ?>
            <div class="panel-body">               
                <div class="form-group">
                    
                    <select class="form-control select2" id="customer_list" autocomplete="off" data-allow-clear="true" data-placeholder="Select Customer...">
                        <option></option>
                                <optgroup label="Customers">   
                        <?php

                                        
                        foreach ($query->result() as $row) { ?>
                                            

                        <option value="<?php echo $row->CustomerID ?>"><?php 
                            if ($row->BusinessName)
                            {
                               echo $row->BusinessName.','.$row->FirstName.','.$row->MobilePhone.','.$row->BillingAddress.','.$row->BillingSuburb.','.$row->BillingPostcode.',';
                            }
                            else 
                            {
                               echo $row->LastName.','.$row->FirstName.','.$row->MobilePhone.','.$row->BillingAddress.','.$row->BillingSuburb.','.$row->BillingPostcode.',';
                            }
                            ?>
                        </option>
                        <?php } ?>
                        </optgroup>
                    </select>
                </div>
                <div class="form-group">
                    <button type="button" class="btn btn-primary pull-left" id="create_new_customer">New Customer</button>
                    <button type="button" class="btn btn-success pull-right" id="go_to_customer">Go to Customer</button>
                </div>
            </div>
        </div> 
                    
    </div>
</div>      
            
<script src="<?= base_url().'assets/js/select2/select2.js'?>"></script>
<script type="text/javascript">
    jQuery("#create_new_customer").on('click',function() 
        {
            window.location.assign('<?php echo base_url()?>customers/vc/newCustomer');
        }
    );
    jQuery(document).ready(function (a) {
        
        a("#go_to_customer").click(function(){
            if(a("#customer_list").val() == "")
                alert("Please select customer.");
            else
                document.location = "<?php echo base_url()?>customers/vc/customerDetails/"+ a("#customer_list").val();
        });

});
</script>