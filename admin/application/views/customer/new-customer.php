<link rel="stylesheet" href="<?= base_url().'assets/js/select2/select2.css'?>">
<link rel="stylesheet" href="<?= base_url().'assets/js/select2/select2-bootstrap.css'?>">
<form action="<?php echo base_url()?>customers/vc/newCustomer" method="post" id="new-customer-form" accept-charset="utf-8" class="form-horizontal" target="_top">
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" >
            <div class="panel-heading">
                <div class="panel-title">
                    Basic Details
                </div>
            </div>
                
            <div class="panel-body">
            <div class="row">
                <div class="col-md-5 col-md-offset-1">
                    <div class="col-sm-12">
                        <h4>Customer Type: </h4>
                        <select name="CustomerTypeID" id="CustomerTypeID" class="form-control select2" data-validate="required" data-message-required="This field is required." data-placeholder="Please select..." >
                            <option></option>
                            <optgroup label="Customer Types">
                            <?php   
                                foreach ($customerData as $row_customerType) {
                                    $selected = ($row_customerType->CustomerTypeID == 1) ? " selected='selected'" : "";
                                    echo "<option value='$row_customerType->CustomerTypeID' $selected >$row_customerType->CustomerType</option>";
                                } 
                            ?>
                            </optgroup>
                        </select>
                    </div>


                    <div class="col-md-12">
                        <h4>First Name: </h4> 
                        <input type="text" name="Firstname" id="Firstname"  class="form-control" data-validate="required" data-message-required="This field is required." data-placeholder="Please enter First Name...">  
                    </div>

                    <div class="col-md-12">
                        <h4>Last Name: </h4>  
                        <input type="text" name="LastName" id="LastName" class="form-control" data-validate="required" data-message-required="This field is required." data-placeholder="Please enter Last Name...">    
                    </div>

                    <div class="col-md-12">
                        <h4>Special Instructions: </h4> 
                        <input type="text" name="Comments" id="Comments" class="form-control" data-placeholder="Comments..."> 
                    </div>    
                </div>

                <div class="col-md-5"> 
                    <div class="col-md-12">
                        <h4>Business Name: </h4> 
                        <input type="text" name="BusinessName" id="BusinessName" class="form-control" data-message-required="This field is required." data-placeholder="Please enter Business Name..." > 
                    </div>
                     
                    <div class="col-md-12">
                        <h4>Customer Problem Flag: </h4> 
                        <select name="CustomerProblemFlagID" id="CustomerProblemFlagID" class="form-control select2" data-validate="required" data-message-required="This field is required." data-placeholder="Please select...">
                            <option></option>
                            <optgroup label="Customer Types">
                                <?php  
                                    foreach ($customerproblemflags as  $row_customerFlag) {
                                        $selected = ($row_customerFlag->CustomerProblemFlagID == 1) ? " selected='selected'" : "";
                                        echo "<option value='$row_customerFlag->CustomerProblemFlagID' $selected > $row_customerFlag->Flag</option>";
                                    } 
                                ?>
                            </optgroup>
                        </select> 
                    </div>   

                    <div class="col-md-12">
                        <h4>Price Group: </h4> 
                        <select name="PriceGroupID" id="PriceGroupID"  class="form-control select2"> 
                            <optgroup label="Price Groups"> 
                                <?php  
                                foreach ($priceGroups as $row) {
                                    $selected = ($row->PriceGroupID == 1)?'selected':'';
                                    echo "<option value='".$row->PriceGroupID."' $selected >".$row->PriceGroup."</option>";
                                }
                                ?>
                            </optgroup>
                        </select>
                    </div>  
                </div>
            </div>  
            <br/>
            <br/>
                <div class="row">
                    <div class="col-md-7">
                        <button type="submit" class="btn btn-primary col-md-5 pull-right" id="add_customer">Add Customer</button>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</div> 
</form> 

<script src="<?= base_url().'assets/js/select2/select2.js'?>"></script>

<script type="text/javascript">
jQuery(document).ready(function () {

    jQuery("#new-customer-form").validate({
        ignore: 'input[type=hidden]',
        rules:{
            CustomerTypeID: { required: true }, 
            Firstname: { required: true }, 
            LastName: { required: true },  
            CustomerProblemFlagID: { required: true }
        },
 
        errorPlacement: function (error, element) { 
            var elem = jQuery(element);
            error.addClass('errorLabel');
            error.insertAfter(element);
        },
                        
        success: function (label, element) {  
            var elem = jQuery(element);
            elem.siblings('label.error').remove();
        },

        submitHandler: function(form) {
            return true;
        }
    });
    
    function validateForm(){ 
        return false;
    }

 });
</script>