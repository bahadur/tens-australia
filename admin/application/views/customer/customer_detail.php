<link rel="stylesheet" href="<?= base_url().'assets/js/select2/select2.css'?>">
<link rel="stylesheet" href="<?= base_url().'assets/js/select2/select2-bootstrap.css'?>">
<script src="<?php echo base_url().'assets/js/tensApi/formArrayToJson.js'; ?>"></script>
<script src="<?php echo base_url().'assets/js/tensApi/lazyLoadingButton.js'; ?>"></script>
<script src="<?php echo base_url().'assets/js/tensApi/tensDatesFormatter.js'; ?>"></script>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" >
            <div class="panel-heading">
                <div class="panel-title">
                    Basic Details
                </div>
            </div> 
            <div class="panel-body"> 
             <?php 
                $this->load->view('customer/tabs/basic-details-tab');
             ?>  
                <div class="row">
                    <div class="col-md-12"> 
                        <ul class="nav nav-tabs" id="tabs">
                            <li class="active"><a href="#address" data-toggle="tab">Address, Phone, Email &amp; Web</a></li>
                            <li><a href="#partner" data-toggle="tab">Partner</a></li>
                            <li><a href="#abn" data-toggle="tab">Active, ABN, Insurance, &amp; Pricing</a></li>
                            <li><a href="#order" data-toggle="tab">Order</a></li>
                            <li><a href="#note" data-toggle="tab">Note</a></li>
                            <li><a href="#classes" data-toggle="tab">Classes</a></li>
                        </ul>
                        <div class="tab-content" id="my-tab-content">
                            <div class="tab-pane active" id="address">
                             <?php 
                                $this->load->view('customer/tabs/address-contacts-tab');
                             ?>
                            </div> 
                            <div class="tab-pane " id="partner">
                             <?php 
                                $this->load->view('customer/tabs/partner-tab');
                             ?>
                            </div> 
                            <div class="tab-pane " id="abn">
                             <?php 
                                $this->load->view('customer/tabs/other-options-tab');
                             ?>
                            </div> 
                            <div class="tab-pane " id="order">
                             <?php 
                                $this->load->view('customer/tabs/orders-tab');
                             ?>
                            </div> 
                             <div class="tab-pane " id="note">
                             <?php 
                                $this->load->view('customer/tabs/notes-tab');
                             ?>
                            </div> 
                             <div class="tab-pane " id="classes">
                             <?php 
                                $this->load->view('customer/tabs/classes-tab');
                             ?>
                            </div> 
                        </div>  
                    </div>
                </div>  
            </div>
        </div>
    </div>
</div> 
<!-- Modal Add class --> 
<div class="modal fade custom-width" id="modal-class-add">
    <div class="modal-dialog" style="width: 60%;">
        <div class="modal-content">
          
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add Product</h4>
            </div>
          
            <div class="modal-body">
                <div class="col-md-12">
                    <form class="form-horizontal form-groups-bordered validate" id="form-add-class" target="_top">
                    
                        <div class="row">
                            <label>Select a class</label>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input class="form-control" id="EduClassID" name="EduClassID" type="hidden"></input> 
                                </div>
                            </div> 

                            <div class="form-actions">
                                <div class="col-sm-offset-3 col-sm-5">
                                    <input type="hidden" name="CutomerID" id="CutomerID" value="<?php echo $customer->CustomerID ?>"  class="form-control">
                                    <button type="submit" class="btn btn-blue" id="insert-product" >Add Class</button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
          
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>


<div class="modal fade custom-width" id="modal-class-cancel">
    <div class="modal-dialog" style="width: 60%;">
        <div class="modal-content">
          
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Cancel Class</h4>
            </div>
            <input type="hidden" id="eduCustomerClassIdToCancel"/>
            <div class="modal-body">
                <p>Are you sure you want to cancel the class booking?</p>
            </div>
          
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" id="cancel-class-btn">Yes</button>
                <button type="button" class="btn btn-info" data-dismiss="modal">No</button>
            </div>

        </div>
    </div>
</div> 
<form style="display:none" method="post" id="openEmailForm" action="<?= base_url().'emailer/remindCustomerClass';?>">
    <input type="hidden" name="emailCustomerId" id="emailCustomerId"></input>
    <input type="hidden" name="emailTemplateId" value="3"></input>
    <input type="hidden" name="emailEduCustomerClassId" id="emailEduCustomerClassId"></input>
</form> 
<script src="<?= base_url().'assets/js/select2/select2.js'?>"></script>
<script type="text/javascript">


var addressVm = new AddressVM();
addressVm.bind();
var ordersVm = new OrdersVM();
ordersVm.bind();
var notesVm = new NotesVM();
notesVm.bind();
var classesVm = new ClassesVM();
classesVm.bind(); 

var newNoteVm = new NewNoteVM();
newNoteVm.bind(notesVm);
/*
jQuery(document).ready(function ($) {
    var cancelClassButton = jQuery('#cancel-class-btn');
    var note_table = jQuery("#note_table").DataTable({
        processing: true,
        serverSide: true,
        sDom:'tp',
        ajax: {
            url: "<?php echo base_url()?>customers/formapi/notes",
            type: "POST",
            data: function(d) { 
                d.customerid = <?php echo $customer->CustomerID ?> ;
            }
        }
    });


    var classes_table = jQuery("#classes_table").dataTable({
        processing: true,
        serverSide: true,
        sDom:'tp',
        ajax: {
            url: "<?php echo base_url()?>customers/formapi/classes",
            type: "POST",
            cache: false,
            data: function(d) { 
                d.customerid = <?php echo $customer->CustomerID ?>;
            }
        },
        aoColumns: [
            {
                data: 'eduClassCustomerId', visible: false
            },
            {
                data: 'eduClassID', visible: false
            },
            {
                data: 'customerID', visible: false
            },
            { 
                data: 'classDate',
                mRender: function(data, type, full) {
                    var dtf = new DateTimeFormatter();
                    return dtf.getFrontEndDate(data);
                }
            },
            { 
                data: 'startTime',
                mRender: function(data, type, full) {
                    var dtf = new DateTimeFormatter();
                    return dtf.getFrontEndTime(data);
                }
            },
            { 
                data: 'locName' 
            },
            { 
                data: 'locSuburb' 
            },
            { 
                data: 'businessName' 
            },
            { 
                data: 'numberAttending'
            },
            {
                data: 'cancelled',
                mRender: function(data, type, full) { 
                    if (data * 1 == 0) 
                    {
                        return "No";
                    }
                    else 
                    {
                        return "Yes";
                    }
                }
            },
            {
                data:'nullcell',
                mRender: function(data, type, full) { 
                    if (full['cancelled'] == "1")
                    {
                        return "<button class='btn btn-success email-btn'>Email</button><button class='btn btn-danger cancel-btn ' disabled>Cancel</button>"; 
                    }
                    else 
                    {
                        return "<button class='btn btn-success email-btn'>Email</button><button class='btn btn-danger cancel-btn '>Cancel</button>"; 
                    }
                },
                fnCreatedCell: function( nTd, sData, oData, iRow, iCol ) { 
                    var emailBtn = jQuery(nTd).find('.email-btn');
                    var cancelBtn = jQuery(nTd).find('.cancel-btn'); 
                    emailBtn.on('click', function(e) { 
                        jQuery("#emailEduCustomerClassId").val(oData['eduClassCustomerId']);
                        jQuery("#emailCustomerId").val(oData['customerID']);  
                        jQuery("#openEmailForm").submit();
                    });
                    cancelBtn.on('click', function(e) { 
                        jQuery("#modal-class-cancel").modal('show'); 
                        jQuery("#eduCustomerClassIdToCancel").val(oData['eduClassCustomerId']);  
                    });
                }
            }
        ]
    });
    var classesTableAPI = jQuery("#classes_table").DataTable();

    cancelClassButton.on('click', function(e) {
        cancelClassButton.attr("disabled", true);
        cancelClassButton.html("Cancelling...");
        var id = jQuery("#eduCustomerClassIdToCancel").val()
        jQuery.ajax({
            url: "<?php echo base_url()?>ajaxClasses/cancelClass",
            type: "POST",
            data: "eduCustomerClassIdToCancel=" + id,
            success: function(data)
            {  
                cancelClassButton.html("Cancel");
                cancelClassButton.attr("disabled", false);
                if (data == "1")
                {
                    var data = classes_table.fnGetData();
                    for (var col = 0 ; col < data.length; col++)
                    {
                        if (data[col]['eduClassCustomerId'] == id)
                        { 
                            console.log(classes_table);
                            console.log(classesTableAPI);
                            classes_table.fnUpdate(1, col, 0);  
                            classes_table.fnUpdate(0, col, 10);   
                        }
                    }
                }
                else 
                {
                    alert("Could not cancel the class");
                }
                jQuery('#modal-class-cancel').modal('toggle');
            },
            error :function(jqXHR, textStatus, errorThrown ) {
                cancelClassButton.html("Cancel");
                cancelClassButton.attr("disabled", false);
                alert("Could not cancel the class");
                jQuery('#modal-class-cancel').modal('toggle');
            }  
        })
    });

    var orders_table = jQuery("#orders_table").DataTable({
        processing: true,
        serverSide: true,
        sDom:'tp',
        ajax: {
            url: "<?php echo base_url()?>ajax/orders",
            type: "POST",
            data: function(d) { 
                d.customerid = <?php echo $customer->CustomerID ?> ;
            }
        },
        columnDefs: [ 
            {
                targets: -1,
                data: null,
                defaultContent: "<button class='btn btn-sm btn-primary'>Detail</button>"
            } 
        ]
    });
    
    $('#orders_table').on( 'click', 'button', function () {
        var data = orders_table .row( $(this).parents('tr') ).data();
        document.location = '<?php echo base_url()?>orders/details/' + data[0]
    });
        
       
    jQuery("#chk_same").change(
        function() {
            if (jQuery(this).is(":checked")) {
                jQuery("#ShippingAddress").attr("disabled","disabled");
                jQuery("#ShippingSuburb").attr("disabled","disabled");
                jQuery("#ShippingState").attr("disabled","disabled");
                jQuery("#ShippingPostcode").attr("disabled","disabled");
                jQuery("#ShippingCountry").attr("disabled","disabled");
            } else {
                jQuery("#ShippingAddress").removeAttr("disabled");
                jQuery("#ShippingSuburb").removeAttr("disabled");
                jQuery("#ShippingState").removeAttr("disabled");
                jQuery("#ShippingPostcode").removeAttr("disabled");
                jQuery("#ShippingCountry").removeAttr("disabled");
            }
        }
    );


    jQuery("#tabs").tab();


        // update basic detail

    var orig_basic = [];


    $("#form_basic_detail :input").each(function () {
        var type = jQuery(this).getType();
        var tmp = {'type': type, 'value': jQuery(this).val()};
        orig_basic[jQuery(this).attr('id')] = tmp;

    });

        // Check values on change
    jQuery('#form_basic_detail').bind('change keyup', function () {
        var disable = true;

        jQuery("#form_basic_detail :input").each(function () {
            var type = jQuery(this).getType();
            var id = jQuery(this).attr('id');  
            if (type == 'text' ) {
                disable = (orig_basic[id].value == jQuery(this).val());
            } 
                //  else if (type == 'select') {
                //      disable = (orig_basic[id].value == jQuery(this).val());
                // }   
            if (!disable) { return false; } // break out of loop
        });

        jQuery('#update_basic_detail').prop('disabled', disable); // update button
    });



    jQuery("#update_basic_detail").click(function(){
        jQuery.ajax({
            url: '<?php echo base_url()?>ajax/update_customer',
            type: 'post',
            data: jQuery('#form_basic_detail').serializeArray(),
            success:function(res){
                if(res == 1)
                    alert('Record updated.');
                jQuery('#update_basic_detail').attr({disabled:'disabled'});
            }
        });
    });




    var orig_partner = [];

        
        //// update partner tab
    $("#form-partner :input").each(function () {
        var type = jQuery(this).getType();
        var tmp = {'type': type, 'value': jQuery(this).val()};
        if (type == 'radio') { tmp.checked = jQuery(this).is(':checked'); }
        orig_partner[jQuery(this).attr('id')] = tmp;
    });

        // Check values on change
    jQuery('#form-partner').bind('change keyup', function () {
        var disable = true;
        jQuery("#form-partner :input").each(function () {
            var type = jQuery(this).getType();
            var id = jQuery(this).attr('id');    
            if (type == 'text' || type == 'select') {
                disable = (orig_partner[id].value == jQuery(this).val());
            } else if (type == 'radio') {
                disable = (orig_partner[id].checked == jQuery(this).is(':checked'));
            }    
            if (!disable) { return false; } // break out of loop
        });

        jQuery('#update_partner').prop('disabled', disable); // update button
    });



    jQuery("#update_partner").click(function(){
        jQuery.ajax({
            url: '<?php echo base_url()?>ajax/update_customer',
            type: 'post',
            data: jQuery('#form-partner').serializeArray(),
            success: function(res){
                if(res == 1)
                    alert('Record updated.');
                jQuery('#update_partner').attr({disabled:'disabled'});
            }
       });
    });



        //// update customers tab
    var orig_addr = [];
    $("#form-address :input").each(function () {
        var type = jQuery(this).getType();
        var tmp = {'type': type, 'value': jQuery(this).val()};
        if (type == 'radio') { tmp.checked = jQuery(this).is(':checked'); }
        if (type == 'checkbox') { tmp.checked = jQuery(this).is(':checked'); }
        orig_addr[jQuery(this).attr('id')] = tmp;
    });

        // Check values on change
    jQuery('#form-address').bind('change keyup', function () {
        var disable = true;
        jQuery("#form-address :input").each(function () {
            var type = jQuery(this).getType();
            var id = jQuery(this).attr('id');    
            if (type == 'text' || type == 'select') {
                disable = (orig_addr[id].value == jQuery(this).val());
            } else if (type == 'radio') {
                disable = (orig_addr[id].checked == jQuery(this).is(':checked'));
            }  else if (type == 'checkbox') {
                disable = (orig_addr[id].checked == jQuery(this).is(':checked'));
            }    
                if (!disable) { return false; } // break out of loop
        });

        jQuery('#update_address').prop('disabled', disable); // update button
    });



    jQuery("#update_address").click(function(){

    jQuery.ajax({
        url: '<?php echo base_url()?>ajax/update_customer',
        type: 'post',
        data: jQuery('#form-address').serializeArray(),
        success: function(res){
            if(res == 1)
                alert('Record updated.');
                jQuery('#update_address').attr({disabled:'disabled'});
            }
        });
    });


        

        //// update ABN tab
    var orig_abn = [];
    $("#form-abn :input").each(function () {
        var type = jQuery(this).getType();
        var tmp = {'type': type, 'value': jQuery(this).val()};
        if (type == 'radio') { tmp.checked = jQuery(this).is(':checked'); }
        if (type == 'checkbox') { tmp.checked = jQuery(this).is(':checked'); }
        orig_abn[jQuery(this).attr('id')] = tmp;
    });

    // Check values on change
    jQuery('#form-abn').bind('change keyup', function () {
        var disable = true;
        jQuery("#form-abn :input").each(function () {
            var type = jQuery(this).getType();
            var id = jQuery(this).attr('id');    
            if (type == 'text' || type == 'select') {
                disable = (orig_abn[id].value == jQuery(this).val());
            } else if (type == 'radio') {
                disable = (orig_abn[id].checked == jQuery(this).is(':checked'));
            }  else if (type == 'checkbox') {
                disable = (orig_abn[id].checked == jQuery(this).is(':checked'));
            }   
            if (!disable) { return false; } // break out of loop
        });

        jQuery('#update_abn').prop('disabled', disable); // update button
    });



    jQuery("#update_abn").click(function(){

        jQuery.ajax({
            url: '<?php echo base_url()?>ajax/update_customer',
            type: 'post',
            data: jQuery('#form-abn').serializeArray(),
            success: function(res){
                if(res == 1)
                    alert('Record updated.');
                jQuery('#update_abn').attr({disabled:'disabled'});
            }
        });
    });



     jQuery("#add-class").on('click',function(){
      
        jQuery('#modal-class-add').modal('show');
        jQuery('#modal-class-add').on('show.bs.modal', function () { 
            jQuery("#EduClassID").val(0);
            jQuery("#NumberAttending").val('');  
        }); 
    });


    jQuery("#form-add-class").submit(function(event ){
        event.preventDefault();
      
        var values = {};
        values['EduClassID'] = jQuery("#EduClassID").val(); 
        values['CustomerID'] = jQuery("#CustomerID").val(); 
        
        jQuery.ajax({
            url: '<?php echo base_url()?>ajaxClasses/addCustomerClass',
            type: 'post',
            dataType: 'json',
            data: values,
            success: function(resp) {
                // if(resp.status == 1)
                 if (resp.status == 1)
                 {
                    jQuery('#modal-class-add').modal('hide');
                    classesTableAPI.draw();
                 }
                 else if (resp.status == 1062)
                 {
                    alert("The customer is already booked to this class.")
                 }
                 else 
                 {
                    alert("Could not add : " + resp.status);
                 }
                //  //document.location = '<?php echo base_url()?>ajax/addCustomerClass';
            }
        });
        
        
        
      

    });
        
    $('#BookingDateTime').datepicker({
        format: 'yyyy-mm-dd'
        
    })


    jQuery("#EduClassID").select2(
        {
            ajax: {
                url: "<?php echo base_url().'ajaxClasses/getSelect2Classes'?>",
                dataType: 'json',
                width: 'resolve',
                delay: 250,
                results: function (data, page) { 
                    return {results: data};
                },
                data: function (params) { 
                  return {
                    q: params.term, // search term
                    page: params.page
                  };
                },
                minimumInputLength: 1 
            },
            placeholder:'Select a class'
        }
    );

 });

jQuery.fn.getType = function () { 
    if(this[0].tagName == "INPUT"){
        return jQuery(this[0]).attr("type").toLowerCase() 
    }
    else {
        return this[0].tagName.toLowerCase();        
    }
}*/
</script>