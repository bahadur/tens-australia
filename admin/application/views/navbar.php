<?php $ci = get_instance(); ?>
<div class="sidebar-menu">

        <div class="sidebar-menu-inner">
            
            <header class="logo-env">

                <!-- logo -->
                <div class="logo">
                    <a href="<?= base_url() ?>">
                        <img src="<?= base_url().'assets/neon/images/logo.png'?>" width="120" alt="" />
                    </a>
                </div>

                <!-- logo collapse icon -->
                <div class="sidebar-collapse">
                    <a href="#" class="sidebar-collapse-icon"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
                        <i class="entypo-menu"></i>
                    </a>
                </div>

                                
                <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
                <div class="sidebar-mobile-menu visible-xs">
                    <a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
                        <i class="entypo-menu"></i>
                    </a>
                </div>

            </header>
 
                                    
            <ul id="main-menu" class="main-menu">
                <!-- add class "multiple-expanded" to allow multiple submenus to open -->
                <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
 
                <li class="<?php if(isset($expandBooking)){echo 'active opened active';}?>">
                    <a href="index.html">
                        <i class="entypo-doc-text"></i>
                        <span class="title">Booking</span>
                    </a>
                    <ul>
                        <li class="<?php if($this->router->fetch_method() == 'confirmOrders') { echo 'active';}?>"> 
                            <a href="<?= base_url().'confirmorders/vc/' ?>">
                                <span class="title">Confirm bookings</span>
                            </a>
                        </li>
                        <li class= "<?php if($this->router->fetch_method() == 'newOrder'){echo 'active';}?>"> 
                            <a href="<?= base_url().'orders/vc/newOrder' ?>">
                                <span class="title">New bookings</span>
                            </a>
                        </li>
                        <li class = "<?php if($this->router->fetch_method() == 'shipout'){echo 'active';}?>">
                            <a href="<?= base_url().'shipout/vc/shipout' ?>">
                                <span class="title">Ship Out</span>
                            </a>
                        </li>
                        <li class = "<?php if($this->router->fetch_method() == 'returnHiringProduct'){echo 'active';}?>">
                            <a href="<?= base_url().'returns/vc/returnHiringProduct' ?>">
                                <span class="title">Return by barcode</span>
                            </a>
                        </li>

                        <?php
                        if ($this->tank_auth->is_logged_in_as(array('administrator')))
                        { 
                        ?>
                            <li class = "<?php if($this->router->fetch_method() == 'returnHiringProduct'){echo 'active';}?>">
                                <a href="<?= base_url().'returnProduct/vc/returnHiredProductByOrderID' ?>">
                                    <span class="title">Return by Order ID</span>
                                </a>
                            </li>
                        <?php 
                        }
                        ?>
                        <li class = "<?php if($this->router->fetch_method() == 'refund'){ echo 'active';}?>">
                            <a href="<?= base_url().'refunds/vc/refund' ?>">
                                <span class="title">Refunds</span>
                            </a>
                        </li>
                        <?php
                        if ($this->tank_auth->is_logged_in_as(array('administrator')))
                        { 
                        ?>
                            <li class = "<?php if($this->router->fetch_method() == 'returnHiringProduct'){echo 'active';}?>">
                                <a href="<?= base_url().'refunds/vc/refundByOrderID' ?>">
                                    <span class="title">Refund by Order ID</span>
                                </a>
                            </li>
                        <?php 
                        }
                        ?>
                    </ul>
                </li>

                <li class="<?php if(isset($expandSearch)){echo 'active opened active';}?>">
                    <a href="#">
                        <i class="entypo-search"></i>
                        <span class="title">Search</span>
                    </a>       
                    <ul>
                        <li class="<?php if($this->router->fetch_method() == 'byArticleId'){echo 'active';}?>">
                            <a href="<?= base_url().'search/vc/byArticleId' ?>">
                                <span class="title">Search By Article API ID</span>
                            </a>
                        </li>                       
                        <li class = "<?php if($this->router->fetch_method() == 'byBarcode'){echo 'active';}?>">
                            <a href="<?= base_url().'search/vc/byBarcode' ?>">
                                <span class="title">Search By Barcode</span>
                            </a>
                        </li>                       
                        <li class = "<?php if($this->router->fetch_method() == 'byBookings'){echo 'active';}?>">
                            <a href="<?= base_url().'search/vc/byBookings' ?>">
                                <span class="title">Search By Booking</span>
                            </a>
                        </li>                       
                        <li class = "<?php if($this->router->fetch_method() == 'byCustomer'){echo 'active';}?>">
                            <a href="<?= base_url().'search/vc/byCustomer' ?>">
                                <span class="title">Search By Customer</span>
                            </a>
                        </li>                       
                        <li  class="<?php if($this->router->fetch_method() == 'byCustomerNotes'){echo 'active';}?>">
                            <a href="<?= base_url().'search/vc/byCustomerNotes' ?>">
                                <span class="title">Search By Customer Notes</span>
                            </a>
                        </li>                       
                        <li class = "<?php if($this->router->fetch_method() == 'byEducator'){echo 'active';}?>">
                            <a href="<?= base_url().'search/vc/byEducator' ?>">
                                <span class="title">Search By Educator</span>
                            </a>
                        </li>                        
                        <li class = "<?php if($this->router->fetch_method() == 'byLocation'){echo 'active';}?>">
                            <a href="<?= base_url().'search/vc/byLocation' ?>">
                                <span class="title">Search By Location</span>
                            </a>
                        </li>                    
                        <li class = "<?php if($this->router->fetch_method() == 'byMobileNumber'){echo 'active';}?>">
                            <a href="<?= base_url().'search/vc/byMobileNumber' ?>">
                                <span class="title">Search By Mobile Number</span>
                            </a>
                        </li>                       
                        <li class = "<?php if ($this->router->fetch_method() == 'byOrderId'){echo 'active';}?>">
                            <a href="<?= base_url().'search/vc/byOrderId' ?>">
                                <span class="title">Search By Order ID</span>
                            </a>
                        </li>

                    </ul>
                </li>


                <li class="<?php if(isset($expandMaintainEquip)){echo 'active opened active';}?>">
                    <a href="#">
                        <i class="entypo-suitcase"></i>
                        <span class="title">Maintain Equipment</span>
                    </a> 
                    <ul>
                        <li class = "<?php if($this->router->fetch_method() == 'equipment_move_location'){echo 'active';}?>">
                            <a href="<?php echo base_url();?>equipment/vc/equipment_move_location">
                                <span class="title">Equip Move Location</span>
                            </a>
                        </li>   
                        <li class = "<?php if($this->router->fetch_method() == 'purchaseOrder'){echo 'active';}?>">
                            <a href="<?php echo base_url();?>equipment/vc/purchaseOrder">
                                <span class="title">Equip Purchase Order</span>
                            </a>
                        </li>   
                        <li class = "<?php if($this->router->fetch_method() == 'swapProduct'){echo 'active';}?>">
                            <a href="<?php echo base_url();?>equipment/vc/swapProduct">
                                <span class="title">Equip-Swap Product Type</span>
                            </a>
                        </li>
                        <li class = "<?php if($this->router->fetch_method() == 'ViewLocationsStocks'){echo 'active';}?>">
                            <a href="<?php echo base_url();?>equipment/vc/ViewLocationsStocks">
                                <span class="title">View Stock at Locations</span>
                            </a>
                        </li>    
                        
                    </ul>
                    
                </li>
                <li class="<?php if(isset($expandClasses)){echo 'active opened active';}?>">
                    <a href="#">
                        <i class="entypo-monitor"></i>
                        <span class="title">Classes</span>
                    </a> 
                    <ul>
                        <li class = "<?php if($this->router->fetch_method() == 'ClassList'){echo 'active';}?>">
                            <a href="<?= base_url().'classes/vc/classlist' ?>">
                                <span class="title">Class List</span>
                            </a>
                        </li>   
                        <li class="<?php if($this->router->fetch_method() == 'find'){echo 'active';}?>" >
                            <a href="<?= base_url().'customers/vc/find' ?>">
                                <span class="title">Find Customer</span>
                            </a>
                        </li>   
                    </ul>

                </li>

                <li class = "<?php if(isset($expandServices)){echo 'active opened active';}?>">
                    <a href="#">
                        <i class="entypo-suitcase"></i>
                        <span class="title">Services</span>
                    </a> 
                    <ul>
                        <li class="<?php if($this->router->fetch_method() == 'eduClassesList'){echo 'active';}?>">
                            <a href="<?php echo base_url();?>Services/eduClassesList">
                                <span class="title">FreeMOM TENS Classes Available</span>
                            </a>
                        </li>   
                        <li class = "<?php if($this->router->fetch_method() == 'delSuiteSupportProgramList'){echo 'active';}?>">
                            <a href="<?php echo base_url()?>Services/delSuiteSupportProgramList">
                                <span class="title">FreeMOM TENS Del Suite</span>
                            </a>
                        </li>   
                        <li class = "<?php if($this->router->fetch_method() == 'educatorEquipmentList' ){echo 'active';}?>">
                            <a href="<?php echo base_url();?>Services/educatorEquipmentList">
                                <span class="title">FreeMOM TENS Educator</span>
                            </a>
                        </li>   
                    </ul>
                    
                </li>               

                <li> 
                    <a href="#">
                        <i class="entypo-list"></i>
                        <span class="title">Product/Price List</span>
                    </a>

                    <ul>
                        <li>
                            <a href="#">
                                <span class="title">Product Price List Retail</span>
                            </a>
                        </li>   
                        <li>
                            <a href="#">
                                <span class="title">Product Order Form Retail</span>
                            </a>
                        </li>   
                    </ul>
                    

                </li>

                <li class= "<?php if(isset($expandEndOfTheDay)){echo 'active opened active';}?>">
                    <a href="#">
                        <i class="entypo-light-down"></i>
                        <span class="title">End of Day</span>
                    </a> 
                    <ul>
                        <li class = "<?php if($this->router->fetch_method() == 'endOfTheDay'){echo 'active';}?>">
                            <a href="<?= base_url().'endOfTheDay' ?>">
                                <span class="title">Export to MYOB</span>
                            </a>
                        </li>   
                    </ul>

                    
                </li>
                <?php
            if ($this->tank_auth->is_logged_in_as(array('administrator')))
            {
            ?>
                <li  class="<?php echo ($this->router->fetch_class() == 'usersManagement')?'active opened active':''?>">
                    <a href="#">
                        <i class="entypo-user"></i>
                        <span class="title">Users Management</span>
                    </a> 
                    <ul>
                        <li class="<?php echo ($this->router->fetch_class() == 'index')?'active opened active':''?>">
                            <a href="<?= base_url().'usersmanagement/vc' ?>">
                                <span class="title">Manage Users</span>
                            </a>
                        </li>   
                    </ul>

                    
                </li>

                <li  class="<?php echo ($this->router->fetch_class() == 'cms')?'active opened active':''?>">
                    <a href="#">
                        <i class="entypo entypo-cog"></i>
                        <span class="title">Front-End Content</span>
                    </a> 
                    <ul>
                        <li <?php echo ($this->router->fetch_method() == "manageProducts")?"active":""?>>
                            <a href="<?= base_url().'products/vc/manageProducts' ?>">
                                <span class="title">Manage Front-end Products</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li  class="<?php echo ($this->router->fetch_class() == 'administrator')?'active opened active':''?>">
                    <a href="#">
                        <i class="entypo entypo-cog"></i>
                        <span class="title">Administrator</span>
                    </a> 
                    <ul>
                        <li <?php echo ($this->router->fetch_method() == "configuration")?"active":""?>>
                            <a href="<?= base_url().'administrator' ?>">
                                <span class="title">Configuration</span>
                            </a>
                        </li>

                        <li <?php echo ($this->router->fetch_method() == "classes_admin")?"active":""?>>
                            <a href="<?= base_url().'administrator/classes_admin' ?>">
                                <span class="title">Classes</span>
                            </a>
                        </li>

                        <li <?php echo ($this->router->fetch_method() == "payment_methods")?"active":""?>>
                            <a href="<?= base_url().'administrator/payment_methods' ?>">
                                <span class="title">Payment Methods</span>
                            </a>
                        </li>

                        <li <?php echo ($this->router->fetch_method() == "account_codes")?"active":""?>>
                            <a href="<?= base_url().'administrator/account_codes' ?>">
                                <span class="title">Account Codes</span>
                            </a>
                        </li>

                        <li <?php echo ($this->router->fetch_method() == "area_managers")?"active":""?>>
                            <a href="<?= base_url().'administrator/area_managers' ?>">
                                <span class="title">Area Managers</span>
                            </a>
                        </li>

                        <li <?php echo ($this->router->fetch_method() == "customer_types")?"active":""?>>
                            <a href="<?= base_url().'administrator/customer_types' ?>">
                                <span class="title">Customer Types</span>
                            </a>
                        </li>

                        <li <?php echo ($this->router->fetch_method() == "education_locations")?"active":""?>>
                            <a href="<?= base_url().'administrator/education_locations' ?>">
                                <span class="title">Education Locations</span>
                            </a>
                        </li>

                        <li <?php echo ($this->router->fetch_method() == "hospital_groups")?"active":""?>>
                            <a href="<?= base_url().'administrator/hospital_groups' ?>">
                                <span class="title">Hospital Groups</span>
                            </a>
                        </li>

                        <li <?php echo ($this->router->fetch_method() == "educator_classes")?"active":""?>>
                            <a href="<?= base_url().'administrator/educator_classes' ?>">
                                <span class="title">Educator Classes</span>
                            </a>
                        </li>

                        <li <?php echo ($this->router->fetch_method() == "educators")?"active":""?>>
                            <a href="<?= base_url().'administrator/educators' ?>">
                                <span class="title">Educators</span>
                            </a>
                        </li>

                        <li <?php echo ($this->router->fetch_method() == "locations")?"active":""?>>
                            <a href="<?= base_url().'administrator/locations' ?>">
                                <span class="title">Locations</span>
                            </a>
                        </li>

                        <li <?php echo ($this->router->fetch_method() == "insurance_providers")?"active":""?>>
                            <a href="<?= base_url().'administrator/insurance_providers' ?>">
                                <span class="title">Insurance Providers</span>
                            </a>
                        </li>

                        <li <?php echo ($this->router->fetch_method() == "order_source")?"active":""?>>
                            <a href="<?= base_url().'administrator/order_source' ?>">
                                <span class="title">Order Source</span>
                            </a>
                        </li>

                        <li <?php echo ($this->router->fetch_method() == "reminders")?"active":""?>>
                            <a href="<?= base_url().'administrator/reminders' ?>">
                                <span class="title">Reminders</span>
                            </a>
                        </li>

                        <li <?php echo ($this->router->fetch_method() == "location_types")?"active":""?>>
                            <a href="<?= base_url().'administrator/location_types' ?>">
                                <span class="title">Location Types</span>
                            </a>
                        </li>

                        <li <?php echo ($this->router->fetch_method() == "price_groups")?"active":""?>>
                            <a href="<?= base_url().'administrator/price_groups' ?>">
                                <span class="title">Price Groups</span>
                            </a>
                        </li>

                        <li <?php echo ($this->router->fetch_method() == "product_categories")?"active":""?>>
                            <a href="<?= base_url().'administrator/product_categories' ?>">
                                <span class="title">Product Categories</span>
                            </a>
                        </li>

                        <li <?php echo ($this->router->fetch_method() == "post_codes")?"active":""?>>
                            <a href="<?= base_url().'administrator/post_codes' ?>">
                                <span class="title">Post Codes</span>
                            </a>
                        </li>

                        <li <?php echo ($this->router->fetch_method() == "transaction_types")?"active":""?>>
                            <a href="<?= base_url().'administrator/transaction_types' ?>">
                                <span class="title">Transaction Types</span>
                            </a>
                        </li>

                        <li <?php echo ($this->router->fetch_method() == "ship_methods")?"active":""?>>
                            <a href="<?= base_url().'administrator/ship_methods' ?>">
                                <span class="title">Ship Methods</span>
                            </a>
                        </li>

                        <li <?php echo ($this->router->fetch_method() == "stockmove_types")?"active":""?>>
                            <a href="<?= base_url().'administrator/stockmove_types' ?>">
                                <span class="title">Stock Move Types</span>
                            </a>
                        </li>

                        <li <?php echo ($this->router->fetch_method() == "kits")?"active":""?>>
                            <a href="<?= base_url().'administrator/kits' ?>">
                                <span class="title">Kits</span>
                            </a>
                        </li>

                        <li <?php echo ($this->router->fetch_method() == "hospitals")?"active":""?>>
                            <a href="<?= base_url().'administrator/hospitals' ?>">
                                <span class="title">Hospitals</span>
                            </a>
                        </li>

                        <li <?php echo ($this->router->fetch_method() == "products")?"active":""?>>
                            <a href="<?= base_url().'administrator/products' ?>">
                                <span class="title">Products</span>
                            </a>
                        </li>

                        <li <?php echo ($this->router->fetch_method() == "refundByOrderID")?"active":""?>>
                            <a href="<?= base_url().'administrator/refundByOrderID' ?>">
                                <span class="title">refund By Order ID</span>
                            </a>
                        </li>
                        

                    </ul>

                    
                </li>
                <?php 
                }
                ?>
                
            </ul>
            
        </div>

    </div>
    <div class="main-content">
        
        <div class="row">
            <div class="col-md-6 col-sm-8 clearfix">
                <ul class="user-info pull-left pull-none-xsm">
                    <li class="profile-info dropdown">
                        <a href="#" >
                            <img src="<?php echo base_url()?>assets/img/thumb-1@2x.png" alt="" class="img-circle" width="44" />
                            <?php echo $this->tank_auth->get_full_name();?>
                        </a> 
                    </li>    
                </ul>


            </div>
            <div class="col-md-6 col-sm-4 clearfix hidden-xs">
                <ul class="list-inline links-list pull-right">
                    <li>
                        <a href="<?= base_url().'auth/logout'?>">
                            Log Out <i class="entypo-logout right"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
<hr />
        <div class="row">
        
            <!-- Profile Info and Notifications -->
            <div class="col-md-12 col-sm-12 clearfix">

                <div class="row"> 
                </div>

            </div>
        
        
            <!-- Raw Links -->
            
        
        </div>
        <hr/>
        <h1><?php echo $title; ?></h1>

