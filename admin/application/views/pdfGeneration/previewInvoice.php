<?php

function formatAmount($amount)
{
    return "$".number_format((float)$amount, 2, '.', '');
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>.:: Tens Australia ::.</title>
        <link rel="stylesheet" href="<?php echo base_url();?>assets/pdf/styleInvoice.css">
    </head>
    <body>  
        <div class="container">
            <div class="header">
                <div class="logo">
                    <img class="logo" src="<?php echo base_url();?>assets/img/logo.jpg" alt="">
                </div>
                <div class="address">
                    <table>
                        <tr>
                            <td colspan="2" style="text-align:center">7 / 2 Central. Ave Moorabbin VIC 3189</td>
                        </tr>
                        <tr>
                            <td style="text-align:center">Tel 1300 913 129</td>
                            <td style="text-align:center">Fax 1300 913 149</td>
                        </tr>
                        <tr>
                            <td style="text-align:center">info@tensaustralia.com</td>
                            <td style="text-align:center">www.tensaustralia.com</td>
                        </tr>
                    </table> 
                    <div class="small-font">
                        <p>Tens’R’Us Pty Ltd formerly KRIESLEX PTY LTD</p>
                        <p>ABN 64 130 096 949 ACB 601 058 995</p> 
                    </div>
                </div>
                <div class="invoice">
                    <table>
                        <tr>
                            <th class="invoice" colspan="2">Tax Invoice:</th>
                        </tr>
                        <tr>
                            <td>Invoice #:</td>
                            <td><?php echo $invoiceInfo->OrderID?></td>
                        </tr>
                        <tr>
                            <td>Date:</td>
                            <td><?php 
                                if ($invoiceInfo->InvoiceDate)
                                {
                                    $format = "Y-m-d H:i:s";
                                    $invoiceDate = DateTime::createFromFormat($format, $invoiceInfo->InvoiceDate); 
                                    echo $invoiceDate->format('d/m/Y'); 
                                } 
                                ?></td>
                        </tr>
                        <tr>
                            <td>Terms:</td>
                            <td>C.O.D.</td>
                        </tr> 
                    </table>
                </div>
            </div>
            <div class="clear"></div> 

            <div class="bill_to_left">
                <table class="bill_to_left">
                    <tr>
                        <td>Bill to:</td>
                        <td></td> 
                    </tr>
                    <tr> 
                        <td style="width:30%"></td>
                        <td style="height:30px"><?php echo $invoiceInfo->BusinessName?></td> 
                    </tr>
                    <tr>
                        <td></td>
                        <td style="height:30px"><?php echo $invoiceInfo->Firstname.' '.$invoiceInfo->LastName?></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="height:30px"><?php echo $invoiceInfo->BillingAddress?></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="height:30px"><?php 
                            echo $invoiceInfo->BillingSuburb.' '.$invoiceInfo->BillingState.
                                ' '.$invoiceInfo->BillingPostcode
                                ?></td>
                    </tr>
                </table>
            </div>

            <div class="bill_to_right"> 
                     <p>Victorian Work-cover Authority Provider No: AP4377</p> 
                     <hr class="divider"/>
                     <p>Transport Accident Commission No:20114140</p> 
                     <hr class="divider"/>
                        <p>Bupa Australia Provider Number EP08949</p>
                        <p>HBA Recognised Provider</p>
                        <p>Mutual Community Recognised Provider</p>
                        <p>MBF Recognised Provider</p> 
                        <p>MBF Alliances Recognised Provider</p> 
                    <hr class="divider"/>
                        <p>Nib Provider Number : 10112588</p> 
            </div>
            <div class="clear"></div> 
            <table class="third-row">
                <tr>
                    <td class="label-cell">Customer Tel./Mob.:</td>
                    <td><?php 
                        echo $invoiceInfo->HomePhone.' '.
                        $invoiceInfo->BusinessPhone.'/'.
                        $invoiceInfo->MobilePhone;
                    ?></td>
                    <td class="label-cell">Hospital Name:</td>
                    <td><?php echo $invoiceInfo->HospitalName; ?></td>
                </tr>
                <tr>
                    <td class="label-cell">Payment Method:</td>
                    <td><?php echo $paymentMethods; ?></td>
                    <td class="label-cell">Education Ref.:</td>
                    <td><?php echo $invoiceInfo->Educator; ?></td>
                </tr>
                <tr>
                    <td class="label-cell">Customer Ref.</td>
                    <td><?php echo $invoiceInfo->CustomerReference; ?></td>
                    <td class="label-cell">Ship Method:</td>
                    <td><?php echo $invoiceInfo->ShipMethod; ?></td>
                </tr>
            </table>
            <div class="item-table">
                <table>
                    <tr>
                        <th>Qty</th>
                        <th>Item No</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th>GST</th>
                        <th>Total Inc GST</th>
                    </tr>
                    <?php 
                    $totalGst = 0;
                    $totalAmount = 0;
                    foreach ($invoiceItems as $invoiceItem)
                    {
                    ?>
                    <tr>
                        <td><?php echo $invoiceItem->Quantity; ?></td>
                        <td><?php echo $invoiceItem->ProductCode; ?></td>
                        <td><?php echo $invoiceItem->ProductDetail == NULL ? $invoiceItem->ProductName : $invoiceItem->ProductDetail; ?></td>
                        <td><?php echo formatAmount($invoiceItem->UnitPrice); ?></td>
                        <td><?php echo formatAmount($invoiceItem->GST_Amount); $totalGst+= $invoiceItem->GST_Amount * 1;  ?></td>
                        <td><?php echo formatAmount($invoiceItem->TotalAmount); $totalAmount+= $invoiceItem->TotalAmount * 1; ?></td>
                    </tr>
                    <?php
                    }
                    ?> 
                </table>
            </div>
            <div class="important-info">
                <?php if ($importantInfo) { ?>
                <p>IMPORTANT INFORMATION</p>
                <p><?php echo $importantInfo; ?></p>
                <?php }?>
            </div>
            <br/>
            <br/>
            <div class="left-payment">
                <table class="payment-header">
                    <tr>
                        <td colspan="2" style="font-size:16px">Payment Methods</td>
                    </tr>
                    <tr>
                        <td>Direct Deposit</td>
                        <td>CHEQUE</td>
                    </tr>
                    <tr>
                        <td>BSB: 033-095</td>
                        <td>Payable to: TENS"R"US PTY LTD</td>
                    </tr>
                    <tr>
                        <td>Account:50-06057</td> 
                        <td></td>
                    </tr>
                    <tr>
                        <td>Account Name: TENS"R"US PTY LTD</td> 
                        <td>CREDIT CARD</td>
                    </tr>
                    <tr>
                        <td>Please use the invoice no. as the transaction reference/description</td> 
                        <td>Mastercard of Visa</td>
                    </tr>
                </table>
                <table style="margin-top:12px"  class="payment-header">
                    <tr>
                        <td colspan="2" style="height:45px">Title for the goods listed in this invoice shall not pass to the client until full and cleared payment is received by TENS"R"US Pty Ltd.</td> 
                    </tr>
                </table>
            </div>
            <div class="right-payment">
                <table>
                    <tr>
                        <td style="height:35px">Total GST:</td>
                        <td style="height:35px"><?php echo formatAmount($totalGst); ?></td>
                    </tr>
                    <tr>
                        <td  style="height:35px">Total Inc GST:</td>
                        <td style="height:35px"><?php echo formatAmount($totalAmount); ?></td>
                    </tr>
                    <tr>
                        <td style="height:35px">Payment:</td>
                        <td style="height:35px"><?php echo formatAmount($invoiceInfo->PaymentTotal); ?></td>
                    </tr>
                </table>
                <table style="margin-top:12px; font-weight:bold">
                    <tr>
                        <td style="height:87px">Balance to Pay:</td>
                        <td style="height:87px"><?php echo formatAmount($totalAmount); ?></td>
                    </tr>
                </table>
            </div>
        </div>
    </body>
</html>

