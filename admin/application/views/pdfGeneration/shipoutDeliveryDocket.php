
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>.:: Tens Australia ::.</title>
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/pdf/styleInvoice.css">
</head>
<body style="margin:25px">   
  <table style="position:relative;width:100%; ">
    <tr>
      <td style="width:30%"><img src="<?php echo base_url(); ?>assets/img/logo.jpg" height="100px"></img></td>
      <td style="width:55%">

        <table style="position:relative; width:100%; font-size:16px; font-weight:bold">
          <tr>
            <td colspan="2" style="text-align:center">7 / 2 Central. Ave Moorabbin VIC 3189</td>
          </tr>
          <tr>
            <td style="text-align:right;margin-right:6px"><p style="">Tel 1300 913 129</p></td>
            <td style="text-align:left;margin-left:6px"><p style="">Fax 1300 913 149</p></td>
          </tr>
          <tr>
            <td> <p style="text-align:right; ">info@tensaustralia.com</p></td>
            <td> <p style="text-align:left; ">www.tensaustralia.com</p></td>
          </tr>
          <tr>
            <td colspan="2" style="text-align:center;">
              <p style="width:100%;font-size:10px;margin-top:6px">Tens’R’Us Pty Ltd formerly KRIESLEX PTY LTD     ABN 64 130 096 949</p> 
            </td>
          </tr>
        </table> 
      </td>
      <td style="width:45%; vertical-align:top"><h2>Packing Slip</h2>
        <table style="">
          <tr>
            <td>Invoice</td>
            <td><?php echo $general->OrderID;?></td>
          </tr>
          <tr>
            <td>PO</td>
            <td><?php echo $general->CustomerReference;?></td>
          </tr>
          <tr>
            <td>Required Date</td>
            <td><?php echo date('d/m/y',strtotime($general->RequiredDate)); ?></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <br/>
  <table style="position:relative; width:100%">
    <tr>
      <th style="width:40%; background-color:black;color:white;text-align:left;padding-top:12px; padding-bottom:12px; padding-left:24px"><p style="height:24px">Delivery Address</p></th> 
      <th style="width:60%"></th>
    </tr>
    <tr>
      <td colspan="2" style="padding-left:24px;padding-top:12px">
        <p style="margin-left:12px; margin-top:12px"><?php echo $general->Firstname.' '.$general->LastName.' ('.$general->BusinessName.') '; ?></p></td>
      </tr>
      <tr>
        <td colspan="2" style="padding-left:24px"><p style="margin-left:12px"><?php echo $general->ShippingAddress;?></p></td>
      </tr>
      <tr>
        <td colspan="2" style="padding-left:24px"><p style="margin-left:12px"><?php echo $general->ShippingSuburb.' '.$general->ShippingState.' '.$general->ShippingPostcode;?></p></td>
      </tr>
      <tr>
        <td colspan="2" style="padding-left:24px"><p style="margin-left:12px"><?php echo $general->ShippingCountry;?></p></td>
      </tr>
    </table>
    <br/>
    <br/>

    <table style="position:relative; width:100%">
      <tr style="background-color:black;color:white;border:black">
        <th style="padding:12px;color:white; text-align:center"><p>Qty</p></th>
        <th style="padding:12px;color:white; text-align:center"><p>Product Description(s)</p></th>
        <th style="padding:12px;color:white; text-align:center"><p>Barcode</p></th>
      </tr>
      <tr>
        <?php 
        if ($details && sizeof($details)) {
          foreach ($details as $detail) {
            ?><tr>
            <td style='text-align:center'><?php echo $detail->Quantity; ?></td>
            <td style='text-align:center'><?php echo $detail->ProductDetail.'['.$detail->ProductCode.']'; ?></td>
            <td style='text-align:center'><?php echo $detail->Barcode; ?></td></tr>
            <?php } 
          }?>
        </tr>
      </table>

      <br/>
      <br/>
      <p>Comments: <?php if ($details && sizeof($details)) { echo $details[0]->Comments;} ?> </p>
    </body>
    </html>

