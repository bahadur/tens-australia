<div class="panel panel-primary">
  <div class="panel-body">
    <table id="products-table" class="table table-bordered">
      <thead>
        <tr>
          <th>Product ID</th>
          <th>Code</th>
          <th>Name</th>
          <th>Category</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody></tbody>
    </table>
  </div>
</div>

<script type="text/javascript">
 jQuery("#products-table").dataTable( {
        processing: true,
        serverSide: true,
        pagination: true,
        pageLength: 10,
        ajax: {
         url: "<?php echo base_url().'products/api/getManageProducts'; ?>",
         type: "POST"
        },
        aoColumns:[
          {
          data: 'ProductID'
          },
            {
          data: 'ProductCode'
          },
            {
          data: 'ProductName'
          },
            {
          data: 'ProductCategory'
          },{
          data: 'action'
          }
        ],
        aoColumnDefs : [ 
          {   
          aTargets: [ 4 ],   
          mRender: function ( data, type, full ) { 
            return '<a class="btn btn-primary" href="<?php echo base_url().'products/vc/viewProduct/'?>' + full['ProductID'] + '">View</a>';
          }
        }
        ]
    } );
// Create a datatable.
</script>