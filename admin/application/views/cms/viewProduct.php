  <link rel="stylesheet" href="<?php echo base_url()?>assets/js/icheck/skins/minimal/_all.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/js/icheck/skins/square/_all.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/js/icheck/skins/flat/_all.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/js/icheck/skins/futurico/futurico.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/js/icheck/skins/polaris/polaris.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/js/icheck/skins/line/_all.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/croppic/croppic.css">
  <script src="<?php echo base_url()?>assets/js/icheck/icheck.min.js"></script>
  <script src="<?php echo base_url()?>assets/croppic/croppic.js"></script>



  <style type="text/css">

  #dropzone-area {
      width: 200px;
      height: 150px;
      
      position:relative; /* or fixed or absolute */
    }

    </style>
<div class='panel panel-primary'> 
  <div class='panel-body'>  
    <div class='row'>
    <div class='col-md-3'>
      <div class="well">
        <div id='dropzone-area'>
            <!-- <img src='<?php //echo base_url().'assets/img/product-placeholder.png'?>' height='200'/> -->

        </div>
      </div>
    </div>
    <div class='col-md-offset-1 col-md-8'>
      <form class='form'>
        <label><h4>Product Title</h4></label>
        <input class='form-control' value='<?php echo $product->ProductName; ?>'/> 
        <br/>

        <div class='form-group'>
          <label><h4>Product Category</h4></label>
          <select class='form-control select2' id='select2categories'>
          <?php foreach ($categories as $category) {
            if ($category->ProductCategoryID === $product->ProductCategoryID) {
            ?>
              <option value='<?php $category->ProductCategoryID; ?>' selected><?php echo $category->ProductCategory; ?></option>
            <?php } else { ?>
              <option value='<?php $category->ProductCategoryID; ?>'><?php echo $category->ProductCategory; ?></option>
          <?php }
          }?>
          </select>
        </div>


        <div class='form-group'>
          <div class="row">
            <div class="col-md-4">
              <label><h4>Show on website</h4></label>
            </div>
            <div class="col-md-6">
              <input type='checkbox' class='icheck-1 form-control' name='onwebsite' <?php if ($product->OnWebsite) { echo 'checked'; } ?>/>
            </div>
          </div>
        </div>

        <div class='form-group'>
          
          <div class="row">
            <div class="col-md-4">
              <label><h4>Active</h4></label>
            </div>
            <div class="col-md-6">
              <input type='checkbox' class='icheck-1 form-control' name='active' <?php if ($product->Active) { echo 'checked'; } ?>/>
            </div>
          </div>
        

        </div>

        <div class='form-group'>
          
          <div class='pull-right' style='margin:5px;margin-right:15px'>
              <a class='btn btn-danger'>Back</a>
              <a class='btn btn-success'>Save</a>
          </div>
          
        </div>

      </form>
    </div>
</div>
    <div class='row col-md-12'>
        <ul class="nav nav-tabs bordered" id="tabs">
            <li class="active"><a href="#addons" data-toggle="tab">Product Addons</a></li>
            <li><a href="#account_code" data-toggle="tab">Account Code</a></li>
            <li><a href="#quantities" data-toggle="tab">Quantities</a></li>
            <li><a href="#barcodes" data-toggle="tab">Barcodes</a></li>
            <li><a href="#prices" data-toggle="tab">Prices</a></li>
            <li><a href="#re_order_level" data-toggle="tab">Re-Order Level</a></li>
            <li><a href="#kits" data-toggle="tab">Kits</a></li>
            <li><a href="#stock_move_history" data-toggle="tab">Stock Move History</a></li>
        </ul>
        <div class="tab-content" id="my-tab-content">
            <div class="tab-pane active" id="addons">
              <div class='row col-md-12'>
                <div class='pull-right'>
                  <a class='btn btn-primary' style='margin:12px; margin-right:-30px' id='add-new-product'>Add a new addon product</a>
                </div>
              </div>
              <table class='table table-bordered' id='addons-table'>
                <thead>
                  <tr>
                    <th>Addon ID</th>
                    <th>Addon Name</th>
                    <th>Field Type</th>
                    <th>Default Value</th>
                    <th>Min Value</th>
                    <th>Position</th>
                    <th>MainProductID</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody> 
                </tbody>
              </table>
            </div>
            <div class="tab-pane" id="prices">
                
              <div class='row col-md-12'>
                <div class='pull-right'>
                  <a class='btn btn-primary' style='margin:12px; margin-right:-30px'>Add a new price</a>
                </div>
              </div>
              <table class='table table-bordered' id='prices-table'>
                <thead>
                  <tr> 
                    <th>Price Group</th>
                    <th>Price</th>
                    <th>ProductPriceID</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody> 
                </tbody>
              </table>
            </div> 
        </div>
    </div>
  </div>
  
</div>


<div class="modal fade custom-width" id="modal-add-product">
  <div class="modal-dialog" style="width: 60%;">
    <div class="modal-content">
      
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Custom Width Modal</h4>
      </div>
      
      <div class="modal-body">
        <form class="form" id="form-add-product" target="_top">
        
        <div class="row">
          <div class="col-md-offset-2 col-md-8">
        

        <div class="form-group">
          <label  class="control-label"><h4>Product</h4></label>
          <select class="form-control" id="AddonProductID" name="AddonProductID" required>
            <option></option>    
            <?php
            $query = $this->db->query("
            SELECT 
            product.ProductID, 
            product.ProductName 
            FROM Product
            ORDER BY product.ProductName");
            foreach ($query->result() as $row) { ?>
              <option value="<?php echo $row->ProductID ?>" ><?php echo $row->ProductName?></option>
            <?php } ?>
          </select>
        
      </div>

        <div class="form-group">
                      <label  class="control-label"><h4>Field Type</h4></label>
                      
                        <select class="form-control" id="FieldTypeID" name="FieldTypeID" required>
                                 <option></option>    
                                <?php
                                $query = $this->db->query("
                                    SELECT 
                                    addonsfieldtypes.FieldTypeID, 
                                    addonsfieldtypes.FieldType 
                                    
                                    FROM addonsfieldtypes
                                    ORDER BY addonsfieldtypes.FieldType");

                                        
                                foreach ($query->result() as $row) { ?>
                                    <option value="<?php echo $row->FieldTypeID ?>" ><?php echo $row->FieldType?></option>




                                <?php } ?>
                        </select>
                     
                  </div>


                  <div class="form-group" id="field_barcode">
                      <label  class="control-label"><h4>Detault Value</h4></label>
                      
                          <input type="text" name="DefaultValue" id="DefaultValue"  data-rule-required="true" data-message-required="Barcode is required" value="" autofocus class="form-control" > 
                     
                  </div>

                  <div class="form-group"  id="field_qty">
                      <label  class="control-label"><h4>Minmum Value</h4></label>
                      
                          <input type="text" name="MinValue" id="MinValue"   class="form-control" required /> 
                      
                  </div>

                  <div class="form-group"  id="field_qty">
                      <label  class="control-label"><h4>Position</h4></label>
                      
                          <input type="text" name="Position" id="Position"   class="form-control" required /> 
                      
                  </div>

                  <div class="form-actions">
                    <div class="col-sm-offset-3 col-sm-5">
                      <input type="hidden" name="product" id="product"   class="form-control">
                      <input type="hidden" name="barcodecorrect" id="barcodecorrect" value="1">
                      
                      <button type="submit" class="btn btn-blue" id="insert-product" >Add Prodcuct</button>
                      <input type='hidden' name='MainProductID' id='MainProductID' value='<?php echo $product->ProductID?>' />
                    </div>
                  </div>

                  
                  </div>
                </div>
              </form>
            </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <!--button type="button" class="btn btn-info">Save changes</button -->
      </div>
    </div>
  </div>
</div>



<link type='stylesheet' href='<?php echo base_url().'assets/dropzone/dropzone.css'?>'></link>
<script src='<?php echo base_url().'assets/dropzone/dropzone.js'?>'></script>
<script type='text/javascript'> 
  //jQuery("div#dropzone-area").dropzone({ url: "/file/post" });

var addons_table = jQuery('#addons-table').DataTable({
  processing: true,
  serverSide: true,
  pagination: true,
  pageLength: 10,
  sDom:'tp',
  ajax: {
      url: "<?php echo base_url()?>products/api/getProductAddons/<?php echo $product->ProductID;?>",
      type: "POST"
  },
  aoColumns:[
        {
        data: 'AddonProductID'
        },{
        data: 'ProductName'
        },{
        data: 'FieldType'
        },{
        data: 'DefaultValue'
        },{
        data: 'MinValue'
        },{
        data: 'Position'
        },{
        data: 'MainProductID'
        },{
        data: 'action'
        }
  ],
  aoColumnDefs: [ 
    {
      aTargets: [6],
      visible: false
    },{
        aTargets: [7],
        mRender: function ( data, type, full ) { 
          return '<button class=\'btn\'>Edit</button>';
                
        }
    }
  ]
});


jQuery("#form-add-product").submit(function(event ){
    event.preventDefault();
      
    jQuery.ajax({
      url: '<?php echo base_url()?>products/api/add_addon',
      method: 'post',
      data: jQuery("#form-add-product").serialize()
    });

    addons_table.draw();

    jQuery('#modal-add-product').modal('hide');

  });


jQuery('#prices-table').dataTable({
  processing: true,
  serverSide: true,
  pagination: true,
  pageLength: 10,
  sDom:'tp',
  ajax: {
      url: "<?php echo base_url()?>products/api/getProductPrices/<?php echo $product->ProductID;?>",
      type: "POST"
  },
  aoColumns:[
        {
        data: 'PriceGroup'
        },{
        data: 'Price'
        },{
        data: 'ProductPriceID'
        },{
        data: 'action'
        }
  ],
  aoColumnDefs: [ 
    {
      aTargets: [2],
      visible: false
    },{
        aTargets: [3],
        mRender: function ( data, type, full ) { 
          return '<button class=\'btn\'>Edit</button>';
                
        }
    }
  ]

});



  jQuery('input.icheck-1').iCheck({
    checkboxClass: 'icheckbox_minimal',
    radioClass: 'iradio_minimal'
  });

  var cropperOptions = {
      uploadUrl:'<?php echo base_url()?>products/api/upload_image',
      cropUrl:'<?php echo base_url()?>products/api/crop_image'
  }
  
  var cropperHeader = new Croppic('dropzone-area',cropperOptions);

  jQuery('#add-new-product').click(function(){
    
      jQuery('#modal-add-product').modal('show');
      jQuery('#modal-add-product').on('show.bs.modal', function () {
        // jQuery("#productid").val(0);
        // jQuery("#barcode").val('');
        // jQuery("#qty").val('');
        // jQuery("#barcode").removeAttr("disabled","disabled");
        // jQuery("#qty").removeAttr("disabled","disabled");
        // jQuery('#modal-add-product button#insert-product').attr("disabled","disabled");
      });
    
  });

 /*
jQuery("#select2categories").select2(
  {
    ajax: {
      url: "<?php echo base_url().'category/api/allcategories'?>",
        dataType: 'json',
        delay: 250,
        results: function (data, page) {  
            return {results: data};
        },
        data: function (params) {  
          return {
            q: params.term, // search term
            page: params.page
          };
        },
        minimumInputLength: 1
    },
    placeholder:'Select a category',
    initSelection: function(element, callback) {  
      console.log('init selection');
      console.log(element);
      callback({id: '<?php echo $product->ProductCategoryID; ?>', text: '<?php echo $product->ProductCategory; ?>' });
    }
  }
);*/


</script>
