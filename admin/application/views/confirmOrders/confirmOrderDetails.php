<link rel='stylesheet' href='<?= base_url().'assets/neon/js/select2/select2.css'?>'>   
<script src='<?= base_url().'assets/neon/js/select2/select2.min.js'?>'></script> 
<script src='<?= base_url().'assets/neon/js/jquery.validate.min.js'?>'></script> 
<div id='error-alert-message' class='alert alert-danger' style='position:fixed; top:2%; width:40%;z-index:999; left:30%; display:none'>
Oh snap, the field could not show.
</div>
<form action='<?php echo base_url() ?>orders/createOrder' method='POST' id='confirmOrderForm'  
    class='form-groups-bordered'
    onsubmit='return false'>
  <div class='col-md-12'>
         <ul class='nav nav-tabs bordered' id='tabs'>
                    <li class='active'><a href='#order' data-toggle='tab'>Customer Details</a></li> 
                    <li><a href='#products' data-toggle='tab'>Order Items</a></li>  
        </ul>
        <div class='tab-content' id='my-tab-content'>
            <div class='tab-pane active' id='order'>
                    
                    <?php 
                        $this->load->view('confirmOrders/tabs/orderDetailsTab');
                    ?>

            </div> 
            <div class='tab-pane' id='products'>
                    
                    <?php 
                        $this->load->view('confirmOrders/tabs/orderItemsTab');
                    ?>

            </div>
        </div>
    <!--***********************************************************************-->
    <!-- contents for every page starts here -->
    </div>
</form>


<script src='<?= base_url().'assets/js/tensApi/tensApi.js'?>'></script>
<script type='text/javascript'> 
 
function AlertBox() {
    var alertBox = jQuery('#error-alert-message');
    this.show = function(message) {
        alertBox.html(message);
        alertBox.fadeIn(400, function() {
            setTimeout(function() {
                alertBox.fadeOut(400, function() {

                });
            }, 3000);
        });
    }
}

var alertBox = new AlertBox(); 

function CustomerSelectionViewModel(customerDetailsVM) {
    var self = this;
    self.bind = function(customerDetailsVM) {
        customerDetailsVM.bind(self);
        self.customerDetailsVM = customerDetailsVM;
        self.row = jQuery('#tens-customer-selection-row');
        self.selectPanel = self.row.find('#tens-select-customer-panel');
        self.loadingPanel = self.row.find('#tens-loading-panel');
        self.select2Element = self.selectPanel.find('#tens-select2customers'); 
        self.newCustomerButton = self.selectPanel.find('#tens-new-customer-btn');

        self.selectedCustomerPanel = self.row.find('#tens-selected-customer-panel');
        self.selectedCustomer = self.selectedCustomerPanel.find('#tens-selected-customer');
        self.resetCustomerButton = self.selectedCustomerPanel.find('#tens-reset-button');
        
        self.resetCustomerButton.on('click', function(e) {
            self.selectedCustomerPanel.hide();
            self.selectPanel.show();
            self.select2Element.select2('val', '');
            customerDetailsVM.resetCustomer();
        });

        self.newCustomerButton.on('click', function(e) { 
            self.selectPanel.hide();
            self.selectedCustomer.html('New customer');
            self.selectedCustomerPanel.show();
            customerDetailsVM.setNewCustomer();
        });

        self.select2Element.select2(
        {
            ajax: {
                url: '<?php echo base_url().'customers/api/getCustomersSelect'?>',
                dataType: 'json',
                delay: 250,
                results: function (data, page) { 
                    return {results: data};
                },
                data: function (params) { 
                  return {
                    q: params.term, // search term
                    page: params.page
                  };
                },
                minimumInputLength: 1 
            },
            placeholder:'Select a customer to reconcile with'
        })
        .on('change', function(e) {  
            self.selectPanel.hide();
            self.loadingPanel.show();
            var customer = {
                customerId : e.val
            }
            new TensAPI(jQuery, '<?php echo base_url();?>').getCustomer(e.val, function(data) {
                self.loadingPanel.hide();
                self.selectedCustomer.html(data.Firstname + ' ' + data.LastName + ' ' + data.MobilePhone);
                self.selectedCustomerPanel.show();
                customerDetailsVM.setCustomer(data);
            }, function(text) {
                customerDetailsVM.resetCustomer();
                alertBox.show(text);
            }); 
        });

    }
}

function SelectedCustomerFieldRowVM() {
    var self = this;
    self.bind = function(element) { 
        var jqElement = jQuery(element);
        self.name = jqElement.attr('tens-row-key'); 
        self.enteredValueElement = jqElement.find('.tens-entered-value');
        self.reconcileButton = jqElement.find('.tens-reconcile-field-btn');
        self.actualValueElement = jqElement.find('.tens-actual-value');
        self.resetButton = jqElement.find('.tens-reset-field-btn'); 
 
        self.reconcileButton.on('click', function(e) { 
            self.actualValueElement.val(self.enteredValueElement.val());
            self.resetButton.show();
        });

        self.actualValueElement.on('change', function(e) {
            self.resetButton.show();
        })

        self.resetButton.on('click', function(e) {
            self.actualValueElement.val(self.originalValue);
            self.resetButton.hide();
        })
    };
    self.setData = function(value) { 
        self.reconcileButton.show();
        self.actualValueElement.show();
        self.originalValue = value;
        self.actualValueElement.val(value);
    }

    self.reset = function() {
        self.originalValue = null;
        self.reconcileButton.hide();
        self.actualValueElement.val('');
        self.actualValueElement.hide();
        self.resetButton.hide();
    }
}

function SelectedCustomerVM() {
    var self = this;
    self.rowsVMs = new Array();
    self.bind = function(selectionViewModel) {
        self.selectionViewModel = selectionViewModel;
        var panel = jQuery('#tens-customer-details-rows');
        var rows = panel.find('.tens-field-row'); 
        rows.each(function(i, el) { 
            var vm = new SelectedCustomerFieldRowVM();
            vm.bind(el); 
            self.rowsVMs[vm.name] = vm; 
        });
    };

    self.setNewCustomer = function() {
        var fields = [
        'Firstname',
        'LastName',
        'HomePhone',
        'MobilePhone',
        'Email', 
        'BillingAddress',
        'BillingSuburb',
        'BillingState',
        'BillingPostcode',
        'BillingCountry', 
        'ShippingAddress',
        'ShippingSuburb',
        'ShippingState',
        'ShippingPostcode',
        'ShippingCountry',
        ];
        for (var k = 0; k < fields.length; k++) {
            self.rowsVMs[fields[k]].setData('');
        }
    };

    self.setCustomer = function(customerData) {   
        for (var key in customerData) {
          if (customerData.hasOwnProperty(key)) {
            var rowVm = self.rowsVMs[key];
            if (rowVm) {
                rowVm.setData(customerData[key]);
            }
          }
        } 
    };

    self.resetCustomer = function() {
        for (var key in self.rowsVMs) {
            self.rowsVMs[key].reset();
        }
    }
}

var selectedCustomerVM = new SelectedCustomerVM();
var customerSelectionVM = new CustomerSelectionViewModel();
customerSelectionVM.bind(selectedCustomerVM); 
 

</script>