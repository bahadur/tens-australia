
<div class="col-lg-9">
          <!--***********************************************************************-->
          <!-- contents for every page starts here --> 
          <div class="row">
            <div class="col-lg-12">
                <div class="col-lg-12">&nbsp;</div>
                <span style="color:#003399;font-weight:bold;font-size: medium">Class Detail</span>
                <div class="col-lg-12">&nbsp;</div> 
    
                <form class="form-horizontal">
                      <div class="form-group">
                        <label for="EduClassID" class="col-sm-2 control-label">EduClassID</label>
                        <div class="col-sm-6">
                          <?php 
                             echo '<input type="text" class="form-control" disabled="disabled"  placeholder="Enter EduClassID" value="'.$result->EduClassID.'">';
                             ?>
                        </div>
                      </div>
                       <div class="form-group ">
                        <label for="EducationLocation" class="col-sm-2 control-label">Education Location</label>
                        <div class="col-sm-6">
                          <?php
                            echo '<input type="text" class="form-control" disabled="disabled"  placeholder="Enter Education Location" value="'.$result->LocName.'">';
                          ?>
                        </div>
                      </div>
                       <div class="form-group ">
                        <label for="ClassDate" class="col-sm-2 control-label">Class Date</label>
                        <div class="col-sm-6">
                          <?php
                            echo '<input type="text" class="form-control" disabled="disabled"  placeholder="Enter Class Date" value="'.$result->ClassDate.'">';
                          ?>
                        </div>
                      </div>
                       <div class="form-group ">
                        <label for="StartTime" class="col-sm-2 control-label">Start Time</label>
                        <div class="col-sm-6">
                          <?php
                            echo '<input type="text" class="form-control" disabled="disabled"  placeholder="Enter Start Time" value="'.$result->StartTime.'">';
                          ?>
                        </div>
                      </div>
                       <div class="form-group ">
                        <label for="Duration" class="col-sm-2 control-label">Duration</label>
                        <div class="col-sm-6">
                          <?php 
                            echo '<input type="text" class="form-control" disabled="disabled" placeholder="Enter Duration" value="'.$result->Duration.'">';
                          ?>
                        </div>
                      </div>
                       <div class="form-group ">
                        <label for="Educator" class="col-sm-2 control-label">Educator</label>
                        <div class="col-sm-6">
                          <?php
                            echo '<input type="text" class="form-control" disabled="disabled" placeholder="Enter Educator" value="'.$result->BusinessName.'">';
                          ?>
                        </div>
                      </div>
                       <div class="form-group ">
                        <label for="Hospital" class="col-sm-2 control-label">Hospital</label>
                        <div class="col-sm-6">
                          <?php
                            echo '<input type="text" class="form-control" disabled="disabled" placeholder="Enter Hospital" value="'.$result->HospitalName.'">';
                          ?>
                        </div>
                      </div>
                      <div class="form-group ">
                        <label for="Course" class="col-sm-2 control-label">Course</label>
                        <div class="col-sm-6">
                          <?php
                            echo '<input type="text" class="form-control" disabled="disabled" placeholder="Enter Course" value="'.$result->CourseName.'">';
                          ?>
                        </div>
                      </div>
                      
                      
                </form>
                      
                  <h2>Customers</h2>
                  <table class="table table-striped tableStyle" id="customersTable" style="margin-top:15px;" >
                      <thead>
                          <tr>
                              <th>First Name</th>
                              <th>Last Name</th>
                              <th>Address</th>
                              <th>Suburb</th>
                              <th>Mobile Phone</th> 
                              <th>Booking Date & Time</th>
                              
                              <th>No. of People Attending</th>
                              <th>Cancelled</th>
                              
                              
                          </tr>
                      </thead>    
                          <tbody>
                          <?php    
                            if (!is_null($customers) && sizeof($customers) > 0)
                            {
                                foreach ($customers as $customer)
                                {
                                  echo "<td>$customer->Firstname</td>";
                                  echo "<td>$customer->LastName</td>";
                                  echo "<td>$customer->BillingAddress</td>";
                                  echo "<td>$customer->BillingSuburb</td>";
                                  echo "<td>$customer->MobilePhone</td>"; 
                                  echo "<td>$customer->BookingDateTime</td>";
                                  echo "<td>$customer->NumberAttending</td>";
                                  if ($customer->Cancelled == 1)
                                  { 
                                    echo "<td><input type=\"checkbox\" checked=\"checked\" disabled=\"disabled\"></td>";
                                  }
                                  else 
                                  {
                                    echo "<td><input type=\"checkbox\" disabled=\"disabled\"></td>";
                                  }
                                }
                            }   
                          ?> 
                              
                          </tbody>
                  </table>
                  <script type="text/javascript">
                    $("#customersTable").dataTable( {
                                          bPaginate: false,
                                          bFilter: false});
                  </script>
                       
          
                  </div>
              </div>
          <!-- contents for every page ends here -->
          <!--***********************************************************************-->
      </div>      
            

<!-- add the below lines on every content pages -->            
        </div>
    </div>
        <div class="col-lg-1"></div>
    </div><!-- end of row in navbar view -->
</div><!-- end of container in navbar view -->

