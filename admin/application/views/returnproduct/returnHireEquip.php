<div class="row">    
		<div class="col-md-12">
			<div class="panel panel-primary" > 
				<div class="panel-body">
				<br/><br/>
				<form class="form-horizontal" method="post">
				<div class="row">
					<div class="col-md-5">
						<div class="form-group">
							<label class="control-label col-md-4">Order ID: </label>
							<div class="col-md-8">
								<input type="text" class="form-control" readonly="readonly"  id="OrderID" name="OrderID" value="<?php echo $generalInfo->OrderID;?>">
							</div>
						</div>
				
						<div class="form-group">
							<label class="control-label col-md-4">Order Date: </label>
							<div class="col-md-8">
								<input type="text" class="form-control" readonly="readonly"  id="order-date" value=""  /> 
							</div> 
						</div>
						<div class="form-group">
							<label class="control-label col-md-4">Shipped Date: </label>
							<div class="col-md-8">
								<input type="text" class="form-control" readonly="readonly" id="ship-date" value="" /> 
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-4">Customer: </label>
							<div class="col-md-8">
								<input type="text" class="form-control" id="Customer" readonly="readonly" value="<?php echo html_escape($generalInfo->CustomerName);?>"/>
							</div>
						</div>



						<div class="form-group">
							<label class="control-label col-md-4">To Location: </label>
							<div class="col-md-8">
								<select class="form-control" name="returnLocationSelect" id="return-location-select" autocomplete="off">
									<option value="-1" disabled selected>Select Locaton....</option>
									<?php 
										foreach ($returnLocation as $RLocation){?>
											<option value= "<?php echo $RLocation-> LocationID;?>">
												<?php echo $RLocation-> LocationDescription;?>
											</option> 
									<?php }?>
								</select>
                <label class="error-message" id="location-error" style="display:none">Please select a location</label>
							</div>
						</div>

						<div class="form-group" >
							<label class="control-label col-md-4">Scan additional barcode: </label> 
							<div class='col-md-8'>
								<input type='text' class="form-control" name="Barcode"/>
								<button class="btn btn-primary col-md-4" id="scanbtn">Scan</button>
							</div> 
						</div> 
					</div> 
					<div class="col-md-5">
						<div class="form-group">
							<label class="control-label col-md-4">Shipping Address: </label>
							<div class="col-md-8">
								<input type="text" class="form-control" readonly="readonly"  id="ShippingAddress" value="<?php echo $generalInfo->Address ?>" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-4">Home Phone: </label>
							<div class="col-md-8">
								<input type="text" class="form-control" readonly="readonly"  id="HomePhone" value="<?php echo $generalInfo->HomePhone;?> "/>
							</div>
						</div>
						
						
						<div class="form-group">
							<label class="control-label col-md-4">Mobile Phone: </label>
							<div class="col-md-8">
								<input type="text" class="form-control" readonly="readonly"  id="MobilePhone" value="<?php echo $generalInfo->MobilePhone;?>">
							</div>
						</div>
						
						
						<div class="form-group">
							<label class="control-label col-md-4">Email: </label>
							<div class="col-md-8">
								<input type="text" class="form-control" readonly="readonly"  id="Email" value="<?php echo explode('#',$generalInfo->Email)[0]?>" >
							</div>
						</div>

 
						<div class="form-group">
							<label class="control-label col-md-4">Return Date: </label>
							<div class='col-md-8 input-group date' id='datetimepicker'>
								<input type='text' class="form-control" name="return-date" id="return-date" value = "" />
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
								</span> 
							</div>
						</div> 
					</div>
					</div>  
					<div class="row">
						<div class="col-md-offset-1 col-md-9">
							<table class="table table-bordered" id="return-products" width="100%">
								<thead>
									<th>HasBarcodeID</th>
									<th>Product</th>
									<th>Ship Quantity</th>
									<th>BarCode</th>
									<th>Return Quantity</th> 
								</thead>
								<tbody>  	
								</tbody>
							</table>
							</div>
						</div>
						<div class="row"> 
							<div class="col-md-10">
								<a class="col-md-offset-10 col-md-2 btn btn-primary" id="btn-confirm-return">Confirm Return</a> 
							</div>
						</div>
					</form>
				</div>
			</div>
	</div> 
</div>   

<link rel="stylesheet" href="<?php echo base_url().'assets/css/styles.css'?>"/>
<script src="<?php echo base_url().'assets/js/tensApi/tensDatesFormatter.js';?>"></script>
<script src="<?php echo base_url().'assets/js/tensApi/lazyLoadingButton.js';?>"></script>
<script type="text/javascript">

function ReturnHireVM() {
	var self = this;

  self.validateSubmission = function() {
    var selectLoc = jQuery('#return-location-select');
    var toLocationId = selectLoc.val(); 
    if (toLocationId && toLocationId != "-1") {
      selectLoc.removeClass('select-error');
      jQuery('#location-error').hide();
      return true;
    } else {
      console.log('Error');
      selectLoc.addClass('select-error');
      jQuery('#location-error').show();
      return false;
    }
  }

	self.bind = function() {
		var df = new DateTimeFormatter();
		var orderdate = '<?php echo $generalInfo->OrderDate;?>';
		var shipdate = '<?php echo $generalInfo->MoveDate;?>';
		jQuery('#return-location-select').on('change', function(e) {
      self.validateSubmission();
    });
		if (orderdate !== '') {
			jQuery('#order-date').val(df.toFrontEndDate(orderdate));
		}
		if (shipdate !== '') {
			jQuery('#ship-date').val(df.toFrontEndDate(shipdate));
		}
		jQuery('#return-date').datepicker({
		  format: "dd/mm/yyyy"
		});
		jQuery('#return-date').val(df.toFrontEndNow());

    jQuery('#btn-confirm-return').lazyLoadingButton({
        ajax: '<?php echo base_url().'returns/api/orderReturnSubmit';?>',
        data: function() {
            var sendData = {
                "ToLocationID" : jQuery('#return-location-select').val(),
                "OrderID" : "<?php echo $generalInfo->OrderID?>"
            };  
            return sendData;
        },
        callback : function(err, value) {
            if (err) { 
                alert(err.jqXHR.responseText); 
            } else {  
              alert(value);
              jQuery('#btn-confirm-return').attr('disabled',true);
                self.reloadDatatable();
            }
        }, 
        validation: function() {
            return self.validateSubmission();
        },
        loadingText : 'Saving...'
    });
		initializeDatatable();
	}

	self.reloadDatatable = function() {
      self.dataTable.ajax.url(self.orderdetailsUrl).load(); 
	};

	function initializeDatatable() {
		self.orderdetailsUrl = '<?php echo base_url()."returns/api/getReturnItemsDt"; ?>';
		self.dataTable = jQuery('#return-products').DataTable({
			"serverside": true,
      "ajax": {
        "url": self.orderdetailsUrl,
        "data": function(d) {
          d.OrderID = "<?php echo $generalInfo->OrderID?>";
          d.selectedStation = function() {
            return window.cs;
          };
          return JSON.stringify(d);
        },
        "contentType": "application/json",
        "dataType": "json",
        "method": "POST"
      },
			"bFilter": false, "bInfo": false, "bPaginate" : false,
			"aoColumnDefs": 
			[ 
				{ "aTargets": [ 0 ], "sClass" : "hide_column", "data":"HasBarcodeID"}, 
				{ "aTargets": [ 1 ], "data":"ProductName"}, 
				{ "aTargets": [ 2 ], "data":"AlreadyShipped"},
				{  
					"aTargets": [ 3 ],
					"sWidth":"150px",
					"data":"Barcode",
				 	"mRender": function ( data, type, full ) {
				 	// If HasBarcodeId == 1 => enable editing barcode, disable quantity, else, blah!  
						return data;
				  	}
				},
				{  
					"aTargets": [ 4 ],
					"sWidth":"150px",
					"data":"TempReturnQuantity",
				 	"mRender": function ( data, type, full ) { 
						return data;
			  	},
					"fnCreatedCell" : function( nTd, sData, oData, iRow, iCol ) { 
						jQuery(nTd).prop("id",oData['OrderDetailID']);  
						jQuery(nTd).addClass("editable-quantity");  
					}
				}
        /*,
				{  
					"aTargets": [ 5 ],
					"sWidth":"240px",
					"data":"OrderDetailID",
				 	"mRender": function ( data, type, full ) { 
						return "<a class='btn btn-default return-all-action'>Return all</a>" + 
									 " <a class='btn btn-default reset-return-action'>Reset returns</a>";
				  	}
				} */
			],
			"fnDrawCallback": function( oSettings ) { 
				jQuery(this).find('.editable-quantity').editable("<?php echo base_url(); ?>returns/jedapi/updateTempReturnQuantity", {  
          "cancel": '<button class="btn btn-danger" type="cancel" >Cancel</button>',
          "submit": '<button class="btn btn-success" type="submit" >Ok</button>',
          "indicator" : "<img src='<?php echo base_url().'assets/img/indicator.gif';?>' height='16'/>",
          "onerror": function(settings, original, xhr) { 
              try {
                var v = JSON.parse(xhr.responseText); 
                if (v.message) {
                  alert(v.message);
                }
              } catch (e) {

              }
              self.reloadDatatable();
              return "";
          },
          "callback": function(sValue, y) { 
            self.reloadDatatable();
          },
          "onsubmit": function(settings, y) {
            var input = jQuery(this).find('input');
            jQuery(this).validate({
              rules: {
                'value': {
                  number: true
                }
              },
              messages: {
                'actionItemEntity.name': {
                  number: 'Only numbers are allowed'
                }
              }
            });

            return jQuery(this).valid();
          }
        });
			}
	  });
	};  
}

var vm = new ReturnHireVM();
vm.bind();

</script> 