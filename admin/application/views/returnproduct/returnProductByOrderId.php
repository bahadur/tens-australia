<div class="row">
                <!--***********************************************************************-->
                <!-- contents for every page starts here -->
                <div class="row">
                    <div class="col-lg-12">&nbsp;</div>

                </div>                
				<div class="row"> 
					<div class="col-md-12">
						<div class="panel panel-primary" >
							<div class="panel-heading">
								<div class="panel-title">
									Enter the order id
								</div>
							</div>
												
							<div class="panel-body">
								<form class = "form" method = "post" action="<?php echo base_url();?>returnProduct/returnHiredProductByOrderID">
									<div class="col-md-4">
										<div class="form-group">
											<label class="control-label col-sm-4 pull-left">Order ID: </label>
												<div class="col-sm-8 pull-left">
												    <input type = "hidden" class = "form-control" id="form_check" value="3" name="form_check" placeholder = "" >
													<input type = "text" class = "form-control" id= "returnProductOrderID" name = "returnProductOrderID" placeholder = "Enter order ID...." required> 
				                    <?php 
				                    	if (isset($alertOnReturnProductBarcode)) 
				                    		echo "<p class='error-message'>$alertOnReturnProductBarcode</p>"; 
				                    ?>
												</div>
												
												                
										</div>
									</div>
										<button type = "submit" onclick="validationErrorMessage();" class="btn btn-primary btn-sm" style="margin-left:60px;">Return</button>
								</form>
							</div>
						</div>
					</div>
                </div> 
        </div>      
             
<script>
function validationErrorMessage(){
	var returnProductBarCode = document.getElementById('returnProductBarCode').value;
	var form_check = document.getElementById('form_check').value;
	if(returnProductBarCode == "" && form_check ==3){
		
		alert("Please Scan or Enter a Product Barcode");
	
	}


}
 

</script>

										

