        <div class="col-lg-9">
                <!--***********************************************************************-->
                <!-- contents for every page starts here -->
                <div class="row">
                    <div class="col-lg-12">&nbsp;</div>
                </div>                 
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-lg-12">&nbsp;</div>
                        <span style="color:#003399;font-weight:bold;font-size: medium">Class List</span>
                        <div class="col-lg-12">&nbsp;</div>
                        <button id="addRow">Add new row</button><br/>
                        <table id="main-table" class="display" cellspacing="0" width="100%">
                          <thead>
                              <tr>
                                  <th>Name</th>
                                  <th>Position</th>
                                  <th>Office</th>
                                  <th>Extn.</th>
                                  <th>Start date</th>
                                  <th>Salary</th>
                              </tr>
                          </thead>
                   
                          <tfoot>
                              <tr>
                                  <th>Name</th>
                                  <th>Position</th>
                                  <th>Office</th>
                                  <th>Extn.</th>
                                  <th>Start date</th>
                                  <th>Salary</th>
                              </tr>
                          </tfoot>
                      </table>
                        
                    </div>
                </div>
            <!-- contents for every page ends here -->
            <!--***********************************************************************-->
        </div>      
            

<!-- add the below lines on every content pages -->            
        </div>
    </div>
        <div class="col-lg-1"></div>
    </div><!-- end of row in navbar view -->
</div><!-- end of container in navbar view -->
<script type="text/javascript">

$(document).ready(function ($) {

    
    var data = [["1", "2", "3", "4", "5", "6"],["1", "2", "3", "4", "5", "6"],["1", "2", "3", "4", "5", "6"],["1", "2", "3", "4", "5", "6"]]; 
    var class_table = $('#main-table').dataTable({
        "scrollY": "1000px",
        "scrollX": "1000px",
        "scrollCollapse": true,
        "paging": false, 
        //"data": data, 
        "ajax" :"<?= base_url().'orders/getProductsInKitArray/1/9-11-2015/5/6'?>",
        "columnDefs": [ {
            "targets": 1,
            data: "productId",
            "defaultContent": "<select><option>12323</option></select>",
            render: function ( data, type, row ) {
                console.log(data);
                return '<input type="checkbox" class="editor-active" checked="checked" disabled>' + data + '</input>';  
            }
        } 
        ],
        sDom: "t"
    }).makeEditable({ 
        sUpdateURL: "", 
        sSuccessResponse: "IGNORE" ,
                "aoColumns": [
                        {
                                type: 'textarea',
                                submit: 'Save changes'
                            },
                        {
                        },
                        {
                                type: 'textarea',
                                submit: 'Save changes'
                        },
                        {},
                        {
                                type: 'select',
                                data: ["1", "2", "3", "4", "5", "6"]
                        },
                        {} 
                ]
    }); 
    var counter = 0;
    var api = $('#main-table').DataTable();
    // Get selected kit, and add it to the row. Easy : )
        api.row.add(["2", "2", "3", "4", "5", "6"]).draw();
        api.row.add(["3", "2", "3", "4", "5", "6"]).draw();
        api.row.add(["4", "2", "3", "4", "5", "6"]).draw();
    $('#addRow').on( 'click', function () { 
            api.row.add( [
                counter +'.1',
                counter +'.2',
                counter +'.3',
                counter +'.4',
                counter +'.5',
                counter +'.6'
            ] ).draw();
            class_table.makeEditable({ 
                sUpdateURL: "http://google.com", 
                sSuccessResponse: "IGNORE" ,
                "aoColumns": [
                        {},
                        {
                        },
                        {
                                type: 'textarea',
                                submit: 'Save changes'
                        },
                        {},
                        {
                                type: 'select'
                        },
                        {}
                ]
            });
            counter++;
        } );

});

</script>