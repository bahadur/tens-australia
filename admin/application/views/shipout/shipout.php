<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-title">
                    Ship Out
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped tableStyle" id="orders-table">
                    <thead>
                        <tr>
                            <th>Required Date</th>
                            <th>Order ID</th> 
                            <th>Customer</th>   
                            <th>Ship Method</th>
                            <th>Paid</th>
                            <th>Pick Order</th> 
                            <th>Days to Due</th> 
                        </tr>
                    </thead> 
                    <tbody>
                     
                        <?php foreach ($ordersNotShipped as $order) { ?>
                        <tr>
                            <td><?php echo $order->RequiredDate; ?></td>
                            <td><?php echo $order->OrderID; ?></td>
                            <td><?php echo $order->Customer; ?></td>
                            <td><?php echo $order->ShipMethod; ?></td>
                            <td><?php echo $order->Paid; ?></td>
                            <td><?php echo $order->OrderID; ?></td>
                            <td><?php echo $order->DaysToDueDate; ?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                    
                </table>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="modal-not-fully-paid" data-backdrop="static" aria-hidden="true" style="display: none;"> 
    <div class="modal-dialog"> 
        <div class="modal-content"> 
            <div class="modal-header"> 
                <h4 class="modal-title">Warning</h4> 
            </div> 
            <div class="modal-body">
                <div class="panel panel-primary">
                    <div class="panel-body">
                       <p>The order hasn't been paid in full. Are you sure you want to proceed with the fulfilment?</p>
                       <div class="col-md-4"></div>
                       <div class="col-md-4">
                        <button type="button" class="btn btn-success" id="btn-yes">Yes</button>
                        <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div> 
        </div> 
    </div> 
</div> 
</div>

<script type="<?php echo base_url().'assets/js/tensApi/tensDateFormatter.js';?>"></script>
<script type="text/javascript">

    function showAddNote()
    {
        jQuery('#modal-not-fully-paid').modal('show', {backdrop: 'static'}); 
    }
    
    var relocateurl = null;
    jQuery('#btn-yes').on('click', function() { 
        if (relocateurl != null)
        {
            window.location = relocateurl;
        }
    });

    var df = new DateTimeFormatter();

    jQuery(document).ready(function() {
        jQuery('#orders-table').DataTable( { 
            responsive: true, 
            columnDefs: [ 
            {
                'aTargets': [ 0 ],  
                'mRender': function(data, type, full)
                { 
                    if (type == 'display') {
                        if (data !== "") {
                            return df.toFrontEndDate(data);
                        } else {
                            return '';
                        }
                    } else {
                        return data;
                    }
                }
            },
            {
                'aTargets': [ 1 ], 
                'data':"orderId",
                'mRender': function(data, type, full)
                {
                    return "<a href='<?php echo base_url().'orders/vc/details/'?>" + data + "'>" + data + "</a>";
                }
            },
            {
                'aTargets': [ 4 ], 
                'data': 'fullypaid',
                'mRender': function(data, type, full)
                {
                    if (data == 1)
                    {
                        return 'Fully paid';
                    }
                    else 
                    {
                        return 'Not fully paid';
                    } 
                }

            },
            {
                'aTargets': [ 5 ], 
                'data':'pickOrderLink',
                'mRender': function(data, type, full)
                {
                    return "<a href='#'>Pick Order...</a>";
                },
                'fnCreatedCell' : function( nTd, sData, oData, iRow, iCol ) 
                {   
                    var url = '<?php echo base_url()."shipout/vc/orderFullfillment/"?>' + oData['pickOrderLink'] ;  
                    if (oData['fullypaid'] == 1)
                    { 
                        jQuery(nTd).on('click', function() {
                            window.location = url;
                        })
                    }
                    else 
                    {
                        jQuery(nTd).unbind();
                        jQuery(nTd).on('click', function() {
                            relocateurl = url;
                            jQuery('#modal-not-fully-paid').modal('show', {backdrop: 'static'}); 
                        })
                    }  
                }
            },
            ]
        } );
} );

</script>




