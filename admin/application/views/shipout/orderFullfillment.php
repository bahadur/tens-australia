 
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary"> 
			<div class="panel-body">
      <br/><br/>
        <div class="row">
          <form role="form" class="form-horizontal">
            <div class="col-md-5">
              <div class="form-group">
                <label class="col-md-4 control-label">Order ID: </label> 
                <div class="col-md-8 input-group">
                  <input type="text" name="OrderID" id="OrderID" value="<?php echo $orderFulfillment->OrderID?>"  class="form-control" readonly /> 
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-4 control-label">Order Date: </label> 
                <div class="col-md-8 input-group">
                  <input type="text" name="OrderDate" id="OrderDate" value=""  class="form-control" readonly /> 
                </div>
              </div>
              <div class="form-group"> 
                 <label class="col-md-4 control-label">Mobile Phone: </label> 
                <div class="col-md-8 input-group">
                  <input type="text" name="MobilePhone" id="MobilePhone" value="<?php echo $orderFulfillment->MobilePhone?>"  class="form-control" readonly /> 
                </div>
              </div>
              <div class="form-group"> 
                 <label class="col-md-4 control-label">Customer : </label> 
                <div class="col-md-8 input-group">
                  <input type="text" name="CustomerName" id="CustomerName" value="<?php echo html_escape($orderFulfillment->CustomerName); ?>"  class="form-control" readonly /> 
                </div>
              </div>
              <div class="form-group"> 
                 <label class="col-md-4 control-label">Business Phone: </label> 
                <div class="col-md-8 input-group">
                  <input type="text" name="BusinessPhone" id="BusinessPhone" value="<?php echo $orderFulfillment->BusinessPhone?>"  class="form-control" readonly /> 
   
                </div>
              </div>
            </div>


            <div class="col-md-5">
              <div class="form-group">
                <label class="col-md-4 control-label">Invoice Date: </label> 
                <div class="col-md-8 input-group">
              <input type="text" name="InvoiceDate" id="InvoiceDate" value=""  class="form-control" readonly /> 
                </div>
              </div>
              <div class="form-group"> 
                 <label class="col-md-4 control-label">Shipping Address: </label> 
                <div class="col-md-8 input-group">
                <input type="text" name="Address" id="Address" value="<?php echo $orderFulfillment->Address?>"  class="form-control" readonly /> 
                </div>
              </div>
              <div class="form-group"> 
                 <label class="col-md-4 control-label">Home Phone: </label> 
                <div class="col-md-8 input-group">
                <input type="text" name="HomePhone" id="HomePhone" value="<?php echo $orderFulfillment->HomePhone?>"  class="form-control" readonly /> 
                </div>
              </div>
              <div class="form-group"> 
                 <label class="col-md-4 control-label">Email: </label> 
                <div class="col-md-8 input-group">
                  <input type="text" name="Email" id="Email" value="<?php echo $orderFulfillment->Email?>"  class="form-control" readonly /> 
                </div>
              </div>

              <div class="form-group">
                 <label class="col-md-4 control-label">Pick Location: </label>
                   <div class="col-md-8 input-group"> 
                      <select name="pick-location" class="form-control"  id="pick-location"  autocomplete="off">
                       <?php 
                        if (!isset($DefaultPickLocationID)) {
                          echo '<option value="-99" disabled selected>Select a location</option>';
                        }
                         foreach ($locations as $rows) {
                           if (isset($DefaultPickLocationID) && $DefaultPickLocationID == $rows->LocationID) {
                              echo "<option value='".$rows->LocationID."' selected='selected' >".$rows->LocationDescription."</option>";
                           } else {
                              echo "<option value='".$rows->LocationID."'>".$rows->LocationDescription."</option>";
                           }
                         } 
                       ?>
                     </select> <br/>
                     <label id="location-error" class="error-message" style="display:none">This field is required.</label>
                 </div>
              </div>

            </div>
 

          </form>  
        <form method="post" action="<?php echo base_url().'orders/test' ?>" id="form-order-fulfilment">  
            <div class="col-md-offset-1 col-md-9">
              <h4>Order Items</h4>
                <table class="table table-bordered tableStyle" id="orders-table" name="lines[]" >
                 <thead>
                  <tr>
                   <th class="hide_column">HasBarcodeID</th>
                   <th class="hide_column">ProductID</th>
                   <th>Product</th>
                   <th>Hire</th>
                   <th>Order Qty</th>
                   <th>Qty supplied</th>
                   <th>Qty Required</th>
                   <th>Barcode</th>
                   <th>Picked</th>
                   <th>Pick Qty</th>
                   <th class="hide_column">OrderDetailID</th>
                 </tr>
               </thead> 
               <tbody>
               </tbody>
             </tbody>
           </table>
         </div> 

          <div class="row">
            <div class="col-md-6 col-md-offset-1">
              <h4>Comments: </h4> 
              <textarea name="comments" class="form-control" id="comments"></textarea>
            </div> 
          </div>
          <br/>
          <br/>
          <div class="row">
            <div class="form-group">
             <div class="col-md-offset-1 col-md-3">
                <button type="button" class="col-md-10 btn btn-success" id="fullfil-order-btn"> Save </button> 
              </div>
              <div class="col-md-8 ">
               <a href="<?=base_url().'reports/pdfGeneration/previewDeliveryDocket/'.$orderFulfillment->OrderID?>" target="_blank" class="btn btn-primary btn-after-submit" disabled>Preview Delivery Docket</a>
               <a href="<?=base_url().'reports/pdfGeneration/previewShippingLabel/'.$orderFulfillment->OrderID?>" target="_blank" class="btn btn-primary btn-after-submit" disabled>Preview Shipping Label</a>
               <a href="<?=base_url().'reports/pdfGeneration/previewInvoiceLabel/'.$orderFulfillment->OrderID?>" target="_blank" type="button" class="btn btn-primary btn-after-submit" disabled>Preview Invoice Label</a>
               <a href="<?=base_url().'reports/pdfGeneration/previewInvoice/'.$orderFulfillment->OrderID?>" target="_blank" type="button" class="btn btn-primary btn-after-submit" disabled>Preview Tax Invoice</a>
               <a href="<?php echo base_url().'reports/pdfGeneration/emailInvoice/1/'.$orderFulfillment->OrderID.'/'.$orderFulfillment->CustomerID;?>" target="_blank" type="button" class="btn btn-primary btn-after-submit" disabled>Email Tax Invoice to Customer</a>
             </div>
            </div>
          </div> 
      </form>  
</div>
</div>
</div>
</div>

<div class="modal fade" id="modal-not-fully-picked" data-backdrop="static" aria-hidden="true" style="display: none;"> 
    <div class="modal-dialog"> 
      <div class="modal-content"> 
        <div class="modal-header"> 
          <h4 class="modal-title">Warning</h4> 
        </div> 
        <div class="modal-body">
          <div class="panel panel-primary">
            <div class="panel-body">
             <p>There is a product where the "Quantity Required" does not match the "Pick Quantity".</p>
             <p>Are you sure you want to continue?</p>
             <div class="col-md-4"></div>
             <div class="col-md-4">
              <button type="button" class="btn btn-success" id="btn-submit-incomplete">Yes</button>
              <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
            </div>
          </div>
        </div> 
      </div> 
    </div> 
  </div> 
</div>

<link rel='stylesheet' href="<?php echo base_url().'assets/css/styles.css'; ?>" />
<script type="text/javascript" src="<?= base_url().'assets/jquery.dataTables.editable.js'?>"></script> 
<script type="text/javascript" src="<?= base_url().'assets/jeditable.checkbox.js'?>"></script>  
<script type="text/javascript" src="<?= base_url().'assets/js/tensApi/tensDatesFormatter.js'?>"></script>  
<script type="text/javascript" src="<?= base_url().'assets/js/tensApi/lazyLoadingButton.js'?>"></script> 
<script type="text/javascript">
function OrderFulfillmentVM() {
  var self = this;
  self.df = new DateTimeFormatter();
 

  function validateLocation() { 
    if (jQuery("#pick-location").val() > 0) {
      jQuery("#pick-location").removeClass('select-error');
      jQuery('#location-error').hide();
      return true;
    } else { 
      jQuery("#pick-location").addClass('select-error');
      jQuery('#location-error').show();
      return false;
    }
  }
  /**
   * Initialize a datatable with editable fields.
   * If a product has a barcode (hasBarcode = 1), then barcode cell is enabled, and the quantity cell is disabled
   * If a product doesn't have a barcode, then enable quantity cell editing and disable barcode cell editing
   * @return {[type]} [description]
   */
  function initializeDatatable() {
    self.orderdetailsUrl = '<?php echo base_url();?>shipout/api/getOrderDetails';
    self.datatableFulfillment = jQuery("#orders-table").DataTable({
      "serverside": true,
      "ajax": {
        "url": self.orderdetailsUrl,
        "data": function(d) {
          d.OrderID = "<?php echo $orderFulfillment->OrderID?>";
          d.selectedStation = function() {
            return window.cs;
          };
          return JSON.stringify(d);
        },
        "contentType": "application/json",
        "dataType": "json",
        "method": "POST"
      },
      "aoColumnDefs": [{
        "aTargets": [0],
        "sClass": "hide_column",
        "data": "HasBarcodeID"
      }, {
        "aTargets": [1],
        "sClass": "hide_column",
        "data": "ProductID"
      }, {
        "aTargets": [2],
        "data": "ProductName"
      }, {
        "aTargets": [3],
        "data": "Hire",
        "mRender": function(data, type, full) {
          if (type === 'display') {
            if (data == "1") {
              return '<input type="checkbox" checked disabled/>';
            } else {
              return '<input type="checkbox" disabled/>';
            }
          } else {
            return data;
          }
        }
      }, {
        "aTargets": [4],
        "data": "Quantity"
      }, {
        "aTargets": [5],
        "data": "AlreadyShipped"
      }, {
        "aTargets": [6],
        "data": "Quantity",
        "mRender": function(data, type, full) {
          if (type === 'display') {
            return full['Quantity'] * 1 - full['AlreadyShipped'] * 1;
          } else {
            return data;
          }
        },
      }, {
        "aTargets": [7],
        "data": "TempBarcode",
        "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
          jQuery(nTd).prop("id", iRow + "-" + iCol);
          if (oData['HasBarcodeID'] == "1") {
            jQuery(nTd).addClass("editable-barcode");
          } else if (oData['HasBarcodeID'] == "2") {
            jQuery(nTd).addClass("editable-noninv-barcode");
          }
        }
      }, {
        "aTargets": [8],
        "data": "TempPicked",
        "mRender": function(data, type, full) {
          if (type == 'display') {
            var disabledClass = '';
            if (full['HasBarcodeID'] == "1" || full['HasBarcodeID'] == "2") {
              disabledClass = 'disabled';
            } else {
              disabledClass = '';
            }
            if (data == "1") {
              return '<input type="checkbox" checked ' + disabledClass + '/>';
            } else {
              return '<input type="checkbox" ' + disabledClass + '/>';
            }
          } else {
            return data;
          }
        },
        "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
          jQuery(nTd).prop("id", iRow + "-" + iCol);
          if (oData['HasBarcodeID'] != "1" && oData['HasBarcodeID'] != "2") {
            jQuery(nTd).addClass("editable-pick");
          }
        }
      }, {
        "aTargets": [9],
        "data": "TempPickQuantity", 
        "mRender": function(data, type, full) {
          return data;
        },
        "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) { 
          if (oData['TempPicked'] == "1" && oData['HasBarcodeID'] != "1" && oData['HasBarcodeID'] != "2") {
            jQuery(nTd).addClass("editable-pick-qty");
          }
        }
      }, {
        "aTargets": [10],
        "sClass": "hide_column",
        "data": "OrderDetailID"
      }],
      "fnDrawCallback": function( oSettings ) {   
        jQuery(this).find('.editable-pick-qty').editable("<?php echo base_url(); ?>shipout/api/updateTempQuantity", {  
          "cancel": '<button class="btn btn-danger" type="cancel" >Cancel</button>',
          "submit": '<button class="btn btn-success" type="submit" >Ok</button>',
          "indicator" : "<img src='<?php echo base_url().'assets/img/indicator.gif';?>' height='16'/>",
          "onerror": function(settings, original, xhr) { 
              try {
                var v = JSON.parse(xhr.responseText); 
                if (v.message) {
                  alert(v.message);
                }
              } catch (e) {

              }
              self.reloadDatatable();
              return "";
          },
          "callback": function(sValue, y) {
            var aPos = self.datatableFulfillment.fnGetPosition(this);
            self.datatableFulfillment.fnUpdate(sValue, aPos[0], aPos[1]);
          },
          "onsubmit": function(settings, y) {
            var input = jQuery(this).find('input');
            jQuery(this).validate({
              rules: {
                'value': {
                  number: true
                }
              },
              messages: {
                'actionItemEntity.name': {
                  number: 'Only numbers are allowed'
                }
              }
            });

            return jQuery(this).valid();
          },
          "submitdata": function(value, settings) {
            var data = {
              "OrderDetailID": jQuery(this).parent().find('td:eq(10)').text()
            };
            return data;
          }
        });

        jQuery(this).find('.editable-pick').editable(
          "<?php echo base_url().'shipout/api/updateTempPicked'?>",
           {
            "indicator" : "<img src='<?php echo base_url().'assets/img/indicator.gif';?>' height='16'/>",
            "cancel": '<button class="btn btn-danger" type="cancel" >Cancel</button>',
            "submit": '<br/><button class="btn btn-success" type="submit" >Ok</button>',
            "type" :'checkbox', 
            "onerror": function(settings, original, xhr) {
              try {
                var v = JSON.parse(xhr.responseText); 
                if (v.message) {
                  alert(v.message);
                }
              } catch (e) {

              }
              self.reloadDatatable();
              return "";
            },
            "callback": function( sValue, y )  {  
               self.reloadDatatable();
               return ""; 
             },
             "submitdata": function (value, settings) 
             {   
               var data = { 
                "OrderDetailID": jQuery(this).parent().find('td:eq(10)').text()
              }; 
              return data;
            },
          });


          jQuery(this).find('.editable-noninv-barcode').editable(
            "<?php echo base_url().'shipout/formapi/updateTempNoninvBarcode'?>", {
              "indicator" : "<img src='<?php echo base_url().'assets/img/indicator.gif';?>' height='16'/>",
              "cancel": '<button class="btn btn-danger" type="cancel" >Cancel</button>',
              "submit": '<br/><button class="btn btn-success" type="submit" >Ok</button>',
              "type": 'text',
              "method": "POST", 
              "onerror": function(settings, original, xhr) {
                try {
                  var response = JSON.parse(xhr.responseText);
                  if (response.message) {
                    alert(response.message);
                  } 
                } catch(e) {
                }
                self.reloadDatatable();
              },
              "callback": function(sValue, y) {  
                self.reloadDatatable();
              },
              "submitdata": function(value, settings) { 
                var data = {
                  "OrderDetailID": jQuery(this).parent().find('td:eq(10)').text(),
                  "LocationID" : jQuery("#pick-location").val()
                };
                return data;
              }, 
              "onsubmit" : function(settings, original) {  
                return validateLocation();
              },
              "ajaxoptions": {
                "type": "POST"
              }
            }
          );

          jQuery(this).find('.editable-barcode').editable(
            "<?php echo base_url().'shipout/api/updateTempBarcode'?>", {
              "indicator" : "<img src='<?php echo base_url().'assets/img/indicator.gif';?>' height='16'/>",
              "cancel": '<button class="btn btn-danger" type="cancel" >Cancel</button>',
              "submit": '<br/><button class="btn btn-success" type="submit" >Ok</button>',
              "type": 'text',
              "method": "POST", 
              "onerror": function(settings, original, xhr) {
                try {
                  var response = JSON.parse(xhr.responseText);
                  if (response.message) {
                    alert(response.message);
                  } 
                } catch(e) {
                }
                self.reloadDatatable();
              },
              "callback": function(sValue, y) {  
                self.reloadDatatable();
              },
              "submitdata": function(value, settings) { 
                var data = {
                  "OrderDetailID": jQuery(this).parent().find('td:eq(10)').text(),
                  "LocationID" : jQuery("#pick-location").val()
                };
                return data;
              }, 
              "onsubmit" : function(settings, original) {  
                return validateLocation();
              },
              "ajaxoptions": {
                "contentType": "application/json",
                "dataType": "json"
              }
            }
          );
      }
    });
  };

  self.reloadDatatable = function() { 
      self.datatableFulfillment.ajax.url(self.orderdetailsUrl).load(); 
  };

  self.setAfterShipState = function() {
    jQuery('#fullfil-order-btn').attr('disabled',true);
    jQuery('.btn-after-submit').removeAttr('disabled');
  };

  function submitOrderFulfilment() {
    jQuery('#fullfil-order-btn').trigger('click');
  }


  function bindButtons() {

    jQuery("#btn-submit-incomplete").on("click", function() {
      submitOrderFulfilment();
      jQuery('#modal-not-fully-picked').modal('toggle');
    });

    jQuery("#fullfil-order-btn").lazyLoadingButton({
      "ajax": "<?php echo base_url().'shipout/api/fulfillOrder';?>",
      "data": function() {
        return {
          "OrderID": "<?php echo $orderFulfillment->OrderID; ?>",
          "FromLocationID" : jQuery('#pick-location').val(),
          "Comments" : jQuery('#comments').val()
        };
      },
      "callback": function(err, value) {
        if (err) {
          alert(err.jqXHR.responseText);
        } else {
          self.reloadDatatable();
          self.setAfterShipState();
        }
      },
      "validation" : function() {
        return validateLocation();
      }, 
      "loadingText": 'Saving...'
    });

  };

  function fillOrderDates() {
    var orderDate = '<?php echo $orderFulfillment->OrderDate; ?>';
    var invoiceDate = '<?php echo $orderFulfillment->InvoiceDate; ?>';
    if (orderDate && orderDate.length > 0) {
      jQuery('#OrderDate').val(self.df.toFrontEndDate(orderDate));
    }
    if (invoiceDate && invoiceDate.length > 0) {
      jQuery('#InvoiceDate').val(self.df.toFrontEndDate(invoiceDate));
    }
  };

  self.bind = function() {
    initializeDatatable();
    bindButtons();
    fillOrderDates();

    jQuery('#pick-location').on('change', function(e) { 
      validateLocation();
    });
  };
}
var vm = new OrderFulfillmentVM();
vm.bind();

</script>

