<div class="col-md-12"> 
        <div class="row">
            <div class="col-md-12">
                <h2></h2>  
                <p> <label>Order ID: </label> <?php echo $shipoutinfo->OrderID;?></p> 
                <p> <label>Order Date: </label> <?php echo $shipoutinfo->OrderDate;?></p> 
                <p> <label>Customer: </label> <?php echo $shipoutinfo->CustomerName;?></p> 
                <p> <label>Shipping Address: </label> <?php echo $shipoutinfo->Address;?></p> 
                <p> <label>Home Phone: </label> <?php echo $shipoutinfo->HomePhone;?></p> 
                <p> <label>Mobile Phone: </label> <?php echo $shipoutinfo->MobilePhone;?></p> 
                <p> <label>Business Phone: </label> <?php echo $shipoutinfo->BusinessPhone;?></p> 
                <p> <label>Email: </label> <?php echo $shipoutinfo->Email;?></p> 
                <p> <label>Pick Location: </label> 
                    <select>
                        <?php 
                            foreach ($locations as $location)
                            {
                                echo '<option value="'.$location->LocationID.'">'.$location->LocationDescription.'</option>';
                            }
                        ?>
                    </select> </p>
                <p> <label>Invoice Date: </label> <?php echo $shipoutinfo->InvoiceDate;?></p> 
            </div>
        </div>
        <div class="row">
            <table class="table table-striped tableStyle" id="products-table">
                <thead>
                    <tr>
                        <th>Product</th>
                        <th>Hire</th> 
                        <th>Order Qty</th> 
                        <th>Qty Required</th>
                        <th>Barcode</th>
                        <th>Picked</th> 
                        <th>Pick Qty</th> 
                    </tr>
                </thead> 
                <tbody> 
                <?php
                    $baseUrl = $this->config->config['base_url']; 
                    foreach ($productsFulfilled as $product)
                    {     
                        $qtyRequired = $product->Quantity - $product->AlreadyShipped;
                        echo '<tr>';
                        echo '<td>'.$product->ProductName.'</td>'; 
                        echo '<td>'.$product->Hire.'</td>'; 
                        echo '<td>'.$product->AlreadyShipped.'</td>'; 
                        echo '<td>'.$qtyRequired.'</td>'; 
                        echo '<td>'.$product->TempBarcode.'</td>'; 
                        echo '<td>'.$product->TempPicked.'</td>'; 
                        echo '<td>'.$product->TempPickQuantity.'</td>';  
                        echo '</tr>';
                    }
                ?>
                </tbody>
            </table>
        </div>
        <div class="row">
            <p><label>Comments</label> <textarea></textarea></p>
            <p>
            <button type="button" class="btn btn-primary" area-label="Left Align" onclick="filterOrdersByArticleId()">
                Save the order
            </button>
            <button type="button" class="btn btn-primary" area-label="Left Align" onclick="filterOrdersByArticleId()">
                Cancel
            </button></p>
            <p>
            <button type="button" class="btn btn-primary" area-label="Left Align" onclick="filterOrdersByArticleId()">
                Preview delivery docket
            </button>
            <button type="button" class="btn btn-primary" area-label="Left Align" onclick="filterOrdersByArticleId()">
                Preview shipping label
            </button></p>
            <p>
            <button type="button" class="btn btn-primary" area-label="Left Align" onclick="filterOrdersByArticleId()">
                Preview invoice label
            </button>
            <button type="button" class="btn btn-primary" area-label="Left Align" onclick="filterOrdersByArticleId()">
                Preview tax invoice
            </button>
            <button type="button" class="btn btn-primary" area-label="Left Align" onclick="filterOrdersByArticleId()">
                Email tax invoice to customer
            </button></p>
        </div>  
</div>       






<script type="text/javascript">
    jQuery("#products-table").dataTable({
        bPaginate: false,
        bFilter: false
    });
</script>