<div class="row">
    <div class="col-md-12">
        
        <ul class="nav nav-tabs bordered">
            <li class="active">
                <a href="#locationtypes"  data-toggle="tab">
                <span class="visible-xs"><i class="entypo-tag"></i></span>
                <span class="hidden-xs">Order Sources</span>
                </a>

            </li>
            <li>
                <a href="#add-locationtype"  data-toggle="tab">
                <span class="visible-xs"><i class="entypo-plus-squared"></i></span>
                <span class="hidden-xs">Add</span>
                </a>
                
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="locationtypes">
                <table id="locationtype-table" class="table    " cellspacing="0" >
                    <thead>
                                   
                        <tr>
                            
                            <th>Location Type ID</th>
                            <th>Location Type</th>
                                

                            <th></th>

                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                    foreach ($locationtypes as $row){
                        echo "<tr>";
                            echo "<td><a href='".base_url()."classes/detail/".$row->LocationTypeID."' target='_blank' >".$row->LocationTypeID."</a></td>";
                            echo "<td>".$row->LocationType."</td>";
                            echo "<td>
                                <div class='btn-group pull-right'> 
                                    <button type='button' class='btn btn-default btn-sm dropdown-toggle' data-toggle='dropdown'>
                                        Action <span class='caret'></span> 
                                    </button>
                                    <ul class='dropdown-menu dropdown-default pull-right' role='menu'> 
                                        <li> <a href='#' onclick='showEditModal(".$row->LocationTypeID.")'> <i class='entypo-pencil'></i> Edit </a> </li> 
                                        <li class='divider'></li> 
                                        <li> <a href='#' onclick='showDeleteModal(".$row->LocationTypeID.")'> <i class='entypo-trash'></i> Delete </a> </li>' 
                                    </ul>
                                </div>
                            </td>";
                        echo "</tr>";
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <div class="tab-pane" id="add-locationtype">
                <form class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url()?>administrator/location_types" id="form-order-accountcodes">
            

                    
                   

                    
                     <div class="form-group">
                        <label class="col-sm-3 control-label">Location Type:</label>
                        <div class="col-sm-5">
                            <input type="text"  name="LocationType" id="LocationType" class="form-control" />
                        </div>
                    </div>

                    


                

  
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" class="btn btn-primary" >Insert Reminder</button>
                            <input type="hidden" name="action" value="insert">
                        </div>
                    </div>
            

            
            
                </form>
            </div>
        </div>

        
    </div>
</div>


<!-- Modal Edit -->
 <div class="modal fade" id="modal-edit" data-backdrop="static" aria-hidden="true" style="display: none;"> 
                <div class="modal-dialog"> 
                    <div class="modal-content"> 
                        <div class="modal-header"> 
                            <h4 class="modal-title">Confirm Modal</h4> 
                        </div> 
                        <div class="modal-body">
                            <div class="row">  </div>
                        </div>
                        <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>  
                    </div> 
                </div> 
</div>

<div class="modal fade" id="modal-delete" data-backdrop="static" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content" style="margin-top:100px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" style="text-align:center;">Are you sure to delete this information ?</h4>
            </div>

            <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">


jQuery(document).ready(function ($) {

   

    var locationtype_table = $('#locationtype-table').DataTable({ 
       
        
        //"paging": false,
        //sDom: "lrtip"
    });


   


});

function showEditModal(LocationTypeID)

{ 
    var priceGroupId = jQuery("#price-group-select").val();
    var stringUrl = "<?=  base_url().'modals/locationTypeEdit/' ?>" + LocationTypeID ; 
    //console.log("string url : " + stringUrl);
    jQuery('#modal-edit').find('.modal-body').html("<div class='row'><image src='<?= base_url().'assets/img/indicator.gif' ?>'/></div>");
    jQuery.ajax({       
        type: 'get', 
        url: stringUrl,
        success: function(data)
        {  
            jQuery('#modal-edit').find('.modal-body').html(data); 
        },
        error :function(jqXHR, textStatus, errorThrown ) {

        }  
    });
    jQuery('#modal-edit').modal('show', {backdrop: 'static'});
} 


function showDeleteModal(LocationTypeID)
{ 
   var stringUrl = "<?= base_url().'administrator/location_types'?>" ; 
    jQuery('#modal-delete').find('.modal-body').html("<div class='row'><image src='<?= base_url().'assets/img/indicator.gif' ?>'/></div>");
    jQuery('#modal-delete').find('.modal-footer').html('<form action="'+stringUrl+'"  method="post" accept-charset="utf-8" class="form-horizontal form-groups-bordered validate" target="_top" enctype="multipart/form-data">'+
                    '<input name="orderDetailId" type="hidden" id="deleteOrderDetailId"/>'+
                    '<button type="submit" class="btn btn-danger" id="delete_link">delete</a>'+
                    '<button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>'+
                    '<input type="hidden" name="LocationTypeID" id="LocationTypeID" value="'+LocationTypeID+'" class="form-control" />'+
                    '<input type="hidden" name="action" value="delete">'+
                '</form>'); 
    jQuery('#modal-delete').modal('show', {backdrop: 'static'});
} 


</script>

