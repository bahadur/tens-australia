<form class="form-horizontal " id="form-orderid" method="post">
<div class="row">
	    <div class="col-md-6">
	        <div class="panel panel-default">
	  			<div class="panel-body">

			        <div class="form-group">
			        	<label class="col-sm-4 control-label">Order ID</label>
			        	<div class="col-sm-8">
			        		<input type="text" name="OrderID" id="OrderID" value="<?php echo $refundOrder->OrderID?>" value="<?php echo $refundOrder->OrderID?>" class="form-control" readonly />
			        	</div>
			        </div>

			        <div class="form-group">
			        	<label class="col-sm-4 control-label">Customer</label>
			        	<div class="col-sm-8">
			        		<input type="text" name="CustomerName" id="CustomerName" value="<?php echo $refundOrder->CustomerName?>" class="form-control" readonly />
			        	</div>
			        </div>

			        <div class="form-group">
			        	<label class="col-sm-4 control-label">Shipping Address</label>
			        	<div class="col-sm-8">
			        		
			        		<textarea name="Address" id="Address" class="form-control" readonly /><?php echo $refundOrder->Address?></textarea>
			        	</div>
			        </div>

			        <div class="form-group">
			        	<label class="col-sm-4 control-label">Home Phone</label>
			        	<div class="col-sm-8">
			        		<input type="text" name="HomePhone" id="HomePhone" value="<?php echo $refundOrder->HomePhone?>" class="form-control" readonly />
			        	</div>
			        </div>

			        <div class="form-group">
			        	<label class="col-sm-4 control-label">Mobile Phone</label>
			        	<div class="col-sm-8">
			        		<input type="text" name="MobilePhone" id="MobilePhone" value="<?php echo $refundOrder->MobilePhone?>" class="form-control" readonly />
			        	</div>
			        </div>

			        <div class="form-group">
			        	<label class="col-sm-4 control-label">Email</label>
			        	<div class="col-sm-8">
			        		<input type="text" name="Email" id="Email" value="<?php echo $refundOrder->Email?>" class="form-control" readonly />
			        	</div>
			        </div>

	        	</div>
	        </div>

	    </div>
	    <div class="col-md-6">
	        <div class="panel panel-default">
	  			<div class="panel-body">

			        <div class="form-group">
			        	<label class="col-sm-4 control-label">Order Date</label>
			        	<div class="col-sm-8">
			        		<input type="text" name="OrderDate" id="OrderDate" value="<?php echo $refundOrder->OrderDate?>" class="form-control" readonly />
			        	</div>
			        </div>

			        <div class="form-group">
			        	<label class="col-sm-4 control-label">Due Date</label>
			        	<div class="col-sm-8">
			        		<input type="text" name="DueDate" id="DueDate" value="<?php echo $refundOrder->DueDate?>" class="form-control" readonly />
			        	</div>
			        </div>

			        <div class="form-group">
			        	<label class="col-sm-4 control-label">Return Actual Date</label>
			        	<div class="col-sm-8">
			        		<input type="text" name="ReturnActualDate" id="ReturnActualDate" value="<?php echo $refundOrder->ReturnActualDate?>" class="form-control" readonly />
			        	</div>
			        </div>

	        	</div>
	        </div>

	    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered" id="refund-product-table">
			<thead>
				<tr>
					<th>Product</th>
					<th>Ship Qty</th>
					<th>Return Qty</th>
					<th>Return Date</th>
					<th>Total Paid</th>
					<th>Total Refunded</th> 
					<th>This Refund</th> 
					<th>Not Refunded</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				$totalRefund=0;
				foreach ($refundProducts as $row) { 
				$totalRefund += $row->TempTransactionAmount;
				?>
				<tr>
					<td><?php echo $row->ProductName?></td>
					<td><?php echo $row->ShippedQty?></td>
					<td><?php echo $row->ReturnQty?></td>
					<td><?php echo $row->ReturnActualDate?></td>
					<td>
						<div class="form-group has-success">
							<div class="input-group col-sm-offset-2">
								<span class="input-group-addon">$</span>
								<input type="text" value="<?php echo number_format($row->TotalPaid,2,".",",")?>" class="form-control  input-sm total-paid" style="width:50%"  readonly />
							</div>
						</div>
					</td>
					<td>
						<div class="form-group has-success">
							<div class="input-group col-sm-offset-2">
								<span class="input-group-addon">$</span>
								<input type="text" value="<?php echo number_format($row->TotalRefunded,2,".",",")?>" class="form-control input-sm total-refund" style="width:50%"  readonly />
							</div>
						</div>
					</td> 
					<td>
						<div class="form-group has-success">
							<div class="input-group col-sm-offset-2">
								<span class="input-group-addon">$</span>
								<input type="text" value="<?php echo number_format($row->TempTransactionAmount,2,".",",")?>" class="form-control input-sm this-refund" style="width:50%"  />
								<input type="hidden" value="<?php echo number_format($row->TempTransactionAmount,2,".",",")?>" class="form-control input-sm total-paid-hidden"  />
								<input type="hidden" value="<?php echo $row->orderdetailID?>" class="form-control input-sm orderDetailId"  />
								
							</div>
						</div>
					</td>
					<td>
						<div class="form-group has-success">
							<div class="input-group col-sm-offset-2">
								<span class="input-group-addon">$</span>
								<input type="text" value="<?php echo number_format(($row->TotalPaid - $row->TotalRefunded - $row->TempTransactionAmount),2,".",",")?>" class="form-control input-sm not-refund" style="width:50%" readonly />
							</div>
						</div>
						
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>

<div class="row">
	<div class="col-md-6">
		
		<div class="form-group">
			<label class="col-sm-4 control-label">Refund Method</label>
			<div class="col-sm-8">
				<select class="form-control" name="paymentTerminalID">
					<?php 
					$rs = $this->db->query("
						SELECT 
						`paymentTerminal`.`PaymentTerminalID`, 
						CONCAT(`paymentMethod`.`PaymentMethod` , ' - ' , `terminal`.`Terminal`) AS PaymentMethod, 
						`paymentTerminal`.`PaymentMethodID`, 
						`paymentTerminal`.`TerminalID`
						FROM (`paymentTerminal` INNER JOIN `paymentMethod` ON `paymentTerminal`.`PaymentMethodID` = `paymentMethod`.`PaymentMethodID`) INNER JOIN `terminal` ON `paymentTerminal`.`TerminalID` = `terminal`.`TerminalID`
						ORDER BY `paymentTerminal`.`TerminalID`, CONCAT(`paymentMethod`.`PaymentMethod` , ' - ' , `terminal`.`Terminal`)
						");
					foreach ($rs->result() as $row) { ?>
						<option value="<?php echo $row->PaymentTerminalID ?>"><?php echo $row->PaymentMethod ?></option>
					<? } ?>
				</select>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-4 control-label">Refund Date</label>
			<div class="col-sm-8">
				<input type="text" name="refundDate" id="refundDate" class="form-control" />
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-4 control-label">Reference No</label>
			<div class="col-sm-8">
				<input type="text" name="referenceNo" id="referenceNo" class="form-control" />
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-4 control-label">Comments</label>
			<div class="col-sm-8">
				<textarea  class="form-control" rows="3" name="comments"></textarea>
			</div>
		</div>

		

	</div>


	<div class="col-md-6">

		<div class="form-group">
			<label class="col-sm-4 control-label">This Refund Total</label>
			<div class="col-sm-8"> 
				<div class="input-group col-sm-offset-2">
					<span class="input-group-addon">$</span>
					<input type="text" name="thisRefundTotal" id="thisRefundTotal" class="form-control" value="<?php echo number_format($totalRefund,2,".",",")?>" readonly />
				</div>
			</div>
		</div>


		

	</div>

	

</div>

<div class="row">
			
	<div class="col-md-12">
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-3">
				<button type="submit" class="btn btn-primary">Confirm Refund</button>
				<input type="hidden" name="orderid" value="<?php echo $refundOrder->OrderID?>">
				<input type="hidden" name="action" value="confirm_refund">
			</div>
			
			<div class="col-sm-6">
				<button type="button" class="btn btn-success">Close</button>
			</div>
		</div>

			
	</div>
</div>

</form>


<script type="text/javascript">
jQuery(document).ready(function ($) {
   

   table_product = jQuery("#refund-product-table").DataTable();

   
   jQuery(".this-refund").focusout(function(){

   		var this_refund = parseFloat(jQuery(this).val());
   		var this_refund_hidden = parseFloat(jQuery(this).parent().parent().parent().parent().find('.total-paid-hidden').val());
	   	var total_paid = parseFloat(jQuery(this).parent().parent().parent().parent().find('.total-paid').val());

	   	var not_refund; 
	   	
	   	if(this_refund > total_paid ){
	   		alert("You cannot refund more then the amount originally paid.");
	   		jQuery(this).val(this_refund_hidden);
	   		jQuery(this).focus();
	   	} else {
	   		

	   		var orderDetailId = parseInt(jQuery(this).parent().find('.orderDetailId').val());
	   		
			not_refund = total_paid - this_refund;
			
			jQuery(this).parent().parent().parent().parent().find('.not-refund').val(not_refund);
	   		
	   		jQuery.ajax({
	   			url:'<?php echo base_url()?>administrator/refundByOrderID',
	   			type: 'POST',
	   			data: {"orderId":<?php echo $refundOrder->OrderID?>,"orderDetailId":orderDetailId,"this_refund":this_refund,"action": "this_refund"},
	   			success:function(d){

				    jQuery("#thisRefundTotal").val(d.totalRefund);
				    
	   			}
	   		});
	   }


  
   });



});
</script>