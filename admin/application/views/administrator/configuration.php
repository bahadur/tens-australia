<link rel="stylesheet" href="<?php echo base_url()?>assets/js/wysihtml5/bootstrap-wysihtml5.css">
<script src="<?php echo base_url()?>assets/js/wysihtml5/wysihtml5-0.4.0pre.min.js"></script>
<script src="<?php echo base_url()?>assets/js/wysihtml5/bootstrap-wysihtml5.js"></script>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			
			<div class="panel-heading">
				<div class="panel-title">
					Configuration
				</div>
			</div>
			<div class="panel-body">
				<form action="" id="form-configuration" method="post" accept-charset="utf-8" class="form-horizontal" target="_top">
					

					<div class="form-group">
						<label class="control-label col-sm-3 col-sm-3">Default Price Group ID:</label>
						<div class="col-sm-5">
							<input type="text" name="DefaultPriceGroupID" id="DefaultPriceGroupID" value="<?php echo $config->DefaultPriceGroupID?>" class="form-control" />
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-3">GST Percent:</label>
						<div class="col-sm-5">
							<input type="text" name="GST_Percent" id="GST_Percent" value="<?php echo $config->GST_Percent?>" class="form-control" />
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-3">Default Pick Location ID:</label>
						<div class="col-sm-5">
							<input type="text" name="DefaultPickLocationID" id="DefaultPickLocationID" value="<?php echo $config->DefaultPickLocationID?>" class="form-control" />
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-3">Default Hire Return Location ID:</label>
						<div class="col-sm-5">
							<input type="text" name="DefaultHireReturnLocationID" id="DefaultHireReturnLocationID" value="<?php echo $config->DefaultHireReturnLocationID?>" class="form-control" />
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-3">Search Right Chars In Barcode:</label>
						<div class="col-sm-5">
							<input type="text" name="SearchRightCharsInBarcode" id="SearchRightCharsInBarcode" value="<?php echo $config->SearchRightCharsInBarcode?>" class="form-control" />
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-3">Return Expected Due Plus Days:</label>
						<div class="col-sm-5">
							<input type="text" name="ReturnExpectedDuePlusDays" id="ReturnExpectedDuePlusDays" value="<?php echo $config->ReturnExpectedDuePlusDays?>" class="form-control" />
						</div>
					</div>

					<!--div class="form-group">
						<label class="control-label col-sm-3">Sending SMS:</label>
						<div class="col-sm-5">
							<input type="text" name="SendingSMS" id="SendingSMS" value="<?php echo $config->SendingSMS?>" class="form-control" />
						</div>
					</div -->

					<div class="form-group">
						<label class="control-label col-sm-3">Send SMS Time:</label>
						<div class="col-sm-5">
							<input type="text" name="SendSMS_Time" id="SendSMS_Time" value="<?php echo $config->SendSMS_Time?>" class="form-control" />
						</div>
					</div>

					<!--div class="form-group">
						<label class="control-label col-sm-3">SMS Sent Date:</label>
						<div class="col-sm-5">
							<input type="text" name="SMS_SentDate" id="SMS_SentDate" value="<?php echo $config->SMS_SentDate?>" class="form-control" />
						</div>
					</div -->

					<div class="form-group">
						<label class="control-label col-sm-3">Required Warning Due Minus Days:</label>
						<div class="col-sm-5">
							<input type="text" name="RequiredWarningDueMinusDays" id="RequiredWarningDueMinusDays" value="<?php echo $config->RequiredWarningDueMinusDays?>" class="form-control" />
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-3">Invoice Email Subject:</label>
						<div class="col-sm-5">
							<input type="text" name="InvoiceEmailSubject" id="InvoiceEmailSubject" value="<?php echo $config->InvoiceEmailSubject?>" class="form-control" />
						</div>
					</div>

					
					<!-- div class="form-group">
						<label class="control-label col-sm-3">Refund Email Subject:</label>
						<div class="col-sm-5">
							<input type="text" name="RefundEmailSubject" id="RefundEmailSubject" value="<?php echo $config->RefundEmailSubject?>" class="form-control" />
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-3">Refund Email Body:</label>
						<div class="col-sm-5">
							<input type="text" name="RefundEmailBody" id="RefundEmailBody" value="<?php echo $config->RefundEmailBody?>" class="form-control" />
						</div>
					</div -->

					<div class="form-group">
						<label class="control-label col-sm-3">Admin Password:</label>
						<div class="col-sm-5">
							<input type="text" name="AdminPassword" id="AdminPassword" value="<?php echo $config->AdminPassword?>" class="form-control" />
						</div>
					</div>

					<!-- div class="form-group">
						<label class="control-label col-sm-3">Class Reminder Minus Days:</label>
						<div class="col-sm-5">
							<input type="text" name="ClassReminderMinusDays" id="ClassReminderMinusDays" value="<?php echo $config->ClassReminderMinusDays?>" class="form-control" />
						</div>
					</div >

					<div class="form-group">
						<label class="control-label col-sm-3">Class Reminder Message Text:</label>
						<div class="col-sm-5">
							<input type="text" name="ClassReminderMessageText" id="ClassReminderMessageText" value="<?php echo $config->ClassReminderMessageText?>" class="form-control" />
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-3">Common Email Footer:</label>
						<div class="col-sm-5">
							<input type="text" name="CommonEmailFooter" id="CommonEmailFooter" value="<?php echo $config->CommonEmailFooter?>" class="form-control" />
						</div>
					</div-->

					<div class="form-group">
						<label class="control-label col-sm-3">Invoice Email Body:</label>
						<div class="col-sm-5">
							<textarea name="InvoiceEmailBody" id="InvoiceEmailBody"class="form-control wysihtml5" data-stylesheet-url="<?php echo base_url()?>assets/css/wysihtml5-color.css" name="sample_wysiwyg" id="sample_wysiwyg"><?php echo $config->InvoiceEmailBody?></textarea>
						</div>
					</div>

					<div class="col-sm-offset-3 col-sm-3">
                        <button type="submit" name="update_config" id="update_config"  class="btn btn-success col-md-5" >Update</button>
                        <input type="hidden" name="ConfigID" id="ConfigID" value="<?php echo $config->ConfigID?>" class="form-control">
                     </div>


				</form>
			</div>

		</div>
	</div>

</div>
