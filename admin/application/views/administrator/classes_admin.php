
<script type="text/javascript" src="<?= base_url().'assets/neon/js/bootstrap-timepicker.min.js'?>"></script>
<div class="row">
	<div class="col-md-12">
		<!-- 
			Classes administration...
			Fields:
		-->  

		<ul class="nav nav-tabs bordered">
			<li class="active">
				<a href="#view-classes"  data-toggle="tab">
				<span class="visible-xs"><i class="entypo-tag"></i></span>
				<span class="hidden-xs">Classes</span>
				</a>

			</li>
			<li>
				<a href="#add-class"  data-toggle="tab">
				<span class="visible-xs"><i class="entypo-plus-squared"></i></span>
				<span class="hidden-xs">Add a new class</span>
				</a>
				
			</li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="view-classes">

				<table id="classes-table" class="table table-bordered datatable">
					<thead><tr>
						<th>Educator Class ID</th>
						<th>Class Date</th>
						<th>Class Time</th>
						<th>Duration</th>
						<th>Patients Invited</th>
						<th>Educator</th>
						<th>Location</th>
						<th>Course</th>
						<th>Hospital</th>
						<th>Instructions</th>
						
					</tr></thead>
					<tbody> 
				</tbody>

				</table>
			</div>
			<div class="tab-pane" id="add-class">  
			<form class="form-horizontal" method="post" 
				action="<?php echo base_url()?>administrator/addClass" id="form-add-class">
					<div class="form-group">
		                <label class="col-sm-3 control-label">Class location:</label>
		                <div class="col-sm-5"> 
		                 	<select name="newClassLocation" id="newClassLocation" class="form-control select2" required>
		                 		<option class="hidden" selected value=''>Please select a location</option>
		                        <?php 
		                        foreach ($educatorlocations as $location) {
		                            
		                            echo "<option value='$location->EduLocationID' >$location->LocName</optiom>";
		                        }
		                        ?>
		                    </select> 
						</div>
					</div>
					<div class="form-group">
		                <label class="col-sm-3 control-label">Hospital:</label>
		                <div class="col-sm-5"> 
		                 	<select name="newClassHospital" id="newClassHospital" class="form-control select2" required>
		                 		<option class="hidden" selected value=''>Please select a hospital...</option>
		                        <?php 
		                        foreach ($hospitals as $hospital) {
		                            
		                            echo "<option value='$hospital->HospitalID' >$hospital->HospitalName</optiom>";
		                        }
		                        ?>
		                    </select> 
						</div>
					</div>
					<div class="form-group">
		                <label class="col-sm-3 control-label">Educator:</label>
		                <div class="col-sm-5"> 
		                 	<select name="newClassEducator" id="newClassEducator" class="form-control select2" required>
		                 		<option class="hidden" selected value=''>Please select a educator...</option>
		                        <?php 
		                        foreach ($educators as $educator) {
		                            
		                            echo "<option value='$educator->EducatorID' >$educator->Firstname $educator->Lastname, $educator->BusinessName</optiom>";
		                        }
		                        ?>
		                    </select> 
						</div>
					</div>
					<div class="form-group">
		                <label class="col-sm-3 control-label">Course:</label> 
		                <div class="col-sm-5"> 
		                 	<select name="newClassCourse" id="newClassCourse" class="form-control select2" required>
		                 		<option class="hidden" selected value=''>Please select a course...</option>
		                        <?php 
		                        foreach ($courses as $course) {
		                            
		                            echo "<option value='$course->CourseID' >$course->CourseName</optiom>";
		                        }
		                        ?>
		                    </select> 
						</div>
					</div>
					<div class="form-group">
		                <label class="col-sm-3 control-label">Duration:</label>
		                <div class="col-sm-5"> 
		                 	<input class="form-control" name="newClassDuration" id="newClassDuration" value="60" required/>
						</div>
					</div>
					<div class="form-group">
		                <label class="col-sm-3 control-label">Start date:</label>
						<div class="col-md-5">  
			                <div class="input-group">
			                    <input type="text" name="newClassStartDate" 
			                    	class="form-control datepicker" 
			                    	id="start-date" data-format="dd-mm-yyyy" value="<?php echo date('d-m-Y'); ?>" required>
			                    <div class="input-group-addon"> <a href="#"><i class="entypo-calendar"></i></a> </div> 
			                </div>
			            </div>
			        </div>
					<div class="form-group">
		                <label class="col-sm-3 control-label">Start time:</label>
						<div class="col-md-5">  
			                <div class="input-group">  
			                	<input 
			                		type="text" class="form-control timepicker" data-template="dropdown" 
			                		name="newClassTime" id="newClassTime"
			                		data-show-seconds="true" data-show-meridian="true" data-minute-step="5" 
			                		data-second-step="5" value="<?php echo date('H:i:s a') ?>" required>
		                		<div class="input-group-addon"> 
		                			<a href="#">
		                				<i class="entypo-clock"></i>
		                			</a> 
		                		</div> 
		                	</div>
			            </div>
			        </div>

				<div class="col-md-offset-7">
					<button class="btn btn-success" type="submit">Save</button>
					<button class="btn btn-danger" type="reset">Reset</button>
				</div>  
			</form>
		</div> 
	</div>
		<!-- 
	--> 
	</div>
</div>

<script type="text/javascript" src='<?= base_url()."assets/js/tensApi/tensDatesFormatter.js"?>'></script>
<script type="text/javascript">
 
 jQuery("#form-add-class").validate({
                        ignore: 'input[type=hidden]',
                        rules:
                        {
                            newClassLocation: { required: true }, 
                            newClassHospital: {required: true},
                            newClassEducator: {required: true},
                            newClassCourse: {required: true},
                            newClassDuration: {required: true},
                            newClassStartDate: {required: true},
                            newClassTime: {required: true} 
                        },
 
                        errorPlacement: function (error, element) { 
                            var elem = jQuery(element);
                            error.addClass('errorLabel');
                            error.insertAfter(element);
                        },
                        
                        success: function (label, element) {  
                            var elem = jQuery(element);
                            elem.siblings('label.error').remove();
                        },

                        submitHandler: function(form) {
                            return true;
                        }
                    }); 

jQuery("#classes-table").dataTable({
	processing: true,
    serverSide: true,
    pagination: true,
    pageLength: 10,
    sDom:'tp',
    ajax: {
        url: "<?php echo base_url()?>ajaxClasses/getClasses",
        type: "GET"
    },
    aoColumns : 
    [
    	{'data':'eduClassId', "sClass":'hide_column'},
    	{	
    		'data':'classDate', 
    		"mRender" : function(data, type, full) 
    		{
    			return formatDate(data);
    		}
    	},
    	{	
    		'data':'startTime', 
    		"mRender" : function(data, type, full) 
    		{
    			return formatTime(data);
    		}
    	},
    	{'data':'duration'},
    	{'data':'numberattending'},
    	{'data':'educatorName'},
    	{'data':'locationName'},
    	{'data':'courseName'},
    	{'data':'hospitalName'},
    	{'data':'instructions'}
    ]}
);
</script>