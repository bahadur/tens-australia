

<div class="row">
	<div class="col-md-12">
		
		<ul class="nav nav-tabs bordered">
			<li class="active">
				<a href="#payment-methods"  data-toggle="tab">
				<span class="visible-xs"><i class="entypo-tag"></i></span>
				<span class="hidden-xs">Payment Methods</span>
				</a>

			</li>
			<li>
				<a href="#add-payment-method"  data-toggle="tab">
				<span class="visible-xs"><i class="entypo-plus-squared"></i></span>
				<span class="hidden-xs">Add</span>
				</a>
				
			</li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="payment-methods">
				<table id="area-manager_table" class="table" cellspacing="0" >
		            <thead>
		                  



		            	<tr>
		            		<th>AreaManagerID</th>
							<th>BusinessName</th>
							<th>ABN</th>
							<th>Firstname</th>
							<th>Lastname</th>
							<th>Position</th>
							
							<th>Active</th>
		                	<th></th>

		                </tr>
		            </thead>
					<tbody>
		            <?php 
		            foreach ($areamanagers as $row){
		                echo "<tr>";
		                	// echo "<td><a href='".base_url()."classes/detail/".$row->PaymentMethodID."' target='_blank' >".$row->PaymentMethodID."</a></td>";
		                    
		                 echo "<td>".$row->AreaManagerID."</td>";
						 echo "<td>".$row->BusinessName."</td>";
						 echo "<td>".$row->ABN."</td>";
						 echo "<td>".$row->Firstname."</td>";
						 echo "<td>".$row->Lastname."</td>";
						 echo "<td>".$row->Position."</td>";
						 echo "<td>".$row->Active."</td>";


		                    echo "<td>
		                    	<div class='btn-group pull-right'> 
				            		<button type='button' class='btn btn-default btn-sm dropdown-toggle' data-toggle='dropdown'>
				            			Action <span class='caret'></span> 
				            		</button>
				                    <ul class='dropdown-menu dropdown-default pull-right' role='menu'> 
				                    	<li> <a href='#' onclick='showDetailModal(".$row->AreaManagerID.")'> <i class='entypo-list'></i> Detail </a> </li> 
				                    	<li class='divider'></li> 
				                    	<li> <a href='#' onclick='showEditModal(".$row->AreaManagerID.")'> <i class='entypo-pencil'></i> Edit </a> </li> 
				                    	<li class='divider'></li> 
				                    	<li> <a href='#' onclick='showDeleteModal(".$row->AreaManagerID.")'> <i class='entypo-trash'></i> Delete </a> </li>' 
				                    </ul>
				                </div>
		                    </td>";
		                echo "</tr>";
		            }
		            ?>
		            </tbody>
		        </table>
			</div>
			<div class="tab-pane" id="add-payment-method">
				<form class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url()?>administrator/area_managers" id="form-area-manager">
			

					
					<div class="form-group">
		                    <label class="col-sm-3 control-label">Business Name:</label>
		                    <div class="col-sm-5">
		                        <input type="text" name="BusinessName" id="BusinessName" class="form-control" />
		                    </div>
		            </div>
					<div class="form-group">
		                    <label class="col-sm-3 control-label">ABN:</label>
		                    <div class="col-sm-5">
		                        <input type="text" name="ABN" id="ABN" class="form-control" />
		                    </div>
		            </div>
					<div class="form-group">
		                    <label class="col-sm-3 control-label">First Name:</label>
		                    <div class="col-sm-5">
		                        <input type="text" name="Firstname" id="Firstname" class="form-control" />
		                    </div>
		            </div>
					<div class="form-group">
		                    <label class="col-sm-3 control-label">Last Name:</label>
		                    <div class="col-sm-5">
		                        <input type="text" name="Lastname" id="Lastname" class="form-control" />
		                    </div>
		            </div>
					<div class="form-group">
		                    <label class="col-sm-3 control-label">Position:</label>
		                    <div class="col-sm-5">
		                        <input type="text" name="Position" id="Position" class="form-control" />
		                    </div>
		            </div>
					<div class="form-group">
		                    <label class="col-sm-3 control-label">Address:</label>
		                    <div class="col-sm-5">
		                        <input type="text" name="Address" id="Address" class="form-control" />
		                    </div>
		            </div>
					<div class="form-group">
		                    <label class="col-sm-3 control-label">Suburb:</label>
		                    <div class="col-sm-5">
		                        <input type="text" name="Suburb" id="Suburb" class="form-control" />
		                    </div>
		            </div>
					<div class="form-group">
		                    <label class="col-sm-3 control-label">State:</label>
		                    <div class="col-sm-5">
		                        <input type="text" name="State" id="State" class="form-control" />
		                    </div>
		            </div>
					<div class="form-group">
		                    <label class="col-sm-3 control-label">Post Code:</label>
		                    <div class="col-sm-5">
		                        <input type="text" name="Postcode" id="Postcode" class="form-control" />
		                    </div>
		            </div>
					<div class="form-group">
		                    <label class="col-sm-3 control-label">Country:</label>
		                    <div class="col-sm-5">
		                        <input type="text" name="Country" id="Country" class="form-control" />
		                    </div>
		            </div>
					<div class="form-group">
		                    <label class="col-sm-3 control-label">Phone:</label>
		                    <div class="col-sm-5">
		                        <input type="text" name="Phone" id="Phone" class="form-control" />
		                    </div>
		            </div>
					<div class="form-group">
		                    <label class="col-sm-3 control-label">Fax:</label>
		                    <div class="col-sm-5">
		                        <input type="text" name="Fax" id="Fax" class="form-control" />
		                    </div>
		            </div>
					<div class="form-group">
		                    <label class="col-sm-3 control-label">Mobile Phone:</label>
		                    <div class="col-sm-5">
		                        <input type="text" name="MobilePhone" id="MobilePhone" class="form-control" />
		                    </div>
		            </div>
					<div class="form-group">
		                    <label class="col-sm-3 control-label">Email:</label>
		                    <div class="col-sm-5">
		                        <input type="text" name="Email" id="Email" class="form-control" />
		                    </div>
		            </div>
					<div class="form-group">
		                    <label class="col-sm-3 control-label">Referral Fee:</label>
		                    <div class="col-sm-5">
		                        <input type="text" name="ReferralFee" id="ReferralFee" class="form-control" />
		                    </div>
		            </div>
					<div class="form-group">
		                    <label class="col-sm-3 control-label">Start Date:</label>
		                    <div class="col-sm-5">
		                        <input type="text" name="StartDate" id="StartDate" class="form-control" />
		                    </div>
		            </div>
					<div class="form-group">
		                    <label class="col-sm-3 control-label">Active:</label>
		                    <div class="col-sm-5">
		                        <input type="text" name="Active" id="Active" class="form-control" />
		                    </div>
		            </div>

		            

		             <div class="form-group">
		                    <label class="col-sm-3 control-label">Postal Codes:</label>
		                    <div class="col-sm-5">
		                   
		                    	<!-- <div id="postal_codes"></div>
		                    	<input type="hidden" name="postal_codes_valus" id="postal_codes_valus" /> -->
		                   		<input class="form-control" data-val="true"  id="postal_codes" name="postal_codes" type="hidden" />
		                    </div>
		            </div>


		            

                

  
	                <div class="form-group">
	                    <div class="col-sm-offset-3 col-sm-5">
	                        <button type="submit" class="btn btn-primary" >Insert Area Manager</button>
	                        <input type="hidden" name="action" value="insert">
	                    </div>
	                </div>
			

            
			
				</form>
			</div>
		</div>

		
	</div>
</div>


<!-- Modal Edit -->
 <div class="modal fade" id="modal-edit" data-backdrop="static" aria-hidden="true" style="display: none;"> 
                <div class="modal-dialog"> 
                    <div class="modal-content"> 
                        <div class="modal-header"> 
                            <h4 class="modal-title">Confirm Modal</h4> 
                        </div> 
                        <div class="modal-body">
                            <div class="row">  </div>
                        </div> 
                        <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div> 
                    </div> 
                </div> 
</div>

<div class="modal fade" id="modal-delete" data-backdrop="static" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
    	<div class="modal-content" style="margin-top:100px;">
        	<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" style="text-align:center;">Are you sure to delete this information ?</h4>
			</div>

			<div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
				
			</div>
        </div>
    </div>
</div>


<script type="text/javascript">


jQuery(document).ready(function () {

   

    var payment_table = jQuery('#area-manager_table').DataTable({ 
       
    	
        //"paging": false,
        //sDom: "lrtip"
    });

    // jQuery("#form-area-manager").submit(function(){
    // 	// console.log(jQuery("#postal_codes").val());

    // 	jQuery("#postal_codes_valus").val(jQuery("#postal_codes").val());
    // 	return true;
    // })

jQuery('#postal_codes').select2(
{
    placeholder: "Add Postal Codes!",
    minimumInputLength: 2,
    multiple: true,
    ajax: {
        url: "<?php echo base_url()?>ajax/postal_codes",
        dataType: 'json',
        quietMillis: 100,
        data: function (term, page) {
            return {
            term: term, //search term
            page_limit: 10, // page size
            page: page //you need to send page number or your script do not know witch results to skip
            };
        },
        results: function (data, page)
        {
            // var more = (page * 10) < data.total;
            // return { results: data.results, more: more };
            return { results: data.results };
        },
        dropdownCssClass: "bigdrop"
    }
});

   




});

function showEditModal(AreaManagerID)

{ 
    var priceGroupId = jQuery("#price-group-select").val();
    var stringUrl = "<?=  base_url().'modals/areaManagerEdit/' ?>" + AreaManagerID ; 
    //console.log("string url : " + stringUrl);
    jQuery('#modal-edit').find('.modal-body').html("<div class='row'><image src='<?= base_url().'assets/img/indicator.gif' ?>'/></div>");
    jQuery.ajax({       
        type: 'get', 
        url: stringUrl,
        success: function(data)
        {  
            jQuery('#modal-edit').find('.modal-body').html(data); 
        },
        error :function(jqXHR, textStatus, errorThrown ) {

        }  
    });
    jQuery('#modal-edit').modal('show', {backdrop: 'static'});
} 


function showDeleteModal(AreaManagerID)
{ 
   var stringUrl = "<?= base_url().'administrator/area_managers'?>" ; 
    jQuery('#modal-delete').find('.modal-body').html("<div class='row'><image src='<?= base_url().'assets/img/indicator.gif' ?>'/></div>");
    jQuery('#modal-delete').find('.modal-footer').html('<form action="'+stringUrl+'"  method="post" accept-charset="utf-8" class="form-horizontal form-groups-bordered validate" target="_top" enctype="multipart/form-data">'+
					'<input name="orderDetailId" type="hidden" id="deleteOrderDetailId"/>'+
					'<button type="submit" class="btn btn-danger" id="delete_link">delete</a>'+
					'<button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>'+
					'<input type="hidden" name="AreaManagerID" id="AreaManagerID" value="'+AreaManagerID+'" class="form-control" />'+
					'<input type="hidden" name="action" value="delete">'+
				'</form>'); 
    jQuery('#modal-delete').modal('show', {backdrop: 'static'});
} 


</script>

