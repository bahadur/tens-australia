<div class="row">
    <div class="col-md-12">
        
        <ul class="nav nav-tabs bordered">
            <li class="active">
                <a href="#locations"  data-toggle="tab">
                <span class="visible-xs"><i class="entypo-tag"></i></span>
                <span class="hidden-xs">Locations</span>
                </a>

            </li>
            <li>
                <a href="#add-location"  data-toggle="tab">
                <span class="visible-xs"><i class="entypo-plus-squared"></i></span>
                <span class="hidden-xs">Add</span>
                </a>
                
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="locations">
                <table id="locations-table" class="table    " cellspacing="0" >
                    <thead>
                                   
                        <tr>
                            
                            <th>Location ID</th>
                            <th>Location Type</th>
                            <th>Location Description</th>
                            <th>Address</th>
                            <th>Suburb</th>
                            <th>State</th>
                            <th>Post Code</th>
                            <th>Country</th>
                            <th>Pick Stock</th>
                            <th>Hire Returns</th>

                            <th></th>

                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                    foreach ($locations as $row){
                        echo "<tr>";
                            echo "<td><a href='".base_url()."classes/detail/".$row->LocationID."' target='_blank' >".$row->LocationID."</a></td>";
                            echo "<td>".$row->LocationType."</td>";
                            echo "<td>".$row->LocationDescription."</td>";
                            echo "<td>".$row->Address."</td>";
                            echo "<td>".$row->Suburb."</td>";
                            echo "<td>".$row->State."</td>";
                            echo "<td>".$row->Postcode."</td>";
                            echo "<td>".$row->Country."</td>";
                            echo "<td>".$row->PickStock."</td>";
                            echo "<td>".$row->HireReturns."</td>";

                            echo "<td>
                                <div class='btn-group pull-right'> 
                                    <button type='button' class='btn btn-default btn-sm dropdown-toggle' data-toggle='dropdown'>
                                        Action <span class='caret'></span> 
                                    </button>
                                    <ul class='dropdown-menu dropdown-default pull-right' role='menu'> 
                                        <li> <a href='#' onclick='showEditModal(".$row->LocationID.")'> <i class='entypo-pencil'></i> Edit </a> </li> 
                                        <li class='divider'></li> 
                                        <li> <a href='#' onclick='showDeleteModal(".$row->LocationID.")'> <i class='entypo-trash'></i> Delete </a> </li>' 
                                    </ul>
                                </div>
                            </td>";
                        echo "</tr>";
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <div class="tab-pane" id="add-location">
                <form class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url()?>administrator/locations" id="form-order-accountcodes">
            



                    <div class="form-group">
                        <label class="col-sm-3 control-label">Location Type:</label>
                        <div class="col-sm-5">

                            <select name="LocationTypeID" id="LocationTypeID" class="form-control select2">
                                    <?php 
                                    foreach ($this->db->get("locationtype")->result() as $locationtype) {
                                        
                                        echo "<option value='$locationtype->LocationTypeID' >$locationtype->LocationType</optiom>";
                                    }
                                    ?>
                                </select>
                            
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Location Description:</label>
                        <div class="col-sm-5">
                            <input type="text"  name="LocationDescription" id="LocationDescription" class="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Address:</label>
                        <div class="col-sm-5">
                            <input type="text"  name="Address" id="Address" class="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Suburb:</label>
                        <div class="col-sm-5">
                            <input type="text"  name="Suburb" id="Suburb" class="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">State:</label>
                        <div class="col-sm-5">
                            <input type="text"  name="State" id="State" class="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Post Code:</label>
                        <div class="col-sm-5">
                            <input type="text"  name="Postcode" id="Postcode" class="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Country:</label>
                        <div class="col-sm-5">
                            <input type="text"  name="Country" id="Country" class="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Pick Stock:</label>
                        <div class="col-sm-5">
                            <input type="text"  name="PickStock" id="PickStock" class="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Hire Returns:</label>
                        <div class="col-sm-5">
                            <input type="text"  name="HireReturns" id="HireReturns" class="form-control" />
                        </div>
                    </div>


                

  
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" class="btn btn-primary" >Insert Location</button>
                            <input type="hidden" name="action" value="insert">
                        </div>
                    </div>
            

            
            
                </form>
            </div>
        </div>

        
    </div>
</div>


<!-- Modal Edit -->
 <div class="modal fade" id="modal-edit" data-backdrop="static" aria-hidden="true" style="display: none;"> 
                <div class="modal-dialog"> 
                    <div class="modal-content"> 
                        <div class="modal-header"> 
                            <h4 class="modal-title">Confirm Modal</h4> 
                        </div> 
                        <div class="modal-body">
                            <div class="row">  </div>
                        </div> 
                        <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div> 
                    </div> 
                </div> 
</div>

<div class="modal fade" id="modal-delete" data-backdrop="static" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content" style="margin-top:100px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" style="text-align:center;">Are you sure to delete this information ?</h4>
            </div>

            <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">


jQuery(document).ready(function ($) {

   

    var locations_table = $('#locations-table').DataTable({ 
       
        
        //"paging": false,
        //sDom: "lrtip"
    });


   


});

function showEditModal(LocationID)

{ 
    var priceGroupId = jQuery("#price-group-select").val();
    var stringUrl = "<?=  base_url().'modals/LocationEdit/' ?>" + LocationID ; 
    //console.log("string url : " + stringUrl);
    jQuery('#modal-edit').find('.modal-body').html("<div class='row'><image src='<?= base_url().'assets/img/indicator.gif' ?>'/></div>");
    jQuery.ajax({       
        type: 'get', 
        url: stringUrl,
        success: function(data)
        {  
            jQuery('#modal-edit').find('.modal-body').html(data); 
        },
        error :function(jqXHR, textStatus, errorThrown ) {

        }  
    });
    jQuery('#modal-edit').modal('show', {backdrop: 'static'});
} 


function showDeleteModal(LocationID)
{ 
   var stringUrl = "<?= base_url().'administrator/locations'?>" ; 
    jQuery('#modal-delete').find('.modal-body').html("<div class='row'><image src='<?= base_url().'assets/img/indicator.gif' ?>'/></div>");
    jQuery('#modal-delete').find('.modal-footer').html('<form action="'+stringUrl+'"  method="post" accept-charset="utf-8" class="form-horizontal form-groups-bordered validate" target="_top" enctype="multipart/form-data">'+
                    '<input name="orderDetailId" type="hidden" id="deleteOrderDetailId"/>'+
                    '<button type="submit" class="btn btn-danger" id="delete_link">delete</a>'+
                    '<button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>'+
                    '<input type="hidden" name="LocationID" id="LocationID" value="'+LocationID+'" class="form-control" />'+
                    '<input type="hidden" name="action" value="delete">'+
                '</form>'); 
    jQuery('#modal-delete').modal('show', {backdrop: 'static'});
} 


</script>

