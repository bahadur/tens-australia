<div class="row">
    <div class="col-md-12">
        
        <ul class="nav nav-tabs bordered">
            <li class="active">
                <a href="#products"  data-toggle="tab">
                <span class="visible-xs"><i class="entypo-tag"></i></span>
                <span class="hidden-xs">Products</span>
                </a>

            </li>
            <li>
                <a href="#add-product"  data-toggle="tab">
                <span class="visible-xs"><i class="entypo-plus-squared"></i></span>
                <span class="hidden-xs">Add</span>
                </a>
                
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="products">
                <table id="products-table" class="table" cellspacing="0" >
                    <thead>
                                   
                        <tr>
                            <th>Product ID</th>    
                            <th>Product Category</th>
                            <th>Product Code</th>
                            <th>Product Name</th>
                            <th>Active</th>
                            <th>Notes</th>
                            

                            <th></th>

                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                    foreach ($products as $row){
                        echo "<tr>";
                            echo "<td><a href='".base_url()."classes/detail/".$row->ProductID."' target='_blank' >".$row->ProductID."</a></td>";
                            echo "<td>".$row->ProductCategory."</td>";
                            echo "<td>".$row->ProductCode."</td>";
                            echo "<td>".$row->ProductName."</td>";
                            echo "<td>".$row->Active."</td>";
                            echo "<td>".$row->Notes."</td>";
                            


                            echo "<td>
                                <div class='btn-group pull-right'> 
                                    <button type='button' class='btn btn-default btn-sm dropdown-toggle' data-toggle='dropdown'>
                                        Action <span class='caret'></span> 
                                    </button>
                                    <ul class='dropdown-menu dropdown-default pull-right' role='menu'> 
                                        <li> <a href='#' onclick='showEditModal(".$row->ProductID.")'> <i class='entypo-pencil'></i> Edit </a> </li> 
                                        <li class='divider'></li> 
                                        <li> <a href='#' onclick='showDeleteModal(".$row->ProductID.")'> <i class='entypo-trash'></i> Delete </a> </li>' 
                                    </ul>
                                </div>
                            </td>";
                        echo "</tr>";
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <div class="tab-pane" id="add-product">
                <form class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url()?>administrator/products" id="form-order-accountcodes">
            

                                      


                    <div class="form-group">
                        <label class="col-sm-3 control-label">Product Category:</label>
                        <div class="col-sm-5">
                            <select name="ProductCategoryID" id="ProductCategoryID" class="form-control select2">
                                    <?php 
                                    foreach ($this->db->get("productcategory")->result() as $productcategory) {
                                        
                                        echo "<option value='$productcategory->ProductCategoryID' >$productcategory->ProductCategory</optiom>";
                                    }
                                    ?>
                                </select>
                            
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">ProductCode:</label>
                        <div class="col-sm-5">
                            <input type="text"  name="ProductCode" id="ProductCode" class="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">ProductName:</label>
                        <div class="col-sm-5">
                            <input type="text"  name="ProductName" id="ProductName" class="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Active:</label>
                        <div class="col-sm-5">
                            <input type="text"  name="Active" id="Active" class="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Notes:</label>
                        <div class="col-sm-5">
                            <input type="text"  name="Notes" id="Notes" class="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Hire:</label>
                        <div class="col-sm-5">
                            <input type="text"  name="Hire" id="Hire" class="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tax Code:</label>
                        <div class="col-sm-5">
                             <select name="TaxCodeID" id="TaxCodeID" class="form-control select2">
                                    <?php 
                                    foreach ($this->db->get("taxcode")->result() as $taxcode) {
                                        
                                        echo "<option value='$taxcode->TaxCodeID' >$taxcode->TaxCode</optiom>";
                                    }
                                    ?>
                                </select>
                            
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">LocationID:</label>
                        <div class="col-sm-5">
                                <select name="LocationID" id="LocationID" class="form-control select2">
                                    <?php 
                                    foreach ($this->db->get("location")->result() as $location) {
                                        
                                        echo "<option value='$location->LocationID' >$location->LocationDescription</optiom>";
                                    }
                                    ?>
                                </select>
                            
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">HasBarcodeID:</label>
                        <div class="col-sm-5">
                                <select name="HasBarcodeID" id="HasBarcodeID" class="form-control select2">
                                    <?php 
                                    foreach ($this->db->get("hasbarcode")->result() as $hasbarcode) {
                                        
                                        echo "<option value='$hasbarcode->HasBarcodeID' >$hasbarcode->HasBarcode</optiom>";
                                    }
                                    ?>
                                </select>
                            
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">zzGST:</label>
                        <div class="col-sm-5">
                            <input type="text"  name="zzGST" id="zzGST" class="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">AccountCode:</label>
                        <div class="col-sm-5">
                            <input type="text"  name="AccountCode" id="AccountCode" class="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">SwapToProductID:</label>
                        <div class="col-sm-5">
                            <input type="text"  name="HospitalName" id="HospitalName" class="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">ImportantDatesNotification:</label>
                        <div class="col-sm-5">
                            <input type="text"  name="HospitalName" id="HospitalName" class="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">ShowRefundButton:</label>
                        <div class="col-sm-5">
                            <input type="text"  name="SwapToProductID" id="SwapToProductID" class="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">PriceList:</label>
                        <div class="col-sm-5">
                            <input type="text"  name="PriceList" id="PriceList" class="form-control" />
                        </div>
                    </div>





                    


                

  
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" class="btn btn-primary" >Insert Product</button>
                            <input type="hidden" name="action" value="insert">
                        </div>
                    </div>
            

            
            
                </form>
            </div>
        </div>

        
    </div>
</div>


<!-- Modal Edit -->
 <div class="modal fade" id="modal-edit" data-backdrop="static" aria-hidden="true" style="display: none;"> 
                <div class="modal-dialog"> 
                    <div class="modal-content"> 
                        <div class="modal-header"> 
                            <h4 class="modal-title">Confirm Modal</h4> 
                        </div> 
                        <div class="modal-body">
                            <div class="row">  </div>
                        </div>
                        <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>  
                    </div> 
                </div> 
</div>

<div class="modal fade" id="modal-delete" data-backdrop="static" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content" style="margin-top:100px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" style="text-align:center;">Are you sure to delete this information ?</h4>
            </div>

            <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">


jQuery(document).ready(function ($) {

   

    var products_table = $('#products-table').DataTable({ 
       
        
        //"paging": false,
        //sDom: "lrtip"
    });


   


});

function showEditModal(ProductID)

{ 
    var priceGroupId = jQuery("#price-group-select").val();
    var stringUrl = "<?=  base_url().'modals/productEdit/' ?>" + ProductID ; 
    //console.log("string url : " + stringUrl);
    jQuery('#modal-edit').find('.modal-body').html("<div class='row'><image src='<?= base_url().'assets/img/indicator.gif' ?>'/></div>");
    jQuery.ajax({       
        type: 'get', 
        url: stringUrl,
        success: function(data)
        {  
            jQuery('#modal-edit').find('.modal-body').html(data); 
        },
        error :function(jqXHR, textStatus, errorThrown ) {

        }  
    });
    jQuery('#modal-edit').modal('show', {backdrop: 'static'});
} 


function showDeleteModal(ProductID)
{ 
   var stringUrl = "<?= base_url().'administrator/products'?>" ; 
    jQuery('#modal-delete').find('.modal-body').html("<div class='row'><image src='<?= base_url().'assets/img/indicator.gif' ?>'/></div>");
    jQuery('#modal-delete').find('.modal-footer').html('<form action="'+stringUrl+'"  method="post" accept-charset="utf-8" class="form-horizontal form-groups-bordered validate" target="_top" enctype="multipart/form-data">'+
                    '<input name="orderDetailId" type="hidden" id="deleteOrderDetailId"/>'+
                    '<button type="submit" class="btn btn-danger" id="delete_link">delete</a>'+
                    '<button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>'+
                    '<input type="hidden" name="ProductID" id="ProductID" value="'+ProductID+'" class="form-control" />'+
                    '<input type="hidden" name="action" value="delete">'+
                '</form>'); 
    jQuery('#modal-delete').modal('show', {backdrop: 'static'});
} 


</script>

