<div class="row">
	<div class="col-md-12">
		
		<ul class="nav nav-tabs bordered">
			<li class="active">
				<a href="#education_locations"  data-toggle="tab">
				<span class="visible-xs"><i class="entypo-tag"></i></span>
				<span class="hidden-xs">Customer Types</span>
				</a>

			</li>
			<li>
				<a href="#add-education-locations"  data-toggle="tab">
				<span class="visible-xs"><i class="entypo-plus-squared"></i></span>
				<span class="hidden-xs">Add</span>
				</a>
				
			</li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="education_locations">
				<table id="educatorlocations-table" class="table 	" cellspacing="0" >
		            <thead>
		                           
		            	<tr>

		            	<th>EduLocationID</th>
						<th>LocName</th>
						<th>LocAddress</th>
						<th>LocSuburb</th>
						<th>LocState</th>
						<th>LocPostcode</th>
						<th>LocContact</th>
						<th></th>

		                </tr>
		            </thead>
					<tbody>
		            <?php 
		            foreach ($educatorlocations as $row){

		            	



		                echo "<tr>";
		                	echo "<td><a href='".base_url()."classes/detail/".$row->EduLocationID."' target='_blank' >".$row->EduLocationID."</a></td>";
		                    echo "<td>".$row->LocName."</td>";
							echo "<td>".$row->LocAddress."</td>";
							echo "<td>".$row->LocSuburb."</td>";
							echo "<td>".$row->LocState."</td>";
							echo "<td>".$row->LocPostcode."</td>";
							echo "<td>".$row->LocContact."</td>";
		                    echo "<td>
		                    	<div class='btn-group pull-right'> 
				            		<button type='button' class='btn btn-default btn-sm dropdown-toggle' data-toggle='dropdown'>
				            			Action <span class='caret'></span> 
				            		</button>
				                    <ul class='dropdown-menu dropdown-default pull-right' role='menu'> 
				                    	<li> <a href='#' onclick='showEditModal(".$row->EduLocationID.")'> <i class='entypo-pencil'></i> Edit </a> </li> 
				                    	<li class='divider'></li> 
				                    	<li> <a href='#' onclick='showDeleteModal(".$row->EduLocationID.")'> <i class='entypo-trash'></i> Delete </a> </li>' 
				                    </ul>
				                </div>
		                    </td>";
		                echo "</tr>";
		            }
		            ?>
		            </tbody>
		        </table>
			</div>
			<div class="tab-pane" id="add-education-locations">
				<form class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url()?>administrator/education_locations" id="form-order-accountcodes">

					<div class="form-group">
		                    <label class="col-sm-3 control-label">LocName:</label>
		                    <div class="col-sm-5">
		                        <input type="text"  name="LocName" id="LocName" class="form-control" />
		                        
		                    </div>
		            </div>
					<div class="form-group">
		                    <label class="col-sm-3 control-label">LocAddress:</label>
		                    <div class="col-sm-5">

		                     	<select name="LocAddress" id="LocAddress" class="form-control select2">
                                    <?php 
                                    foreach ($this->db->get("locationtype")->result() as $locationtype) {
                                        
                                        echo "<option value='$locationtype->LocationTypeID' >$locationtype->LocationType</optiom>";
                                    }
                                    ?>
                                </select>
		                        <input type="text"  name="LocAddress" id="LocAddress" class="form-control" />
		                        
		                    </div>
		            </div>
					<div class="form-group">
		                    <label class="col-sm-3 control-label">LocSuburb:</label>
		                    <div class="col-sm-5">
		                        <input type="text"  name="LocSuburb" id="LocSuburb" class="form-control" />
		                        
		                    </div>
		            </div>
					<div class="form-group">
		                    <label class="col-sm-3 control-label">LocState:</label>
		                    <div class="col-sm-5">
		                        <input type="text"  name="LocState" id="LocState" class="form-control" />
		                        
		                    </div>
		            </div>
					<div class="form-group">
		                    <label class="col-sm-3 control-label">LocPostcode:</label>
		                    <div class="col-sm-5">
		                        <input type="text"  name="LocPostcode" id="LocPostcode" class="form-control" />
		                        
		                    </div>
		            </div>
					<div class="form-group">
		                    <label class="col-sm-3 control-label">LocContact:</label>
		                    <div class="col-sm-5">
		                        <input type="text"  name="LocContact" id="LocContact" class="form-control" />
		                        
		                    </div>
		            </div>
					<div class="form-group">
		                    <label class="col-sm-3 control-label">LocTelephone:</label>
		                    <div class="col-sm-5">
		                        <input type="text"  name="LocTelephone" id="LocTelephone" class="form-control" />
		                        
		                    </div>
		            </div>
					<div class="form-group">
		                    <label class="col-sm-3 control-label">Active:</label>
		                    <div class="col-sm-5">
		                        <input type="text"  name="Active" id="Active" class="form-control" />
		                        
		                    </div>
		            </div>
					<div class="form-group">
		                    <label class="col-sm-3 control-label">Instructions:</label>
		                    <div class="col-sm-5">
		                        <input type="text"  name="Instructions" id="Instructions" class="form-control" />
		                        
		                    </div>
		            </div>
					<div class="form-group">
		                    <label class="col-sm-3 control-label">EducatorPatientsID:</label>
		                    <div class="col-sm-5">
		                        <input type="text"  name="EducatorPatientsID" id="EducatorPatientsID" class="form-control" />
		                        
		                    </div>
		            </div>
					<div class="form-group">
		                    <label class="col-sm-3 control-label">Fee:</label>
		                    <div class="col-sm-5">
		                        <input type="text"  name="Fee" id="Fee" class="form-control" />
		                        
		                    </div>
		            </div>
					<div class="form-group">
		                    <label class="col-sm-3 control-label">ClassSize:</label>
		                    <div class="col-sm-5">
		                        <input type="text"  name="ClassSize" id="ClassSize" class="form-control" />
		                        
		                    </div>
		            </div>
					<div class="form-group">
		                    <label class="col-sm-3 control-label">LocMapURL:</label>
		                    <div class="col-sm-5">
		                        <input type="text"  name="LocMapURL" id="LocMapURL" class="form-control" />
		                        
		                    </div>
		            </div>


                

  
	                <div class="form-group">
	                    <div class="col-sm-offset-3 col-sm-5">
	                        <button type="submit" class="btn btn-primary" >Insert Education Location</button>
	                        <input type="hidden" name="action" value="insert">
	                    </div>
	                </div>
			

            
			
				</form>
			</div>
		</div>

		
	</div>
</div>


<!-- Modal Edit -->
 <div class="modal fade" id="modal-edit" data-backdrop="static" aria-hidden="true" style="display: none;"> 
                <div class="modal-dialog"> 
                    <div class="modal-content"> 
                        <div class="modal-header"> 
                            <h4 class="modal-title">Confirm Modal</h4> 
                        </div> 
                        <div class="modal-body">
                            <div class="row">  </div>
                        </div> 
                        <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div> 
                    </div> 
                </div> 
</div>

<div class="modal fade" id="modal-delete" data-backdrop="static" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
    	<div class="modal-content" style="margin-top:100px;">
        	<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" style="text-align:center;">Are you sure to delete this information ?</h4>
			</div>

			<div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
				
			</div>
        </div>
    </div>
</div>
<script type="text/javascript">


jQuery(document).ready(function ($) {

   

    var educatorlocations_table = $('#educatorlocations-table').DataTable({ 
       
    	
        //"paging": false,
        //sDom: "lrtip"
    });


   


});

function showEditModal(EduLocationID)

{ 
    var priceGroupId = jQuery("#price-group-select").val();
    var stringUrl = "<?=  base_url().'modals/educationLocationEdit/' ?>" + EduLocationID ; 
    //console.log("string url : " + stringUrl);
    jQuery('#modal-edit').find('.modal-body').html("<div class='row'><image src='<?= base_url().'assets/img/indicator.gif' ?>'/></div>");
    jQuery.ajax({       
        type: 'get', 
        url: stringUrl,
        success: function(data)
        {  
            jQuery('#modal-edit').find('.modal-body').html(data); 
        },
        error :function(jqXHR, textStatus, errorThrown ) {

        }  
    });
    jQuery('#modal-edit').modal('show', {backdrop: 'static'});
} 


function showDeleteModal(EduLocationID)
{ 
   var stringUrl = "<?= base_url().'administrator/education_locations'?>" ; 
    jQuery('#modal-delete').find('.modal-body').html("<div class='row'><image src='<?= base_url().'assets/img/indicator.gif' ?>'/></div>");
    jQuery('#modal-delete').find('.modal-footer').html('<form action="'+stringUrl+'"  method="post" accept-charset="utf-8" class="form-horizontal form-groups-bordered validate" target="_top" enctype="multipart/form-data">'+
					'<input name="orderDetailId" type="hidden" id="deleteOrderDetailId"/>'+
					'<button type="submit" class="btn btn-danger" id="delete_link">delete</a>'+
					'<button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>'+
					'<input type="hidden" name="EduLocationID" id="EduLocationID" value="'+EduLocationID+'" class="form-control" />'+
					'<input type="hidden" name="action" value="delete">'+
				'</form>'); 
    jQuery('#modal-delete').modal('show', {backdrop: 'static'});
} 


</script>

