<div class="row">
	    <div class="col-md-12">
	        <div class="panel panel-default">
	  			<div class="panel-body">

			       
		  			<table class="table" id="refund-order-table">

		  				<thead>
		  					<tr>
			  					<th>Order Detail ID</th>
			  					<th>Order Date</th>
			  					<th>Customer Name</th>
			  					<th>Address</th>
			  					<th>Home Phone</th>
			  					<th>Mobile Phone</th>
			  					<th>Email</th>
			  					<th>Due Date</th>
		  					</tr>
		  				</thead>
		  				
		  				<tbody>
		  					<?php  foreach ($refundOrderList as $row) { ?>
		  					<tr>
		  						<td> <a href="<?php echo base_url()?>administrator/refundByOrderID/detail/<?php echo $row->OrderID?>/<?php echo $row->OrderDetailID?>"> <?php echo $row->OrderDetailID?></a></td>
			  					<td><?php echo $row->OrderDate?></td>
			  					<td><?php echo $row->CustomerName?></td>
			  					<td><?php echo $row->Address?></td>
			  					<td><?php echo $row->HomePhone?></td>
			  					<td><?php echo $row->MobilePhone?></td>
			  					<td><?php echo $row->Email?></td>
			  					<td><?php echo $row->DueDate?></td>
		  					</tr>
		  					<?php }?>
		  						


		  					</tr>
		  				</tbody>
		  			</table>
		

			        
			        

	        	</div>
	        </div>

	    </div>
	
</div>

<script type="text/javascript">
jQuery(document).ready(function ($) {

   jQuery("#refund-order-table").DataTable();

   


});
</script>
