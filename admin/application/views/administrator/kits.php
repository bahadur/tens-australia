<div class="row">
    <div class="col-md-12">
        
        <ul class="nav nav-tabs bordered">
            <li class="active">
                <a href="#kits"  data-toggle="tab">
                <span class="visible-xs"><i class="entypo-tag"></i></span>
                <span class="hidden-xs">Kits</span>
                </a>

            </li>
            <li>
                <a href="#add-kit"  data-toggle="tab">
                <span class="visible-xs"><i class="entypo-plus-squared"></i></span>
                <span class="hidden-xs">Add</span>
                </a>
                
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="kits">
                <table id="kits-table" class="table" cellspacing="0" >
                    <thead>
                                   
                        <tr>
                            
                            <th>Kit ID</th>
                            <th>Kit Name</th>
                            <th>Notes</th>
                            <th></th>

                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                    foreach ($kits as $row){
                        echo "<tr>";
                            echo "<td><a href='".base_url()."classes/detail/".$row->KitID."' target='_blank' >".$row->KitID."</a></td>";
                            echo "<td>".$row->KitName."</td>";
                            echo "<td>".$row->Notes."</td>";
                            echo "<td>
                                <div class='btn-group pull-right'> 
                                    <button type='button' class='btn btn-default btn-sm dropdown-toggle' data-toggle='dropdown'>
                                        Action <span class='caret'></span> 
                                    </button>
                                    <ul class='dropdown-menu dropdown-default pull-right' role='menu'> 
                                        <li> <a href='#' onclick='showProductModal(".$row->KitID.")'> <i class='entypo-drive'></i> Products </a> </li> 
                                        <li class='divider'></li> 
                                        <li> <a href='#' onclick='showEditModal(".$row->KitID.")'> <i class='entypo-pencil'></i> Edit </a> </li> 
                                        <li class='divider'></li> 
                                        <li> <a href='#' onclick='showDeleteModal(".$row->KitID.")'> <i class='entypo-trash'></i> Delete </a> </li>' 
                                    </ul>
                                </div>
                            </td>";
                        echo "</tr>";
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <div class="tab-pane" id="add-kit">
                <form class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url()?>administrator/kits" id="form-kits">
            

                    
                   

                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Kit:</label>
                        <div class="col-sm-5">
                            <input type="text"  name="KitName" id="KitName" class="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Notes:</label>
                        <div class="col-sm-5">
                            <textarea  name="Notes" id="Notes" class="form-control"></textarea>
                            
                        </div>
                    </div>

                    

                    <div class="form-group">
                        <label  class="col-sm-3 control-label">Products</label>
                        <div class="col-sm-9">
                          <table class="table table-bordered datatable" id="kit_products">
                            <thead>
                                <tr>
                                    <th>Productid</th>
                                    <th>Product</th>
                                    <th>Quantity</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                          </table>
                        </div>
                        <div class="col-sm-2  pull-right" >
                            <button type="button" class="btn btn-info form-control" id="add_product">Add Product</button>  
                        </div>
                    </div>

                    

                    


                

  
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" class="btn btn-primary" >Insert Kit</button>
                            
                            <input type="hidden" name="action" value="insert">
                        </div>
                    </div>
            

            
            
                </form>
            </div>
        </div>

        
    </div>
</div>


<!-- Modal Edit -->

<div class="modal fade" id="modal-product-list" data-backdrop="static" aria-hidden="true" style="display: none;"> 
    <div class="modal-dialog"> 
        <div class="modal-content"> 
            <div class="modal-header"> 
                <h4 class="modal-title">Kits Products</h4> 
            </div> 
            <div class="modal-body">
                <div class="row">  </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div> 
        </div> 
    </div> 
</div>




<div class="modal fade" id="modal-edit" data-backdrop="static" aria-hidden="true" style="display: none;"> 
    <div class="modal-dialog"> 
        <div class="modal-content"> 
            <div class="modal-header"> 
                <h4 class="modal-title">Confirm Modal</h4> 
            </div> 
            <div class="modal-body">
                <div class="row">  </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div> 
        </div> 
    </div> 
</div>

<div class="modal fade" id="modal-delete" data-backdrop="static" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content" style="margin-top:100px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" style="text-align:center;">Are you sure to delete this information ?</h4>
            </div>

            <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade custom-width" id="modal-product">
    <div class="modal-dialog" style="width: 60%;">
        <div class="modal-content">
          
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add Product</h4>
            </div>
          
            <div class="modal-body">
                <form class="form-horizontal form-groups-bordered validate" id="form-add-product" target="_top">
                
                    <div class="row">
                        
                        <div class="form-group">
                            <label  class="col-sm-3 control-label">Product</label>
                            <div class="col-sm-5">
                                <select class="form-control" id="productid" name="productid" required>
                                    <option></option>    
                                        <?php
                                        $query = $this->db->query("
                                        SELECT 
                                        product.ProductID, 
                                        product.ProductName, 
                                        product.HasBarcodeID
                                        FROM Product
                                        ORDER BY product.ProductName");
                            
                                        foreach ($query->result() as $row) { ?>
                                            <option value="<?php echo $row->ProductID ?>" hasbarcode="<?php echo $row->HasBarcodeID?>"><?php echo $row->ProductName?></option>
                                        <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group"  id="field_qty">
                            <label  class="col-sm-3 control-label">Quantity</label>
                            <div class="col-sm-5">
                                <input type="text" name="qty" id="qty"   class="form-control" required /> 
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="col-sm-offset-3 col-sm-5">
                                <input type="hidden" name="product" id="product"   class="form-control">
                                <button type="submit" class="btn btn-blue" id="insert-product" >Add Prodcuct</button>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
          
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>

<script type="text/javascript">


jQuery(document).ready(function ($) {

   

    var kits_table = jQuery('#kits-table').DataTable({ 
       
        
        //"paging": false,
        //sDom: "lrtip"
    });

    var kit_products = jQuery("#kit_products").DataTable();



    jQuery("#add_product").on('click',function(){
      
        jQuery('#modal-product').modal('show');
        jQuery('#modal-product').on('show.bs.modal', function () {
        

            jQuery("#productid").val(0);
            jQuery("#product").val('');
            jQuery("#qty").val('');
           


        });
      
    });

    jQuery("#form-add-product").submit(function(event ){
        event.preventDefault();
      
        var values = {};
        values['product'] = jQuery("#productid option:selected").text();
        values['productid'] = jQuery("#productid").val();
        

        if(jQuery("#qty").val() == ""){
            values['qty'] = "";
        } else{
            values['qty'] = jQuery("#qty").val();
        }
        

       
        var rowNode = kit_products
            .row.add( [ values['productid'],values['product'],  values['qty'] ] )
            .draw()
            .node();
        
        jQuery('#modal-product').modal('hide');
      

    });


    jQuery("#form-kits").submit(function(event){
        

        //event.preventDefault();
        //alert("form sbmitting");
        var product_data = kit_products
            .rows()
            .data();
       
        var values = {};
        jQuery.each(jQuery(this).serializeArray(), function(i, field) {
            values[field.name] = field.value;
        });
        values['products'] = new Array();

        for(i = 0; i<product_data.length; i++){
           values['products'][i] = {'productid':product_data[i][0],'product':product_data[i][1],'qty':product_data[i][2]};
        }

        


        jQuery.ajax({
          url: '<?php echo base_url()?>administrator/kits',
          type: 'post',
          dataType: 'json',
          data: values,
          success: function(resp) {
            if(resp.status == 1)
             
             document.location = '<?php echo base_url()?>administrator/kits';
          }
        });

        return false;



    });


    
   


});





function showProductModal(KitID)

{ 
    var priceGroupId = jQuery("#price-group-select").val();
    var stringUrl = "<?=  base_url().'modals/kitProductList/' ?>" + KitID ; 
    //console.log("string url : " + stringUrl);
    jQuery('#modal-product-list').find('.modal-body').html("<div class='row'><image src='<?= base_url().'assets/img/indicator.gif' ?>'/></div>");
    jQuery.ajax({       
        type: 'get', 
        url: stringUrl,
        success: function(data)
        {  
            jQuery('#modal-edit').find('.modal-body').html(data); 
        },
        error :function(jqXHR, textStatus, errorThrown ) {

        }  
    });
    jQuery('#modal-edit').modal('show', {backdrop: 'static'});
} 

function showEditModal(KitID)

{ 
    var priceGroupId = jQuery("#price-group-select").val();
    var stringUrl = "<?=  base_url().'modals/kitEdit/' ?>" + KitID ; 
    //console.log("string url : " + stringUrl);
    jQuery('#modal-edit').find('.modal-body').html("<div class='row'><image src='<?= base_url().'assets/img/indicator.gif' ?>'/></div>");
    jQuery.ajax({       
        type: 'get', 
        url: stringUrl,
        success: function(data)
        {  
            jQuery('#modal-edit').find('.modal-body').html(data); 
        },
        error :function(jqXHR, textStatus, errorThrown ) {

        }  
    });
    jQuery('#modal-edit').modal('show', {backdrop: 'static'});
} 


function showDeleteModal(KitID)
{ 
   var stringUrl = "<?= base_url().'administrator/kits'?>" ; 
    jQuery('#modal-delete').find('.modal-body').html("<div class='row'><image src='<?= base_url().'assets/img/indicator.gif' ?>'/></div>");
    jQuery('#modal-delete').find('.modal-footer').html('<form action="'+stringUrl+'"  method="post" accept-charset="utf-8" class="form-horizontal form-groups-bordered validate" target="_top" enctype="multipart/form-data">'+
                    '<input name="orderDetailId" type="hidden" id="deleteOrderDetailId"/>'+
                    '<button type="submit" class="btn btn-danger" id="delete_link">delete</a>'+
                    '<button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>'+
                    '<input type="hidden" name="KitID" id="KitID" value="'+KitID+'" class="form-control" />'+
                    '<input type="hidden" name="action" value="delete">'+
                '</form>'); 
    jQuery('#modal-delete').modal('show', {backdrop: 'static'});
} 


</script>

