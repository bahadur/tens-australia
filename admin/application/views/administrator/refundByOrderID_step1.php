<div class="row">
    <div class="col-md-12">
        
        <form class="form-horizontal " id="form-orderid">
        
            <div class="form-group ">
                <label class="col-sm-3 control-label">Order ID:</label>
                <div class="col-sm-5">
                    <input type="text"  name="orderid" id="orderid" class="form-control" />
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-5">
                    <button type="submit" class="btn btn-primary" id="orderid_click" >Go</button>
                
                <input type="hidden" name="action" value="insert">
                </div>
            </div>    
            
        </form>
        

        
    </div>
</div>





<script type="text/javascript">
jQuery(document).ready(function ($) {

   jQuery("#form-orderid").submit(function(e){
        e.preventDefault();
        
        if(jQuery('#orderid').val() == ""){
            jQuery('#orderid').parent().parent().addClass('has-error');
        } else{

        document.location = '<?php echo base_url()?>administrator/refundByOrderID/'+jQuery('#orderid').val();
        }

        
   });

   


});
</script>

