<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Tens Australia Back Office" />
    <meta name="author" content="" />

    <title>Tens Australia Back Office</title> 

    <link rel="stylesheet" href="<?= base_url().'assets/css/styles.css'?>">
    <link rel="stylesheet" href="<?= base_url().'assets/neon/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css'?>">
    <link rel="stylesheet" href="<?= base_url().'assets/neon/css/font-icons/entypo/css/entypo.css'?>">
    <link rel="stylesheet" href="<?= base_url().'assets/neon/css/font-icons/font-awesome/css/font-awesome.min.css'?>">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
    <link rel="icon" href="<?=base_url()?>../assets/images/favicon.png" type="image/png">
    

    
	<link rel="stylesheet" href="<?php echo base_url();?>assets/neon/css/neon-core.css">
    <link rel="stylesheet" href="<?= base_url().'assets/neon/css/neon-theme.css'?>">
    <link rel="stylesheet" href="<?= base_url().'assets/neon/css/neon-forms.css'?>">
    <link rel="stylesheet" href="<?= base_url().'assets/neon/css/custom.css'?>">
    <link rel="stylesheet" href="<?= base_url().'assets/neon/css/skins/blue.css'?>"> 
   
    
    <link rel="stylesheet" href="<?= base_url().'assets/neon/css/bootstrap.css'?>">
    <link rel="stylesheet" href="<?= base_url().'assets/neon/js/select2/select2.css'?>">
    <link rel="stylesheet" href="<?= base_url().'assets/neon/js/select2/select2-bootstrap.css'?>">
     <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.css" >
    

    
    <script src="<?= base_url().'assets/jquery-1.11.2.min.js'?>"></script>
    <script src="<?= base_url().'assets/moment/moment.min.js'?>"></script>
    <script src="<?= base_url().'assets/jquery.jeditable.js'?>"></script>
    <script src="<?= base_url().'assets/jquery.jeditable.inputs.js'?>"></script>
    
    <script src="<?= base_url().'assets/neon/js/jquery.dataTables.min.js'?>"></script>
    <script src="<?= base_url().'assets/neon/js/datatables/TableTools.min.js'?>"></script>

    <script src="<?= base_url().'assets/neon/js/jquery.validate.min.js'?>"></script>
    <?php 
        if (isset($noBootstrap) && $noBootstrap) 
        {
        }
        else 
        {
            echo "<script src=\"". base_url()."assets/neon/js/bootstrap.min.js\"></script>";
        }
    ?>
    <script src="<?= base_url().'assets/neon/js/bootstrap-datepicker.js'?>"></script>
    <script src="<?= base_url().'assets/neon/js/dataTables.bootstrap.js'?>"></script>
    
    <script src="<?= base_url().'assets/neon/js/select2/select2.min.js'?>"></script>
  
    <script src="<?= base_url().'assets/js/tensApi/tensDatesFormatter.js'?>"></script>

    <script>$.noConflict();</script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>
<body class="page-body" >

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
    

