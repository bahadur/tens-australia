<div class="col-lg-12">
    <!--***********************************************************************-->
    <!-- contents for every page starts here -->
    <div class="row">
    	<div class="panel panel-primary">
    		<div class="panel-heading">
    			<div class="panel-title">Confirm Orders</div>
    		</div>
	    	<div class="panel-body">
	    		<div class="col-md-12">
		    		<table class="table table-bordered" id="table-orders">
		    			<thead>
		    				<tr>
			    				<th>Confirm Order Id</th>
			    				<th>Status</th>
			    				<th>Customer</th>
			    				<th>Application Date</th>
			    				<th>Required Date</th>
			    				<th>Due Date</th>
			    				<th>Action</th>

		    				</tr>
		    			</thead> 
		    		</table>
		    	</div>
	    	</div>
    	</div>

    </div>
</div>

<script type="text/javascript">

function reconcile(unconfirmedOrderId)
{
	window.location = '<?php echo base_url()."confirmorders/view/unconfirmedOrderView/" ?>' + unconfirmedOrderId;
};


                
jQuery("#table-orders").DataTable({
		processing: true,
        serverSide: true,
        responsive: true,
        "ajax": "<?php echo base_url().'confirmorders/api/getUnconfirmedOrders'?>",
        "aoColumnDefs": 
		[ 
			{   
				"aTargets": [ 0 ],   
			 	"mRender": function ( data, type, full ) { 
					return '<a href="<?php echo base_url()."confirmorders/view/unconfirmedOrderView" ?>">' + data + '</a>';
		  	}
	  	},
			{   
				"aTargets": [ 1 ]  ,   
			 	"mRender": function ( data, type, full ) {
			 		// If HasBarcodeId == 1 => enable editing barcode, disable quantity, else, blah!  
			 		if (data == 2)
			 		{
			 			return "Confirmed";
			 		}
			 		else 
			 		{
			 			return "Unconfirmed";
			 		} 
			  	}
			},
			{   
				"aTargets": [ 2 ]  
			},
			{   
				"aTargets": [ 3 ]  
			},
			{  
				"aTargets": [ 4 ]
			},
			{  
				"aTargets": [ 5 ]
			},
			{  
				"aTargets": [ 6 ],
			 	"mRender": function ( data, type, full ) {
			 	// If HasBarcodeId == 1 => enable editing barcode, disable quantity, else, blah!  
			 	console.log(full);
					return '<button class="btn btn-info reconcile-btn" onclick="reconcile(' + full[0] + ')">Reconcile</btn>';
			  	}
			}
		]
});
</script>
