<link rel="stylesheet" href="<?= base_url().'assets/neon/js/select2/select2.css'?>">   
<script src="<?= base_url().'assets/neon/js/select2/select2.min.js'?>"></script> 
<script src="<?= base_url().'assets/neon/js/jquery.validate.min.js'?>"></script> 

<form action="<?php echo base_url() ?>orders/createOrder" method="POST" id="confirmOrderForm"  
    class="form-groups-bordered"
    onsubmit="return false">
	<div class="col-md-12">
         <ul class="nav nav-tabs bordered" id="tabs">
                    <li class="active"><a href="#customer" data-toggle="tab">Customer Details</a></li>
                    <li><a href="#order" data-toggle="tab">Order Details</a></li> 
                    <li><a href="#products" data-toggle="tab">Order Items</a></li> 

        </ul>
        <div class="tab-content" id="my-tab-content">
            <div class="tab-pane active" id="customer">
                    
                    <?php 
                        $this->load->view('unconfirmedOrders/tabs/customerDetailsTab');
                    ?>

            </div>
            <div class="tab-pane" id="order">
                    
                    <?php 
                        $this->load->view('unconfirmedOrders/tabs/orderDetailsTab');
                    ?>

            </div>
            <div class="tab-pane" id="products">
                    
                    <?php 
                        $this->load->view('unconfirmedOrders/tabs/orderProductsTab');
                    ?>

            </div>
        </div>
    <!--***********************************************************************-->
    <!-- contents for every page starts here -->
    </div>
</form>

<script type="text/javascript"> 

</script>