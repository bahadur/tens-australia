<div class="col-lg-9">
                <!--***********************************************************************-->
                <!-- contents for every page starts here -->
                <div class="row">
                    <div class="col-lg-12">&nbsp;</div>
                </div>                
				<div class="row">
                    <div class="col-lg-12">
						
												
					</div>
					<div class="col-lg-12" >
						<div class="panel panel-primary"  style="width:160%">
							<div class="panel-heading">
								<div class="panel-title">
										Delivery Suite Support Program Equipment List
								</div>
							</div>
												
							<div class="panel-body">
								<table class="table table-bordered" id="">
										<thead>
											<th>Order ID</th>
											<th>Customer</th>
											<th>Product Name</th>
											<th>Temp BarCode</th>
											<th>Invoice Date</th>
											<th>Days</th>
											<th>Return Expected Date</th>
										</thead>
										<tbody>
												<?php foreach ($suiteProductResult as $result){?>
												<tr> 
													
													<td ><input type = "text"  id ="OrderID"  value="<?php echo $result->OrderID;?>" style="width:80px;" disabled></td>
													<td ><input type = "text"  id = "Customer"  value="<?php echo $result->Customer;?>" style="width:200px;" disabled ></td>
													<td ><input type = "text"  id = "ProductName"   value="<?php echo $result->ProductName;?>" style="width:200px;" disabled ></td>
													<td><input type = "text" id = "TempBarcode" value="<?php echo $result->TempBarcode;?>" style="width:150px;" disabled ></td>
													<td ><input type = "text"  id = "InvoiceDate"   value="<?php $InvoiceDate = $result->InvoiceDate; $Idate = new DateTime($InvoiceDate);echo $Idate->format('Y-m-d'); ?>" style="width:120px;" disabled>
														<!--<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
														</span>-->
													</td>
													<td><input type = "text"  id = "Days"   value="<?php echo $result->Days;?>" style="width:100px;" disabled ></td>
													<td>
														<input type = "text"  id = "ReturnExpectedDate" value="<?php echo $result->ReturnExpectedDate;?>" style="width:150px;" disabled>
															<!--<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
															</span>-->
													</td>
													
													
												</tr>
												<?php }?>
										</tbody>
								</table>
								
							</div>
						</div>
					</div>
                </div>
            <!-- contents for every page ends here -->
            <!--***********************************************************************-->
</div>      
            

<!-- add the below lines on every content pages -->            
        </div>
    </div>
        <div class="col-lg-1"></div>
    </div><!-- end of row in navbar view -->
</div><!-- end of container in navbar view -->

<!--<script type="text/javascript">
			jQuery('#InvoiceDate').datepicker({
                format: "yyyy-mm-dd"
            });
			jQuery('#ReturnExpectedDate').datepicker({
                format: "yyyy-mm-dd"
            });
</script>

