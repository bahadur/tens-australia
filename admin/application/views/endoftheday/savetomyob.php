<?php

class MyOBRow {  
    public $strJournalNo;
    public $strDate;
    public $strMemo;
    public $strGST;
    public $strInclusive;
    public $strAccountNumber;
    public $strCreditExTax;
    public $strCreditIncTax;
    public $strTaxCode;
    public $strCreditTaxAmount; 
    public $strDebitAccountNumber;
	public $curTotalDebitIncTax;


    function MyOBRow() { 
        
    } 
}  

if (isset($_GET['transactionDate']))
{
	$transactionDate = $_GET['transactionDate'];
	if (!validateDate($transactionDate, 'd-m-Y'))
	{
		echo "Wrong date format.";
		exit;
	} 

    $newTransactionDate = $transactionDate.' 00:00:00';
    $start_date = $this->projectdates->parseToMySQLMyobDate($newTransactionDate);
    $stop_date = date('Y-m-d H:i:s', strtotime($start_date) + 86399);
	$queryStr = 'SELECT orderdetail.OrderID AS JournalNumber, transaction.TransactionTypeID, transaction.TransactionDate AS `Date`, 
		CONCAT(`paymentmethod`,"\, ", `transaction`.TransactionID, "\, ", TransactionReference,"\, ", LastName, "\, ", Firstname) AS Memo, 
		transactiondetail.AccountCode AS AccountNumber, SUM(transactiondetail.GST_Amount) AS CreditTaxAmount, SUM(IFNULL(Amount, 0)) AS CreditIncTaxAmount,
	 	taxcode.TaxCode, 
	 	IF(CHARACTER_LENGTH (CONCAT ("",Terminal) ) =0,"Z",SUBSTRING(Terminal,1, 1)) AS TerminalCode
		FROM transactiontype INNER JOIN (taxcode INNER JOIN (product INNER JOIN (paymentmethod INNER JOIN (((customer INNER JOIN `order` 
		ON customer.CustomerID = order.CustomerID) INNER JOIN (orderdetail INNER JOIN transactiondetail 
		
		ON orderdetail.OrderDetailID = transactiondetail.OrderDetailID) ON order.OrderID = orderdetail.OrderID) 
		INNER JOIN (`transaction` LEFT JOIN terminal ON transaction.TerminalID = terminal.TerminalID) 
		ON (transaction.TransactionID = transactiondetail.TransactionID) AND (order.OrderID = transaction.OrderID)) 

		ON paymentmethod.PaymentMethodID = transaction.PaymentMethodID) ON product.ProductID = orderdetail.ProductID) 
		ON taxcode.TaxCodeID = product.TaxCodeID) ON transactiontype.TransactionTypeID = transaction.TransactionTypeID
		GROUP BY orderdetail.OrderID, transaction.TransactionTypeID, transaction.TransactionDate, 

		CONCAT(`paymentmethod`, "\, ", `transaction`.TransactionID, "\, ", `TransactionReference`, "\, ", LastName, "\, ", Firstname), 
		transactiondetail.AccountCode, taxcode.TaxCode, IF(CHARACTER_LENGTH( CONCAT("", Terminal) )=0,"Z",SUBSTRING(Terminal, 1, 1))
		HAVING ((transaction.TransactionDate BETWEEN \''.$start_date.'\' AND \''.$stop_date.'\'))
		ORDER BY orderdetail.OrderID, transaction.TransactionTypeID;'; 
        echo $queryStr;
	$query = $this->db->query($queryStr);

	$fName = "MYOB Transactions ".$transactionDate." Created ".time().".txt";
	$fHandle = fopen($fName, "w") or die("Unable to open file!");
	$mysqlResult = $query->result();  
    $totalItems = sizeof($mysqlResult);
	if ($totalItems > 0)
	{
		writeHeaders($fHandle);
		$i = 0; 
		$row = $mysqlResult[$i]; 
        $intTransactionType = $row->TransactionTypeID;
        $strJournalPrefix = $row->TerminalCode;
        If ($intTransactionType == 1) 
        {
            $strJournalPrefix = $strJournalPrefix."P";
        }
        Else
        {
            $strJournalPrefix = $strJournalPrefix."R";
        } 
        $myOBRowItem = new MyOBRow();
        $intJournalNo = $row->JournalNumber;
        $myOBRowItem->strJournalNo = $strJournalPrefix."".sprintf("%'.06d", $intJournalNo);//Format($intJournalNo, "000000")

        $phpdate = strtotime( $row->Date );
        $myOBRowItem->strDate = date('j/m/Y', $phpdate ); 
        $myOBRowItem->strMemo = $row->Memo;
        $myOBRowItem->strAccountNumber = $row->AccountNumber;
        $myOBRowItem->strTaxCode = $row->TaxCode;
        
        $curCreditIncTax = $row->CreditIncTaxAmount;
        $curCreditTaxAmount = $row->CreditTaxAmount;
        $curCreditExTax = $curCreditIncTax - $curCreditTaxAmount;
         
        $myOBRowItem->strCreditIncTax = sprintf("$%.2f", $curCreditIncTax);
        $myOBRowItem->strCreditTaxAmount = sprintf("$%.2f", $curCreditTaxAmount);//"$".$curCreditTaxAmount;
        $myOBRowItem->strCreditExTax = sprintf("$%.2f", $curCreditExTax);//"$".$curCreditExTax;

        $myOBRowItem->strGST = "S";
        $myOBRowItem->strInclusive = "X";
        $myOBRowItem->strDebitAccountNumber = "1-0180";
            
        $myOBRowItem->curTotalDebitIncTax = 0; 
		while (!is_null($row))
		{
	        writeMyobLine($intTransactionType, $myOBRowItem, $fHandle);

	    	//increment the total for the debit/Credit row
	    	$myOBRowItem->curTotalDebitIncTax = $myOBRowItem->curTotalDebitIncTax + $curCreditIncTax;  

	    	//  rsMYOB.MoveNext 
			$i++;
			if ($i < $totalItems)
			{
				$row = $mysqlResult[$i];
			}
			else 
			{
				$row = null;
			}

	    	if (!is_null($row))
	    	{
				if ($intJournalNo != $row->JournalNumber || 
					$intTransactionType != $row->TransactionTypeID)
				{
					//insert the debit/Credit record from the previous order
                    writeMyobEndingLine($intTransactionType, $myOBRowItem, $fHandle);
                    writeEmptyLine($fHandle);
                    writeEmptyLine($fHandle);


                    // store new journal no
                    // is this an payment or a Credit
                    $intTransactionType = $row->TransactionTypeID;

                    $strJournalPrefix = $row->TerminalCode;
                    if ($intTransactionType == 1)
                    {
                        $strJournalPrefix = $strJournalPrefix."P";
                    }
                    else 
                    {
                        $strJournalPrefix = $strJournalPrefix."R";
                    }

                    //store data
                    $intJournalNo = $row->JournalNumber;
                    $myOBRowItem->strJournalNo = $strJournalPrefix.sprintf("%'.06d", $intJournalNo);

                    // zero the totals
                    $myOBRowItem->curTotalDebitIncTax = 0;
				} 
                // store data  NB don't get the journal number yet 
		        $phpdate = strtotime( $row->Date );
		        $myOBRowItem->strDate = date('j/m/Y', $phpdate ); 
                $myOBRowItem->strMemo = $row->Memo;
                $myOBRowItem->strAccountNumber = $row->AccountNumber;
                $myOBRowItem->strTaxCode = $row->TaxCode;
                
                $curCreditIncTax = $row->CreditIncTaxAmount;
                $curCreditTaxAmount = $row->CreditTaxAmount;
                $curCreditExTax = $curCreditIncTax - $curCreditTaxAmount;
                
                $myOBRowItem->strCreditIncTax = sprintf("$%.2f", $curCreditIncTax);
                $myOBRowItem->strCreditTaxAmount = sprintf("$%.2f", $curCreditTaxAmount);
                $myOBRowItem->strCreditExTax = sprintf("$%.2f", $curCreditExTax); 
			} 
		} 
        writeMyobEndingLine($intTransactionType, $myOBRowItem, $fHandle); 
	}
	else 
	{
		echo "No records for ".$transactionDate;
	}
	fclose($fHandle);  
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename='.basename($fName));
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($fName));
    readfile($fName);
    if (file_exists($fName)) {
        unlink($fName);
    }
    exit;
}

function validateDate($date, $format)
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}

function writeHeaders($fHandle)
{
	fwrite($fHandle, '"Journal Number","Date","Memo","GST [BAS] Reporting","Inclusive","Account Number","Debit Ex-Tax Amount","Debit Inc-Tax Amount","Credit Ex-Tax Amount","Credit Inc-Tax Amount","Job","Tax Code","Non-GST/LCT Amount","Tax Amount","LCT Amount","Import Duty Amount","Currency Code","Exchange Rate","Allocation Memo","Category"');
	fwrite($fHandle, "\n");
}

function writeEmptyLine($fHandle)
{
	fwrite($fHandle,"\n");
}

function writeMyobLine($intTransactionType, $myOBRowItem, $fHandle)
{
	if ($intTransactionType == 1)
    {
    	/* Write #1, strJournalNo, strDate, strMemo, strGST,
    	strInclusive, strAccountNumber, , , strCreditExTax, strCreditIncTax, , 
    	strTaxCode, "$0.00", strCreditTaxAmount, "$0.00", "$0.00", , , ,
        */
        fwrite($fHandle, "\"$myOBRowItem->strJournalNo\",\"$myOBRowItem->strDate.\",\"$myOBRowItem->strMemo\",\"$myOBRowItem->strGST\",\"$myOBRowItem->strInclusive\",\"$myOBRowItem->strAccountNumber\",\"\",\"\",\"$myOBRowItem->strCreditExTax\",\"$myOBRowItem->strCreditIncTax\",\"\",\"$myOBRowItem->strTaxCode\",\"$0.00\",\"$myOBRowItem->strCreditTaxAmount\",\"$0.00\",\"$0.00\",\"\",\"\",\"\"");  
		fwrite($fHandle,"
"); 
	}
	else 
	{
		/*
			Write #1, 
			strJournalNo, strDate, strMemo, strGST, strInclusive, strAccountNumber, 
			strCreditExTax, strCreditIncTax, , , , strTaxCode, "$0.00", strCreditTaxAmount, "$0.00", "$0.00", , , ,
        
		*/
        fwrite($fHandle,"\"$myOBRowItem->strJournalNo\",\"$myOBRowItem->strDate.\",\"$myOBRowItem->strMemo\",\"$myOBRowItem->strGST\",\"$myOBRowItem->strInclusive\",\"$myOBRowItem->strAccountNumber\",\"$myOBRowItem->strCreditExTax\",\"$myOBRowItem->strCreditIncTax\",\"\",\"\",\"\",\"$myOBRowItem->strTaxCode\",\"$0.00\",\"$myOBRowItem->strCreditTaxAmount\",\"$0.00\",\"$0.00\",\"\",\"\",\"\",\"\"");  
		fwrite($fHandle,"
"); 
	} 
}


function writeMyobEndingLine($intTransactionType, $myOBRowItem, $fHandle)
{
	if ($intTransactionType == 1) 
    {
		/* 
            Write #1, strJournalNo, strDate, strMemo, strGST, strInclusive, strDebitAccountNumber, 
            Format(curTotalDebitIncTax, "Currency"), Format(curTotalDebitIncTax, "Currency"), , , , 
            "N-T", "$0.00", "$0.00", "$0.00", "$0.00", , , ,
    	*/
        fwrite($fHandle,"\"$myOBRowItem->strJournalNo\",\"$myOBRowItem->strDate.\",\"$myOBRowItem->strMemo\",\"$myOBRowItem->strGST\",\"$myOBRowItem->strInclusive\",\"$myOBRowItem->strDebitAccountNumber\",\"".sprintf("$%.2f", $myOBRowItem->curTotalDebitIncTax)."\",\"".sprintf("$%.2f", $myOBRowItem->curTotalDebitIncTax)."\",\"\",\"\",\"\",\"N-T\",\"$0.00\",\"$0.00\",\"$0.00\",\"$0.00\",\"\",\"\",\"\",\"\"");  
		fwrite($fHandle,"
"); 
    }
    else
    {
		/* 
			Write #1, strJournalNo, strDate, strMemo, strGST, strInclusive, strDebitAccountNumber, , , 
			Format(curTotalDebitIncTax, "Currency"), Format(curTotalDebitIncTax, "Currency"), , 
			"N-T", "$0.00", "$0.00", "$0.00", "$0.00", , , ,
    	*/
		fwrite($fHandle,"\"$myOBRowItem->strJournalNo\",\"$myOBRowItem->strDate.\",\"$myOBRowItem->strMemo\",\"$myOBRowItem->strGST\",\"$myOBRowItem->strInclusive\",\"$myOBRowItem->strDebitAccountNumber\",\"\",\"\",\"".sprintf("$%.2f", $myOBRowItem->curTotalDebitIncTax)."\",\"".sprintf("$%.2f", $myOBRowItem->curTotalDebitIncTax)."\",\"\",\"N-T\",\"$0.00\",\"$0.00\",\"$0.00\",\"$0.00\",\"\",\"\",\"\",\"\"");   
		fwrite($fHandle,"
"); 
    }
}
 
?>
 