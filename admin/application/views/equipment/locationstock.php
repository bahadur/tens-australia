<div class="row">
        <div class="col-md-12">
            
            <div class="panel panel-primary" >
            
                <div class="panel-heading">
                    <div class="panel-title">
                        Move Equipment
                    </div>
                </div>
                
            <div class="panel-body">
                <table id="stocktable"  class="table table-bordered datatable">
                	<thead>
                		<tr>
                			<th>Location</th>
                			<th>Product</th>
                			<th>Barcode</th>
                			<th>Quantity</th>
                		</tr>
                	</thead>
                	<tbody>
                		<?php 
                		foreach ($locationstocks as $locationstock)
                		{
                		echo '<tr>';
                			echo '<td>'.$locationstock->LocationDescription.'</td>';
                			echo '<td>'.$locationstock->ProductName.'</td>';
                			echo '<td>'.$locationstock->NULL.'</td>';
                			echo '<td>'.$locationstock->Quantity.'</td>';
                		echo '</tr>';
                		}
                		?>
                	</tbody>
                </table>
            </div>

        </div>
	</div>
</div>
<script type="text/javascript">

jQuery("#stocktable").dataTable();
</script>