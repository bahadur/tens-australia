<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" >
			<div class="panel-heading">
				<div class="panel-title">
					Equip Purchase Order
				</div>
			</div>
											
			<div class="panel-body">
				<form action="" class="" method="post"  name="purchaseOrderForm">
					
					<div class="row">
						<div class="col-md-offset-1 col-md-10">
							
					
							<div class="form-group">
								<label class="control-label"><h4>Form Purchase Order No:</h4></label>
								<input type="text" class="form-control" id="FormPurchaseOrderNo" name="FormPurchaseOrderNo" autocomplete="off" required>
							</div>
							
							<div class="form-group">
								<label class="control-label"><h4>To Location:</h4></label>
								<select class="form-control" name="ToLocation" id="ToLocation" required autocomplete="off">
                <option></option>
									<?php foreach ($ToLocationResult as $LocationResult){?>
									<option value= "<?php echo $LocationResult->LocationID; ?>">
									<?php echo $LocationResult->LocationDescription;?>
									</option>
									<?php }?>
								</select>
							</div>
												
							<div class="form-group">
								<label class="control-label"><h4>Move Date:</h4></label>
								<div class='input-group date' id='datetimepicker'>
									<input type='text' class="form-control" name="MoveDate" id="move-date" autocomplete="off" required />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label"><h4>Comments:</h4></label>
								<textarea class="form-control" rows="3" autocomplete="off" name="Comments"></textarea>
							</div>
					
							<div class="form-group">
		                    	<div class="pull-right">
				                	<button type = "button"  id="purchaseOrderButton"  class = "btn btn-default" >Add an item</button>
				                </div>
				            </div>

				            <div class="form-group">
			                	<label  class="control-label"><h4>Products</h4></label>
								<table class="table table-bordered" id="purchase-items-table">
									<thead>
									 	<th>Productid</th>
										<th>Product</th>
										<th>Barcode</th>
										<th>Quantity</th>
										<th>Action</th>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
							<input type="submit"  id="" class = "btn btn-success" onclick="sucess_alert();" style="margin-top: 10px;float:left;" value="save">
							<button type = "button"  class = "btn btn-info" style="margin-top:10px;float:right;">Cancel</button>
						</div>
					</div>		
				</form>

											
			</div>
		</div>
	</div>
		<!-- End Of Swap From Product Information Table-->
</div>

		<!--Purchase Modal Start From Here-->
<div class="modal fade" id="PurchaseModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">Add A Purchase Order</h4>
			</div>
			
			<div class="modal-body">
				<form class="form-horizontal" id="modal-add-product" target="_top">
					
					<div class="form-group ">
						<label class="col-md-2 control-label">Product:</label>
						<div class="col-md-9" style="">
							<input id="productid" class="form-control" autocomplete="off" name="productid" required/>
						</div>
					</div>
					
					<div class="form-group" id="field_barcode"  style="display:none">
						<label class="col-md-2 control-label" >Barcode:</label>
						<div class="col-md-9">
							<input type="text" class="form-control" id="barcode" name="barcode" required>
						</div>
					</div> 
					
					<div class="form-group " id="field_qty" style="display:none">
						<label class="col-md-2 control-label" >Quantity:</label>
						<div class="col-md-9">
							<input type="text" class="form-control" id="qty" name="qty" required>
						</div>
					</div> 
				</form>
			</div>
			
			<div class="modal-footer">
				<input type="hidden" name="product" id="product"   class="form-control">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" autocomplete="off" class="btn btn-primary" id="insert-barcode-product">Add the item</button>
        <button type="button" autocomplete="off" class="btn btn-primary" id="insert-quantity-product" style="display:none">Add the item</button>
			</div>
		</div>
	</div>
</div>
<!--Purchase Modal End  Here-->

<script type="text/javascript" src="<?php echo base_url();?>assets/bootstrap/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/tensApi/tensApiDateFormatter.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/tensApi/lazyLoadingButton.js"></script>
<script type="text/javascript">


function PurchaseOrderVM() {
  var self = this;
  self.df = new DateTimeFormatter();
  function initializeDatatable() { 
    self.itemsTable = jQuery("#purchase-items-table").DataTable({
        "sDom": 't',
        "aoColumns" : [
          {"data" : "ProductID", "sClass": "hide_column"},
          {"data" : "ProductName"},
          {"data" : "Barcode"},
          {"data" : "Quantity"},
          {"data" : "ProductID",
            "mRender": function(data, type, full) {
              return '<a href="#" class="btn btn-primary">Edit</a>'
            }}
        ]
      }); 
  };

  function initializeSelects() { 
    jQuery('#ToLocation').select2({
      "placeholder": 'Select a location'
    });
    jQuery('#productid').select2({ 
      "ajax": {
          "url": "<?php echo base_url().'products/formapi/getProductsSelect'?>",
          "type": 'POST',  
          "delay": 250,
          "results": function(data, page) { 
              return {
                  results: data
              };
          },   
          data: function (term, page) {
              return {
                  "q": term,
                  "page" : page
              };
          },
          "cache" : true
      },
      initSelection: function(element, callback) {
        callback({"id" : -1, "text" : "Please select a product"})
      },
      "placeholder": 'Select a product'
    })
    .on('change', function(e) {
      var selectedData = jQuery(this).select2('data');
      console.log(selectedData);
      if (selectedData.HasBarcodeID * 1 == 1) { 
        jQuery('#field_qty').hide();
        jQuery('#field_barcode').show();
        jQuery('#insert-quantity-product').hide();
        jQuery('#insert-barcode-product').show();
      } else {
        console.log('Not 1');
        jQuery('#field_qty').show();
        jQuery('#field_barcode').hide();
        jQuery('#insert-quantity-product').show();
        jQuery('#insert-barcode-product').hide();
      }
    }); 
  };

  function validateNewItem() {
    var selectedData = jQuery('#productid').select2('data');
    if (selectedData.HasBarcodeID == 1) {
      var barcode = jQuery('#barcode').val();
      if (barcode.length > 0) {
        var dt = jQuery('#purchase-items-table').dataTable().fnGetData();
        for (var k = 0; k < dt.length; k++) {
          if (dt[k].Barcode === barcode) {
            alert('This barcode has already been added.');
            return false;
          }
        }
        return true;
      } else {
        alert('Please enter a barcode.');
        return false;
      }
    } else {
      if (jQuery.isNumeric(jQuery('#qty').val())) {
        return true;
      } else {
        alert('Please enter a valid number.');
        return false;
      }
    }
  }

  function bindAddItemButton() {
    jQuery('#insert-quantity-product').on('click', function() {
      if (validateNewItem()) {
        var selectedData = jQuery('#productid').select2('data');
        var productToAdd = {
            "ProductID" : selectedData.id,
            "ProductName" : selectedData.text,
            "Barcode" : null,
            "Quantity" : jQuery('#qty').val()
          };
          self.itemsTable.row.add(productToAdd).draw();
          jQuery('#PurchaseModal').modal('toggle');
      }
    });

    jQuery('#insert-barcode-product').lazyLoadingButton({
        "ajax": '<?php echo base_url().'equipment/api/barcodeExists/'; ?>',
        "data": function() {
          var selectedData = jQuery('#productid').select2('data');
          self.productToInsert = {
            "ProductID" : selectedData.id,
            "ProductName" : selectedData.text,
            "Barcode" : jQuery('#barcode').val(),
            "Quantity" : null
          };
          return self.productToInsert;
        },
        "callback" : function(err, value) {
            if (err) { 
                alert(err.jqXHR.responseText); 
            } else {  
              console.log('Adding:');
              console.log(self.productToInsert);
              self.itemsTable.row.add(self.productToInsert).draw();
              jQuery('#PurchaseModal').modal('toggle');
            }
        },  
        "validation" : function() {
          return validateNewItem();
        },
        "loadingText" : 'Adding...'
    });
  }

  self.bind = function() {    
    jQuery('#move-date').datepicker({
      format: self.df.frontendDatepickerFormat
    });
    jQuery('#move-date').val(self.df.toFrontEndNow());
    initializeDatatable();
    initializeSelects();
    bindAddItemButton();
    jQuery("#purchaseOrderButton").on('click',function(){
      jQuery("#barcode").val('');
      jQuery("#qty").val('');
      jQuery('#PurchaseModal').modal('show');
    });
  };
};

var vm = new PurchaseOrderVM();
vm.bind();

</script>