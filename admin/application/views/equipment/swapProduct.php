	<link rel="stylesheet" href="<?php echo base_url()?>assets/js/icheck/skins/minimal/_all.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/js/icheck/skins/square/_all.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/js/icheck/skins/flat/_all.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/js/icheck/skins/futurico/futurico.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/js/icheck/skins/polaris/polaris.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/js/icheck/skins/line/_all.css">
	<script src="<?php echo base_url()?>assets/js/icheck/icheck.min.js"></script>

	<div class="row">
		<!-- contents for every page starts here -->
        <div class="row">
            <div class="col-lg-12">&nbsp;</div>
        </div>                
		<div class="row">
            <div class="col-lg-12">
                <div class="col-lg-12">&nbsp;</div>
                <span style="color:#003399;font-weight:bold;font-size: medium"></span>
                <div class="col-lg-12">&nbsp;</div>
				<!--Scan BarCode From-->
				<div class="col-md-12">
					<div class="panel panel-primary" >
						<div class="panel-heading">
							<div class="panel-title">
								Scan A Barcode
							</div>
						</div>
											
						<div class="panel-body">
							<form class = "form-inline" id="form-barcode" method = "post" action="<?php echo base_url();?>equipment/vc/swapProduct">
								<div class="form-group">
									<label class="control-label col-sm-6"><h4>Enter a Barcode:</h4></label>
										<div class="col-sm-5">
											<input type = "text" class = "form-control"  name = "ProductBarCode" placeholder = "Enter a Barcode...." >
														
										</div>
								</div>
											<button type = "submit" class = "btn btn-primary btn-sm" style="margin-left:20px;">Scan Bar Code</button>
							</form>
						</div>
					</div>
				</div>
							
					<!-- End Of Scan BarCode From-->
				
					<!--Swap From Product Information Table-->
					<form class = "form" method ="post" name ="SwapProductInfoForm" action="<?php echo base_url();?>equipment/vc/SaveSwapInfos">
						<div class="col-md-6">
							<div class="panel panel-primary" >
								<div class="panel-heading">
									<div class="panel-title">
										Swap From product Information
									</div>
								</div>
								<div class="panel-body">
									<div class="form-group">
										<label class="control-label"><h4>Product Code:</h4></label>
										<input type="text" id="swapProuctFromProductCode" class="form-control" value = "<?php  echo $swapProuctFromProductCode;?>"  disabled = "disabled">
										<input type="hidden" id="search_result" name="search_result" value="<?php echo $no_result; ?>">
										<input type="hidden" id="ProductBarCode" name="ProductBarCode" value="<?php echo $ProductBarCode; ?>">
										<input type="hidden" id="" name="productID" value="<?php echo $ProductID; ?>">
									</div>
											 
									
									<div class="form-group">
										<label class="control-label"><h4>Product Name:</h4></label>
										<input type="text" id="swapProuctFromProductName" class="form-control" value = "<?php  echo $swapProuctFromProductName;?>"  disabled = "disabled">
										<input type="hidden" id="" name="SwapFromProductID" value="<?php echo $SwapFromProductID; ?>">
									</div>
									
									<div class="form-group">
										<label class="control-label"><h4>Product Category:</h4></label>
										<input type="text" id="swapProuctFromProductCategory" class="form-control"   value = "<?php  echo $swapProuctFromProductCategory;?>"   disabled = "disabled">
										<input type="hidden" id="" name="SwapToProductID" value="<?php echo $SwapToProductID; ?>">
									</div>
									
									<div class="form-group">
										<label class="control-label"><h4>Has Barcode:</h4></label>
										<input type="text" id="swapProuctFromHasBarcode" class="form-control" name="swapProuctFromHasBarcode" value = "<?php  echo $swapProuctFromHasBarcode;?>"  disabled = "disabled">
										
									</div>	
									
									<div class="form-group">
										<input type="checkbox" id= "" class="icheck-1 form-control"  value=""<?php echo $swapProuctFromActive==1 ? 'checked':''?> disabled = "disabled">
										<label class="control-label"><h4>Active:</h4></label>
									</div>	
										
									<div class="form-group">
										<input type="checkbox" id="" class="icheck-1 form-control" value=""<?php echo $swapProuctFromHire==1 ? 'checked':'' ?> disabled="disabled" >
										<label class="control-label"><h4>Hire:</h4></label>
									</div>		
											
								</div>
							</div>
						</div>
										<!--End Of Swap From Product Information Table-->
						
					<div class="col-md-6">
						<div class="panel panel-primary" >
							<div class="panel-heading">
								<div class="panel-title">
									Swap To product Information
								</div>
							</div>
											
							<div class="panel-body">
								<div class="form-group">
									<label class="control-label"><h4>Product Code:</h4></label>
										<input type="text" id="swapProuctToProductCode" class="form-control" value = "<?php echo $swapProuctToProductCode;?>"  disabled = "disabled">
										<input type="hidden" id="swapProductSearhResult" name="swapProductSearhResult"  value="<?php echo $swapProductSearhResult; ?>" >
									
								</div>
											 
								<div class="form-group">
									<label class="control-label"><h4>Product Name:</h4></label>
									<input type="text" id="swapProuctToProductName"  class="form-control" value = "<?php echo $swapProuctToProductName;?>"disabled = "disabled">
								</div>
										
								<div class="form-group">
									<label class="control-label"><h4>Product Category:</h4></label>
									<input type="text" id="swapProuctToProductCategory" class="form-control"value = "<?php echo $swapProuctToProductCategory; ?>" disabled="disabled">
								</div>
								
								<div class="form-group">
									<label class="control-label"><h4>Has Barcode:</h4></label>
									<input type="text" id="swapProuctToHasBarcode" class="form-control" value="<?php echo $swapProuctToHasBarcode?>" disabled="disabled">
									
								</div>	
										
								<div class="form-group">
									<input type="checkbox" id="swapProuctToActive" class="icheck-1 form-control" value=""<?php echo $swapProuctToActive ==1 ? 'checked':''?> disabled="disabled">
									<label class="control-label"><h4>Active:</h4></label>
									
								</div>	
										
										
								<div class="form-group">
									<input type="checkbox" id="swapProuctToHire" class="icheck-1  form-control" value=""<?php echo $swapProuctToHire ==1 ? 'checked':''?> disabled = "disabled">
									<label class="control-label"><h4>Hire:</h4></label>
									
								</div>		
											
											
													
							</div>
						</div>
					</div>	
							<!--Swap To Product Information Table-->
						<button type = "submit" onclick="alert_msg();" id="SwapProduct" class = "btn btn-primary" style="margin-top: 10px; margin-left:250px;float:left;" disabled="disabled" >Swap Product</button>
						<button type = "button" onclick ="cancel_action();" class = "btn btn-danger" style="margin-top:10px;float:right;margin-right: 300px">cancel</button>
				</form>
			</div>
		</div>
	</div> 

<!--Inline Javascript goes Here-->

<script>
	var decission = document.getElementById("search_result").value;
	if (decission == "yes") {
		alert ('No Result');
	}
	
	var swapProductSearhResult = document.getElementById("swapProductSearhResult").value;
	if(swapProductSearhResult == "no"){
		alert('This product is not swapable');
	}

	var swapProuctFromProductCode = document.getElementById("swapProuctFromProductCode").value;
	var swapProuctFromProductName = document.getElementById("swapProuctFromProductName").value;
	var swapProuctFromProductCategory = document.getElementById("swapProuctFromProductCategory").value;
	var swapProuctFromHasBarcode = document.getElementById("swapProuctToHasBarcode").value;

	var swapProuctToProductCode = document.getElementById("swapProuctToProductCode").value;
	var swapProuctToProductName = document.getElementById("swapProuctToProductName").value;
	var swapProuctToProductCategory = document.getElementById("swapProuctToProductCategory").value;
	var swapProuctToHasBarcode = document.getElementById("swapProuctToHasBarcode").value;

	if ( swapProuctFromProductCode != "" && swapProuctFromProductName !="" && swapProuctFromProductCategory !="" && swapProuctFromHasBarcode !="" && swapProuctToProductCode !="" && swapProuctToProductName !="" && swapProuctToProductCategory !="" && swapProuctToHasBarcode !="" ){

		document.getElementById('SwapProduct').disabled = false;

	} else {

		document.getElementById('SwapProduct').disabled = true;

	}

	function alert_msg(){

	alert ('Product Swapped SuccessFully');

	}

	function cancel_action(){
		window.location ="<?php echo base_url()?>equipment/vc/swapProduct";

	}
jQuery(document).ready(function($)
{

	jQuery('input.icheck-1').iCheck({
		checkboxClass: 'icheckbox_minimal',
		radioClass: 'iradio_minimal'
	});


	jQuery('#form-barcode').submit(function(){

		
	});


});
</script>
										
<!--Inline Javascript ends Here-->
