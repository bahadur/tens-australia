 
<form class="form-groups-bordered validate" id="equipment-form" target="top">
    <div class="row">
        <div class="col-md-12">
            
            <div class="panel panel-primary" >
            
                <div class="panel-heading">
                    <div class="panel-title">
                        Move Equipment
                    </div>
                </div>
                
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-offset-2 col-md-8">
                      
                    
                  <div class="form-group">
                      <label  class="control-label"><h4>From Location:</h4></label>
                      
                          <select class="form-control select2" id="from_location" name="from_location" required>
                                 <option value='0'>Select Location</option>    
                                <?php
                                $query = $this->db->query("
                                    SELECT 
                                    location.LocationID, 
                                    location.LocationDescription
                                    FROM location
                                    ORDER BY location.LocationDescription");

                                        
                                foreach ($query->result() as $row) { ?>
                                    <option value="<?php echo $row->LocationID ?>"><?php echo $row->LocationDescription?></option>




                                <?php } ?>

                            </select>


                      
                  </div>
                    
                  <div class="form-group">
                      <label  class="control-label"><h4>To Location</h4></label>
                      
                          <select class="form-control select2" id="to_location" name="to_location" required>
                                 <option value='0'>Select Location</option>    
                                <?php
                                $query = $this->db->query("
                                    SELECT 
                                    location.LocationID, 
                                    location.LocationDescription
                                    FROM location
                                    ORDER BY location.LocationDescription");

                                        
                                foreach ($query->result() as $row) { ?>
                                    <option value="<?php echo $row->LocationID ?>"><?php echo $row->LocationDescription?></option>




                                <?php } ?>

                            </select>

                      
                  </div>


                 
                    
                  <div class="form-group">
                      <label  class="control-label"><h4>Move Date</h4></label>
                      
                         <input type="text" name="move_date" id="move_date"   class="form-control datepicker" data-format="D, dd MM yyyy" value="<?php echo date('l, j F Y')?>" required> 

                      
                  </div>
                    
                  <div class="form-group">
                      <label  class="control-label"><h4>Comments</h4></label>
                      
                          <input type="text" name="comments" id="comments"   class="form-control"> 
                      
                  </div>
                 

                 <div class="form-group">
                   
                 </div>

 
                  <div class="form-group">
                      
                      <div class="col-sm-offset-3 col-sm-2  pull-right">
                         <button type="button" class="btn btn-default" id="add_product">Add Product</button>  
                      </div>
                  </div>

                  <div class="form-group">
                    <label  class="control-label"><h4>Products</h4></label>
                    
                      <table class="table table-bordered datatable" id="move_equipment">
                        <thead>
                            <tr>
                              <th>Productid</th>
                              <th>Product</th>
                              <th>Barcode</th>
                              <th>Qty</th>
                              <th></th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                      </table>
                    
                  </div>
                    
                  
                    
                  
                    
                  
                    
                  
                  <div class="form-group">
                    
                        <button type="submit" class="btn btn-success  col-md-4" id="save_submit">save</button>
                    
                  </div>
                  
                  </div>
                  </div>  
                  
                  

                </div>
            
            </div>
        
        </div>
    </div>
    
                
</form>

<!-- Modal -->

<div class="modal fade custom-width" id="confirm-modal">
  <div class="modal-dialog" style="width: 60%;">
    <div class="modal-content">
      
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Items moved</h4>
      </div>
      
      <div class="modal-body">
          <p>The items has been moved</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <!--button type="button" class="btn btn-info">Save changes</button -->
      </div>
    </div>
  </div>
</div>

<div class="modal fade custom-width" id="modal-2">
  <div class="modal-dialog" style="width: 60%;">
    <div class="modal-content">
      
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Custom Width Modal</h4>
      </div>
      
      <div class="modal-body">
        <form class="form-horizontal form-groups-bordered validate" id="modal-add-product" target="_top">
        
        <div class="row">
        <div class="form-group">
                      <label  class="col-sm-3 control-label">Product</label>
                      <div class="col-sm-5">
                        <select class="form-control" id="productid" name="productid" required>
                                 <option></option>    
                                <?php
                                $query = $this->db->query("
                                    SELECT 
                                    product.ProductID, 
                                    product.ProductName, 
                                    product.HasBarcodeID
                                    FROM Product
                                    ORDER BY product.ProductName");

                                        
                                foreach ($query->result() as $row) { ?>
                                    <option value="<?php echo $row->ProductID ?>" hasbarcode="<?php echo $row->HasBarcodeID?>"><?php echo $row->ProductName?></option>




                                <?php } ?>
                        </select>
                      </div>
                  </div>

                  <div class="form-group" id="field_barcode" style="display:none">
                      <label  class="col-sm-3 control-label">Barcode</label>
                      <div class="col-sm-5">
                          <input type="text" name="barcode" id="barcode"  data-rule-required="true" data-message-required="Barcode is required" value="" autofocus class="form-control" > 
                      </div>
                  </div>

                  <div class="form-group"  id="field_qty" style="display:none">
                      <label  class="col-sm-3 control-label">Quantity</label>
                      <div class="col-sm-5">
                          <input type="text" name="qty" id="qty"   class="form-control" required /> 
                      </div>
                  </div>

                  <div class="form-actions">
                    <div class="col-sm-offset-3 col-sm-5">
                      <input type="hidden" name="product" id="product"   class="form-control">
                      <input type="hidden" name="barcodecorrect" id="barcodecorrect" value="1">
                      
                      <button type="submit" class="btn btn-blue" id="insert-product" disabled="disabled">Add Prodcuct</button>
                    </div>
                  </div>

                  
        
                </div>
              </form>
            </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <!--button type="button" class="btn btn-info">Save changes</button -->
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

jQuery(document).ready(function($)
{

  var counter = 1;
  var product_table = jQuery("#move_equipment").DataTable({
    sDom: 't',
    columnDefs: [ 
      {
        targets: 0,
        visible: false                      
      } 
    ]
  });
    
    
  jQuery("#productid").change(function(){
    var element = $(this).find('option:selected');
    var hasbarcode = element.attr("hasbarcode");
    jQuery("#qty").val('');
    jQuery("#barcode").val('');
    if(hasbarcode == 1){
      // jQuery("#qty").val('');
      // jQuery("#barcode").val('');
      // jQuery("#qty").attr("disabled","disabled");
      // jQuery("#barcode").removeAttr("disabled","disabled");
      jQuery("#field_qty").hide();
      jQuery("#field_barcode").fadeIn();
      jQuery('#modal-2 button#insert-product').attr("disabled","disabled");
    } else if(hasbarcode == 2 || hasbarcode == 0){

      // jQuery("#barcode").attr("disabled","disabled");
      // jQuery("#qty").removeAttr("disabled","disabled");
      jQuery("#field_barcode").hide();
      jQuery("#field_qty").fadeIn();
      jQuery('#modal-2 button#insert-product').removeAttr("disabled");
    }
    
  });

  jQuery("#add_product").on('click',function(){
    if(jQuery("#from_location").val() == 0){
      alert("You must select From Location.");
    } else {
      jQuery('#modal-2').modal('show');
      jQuery('#modal-2').on('show.bs.modal', function () {
        jQuery("#productid").val(0);
        jQuery("#barcode").val('');
        jQuery("#qty").val('');
        jQuery("#barcode").removeAttr("disabled","disabled");
        jQuery("#qty").removeAttr("disabled","disabled");
        jQuery('#modal-2 button#insert-product').attr("disabled","disabled");
      });
    }
  });


  jQuery("#barcode").keyup(function(event){
    //if(jQuery(this).val().length > 4 ){
    jQuery.ajax({
      url: '<?php echo base_url()?>equipment/api/barcode_location',
      type: 'post',
      dataType: 'json',
      data: {
        barcode: jQuery("#barcode").val(),
        fromlocationid: jQuery("#from_location").val()
      },
      success: function(resp) {
        if(resp.status){
          // alert("The barcode doesn't exist int he specified From Location.");
          // jQuery("#barcodecorrect").val(0);
          // jQuery("#barcode").val('');
          // jQuery("#barcode").focus();
          jQuery('#modal-2 button#insert-product').removeAttr("disabled");
        } 
        else {
            jQuery('#modal-2 button#insert-product').attr("disabled","disabled");
          }
      }
    });
      //}
  });


  jQuery("#modal-add-product").submit(function(event ){
    event.preventDefault();
      
    var values = {};
      values['product'] = jQuery("#productid option:selected").text();
      values['productid'] = jQuery("#productid").val();
      if(jQuery("#barcode").val() == ""){
        values['barcode'] = "";
      } else {
      values['barcode'] = jQuery("#barcode").val();
    }

    if(jQuery("#qty").val() == ""){
      values['qty'] = "";
    }
    else{
      values['qty'] = jQuery("#qty").val();
    }

    if(jQuery("#barcodecorrect").val() == 1){
      var rowNode = product_table
         .row.add( [ values['productid'],values['product'], values['barcode'], values['qty'],"<a href='javascript:void(0);' id='deleteme'>[X]</a>" ] )
         .draw()
         .node();
        //console.log(values);
        jQuery('#modal-2').modal('hide');
    }

  });

  
  
   


  jQuery('#move_equipment tbody').on( 'click', 'a#deleteme', function () {
    
     product_table
         .row( jQuery(this).parents('tr') )
         .remove()
         .draw();
  });

  jQuery("#equipment-form").submit(function(e){
    e.preventDefault(); 
    var values = {};
    jQuery.each(jQuery(this).serializeArray(), function(i, field) {
      values[field.name] = field.value;
    });
    if(values['from_location'] == 0){
      alert('You must select a From Purchase Order Number or a From Location before saving.');
      return false;
    }
    if(values['to_location'] == 0){
      alert('You must select a To Location before saving.');
      return false;
    }

    var product_data = product_table
      .rows()
      .data();
    console.log(product_data);
    if(product_data.length ==0){
      alert("Each row must have a Product selected.");
      return false;
    }
    values['products'] = new Array();

    for(i = 0; i<product_data.length; i++){
      values['products'][i] = {'productid':product_data[i][0],'barcode':product_data[i][2],'qty':product_data[i][3]};
    }
    
    jQuery.ajax({
      url: '<?php echo base_url()?>equipment/api/move_equipment',
      type: 'post',
      dataType: 'json',
      data: values,
      success: function(resp) {
        if(resp.status == 1){

              //jQuery('#confirm-modal').modal('show');
        }
      }
    });
        
    return false;
        //console.log(values.products);
        
  });
 


});
    
</script>
