 

<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-primary">
	    	<div class="panel-heading">
	    		<div class="panel-title">
	    			Order details
	    		</div>
	    	</div>
		    <div class=="panel-body"> 
		    	<br/>
		    	<br/>
		    	<form id="refund-form" class="form-horizontal">
			    	<div class="col-md-12">
			    	<div class="row">
				    	<div class="col-md-5"> 
					    	<div class="form-group">
					    		<label class="col-md-4 control-label" >Order ID</label>
	                <div class="col-md-8 "> 
						    		<input type="text" readonly value="<?php echo $orderData->OrderID ?>" class="form-control"/> 
						    	</div>
					    	</div>
					    	<div class="form-group">
					    		<label class="col-md-4 control-label">Customer</label>
                    <div class="col-md-8 ">
						    		<input type="text" readonly value="<?php echo htmlspecialchars($orderData->CustomerName) ?>" class="form-control"/>
						    	</div>
					    	</div>
					    	<div class="form-group">
					    		<label class="col-md-4 control-label">Shipping Address</label>
		                        <div class="col-sm-8 ">
						    		<input type="text" readonly value="<?php echo $orderData->Address ?>" class="form-control"/>
						    	</div>
					    	</div>
					    	<div class="form-group">
					    		<label class="col-md-4 control-label">Home Phone</label>
		                        <div class="col-md-8 ">
						    		<input type="text" readonly value="<?php echo $orderData->HomePhone ?>" class="form-control"/>
						    	</div>
					    	</div>
					    	<div class="form-group">
					    		<label class="col-md-4 control-label">Mobile Phone</label>
		                        <div class="col-md-8 ">
						    		<input type="text" readonly value="<?php echo $orderData->MobilePhone ?>" class="form-control"/>
						    	</div>
					    	</div>
					    	<div class="form-group">
					    		<label class="col-md-4 control-label">Email</label>
		                        <div class="col-md-8 ">
						    		<input type="text" readonly value="<?php echo explode('#',$orderData->Email)[0] ?>" class="form-control"/>
						    	</div>
					    	</div>
				    	</div> 
				    	<div class="col-md-5">
					    	<div class="form-group">
					    		<label class="col-md-4  control-label">Order Date</label>
		                        <div class="col-md-8 ">
						    		<input type="text" readonly id="order-date" value="" class="form-control"/>
						    	</div>
					    	</div>
					    	<div class="form-group">
					    		<label class="col-md-4  control-label">Invoice Date</label>
		                        <div class="col-md-8 ">
						    		<input type="text" readonly id="invoice-date" value="" class="form-control"/>
						    	</div>
					    	</div>
					    	<div class="form-group">
					    		<label class="col-md-4  control-label">Due Date</label>
		                        <div class="col-md-8 ">
						    		<input type="text" readonly id="due-date" value="" class="form-control"/>
						    	</div>
					    	</div>
					    	<div class="form-group">
					    		<label class="col-md-4  control-label">Return Actual Date</label>
		                        <div class="col-md-8 ">
						    		<input type="text" readonly id="return-actual-date" value="" class="form-control"/>
						    	</div>
					    	</div>
					    	<div class="form-group">
					    		<label class="col-md-4  control-label">Inv to Return Days</label>
		                        <div class="col-md-8 ">
						    		<input type="text" readonly value="<?php echo $DayDiff ?>" class="form-control"/>
						    	</div>
					    	</div>
				    	</div>
				    	</div>
				    </div>
			    </form>
			    <div class="row">
			    <div class="col-md-offset-1 col-md-10"> 
			    <h4>Refund items</h4>
						<table class="table table-bordered tableStyle" id="products-table">
							<thead>
								<tr>
								<th>Order Detail ID</th>
								<th>Product</th>
								<th>Ship Quantity</th>
								<th>Return Quantity</th>
								<th>Return Date</th>
								<th>Unit Price</th>
								<th>Refund Quantity</th>
								<th>Line Quantity</th>
								<th>Refund</th>
							</tr>
							</thead>
						</table>
						</div>
			    </div>
			</div>
		</div>
	</div>
</div> 

<div class="row">
	<div class="col-md-12">
    <div class="panel panel-primary">
    	<div class="panel-heading">
    		<div class="panel-title">
    			Refund Details
    		</div>
    	</div>
	    <div class=="panel-body">
	    	<form class="form-horizontal" onsubmit="return validate()" method="POST" action="<?php echo base_url().'refunds/vc/refundOrder' ?>"><br/><br/>
 					<div class="row">
	 					<div class="col-md-12">
	 						<div class="col-md-5">
	 							<div class="form-group">  
					    		<label class="col-md-4 control-label">Payment method</label>
			            <div class="col-md-8">
			            	<input id="select-refund-method" type="hidden" autocomplete="off"/> 
							    	<p class="error-message" id="payment-method-required" style="display:none">This field is required.</p>
						    	</div>
						    </div>  
			          <div class="form-group">  
						    		<label class="col-md-4 control-label">Refund Total:</label>
			                <div class="col-md-8 ">
						    			<input readonly class="form-control" id="total-refund" name="transactionTotal" value=""/> 
						    		</div>
						    </div> 
				        <div class="form-group">  
				  				<label class="col-md-4 control-label" >Refund Date</label>
				            <div class="col-md-8 input-group">
				                <input type="text" class="form-control datepicker" name="refundDate" id="refund-date" value="" autocomplete="off" data-format="dd/mm/yyyy">
				                <div class="input-group-addon"> <a href="#"><i class="entypo-calendar"></i></a> </div> 
				            </div> 
							    	<p class="error-message col-md-offset-4" id="refund-date-required" style="padding-left:15px;display:none">This field is required.</p>
						    </div>
	 						</div>
	 						<div class="col-md-5">


			            <div class="form-group">  
						    		<label class="col-md-4 control-label">Reference Number</label>
			                        <div class="col-md-8 ">
							    	<input class="form-control" type="text" id="transactionreference" name="transactionReference" autocomplete="off" value=""/> 
							    	<p class="error-message" id="reference-number-required" style="display:none">This field is required.</p>
						    	</div>
						    </div>
			            <div class="form-group">  
						    		<label class="col-md-4 control-label">Comments</label>
			                        <div class="col-md-8 ">
							    		<textarea class="form-control" id="comments" name="comments"></textarea> 
							    	</div>
						    </div>
	 						</div>
 						</div>
 					</div>
	    		<input type="hidden" name="orderId" id="orderId" value="<?php echo $orderData->OrderID ?>"/> 
          <div class="row">
	          <div class="col-md-10">
                <a id='email-invoice-btn' href='<?php echo base_url()."repots/pdfGeneration/emailRefundInvoice"; ?>' target='_blank' 
                	class="btn btn-primary pull-right" disabled>Email Refund Invoice</a>  
                <a id='preview-invoice-btn' href='#' target='_blank' 
                	class="btn btn-primary pull-right" disabled>Preview Refund Invoice</a> 
	          		<a id="btn-refund-confirm" 
	          			class="btn btn-success pull-right">Confirm Refund</a> 
	          </div>
          </div> 
		    </form>
        <p id="result"></p>

	    </div>
    </div>
	</div>
</div>
 

<link rel="stylesheet" href="<?php echo base_url().'assets/css/styles.css';?>"/>
<script src="<?php echo base_url().'assets/js/tensApi/tensApi.js';?>"></script>
<script src="<?php echo base_url().'assets/js/tensApi/tensDatesFormatter.js';?>"></script>
<script src="<?php echo base_url().'assets/js/tensApi/lazyLoadingButton.js';?>"></script>
<script type="text/javascript">  
function OrderRefundVM() {
	
	var self = this;
  var df = new DateTimeFormatter();

  self.validateSubmission = function() {
  	var valid = true;
  	var selectEl = jQuery("#select-refund-method");
  	var actualSelectEl = jQuery("#s2id_select-refund-method");
  	var payMethod = selectEl.val();
  	var refEl = jQuery("#transactionreference");
  	var reference = refEl.val();
  	var dateEl = jQuery("#refund-date");
 		var refundDate = dateEl.val();
 		if (!payMethod || payMethod === "") {
 			actualSelectEl.addClass('select2-error-border');
 			actualSelectEl.find('.select2-chosen').addClass('select2-error-font');
 			jQuery("#payment-method-required").show();
 			valid = false;
 		} else {
 			actualSelectEl.removeClass('select2-error-border');
 			actualSelectEl.find('.select2-chosen').removeClass('select2-error-font');
 			jQuery("#payment-method-required").hide();
 		}
 		if (!reference || reference === "") { 
 			refEl.addClass('select-error');
 			jQuery("#reference-number-required").show();
 			valid = false;
 		} else { 
 			refEl.removeClass('select-error');
 			jQuery("#reference-number-required").hide();
 		}
 		if (!refundDate || refundDate === "") {  
 			dateEl.addClass('select-error');
 			jQuery("#refund-date-required").show();
 			valid = false;
 		} else {
 			dateEl.removeClass('select-error');
 			jQuery("#refund-date-required").hide();
 		}
 		return valid;
  };
	
	self.reloadDatatable = function() { 
		self.totalRefund = 0;
    self.refundLinesTable.ajax.url(self.url).load(); 
	};

	function initializeDatable() 
	{
		self.totalRefund = 0;
		self.url = "<?php echo base_url().'refunds/formapi/getRefundLines/'.$orderData->OrderID;?>"; 
		self.refundLinesTable = jQuery("#products-table").DataTable({ 
		  "ajax" : self.url,
		  "serverside" : true,  
	    "aoColumnDefs": [
	        { "aTargets": [ 0 ], "data": "OrderDetailID", sClass:"hide_column"}, 
	        { "aTargets": [ 1 ], "data": "ProductName"}, 
	        { "aTargets": [ 2 ], "data": "ShippedQty"}, 
	        { "aTargets": [ 3 ], "data": "ReturnQty"},
	        { "aTargets": [ 4 ], "data": "ReturnActualDate", 
	      		"mRender" : function(data, type, full) {
	      			if (type == 'display') {
	      				return df.toFrontEndDateTime(data);
	      			} else {
	      				return data;
	      			}
	      		}},
      		{ "aTargets": [ 5 ], "data": "UnitPrice", "mRender": function ( data, type, full ) { 
			     		if (type == 'display') {
					     	var amount = data * 1;
					     	return "$" + amount.toFixed(2);
			     		} else {
			     			return data;
			     		}
			      } },
	        { "aTargets": [ 6 ], "data": "TempRefundQuantity", sClass: "input-quantity"},
	        { "aTargets": [ 7 ], "data": "TempTransactionAmount", 
			     "mRender": function ( data, type, full ) { 
			     		if (type == 'display') {
					     	var amount = data * 1;
					     	self.totalRefund += amount;
					     	return "$" + amount.toFixed(2);
			     		} else {
			     			return data;
			     		}
			      } }, 
	        { "aTargets": [ 8 ], "data": "RefundDeposit", 
			     "mRender": function ( data, type, full ) {
			     	if (type == 'display') {
				     	if (data && data.length > 0) {
		     				return "<button type='button' odi='" + full['OrderDetailID'] + "' qty='" + full['ReturnQty'] + "' class='btn btn-default refund-btn'>Refund all</button>"; 
				     	} else  {
				     		return "";
				     	}
				     } else {
				     	return data;
				     }
			      }} ],  
	    	"fnDrawCallback": function (e) {  
	    		jQuery('#total-refund').val('$' + self.totalRefund.toFixed(2))
	    		bindCells();
	    	}
	    }); 
	}
	

	 function recalculateTotalFromScratch()
	 {
		var total = 0; 
		var rows = self.refundLinesTable.fnGetData();
	 	for (var k = 0 ; k < rows.length; k++)
	 	{
	 		var row = rows[k];
	 		if (row.hasOwnProperty('TempTransactionAmount'))
	 		{
		 		total += row['TempTransactionAmount'] * 1; 
	 		}
	 	}
	 	jQuery("#totalRefund").val("$" + total.toFixed(2)); 
	 };


	function bindCells() 
	{   
		var table = jQuery('#products-table');
		table.find('button.refund-btn').on('click', function(e) {  
      jQuery(this).html('Refunding...'); 
      var data = {
        "OrderDetailID" : jQuery(this).attr('odi'),
        "RefundQty" : jQuery(this).attr('qty')
      };
      jQuery.ajax({
          url: '<?php echo base_url()."refunds/api/updateRefundLine"; ?>',
          data: JSON.stringify(data),
          type: 'POST',   
          dataType: 'json',
          contentType : 'application/json',
          error : function(jqXHR, textStatus, errorThrown) {
              jQuery(self).html('Refund all');
              alert(jqXHR.responseText);
          },
          success : function(data, textStatus, jqXHR) { 
              self.reloadDatatable();
          }
      });
    });

 
    table.find('td.input-quantity').editable('<?php echo base_url()."refunds/formapi/updateRefundLine"; ?>', {  
		    "placeholder": "",
        "indicator" : "<img src='<?php echo base_url().'assets/img/indicator.gif';?>' height='16'/>",
	      "cancel": '<button class="btn btn-danger" type="cancel" >Cancel</button>',
	      "submit": '<button class="btn btn-success" type="submit" >Ok</button>',
      	"onsubmit": function(settings, y) {
            var input = jQuery(this).find('input');
            jQuery(this).validate({
              rules: {
                'value': {
                  number: true
                }
              },
              messages: {
                'actionItemEntity.name': {
                  number: 'Only numbers are allowed'
                }
              }
            });

            return jQuery(this).valid();
          },
        "submitdata": function (value, settings) 
        { 
            var orderDetailId = jQuery(this).parent().children(':first').html();
            return {
                "OrderDetailID": orderDetailId
            };
        },
        "callback": function( sValue, y ) { 
        	self.reloadDatatable();
        }  
      }); 
	 }

	 
	 function recalculateTotal(rows)
	 {
		var total = 0; 
	 	for (row in rows)
	 	{
	 		if (rows.hasOwnProperty(row))
	 		{
		 		total += rows[row]._aFilterData[6] * 1; 
	 		}
	 	}
	 	jQuery("#totalRefund").val("$" + total.toFixed(2)); 
	 } 


	function bindButtons() {
		jQuery("#btn-refund-confirm").lazyLoadingButton({
			"ajax": '<?php echo base_url().'refunds/api/confirmRefund'; ?>',
			"validation" : function() {
				return self.validateSubmission();
			},
			"data": function() {
	        var selData = jQuery('#select-refund-method').select2('data');  
	        var refundTotalEl = jQuery('#total-refund').val(); 
	        var payMethodId = null;
	        var terminalId = null;
	        console.log(selData);
	        if (selData) {
	        	payMethodId = selData.PaymentMethodID;
	        	terminalId = selData.TerminalID;
	        }
			    var params = { 
			    	"OrderID" : '<?php echo $orderData->OrderID ?>',
				    "TransactionTotal" : refundTotalEl.replace('$',''), 
				    "TransactionDate" : jQuery('#refund-date').val(), 
				    "TransactionReference" : jQuery('#transactionreference').val(),
				    "Comments" :jQuery('#comments').val(),
				    "PaymentMethodID": payMethodId,
				    "TerminalID" : terminalId
			  	}; 	 
	        return params;
		    },
	    "callback" : function(err, value) {
	        if (err) {
	            alert(err.jqXHR.responseText);
	            console.log(err);
	        } else { 
	            self.reloadDatatable();
	            jQuery('#btn-refund-confirm').html('Refunded');
	            jQuery('#btn-refund-confirm').attr('disabled', true);
	            jQuery('#preview-invoice-btn').removeAttr('disabled');
	    				jQuery('#email-invoice-btn').removeAttr('disabled');
	        }
	    }, 
	    "loadingText" : 'Refunding...'
		});
	}

	function initDates() {
		jQuery('#order-date').val(df.toFrontEndDate('<?php echo $orderData->OrderDate;?>'));
		jQuery('#invoice-date').val(df.toFrontEndDate('<?php echo $orderData->InvoiceDate;?>'));
		jQuery('#due-date').val(df.toFrontEndDate('<?php echo $orderData->DueDate;?>'));
		jQuery('#return-actual-date').val(df.toFrontEndDate('<?php echo $orderData->ReturnActualDate;?>'));
	}

	self.bind = function() {
		jQuery('#select-refund-method').select2({
        placeholder: 'Select a payment method',
        initSelection: function(element, callback) {      
            callback({id: null, text: 'initSelection test' });             
        },
        ajax: {
            url: "<?php echo base_url().'paymethods/api/getPaymentMethodsSelect'?>",
            type: 'POST',
            params: {
                contentType: 'application/json; charset=utf-8'
            },
            dataType: 'json',
            delay: 250,
            results: function(data, page) {
                return {
                    results: data
                };
            },
            data: function(params) {
                return JSON.stringify({
                    q: params.term,  
                    page: params.page
                });
            },
            minimumInputLength: 1
        },
    });
    initDates();
		initializeDatable();
		jQuery('#refund-date').val(df.toFrontEndNow());
		bindButtons();

	} 
	 
}

jQuery(document).ready(function() 
{ 
	var orderRefundVM  = new OrderRefundVM();
	orderRefundVM.bind();

});


 
</script>