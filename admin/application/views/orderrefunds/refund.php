    <div class="row">
        <div class="col-md-12">
            
            <div class="panel panel-primary" >
            
                <div class="panel-heading">
                    <div class="panel-title">
                        Refund
                    </div>
                </div>
                
                <div class="panel-body">
                    
                  <table class="table table-bordered datatable" id="table_refund">
                        <thead>
                            <tr>
                              <th>Order ID</th>
                              <th>Last Name</th>
                              <th>Customer</th>
                              <th>Order Date</th>
                              <th>Required Date</th>
                              <th>Ship Date</th>
                              <th>Due Date</th>
                              <th>Return Actual Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($refunds as $row) { ?>
                            <tr>
                                <td><a href="<?php echo base_url().'refunds/vc/refundView/'.$row->OrderID ?>"><?php echo $row->OrderID?></a></td>
                                <td><?php echo $row->Customer?></td>
                                <td><?php echo $row->Lastname?></td>
                                <td><?php echo $row->OrderDate?></td>
                                <td><?php echo $row->RequiredDate?></td>
                                <td><?php echo $row->FormatMoveDate?></td>
                                <td><?php echo $row->DueDate?></td>
                                <td><?php echo $row->FormatReturnActualDate?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                      </table>
                  
                    
                </div>
            
            </div>
        
        </div>
    </div>
    
                
<script type="text/javascript">

  jQuery(document).ready(function($)
  {

    var table_refund = jQuery("#table_refund").DataTable();

  });

</script>


