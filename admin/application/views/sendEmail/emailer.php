  <!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta charset="utf-8">

<title>bootstrap-wysihtml5</title>

<!-- include libraries(jQuery, bootstrap, fontawesome) --> 
<script src="<?php echo base_url().'assets/jquery-1.11.2.min.js'?>"></script>

<link rel="stylesheet" href="<?php echo base_url().'assets/neon/css/bootstrap.css'?>"> 
<script src="<?php echo base_url().'assets/neon/js/bootstrap.min.js'?>"></script> 
 
<link rel="stylesheet" href="<?php echo base_url().'assets/neon/css/font-icons/font-awesome/css/font-awesome.min.css'?>"> 

<!-- include summernote css/js-->
<link href="<?php echo base_url().'assets/summernote/summernote.css'?>" rel="stylesheet">
<script src="<?php echo base_url().'assets/summernote/summernote.min.js'?>"></script>

</head>

<body id="bodyAll"> 
	<div id="emailBodyDefault" style="display:none">
		<?php 
			echo $emailBody; 
		?>
	</div>
	<br/>
	<form action="<?php echo base_url().'emailer/send';?>" classs="form-horizontal" method="POST">
		<div class="container">  
		<div class="hero-unit" style="margin-top:40px">
			<h3>Send an email</h3> 
				<label for="emailTo" class="control-label">To:</label> 
				<input type="hidden" value="<?php 
					$customerEmailParts = explode('#', $customer->Email);
					if (sizeof($customerEmailParts) > 0)
					{
						echo $customerEmailParts[0];
					}
					else 
					{
						echo $customer->Email;
					} 
					?>" id="recipient"></input>
				<input type="hidden" value="<?php echo $customer->Firstname.' '.$customer->LastName; ?>" id="recipientFullName">
			</input>
				<input type="text" class="form-control" id="emailTo" 
					name="recipient" value="<?php 
					echo explode('#', $customer->Email)[0]; 
					?>"  
					readonly></input> 

				<label for="to">From:</label>
				<input type="text" class="form-control" 
						value="info@tensaustralia.com" placeholder="Type something…" 
						id="sender"
						name="sender"></input> 
				<label for="to">Subject:</label>
				<input type="text" class="form-control" placeholder="Type something…" 
						value="<?php echo $emailSubject; ?>" 
						id="subject"
						name="subject"></input>
				<div class="">
					<?php if (isset($filename)) {?>
						<label for="file">Attachment:</label>
						<p><?php echo $filename; ?> <image height="48" width="48" src="<?php echo base_url().'assets/img/PDF-Icon.jpg'; ?>"></image>  
							<a class="btn btn-primary" href="<?php echo base_url().'tempfiles/'.$filename;?>" target="_blank">View</a></p>
					<?php } ?>
				</div>

				<label for="to">Body:</label>
				<p class="container">
					<textarea class="input-block-level" id="summernote" name="body" rows="18">
					</textarea>
				</p>
				
				<br/><br/>
			    <button type="button" id="sendButton" class="btn">Submit</button>
			    <button type="button" id="cancelButton" class="btn">Cancel</button> 
			</div>
		</div>
	</form>

	<script type="text/javascript">
	var cancelButton = jQuery("#cancelButton");
 	var sendButton = jQuery("#sendButton");

	cancelButton.on('click', function() {
		window.close();
	})
	jQuery('#summernote').summernote({
                height: 400
				});
 	jQuery('#summernote').code(jQuery("#emailBodyDefault").html());

 	sendButton.on('click', function() 
 		{ 
 			sendButton.prop('disabled', true);
 			sendButton.html("Sending...");
	 		var subject = jQuery("#subject").val();
	 		var recipient = jQuery("#recipient").val();
	 		var sender = jQuery("#sender").val();
	 		var filename = '<?php if (isset($filename)) 
	 			{ 
	 				echo $filename;
	 			}; ?>';
	 		if (filename == '') filename = null;
	 		var body = jQuery("#summernote").code();
	 		var recipientFullName =  jQuery("#recipientFullName").val();
	 		var postBody = "subject=" + encodeURIComponent(subject) +
	 			"&recipient=" + encodeURIComponent(recipient) +
	 			"&sender=" + encodeURIComponent(sender) +
	 			"&body=" + encodeURIComponent(body) + 
	 			'&recipientFullName=' + encodeURIComponent(recipientFullName);
	 		if (filename)
	 		{
	 			postBody = postBody + "&filename=" + encodeURIComponent(filename);
	 		}
	 		jQuery.ajax({
	 			type : 'post',
	 			url : "<?php echo base_url().'emailer/send'?>",
	 			data : postBody,
	 			complete: function(e, txtStatus) 
	 			{ 
		 			sendButton.prop('disabled', false);
		 			sendButton.html("Send");
	 				if (e.responseText == "1")
	 				{
	 					jQuery("#bodyAll").html("Successfully sent the email.");
	 				}
	 				else 
	 				{
	 					jQuery("#bodyAll").html(e.responseText);
	 				}
	 			}
 		})
 	});
	</script>
</body>
</html>