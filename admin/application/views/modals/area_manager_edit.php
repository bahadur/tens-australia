<div class="tab-pane box active" style="padding: 5px">
	
	<div class="box-content">
		<form class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url()?>administrator/area_managers" id="form-area-manager-edit">
			

            

            

            
                    <div class="form-group">
                            <label class="col-sm-3 control-label">Business Name:</label>
                            <div class="col-sm-5">
                                <input type="text" name="BusinessName" id="BusinessName"  value="<?php echo $rec->BusinessName?>" class="form-control" />
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-sm-3 control-label">ABN:</label>
                            <div class="col-sm-5">
                                <input type="text" name="ABN" id="ABN"  value="<?php echo $rec->ABN?>" class="form-control" />
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-sm-3 control-label">First Name:</label>
                            <div class="col-sm-5">
                                <input type="text" name="Firstname" id="Firstname"  value="<?php echo $rec->Firstname?>" class="form-control" />
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-sm-3 control-label">Last Name:</label>
                            <div class="col-sm-5">
                                <input type="text" name="Lastname" id="Lastname"  value="<?php echo $rec->Lastname?>" class="form-control" />
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-sm-3 control-label">Position:</label>
                            <div class="col-sm-5">
                                <input type="text" name="Position" id="Position"  value="<?php echo $rec->Position?>" class="form-control" />
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-sm-3 control-label">Address:</label>
                            <div class="col-sm-5">
                                <input type="text" name="Address" id="Address"  value="<?php echo $rec->Address?>" class="form-control" />
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-sm-3 control-label">Suburb:</label>
                            <div class="col-sm-5">
                                <input type="text" name="Suburb" id="Suburb"  value="<?php echo $rec->Suburb?>" class="form-control" />
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-sm-3 control-label">State:</label>
                            <div class="col-sm-5">
                                <input type="text" name="State" id="State"  value="<?php echo $rec->State?>" class="form-control" />
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-sm-3 control-label">Post Code:</label>
                            <div class="col-sm-5">
                                <input type="text" name="Postcode" id="Postcode"  value="<?php echo $rec->Postcode?>" class="form-control" />
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-sm-3 control-label">Country:</label>
                            <div class="col-sm-5">
                                <input type="text" name="Country" id="Country"  value="<?php echo $rec->Country?>" class="form-control" />
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-sm-3 control-label">Phone:</label>
                            <div class="col-sm-5">
                                <input type="text" name="Phone" id="Phone"  value="<?php echo $rec->Phone?>" class="form-control" />
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-sm-3 control-label">Fax:</label>
                            <div class="col-sm-5">
                                <input type="text" name="Fax" id="Fax"  value="<?php echo $rec->Fax?>" class="form-control" />
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-sm-3 control-label">Mobile Phone:</label>
                            <div class="col-sm-5">
                                <input type="text" name="MobilePhone" id="MobilePhone"  value="<?php echo $rec->MobilePhone?>" class="form-control" />
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-sm-3 control-label">Email:</label>
                            <div class="col-sm-5">
                                <input type="text" name="Email" id="Email"  value="<?php echo $rec->Email?>" class="form-control" />
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-sm-3 control-label">Referral Fee:</label>
                            <div class="col-sm-5">
                                <input type="text" name="ReferralFee" id="ReferralFee"  value="<?php echo $rec->ReferralFee?>" class="form-control" />
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-sm-3 control-label">Start Date:</label>
                            <div class="col-sm-5">
                                <input type="text" name="StartDate" id="StartDate"  value="<?php echo $rec->StartDate?>" class="form-control" />
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-sm-3 control-label">Active:</label>
                            <div class="col-sm-5">
                                <input type="text" name="Active" id="Active"  value="<?php echo $rec->Active?>" class="form-control" />
                            </div>
                    </div>

                    <div class="form-group">
                            <label class="col-sm-3 control-label">Postal Codes:</label>
                            <div class="col-sm-5">
                           
                               <!--  <div id="postal_codes" data-init-text="8541"></div>
                                <input type="hidden" name="postal_codes_valus" id="postal_codes_valus" /> -->
                                <input class="form-control" data-val="true"  id="postal_codes" name="postal_codes" type="hidden" value="<?php echo $postcodes?>" >
                           
                            </div>
                    </div>




  
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-primary" id="create_new_customer">Update Area Manager</button>
                        <input type="hidden" name="AreaManagerID" id="AreaManagerID" value="<?php echo $rec->AreaManagerID?>" class="form-control" />
                        <input type="hidden" name="action" value="update">
                    </div>
                </div>
			

            
			
		</form>
	</div>
</div>
<script type="text/javascript">


jQuery(document).ready(function () {

   

    

    jQuery("#form-area-manager-edit").submit(function(){
        // console.log(jQuery("#postal_codes").val());

        jQuery("#form-area-manager-edit #postal_codes_valus").val(jQuery("#postal_codes").val());
        return true;
        
    })

    //var element = [{id:5854,text:5854}];

jQuery('#form-area-manager-edit #postal_codes').select2(
{
   
    placeholder: "Add Postal Codes!",
    minimumInputLength: 2,
    multiple: true,
    ajax: {
        url: "<?php echo base_url()?>ajax/postal_codes",
        dataType: 'json',
        quietMillis: 100,
        data: function (term, page) {
            return {
            term: term, //search term
            page_limit: 10, // page size
            page: page //you need to send page number or your script do not know witch results to skip
            };
        },
        results: function (data, page){
            // var more = (page * 10) < data.total;
            // return { results: data.results, more: more };
            return { results: data.results };
        },
        dropdownCssClass: "bigdrop",
        
        },
        initSelection : function (element, callback) {
            // var preselected_ids = extract_preselected_ids(element); //1,3,4 are the pre-selected IDs as per HTML attributes
            // var preselections = preselect(preselected_ids);
            // callback(preselections);

            callback(jQuery.map(element.val().split(','), function (id) {
                return { id: id, text: id };
            }));
        }

    });

});
</script>
