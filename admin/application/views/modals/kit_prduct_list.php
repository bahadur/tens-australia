<div class="tab-pane box active" style="padding: 5px">
	
	<div class="box-content">
		
		<table class="table table-bordered datatable" id="kit_products_in_modal">
			<thead>
            	<tr>
                	<th>Product</th>
                    <th>Quantity</th>
                </tr>
            </thead>
            <tbody>
            	<?php 
            	foreach ($rec as $product) { ?>
            		<tr>
            			<td><?php echo $product->ProductName ?></td>
            			<td><?php echo $product->Quantity ?></td>
            		</tr>
            	<?php }
            	?>
            	
			</tbody>
			<tfoot>
				<tr>
                	<th>Product</th>
                    <th>Quantity</th>
                </tr>
			</tfoot>
        </table>

	</div>
</div>

<script type="text/javascript">
	jQuery(document).ready(function ($) {

		jQuery("#kit_products_in_modal").DataTable();

	});
</script>