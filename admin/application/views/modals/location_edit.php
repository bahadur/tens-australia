<div class="tab-pane box active" style="padding: 5px">
	
	<div class="box-content">
		<form class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url()?>administrator/locations" id="form-order-fulfilment">
			

            

            

            <div class="form-group">
                <label class="col-sm-3 control-label">LocationTypeID:</label>
                <div class="col-sm-5">
                    <select name="LocationTypeID" id="LocationTypeID" class="form-control">
                        <?php 
                        foreach ($this->db->get("locationtype")->result() as $locationtype) {
                            
                            $selected = ($rec->LocationTypeID == $locationtype->LocationTypeID)?"selected":"";      
                         echo "<option value='$locationtype->LocationTypeID' $selected >$locationtype->LocationType</optiom>";
                        }
                        ?>
                    </select>
                    
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">LocationDescription:</label>
                <div class="col-sm-5">
                    <input type="text" name="LocationDescription" id="LocationDescription" value="<?php echo $rec->LocationDescription?>" class="form-control" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Address:</label>
                <div class="col-sm-5">
                    <input type="text" name="Address" id="Address" value="<?php echo $rec->Address?>" class="form-control" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Suburb:</label>
                <div class="col-sm-5">
                    <input type="text" name="Suburb" id="Suburb" value="<?php echo $rec->Suburb?>" class="form-control" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">State:</label>
                <div class="col-sm-5">
                    <input type="text" name="State" id="State" value="<?php echo $rec->State?>" class="form-control" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Postcode:</label>
                <div class="col-sm-5">
                    <input type="text" name="Postcode" id="Postcode" value="<?php echo $rec->Postcode?>" class="form-control" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Country:</label>
                <div class="col-sm-5">
                    <input type="text" name="Country" id="Country" value="<?php echo $rec->Country?>" class="form-control" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">PickStock:</label>
                <div class="col-sm-5">
                    <input type="text" name="PickStock" id="PickStock" value="<?php echo $rec->PickStock?>" class="form-control" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">HireReturns:</label>
                <div class="col-sm-5">
                    <input type="text" name="HireReturns" id="HireReturns" value="<?php echo $rec->HireReturns?>" class="form-control" />
                </div>
            </div>



  
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-primary" id="create_new_customer">Update Location</button>
                        <input type="hidden" name="LocationID" id="LocationID" value="<?php echo $rec->LocationID?>" class="form-control" />
                        <input type="hidden" name="action" value="update">
                    </div>
                </div>
			

            
			
		</form>
	</div>
</div>
