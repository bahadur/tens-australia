<div class="tab-pane box active" style="padding: 5px">
	
	<div class="box-content">
		<form class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url()?>administrator/education_locations" id="form-order-fulfilment">
			

                <div class="form-group">
                            <label class="col-sm-3 control-label">LocName:</label>
                            <div class="col-sm-5">
                                <input type="text"  name="LocName" id="LocName" class="form-control" value="<?php echo $rec->LocName?>" />
                                
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-sm-3 control-label">LocAddress:</label>
                            <div class="col-sm-5">
                                <input type="text"  name="LocAddress" id="LocAddress" class="form-control" value="<?php echo $rec->LocAddress?>" />
                                
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-sm-3 control-label">LocSuburb:</label>
                            <div class="col-sm-5">
                                <input type="text"  name="LocSuburb" id="LocSuburb" class="form-control" value="<?php echo $rec->LocSuburb?>" />
                                
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-sm-3 control-label">LocState:</label>
                            <div class="col-sm-5">
                                <input type="text"  name="LocState" id="LocState" class="form-control" value="<?php echo $rec->LocState?>" />
                                
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-sm-3 control-label">LocPostcode:</label>
                            <div class="col-sm-5">
                                <input type="text"  name="LocPostcode" id="LocPostcode" class="form-control" value="<?php echo $rec->LocPostcode?>" />
                                
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-sm-3 control-label">LocContact:</label>
                            <div class="col-sm-5">
                                <input type="text"  name="LocContact" id="LocContact" class="form-control" value="<?php echo $rec->LocContact?>" />
                                
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-sm-3 control-label">LocTelephone:</label>
                            <div class="col-sm-5">
                                <input type="text"  name="LocTelephone" id="LocTelephone" class="form-control" value="<?php echo $rec->LocTelephone?>" />
                                
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-sm-3 control-label">Active:</label>
                            <div class="col-sm-5">
                                <input type="text"  name="Active" id="Active" class="form-control" value="<?php echo $rec->Active?>" />
                                
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-sm-3 control-label">Instructions:</label>
                            <div class="col-sm-5">
                                <input type="text"  name="Instructions" id="Instructions" class="form-control" value="<?php echo $rec->Instructions?>" />
                                
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-sm-3 control-label">EducatorPatientsID:</label>
                            <div class="col-sm-5">
                                <input type="text"  name="EducatorPatientsID" id="EducatorPatientsID" class="form-control" value="<?php echo $rec->EducatorPatientsID?>" />
                                
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-sm-3 control-label">Fee:</label>
                            <div class="col-sm-5">
                                <input type="text"  name="Fee" id="Fee" class="form-control" value="<?php echo $rec->Fee?>" />
                                
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-sm-3 control-label">ClassSize:</label>
                            <div class="col-sm-5">
                                <input type="text"  name="ClassSize" id="ClassSize" class="form-control" value="<?php echo $rec->ClassSize?>" />
                                
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-sm-3 control-label">LocMapURL:</label>
                            <div class="col-sm-5">
                                <input type="text"  name="LocMapURL" id="LocMapURL" class="form-control" value="<?php echo $rec->LocMapURL?>" />
                                
                            </div>
                    </div>

                

  
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-primary" id="create_new_customer">Update Customer Type</button>
                        <input type="hidden" name="EduLocationID" id="EduLocationID" value="<?php echo $rec->EduLocationID?>" class="form-control" />
                        <input type="hidden" name="action" value="update">
                    </div>
                </div>
			

            
			
		</form>
	</div>
</div>
