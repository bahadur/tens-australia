<div class="tab-pane box active" style="padding: 5px">
	
	<div class="box-content">
		<form class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url()?>administrator/customer_types" id="form-order-fulfilment">
			

            <div class="form-group">
                    <label class="col-sm-3 control-label">Customer Type:</label>
                    <div class="col-sm-5">
                        <input type="text" name="CustomerType" id="CustomerType" value="<?php echo $rec->CustomerType?>" class="form-control" />
                    </div>
                </div>

                

  
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-primary" id="create_new_customer">Update Customer Type</button>
                        <input type="hidden" name="CustomerTypeID" id="CustomerTypeID" value="<?php echo $rec->CustomerTypeID?>" class="form-control" />
                        <input type="hidden" name="action" value="update">
                    </div>
                </div>
			

            
			
		</form>
	</div>
</div>
