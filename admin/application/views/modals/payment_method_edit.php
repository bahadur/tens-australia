<div class="tab-pane box active" style="padding: 5px">
	
	<div class="box-content">
		<form class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url()?>administrator/payment_methods" id="form-order-fulfilment">
			

            <div class="form-group">
                    <label class="col-sm-3 control-label">Payment Method:</label>
                    <div class="col-sm-5">
                        <input type="text" name="PaymentMethod" id="PaymentMethod" value="<?php echo $rec->PaymentMethod?>" class="form-control" />
                    </div>
                </div>

                

  
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-primary" id="create_new_customer">Update Payment Method</button>
                        <input type="hidden" name="PaymentMethodID" id="PaymentMethodID" value="<?php echo $rec->PaymentMethodID?>" class="form-control" />
                        <input type="hidden" name="action" value="update">
                    </div>
                </div>
			

            
			
		</form>
	</div>
</div>
