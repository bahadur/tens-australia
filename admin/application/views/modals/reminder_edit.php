<div class="tab-pane box active" style="padding: 5px">
	
	<div class="box-content">
		<form class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url()?>administrator/reminders" id="form-order-fulfilment">
			

            
            
            <div class="form-group">
                <label class="col-sm-3 control-label">GraceDays:</label>
                <div class="col-sm-5">
                    <input type="text" name="GraceDays" id="GraceDays" value="<?php echo $rec->GraceDays?>" class="form-control" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">GraceDaysProgrammerComment:</label>
                <div class="col-sm-5">
                    <input type="text" name="GraceDaysProgrammerComment" id="GraceDaysProgrammerComment" value="<?php echo $rec->GraceDaysProgrammerComment?>" class="form-control" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">MessageText:</label>
                <div class="col-sm-5">
                    <textarea name="MessageText" id="MessageText"  class="form-control"><?php echo $rec->MessageText?></textarea>
                    
                </div>
            </div>



  
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-primary" id="create_new_customer">Update Reminder</button>
                        <input type="hidden" name="ReminderID" id="ReminderID" value="<?php echo $rec->ReminderID?>" class="form-control" />
                        <input type="hidden" name="action" value="update">
                    </div>
                </div>
			

            
			
		</form>
	</div>
</div>
