<div class="tabl-pane box active" style="padding: 5px">
	
	<div class="box-content">
		<form class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url()?>usersmanagement/edituser" id="form-order-fulfilment">
			

            <div class="form-group">
                    <label class="col-sm-3 control-label">User Name:</label>
                    <div class="col-sm-5">
                        <input type="text" name="username" id="username" value="<?php echo $rec_user->username?>" class="form-control" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Email:</label>
                    <div class="col-sm-5">
                        <input type="text" name="email" id="email" value="<?php echo $rec_user->email?>" class="form-control" />
                    </div>
                </div>

                

                <div class="form-group">
                    <label class="col-sm-3 control-label">Group:</label>
                    <div class="col-sm-5">
                        <select  name="group" id="group" class="form-control">
                        <?php 

                        foreach ($this->db->get('groups')->result() as $row) {
                            $selected = ($row->name == $rec_user->group)?"selected":"";    
                            echo "<option value='$row->id' $selected >$row->name</option>";
                        }

                        ?>      
                        
                        </select>


                    </div>
                </div>


                <div class="form-group">
                    <label class="col-sm-3 control-label">First Name:</label>
                    <div class="col-sm-5">
                        <input type="text" name="firstname" id="firstname" value="<?php echo $rec_user->firstname?>" class="form-control" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Last Name:</label>
                    <div class="col-sm-5">
                        <input type="text" name="lastname" id="lastname" value="<?php echo $rec_user->lastname?>" class="form-control" />
                    </div>
                </div>
 

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-primary" id="create_new_customer">Update user</button>
                        <input type="hidden" name="id" id="id" value="<?php echo $rec_user->id?>" class="form-control" />
                    </div>
                </div>
			


            


            
            

            
			
		</form>
	</div>
</div>
<script src="<?= base_url().'assets/neon/js/jquery.multi-select.js'?>"></script>
<script type="text/javascript">
jQuery(document).ready(function () {

    

	
});
</script>