<div class="tab-pane box active" style="padding: 5px">
	
	<div class="box-content">
		<form class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url()?>administrator/hospitals" id="form-order-fulfilment">
			


            
            <div class="form-group">
                <label class="col-sm-3 control-label">Hospital Group:</label>
                <div class="col-sm-5">
                    <select name="HospitalGroupID" id="HospitalGroupID" class="form-control">
                    <?php 
                        foreach ($this->db->get("hospitalgroup")->result() as $hospitalgroup) {
                             $selected = ($rec->HospitalGroupID == $hospitalgroup->HospitalGroupID)?"selected":"";            
                            echo "<option value='$hospitalgroup->HospitalGroupID' $selected >$hospitalgroup->HospitalGroup</optiom>";
                        }
                        ?>
                    </select>
                    
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Hospital Name:</label>
                <div class="col-sm-5">
                    <input type="text" name="HospitalName" id="HospitalName" value="<?php echo $rec->HospitalName?>" class="form-control" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Address:</label>
                <div class="col-sm-5">
                    <input type="text" name="Address" id="Address" value="<?php echo $rec->Address?>" class="form-control" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Suburb:</label>
                <div class="col-sm-5">
                    <input type="text" name="Suburb" id="Suburb" value="<?php echo $rec->Suburb?>" class="form-control" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">State:</label>
                <div class="col-sm-5">
                    <input type="text" name="State" id="State" value="<?php echo $rec->State?>" class="form-control" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Country:</label>
                <div class="col-sm-5">
                    <input type="text" name="Country" id="Country" value="<?php echo $rec->Country?>" class="form-control" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Postcode:</label>
                <div class="col-sm-5">
                    <input type="text" name="Postcode" id="Postcode" value="<?php echo $rec->Postcode?>" class="form-control" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Phone:</label>
                <div class="col-sm-5">
                    <input type="text" name="Phone" id="Phone" value="<?php echo $rec->Phone?>" class="form-control" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Fax:</label>
                <div class="col-sm-5">
                    <input type="text" name="Fax" id="Fax" value="<?php echo $rec->Fax?>" class="form-control" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Email:</label>
                <div class="col-sm-5">
                    <input type="text" name="Email" id="Email" value="<?php echo $rec->Email?>" class="form-control" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Website:</label>
                <div class="col-sm-5">
                    <input type="text" name="Website" id="Website" value="<?php echo $rec->Website?>" class="form-control" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Maternity:</label>
                <div class="col-sm-5">
                    <input type="text" name="Maternity" id="Maternity" value="<?php echo $rec->Maternity?>" class="form-control" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Rehab:</label>
                <div class="col-sm-5">
                    <input type="text" name="Rehab" id="Rehab" value="<?php echo $rec->Rehab?>" class="form-control" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">OnHold:</label>
                <div class="col-sm-5">
                    <input type="text" name="OnHold" id="OnHold" value="<?php echo $rec->OnHold?>" class="form-control" />
                </div>
            </div>





  
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-primary" id="create_new_customer">Update Hospital</button>
                        <input type="hidden" name="HospitalID" id="HospitalID" value="<?php echo $rec->HospitalID?>" class="form-control" />
                        <input type="hidden" name="action" value="update">
                    </div>
                </div>
			

            
			
		</form>
	</div>
</div>
