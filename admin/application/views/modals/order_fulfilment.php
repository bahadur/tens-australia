<div class="tabl-pane box active" style="padding: 5px">
	
	<div class="box-content">
		<form class="form-horizontal form-groups-bordered" id="form-order-fulfilment">
			<div class="form-group">
                <label class="control-label col-sm-3">Pick Location: </label>
                <div class="col-sm-5">
                	<select name="pick_location" id="pick_location">
                	<?php 
                	$query = $this->db->query("
		                		SELECT location.LocationID, Location.LocationDescription
								FROM Location
								WHERE (((Location.PickStock)=True))
								ORDER BY Location.LocationDescription");
                	foreach ($query->result() as $rows) {
                		echo "<option value='".$rows->LocationID."'>".$rows->LocationDescription."</option>";
                	}

					?>
					</select>

                </div>
            </div>
			


            <div class="form-group">
                <label class="col-sm-3 control-label">TempBarcode</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="TempBarcode" value="<?php echo $product_detail->TempBarcode?>"/>
            	</div>
            </div>


            <div class="form-group">
                
                <div class="col-sm-offset-3 col-sm-5">
                    <button type="submit"  class="btn btn-blue">Update</button>
            	</div>
            </div>
            

            
			
		</form>
	</div>
</div>

<script type="text/javascript">
jQuery(document).ready(function () {
	jQuery("#form-order-fulfilment").submit(function(event){
			event.preventDefault();
			var values = {};
        	jQuery.each(jQuery(this).serializeArray(), function(i, field) {
          
            	values[field.name] = field.value;
        	});

        	jQuery.ajax({
	          url: '<?php echo base_url()?>ajax/order_fulfilment',
	          type: 'post',
	          dataType: 'json',
	          data: values,
	          success: function(resp) {
	              
	        }
        	});
		});
});
</script>