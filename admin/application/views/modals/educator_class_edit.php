<div class="tab-pane box active" style="padding: 5px">
	
	<div class="box-content">
		<form class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url()?>administrator/educator_classes" id="form-order-fulfilment">
			

            

                
                
            <div class="form-group">
                <label class="col-sm-3 control-label">Location:</label>
                <div class="col-sm-5">
                    <select name="EduLocationID" id="EduLocationID" class="form-control">
                        <?php 
                        foreach ($this->db->get("educatorlocation")->result() as $edu_locations) {
                            
                            $selected = ($rec->EduLocationID == $edu_locations->EduLocationID)?"selected":"";      
                         echo "<option value='$edu_locations->EduLocationID' $selected >$edu_locations->LocName</optiom>";
                        }
                        ?>
                    </select>
                    
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label">Class Date:</label>
                <div class="col-sm-5">
                    <input type="text" name="ClassDate" id="ClassDate" value="<?php echo $rec->ClassDate?>" class="form-control" />
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label">Start Time:</label>
                <div class="col-sm-5">
                    <input type="text" name="StartTime" id="StartTime" value="<?php echo $rec->StartTime?>" class="form-control" />
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label">Duration:</label>
                <div class="col-sm-5">
                    <input type="text" name="Duration" id="Duration" value="<?php echo $rec->Duration?>" class="form-control" />
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label">Educator:</label>
                <div class="col-sm-5">
                    <select name="EducatorID" id="EducatorID" class="form-control">
                                    <?php 
                                    foreach ($this->db->get("educator")->result() as $edu_locations) {
                                         $selected = ($rec->EducatorID == $edu_locations->EducatorID)?"selected":""; 
                                        echo "<option value='$edu_locations->EducatorID' $selected  >$edu_locations->Firstname $edu_locations->Lastname</optiom>";
                                    }
                                    ?>
                                </select>
                    
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label">Hospital:</label>
                <div class="col-sm-5">
                    <select name="HospitalID" id="HospitalID" class="form-control">
                    <?php 
                        foreach ($this->db->get("hospital")->result() as $edu_hospitals) {
                            $selected = ($rec->HospitalID == $edu_locations->HospitalID)?"selected":"";           
                            echo "<option value='$edu_hospitals->HospitalID' $selected  >$edu_hospitals->HospitalName</optiom>";
                        }
                    ?>
                    </select>
                    
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label">Course:</label>
                <div class="col-sm-5">
                    <select name="CourseID" id="CourseID" class="form-control">
                    <?php 
                        foreach ($this->db->get("educatorclasscourse")->result() as $edu_courses) {
                            $selected = ($rec->CourseID == $edu_locations->CourseID)?"selected":"";            
                            echo "<option value='$edu_courses->CourseID' $selected >$edu_courses->CourseName</optiom>";
                        }
                        ?>
                    </select>
                    
                </div>
            </div>

  
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-primary" id="create_new_customer">Update Educator Class</button>
                        <input type="hidden" name="EduClassID" id="EduClassID" value="<?php echo $rec->EduClassID?>" class="form-control" />
                        <input type="hidden" name="action" value="update">
                    </div>
                </div>
			

            
			
		</form>
	</div>
</div>
