<div class="tab-pane box active" style="padding: 5px">
	
	<div class="box-content">
		<form class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url()?>administrator/insurance_providers" id="form-order-fulfilment">
			

            

            
            <div class="form-group">
                <label class="col-sm-3 control-label">Company Name:</label>
                <div class="col-sm-5">
                    <input type="text" name="CompanyName" id="CompanyName" value="<?php echo $rec->CompanyName?>" class="form-control" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Provider No:</label>
                <div class="col-sm-5">
                    <input type="text" name="ProviderNo" id="ProviderNo" value="<?php echo $rec->ProviderNo?>" class="form-control" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Address:</label>
                <div class="col-sm-5">
                    <input type="text" name="Address" id="Address" value="<?php echo $rec->Address?>" class="form-control" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Suburb:</label>
                <div class="col-sm-5">
                    <input type="text" name="Suburb" id="Suburb" value="<?php echo $rec->Suburb?>" class="form-control" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Post Code:</label>
                <div class="col-sm-5">
                    <input type="text" name="Postcode" id="Postcode" value="<?php echo $rec->Postcode?>" class="form-control" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">State:</label>
                <div class="col-sm-5">
                    <input type="text" name="State" id="State" value="<?php echo $rec->State?>" class="form-control" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Country:</label>
                <div class="col-sm-5">
                    <input type="text" name="Country" id="Country" value="<?php echo $rec->Country?>" class="form-control" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Website:</label>
                <div class="col-sm-5">
                    <input type="text" name="Website" id="Website" value="<?php echo $rec->Website?>" class="form-control" />
                </div>
            </div>

    

  
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-primary" id="create_new_customer">Update Insurance Provider</button>
                        <input type="hidden" name="InsuranceProviderID" id="InsuranceProviderID" value="<?php echo $rec->InsuranceProviderID?>" class="form-control" />
                        <input type="hidden" name="action" value="update">
                    </div>
                </div>
			

            
			
		</form>
	</div>
</div>
