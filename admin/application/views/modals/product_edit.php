<div class="tab-pane box active" style="padding: 5px">
	
	<div class="box-content">
		<form class="form-horizontal form-groups-bordered" method="post" action="<?php echo base_url()?>administrator/products" id="form-order-fulfilment">
			


            
            <div class="form-group">
                <label class="col-sm-3 control-label">Product Category:</label>
                <div class="col-sm-5">
                    <select name="ProductCategoryID" id="ProductCategoryID" class="form-control">
                    <?php 
                        foreach ($this->db->get("productcategory")->result() as $productcategory) {
                             $selected = ($rec->ProductCategoryID == $productcategory->ProductCategoryID)?"selected":"";            
                            echo "<option value='$productcategory->ProductCategoryID' >$productcategory->ProductCategory</optiom>";
                        }
                    ?>
                    </select>
                    
                </div>
            </div>

            

                      
             <div class="form-group">
                <label class="col-sm-3 control-label">ProductCode:</label>
                <div class="col-sm-5">
                    <input type="text" name="ProductCode" id="ProductCode" value="<?php echo $rec->ProductCode?>" class="form-control" />
                </div>
            </div>

             <div class="form-group">
                <label class="col-sm-3 control-label">ProductName:</label>
                <div class="col-sm-5">
                    <input type="text" name="ProductName" id="ProductName" value="<?php echo $rec->ProductName?>" class="form-control" />
                </div>
            </div>

             <div class="form-group">
                <label class="col-sm-3 control-label">Active:</label>
                <div class="col-sm-5">
                    <input type="text" name="Active" id="Active" value="<?php echo $rec->Active?>" class="form-control" />
                </div>
            </div>

             <div class="form-group">
                <label class="col-sm-3 control-label">Notes:</label>
                <div class="col-sm-5">
                    <input type="text" name="Notes" id="Notes" value="<?php echo $rec->Notes?>" class="form-control" />
                </div>
            </div>

             <div class="form-group">
                <label class="col-sm-3 control-label">Hire:</label>
                <div class="col-sm-5">
                    <input type="text" name="Hire" id="Hire" value="<?php echo $rec->Hire?>" class="form-control" />
                </div>
            </div>

             <div class="form-group">
                <label class="col-sm-3 control-label">Tax Code:</label>
                <div class="col-sm-5">
                    <select name="TaxCodeID" id="TaxCodeID" class="form-control">
                    <?php 
                        foreach ($this->db->get("taxcode")->result() as $taxcode) {
                             $selected = ($rec->TaxCodeID == $taxcode->TaxCodeID)?"selected":"";            
                            echo "<option value='$taxcode->TaxCodeID' >$taxcode->TaxCode</optiom>";
                        }
                    ?>
                    </select>
                    
                </div>
            </div>

             <div class="form-group">
                <label class="col-sm-3 control-label">LocationID:</label>
                <div class="col-sm-5">
                    <select name="LocationID" id="LocationID" class="form-control">
                    <?php 
                        foreach ($this->db->get("location")->result() as $location) {
                             $selected = ($rec->LocationID == $location->LocationID)?"selected":"";            
                            echo "<option value='$location->LocationID' >$location->LocationDescription</optiom>";
                        }
                    ?>
                    </select>
                    
                </div>
            </div>

             <div class="form-group">
                <label class="col-sm-3 control-label">HasBarcodeID:</label>
                <div class="col-sm-5">
                    <select name="HasBarcodeID" id="HasBarcodeID" class="form-control">
                                    <?php 
                                    foreach ($this->db->get("hasbarcode")->result() as $hasbarcode) {
                                         $selected = ($rec->HasBarcodeID == $hasbarcode->HasBarcodeID)?"selected":""; 
                                        echo "<option value='$hasbarcode->HasBarcodeID' $selected >$hasbarcode->HasBarcode</optiom>";
                                    }
                                    ?>
                    </select>
                    
                </div>
            </div>

             <div class="form-group">
                <label class="col-sm-3 control-label">zzGST:</label>
                <div class="col-sm-5">
                    <input type="text" name="zzGST" id="zzGST" value="<?php echo $rec->zzGST?>" class="form-control" />
                </div>
            </div>

             <div class="form-group">
                <label class="col-sm-3 control-label">AccountCode:</label>
                <div class="col-sm-5">
                    <input type="text" name="AccountCode" id="AccountCode" value="<?php echo $rec->AccountCode?>" class="form-control" />
                </div>
            </div>

             <div class="form-group">
                <label class="col-sm-3 control-label">SwapToProductID:</label>
                <div class="col-sm-5">
                    <input type="text" name="SwapToProductID" id="SwapToProductID" value="<?php echo $rec->SwapToProductID?>" class="form-control" />
                </div>
            </div>

             <div class="form-group">
                <label class="col-sm-3 control-label">ImportantDatesNotification:</label>
                <div class="col-sm-5">
                    <input type="text" name="ImportantDatesNotification" id="ImportantDatesNotification" value="<?php echo $rec->ImportantDatesNotification?>" class="form-control" />
                </div>
            </div>

             <div class="form-group">
                <label class="col-sm-3 control-label">ShowRefundButton:</label>
                <div class="col-sm-5">
                    <input type="text" name="ShowRefundButton" id="ShowRefundButton" value="<?php echo $rec->ShowRefundButton?>" class="form-control" />
                </div>
            </div>

             <div class="form-group">
                <label class="col-sm-3 control-label">PriceList:</label>
                <div class="col-sm-5">
                    <input type="text" name="PriceList" id="PriceList" value="<?php echo $rec->PriceList?>" class="form-control" />
                </div>
            </div>





  
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-primary" id="create_new_customer">Update Product</button>
                        <input type="hidden" name="ProductID" id="ProductID" value="<?php echo $rec->ProductID?>" class="form-control" />
                        <input type="hidden" name="action" value="update">
                    </div>
                </div>
			

            
			
		</form>
	</div>
</div>
