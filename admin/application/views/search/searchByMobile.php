<div class="row">
  
  <div class="col-md-12">
    
    <div class="panel panel-primary">  
      
      <div class="panel-body">
        
        
          
          <div class="table-responsive">
            
            <table class="table display" cellspacing="0" width="100%" id="customers-table">
              <thead>
                <tr>
                  <th>Detail</th>
                  
                  <th>Business Name</th>
                  <th>Identifiers</th>
                  <th>Customer Name</th>
                  <th>Mobile Phone</th>
                  <th>Home Phone</th>
                  <th>Business Phone</th> 
                  <th>Email</th>
                </tr>
              </thead>
              
            </table>

          </div> 

        

      </div>

    </div>

  </div>

</div> 

<script type="text/javascript">
jQuery("#customers-table").dataTable({
  processing: true,
  serverSide: true,
  responsive: true,
  ajax: {
    url: "<?php echo base_url()?>search/formapi/searchByMobile",
    data:{mobileNumber: "<?php echo $mobileNumber?>"},
    type: 'post'
  },
  aoColumns: [  
    {   
      data: "CustomerID"
    },
    {
      data: "BusinessName"
    },
    {
      data: "Identifiers"
    },
    {
      data: "CustomerName"
    },
    {
      data: "MobilePhone"
    },
    {
      data: "HomePhone"
    },
    {
      data: "BusinessPhone"
    },
    {
      data: "Email"
    } 
  ],
  columnDefs: [ 
     {
       targets: 0,
       mRender: function (  data, type, full ){
          return '<a href="<?php echo base_url()."customers/vc/customerDetails/" ?>' + data + '">' + data + '</a>' ;
       }
       
     }
  ]
}); 
</script>


