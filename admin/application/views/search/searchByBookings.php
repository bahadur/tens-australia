
<script src="<?= base_url().'assets/js/tensApi/tensDatesFormatter.js'?>"></script>
<div class="row">
  <div class="col-md-12">
<div class="panel panel-primary">
  <div class="panel-body">  
    <div class="col-md-12">
    <table class="table table-bordered display" id="orders-table" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Required Date</th>
                <th>Invoice Date</th> 
                <th>Order ID</th> 
                <th>Customer</th>
                <th>Hospital Name</th>
                <th>Shipping Method</th>
                <th>Paid</th>
                <th>Shipped Status</th>
                <th>Mobile Phone</th>
                <th>Suburb</th>
                <th>Postcode</th>
                <th>Problem Customer</th>  
                <th>Notes</th>
            </tr>
        </thead> 
    </table>
  <script type="text/javascript">
  var df = new DateTimeFormatter();
  var table = jQuery("#orders-table");
  table.dataTable({
     "processing": true,
     "serverSide": true,
     "serverMethod": "POST",
     "ajax": "<?php echo base_url()?>orders/formapi/searchByBooking",
     "aoColumns": 
    [ 
      {"data" : "RequiredDate",
        "mRender": function ( data, type, full ) { 
          if (type == 'display') {
            return df.toFrontEndDate(data);
          } else {
            return data;
          }
        }},
      {"data" : "InvoiceDate",
        "mRender": function ( data, type, full ) { 
          if (type == 'display') {
            return df.toFrontEndDate(data);
          } else {
            return data;
          }
        }},
      {"data" : "OrderID", 
        "mRender": function(data, type, full) {
          if (type == 'display') {
            return '<a href="<?php echo base_url().'orders/vc/details/';?>' + data + '">' + data + '</a>';
          } else {
            return data;
          }
        }},
      {"data" : "Customer"},
      {"data" : "HospitalName"},
      {"data" : "ShipMethod"},
      {"data" : "Paid"},
      {"data" : "ShippedStatus"},
      {"data" : "MobilePhone"},
      {"data" : "BillingSuburb"},
      {"data" : "BillingPostcode"},
      {"data" : "Flag"},
      {"data" : "Notes"}
    ]
  });


 
  </script>
  </div>
</div>
</div> 
</div>
</div>






