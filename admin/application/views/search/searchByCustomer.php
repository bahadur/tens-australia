<div class="row">
  <div class="col-md-12">
  <div class="panel panel-primary">  
    <div class="panel-body">
      <div class="col-md-12">
        <div class="table-responsive">
        <table class="table table-bordered" id="customers-table">
            <thead>
                <tr>
                    
                    <th>Details</th>
                    <th>Business Name</th>
                    <th>Identifiers</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Mobile Phone</th>
                    <th>Email</th>
                </tr>
            </thead> 
            <tbody></tbody>
        </table>
        </div> 
      </div>
    </div>
    </div>
  </div>
</div> 

 <script type="text/javascript">
      jQuery("#customers-table").dataTable({
        processing: true,
        serverSide: true,
        serverMethod: "POST",
        ajax: "<?php echo base_url()?>search/formapi/searchByCustomer",
        aoColumns:[
          {
            data: "CustomerID"
          },
          {
            data: "BusinessName"
          },
          {
            data: "Identifiers"
          },
          {
            data: "Firstname"
          },
          {
            data: "LastName"
          },
          {
            data: "MobilePhone"
          },
          {
            data: "Email"
          }
        ],
        aoColumnDefs: [ 
          
          {
            aTargets: [0],
            mRender: function ( data, type, full ) { 
              
              return '<a href="<?php echo base_url()."customers/vc/customerDetails/" ?>' + data + '">' + data + '</a>' ;

            }
          }
        ]
      }); 
    </script>


