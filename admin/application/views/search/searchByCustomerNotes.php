 
 <div class="panel panel-primary">
    <div class="panel-body">
        <div class="col-md-12"> 
    <table class="table table-bordered datatable" id="search-results-table">
        <thead>
            <tr>
                <th>DateTime</th>
                <th>Order Id</th>
                <th>Customer</th>
                <th>Staff</th>
                <th>Note</th> 
            </tr>
        </thead>
        <tbody></tbody>
        
    </table>
    <script type="text/javascript">
    var df = new DateTimeFormatter();
    jQuery("#search-results-table").dataTable({ 
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: "<?php echo base_url()?>search/formapi/byCustomerNotes",
            
        },
        aoColumns: [  
            {   
              data: "DateTime"
            },
            {
              data: "OrderID"
            },
            {
              data: "Customer"
            },
            {
              data: "Staff"
            },
            {
              data: "Note"
            }
          ],
          columnDefs: [ 
            {
                targets: 0,
                mRender: function ( data, type, full ) { 
                    
                   if (type == 'display') {
                        return df.toFrontEndDateTime (data);
                    } else {
                        return data;
                    }
                }
            },
            {
                targets: 1,
                mRender: function ( data, type, full ) { 
                    
                    return '<a href="<?php echo base_url()."orders/details/" ?>' + full['OrderID'] + '">' + full['OrderID'] + '</a>' ;
                }
            }
            ]
    });
    </script>
</div>
</div>
 </div> 





