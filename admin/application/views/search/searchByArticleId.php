

<div class="panel panel-primary">
    <div class="panel-heading">
        <div class="panel-title">Search Australia Post Article ID</div>
    </div>
    <div class="panel-body">
        <div class="col-md-12">    
            <label for="articleId">Australia Post Article Id</label>
            <input type="text" class="form-control" id="articleId"></input> 
        </div>
        <br/>
        <div class="col-md-12">
             <button type="button" class="btn btn-primary col-md-3" id="search-btn" area-label="Left Align">
                Search
             </button> 
        </div>
    </div>
</div>  




<script type="text/javascript">

 jQuery('#search-btn').on('click', function() {
    var articleId = jQuery("#articleId").val();
    if (articleId.length > 0) { 
        var url = '<?php echo base_url().'search/vc/articleIdResults/'; ?>' + articleId;
        console.log(url);
        jQuery(location).attr('href',url);
    } else {
        alert("Please enter Australia Post ID"); 
    }
 }); 

</script>

