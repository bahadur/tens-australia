

<div class="panel panel-primary">
     <div class="panel-heading">
        <div class="panel-title">Search Barcode</div>
     </div>
     <div class="panel-body">
        <div class="col-md-12">
            <label for="barcode">Barcode</label>
            <input type="text" class="form-control" id="barcode"></input>
        </div>
        <div class="col-md-12">
             <button type="button" class="btn btn-primary" area-label="Left Align" onclick="filterOrdersByBarcode()">
             	Search
             </button>
         </div>
     </div> 
</div>       
<script type="text/javascript">
function filterOrdersByBarcode()
{
    var barcode = jQuery("#barcode").val();
    if (barcode.length > 0)
    {
        var url = '<?= base_url().'search/vc/barcodeResults/'; ?>' + barcode; 
        jQuery(location).attr('href',url);
    }
    else 
    {
        alert("Please enter Barcode");
    }
}
</script>


