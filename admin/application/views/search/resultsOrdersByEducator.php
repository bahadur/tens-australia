 
<div class="panel panel-primary">
 <div class="panel-body">
 	<div class="col-md-12">
<table class="table table-bordered datatable" id="orders-by-educator-table">
	<thead>
        <th>Order ID</th>
        <th>Invoice Date</th>
        <th>Customer Name</th> 
        <th>Hospital Name</th> 
        <th>Shipped Status</th>
        <th>Required Date</th>
        <th>Business Name</th> 
	</thead>
	<tbody>
		<?php 
			foreach ($educatorsOrders as $row)
			{
				echo "<tr>";
				echo "<td><a href='".base_url()."orders/details/".$row->OrderID."' >".$row->OrderID."</a></td>";
				echo "<td>".$row->InvoiceDate."</td>";
				echo "<td>".$row->CustomerName."</td>";
				echo "<td>".$row->HospitalName."</td>";
				echo "<td>".$row->ShippedStatus."</td>";
				echo "<td>".$row->RequiredDate."</td>";
				echo "<td>".$row->BusinessName."</td>";
				echo "</tr>";
			}
		?>
	</tbody>

	<script type="text/javascript">
		jQuery("#orders-by-educator-table").dataTable();
	</script>
</table>
</div>
</div>
</div>