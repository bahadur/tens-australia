
<script type="text/javascript">
function filterByEducator()
{ 
    location.href = "<?= base_url().'search/vc/educatorsResults/'?>" + jQuery("#educators-select option:selected").text() + "/" + jQuery("#educators-select").val();
}
</script>
<div class="panel panel-primary">              

        <?php 
            $query = $this->db->query("SELECT * FROM educator ORDER BY educator.BusinessName;");
        ?>
        <div class="panel-heading"><div class="panel-title">Choose</div></div>
        <div class="panel-body">
            <form class="form-horizontal" action="<?php echo base_url().'orders/createOrderDetail/' ?>">
                 

                <div class="form-group">
                    <label class="col-md-4 control-label" for="new-product-price">Educator:</label>
                    <div class="col-md-5">  
                       <select data-placeholder="Choose an educator..." class="form-control col-md-6" id="educators-select">
                        <?php
                            foreach ($query->result() as $row)
                            {
                                echo "<option value='".$row->EducatorID."'>".$row->BusinessName."</option>";
                            }
                        ?> 
                        </select>
                     </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4"></div>
                     <button type="button" class="btn btn-primary" area-label="Left Align" onclick="filterByEducator()">
                        Show Order List by Educator
                     </button>
                </div>
             </form>
         </div>

    <!-- contents for every page ends here -->
    <!--***********************************************************************-->
</div>      
            
 



