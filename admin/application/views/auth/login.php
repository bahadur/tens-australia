    <div class="row">
		<aside class="col-lg-1"></aside>
    	<article class="col-lg-11">
            <ol class="breadcrumb">
              <li><a href="#">Home</a></li>
              <li class="active">Login</li>
            </ol>
			<div class="clearfix visible-xs visible-lg"></div>
            <div class="row">
                <div class="col-lg-8">
                	<div class="thumbnail">
                        <div class="caption">
                            <h4>Login</h4>
                            <form role="form" method="post" action="<?php echo(base_url());?>index.php/login/check_user">
							<div class="form-group">
							  <label for="username">Username:</label>
							  <input type="text" class="form-control" name="username" id="username" placeholder="Enter username">
							</div>
							<div class="form-group">
							  <label for="pwd">Password:</label>
							  <input type="password" class="form-control" name="password" id="password" placeholder="Enter password">
							</div>
							<button type="submit" class="btn btn-default">Submit</button>
						  </form>
                        </div>
                    </div>
                </div>
            </div><!-- end nested row 3a -->
        </article>
    </div><!-- end row 3 -->