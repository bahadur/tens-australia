
<!DOCTYPE html>
<html lang="en">
<head>
		<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Neon Admin Panel" />
	<meta name="author" content="" />
	
	<title>login | Tens Australia Back Office System</title>
	

	<link rel="stylesheet" href="<?php echo base_url() ?>assets/neon/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/neon/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/neon/css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/neon/css/neon-core.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/neon/css/neon-theme.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/neon/css/neon-forms.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/neon/css/custom.css">

	<script src="<?php echo base_url() ?>assets/neon/js/jquery-1.11.0.min.js"></script>

	<!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	<link rel="shortcut icon" href="<?php echo base_url() ?>assets/images/favicon.png">
	
<style type = "text/css">

.login-page {
    background-color: #003471;
}
 
.login-page .login-header {
    background-color: #134580;
}

.login-page .login-form .form-group .input-group {
    background-color: #134580;
	border: 1px solid #134580;
}

.input-group-addon
{
	color: #6990bd;
}

.login-page .login-form .form-group .input-group .input-group-addon:after
{
	background: #134580;
}

.login-page .login-header.login-caret:after
{
	border-top-color: #134580;
	border-right-color: transparent;
	border-bottom-color: transparent;
	border-left-color: transparent;
}

.btn-primary 
{
	background-color: #134580; 
}

.login-page .login-form .form-group .btn-login 
{
	border: 1px solid #003471;
} 


.btn-primary-login {
  color: #ffffff;
  background-color: #003471;
  border: 1px solid #134580;
  text-align: left;
  padding: 15px 20px;
  font-size: 14px;
  -moz-transition: all 300ms ease-in-out;
  -o-transition: all 300ms ease-in-out;
  -webkit-transition: all 300ms ease-in-out;
  transition: all 300ms ease-in-out;
}


.btn-primary-login:hover { 
  color:white;
  background-color: #134580;
  border-color: #134580;
}

.entypo-login {
	float:right;
}

</style>
</head>
<body class="page-body login-page login-form-fall">


<!-- This is needed when you send requests via Ajax -->
<script type="text/javascript">


function validate()
{
	var allValid = true;
	if (jQuery("#login").val().length == 0)
	{
		jQuery("#login-group").attr('class', 'input-group validate-has-error');;
		jQuery("#login-error").show();
		allValid = false;
	}
	else 
	{
		jQuery("#login-group").attr('class', 'input-group');
		jQuery("#login-error").hide();
	}
	if (jQuery("#password").val().length == 0)
	{
		jQuery("#password-group").attr('class', 'input-group validate-has-error');
		jQuery("#password-error").show();
		allValid = false;
	}
	else 
	{
		jQuery("#password-group").attr('class', 'input-group');
		jQuery("#password-error").hide();
	} 
	return allValid;
}
</script>

<div class="login-container">
	
	<div class="login-header login-caret">
		
		<div class="login-content" style="width:100%;">
			
			<a href="http://localhost:8080/school-management/" class="logo">
				<img src="<?php echo base_url() ?>assets/neon/images/logo.png" height="120" alt="" />
			</a>
			
			<p class="description">
            	<h2 style="color:#cacaca; font-weight:100;">
					Tens Australia Back Office System
				</h2>
           </p>
			
			<!-- progress bar indicator -->
			<div class="login-progressbar-indicator">
				<h3>43%</h3>
				<span>logging in...</span>
			</div>
		</div>
		
	</div>
	
	<div class="login-progressbar">
		<div></div>
	</div>
	
	<div class="login-form">
		
		<div class="login-content">
			<?php  
				if (isset($errors) && sizeof($errors) > 0) 
				{
			?>
			<div class="alert alert-danger" style="display:block"> 
				<?php  
				foreach ($errors as $key=>$value) {
					echo $value;
					}  ?>
			</div>
			<?php
				}
			?>
			<form method="post" role="form" action="<?php echo base_url()?>auth/login" onsubmit="return validate()">
				
				<div class="form-group">
					
					<div class="input-group" id="login-group">
						<div class="input-group-addon">
							<i class="entypo-user"></i>
						</div>
						
						<input type="text" class="form-control" name="login" id="login" placeholder="Login" autocomplete="off" data-mask="email" />
						<label for="login" id="login-error" class="error" style="display:none">This field is required.</label>
					</div>
					
				</div>
				
				<div class="form-group">
					
					<div class="input-group" id="password-group">
						<div class="input-group-addon">
							<i class="entypo-key"></i>
						</div>
						
						<input type="password" class="form-control" name="password" id="password" placeholder="Password" autocomplete="off" />
						<label for="email" id="password-error" class="error" style="display:none">This field is required.</label>
					</div>
				
				</div>
				
				<div class="form-group">
					<button type="submit" class="btn btn-primary-login btn-block ">
						<i class="entypo-login"></i>
						Login
					</button>
				</div>
				
						
			</form>
			
			
			<!--<div class="login-bottom-links">
				<a href="#" class="link">Forgot your password?</a>
			</div>-->
			
		</div>
		
	</div>
	
</div>


	<!-- Bottom Scripts -->
	<script src="<?php echo base_url() ?>assets/neon/js/gsap/main-gsap.js"></script>
	<script src="<?php echo base_url() ?>assets/neon/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="<?php echo base_url() ?>assets/neon/js/bootstrap.js"></script>
	<script src="<?php echo base_url() ?>assets/neon/js/joinable.js"></script>
	<script src="<?php echo base_url() ?>assets/neon/js/resizeable.js"></script>
	<script src="<?php echo base_url() ?>assets/neon/js/neon-api.js"></script>
	<script src="<?php echo base_url() ?>assets/neon/js/jquery.validate.min.js"></script>
	<script src="<?php echo base_url() ?>assets/neon/js/neon-login.js"></script>
	<script src="<?php echo base_url() ?>assets/neon/js/neon-custom.js"></script>
	<script src="<?php echo base_url() ?>assets/neon/js/neon-demo.js"></script>

</body>
</html>