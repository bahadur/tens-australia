        <div class="col-lg-9"> 
                
                
                
                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-lg-12">&nbsp;</div>
                        <span style="color:#003399;font-weight:bold;font-size: medium">Class List</span>
                        <div class="col-lg-12">&nbsp;</div>
                       <table id="class_table" class="table table-striped table-hover table-condensed" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>EduClassId</th>
                                <!--th>EduLocationId</th -->
                                <th>Class Date</th>
                                <th>Start Time</th>
                                <th>Duration</th>
                                <th>Location</th>
                                <th>Business Name</th>
                                <th>Hospital Name</th>
                                <th>Course Name</th>
                                <th>Class Size</th>
                                <th>Attending</th>
                                <th>Remaining</th>
                                
                            </tr>
                             </thead>
                             <tbody>
                            <?php 
                                $query = $this->db->query("
                                  SELECT 
                                  `educatorclass`.`EduClassID`, 
                                  `educatorclass`.`EduLocationID`, 
                                  `educatorclass`.`ClassDate`, 
                                  `educatorclass`.`StartTime`, 
                                  `educatorclass`.`Duration`, 
                                  `educatorlocation`.`LocName`, 
                                  `educator`.`BusinessName`, 
                                  `hospital`.`HospitalName`, 
                                  `educatorclasscourse`.`CourseName`, 
                                  `educatorlocation`.`ClassSize`, 
                                  @attended := (SELECT count(`EducatorClassCustomerID`) from `educatorclasscustomer` WHERE `EduClassID` = `educatorclass`.`EduClassID`)  AS Attending,
                                  (`educatorlocation`.`ClassSize` - @attended) AS Remaining
                                  FROM `hospital` INNER JOIN (`educatorlocation` RIGHT JOIN (`educator` INNER JOIN (`educatorclasscourse` INNER JOIN `educatorclass` ON `educatorclasscourse`.`CourseID` = `educatorclass`.`CourseID`) ON `educator`.`EducatorID` = `educatorclass`.`EducatorID`) ON `educatorlocation`.`EduLocationID` = `educatorclass`.`EduLocationID`) ON `hospital`.`HospitalID` = `educatorclass`.`HospitalID`
                                  WHERE (((`educatorclass`.`ClassDate`)>=CURDATE()))
                                  ORDER BY `educatorclass`.`ClassDate`, `educatorclass`.`StartTime`");



                                foreach ($query->result() as $row)
                                {
                                      echo "<tr id='.$row->EduClassID.'>";
                                      echo "<td><a href='".base_url()."classes/detail/".$row->EduClassID."' target='_blank' >".$row->EduClassID."</a></td>";
                                      echo "<td>".$row->ClassDate."</td>";
                                      echo "<td>".$row->StartTime."</td>";
                                      echo "<td>".$row->Duration."</td>";
                                      echo "<td>".$row->LocName."</td>";
                                      echo "<td>".$row->BusinessName."</td>";
                                      echo "<td>".$row->HospitalName."</td>";
                                      echo "<td>".$row->CourseName."</td>";
                                      echo "<td>".$row->ClassSize."</td>";
                                      echo "<td>".$row->Attending."</td>";
                                      echo "<td>".$row->Remaining."</td>";
                                      echo "</tr>";
                                }
                            ?>
                       
                    </tbody>
                    </table>
                        
                    </div>
                </div>
            <!-- contents for every page ends here -->
            <!--***********************************************************************-->
        </div>      
            

<!-- add the below lines on every content pages -->            
        </div>
    </div>
        <div class="col-lg-1"></div>
    </div><!-- end of row in navbar view -->
</div><!-- end of container in navbar view -->
<script type="text/javascript">


$(document).ready(function ($) {

   

    var class_table = $('#class_table').dataTable({
        scrollY: "300px",
        scrollX: "100%",
        scrollCollapse: true,
        columnDefs:[
            { visible: false, targets: 4}
        ],

        drawCallback: function ( settings ){
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(4, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="10">'+group+'</td></tr>'
                    );
 
                    last = group;
                }
            } );
        },

        //"paging": false,
        sDom: "lrtip"
    });


   


});

</script>