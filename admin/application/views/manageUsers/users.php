
<div class="row">
<?php 
if ($errors) {
  print_r($errors);
}?>
<div class="col-md-12">
	<ul class="nav nav-tabs bordered">
		<li class='<?php if (!$addusertab) echo 'active' ?>'>
			<a href="#list-users" data-toggle="tab">
				<span class="visible-xs"><i class="entypo-user"></i></span>
				<span class="hidden-xs">Users</span>
			</a>
		</li>
		<li class='<?php if ($addusertab) echo 'active' ?>'>
			<a href="#add-user" data-toggle="tab">
				<span class="visible-xs"><i class="entypo-plus-squared"></i></span>
				<span class="hidden-xs">Add a user</span>
			</a>
		</li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane <?php if (!$addusertab) echo 'active' ?>" id="list-users">
			<table id="users-table" class="table table-bordered datatable">
						<thead>
							<tr>
								<th>ID</th>
								<th>Name</th>
								<th>Last Name</th>
								<th>User Group</th>
								<th>Username</th>
								<th>Email</th>
								<th>Action</th>
							</tr>
						</thead>
						
					</table> 
		</div>
		<div class="tab-pane <?php if ($addusertab) echo 'active' ?>" id="add-user">
			<form class="form-horizontal form-group-bordered" id="form-new-user" method="post" action="<?php echo base_url()?>auth/register">
				<div class="form-group">
					<label class="col-sm-3 control-label">User Name:</label>
					<div class="col-sm-5">
						<input 
						type="text" 
						name="username" 
						id="username" 
						class="form-control"
						data-validate="required" 
                        data-message-required="This field is required."
                        data-placeholder="Please enter User Name..." />
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label">Email:</label>
					<div class="col-sm-5">
						<input 
						type="text" 
						name="email" 
						id="email" 
						class="form-control"
						data-validate="required" 
                        data-message-required="This field is required."
                        data-placeholder="Please enter Email..." />
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label">Password:</label>
					<div class="col-sm-5">
						<input 
						type="password" 
						name="password" 
						id="password" 
						class="form-control"
						data-validate="required" 
                        data-message-required="This field is required."
                        data-placeholder="Please enter Password..." />
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label">Confirm Password:</label>
					<div class="col-sm-5">
						<input 
						type="password" 
						name="confirm_password" 
						id="confirm_password" 
						class="form-control"
						data-validate="required" 
                        data-message-required="This field is required."
                        data-placeholder="Confirm Password..." />
					</div>
				</div>

				

				<div class="form-group">
					<label class="col-sm-3 control-label">Group:</label>
					<div class="col-sm-5">
						<select  name="group" id="group" class="form-control">
						<?php 
						foreach ($this->db->get('groups')->result() as $row) {
							echo "<option value='$row->name'>$row->name</option>";
						}

						?>		
						
						</select>


					</div>
				</div>


				<div class="form-group">
					<label class="col-sm-3 control-label">First Name:</label>
					<div class="col-sm-5">
						<input 
						type="text" 
						name="firstname" 
						id="firstname" 
						class="form-control"
						data-validate="required" 
                        data-message-required="This field is required."
                        data-placeholder="Please enter First Name..." />
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label">Last Name:</label>
					<div class="col-sm-5">
						<input 
						type="text" 
						name="lastname" 
						id="lastname" 
						class="form-control"
						data-validate="required" 
                        data-message-required="This field is required."
                        data-placeholder="Please enter Last Name..." />
					</div>
				</div>
 

				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-5">
						<button type="submit" class="btn btn-primary" id="create_new_customer">Add user</button>
					</div>
				</div>
				
			</form>
		</div>
	</div>
</div>


<script src="<?= base_url().'assets/neon/js/jquery.multi-select.js'?>"></script>

<script type="text/javascript">
 	
	jQuery("#form-new-user").validate({
        ignore: 'input[type=hidden]',
        rules:{
            username: 	{ required: true }, 
            firstname: 	{ required: true }, 
            lastname: 	{ required: true }, 
            email: 		{ required: true,email: true }, 
            password: 	{ 
            	required: true,
                pwcheck: true,
                minlength: 8 
            },
            confirm_password: {
                required: true,
                equalTo: "#password"
            } 
            
        },
 
        errorPlacement: function (error, element) { 
            var elem = jQuery(element);
            error.addClass('errorLabel');
            error.insertAfter(element);
        },
                        
        success: function (label, element) {  
            var elem = jQuery(element);
            elem.siblings('label.error').remove();
        },

        submitHandler: function(form) {
            return true;
        }
    });


	jQuery("#users-table").DataTable( {
		processing: true,
        serverSide: true,
        responsive: true,
        ajax: "<?php echo base_url()?>ajax/user_accounts",
        aoColumnDefs: [ 
           
            {
                targets: 0,
                
                visible: false
            },
            {
                targets: 1,
                
                mRender: function (data, type, full){
                    return full[1]+' '+full[2]
                    //return '<a href="<?php echo base_url()?>orders/details/'+full[1]+'">'+full[1]+'</a>';
                }
            },
            {
            	targets: 2,
            	visible: false
            },
            {
            	targets: 6,
            	mRender: function(data, type, full){
            	var btns =  "<div class='btn-group'>";
	            	btns +=  	"<button type='button' class='btn btn-default btn-sm dropdown-toggle' data-toggle='dropdown'>";
	            	btns +=  		"Action <span class='caret'></span>";
	            	btns +=  	"</button>";
	            	btns +=  	"<ul class='dropdown-menu dropdown-default pull-right' role='menu'>";
	            	btns +=  		"<li><a href='#'' onclick='showEditModal("+full[0]+")'>";
	            	btns +=  			"<i class='entypo-pencil'></i>Edit</a>";
	            	btns +=  		"</li>";
	            	btns +=  		"<li class='divider'></li>";
	            	btns +=  		"<li>";
	            	btns +=  			"<a href='#' onclick='showConfirmModal()'><i class='entypo-trash'></i>Delete</a>";
	            	btns +=  		"</li>";
	            	btns +=  	"</ul>";
	            	btns +=  "</div>";
	            	return btns;
            	}


            }

        ]


	});


function showEditModal(id){
	jQuery('#myModal').modal('show', {backdrop: 'static'});
	jQuery.ajax({
	url: "<?php echo base_url()?>modals/user_edit/"+id,
	success: function(response){
		console.log("Response");
		console.log(response);
		jQuery('#myModal .modal-body').html(response);
	}
	});	
}
</script>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
     <form class="form-horizontal nobottommargin" method="post">
    
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
    
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        
      </div>
      </form>
    </div>
  </div>
</div>