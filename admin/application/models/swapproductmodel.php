<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class SwapProductModel extends CI_Model{

	function swapProductFrom($ProductBarCode)
	{	
		//sql for the information of Swap From Product
		$swapProuctFromSql = "SELECT 
					product.ProductID, 
					product.ProductCode, 
					product.ProductName, 
					product.Hire, 
					product.Active, 
					product.SwapToProductID, 
					hasbarcode.HasBarcode, 
					productcategory.ProductCategory 
				FROM hasbarcode RIGHT JOIN (productcategory RIGHT JOIN (product INNER JOIN 
					(location INNER JOIN productbarcode ON location.locationID = productbarcode.LocationID) 
					ON product.ProductID = productbarcode.ProductID) 
					ON productcategory.ProductCategoryID = product.ProductCategoryID) 
					ON hasbarcode.HasBarcodeID = product.HasBarcodeID  
				WHERE location.PickStock=True AND productbarcode.Barcode= '$ProductBarCode' ";
		$swapProuctFromQuery= $this->db->query($swapProuctFromSql);
		
		$swapProuctFromResult = $swapProuctFromQuery->result();
		 
		
		
		if ($swapProuctFromResult != null){
		
			 $swapToProductID = $swapProuctFromResult[0]->SwapToProductID;
			 
			 //sql for the information of Swap From Product
			 
			 $swapProuctToSql = "SELECT 
									product.ProductCode, 
									product.ProductName, 
									product.Hire, 
									product.Active, 
									product.SwapToProductID, 
									hasbarcode.HasBarcode, 
									productcategory.ProductCategory 
								FROM productcategory RIGHT JOIN (hasbarcode RIGHT JOIN product 
								ON hasbarcode.HasBarcodeID = product.HasBarcodeID) 
								ON productcategory.ProductCategoryID = product.ProductCategoryID 
								WHERE product.ProductID= '$swapToProductID' ";
				  
				  $swapProductToQuery =	 $this->db->query($swapProuctToSql);
				  
				  $swapProductToResult =  $swapProductToQuery->result();
				  
				  return array (
					
					'swapProuctFromResult' => $swapProuctFromResult,
					'swapProductToResult' => $swapProductToResult
					
				
				  );
			  
			} else {
			 
			   $null_array = array();
			   return $null_array;
			 }
			
	}
	
	function swapProductInsert( $FromProductID,$ToProductID, $ProductID,$ProductBarCode,$date){
	
		//Sql for the insertion in stockmove table
		$insertIntoStockMoveSQL = "INSERT INTO stockmove (StockMoveTypeID, MoveDate, FromProductID, ToProductID )
						VALUES ('5', '$date',  '$FromProductID', '$ToProductID')";
		$this->db->query($insertIntoStockMoveSQL); 
		
		// Fetching the StockMoveID fROM stockMove
		$getStockMoveIdSQL = "SELECT MAX(StockMoveID) FROM stockMove;";
		$getStockMoveIdQuery = $this->db->query($getStockMoveIdSQL);
		$stockMoveIdResult = $getStockMoveIdQuery->result_array();
		
		$stockMoveID = $stockMoveIdResult[0]['MAX(StockMoveID)'];
		 
		//Sql for the insertion in stockmovedetail table
		/*$insertIntoStockMoveDeatilSQL = "INSERT INTO stockmovedetail ( StockMoveID, ProductID, Barcode, Quantity ) 
											VALUES (  '$stockMoveID' ,'$ProductID','$ProductBarCode',1)"; 
		$this->db->query($insertIntoStockMoveDeatilSQL);*/
		
		 $updateProductBarCodeSQL = "UPDATE productbarcode SET productbarcode.ProductID = '$FromProductID' 
										WHERE productbarcode.Barcode='$ProductBarCode' ";
										
		$this->db->query($updateProductBarCodeSQL);
		
		
		redirect('equipment/vc/swapProduct','refresh');
	}
}

?>