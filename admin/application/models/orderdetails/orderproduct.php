<?php

class OrderProduct extends CI_Model
{
	/*
product.ProductID, product.ProductName, product.Hire, qryProductPrice.Price, taxcode.CalcGST AS GST, 
    IF(taxcode.CalcGST,(SELECT GST_Percent FROM config WHERE ConfigID = 1),0) AS GSTPercent, 
    product.HasBarcodeID
	*/

	/*
	* A private variable to represent each column in the database
	*/
	private $_productId;
	private $_productName;
	private $_hire;
	private $_price;
	private $_calcGst;
	private $_gstPercent;
	private $_hasBarcodeId;

	public function getJsonReadyArray()
	{
		$jsonProduct = array(
			'productId' => $this->_productId,
			'productName' => $this->_productName,
			'hire' => $this->_hire,
			'price' => $this->_price,
			'calcGst' => $this->_calcGst,
			'gstPercent' => $this->_gstPercent,
			);
		return $jsonProduct;
	}

	public function getProductId()
	{
		return $this->_productId;
	} 

	public function setProductId($value)
	{
		$this->_productId = $value;
	}

	public function getProductName()
	{
		return $this->_productName;
	}

	public function setProductName($value)
	{
		$this->_productName = $value;
	}

	public function getHire()
	{
		return $this->_hire;
	}

	public function setHire($value)
	{
		$this->_hire = $value;
	}

	public function getPrice()
	{
		return $this->_price;
	}

	public function setPrice($value)
	{
		$this->_price = $value;
	}

	public function getCalcGst()
	{
		return $this->_calcGst;
	}

	public function setCalcGst($value)
	{
		$this->_calcGst = $value;
	}

	public function getGstPercent()
	{
		return $this->_gstPercent;
	}

	public function setGstPercent($value)
	{
		$this->_gstPercent = $value;
	}

	public function getHasBarcodeId()
	{
		return $this->_hasBarcodeId;
	}

	public function setHasBarcodeId($value)
	{
		$this->_hasBarcodeId = $value;
	}
	
}
 
?>