<?php

class KitDetails extends CI_Model
{
	/*
	* A private variable to represent each column in the database
	*/
	private $_kitId;
	private $_kitName;


	function __construct()
	{
		parent::__construct();
	}


	public function getKitId()
	{
		return $this->_kitId;
	}

	public function setKitId($value)
	{
		$this->_kitId = $value;
	} 

	public function getKitName()
	{
		return $this->_kitName;
	}

	public function setKitName($value)
	{
		$this->_kitName = $value;
	}

	public function getJsonReadyArray()
	{ 
		$jsonProduct = array(
			'kitId' => $this->_kitId,
			'kitName' => $this->_kitName, 
			);
		return $jsonProduct;
	}

}
 
?>