<?php

class ProductOrderDetail extends CI_Model
{
	private $_orderId;
	private $_productId;
	private $_productName;
	private $_price;
	private $_quantity; 
	private $_calcGst;
	private $_gstPercent;
	private $_taxAmount;
	private $_hire;
	private $_returnExpectedDate;
	private $_orderDetailId;
	private $_cancelReminderMessages;
	private $_hasBarcodeId;
	private $_accountCode;


	public function getJsonReadyArray()
	{ 
		$jsonProduct = array(
			'orderDetailId' => $this->_orderDetailId,
			'orderId' => $this->_orderId,
			'productId' => $this->_productId, 
			'productName' => $this->_productName, 
			'price' => $this->_price, 
			'quantity' => $this->_quantity, 
			'calcGst' => $this->_calcGst, 
			'gstPercent' => $this->_gstPercent, 
			'taxAmount' => $this->_taxAmount, 
			'hire' => $this->_hire, 
			'returnExpectedDate' => $this->_returnExpectedDate, 
			'nullcell' => "" 
			);
		return $jsonProduct;
	} 

	public function setAccountCode($val) {
		$this->_accountCode = $val;
	}

	public function getAccountCode() {
		return $this->_accountCode;
	}

	public function setHasBarcodeId($val) {
		$this->_hasBarcodeId = $val;
	}

	public function getHasBarcodeId() {
		return $this->_hasBarcodeId;
	}

	public function getCancelReminderMessages()
	{
		return $this->_cancelReminderMessages;
	}

	public function setCancelReminderMessages($value)
	{
		$this->_cancelReminderMessages = $value;
	}


	public function getOrderDetailId()
	{ 
		return $this->_orderDetailId;
	}

	public function setOrderDetailId($value)
	{
		$this->_orderDetailId = $value;
	}


	public function getOrderId()
	{
		return $this->_orderId;
	}

	public function setOrderId($value)
	{
		$this->_orderId = $value;
	}

	public function getProductName()
	{
		return $this->_productName;
	}

	public function setProductName($value)
	{
		$this->_productName = $value;
	}

	public function getProductId()
	{
		return $this->_productId;
	}

	public function setProductId($value)
	{
		$this->_productId = $value;
	}

	public function getPrice()
	{
		return $this->_price;
	}

	public function setPrice($value)
	{
		$this->_price = $value;
	}

	public function getQuantity()
	{
		return $this->_quantity;
	}

	public function setQuantity($value)
	{
		$this->_quantity = $value;
	}

	public function getCalcGst()
	{
		return $this->_calcGst;
	}

	public function setCalcGst($value)
	{
		$this->_calcGst = $value;
	}

	public function getGSTPercent()
	{
		return $this->_gstPercent;
	}

	public function setGSTPercent($value)
	{
		$this->_gstPercent = $value;
	}

	public function getTaxAmount()
	{
		return $this->_taxAmount;
	}

	public function setTaxAmount($value)
	{
		$this->_taxAmount = $value;
	}

	public function getHire()
	{
		return $this->_hire;
	}

	public function setHire($value)
	{
		$this->_hire = $value;
	}

	public function getReturnExpectedDate()
	{
		return $this->_returnExpectedDate;
	}

	public function setReturnExpectedDate($value)
	{
		$this->_returnExpectedDate = $value;
	}

	/**
	 * Saves into the database
	 * @return [type] Returns -1 if there is no account code set
	 */
	public function save()
	{ 
		$gstAmount = 0;
		if ($this->_calcGst) {	
			$gstAmount = $this->_gstPercent * $this->_quantity * $this->_price;
		}   
		if ($this->_accountCode) {
			$data = array(
				"OrderID" => $this->_orderId,
				"ProductID" => $this->_productId,
				"Quantity" => $this->_quantity,
				"UnitPrice" => $this->_price,
				"GST" => $this->_calcGst,
				"GST_Percent" => $this->_gstPercent,
				"GST_Amount" => $gstAmount,
				"Hire" => $this->_hire,
				"ReturnExpectedDate" => $this->_returnExpectedDate,
				"CancelReminderMessages" => $this->_cancelReminderMessages,
				"TempPicked" => 0,
				"TempTransactionInclude" => 1,
				"TempTransactionAmount" => $this->_price * $this->_quantity
			);
			if (isset($this->_hasBarcodeId) && $this->_hasBarcodeId == 0) { 
				$data['TempPicked'] = 1;
				$data['TempPickQuantity'] = $this->_quantity; 
			} 
			if ($this->db->insert('orderdetail', $data)) {
				return $this->db->insert_id();
			} else {
				return null;
			}
		} else {
			return -1;
		}
	}
}
 
?>