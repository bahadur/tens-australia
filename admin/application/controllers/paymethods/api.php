<?php
class api extends CI_Controller {


  function __construct() {

    parent::__construct();
    header('Content-Type: application/json'); 
    $this->load->library('Datatables');
    $this->load->library('httpresponses');
    $this->load->helper('ssp');
  }

  /**
   * Get payment methods for select2 control
   * @return [type] [description]
   */
  function getPaymentMethodsSelect() {
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator'))) { 
      $q = $this->db->query('SELECT paymentterminal.PaymentTerminalID, 
        CONCAT(paymentmethod.PaymentMethod, " - ", terminal.terminal) AS PaymentMethod, 
        paymentterminal.PaymentMethodID, paymentterminal.TerminalID 
        FROM (paymentterminal INNER JOIN paymentmethod ON paymentterminal.PaymentMethodID = paymentmethod.PaymentMethodID) 
        INNER JOIN terminal ON paymentterminal.TerminalID = terminal.TerminalID ORDER BY paymentterminal.PaymentTerminalID DESC , 
        paymentterminal.TerminalID;');
      if ($q && $q->num_rows() > 0) {
        $pMethods = $q->result(); 
        $selectTwoMethods = array();
        foreach ($pMethods as $pMethod) {
          array_push($selectTwoMethods, array(
            "id" => $pMethod->PaymentMethodID,
            "text" => $pMethod->PaymentMethod,
            "TerminalID" => $pMethod->TerminalID,
            "PaymentTerminalID" => $pMethod->PaymentTerminalID,
            "PaymentMethodID" => $pMethod->PaymentMethodID
          ));
        }
        $this->httpresponses->respondJsonify($selectTwoMethods, 200);
      } else {
        $this->httpresponses->respondJson('No pay methods', 401);
      }
    } else {
      $this->httpresponses->respondJson('Unauthorized', 401);
    }
  }
 
 }