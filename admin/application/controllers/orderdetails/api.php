<?php
class api extends CI_Controller {


  function __construct() {

    parent::__construct();
    header('Content-Type: application/json'); 
    $this->load->library('Datatables');
    $this->load->library('httpresponses');
    $this->load->helper('ssp');
  }

  public function deleteOrderDetail() {
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator')))
    {
      $requestBody = json_decode(file_get_contents('php://input'));  
      if ($requestBody && $requestBody->OrderDetailID) { 
        $deleteData['OrderDetailID'] = $requestBody->OrderDetailID; 
        if ($this->db->delete('orderdetail', $deleteData)) {
          $this->httpresponses->respondJsonify(array( 'OrderDetailID' => $requestBody->OrderDetailID), 200);
        } else {
          if ($this->db->_error_number() == 1451) {
            $this->httpresponses->respondJsonify(array(
              'message' => 'Could not delete the order detail. Possibly, you have already made a transaction for it.' 
              ), 400);
          } else {
            $this->httpresponses->respondJsonify(array(
              'message' => 'Could not update the order detail',
              'error' => $this->db->_error_message(),
              'code' => $this->db->_error_number()
              ), 400);
          }
        } 
      } else {
        $this->httpresponses->respondJson('Invalid parameters.', 400);
      }
    } else {
        $this->httpresponses->respondJson('Unauthorized.', 401);
    }
  }
  
  public function updateOrderDetail() {
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator')))
    {
      $requestBody = json_decode(file_get_contents('php://input'));  
      if ($requestBody && $requestBody->OrderDetailID && $requestBody->ProductID && $requestBody->Quantity && $requestBody->UnitPrice) {
        $updateData['ProductID'] = $requestBody->ProductID;
        $updateData['Quantity'] = $requestBody->Quantity;
        $updateData['UnitPrice'] = $requestBody->UnitPrice; 
        $updateData['TempTransactionAmount'] = $requestBody->Quantity * $requestBody->UnitPrice;
        if (isset($requestBody->CancelReminderMessages)) {
          $updateData['CancelReminderMessages'] = $requestBody->CancelReminderMessages; 
        }
        $this->db->where('OrderDetailID', $requestBody->OrderDetailID);
        if ($this->db->update('orderdetail', $updateData)) {
          $this->httpresponses->respondJsonify(array( 'OrderDetailID' => $requestBody->OrderDetailID), 200);
        } else {
          $this->httpresponses->respondJson($this->db->_error_message(), 400);
        } 
      } else {
        $this->httpresponses->respondJson('Invalid parameters.', 400);
      }
    } else {
        $this->httpresponses->respondJson('Unauthorized.', 401);
    }
  }
 
  public function createOrderDetail() {
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator')))
    {
      $requestBody = json_decode(file_get_contents('php://input'));  
      if ($requestBody && $requestBody->OrderID && $requestBody->ProductID && $requestBody->Quantity && $requestBody->UnitPrice) {
        $this->load->library('orderdetails/productorderdetailfactory');  
        $res = $this
          ->productorderdetailfactory
          ->createOrderDetail($requestBody->OrderID, $requestBody->ProductID, 
                              $requestBody->Quantity, $requestBody->UnitPrice);
        $err = $res->getError();
        $item = $res->getItem();
        if ($err) {
            $this->httpresponses->respondJson($err, 400);
        } else if ($item) { 
          $orderDetailId = $item->save(); 
          if ($orderDetailId) {
            $this->httpresponses->respondJsonify(array( 'OrderDetailID' => $orderDetailId), 200);
          } else {
            $this->httpresponses->respondJson('Could not add the order detail', 400);
          } 
        } else {
          $this->httpresponses->respondJson('Could not create an order detail.', 400);
        }
      } else {
        $this->httpresponses->respondJson('Invalid parameters.', 400);
      }
    } else {
        $this->httpresponses->respondJson('Unauthorized.', 401);
    }
  }
 

 
 
}