<?php
class formapi extends CI_Controller { 
  
  function __construct() { 
    parent::__construct(); 
    header('Content-Type: application/json'); 
    $this->load->library('Datatables');
    $this->load->library('httpresponses'); 
  }


  public function getOrderDetailsDt($orderId = null) { 
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator'))) { 
      if ($orderId) {
          $this->datatables->select("orderdetail.*, product.ProductID, product.ProductName", false)
              ->from('orderdetail INNER JOIN product ON orderdetail.ProductID = product.ProductID') 
              ->where('orderdetail.OrderID', $orderId);
          return $this->httpresponses->respondJson($this->datatables->generate(), 200);
      } else {
        $this->httpresponses->respondJsonify('Invalid parameters.', 400);
      }
    } else {
        $this->httpresponses->respondJsonify('Not authenticated.', 401);
    }
  }

}


