<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class vc extends CI_Controller{
	
	function __construct (){
		parent::__construct();
		 if (!$this->tank_auth->is_logged_in_as(array('staff', 'administrator')))
        { 
        	redirect(base_url().'auth/login/');
		}
	}

	public function index(){

		redirect('administrator/configuration');
	}

	function test() {
		$this->session->set_flashdata('test_message', 'This is test message');
		redirect('administrator/vc/test2');
	}

	function test2() {
		if ($fd = $this->session->flashdata('test_message')) {
			echo $fd;
		} else {
			$this->session->set_flashdata('test_message', 'Hahahahaha');
			echo '<form action="" method="POST"><input type="hidden" name="t" value="1"></input> <input type="submit" value="Submit"></input></form>';
		}
	}

	public function configuration(){

		
		if($this->input->post()){


			$upd_data = array(
				"DefaultPriceGroupID" 			=> $this->input->post("DefaultPriceGroupID"),
			    "GST_Percent" 					=> $this->input->post("GST_Percent"),
			    "DefaultPickLocationID" 		=> $this->input->post("DefaultPickLocationID"),
			    "DefaultHireReturnLocationID" 	=> $this->input->post("DefaultHireReturnLocationID"),
			    "SearchRightCharsInBarcode" 	=> $this->input->post("SearchRightCharsInBarcode"),
			    "ReturnExpectedDuePlusDays" 	=> $this->input->post("ReturnExpectedDuePlusDays"),
			    "SendSMS_Time" 					=> $this->input->post("SendSMS_Time"),
			    "RequiredWarningDueMinusDays" 	=> $this->input->post("RequiredWarningDueMinusDays"),
			    "InvoiceEmailSubject" 			=> $this->input->post("InvoiceEmailSubject"),
			    "AdminPassword" 				=> $this->input->post("AdminPassword"),
			    "InvoiceEmailBody" 				=> $this->input->post("InvoiceEmailBody")
			     );
			    

		
		$this->db->where("ConfigID",$this->input->post("ConfigID"));

		$this->db->update("config", $upd_data);



		redirect("options/configuration");
		
		}

		
		$data['title']='Configuration';
		
		//$data['config'] = $this->db->get('config')->order_by("PaymentMethod")->row();
		$data['config'] = $this->db->get('config')->row();

		$this->load->view('header',$data); //dynamic header view loaded
        $this->load->view('navbar',$data); //dynamic sidebar view loaded
        $this->load->view('administrator/configuration'); //home page contents loaded, however we can provide contents of any page here to be loaded here
        $this->load->view('footer'); //footer view loaded,
	
	}


	function payment_methods(){


		if($this->input->post()){

			$data = array("PaymentMethod" => $this->input->post("PaymentMethod"));

			if($this->input->post("action") == "update"){
				$this->db->where("PaymentMethodID", $this->input->post("PaymentMethodID"));
				$this->db->update("paymentmethod", $data);
			} elseif ($this->input->post("action") == "insert"){
				$this->db->insert("paymentmethod", $data);
			} elseif($this->input->post("action") == "delete"){
				$this->db->where("PaymentMethodID", $this->input->post("PaymentMethodID"));
				$this->db->delete("paymentmethod");
			}

			redirect("administrator/payment_methods");
		}

		$data['title']='Payment Methods';
		
		$data['paymentmethod'] = $this->db->get('paymentmethod')->result();

		$this->load->view('header',$data); //dynamic header view loaded
        $this->load->view('navbar',$data); //dynamic sidebar view loaded
        $this->load->view('administrator/payment_methods'); //home page contents loaded, however we can provide contents of any page here to be loaded here
        $this->load->view('footer'); //footer view loaded,
	}

	function account_codes(){
		if($this->input->post()){

			$data = array("AccountCode" => $this->input->post("AccountCode"),
							"Description" => $this->input->post("Description"));
			

			if($this->input->post("action") == "update"){
				$this->db->where("AccountCode", $this->input->post("AccountCode"));
				$this->db->update("accountcode", $data);
			} elseif ($this->input->post("action") == "insert"){
				$this->db->insert("accountcode", $data);
			} elseif($this->input->post("action") == "delete"){
				$this->db->where("AccountCode", $this->input->post("AccountCode"));
				$this->db->delete("accountcode");
			}

			redirect("administrator/account_codes");
		}

		$data['title']='Account Codes';
		
		$data['accountcodes'] = $this->db->get('accountcode')->result();

		$this->load->view('header',$data); //dynamic header view loaded
        $this->load->view('navbar',$data); //dynamic sidebar view loaded
        $this->load->view('administrator/account_codes'); //home page contents loaded, however we can provide contents of any page here to be loaded here
        $this->load->view('footer'); //footer view loaded,
	}

	function customer_types(){
		if($this->input->post()){

			$data = array("CustomerType" => $this->input->post("CustomerType")
						);
			

			if($this->input->post("action") == "update"){
				$this->db->where("CustomerTypeID", $this->input->post("CustomerTypeID"));
				$this->db->update("customertype", $data);
			} elseif ($this->input->post("action") == "insert"){
				$this->db->insert("customertype", $data);
			} elseif($this->input->post("action") == "delete"){
				$this->db->where("CustomerTypeID", $this->input->post("CustomerTypeID"));
				$this->db->delete("customertype");
			}

			redirect("administrator/customer_types");
		}

		$data['title']='Cutomer Types';
		
		$data['customertypes'] = $this->db->get('customertype')->result();

		$this->load->view('header',$data); //dynamic header view loaded
        $this->load->view('navbar',$data); //dynamic sidebar view loaded
        $this->load->view('administrator/customer_types'); //home page contents loaded, however we can provide contents of any page here to be loaded here
        $this->load->view('footer'); //footer view loaded,
	}

	function hospital_groups(){
		if($this->input->post()){

			$data = array("HospitalGroup" => $this->input->post("HospitalGroup")
						);
			

			if($this->input->post("action") == "update"){
				$this->db->where("HospitalGroupID", $this->input->post("HospitalGroupID"));
				$this->db->update("hospitalgroup", $data);
			} elseif ($this->input->post("action") == "insert"){
				$this->db->insert("hospitalgroup", $data);
			} elseif($this->input->post("action") == "delete"){
				$this->db->where("HospitalGroupID", $this->input->post("HospitalGroupID"));
				$this->db->delete("hospitalgroup");
			}

			redirect("administrator/hospital_groups");
		}

		$data['title']='Hospital Groups';
		
		$data['hospitalgroups'] = $this->db->get('hospitalgroup')->result();

		$this->load->view('header',$data); //dynamic header view loaded
        $this->load->view('navbar',$data); //dynamic sidebar view loaded
        $this->load->view('administrator/hospital_groups'); //home page contents loaded, however we can provide contents of any page here to be loaded here
        $this->load->view('footer'); //footer view loaded,
	}

	function locations(){
		if($this->input->post()){

			$data = array(
				"LocationTypeID" => $this->input->post("LocationTypeID"),
				"LocationDescription" => $this->input->post("LocationDescription"),
				"Address" => $this->input->post("Address"),
				"Suburb" => $this->input->post("Suburb"),
				"State" => $this->input->post("State"),
				"Postcode" => $this->input->post("HospPostcodeitalGroup"),
				"Country" => $this->input->post("Country"),
				"PickStock" => $this->input->post("PickStock"),
				"HireReturns" => $this->input->post("HireReturns")
				);
			

			if($this->input->post("action") == "update"){
				$this->db->where("LocationID", $this->input->post("LocationID"));
				$this->db->update("location", $data);
			} elseif ($this->input->post("action") == "insert"){
				$this->db->insert("location", $data);
			} elseif($this->input->post("action") == "delete"){
				$this->db->where("LocationID", $this->input->post("LocationID"));
				$this->db->delete("location");
			}

			redirect("administrator/locations");
		}

		$data['title']='Locations';
		
		$this->db->join("locationtype","location.LocationTypeID = locationtype.LocationTypeID");
		$data['locations'] = $this->db->get('location')->result();

		$this->load->view('header',$data); //dynamic header view loaded
        $this->load->view('navbar',$data); //dynamic sidebar view loaded
        $this->load->view('administrator/locations'); //home page contents loaded, however we can provide contents of any page here to be loaded here
        $this->load->view('footer'); //footer view loaded,
	}

	function order_source(){
		if($this->input->post()){

			$data = array(
				"OrderSource" => $this->input->post("OrderSource")
				
				);
			

			if($this->input->post("action") == "update"){
				$this->db->where("OrderSourceID", $this->input->post("OrderSourceID"));
				$this->db->update("ordersource", $data);
			} elseif ($this->input->post("action") == "insert"){
				$this->db->insert("ordersource", $data);
			} elseif($this->input->post("action") == "delete"){
				$this->db->where("OrderSourceID", $this->input->post("OrderSourceID"));
				$this->db->delete("ordersource");
			}

			redirect("administrator/order_source");
		}

		$data['title']='Locations';
		
		
		$data['ordersources'] = $this->db->get('ordersource')->result();

		$this->load->view('header',$data); //dynamic header view loaded
        $this->load->view('navbar',$data); //dynamic sidebar view loaded
        $this->load->view('administrator/ordersource'); //home page contents loaded, however we can provide contents of any page here to be loaded here
        $this->load->view('footer'); //footer view loaded,
	}

	function reminders(){
		if($this->input->post()){

			$data = array(
				"ReminderID" => $this->input->post("ReminderID"),
				"GraceDays" => $this->input->post("GraceDays"),
				"GraceDaysProgrammerComment" => $this->input->post("GraceDaysProgrammerComment"),
				"MessageText" => $this->input->post("MessageText")

				);
			

			if($this->input->post("action") == "update"){
				$this->db->where("ReminderID", $this->input->post("ReminderID"));
				$this->db->update("reminder", $data);
			} elseif ($this->input->post("action") == "insert"){
				$this->db->insert("reminder", $data);
			} elseif($this->input->post("action") == "delete"){
				$this->db->where("ReminderID", $this->input->post("ReminderID"));
				$this->db->delete("reminder");
			}

			redirect("administrator/reminders");
		}

		$data['title']='Reminders';
		
		
		$data['reminders'] = $this->db->get('reminder')->result();

		$this->load->view('header',$data); //dynamic header view loaded
        $this->load->view('navbar',$data); //dynamic sidebar view loaded
        $this->load->view('administrator/reminders'); //home page contents loaded, however we can provide contents of any page here to be loaded here
        $this->load->view('footer'); //footer view loaded,
	}
	
	function location_types(){
		if($this->input->post()){

			$data = array(
				"LocationType" => $this->input->post("LocationType")
				

				);
			

			if($this->input->post("action") == "update"){
				$this->db->where("LocationTypeID", $this->input->post("LocationTypeID"));
				$this->db->update("locationtype", $data);
			} elseif ($this->input->post("action") == "insert"){
				$this->db->insert("locationtype", $data);
			} elseif($this->input->post("action") == "delete"){
				$this->db->where("LocationTypeID", $this->input->post("LocationTypeID"));
				$this->db->delete("locationtype");
			}

			redirect("administrator/location_types");
		}

		$data['title']='Location Types';
		
		
		$data['locationtypes'] = $this->db->get('locationtype')->result();

		$this->load->view('header',$data); //dynamic header view loaded
        $this->load->view('navbar',$data); //dynamic sidebar view loaded
        $this->load->view('administrator/locationtypes'); //home page contents loaded, however we can provide contents of any page here to be loaded here
        $this->load->view('footer'); //footer view loaded,
	}
	
	function price_groups(){
		if($this->input->post()){

			$data = array(
				"PriceGroup" => $this->input->post("PriceGroup")
				

				);
			

			if($this->input->post("action") == "update"){
				$this->db->where("PriceGroupID", $this->input->post("PriceGroupID"));
				$this->db->update("pricegroup", $data);
			} elseif ($this->input->post("action") == "insert"){
				$this->db->insert("pricegroup", $data);
			} elseif($this->input->post("action") == "delete"){
				$this->db->where("PriceGroupID", $this->input->post("PriceGroupID"));
				$this->db->delete("pricegroup");
			}

			redirect("administrator/price_groups");
		}

		$data['title']='Price Groups';
		
		
		$data['pricegroups'] = $this->db->get('pricegroup')->result();

		$this->load->view('header',$data); //dynamic header view loaded
        $this->load->view('navbar',$data); //dynamic sidebar view loaded
        $this->load->view('administrator/price_groups'); //home page contents loaded, however we can provide contents of any page here to be loaded here
        $this->load->view('footer'); //footer view loaded,
	}

	function product_categories(){
		if($this->input->post()){

			$data = array(
				"ProductCategory" => $this->input->post("ProductCategory")
				

				);
			

			if($this->input->post("action") == "update"){
				$this->db->where("ProductCategoryID", $this->input->post("ProductCategoryID"));
				$this->db->update("productcategory", $data);
			} elseif ($this->input->post("action") == "insert"){
				$this->db->insert("productcategory", $data);
			} elseif($this->input->post("action") == "delete"){
				$this->db->where("ProductCategoryID", $this->input->post("ProductCategoryID"));
				$this->db->delete("productcategory");
			}

			redirect("administrator/product_categories");
		}

		$data['title']='Product Categories';
		
		
		$data['productcategories'] = $this->db->get('productcategory')->result();

		$this->load->view('header',$data); //dynamic header view loaded
        $this->load->view('navbar',$data); //dynamic sidebar view loaded
        $this->load->view('administrator/product_categories'); //home page contents loaded, however we can provide contents of any page here to be loaded here
        $this->load->view('footer'); //footer view loaded,
	}

	function post_codes(){
		if($this->input->post()){

			$data = array(
				"Postcode" => $this->input->post("Postcode")
				

				);
			

			if($this->input->post("action") == "update"){
				$this->db->where("PostcodeID", $this->input->post("PostcodeID"));
				$this->db->update("postcode", $data);
			} elseif ($this->input->post("action") == "insert"){
				$this->db->insert("postcode", $data);
			} elseif($this->input->post("action") == "delete"){
				$this->db->where("PostcodeID", $this->input->post("PostcodeID"));
				$this->db->delete("postcode");
			}

			redirect("administrator/post_codes");
		}

		$data['title']='Post Codes';
		
		
		$data['postcodes'] = $this->db->get('postcode')->result();

		$this->load->view('header',$data); //dynamic header view loaded
        $this->load->view('navbar',$data); //dynamic sidebar view loaded
        $this->load->view('administrator/post_codes'); //home page contents loaded, however we can provide contents of any page here to be loaded here
        $this->load->view('footer'); //footer view loaded,
	}
	
	function transaction_types(){
		if($this->input->post()){

			$data = array(
				"TransactionType" => $this->input->post("TransactionType")
				

				);
			

			if($this->input->post("action") == "update"){
				$this->db->where("TransactionTypeID", $this->input->post("TransactionTypeID"));
				$this->db->update("transactiontype", $data);
			} elseif ($this->input->post("action") == "insert"){
				$this->db->insert("transactiontype", $data);
			} elseif($this->input->post("action") == "delete"){
				$this->db->where("TransactionTypeID", $this->input->post("TransactionTypeID"));
				$this->db->delete("transactiontype");
			}

			redirect("administrator/transaction_types");
		}

		$data['title']='Transaction Types';
		
		
		$data['transactiontypes'] = $this->db->get('transactiontype')->result();

		$this->load->view('header',$data); //dynamic header view loaded
        $this->load->view('navbar',$data); //dynamic sidebar view loaded
        $this->load->view('administrator/transaction_types'); //home page contents loaded, however we can provide contents of any page here to be loaded here
        $this->load->view('footer'); //footer view loaded,
	}
	
	function ship_methods(){
		if($this->input->post()){

			$data = array(
				"ShipMethod" => $this->input->post("ShipMethod")
				

				);
			

			if($this->input->post("action") == "update"){
				$this->db->where("ShipMethodID", $this->input->post("ShipMethodID"));
				$this->db->update("shipmethod", $data);
			} elseif ($this->input->post("action") == "insert"){
				$this->db->insert("shipmethod", $data);
			} elseif($this->input->post("action") == "delete"){
				$this->db->where("ShipMethodID", $this->input->post("ShipMethodID"));
				$this->db->delete("shipmethod");
			}

			redirect("administrator/ship_methods");
		}

		$data['title']='Ship Methods';
		
		
		$data['shipmethods'] = $this->db->get('shipmethod')->result();

		$this->load->view('header',$data); //dynamic header view loaded
        $this->load->view('navbar',$data); //dynamic sidebar view loaded
        $this->load->view('administrator/ship_methods'); //home page contents loaded, however we can provide contents of any page here to be loaded here
        $this->load->view('footer'); //footer view loaded,
	}

	function stockmove_types(){
		if($this->input->post()){

			$data = array(
				"StockMoveType" => $this->input->post("StockMoveType")
				

				);
			

			if($this->input->post("action") == "update"){
				$this->db->where("StockMoveTypeID", $this->input->post("StockMoveTypeID"));
				$this->db->update("stockmovetype", $data);
			} elseif ($this->input->post("action") == "insert"){
				$this->db->insert("stockmovetype", $data);
			} elseif($this->input->post("action") == "delete"){
				$this->db->where("StockMoveTypeID", $this->input->post("StockMoveTypeID"));
				$this->db->delete("stockmovetype");
			}

			redirect("administrator/stockmove_types");
		}

		$data['title']='Stock Move Types';
		
		
		$data['stockmovetypes'] = $this->db->get('stockmovetype')->result();

		$this->load->view('header',$data); //dynamic header view loaded
        $this->load->view('navbar',$data); //dynamic sidebar view loaded
        $this->load->view('administrator/stockmove_types'); //home page contents loaded, however we can provide contents of any page here to be loaded here
        $this->load->view('footer'); //footer view loaded,
	}
	

	function kits(){
		if($this->input->post()){


			$data = array(
				"KitName" => $this->input->post("KitName"),
				"Notes" => $this->input->post("Notes")
				

				);
			

			if($this->input->post("action") == "update"){
				$this->db->where("KitID", $this->input->post("KitID"));
				$this->db->update("kit", $data);
			} elseif ($this->input->post("action") == "insert"){
				
				$this->db->insert("kit", $data);

				$kit_id = $this->db->insert_id();

				$product_data = array();
				foreach ($this->input->post('products') as $product) {
					
					$product_data[] = array(
										"KitID" => $kit_id,
										"ProductID" => $product["productid"],
										"Quantity" => $product["qty"]
										);

				}

				$this->db->insert_batch("kitproduct", $product_data);

				echo json_encode(array('status'=>"1"));

				return;


			} elseif($this->input->post("action") == "delete"){

				$this->db->where("KitID", $this->input->post("KitID"));
				$this->db->delete("kitproduct");
				$this->db->where("KitID", $this->input->post("KitID"));
				$this->db->delete("kit");
			}

			redirect("administrator/kits");
		}

		$data['title']='Kits';
		
		
		$data['kits'] = $this->db->get('kit')->result();

		$this->load->view('header',$data); //dynamic header view loaded
        $this->load->view('navbar',$data); //dynamic sidebar view loaded
        $this->load->view('administrator/kits'); //home page contents loaded, however we can provide contents of any page here to be loaded here
        $this->load->view('footer'); //footer view loaded,
	}


	function refundByOrderID($orderId=""){


		if($this->input->post()){

			switch ($this->input->post('action')) {
				case 'this_refund':
					header('Content-Type: application/json');
				 	
					$this->db->set("TempTransactionAmount",$this->input->post('this_refund'));
					$this->db->where("OrderDetailID",$this->input->post('orderDetailId'));
					$this->db->update("orderdetail");
					

					//$this->db->flush_cache();

					$query = $this->db->query("select IF(sum(`TotalPaid`) is null,0,sum(`TotalPaid`)) totalPaid 
												from `qryPerOrderDetailTotalPaid`
												where `OrderDetailID` = ".$this->input->post('orderDetailId'));
					$rs1 = $query->result();
					


					$this->db->select('sum(TempTransactionAmount) totalRefund',false);
					$this->db->from('orderdetail');
					$this->db->where('OrderID',$this->input->post('orderId'));

					$rs2 = $this->db->get()->row();

					echo json_encode(array("totalRefund"=>$rs2->totalRefund,"notRefund"=>( $rs1[0]->totalPaid - $this->input->post('this_refund')) ));
					die;
							
					break;

				case 'confirm_refund':



					$this->db->select("PaymentMethodID, TerminalID");
					$this->db->from("paymentterminal");
					$this->db->where("paymentTerminalID" , $this->input->post("paymentTerminalID"));

					$paymentterminal = $this->db->get()->row();

					$data = array(
								"OrderID" => $this->input->post('orderid') ,
								"TransactionTypeID" => 2,
								"PaymentMethodID" => $paymentterminal->PaymentMethodID,
								"TransactionDate" => $this->input->post('refundDate'),
								"TransactionTotal" => 0,
								"TransactionReference" => $this->input->post('referenceNo'),
								"TransactionCreateDateTime" => date("Y-m-d H:i:s"),
								"TerminalID" => $paymentterminal->TerminalID,
								"Comments" => $this->input->post('comments')

									);

					$this->db->insert("transaction", $data);

					$transactionid = $this->db->insert_id();

					$this->db->query("
					 				INSERT INTO transactiondetail ( TransactionID, OrderDetailID, Amount, AccountCode, GST_Amount ) 
						            SELECT " .$transactionid. " AS TransactionID, orderdetail.OrderDetailID, orderdetail.TempTransactionAmount, productaccountcode.AccountCode, ( - orderdetail.GST * orderdetail.TempTransactionAmount * (1 - 1 / (1 + orderdetail.GST_Percent)) 
						            FROM orderdetail INNER JOIN productaccountcode ON orderdetail.ProductID = productaccountcode.ProductID 
						            WHERE (((productaccountcode.TransactionTypeID)=2) AND (orderdetail.OrderID = " .$this->input->post('orderid'). ") AND ((orderdetail.TempTransactionAmount)>0))"
						            );

					

					$this->db->query("
					 				UPDATE transaction SET  TransactionTotal = " . $this->input->post('thisRefundTotal') . "
             						WHERE transaction.TransactionID= " .$transactionid); 
					
					

					$this->db->query("
								UPDATE orderdetail INNER JOIN transactiondetail ON orderdetail.OrderDetailID = transactiondetail.OrderDetailID 
								SET orderdetail.TempTransactionAmount = Null, orderdetail.TempRefundQuantity = Null, orderdetail.RefundProcessedDate ='" .date("Y-m-d H:i:s")."' 
		             			WHERE (((transactiondetail.TransactionID)= " .$transactionid. "))");
    				
    				
					

    				redirect("administrator/refundByOrderID/$orderId");
				
					break;

				
			}
			

			
		}

		$data['title']='Refund By Order ID';
		
		


		
        
        if (empty($orderId)) {
        	
	        	$this->load->view('header',$data); //dynamic header view loaded
	        	$this->load->view('navbar',$data); //dynamic sidebar view loaded
        		$this->load->view('administrator/refundByOrderID_step1',$data); 
        		$this->load->view('footer'); //footer view loaded,
        		
        } else {

        		
        	
        

        		$rs = $this->db->query("
        			SELECT 
        				`orderdetail`.`OrderDetailID`,
						`order`.`OrderID`, 
						`order`.`OrderDate`, 
						CONCAT(`Firstname`,' ',`LastName`,' (',`BusinessName`,')') AS CustomerName, 
						CONCAT(`order`.`ShippingAddress` , ', ' , `order`.`ShippingSuburb`, ', ' , `order`.`ShippingState` , ' ' , `order`.`ShippingPostcode` , ', ' , `order`.`ShippingCountry`) AS Address, 
						`order`.`HomePhone`, 
						`order`.`MobilePhone`, 
						`order`.`Email`, 
						`order`.`DueDate`, 
						`orderdetail`.`ReturnActualDate`
						FROM (`customer` INNER JOIN `order` ON `customer`.`CustomerID` = `order`.`CustomerID`) INNER JOIN `orderdetail` ON `order`.`OrderID` = `orderdetail`.`OrderID`
						WHERE `order`.`OrderID` = $orderId ");

        		$data['refundOrder'] = $rs->row();

        		$rs = $this->db->query("
        			SELECT 
						`orderDetail`.`OrderID`, 
						`orderDetail`.`orderdetailID`, 
						`product`.`ProductName`, 
						( SELECT IF(sum(`Quantity`) is null,0,sum(`Quantity`)) from `qryHireReturnQty` WHERE `OrderDetailID` = `orderDetail`.`OrderDetailID` ) AS ReturnQty, 
						( SELECT IF(sum(`Quantity`) is null,0,sum(`Quantity`)) FROM `qryFulfilmentListQtyShipped` WHERE `OrderDetailID` = `orderDetail`.`OrderDetailID` ) AS ShippedQty,
						date_format(`orderDetail`.`ReturnActualDate`,'%d-%b-%y') as ReturnActualDate, 
						`orderDetail`.`UnitPrice`, 
						
		        		( SELECT IF(sum(`TotalPaid`) is null,0,sum(`TotalPaid`)) FROM `qryPerOrderDetailTotalPaid` WHERE `OrderDetailID` = `orderDetail`.`OrderDetailID` ) AS TotalPaid,
						( SELECT IF(sum(`TotalRefunded`) is null,0,sum(`TotalRefunded`)) FROM `qryPerOrderDetailTotalRefunded` WHERE `OrderDetailID` = `orderDetail`.`OrderDetailID` ) AS TotalRefunded,
						`orderDetail`.`TempTransactionAmount`

						FROM `product` INNER JOIN `orderDetail` ON `product`.`ProductID` = `orderDetail`.`ProductID`
						WHERE `orderDetail`.`OrderID` = $orderId
						ORDER BY `orderDetail`.`OrderDetailID`
        			");

        		$data['refundProducts'] = $rs->result();
        		$this->load->view('header',$data); //dynamic header view loaded
        		$this->load->view('navbar',$data); //dynamic sidebar view loaded
        		$this->load->view('administrator/refundByOrderID_step3', $data);
        		$this->load->view('footer'); //footer view loaded, 
        		
        	}


       
       
	}

	function hospitals(){
		if($this->input->post()){

			$data = array(
				"HospitalGroupID" => $this->input->post("HospitalGroupID"),
				"HospitalName" => $this->input->post("HospitalName"),
				"Address" => $this->input->post("Address"),
				"Suburb" => $this->input->post("Suburb"),
				"State" => $this->input->post("State"),
				"Country" => $this->input->post("Country"),
				"Postcode" => $this->input->post("Postcode"),
				"Phone" => $this->input->post("Phone"),
				"Fax" => $this->input->post("Fax"),
				"Email" => $this->input->post("Email"),
				"Website" => $this->input->post("Website"),
				"Maternity" => $this->input->post("Maternity"),
				"Rehab" => $this->input->post("Rehab"),
				"OnHold" => $this->input->post("OnHold")

				);
			

			if($this->input->post("action") == "update"){
				$this->db->where("HospitalID", $this->input->post("HospitalID"));
				$this->db->update("hospital", $data);
			} elseif ($this->input->post("action") == "insert"){
				$this->db->insert("hospital", $data);
			} elseif($this->input->post("action") == "delete"){
				$this->db->where("HospitalID", $this->input->post("HospitalID"));
				$this->db->delete("hospital");
			}

			redirect("administrator/hospitals");
		}

		$data['title']='Hospitals';
		
		$this->db->join("hospitalgroup","hospital.HospitalGroupID = hospitalgroup.HospitalGroupID");
		$data['hospitals'] = $this->db->get('hospital')->result();

		$this->load->view('header',$data); //dynamic header view loaded
        $this->load->view('navbar',$data); //dynamic sidebar view loaded
        $this->load->view('administrator/hospitals'); //home page contents loaded, however we can provide contents of any page here to be loaded here
        $this->load->view('footer'); //footer view loaded,
	}
	
	function products(){
		if($this->input->post()){

			$data = array(
				
				"ProductCategoryID" => $this->input->post("ProductCategoryID"),
				"ProductCode" => $this->input->post("ProductCode"),
				"ProductName" => $this->input->post("ProductName"),
				"Active" => $this->input->post("Active"),
				"Notes" => $this->input->post("Notes"),
				"Hire" => $this->input->post("Hire"),
				"TaxCodeID" => $this->input->post("TaxCodeID"),
				"LocationID" => $this->input->post("LocationID"),
				"HasBarcodeID" => $this->input->post("HasBarcodeID"),
				"zzGST" => $this->input->post("zzGST"),
				"AccountCode" => $this->input->post("AccountCode"),
				"SwapToProductID" => $this->input->post("SwapToProductID"),
				"ImportantDatesNotification" => $this->input->post("ImportantDatesNotification"),
				"ShowRefundButton" => $this->input->post("ShowRefundButton"),
				"PriceList" => $this->input->post("PriceList"),


				);
			



			if($this->input->post("action") == "update"){
				$this->db->where("ProductID", $this->input->post("ProductID"));
				$this->db->update("product", $data);
			} elseif ($this->input->post("action") == "insert"){
				$this->db->insert("product", $data);
			} elseif($this->input->post("action") == "delete"){
				$this->db->where("ProductID", $this->input->post("ProductID"));
				$this->db->delete("product");
			}

			redirect("administrator/products");
		}

		$data['title']='products';
		
			$this->db->join("productcategory","product.ProductCategoryID = productcategory.ProductCategoryID");
			$this->db->join("taxcode","product.TaxCodeID = taxcode.TaxCodeID");
			$this->db->join("location","product.LocationID = location.LocationID","left");
			$this->db->join("hasbarcode","product.HasBarcodeID = hasbarcode.HasBarcodeID");
			//SwapToProductID

		
		$data['products'] = $this->db->get('product')->result();

		

		$this->load->view('header',$data); //dynamic header view loaded
        $this->load->view('navbar',$data); //dynamic sidebar view loaded
        $this->load->view('administrator/products'); //home page contents loaded, however we can provide contents of any page here to be loaded here
        $this->load->view('footer'); //footer view loaded,
	}
	
	
	
	
	
	function insurance_providers(){
		if($this->input->post()){

			$data = array(
				
				"InsuranceProviderID" => $this->input->post("InsuranceProviderID"),
				"CompanyName" => $this->input->post("CompanyName"),
				"ProviderNo" => $this->input->post("ProviderNo"),
				"Address" => $this->input->post("Address"),
				"Suburb" => $this->input->post("Suburb"),
				"Postcode" => $this->input->post("Postcode"),
				"State" => $this->input->post("State"),
				"Country" => $this->input->post("Country"),
				"Website" => $this->input->post("Website")

				);
			

			if($this->input->post("action") == "update"){
				$this->db->where("InsuranceProviderID", $this->input->post("InsuranceProviderID"));
				$this->db->update("insuranceprovider", $data);
			} elseif ($this->input->post("action") == "insert"){
				$this->db->insert("insuranceprovider", $data);
			} elseif($this->input->post("action") == "delete"){
				$this->db->where("InsuranceProviderID", $this->input->post("InsuranceProviderID"));
				$this->db->delete("insuranceprovider");
			}

			redirect("administrator/insurance_providers");
		}

		$data['title']='Insurance Providers';
		
		
		$data['insuranceproviders'] = $this->db->get('insuranceprovider')->result();

		$this->load->view('header',$data); //dynamic header view loaded
        $this->load->view('navbar',$data); //dynamic sidebar view loaded
        $this->load->view('administrator/insuranceproviders'); //home page contents loaded, however we can provide contents of any page here to be loaded here
        $this->load->view('footer'); //footer view loaded,
	}
	

	function educators(){
		if($this->input->post()){

			$data = array(

			
				"BusinessName" => $this->input->post("BusinessName"),
				"ABN" => $this->input->post("ABN"),
				"Firstname" => $this->input->post("Firstname"),
				"Lastname" => $this->input->post("Lastname"),
				"Position" => $this->input->post("Position"),
				"Address" => $this->input->post("Address"),
				"Suburb" => $this->input->post("Suburb"),
				"State" => $this->input->post("State"),
				"Postcode" => $this->input->post("Postcode"),
				"Country" => $this->input->post("Country"),
				"Phone" => $this->input->post("Phone"),
				"Fax" => $this->input->post("Fax"),
				"MobilePhone" => $this->input->post("MobilePhone"),
				"Email" => $this->input->post("Email"),
				"ReferralFee" => $this->input->post("ReferralFee"),
				"ClassMinNo" => $this->input->post("ClassMinNo"),
				"ClassMinFee" => $this->input->post("ClassMinFee"),
				"Active" => $this->input->post("Active")
				);


			if($this->input->post("action") == "update"){
				$this->db->where("EducatorID", $this->input->post("EducatorID"));
				$this->db->update("educator", $data);
			} elseif ($this->input->post("action") == "insert"){
				$this->db->insert("educator", $data);
			} elseif($this->input->post("action") == "delete"){
				$this->db->where("EducatorID", $this->input->post("EducatorID"));
				$this->db->delete("educator");
			}

			redirect("administrator/educators");
		}

		$data['title']='Educator';
		
		$data['educators'] = $this->db->get('educator')->result();

		$this->load->view('header',$data); //dynamic header view loaded
        $this->load->view('navbar',$data); //dynamic sidebar view loaded
        $this->load->view('administrator/educators'); //home page contents loaded, however we can provide contents of any page here to be loaded here
        $this->load->view('footer'); //footer view loaded,
	}
	

	function educator_classes(){
		if($this->input->post()){

			$data = array(

				
				
				"EduLocationID" => $this->input->post("EduLocationID"),
				"ClassDate" => $this->input->post("ClassDate"),
				"StartTime" => $this->input->post("StartTime"),
				"Duration" => $this->input->post("Duration"),
				"EducatorID" => $this->input->post("EducatorID"),
				"HospitalID" => $this->input->post("HospitalID"),
				"CourseID" => $this->input->post("CourseID")
				);


			if($this->input->post("action") == "update"){
				$this->db->where("EduClassID", $this->input->post("EduClassID"));
				$this->db->update("educatorclass", $data);
			} elseif ($this->input->post("action") == "insert"){
				$this->db->insert("educatorclass", $data);
			} elseif($this->input->post("action") == "delete"){
				$this->db->where("EduClassID", $this->input->post("EduClassID"));
				$this->db->delete("educatorclass");
			}

			redirect("administrator/educator_classes");
		}

		$data['title']='Educator Classes';

		$this->db->select("
				educatorclass.EduClassID,
				educatorclass.EduLocationID,
				educatorlocation.LocName,
				educatorclass.ClassDate,
				educatorclass.StartTime,
				educatorclass.Duration,
				educatorclass.EducatorID,
				educatorclass.CourseID,
				educatorclass.HospitalID,
				educator.Firstname,
				educator.Lastname,
				hospital.HospitalName,
				educatorclasscourse.CourseName");
				
			$this->db->join("educatorlocation","educatorclass.EduLocationID = educatorlocation.EduLocationID");
			$this->db->join("educator","educatorclass.EducatorID = educator.EducatorID");
			$this->db->join("hospital","educatorclass.HospitalID = hospital.HospitalID");
			$this->db->join("educatorclasscourse","educatorclass.CourseID = educatorclasscourse.CourseID");
		
		$data['educatorclasses'] = $this->db->get('educatorclass')->result();

		$this->load->view('header',$data); //dynamic header view loaded
        $this->load->view('navbar',$data); //dynamic sidebar view loaded
        $this->load->view('administrator/educator_classes'); //home page contents loaded, however we can provide contents of any page here to be loaded here
        $this->load->view('footer'); //footer view loaded,
	}
	
	
	function education_locations(){
		if($this->input->post()){

			$data = array(
				"LocName" => $this->input->post("LocName"),
				"LocAddress" => $this->input->post("LocAddress"),
				"LocSuburb" => $this->input->post("LocSuburb"),
				"LocState" => $this->input->post("LocState"),
				"LocPostcode" => $this->input->post("LocPostcode"),
				"LocContact" => $this->input->post("LocContact"),
				"LocTelephone" => $this->input->post("LocTelephone"),
				"Active" => $this->input->post("Active"),
				"Instructions" => $this->input->post("Instructions"),
				"EducatorPatientsID" => $this->input->post("EducatorPatientsID"),
				"Fee" => $this->input->post("Fee"),
				"ClassSize" => $this->input->post("ClassSize"),
				"LocMapURL" => $this->input->post("LocMapURL")

			
			);
			

			if($this->input->post("action") == "update"){
				$this->db->where("EduLocationID", $this->input->post("EduLocationID"));
				$this->db->update("educatorlocation", $data);
			} elseif ($this->input->post("action") == "insert"){
				$this->db->insert("educatorlocation", $data);
			} elseif($this->input->post("action") == "delete"){
				$this->db->where("EduLocationID", $this->input->post("EduLocationID"));
				$this->db->delete("educatorlocation");
			}

			redirect("administrator/education_locations");
		}

		$data['title']='Account Codes';
		
		$data['educatorlocations'] = $this->db->get('educatorlocation')->result();

		$this->load->view('header',$data); //dynamic header view loaded
        $this->load->view('navbar',$data); //dynamic sidebar view loaded
        $this->load->view('administrator/education_locations'); //home page contents loaded, however we can provide contents of any page here to be loaded here
        $this->load->view('footer'); //footer view loaded,
	}


	function area_managers(){
		if($this->input->post()){

			$data = array("AccountCode" => $this->input->post("AccountCode"));
			$data = array("Description" => $this->input->post("Description"));

			$data = array(
			"BusinessName" => $this->input->post("BusinessName"),
			"ABN" => $this->input->post("ABN"),
			"Firstname" => $this->input->post("Firstname"),
			"Lastname" => $this->input->post("Lastname"),
			"Position" => $this->input->post("Position"),
			"Address" => $this->input->post("Address"),
			"Suburb" => $this->input->post("Suburb"),
			"State" => $this->input->post("State"),
			"Postcode" => $this->input->post("Postcode"),
			"Country" => $this->input->post("Country"),
			"Phone" => $this->input->post("Phone"),
			"Fax" => $this->input->post("Fax"),
			"MobilePhone" => $this->input->post("MobilePhone"),
			"Email" => $this->input->post("Email"),
			"ReferralFee" => $this->input->post("ReferralFee"),
			"StartDate" => $this->input->post("StartDate"),
			"Active" => $this->input->post("Active"));


			if($this->input->post("action") == "update"){
				
				$this->db->where("AreaManagerID", $this->input->post("AreaManagerID"));
				$this->db->update("areamanager", $data);

				$postal_codes = explode(",",$this->input->post("postal_codes"));
				$postcodes = array();

				$this->db->where("AreaManagerID", $this->input->post("AreaManagerID"));
				$this->db->delete("areamanagerpostcode");

				foreach ($postal_codes as $codes) {
					$postcodes[] = array("AreaManagerID" => $this->input->post("AreaManagerID"), "Postcode" => $codes);
				}

				
				$this->db->insert_batch("areamanagerpostcode",$postcodes);
				
			
			} elseif ($this->input->post("action") == "insert"){
				
				$this->db->insert("areamanager", $data);
				$insert_key =  $this->db->insert_id();
				$postal_codes = explode(",",$this->input->post("postal_codes"));
				$postcodes = array();
				foreach ($postal_codes as $codes) {
					$postcodes[] = array("AreaManagerID" => $insert_key, "Postcode" => $codes);
				}

				
				$this->db->insert_batch("areamanagerpostcode",$postcodes);

			
			} elseif($this->input->post("action") == "delete"){
			
				$this->db->where("AreaManagerID", $this->input->post("AreaManagerID"));
				$this->db->delete("areamanager");



			
			}

			redirect("administrator/area_managers");
		}

		$data['title']='Area Managers';
		
		$data['areamanagers'] = $this->db->get('areamanager')->result();

		$this->load->view('header',$data); //dynamic header view loaded
        $this->load->view('navbar',$data); //dynamic sidebar view loaded
        $this->load->view('administrator/area_managers'); //home page contents loaded, however we can provide contents of any page here to be loaded here
        $this->load->view('footer'); //footer view loaded,
	}


	public function addClass()
	{ 
		$newClassLocation = $this->input->post("newClassLocation");
		$newClassHospital = $this->input->post("newClassHospital");
		$newClassEducator = $this->input->post("newClassEducator");
		$newClassCourse = $this->input->post("newClassCourse");
		$newClassDuration = $this->input->post("newClassDuration");
		$newClassStartDate = $this->input->post("newClassStartDate");
		$newClassTime = $this->input->post("newClassTime");
		if ($newClassLocation && $newClassHospital && $newClassEducator
			&& $newClassCourse && $newClassDuration && $newClassStartDate && 
			$newClassTime)
		{
			if (is_numeric($newClassDuration))
			{
				$this->load->library('projectdates');  
				$insertData['EduLocationID'] = $newClassLocation;
				$insertData['ClassDate'] = $this->projectdates->frontEndDateToBackEndDate($newClassStartDate);
				$insertData['StartTime'] = $this->projectdates->frontEndTimeToBackEndDate($newClassTime);
				$insertData['Duration'] = $newClassDuration;
				$insertData['EducatorID'] = $newClassEducator;
				$insertData['HospitalID'] = $newClassHospital;
				$insertData['CourseID'] = $newClassCourse; 
				if ($this->db->insert('educatorclass', $insertData))
				{
					$this->classes_admin();
				}
			}
			else 
			{
				echo "Duration is not a number";
			}
		}
		else 
		{
			echo "Not enough data";
		}
	}

	public function classes_admin()
	{ 
		$sqlEducatorLocations = "SELECT * FROM educatorlocation";
		$sqlEducators = "SELECT * FROM educator";
		$sqlHospitals = "SELECT * FROM hospital";
		$sqlCourses = "SELECT * FROM educatorclasscourse";

		$queryEducatorLocations = $this->db->query($sqlEducatorLocations);
		$queryEducators = $this->db->query($sqlEducators);
		$queryHospitals = $this->db->query($sqlHospitals);
		$queryCourses = $this->db->query($sqlCourses);

		if ($queryEducatorLocations->num_rows() > 0 && $queryEducators->num_rows() > 0 && $queryHospitals->num_rows() > 0 
			&& $queryCourses->num_rows() > 0)
		{
			$data['hospitals'] = $queryHospitals->result();
			$data['courses'] = $queryCourses->result();
			$data['educatorlocations'] = $queryEducatorLocations->result();
			$data['educators'] = $queryEducators->result(); 
			$data['title'] = 'Classes Administration';

			$this->load->view('header',$data); //dynamic header view loaded
	        $this->load->view('navbar'); //dynamic sidebar view loaded
	        $this->load->view('administrator/classes_admin'); //home page contents loaded, however we can provide contents of any page here to be loaded here
	        $this->load->view('footer'); //footer view loaded,
	    }
	    else 
	    {
	    	echo "Could not take out the required data.";
	    }
	}

}