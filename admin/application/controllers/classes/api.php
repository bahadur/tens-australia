<?php

class api extends CI_Controller {


	function __construct(){
		 
		parent::__construct();

		 header('Content-Type: application/json');
		 $this->load->helper('ssp');
         $this->load->library('projectdates');
         $this->load->library('httpresponses');

	} 

    function bookClass() {
        if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator'))) {  
              $req = json_decode(file_get_contents('php://input'));  
              if (($req->CustomerID) && isset($req->EduClassID)) {  
                    $customerId = $req->CustomerID;
                    $eduClassId = $req->EduClassID; 
                    $qClasses = $this->db->query('SELECT * FROM educatorclasscustomer WHERE EduClassID = '.$eduClassId);
                    if ($qClasses) {
                        $numberAttending = $qClasses->num_rows() + 1;
                        $foundCustomerId = null;
                        $allClasses = $qClasses->result();
                        foreach ($allClasses as $educlass) {
                            if ($educlass->CustomerID == $customerId) {
                                $foundCustomerId = $educlass;
                                break;
                            }
                        }
                        if ($foundCustomerId == null) {
                            $insertData['EduClassID'] = $eduClassId;
                            $insertData['CustomerID'] = $customerId;
                            $insertData['NumberAttending'] = $numberAttending; 
                            $insertData['Cancelled'] = 0;
                            $insertData['BookingDateTime'] = $this->projectdates->mysqlNow(); 
                            $insertData['CancelledDateTime'] = null;
                            if ($this->db->insert('educatorclasscustomer', $insertData)) {
                                log_message('error', 'SUCCESS');
                                $this->httpresponses->respondJsonify(array('success' => true), 200);
                            } else {
                                log_message('error', $this->db->last_query());
                                log_message('error', $this->db->_error_message());
                                $this->httpresponses->respondJsonify(array('message' => 'Could not book to this class'), 400);
                            };
                        } else {
                            $this->httpresponses->respondJsonify(array('message' => 'The customer is already booked to this class'), 400);
                        }
                    } else {
                        log_message('error', $this->db->last_query());
                        log_message('error', $this->db->_error_message());
                        $this->httpresponses->respondJsonify(array('message' => 'Could not book to this class-.'), 400);
                    } 
              } else {
                    $this->httpresponses->respondJsonify(array('message' => 'Invalid parameters.'), 400);
              }
        } else {
          $this->httpresponses->respondJsonify(array('message' => 'Unauthorized'), 401);
        } 
    }

    function classes($action=""){

        if($action == "update"){
            $this->db->where("EduClassID", str_replace(".","",$this->input->post('id')));
            
            ($this->db->update("educatorclass",array("ClassDate"=>$this->input->post('value'))));
            
            
            $qtd = $this->db->affected_rows();
            if ($qtd >0){
                $resp_json = array( 'msg' => "Success!",'error'=>0);
            }
            else
            {
                $resp_json = array( 'msg'=>"Failure", 'error'=>1);
            }
                
            echo json_encode($resp_json);
            
        } else {
        $query = $this->db->query("
                select
                educatorclasscustomer.EducatorClassCustomerID, 
                educatorclasscustomer.EduClassID, 
                educatorclasscustomer.CustomerID, 
                educatorclasscustomer.NumberAttending, 
                educatorclasscustomer.BookingDateTime, 
                educatorclasscustomer.Cancelled, 
                educatorclasscustomer.CancelledDateTime,
                educatorclass.ClassDate, 
                educatorclass.StartTime, 
                educatorlocation.LocName, 
                educatorlocation.LocSuburb, 
                educator.BusinessName
                FROM educatorlocation INNER JOIN (educator INNER JOIN (educatorclass INNER JOIN educatorclasscustomer ON educatorclass.EduClassID = educatorclasscustomer.EduClassID) ON educator.EducatorID = educatorclass.EducatorID) ON educatorlocation.EduLocationID = educatorclass.EduLocationID
                WHERE educatorclasscustomer.CustomerID = ".$this->input->post('customerid'));

        $data = array();
        if ($query)
        {
            foreach ($query->result() as $row) { 
                                
              $data[]  = array(
                    'eduClassCustomerId' => $row->EducatorClassCustomerID,
                    'eduClassID' => $row->EduClassID, 
                    'customerID' => $row->CustomerID, 
                    'classDate' => $row->ClassDate,
                    'startTime' => $row->StartTime, 
                    'locName' => $row->LocName, 
                    'locSuburb' => $row->LocSuburb,
                    'businessName' => $row->BusinessName,
                    'numberAttending' => $row->NumberAttending, 
                    'cancelled' => $row->Cancelled,
                    'nullcell' => ''  
                );
            } 
        }
        echo json_encode(
            array("draw"=>$this->input->post("draw"), "recordsTotal"=>4, "recordsFiltered"=>4, "data" => $data)
            );
        }
    }

	function addCustomerClass(){ 
		$this->load->library('projectdates');
		$eduClassId = $this->input->post("EduClassID"); 
		if ($q = $this->db->query("SELECT MAX(NumberAttending) AS ClassSize FROM educatorclasscustomer GROUP BY EduClassID HAVING EduClassID = $eduClassId"))
		{
			
			$this->db->set("EduClassID", $this->input->post("EduClassID"));
			$this->db->set("CustomerID", $this->input->post("CustomerID")); 
			$this->db->set("BookingDateTime", date($this->projectdates->mysqlNow()));
			if ($q->num_rows() > 0)
			{
				$r = $q->result();
				if ($r[0] != null)
				{
					$this->db->set("NumberAttending", $r[0]->ClassSize + 1);
				}
				else 
				{
					$this->db->set("NumberAttending", 1);
				}
			}
			else 
			{
				$this->db->set("NumberAttending", 1);
			}

			if ($this->db->insert("educatorclasscustomer"))
			{ 
				echo json_encode(array("status"=>1));
			}
			else 
			{
				echo json_encode(array("status"=>$this->db->_error_number(), 
					"message"=>$this->db->_error_message()));
			}
		}
		else 
		{ 
			echo $this->db->last_query();
				echo json_encode(array("status"=>"0", 
					"message"=>$this->db->_error_message()));
		}
	}

	function getClasses()
	{

        $table = 'educatorclass';
        $primaryKey = 'EduClassID'; 
         
        $test  = "SELECT educatorclass.EduClassID, ClassDate, StartTime, CourseName, HospitalName, LocName, 
        	CONCAT(educator.Firstname, ' ', educator.Lastname, ',', educator.BusinessName)
        	COUNT(NumberAttending) FROM educatorclass";

        $joins ="INNER JOIN educatorclasscourse ON educatorclass.CourseID = educatorclasscourse.CourseID
        	INNER JOIN hospital ON educatorclass.HospitalID = hospital.HospitalID
        	INNER JOIN educator ON educator.EducatorID = educatorclass.EducatorID
        	INNER JOIN educatorlocation on educatorclass.EduLocationID = educatorlocation.EduLocationID
        	LEFT JOIN educatorclasscustomer ON educatorclass.EduClassID = educatorclasscustomer.EduClassID
            ";

         $joinTable = '';  
        


        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes 
			
        $columns = array( 
            array(
                'db' => 'EduClassID',
                'dt' => 'eduClassId',
                'table' => "educatorclass",
                'alias' => 'EduClassID'
            ),
            array(
                'db' => "ClassDate",
                'dt' => 'classDate',
                'table' => "",
                'alias' => 'ClassDate'
            ),
            array(
                'db' => "StartTime",
                'dt' => 'startTime',
                'table' => "",
                'alias' => 'StartTime'
            ),
            array(
                'db' => "Duration",
                'dt' => 'duration',
                'table' => "",
                'alias' => 'Duration'
            ),
            array(
                'db' => "CourseName",
                'dt' => 'courseName',
                'table' => "",
                'alias' => 'CourseName'
            ),
            array(
                'db' => "HospitalName",
                'dt' => 'hospitalName',
                'table' => "",
                'alias' => 'HospitalName'
            ), 
            array(
                'db' => "LocName",
                'dt' => 'locationName',
                'table' => "",
                'alias' => 'LocationName'
            ),
            array(
                'db' => "
                	CONCAT(IF(educator.Firstname IS NULL, '', educator.Firstname), ' ', IF(educator.Lastname IS NULL, '', educator.Lastname), ',', IF(educator.BusinessName IS NULL, '', educator.BusinessName))",
                'dt' => 'educatorName',
                'table' => "",
                'alias' => 'EducatorName'
            ),
            array(
                'db' => "COUNT(NumberAttending)",
                'dt' => 'numberattending',
                'table' => "",
                'alias' => 'NumberAttending'
            ),
            array(
                'db' => "Instructions",
                'dt' => 'instructions',
                'table' => "",
                'alias' => 'Instructions'
            )
        );
  
        echo json_encode(simple(
        					$_GET, 
        					$table,
        					$primaryKey, 
        					$columns, 
        					$joins, 
        					$joinTable, 
        					'',
        					'educatorclass.EduClassID')
        				);  
  
	}

	public function cancelClass()
	{ 
		$eduClassCustomerId = $this->input->post("eduCustomerClassIdToCancel");
		if ($eduClassCustomerId)
		{
			$this->db->where('EducatorClassCustomerID', $eduClassCustomerId);
			$update['Cancelled'] = 1;
			if ($this->db->update('educatorclasscustomer', $update))
			{
				echo '1';
			}
			else 
			{
				echo $this->db->last_query();
				echo json_encode(array("status"=>$this->db->_error_number(), 
					"message"=>$this->db->_error_message()));
			}
		}
		else 
		{
			echo '0';
		}
	}

	public function getSelect2Classes()
	{
		$sqlString = "SELECT `educatorclass`.`EduClassID` AS  'EduClassID', ClassDate AS  'ClassDate', StartTime AS  'StartTime', Duration AS  'Duration', CourseName AS  'CourseName', HospitalName AS  'HospitalName', LocName AS  'LocationName', 
			CONCAT(IF(educator.Firstname IS NULL, '', educator.Firstname), ' ', IF(educator.Lastname IS NULL, '', educator.Lastname), ',', IF(educator.BusinessName IS NULL, '', educator.BusinessName)) AS  'EducatorName', educator.EducatorID, COUNT(NumberAttending) AS  'NumberAttending', Instructions AS  'Instructions'
			 FROM `educatorclass`
			 INNER JOIN educatorclasscourse ON educatorclass.CourseID = educatorclasscourse.CourseID
        	INNER JOIN hospital ON educatorclass.HospitalID = hospital.HospitalID
        	INNER JOIN educator ON educator.EducatorID = educatorclass.EducatorID
        	INNER JOIN educatorlocation on educatorclass.EduLocationID = educatorlocation.EduLocationID
        	LEFT JOIN educatorclasscustomer ON educatorclass.EduClassID = educatorclasscustomer.EduClassID 
			 GROUP BY educatorclass.EduClassID";
    	$query = $this->db->query($sqlString);
    	if ($query->num_rows() > 0)
    	{
    		$this->load->library('projectdates');
    		$res = $query->result();
    		$resultsArray = array();
    		foreach ($res as $class) 
    		{
    			$rowString = 
    				$this->projectdates->backendDateToFrontEndDate($class->ClassDate).' - '.
    				$this->projectdates->backendDateToFrontEndTime($class->ClassDate).' - '.
    				$class->Duration.' - '. 
    				$class->EducatorName.' - '. 
    				$class->LocationName.' - '. 
    				$class->HospitalName.' - '. 
    				$class->NumberAttending.' - '. 
    				$class->Instructions;


	    		$resultsArray[] = array(
	    			"id" => $class->EduClassID,
	    			"text" => $rowString
	    		);
	    	}
	    	$returnArray = array();
	    	$returnArray[] = array(
	    		"id" => 1,
	    		"text" => "ClassDate - Start Time - Duration - Educator Name - Location Name - Hospital Name - # Attending - Instructions",
	    		"children" => $resultsArray
	    	); 
	    	echo json_encode($returnArray); 
	    	return;
    	}
    	echo "N/A";
	}
}