<?php
 
class vc extends CI_Controller {


	function __construct(){ 
		parent::__construct();
    if (!$this->tank_auth->is_logged_in_as(array('staff', 'administrator')))
    { 
    	redirect(base_url().'auth/login/');
		}
	}

	function index(){ 
		$data['title']='Class List'; 
		$this->load->view('header',$data); //dynamic header view loaded
    $this->load->view('navbar'); //dynamic sidebar view loaded
    $this->load->view('equipment/move_location'); //home page contents loaded, however we can provide contents of any page here to be loaded here
    $this->load->view('footer'); //footer view loaded,  
	}
	
	function ClassList(){ 
		$data['title']='Home Page'; 
		$baseUrl = base_url();
		$data['highlightLink'] = $baseUrl . 'classes/ClassList';
		$data['expandClasses'] = "expandClasses";
		$this->load->view('header',$data); //dynamic header view loaded
    $this->load->view('navbar',$data); //dynamic sidebar view loaded
    $this->load->view('class/classes'); //home page contents loaded, however we can provide contents of any page here to be loaded here
    $this->load->view('footer'); //footer view loaded,  
	}

	private function getClassCustomers($eduClassId)
	{ 
		$query = $this->db->query("
						SELECT 
						educatorclasscustomer.EduClassID, 
						customer.Firstname, 
						customer.LastName, 
						customer.BillingAddress, 
						customer.BillingSuburb, 
						customer.MobilePhone, 
						educatorclasscustomer.NumberAttending, 
						educatorclasscustomer.BookingDateTime, 
						educatorclasscustomer.Cancelled, 
						educatorclasscustomer.CancelledDateTime 
						FROM customer INNER JOIN educatorclasscustomer ON customer.CustomerID = educatorclasscustomer.CustomerID 
						AND educatorclasscustomer.EduClassID = $eduClassId;"
						); 
		return $query->result();  
	}

	function detail($classid){


		$query = $this->db->query("
				SELECT
				educatorclass.EduClassID, 
				educatorclass.EduLocationID, 
				educatorclass.ClassDate, 
				educatorclass.StartTime, 
				educatorclass.Duration, 
				educatorclass.EducatorID, 
				educatorclass.HospitalID, 
				educatorclass.CourseID, 
				educatorlocation.ClassSize, 
				educatorlocation.LocName, 
				educator.BusinessName, 
				hospital.HospitalName, 
				educatorclasscourse.CourseName
				
				from hospital
				INNER JOIN (educatorlocation RIGHT JOIN (educator INNER JOIN (educatorclasscourse INNER JOIN educatorclass ON educatorclasscourse.CourseID = educatorclass.CourseID) ON educator.EducatorID = educatorclass.EducatorID) ON educatorlocation.EduLocationID = educatorclass.EduLocationID) ON hospital.HospitalID = educatorclass.HospitalID
				where educatorclass.EduClassID = ".$classid."
				
				ORDER BY educatorclass.ClassDate, educatorclass.StartTime
		");
		if ($query->num_rows() > 0)
		{
			$result = $query->result();
			$data["result"] = $result[0];
			$data['title']='Home Page';
			$data['customers'] = $this->getClassCustomers($classid);  

			$baseUrl = base_url();
			$data['highlightLink'] = $baseUrl . 'classes/ClassList';
			$data['expandClasses'] = "expandClasses";
			$this->load->view('header',$data); //dynamic header view loaded
	        $this->load->view('navbar'); //dynamic sidebar view loaded
	        $this->load->view('class/classDetail'); //home page contents loaded, however we can provide contents of any page here to be loaded here
	        $this->load->view('footer'); //footer view loaded, 
	    }
	    else 
	    {
	    	echo "No results";
	    }

	}


}

?>
