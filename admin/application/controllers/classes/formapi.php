<?php 

class formapi extends CI_Controller {


  function __construct(){
    parent::__construct();
    $this->load->library('Datatables');
    $this->load->library('httpresponses'); 
  } 

  function getClasses() { 
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator'))) {
      $this->datatables
          ->select('educatorclass.EduClassID, educatorclass.ClassDate, educatorclass.StartTime, 
            educatorlocation.LocName AS Location, educatorlocation.LocSuburb AS Suburb, educator.BusinessName AS Educator, 
            educatorlocation.ClassSize, (ClassSize - SumOfNumberAttending) AS Available, 
            educatorlocation.Instructions')
          ->from('educatorlocation 
            INNER JOIN (educator INNER JOIN 
              ((SELECT educatorclasscustomer.EduClassID, Sum(educatorclasscustomer.NumberAttending) AS SumOfNumberAttending 
                FROM educatorclasscustomer WHERE (((educatorclasscustomer.Cancelled)=False)) GROUP BY educatorclasscustomer.EduClassID) 
            AS CurrentBookings RIGHT JOIN educatorclass ON CurrentBookings.EduClassID = educatorclass.EduClassID) 
              ON educator.EducatorID = educatorclass.EducatorID) ON educatorlocation.EduLocationID = educatorclass.EduLocationID')
          ->where('educatorclass.ClassDate >= Now()'); 
      $this->httpresponses->respondJson($this->datatables->generate(), 200);
    } else {
      $this->httpresponses->respondJsonify(array('message' => 'Unauthorized'), 401);
    } 
  }
}