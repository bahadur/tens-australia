<?php

/*
 
*/
class Emailer extends CI_Controller {


	function __construct(){ 
		parent::__construct();
        if (!$this->tank_auth->is_logged_in_as(array('staff', 'administrator')))
        { 
        	redirect(base_url().'auth/login/');
		}
	}  

	public function remindCustomerClass()
	{    
		$emailTemplateId = $this->input->post("emailTemplateId");
		$customerId = $this->input->post("emailCustomerId");
		$eduClassCustomerId = $this->input->post("emailEduCustomerClassId");
		$qStr = "SELECT customer.Email, customer.Firstname, customer.Lastname, educatorclass.StartTime, educatorclass.Duration, 
			educatorclass.ClassDate, educatorlocation.LocAddress, educatorlocation.LocSuburb, educatorlocation.LocState, 
			educatorlocation.LocPostcode, educatorlocation.LocMapURL, educatorclasscourse.CourseNameHTML FROM educatorclasscourse 
			INNER JOIN (educatorlocation INNER JOIN (educatorclass INNER JOIN (customer INNER JOIN educatorclasscustomer ON 
				customer.CustomerID = educatorclasscustomer.CustomerID) ON educatorclass.EduClassID = educatorclasscustomer.EduClassID) ON 
			educatorlocation.EduLocationID = educatorclass.EduLocationID) ON educatorclasscourse.CourseID = educatorclass.CourseID 
			WHERE educatorclasscustomer.EducatorClassCustomerID= ".$eduClassCustomerId; 
		if ($q = $this->db->query($qStr))
		{
			$r = $q->result();
			$qCustomer = $this->db->query("SELECT * FROM customer WHERE CustomerID = ".$customerId);
			$qEmailTemplate = $this->db->query("SELECT Subject, Body FROM emailtemplate WHERE EmailTemplateID = ".$emailTemplateId);
			$qFooter = $this->db->query("SELECT CommonEmailFooter FROM config WHERE ConfigID = 1");
			if ($qEmailTemplate && $qFooter && $qCustomer)
			{ 
				$rC = $qCustomer->result();
				$rET = $qEmailTemplate->result();
				$rF = $qFooter->result(); 
				$data['customer'] = $rC[0]; 
				$data['emailSubject'] = $rET[0]->Subject;
				$data['emailBody'] = sprintf($rET[0]->Body, $r[0]->Firstname, $r[0]->CourseNameHTML, $r[0]->StartTime, 
					$r[0]->Duration, $r[0]->ClassDate, $r[0]->LocAddress, $r[0]->LocSuburb, $r[0]->LocState, $r[0]->LocPostcode,
					$r[0]->LocMapURL, base_url().'assets/img/google.jpg').'<br/><br/>'.sprintf($rF[0]->CommonEmailFooter, base_url().'assets/img/email-logo.jpg'); 
				$this->load->view('sendEmail/emailer', $data); 
			}
		}
		else 
		{
			echo "Could not get data";
		} 
	}
 
	public function send()
	{   
		$this->load->library('tensemailer');
		
		$sender = $this->input->post('sender');
		$subject = $this->input->post('subject');
		$body = $this->input->post('body');
		$recipient = $this->input->post('recipient');
		$recipientFullName = $this->input->post('recipientFullName');

		$mail = new SaveSentMailer();
		$mail->IsSMTP();
		$mail->SMTPAuth   = true;
		$mail->Host       = "new.tensaustralia.com";
		$mail->Port       = 26;
		$mail->Username   = "boss@new.tensaustralia.com";
		$mail->Password   = "B0ssT3ns#";
		$mail->SMTPSecure = 'tls';
		$mail->SetFrom($sender, 'Tens Australia');
		$mail->AddReplyTo($sender,"Tens Australia");
		$mail->Subject    = $subject;
		$mail->AltBody    = "Message.";
		$mail->MsgHTML($body);
		$mail->IsHTML(true);   
		$mail->AddAddress($recipient, $recipientFullName);
        $attachmentname = $this->input->post('filename');
		if ($attachmentname != null) {    
		    $mail->AddAttachment("tempfiles/".$attachmentname);
		}  
		if(!$mail->Send()) {
	  		echo $mail->ErrorInfo;
	  		return;
		} else {
			$mail->copyToFolder('Sent');
	  		echo "1";
	  		return;
		}
		echo "No parameters.";
	}

}