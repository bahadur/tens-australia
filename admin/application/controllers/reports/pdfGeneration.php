<?php


class pdfgeneration extends CI_Controller{

	function __construct(){

		parent::__construct();  
    if (!$this->tank_auth->is_logged_in_as(array('staff', 'administrator')))
    { 
        redirect(base_url().'auth/login/');
    }
	}
 
	public function emailRefundInvoice($emailTemplateId = 1, $orderId = 1, $customerId = 1) { 
		$fileName = $this->previewInvoice($orderId, true);
		if ($fileName)
		{
			$q = $this->db->query("SELECT * FROM emailtemplate WHERE EmailTemplateID = ".$emailTemplateId);
			if ($q && $q->num_rows() > 0)
			{
				if ($customerId)
				{
					$cq = $this->db->query("SELECT * FROM customer WHERE CustomerID = ".$customerId);
					if ($cq && $cq->num_rows() > 0)
					{
						$cr = $cq->result();
						$data['customer'] = $cr[0];
					}
				}
				$qef = $this->db->query("SELECT CommonEmailFooter FROM config WHERE ConfigID = 1");
				if ($qef)
				{
					$r = $q->result();
					$rF = $qef->result();
					if ($data['customer'])
					{
						$data['emailBody'] = sprintf($r[0]->Body, $data['customer']->Firstname)."<br/><br/>".$rF[0]->CommonEmailFooter;
					}
					else 
					{
						$data['emailBody'] = $r[0]->Body."<br/><br/>".$rF[0]->CommonEmailFooter;
					} 
					$data['emailSubject'] = $r[0]->Subject; 
					$data['filename'] = $fileName;
					$this->load->view('sendEmail/emailer', $data); 
				}
				else 
				{
					echo "Could not find the email footer template";
				}
			}
			else 
			{
				echo "Could not find the email template";
			}
		}
		else 
		{
			echo "Could not create an invoice pdf.";
		}
	}

	public function emailQuote($emailTemplateId = 1, $orderId, $customerId) {	
		$fileName = $this->previewInvoice($orderId, true);
		if ($fileName)
		{
			$q = $this->db->query("SELECT * FROM emailtemplate WHERE EmailTemplateID = ".$emailTemplateId);
			if ($q && $q->num_rows() > 0)
			{
				if ($customerId)
				{
					$cq = $this->db->query("SELECT * FROM customer WHERE CustomerID = ".$customerId);
					if ($cq && $cq->num_rows() > 0)
					{
						$cr = $cq->result();
						$data['customer'] = $cr[0];
					}
				}
				$qef = $this->db->query("SELECT CommonEmailFooter FROM config WHERE ConfigID = 1");
				if ($qef)
				{
					$r = $q->result();
					$rF = $qef->result();
					if ($data['customer'])
					{
						$data['emailBody'] = sprintf($r[0]->Body, $data['customer']->Firstname)."<br/><br/>".$rF[0]->CommonEmailFooter;
					}
					else 
					{
						$data['emailBody'] = $r[0]->Body."<br/><br/>".$rF[0]->CommonEmailFooter;
					} 
					$data['emailSubject'] = $r[0]->Subject; 
					$data['filename'] = $fileName;
					$this->load->view('sendEmail/emailer', $data); 
				}
				else 
				{
					echo "Could not find the email footer template";
				}
			}
			else 
			{
				echo "Could not find the email template";
			}
		}
		else 
		{
			echo "Could not create an invoice pdf.";
		}
	}

	public function emailInvoice($emailTemplateId = 1, $orderId, $customerId)
	{	
		$fileName = $this->previewInvoice($orderId, true);
		if ($fileName)
		{
			$q = $this->db->query("SELECT * FROM emailtemplate WHERE EmailTemplateID = ".$emailTemplateId);
			if ($q && $q->num_rows() > 0)
			{
				if ($customerId)
				{
					$cq = $this->db->query("SELECT * FROM customer WHERE CustomerID = ".$customerId);
					if ($cq && $cq->num_rows() > 0)
					{
						$cr = $cq->result();
						$data['customer'] = $cr[0];
					}
				}
				$qef = $this->db->query("SELECT CommonEmailFooter FROM config WHERE ConfigID = 1");
				if ($qef)
				{
					$r = $q->result();
					$rF = $qef->result();
					$footer = sprintf($rF[0]->CommonEmailFooter, base_url().'assets/img/email-logo.jpg');
					if ($data['customer'])
					{
						$data['emailBody'] = sprintf($r[0]->Body, $data['customer']->Firstname)."<br/><br/>".$footer;
					}
					else 
					{
						$data['emailBody'] = $r[0]->Body."<br/><br/>".$footer;
					} 
					$data['emailSubject'] = $r[0]->Subject;   
					$data['filename'] = $fileName;
					$this->load->view('sendEmail/emailer', $data); 
				}
				else 
				{
					echo "Could not find the email footer template";
				}
			}
			else 
			{
				echo "Could not find the email template";
			}
		}
		else 
		{
			echo "Could not create an invoice pdf.";
		}
	}


	/**
	 * Generates a pdf of a tax invoice
	 * @param  [type]  $orderId    [description]
	 * @param  boolean $createFile [description]
	 * @return [type]              [description]
	 */
	public function previewInvoice($orderId, $createFile = false)
	{

		//$html = $this->load->view('previewInvoice');
		$orderId = strip_tags($orderId);
 

		/*
			Get payment information
		*/

		$paymentMethods = $this->getPaymentMethods($orderId);
 
		/*
		*	Get important information
		*/
		$importantInfo = $this->getImportantInformation($orderId);

 		$sqlQuery = 'SELECT order.OrderID, customer.Firstname, customer.LastName, order.BillingAddress, order.BillingSuburb, order.BillingState, 
 			order.BusinessPhone,
			order.BillingPostcode, order.HomePhone, order.MobilePhone, order.BusinessPhone, CONCAT(educator.Firstname, " ", educator.Lastname) AS Educator, 
			order.CustomerReference, orderdetail.Quantity, product.ProductCode, product.ProductName,
			CONCAT(ProductName, "(", Right((SELECT Barcode FROM stockmovedetail WHERE OrderDetailID=orderdetail.OrderDetailID LIMIT 1),14), ")") AS ProductDetail, 
			orderdetail.UnitPrice, Round(orderdetail.Quantity * orderdetail.UnitPrice,2) AS TotalAmount, orderdetail.GST_Amount, 
			SumOfAmount AS PaymentTotal, 
			order.orderDate, IF(order.HomePhone IS NULL,order.MobilePhone,order.HomePhone) AS CustomerTel, hospital.HospitalName, 
			product.Hire AS HasHire, shipmethod.ShipMethod, customer.BusinessName, order.InvoiceDate, 
			orderdetail.OrderDetailID FROM shipmethod RIGHT JOIN (product INNER JOIN ((hospital RIGHT JOIN (educator RIGHT JOIN (customer INNER JOIN 
			((SELECT orderdetail.OrderID, Sum(transactiondetail.Amount) AS SumOfAmount FROM `transaction` INNER JOIN (orderdetail INNER JOIN transactiondetail 
			ON orderdetail.orderdetailID = transactiondetail.orderdetailID) ON transaction.TransactionID = transactiondetail.TransactionID 
			WHERE (((transaction.TransactionTypeID)=1)) GROUP BY orderdetail.OrderID)  AS PaymentTotal RIGHT JOIN `order` ON PaymentTotal.OrderID = order.OrderID) 
			ON customer.customerID = order.customerID) ON educator.educatorID = order.educatorID) ON hospital.HospitalID = order.HospitalID) 
			INNER JOIN orderdetail ON order.OrderID = orderdetail.OrderID) ON product.ProductID = orderdetail.ProductID) ON shipmethod.ShipMethodID = order.ShipMethodID
			WHERE order.OrderID = '.$orderId;
		$generalInfoQuery = $this->db->query($sqlQuery);
		if ($generalInfoQuery && $generalInfoQuery->num_rows() > 0)
		{ 
			$generalInfoResult = $generalInfoQuery->result(); 
			$data['importantInfo'] = $importantInfo;
			$data['paymentMethods'] = $paymentMethods;
			$data['invoiceInfo'] = $generalInfoResult[0];
			$data['invoiceItems'] = $generalInfoResult; 
		    $html = $this->load->view('pdfGeneration/previewInvoice', $data,TRUE); // render the view into HTML 
		    $this->load->library('pdf');
		    $pdf = $this->pdf->load(); 
		    $pdf->WriteHTML($html);  
		    if ($createFile)
		    {
		    	$fileName = date('d-m-Y').'-'.$generalInfoResult[0]->OrderID.'-invoice.pdf';
		    	$pdf->Output('tempfiles/'.$fileName,'F');
		    	return $fileName; 
		    }
		    else 
		    {
		    	$pdf->Output('invoice.pdf','I'); 
		    } 
		}
		else 
		{
			echo "Cannot print out the pdf";
		}
		return null;
	}

	/**
	 * Get a text to append at the end of an invoice
	 * @param  [type] $orderId [description]
	 * @return [type]          [description]
	 */
	private function getImportantInformation($orderId)
	{
		/*
			Get the expected return date which is invoice date + returnexpecteddate
			If NULL = return null. This should not be null, though.
		*/
		$datesSql = "SELECT IF(InvoiceDate IS NULL, NULL, 
			DATE_ADD(InvoiceDate, INTERVAL (SELECT ReturnExpectedDuePlusDays 
				FROM config WHERE config.ConfigID = 1 LIMIT 1) DAY)) AS ReturnDate, 
			InvoiceDate FROM `order` WHERE order.OrderID = $orderId";
		$datesQuery = $this->db->query($datesSql);

	    $strResult = "Please RETURN the complete kit within 1 week of discharge from the hospital. Please understand, if the FreeMOM TENS Kit is not returned, you may be liable for the replacement cost of $295.";
		if ($datesQuery && $datesQuery->num_rows() > 0)
		{
			$dateResult = $datesQuery->result();
			$date = $dateResult[0];
			if ($date->InvoiceDate == NULL)
			{
				return NULL;
			}
			else 
			{
				$format = "Y-m-d H:i:s";
				$invoiceDate = DateTime::createFromFormat($format, $date->InvoiceDate); 
				$returnDate = DateTime::createFromFormat($format, $date->ReturnDate); 
				$productsDueSql = "SELECT COUNT(product.ProductID) AS CountOfProductID FROM product 
					INNER JOIN (stockmove INNER JOIN stockmovedetail ON stockmove.StockMoveID = stockmovedetail.StockMoveID) 
					ON product.ProductID = stockmovedetail.ProductID WHERE stockmove.ToOrderID = $orderId AND 
					stockmove.StockMoveTypeID = 2 AND product.ImportantDatesNotification = -1  AND product.Hire = -1";
				$productsDueQuery = $this->db->query($productsDueSql);
				if ($productsDueQuery && $productsDueQuery->num_rows() > 0 
					&& $invoiceDate && $returnDate)
				{ 
					$strResult = 
						"IMPORTANT DATES - Hire start date: ".
						$invoiceDate->format("d/m/Y").
						" and Hire return date: ".$returnDate->format("d/m/Y");
					
					return $strResult;
				}
			}
		}
			// if invoice date is null, return null;
		return $strResult ;
	}

	private function getPaymentMethods($orderId)
	{

		/*
		*	Get payment method. Append with comma : `,`
		*/ 
		$paymentMethodsSql = "SELECT DISTINCT paymentmethod.PaymentMethod FROM paymentmethod 
			INNER JOIN `transaction` ON paymentmethod.PaymentMethodID = transaction.PaymentMethodID
			WHERE transaction.OrderID= $orderId  AND transaction.TransactionTypeID= 1";
		$paymentsQuery = $this->db->query($paymentMethodsSql);
		if ($paymentsQuery)
		{
			$numRows = $paymentsQuery->num_rows();
			if ($numRows > 1)
			{
				$paymentsResult = $paymentsQuery->result();  
				$paymentMethodsString = $paymentsResult[0]->PaymentMethod.", ";
				for ($i = 1; $i < $numRows - 1; $i++)
				{
					$paymentMethodsString = $paymentMethodsString.$paymentsResult[$i]->PaymentMethod.", ";
				}
				$paymentMethodsString = $paymentMethodsString.$paymentsResult[$numRows - 1]->PaymentMethod; 
				return $paymentMethodsString;
			}
			else if ($numRows == 1)
			{
				$paymentsResult = $paymentsQuery->result();   
				return $paymentsResult[0]->PaymentMethod; 
			}
		}
		return "";
	}
 
	/**
	 * Similar to invoice, except it is just a quote
	 * @param  [type] $orderId    [description]
	 * @param  [type] $createFile [description]
	 * @return [type]             [description]
	 */
	public function previewQuote($orderId, $createFile)
	{ 
		$orderId = strip_tags($orderId);  
		$paymentMethods = $this->getPaymentMethods($orderId); 
		$importantInfo = $this->getImportantInformation($orderId);

 		$sqlQuery = 'SELECT order.OrderID, customer.Firstname, customer.LastName, order.BillingAddress, order.BillingSuburb, order.BillingState, 
 			order.BusinessPhone,
			order.BillingPostcode, order.HomePhone, order.MobilePhone, order.BusinessPhone, CONCAT(educator.Firstname, " ", educator.Lastname) AS Educator, 
			order.CustomerReference, orderdetail.Quantity, product.ProductCode, product.ProductName,
			CONCAT(ProductName, "(", Right((SELECT Barcode FROM stockmovedetail WHERE OrderDetailID=orderdetail.OrderDetailID LIMIT 1),14), ")") AS ProductDetail, 
			orderdetail.UnitPrice, Round(orderdetail.Quantity * orderdetail.UnitPrice,2) AS TotalAmount, orderdetail.GST_Amount, 
			SumOfAmount AS PaymentTotal, 
			order.orderDate, IF(order.HomePhone IS NULL,order.MobilePhone,order.HomePhone) AS CustomerTel, hospital.HospitalName, 
			product.Hire AS HasHire, shipmethod.ShipMethod, customer.BusinessName, order.InvoiceDate, 
			orderdetail.OrderDetailID FROM shipmethod RIGHT JOIN (product INNER JOIN ((hospital RIGHT JOIN (educator RIGHT JOIN (customer INNER JOIN 
			((SELECT orderdetail.OrderID, Sum(transactiondetail.Amount) AS SumOfAmount FROM `transaction` INNER JOIN (orderdetail INNER JOIN transactiondetail 
			ON orderdetail.orderdetailID = transactiondetail.orderdetailID) ON transaction.TransactionID = transactiondetail.TransactionID 
			WHERE (((transaction.TransactionTypeID)=1)) GROUP BY orderdetail.OrderID)  AS PaymentTotal RIGHT JOIN `order` ON PaymentTotal.OrderID = order.OrderID) 
			ON customer.customerID = order.customerID) ON educator.educatorID = order.educatorID) ON hospital.HospitalID = order.HospitalID) 
			INNER JOIN orderdetail ON order.OrderID = orderdetail.OrderID) ON product.ProductID = orderdetail.ProductID) ON shipmethod.ShipMethodID = order.ShipMethodID
			WHERE order.OrderID = '.$orderId;
		$generalInfoQuery = $this->db->query($sqlQuery);
		if ($generalInfoQuery && $generalInfoQuery->num_rows() > 0)
		{ 
			$generalInfoResult = $generalInfoQuery->result(); 
			$data['importantInfo'] = $importantInfo;
			$data['paymentMethods'] = $paymentMethods;
			$data['quoteInfo'] = $generalInfoResult[0];
			$data['quoteItems'] = $generalInfoResult; 
		    $html = $this->load->view('pdfGeneration/previewQuote', $data, TRUE); // render the view into HTML 
		    $this->load->library('pdf');
		    $pdf = $this->pdf->load(); 
		    $pdf->WriteHTML($html);    
		    if ($createFile)
		    {
		    	$pdf->Output('tempfiles/'.date('d-m-Y').'-'.$generalInfoResult[0]->OrderID.'-quote.pdf','F'); 
		    }
		    else 
		    {
		    	$pdf->Output('invoice.pdf','I'); 
		    } 
		}
		else 
		{
			echo "Cannot print out the pdf";
		}
	}


	/**
	 * Generates a pdf of an invoice label for ship out screen
	 * @param  [type]  $orderId    [description]
	 * @param  boolean $createFile [description]
	 * @return [type]              [description]
	 */
	public function previewInvoiceLabel($orderId = null, $createFile = false) {
		if ($orderId) {
			$orderId = strip_tags($orderId);
	 

			/*
				Get payment information
			*/

			$paymentMethods = $this->getPaymentMethods($orderId);
	 
			/*
			*	Get important information
			*/
			$importantInfo = $this->getImportantInformation($orderId);

	 		$sqlQuery = 'SELECT order.OrderID, customer.Firstname, customer.LastName, order.BillingAddress, order.BillingSuburb, order.BillingState, 
	 			order.BusinessPhone,
				order.BillingPostcode, order.HomePhone, order.MobilePhone, order.BusinessPhone, CONCAT(educator.Firstname, " ", educator.Lastname) AS Educator, 
				order.CustomerReference, orderdetail.Quantity, product.ProductCode, product.ProductName,
				CONCAT(ProductName, "(", Right((SELECT Barcode FROM stockmovedetail WHERE OrderDetailID=orderdetail.OrderDetailID LIMIT 1),14), ")") AS ProductDetail, 
				orderdetail.UnitPrice, Round(orderdetail.Quantity * orderdetail.UnitPrice,2) AS TotalAmount, orderdetail.GST_Amount, 
				SumOfAmount AS PaymentTotal, 
				order.orderDate, IF(order.HomePhone IS NULL,order.MobilePhone,order.HomePhone) AS CustomerTel, hospital.HospitalName, 
				product.Hire AS HasHire, shipmethod.ShipMethod, customer.BusinessName, order.InvoiceDate, 
				orderdetail.OrderDetailID FROM shipmethod RIGHT JOIN (product INNER JOIN ((hospital RIGHT JOIN (educator RIGHT JOIN (customer INNER JOIN 
				((SELECT orderdetail.OrderID, Sum(transactiondetail.Amount) AS SumOfAmount FROM `transaction` INNER JOIN (orderdetail INNER JOIN transactiondetail 
				ON orderdetail.orderdetailID = transactiondetail.orderdetailID) ON transaction.TransactionID = transactiondetail.TransactionID 
				WHERE (((transaction.TransactionTypeID)=1)) GROUP BY orderdetail.OrderID)  AS PaymentTotal RIGHT JOIN `order` ON PaymentTotal.OrderID = order.OrderID) 
				ON customer.customerID = order.customerID) ON educator.educatorID = order.educatorID) ON hospital.HospitalID = order.HospitalID) 
				INNER JOIN orderdetail ON order.OrderID = orderdetail.OrderID) ON product.ProductID = orderdetail.ProductID) ON shipmethod.ShipMethodID = order.ShipMethodID
				WHERE order.OrderID = '.$orderId;
			$generalInfoQuery = $this->db->query($sqlQuery);
			if ($generalInfoQuery && $generalInfoQuery->num_rows() > 0)
			{ 
				$generalInfoResult = $generalInfoQuery->result(); 
				$data['importantInfo'] = $importantInfo;
				$data['paymentMethods'] = $paymentMethods;
				$data['invoiceInfo'] = $generalInfoResult[0];
				$data['invoiceItems'] = $generalInfoResult; 
			    $html = $this->load->view('pdfGeneration/shipoutInvoiceLabel', $data,TRUE); // render the view into HTML 
			    $this->load->library('pdf');
			    $pdf = $this->pdf->load(); 
			    $pdf->WriteHTML($html);  
			    if ($createFile)
			    {
			    	$fileName = date('d-m-Y').'-'.$generalInfoResult[0]->OrderID.'-invoice.pdf';
			    	$pdf->Output('tempfiles/'.$fileName,'F');
			    	return $fileName; 
			    }
			    else 
			    {
			    	$pdf->Output('invoice.pdf','I'); 
			    } 
					return null;
			} 
		}
		echo "Cannot print out the invoice";
		return null;
	}

	/**
	 * Generates a .pdf of a shipping label
	 * @param  [type]  $orderId    [description]
	 * @param  boolean $createFile [description]
	 * @return [type]              [description]
	 */
	public function previewShippingLabel($orderId = null, $createFile = false) { 
		if ($orderId) {
			$sqlString = "SELECT order.OrderID, order.ShippingAddress, order.ShippingSuburb,
				order.ShippingState, order.ShippingPostcode, order.ShippingCountry,
				order.CustomerReference, customer.Firstname, customer.LastName, 
				customer.HomePhone, customer.MobilePhone, customer.BusinessName FROM `order`
				INNER JOIN customer ON order.CustomerID = customer.CustomerID WHERE order.OrderID = ".$orderId;   
			if ($q = $this->db->query($sqlString)) {
				echo 'Here';
				$r = $q->result();
				$data['data'] = $r[0];
		    $html = $this->load->view('pdfGeneration/shipoutShippingLabel', $data, TRUE); // render the view into HTML 
		    $this->load->library('pdf');
		    $pdf = $this->pdf->load(); 
		    $pdf->AddPage('L');
		    $pdf->WriteHTML($html);    
		    if ($createFile)
		    {
		    	$pdf->Output('tempfiles/'.date('d-m-Y').'-'.$generalInfoResult[0]->OrderID.'-shippinglabel.pdf','F'); 
		    }
		    else 
		    {
		    	$pdf->Output('shippinglabel.pdf','I'); 
		    }  
		    return;
			} else {
					echo 'Could not print the shipping label.';
			    echo $this->db->_error_message(); 
			    echo $this->db->_error_number();  
			}
		}
		echo 'Could not print the shipping label.';
		return;
	}

	/**
	 * Generates a .pdf of a delivery docket
	 * @param  [type]  $orderId    [description]
	 * @param  boolean $createFile [description]
	 * @return [type]              [description]
	 */
	public function previewDeliveryDocket($orderId = null, $createFile = false) {
		if ($orderId) {
			$sqlGeneralString = 'SELECT order.OrderID, customer.Firstname, customer.LastName, customer.BusinessName, 
				order.ShippingAddress, order.ShippingSuburb, order.ShippingState, order.ShippingPostcode, order.ShippingCountry,
				order.RequiredDate, order.CustomerReference FROM `order` INNER JOIN `customer` ON `order`.CustomerID = `customer`.CustomerID
				WHERE `order`.OrderID = '.$orderId;
				echo $sqlGeneralString.'<br/>';
			$sqlDetailsString = 'SELECT CONCAT(ProductName, "(" , RIGHT(productbarcode.Barcode, 12),")") AS ProductDetail, product.ProductCode,
				productbarcode.Barcode, stockmovedetail.Quantity, stockmove.Comments FROM stockmove INNER JOIN `order`
				ON order.OrderID = stockmove.ToOrderID 
					INNER JOIN 
						(product INNER JOIN 
							(productbarcode RIGHT JOIN stockmovedetail ON productbarcode.Barcode = stockmovedetail.Barcode)
							ON product.ProductID = stockmovedetail.ProductID)
					ON stockmove.StockMoveID = stockmovedetail.StockMoveID WHERE `order`.OrderID = '.$orderId; 
				echo $sqlDetailsString.'<br/>';
			$qGeneral = $this->db->query($sqlGeneralString);
			$qDetails = $this->db->query($sqlDetailsString);
			if ($qGeneral && $qDetails) {
				$rGeneral = $qGeneral->result();
				$rDetails = $qDetails->result(); 
				$data['general'] = $rGeneral[0];
				$data['details'] = $rDetails;
				$html = $this->load->view('pdfGeneration/shipoutDeliveryDocket', $data, TRUE); // render the view into HTML 
		    $this->load->library('pdf');
		    $pdf = $this->pdf->load();  
		    $pdf->WriteHTML($html);    
		    if ($createFile)
		    {
		    	$pdf->Output('tempfiles/'.date('d-m-Y').'-'.$generalInfoResult[0]->OrderID.'-deliverydocket.pdf','F'); 
		    }
		    else 
		    {
		    	$pdf->Output('deliverydocket.pdf','I'); 
		    }  
			}
		} 
		echo 'Could not print the delivery docket.';
    echo $this->db->_error_message(); 
    echo $this->db->_error_number(); 
		return;
	}

	public function previewRefundTaxInvoice($transactionId = null) {
		if ($transactionId) { 
			// qryDepositRefundAmount
			// qryOrderPickupLocation 
      $queryString = 'SELECT qryDepositRefundAmount.TransactionID, `order`.OrderID, customer.Firstname, customer.LastName, 
          `order`.BillingAddress, `order`.BillingSuburb, `order`.BillingState, `order`.BillingPostcode, `order`.HomePhone, 
          `order`.MobilePhone, CONCAT(educator.Firstname, " ", educator.Lastname) AS Educator, 
          `order`.CustomerReference, product.ProductCode, CONCAT(ProductName, "(", Right(ProductBarcode.Barcode,12), ")") AS ProductDetail, 
          `order`.OrderDate, IF(`order`.HomePhone IS NULL,`order`.MobilePhone,`order`.MobilePhone) AS CustomerTel, hospital.HospitalName, 
          CONVERT(product.hire, UNSIGNED INTEGER) AS HasHire, Round(qryDepositRefundAmount.Amount,2) AS Amount, qryDepositRefundAmount.OrderDetailID, 
          qryOrderPickupLocation.LocationDescription AS PickupLocation, 
          qryDepositRefundAmount.GST_Amount, CONCAT(`order`.OrderID, " - ", qryDepositRefundAmount.TransactionID) AS RefundID, 
          qryDepositRefundAmount.PaymentMethod, qryDepositRefundAmount.TransactionDate AS InvoiceDate, qryDepositRefundAmount.Comments,
          `orderdetail`.OrderDetailID 
          FROM product INNER JOIN ((hospital RIGHT JOIN (educator RIGHT JOIN (customer INNER JOIN 
            (((SELECT `orderdetail`.OrderID, Sum(transactiondetail.Amount) AS SumOfAmount FROM orderdetail 
              INNER JOIN transactiondetail ON `orderdetail`.OrderDetailID=transactiondetail.OrderDetailID GROUP BY `orderdetail`.OrderID) 
                AS PaymentTotal RIGHT JOIN `order` ON PaymentTotal.OrderID = `order`.OrderID) LEFT JOIN qryOrderPickupLocation 
              ON `order`.OrderID = qryOrderPickupLocation.ToOrderID) ON customer.CustomerID = `order`.CustomerID) 
              ON educator.EducatorID = `order`.EducatorID) ON hospital.HospitalID = `order`.HospitalID) 
                INNER JOIN ((orderdetail LEFT JOIN productbarcode ON (`orderdetail`.OrderID = ProductBarcode.OrderID) 
                    AND (`orderdetail`.ProductID = ProductBarcode.ProductID)) INNER JOIN qryDepositRefundAmount 
                    ON `orderdetail`.OrderDetailID = qryDepositRefundAmount.OrderDetailID) ON `order`.OrderID = `orderdetail`.OrderID) 
                  ON product.ProductID = `orderdetail`.ProductID ORDER BY qryDepositRefundAmount.OrderDetailID WHERE TransactionID = '.$transactionId;
		} else {
			echo 'No transaction specified.';
		}
	}

}
 
 