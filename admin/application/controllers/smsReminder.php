<?php
class SmsReminder extends CI_Controller {


	function __construct(){
		 
		parent::__construct(); 
	}

	function sendReminders()
	{
		/*
			How about allow this only to the cronjob.
			Maybe, I will need to change the active record to core mysql functions.
			Plus, I need to add testing to this whole thing...
			
			Q1. What happens to failed sent SMSes??
		*/  
		if ($this->sendHireReminders() && $this->sendClassesReminders())
		{
			$reminderData['SMS_SentDate'] = date('Y-m-d H:i:s');	
			$this->db->update('config',$reminderData);	  
		}
	}


	private function sendClassesReminders()
	{
		$strClassReminderMessageTextQuery = $this->db->query("SELECT ClassReminderMessageText FROM config WHERE ConfigID = 1");
		$intClassReminderMinusDaysQuery = $this->db->query("SELECT ClassReminderMinusDays FROM config WHERE ConfigID = 1");

		if ($strClassReminderMessageTextQuery->num_rows() > 0 && $intClassReminderMinusDaysQuery->num_rows() > 0)
		{
			$strClassReminderMessageTextResult = $strClassReminderMessageTextQuery->result();
			$strClassReminderMessageText = $strClassReminderMessageTextResult[0]->ClassReminderMessageText;
			$intClassReminderMinusDaysResult = $intClassReminderMinusDaysQuery->result();
			$intClassReminderMinusDays = $intClassReminderMinusDaysResult[0]->ClassReminderMinusDays;

			$strSQL = "SELECT customer.MobilePhone, educatorclass.StartTime, educatorclass.ClassDate, educatorlocation.LocName, 
				educatorclasscustomer.EducatorClassCustomerID FROM (educatorlocation INNER JOIN (educatorclass INNER JOIN 
				(customer INNER JOIN educatorclasscustomer ON customer.CustomerID = educatorclasscustomer.CustomerID) 
				ON educatorclass.EduClassID = educatorclasscustomer.EduClassID) ON educatorlocation.EduLocationID = educatorclass.EduLocationID) 
				LEFT JOIN educatorclasscustomerremindersent ON 
				educatorclasscustomer.EducatorClassCustomerID = educatorclasscustomerremindersent.EducatorClassCustomerID  
				WHERE (customer.MobilePhone IS NOT Null) 
				AND (educatorclass.ClassDate = DATE_ADD(DATE(Now()), INTERVAL $intClassReminderMinusDays DAY)) 
				AND ((educatorclasscustomerremindersent.EducatorClassCustomerID) Is Null) 
				AND ((educatorclasscustomer.Cancelled)=False)";
            $classesToRemindQuery = $this->db->query($strSQL); 
            if ($classesToRemindQuery->num_rows() > 0)
            {
            	$classesToRemindResult = $classesToRemindQuery->result();
            	foreach ($classesToRemindResult as $classToRemind)
            	{ 
	                $strClassDate = date('d/m/Y', strtotime($classToRemind->ClassDate));

	                // For some reason, using date doesn't extract the time from the string properly.
	                $dateParts = explode(' ', $classToRemind->StartTime);
	                $timeString = '';
	                if (sizeof($dateParts) > 1)
	                {
	                	$timeParts = explode(':',$dateParts[1]);
	                	if (sizeof($timeParts) > 2)
	                	{
	                		$ampm = 'am';
	                		$h = (int)$timeParts[0];
	                		if ($h > 12)
	                		{
	                			$h = $h - 12;
	                			$ampm = 'pm';
	                		}
	                		$m = $timeParts[1];
	                		$timeString = $h.':'.$m.' '.$ampm;
	                	}
	                }
	                // $timeString = date('H:i a', strtotime($classToRemind->StartTime));
	                $strClassTime = $timeString;
					$finalMessage = sprintf($strClassReminderMessageText, $strClassDate, $strClassTime, $classToRemind->LocName);
	            	if ($this->sendSMSWithReplyToEmail($classToRemind->MobilePhone, $finalMessage))
	            	{
	            		$insertData['EducatorClassCustomerID'] = $classToRemind->EducatorClassCustomerID;
	            		$insertData['SentDateTime'] = date('Y-m-d H:i:s'); 
	            		print_r($insertData);
	            	}
	            }
	            return true;
            }
		}
	    return false;
	}

	private function sendHireReminders()
	{
		/* 
			Sending reminders about hires
		*/
		$durationOne = $this->db->query("SELECT * FROM reminder WHERE ReminderID = 1 LIMIT 1");
		$durationTwo = $this->db->query("SELECT * FROM reminder WHERE ReminderID = 2 LIMIT 1");
		if ($durationOne->num_rows() == 1 && $durationTwo->num_rows() == 1)
		{
			$durationOneResults = $durationOne->result();
			$durationTwoResults = $durationTwo->result();
			$intervalOne = $durationOneResults[0]->GraceDays;
			$intervalSecond = $durationTwoResults[0]->GraceDays; 
			$firstRemindersToSend = "SELECT orderdetail.OrderDetailID, customer.MobilePhone, order.OrderID, customer.Firstname,
				1 AS ReminderID, ReturnExpectedDate FROM (customer INNER JOIN `order` ON customer.CustomerID = order.CustomerID) 
				INNER JOIN (orderdetail LEFT JOIN remindersent ON orderdetail.OrderDetailID = ReminderSent.OrderDetailID) ON 
				order.OrderID = orderdetail.OrderID WHERE (customer.MobilePhone IS NOT NULL) AND (orderdetail.hire = true) AND 
				(orderdetail.ReturnActualDate IS NULL) AND (orderdetail.CancelReminderMessages = false) AND 
				(DATE_ADD(ReturnExpectedDate, INTERVAL 1 DAY) < Now()) AND (ReminderSent.ReminderSentID IS NULL)";

			$secondRemindersToSend = "SELECT orderdetail.OrderDetailID, customer.MobilePhone, order.OrderID, customer.Firstname, 
				2 AS ReminderID, remindersent.SentDateTime FROM (customer INNER JOIN `order` ON customer.CustomerID = order.CustomerID) 
				INNER JOIN (((SELECT ReminderSentID, OrderDetailID, ReminderID FROM remindersent WHERE ReminderID=2)  
				AS Reminder2Sent RIGHT JOIN orderdetail ON Reminder2Sent.OrderDetailID = orderdetail.OrderDetailID) 
				INNER JOIN remindersent ON orderdetail.OrderDetailID = remindersent.OrderDetailID) ON order.OrderID = orderdetail.OrderID
				WHERE (((customer.MobilePhone) Is Not Null) AND ((orderdetail.Hire)=True) AND ((orderdetail.ReturnActualDate) Is Null) 
				AND ((orderdetail.CancelReminderMessages)=False) AND 
				(DATE_ADD(remindersent.SentDateTime, INTERVAL ".$intervalSecond." DAY)<=Now()) AND 
				((remindersent.ReminderID)=1) AND ((Reminder2Sent.ReminderSentID) Is Null))"; 
			$strSQL = "SELECT T1.OrderDetailID, T1.MobilePhone, T1.OrderID, T1.Firstname, T1.ReminderID  
	                    FROM (".$firstRemindersToSend.") T1 
	                    UNION  
	                    SELECT T2.OrderDetailID, T2.MobilePhone, T2.OrderID, T2.Firstname, T2.ReminderID 
	                    FROM (".$secondRemindersToSend.") T2" ; 
	        $query = $this->db->query($strSQL); 
	        if ($query->num_rows() > 0)
	        {
	        	$toSendResult = $query->result();
	        	$remindersSent = $this->db->query("SELECT * FROM remindersent")->result(); 
	        	$msgOneQuery = $this->db->query("SELECT MessageText FROM reminder WHERE ReminderID = 1");
	        	$msgTwoQuery = $this->db->query("SELECT MessageText FROM reminder WHERE ReminderID = 2");
	        	if ($msgOneQuery->num_rows() > 0 && $msgTwoQuery->num_rows() > 0)
	        	{
	        		$msgOne = $msgOneQuery->result();
	        		$msgTwo = $msgTwoQuery->result();
		            $astrMessageText[1] = $msgOne[0]->MessageText; 
		            $astrMessageText[2] = $msgTwo[0]->MessageText;   
		        	foreach ($toSendResult as $toSend)
		        	{
						$finalMessage = sprintf($astrMessageText[$toSend->ReminderID], $toSend->Firstname, $toSend->OrderID);
		        		if ($this->sendSMSWithReplyToEmail($toSend->MobilePhone, $finalMessage))
		        		{ 
		        			$insertData['OrderDetailID'] = $toSend->OrderDetailID;
		        			$insertData['ReminderID'] = $toSend->ReminderID;
		        			$insertData['SentDateTime'] = date('Y-m-d H:i:s'); 
		        			print_r($insertData);
		        			echo "<br/>";
		        			//$this->db->insert('remindersent',$insertData);
		        			/*
							add to reminder sent table
		        			*/
		        		}
		        	}
	        		return true;
	        	}
	        }

	    }
	    return false;
	}

	private function sendSMSWithReplyToEmail($mobileNumber, $finalMessage)
	{
	    $strUsername = "TensAustralia";
	    $strPassword = "674429";
	    $strReplyEmailAddress = "info@tensaustralia.com";
	    $finalMessage = urlencode($finalMessage);
	    $strURL = "http://www.messagenet.com.au/dotnet/lodge.asmx/LodgeSMSMessageWithReply?Username=".$strUsername.
	    	"&Pwd=".$strPassword."&PhoneNumber=".$mobileNumber."&PhoneMessage=".$finalMessage."&ReplyType=EMAIL&ReplyPath="
	    	.$strReplyEmailAddress;
	    /* Script URL */
	    //$url = 'http://www.messagenet.com.au/dotnet/lodge.asmx/LodgeSMSMessageWithReply';
	    $url = "http://tensaustralia.com.au";

	    /* $_GET Parameters to Send */ 
	    $params = array(
	    	'Username' => $strUsername, 
	    	'Pwd' => $strPassword,
	    	'PhoneNumber' => $mobileNumber,
	    	'PhoneMessage' => $finalMessage,
	    	'ReplyType' => 'EMAIL',
	    	'ReplyPath' => $strReplyEmailAddress);
	    echo "<br/>";
 		print_r($params);
	}

}