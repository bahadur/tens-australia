<?php
class EndOfTheDay extends CI_Controller {
	
      function __construct(){ 
            parent::__construct();
            if (!$this->tank_auth->is_logged_in_as(array('staff', 'administrator')))
            { 
                  redirect(base_url().'auth/login/');
            }
      }
	public function index()
	{
            //***************************************************
            $data['title']='Search by Article ID';
            $baseUrl = base_url();
            $data ['highlightLink'] = $baseUrl . 'endoftheday' ;
            $data['expandEndOfTheDay'] = 'expandEndOfTheDay';
            $this->load->view('header',$data); //dynamic header view loaded
            $this->load->view('navbar',$data); //dynamic sidebar view loaded
            $this->load->view('endoftheday/endoftheday');
            $this->load->view('footer-no-js'); //footer view loaded,
	}

	public function saveMyob()
	{
            $this->load->library('projectdates');  
            $data['title']='';
            $this->load->view('header',$data); //dynamic header view loaded
            $this->load->view('navbar'); //dynamic sidebar view loaded
            $this->load->view('endoftheday/savetomyob');
            $this->load->view('footer-no-js'); //footer view loaded,

	}

}

?>