<?php
class api extends CI_Controller { 

  function __construct(){
     
    parent::__construct();  
    $this->load->library('httpresponses');
    header('Content-Type: application/json'); 
  } 

  function getStaffSelect() 
  { 
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator'))) { 
      $staffSelectQ = $this->db->query("SELECT staff.StaffID AS id, 
        CONCAT(LastName, \", \", Firstname) AS `text` FROM staff WHERE staff.Active=1 ORDER BY 
        CONCAT(LastName, \", \", Firstname)");  
      if ($staffSelectQ) {
        $this->httpresponses->respondJsonify($staffSelectQ->result(), 200);
      } else {
        log_message('error', $this->db->last_query());
        log_message('error', $this->db->_error_message());
        $this->httpresponses->respondJsonify(array('message'=>'Could not get staff members'), 401);
      }
    } else {
      $this->httpresponses->respondJsonify(array('message'=>'Unauthorized'), 401);
    }
  }

}