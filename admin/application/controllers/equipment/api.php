<?php 

 class Api extends CI_Controller
 {
 	

 	function __construct()
 	{
 		parent::__construct(); 
 		$this->load->helper('ssp'); 
    $this->load->library('httpresponses'); 
 	}

 	public function barcode_location(){
		$query = $this->db->query("
			SELECT *  FROM productBarcode
			WHERE Barcode='".$this->input->post('barcode')."'
			AND LocationID='".$this->input->post('fromlocationid')."'");
		//echo  $query->num_rows();
		
		echo json_encode(array('status'=> ($query->num_rows()>0)?true:false));
		
	}


	public function barcodeExists(){
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator'))) { 
      $requestBody = json_decode(file_get_contents('php://input'));  
      if (isset($requestBody->Barcode)) {  
        $query = $this->db->query('
          SELECT *  FROM productBarcode
          WHERE Barcode="'.$requestBody->Barcode.'"');
        if ($query) { 
          if ($query->num_rows() == 0) {
            $this->httpresponses->respondJsonify(array('success' => true), 200);
          } else {
            $this->httpresponses->respondJsonify(array('message' => 'Barcode already exists.'), 400);
          }
        } else { 
          log_message('error', 'Barcode exists:');
          log_message('error', $this->db->last_query());
          log_message('error', $this->db->_error_message());
          $this->httpresponses->respondJsonify(array('message' => 'Db error.'), 400);
        }  
      } else {
          $this->httpresponses->respondJsonify(array('message' => 'Invalid parameters.'), 400);
      }
    } else {
      $this->httpresponses->respondJsonify(array('message' => 'Unauthorized.'), 401);
    }
	}
	


	function move_equipment(){
		if($this->input->post()){
			$date = $this->input->post('move_date');
			 $my_date = date('Y-m-d', strtotime($date));
			 
			$data = array('StockMoveTypeID' => 4,
							'MoveDate' =>  $my_date,
							'FromLocationID' => $this->input->post('from_location'),
							'ToLocationID' => $this->input->post('to_location'),
							'Comments' => $this->input->post('comments')

						);

			
			if($this->db->insert('stockmove',$data)){
			$newstocks_id = $this->db->insert_id();	

			foreach($this->input->post('products') as $product){

				$this->db->insert('stockmovedetail',array('StockMoveID' => $newstocks_id,
									'ProductID' => $product['productid'],
									'Barcode' 	=> $product['barcode'],
									'Quantity' 	=> $product['qty']));
					

			}
			// decrement qty from
			$query = $this->db->query("
					UPDATE stockMove INNER JOIN (stockMoveDetail INNER JOIN productLocation ON stockMoveDetail.ProductID = productLocation.ProductID) ON (stockMove.FromLocationID = productLocation.LocationID) AND (stockMove.StockMoveID = stockMoveDetail.StockMoveID) 
                    SET productLocation.Quantity = productLocation.quantity-stockMoveDetail.quantity 
                    WHERE stockMoveDetail.StockMoveID=".$newstocks_id);


			// update the barcode records locations
            $query = $this->db->query("
            		UPDATE stockMove INNER JOIN (productBarcode INNER JOIN stockMoveDetail ON productBarcode.Barcode = stockMoveDetail.Barcode) ON stockMove.StockMoveID = stockMoveDetail.StockMoveID
                    SET productBarcode.LocationID = stockMove.ToLocationID 
                    WHERE stockMove.StockMoveID=".$newstocks_id);



            // increment qty to
        	$query = $this->db->query("
        		UPDATE stockMove INNER JOIN (stockMoveDetail INNER JOIN productLocation ON stockMoveDetail.ProductID = productLocation.ProductID) ON (stockMove.ToLocationID = productLocation.LocationID) AND (stockMove.StockMoveID = stockMoveDetail.StockMoveID)
                SET productLocation.Quantity = productLocation.quantity+stockMoveDetail.quantity
                WHERE stockMoveDetail.StockMoveID=".$newstocks_id);
        
   
        	// create product location if required
        	$query = $this->db->query("
        		INSERT INTO productLocation ( ProductID, LocationID, Quantity )
                SELECT stockMoveDetail.ProductID, ".$this->input->post('to_location'). " AS ToLocationID, stockMoveDetail.Quantity
                FROM stockMoveDetail LEFT JOIN (SELECT productLocation.ProductLocationID, productLocation.ProductID FROM productLocation WHERE locationID = " .$this->input->post('to_location'). " ) AS filteredProductLocation ON stockMoveDetail.ProductID = filteredProductLocation.ProductID
                WHERE (((filteredProductLocation.ProductLocationID) Is Null) AND ((stockMoveDetail.StockMoveID)=".$newstocks_id.") AND (Barcode=0))");
        

			echo json_encode(array('status'=>1));
			} 
			 else {
			 	echo json_encode(array('status'=>0));
			 }
			
		}	
	}



 } ?>