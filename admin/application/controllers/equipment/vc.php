<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class vc extends CI_Controller{
	
	function __construct (){
		
		parent::__construct();

        if (!$this->tank_auth->is_logged_in_as(array('staff', 'administrator')))
        {  
      	  redirect(base_url().'auth/login/');
      	  return;
	    }
		$this->load->model('swapProductModel');
		$this->load->helper('date');

		date_default_timezone_set ('Australia/Sydney');
	}
	
	function index(){
		redirect('maintainEquipment/equipment_move_location');
	}

	function equipment_move_location(){ 
		
		$data['title']='Equipment Move Location'; 
		$data['expandMaintainEquip'] = "ExpandMaintainEquip";
		$this->load->view('header',$data); //dynamic header view loaded
    $this->load->view('navbar',$data); //dynamic sidebar view loaded
    $this->load->view('equipment/move_location'); //home page contents loaded, however we can provide contents of any page here to be loaded here
    $this->load->view('footer'); //footer view loaded,


	}


	//function for fetching information of SwapProductFrom and SwapProductTo 
	public function swapProduct()
	{
		//$data = $array();
		$baseUrl = base_url() ;
		$data['highlightLink'] = $baseUrl . "equipment/vc/swapProduct";
		$data['expandMaintainEquip'] = "ExpandMaintainEquip";
		$data['ProductCode'] = "";
		$data['ProductID'] = "";
		$data['SwapFromProductID'] = "";
		$data['SwapToProductID'] = "";
		$data['ProductBarCode']="";
		$data['no_result'] = "";
		$data['swapProductSearhResult'] = "";
		$data['title'] = 'Swap Product';
		$ProductBarCode = $this->input->post('ProductBarCode');
		
		if ($ProductBarCode) {
			$result = $this->swapProductModel->swapProductFrom($ProductBarCode);
			if ( sizeof ($result ) > 0 ) {
				$data['ProductBarCode'] = $ProductBarCode;
				$data['swapProuctFromProductCode'] =  $result['swapProuctFromResult'][0]->ProductCode;
				$data['swapProuctFromProductName'] = $result['swapProuctFromResult'][0]->ProductName;
				$data['swapProuctFromProductCategory'] = $result['swapProuctFromResult'][0]->ProductCategory;
				$data['swapProuctFromHasBarcode'] = $result['swapProuctFromResult'][0]->HasBarcode;
				$data['swapProuctFromActive'] = $result['swapProuctFromResult'][0]->Active;
				$data['swapProuctFromHire'] = $result['swapProuctFromResult'][0]->Hire;
				
				$data['SwapFromProductID'] = $result['swapProuctFromResult'][0]->SwapToProductID;			
				$data['ProductID'] = $result['swapProuctFromResult'][0]->ProductID;
						
				if ( $result['swapProductToResult'] != null) { 
							
					$data['swapProuctToProductCode'] = $result['swapProductToResult'][0]->ProductCode;
					$data['swapProuctToProductName'] = $result['swapProductToResult'][0]->ProductName;
					$data['swapProuctToProductCategory'] = $result['swapProductToResult'][0]->ProductCategory;
					$data['swapProuctToHasBarcode'] = $result['swapProductToResult'][0]->HasBarcode;
					$data['swapProuctToActive'] = $result['swapProductToResult'][0]->Active;
					$data['swapProuctToHire'] = $result['swapProductToResult'][0]->Hire;
					
					$data['SwapToProductID'] = $result['swapProductToResult'][0]->SwapToProductID;
					
					$data['no_result'] = "no";
				}else {
						
					$data['swapProuctToProductCode'] ='';
					$data['swapProuctToProductName'] = '';
					$data['swapProuctToProductCategory'] = '';
					$data['swapProuctToHasBarcode'] = '';
					$data['swapProuctToActive'] = '';
					$data['swapProuctToHire'] = '';
					
					$data['swapProductSearhResult'] = "no";
						
						 
						
				}
			}else {
					
				$data['no_result'] = "yes";
				$data['ProductCode'] = "";
				$data['swapProuctFromProductCode'] =  "";
				$data['swapProuctFromProductName'] =  "";
				$data['swapProuctFromProductCategory'] =  "";
				$data['swapProuctFromHasBarcode'] =  "";
				$data['swapProuctFromActive'] =  "";
				$data['swapProuctFromHire'] =  "";
				 
				$data['swapProuctToProductCode'] =  "";
				$data['swapProuctToProductName'] =  "";
				$data['swapProuctToProductCategory'] =  "";
				$data['swapProuctToHasBarcode'] =  "";
				$data['swapProuctToActive'] =  "";
				$data['swapProuctToHire'] =  "";
					
			}
		} else {
				    
				$data['ProductCode'] = "";
				$data['swapProuctFromProductCode'] =  "";
				$data['swapProuctFromProductName'] =  "";
				$data['swapProuctFromProductCategory'] =  "";
				$data['swapProuctFromHasBarcode'] =  "";
				$data['swapProuctFromActive'] =  "";
				$data['swapProuctFromHire'] =  "";
				 
				$data['swapProuctToProductCode'] =  "";
				$data['swapProuctToProductName'] =  "";
				$data['swapProuctToProductCategory'] =  "";
				$data['swapProuctToHasBarcode'] =  "";
				$data['swapProuctToActive'] =  "";
				$data['swapProuctToHire'] =  "";
					 
			}
		$this->load->view('header',$data);
		$this->load->view('navbar',$data);
		$this->load->view('equipment/swapProduct',$data);
		$this->load->view('footer');
		
	}
	
	function SaveSwapInfos(){
		
		$date = date('y-m-d');
		$FromProductID = $this->input->post('SwapFromProductID');
		$ToProductID = $this->input->post('SwapToProductID');
		$ProductID = $this->input->post('productID');
		$ProductBarCode = $this->input->post('ProductBarCode');
		
		$result = $this->swapProductModel->swapProductInsert( $FromProductID,$ToProductID, $ProductID,$ProductBarCode,$date);
	}
	

	
	
	function purchaseOrder(){  
		$data = array(); 
		$data['title'] = "Purchase Order";
		
		$ToLocationSQL = "SELECT location.LocationID,location.LocationDescription 
							FROM location ORDER BY location.LocationDescription";
		$ToLocationSQLQuery = $this->db->query($ToLocationSQL);
		$ToLocationSQLResult = $ToLocationSQLQuery->result();
		$data['ToLocationResult'] = $ToLocationSQLResult;
		
		
		$ProductSelectSQl = "SELECT product.ProductID, product.ProductName, product.HasBarcodeID 
							FROM product ORDER BY product.ProductName";
		$ProductSelectSQlQuery = $this->db->query($ProductSelectSQl);
		$ProductResult = $ProductSelectSQlQuery->result();
		$data['ProductResult'] = $ProductResult;
		
		
		
		## Constant Variable Declaration
		$PURCHASE_NEW_STOCK = 1;
		$ORDER_FULFILMENT = 2;
		$RETURN_HIRE_EQUIPMENT = 3;
		$MOVE_STOCK = 4;
		
		## End of constant Variable Declaration
		
		/*Working on later
		$intCountNoProductSQL= "SELECT count(StockMoveDetailID) FROM stockmove,stockmovedetail 
				WHERE stockmovadetail.ProductID IS NULL AND stockmovedeatil.StockMoveID = stockmove.StockMoveID" 
		$intCountNoProductQuery = $this->db->query($intCountNoProductSQL);
		$intCountNoProductResult = $intCountNoProductQuery->result();
		*/
		
		$StockMoveTypeID = $PURCHASE_NEW_STOCK;
		#post data from Purchase Form
		
		$MoveDate = $this->input->post('MoveDate');
		$FromPurchaseOrderNo = $this->input->post('FormPurchaseOrderNo');
		$ToLocationID = $this->input->post('ToLocation');
		
		$Comments = $this->input->post('Comments');
		
		$Product = $this->input->post('product');
		$Barcode = $this->input->post('barcode');
		$Quantity = $this->input->post('quantity');
		
	
		
		if($FromPurchaseOrderNo > 0){
		
				$InsertIntoStockMoveSQL = "INSERT INTO stockmove(StockMoveTypeID,MoveDate,FromPurchaseOrderNo,ToLocationID,Comments)
											VALUES('$StockMoveTypeID','$MoveDate','$FromPurchaseOrderNo','$ToLocationID','$Comments')";
				$InsertIntoStockMoveQuery = $this->db->query($InsertIntoStockMoveSQL);
				 $stockMoveID  = $this->db->insert_id();	
				
				
			
				
				$count = sizeof($Product); 
				
				for ($i=0;$i<$count;$i++){
					
					$product_id =$Product[$i]; 
					$barcode =$Barcode[$i]; 
					$quantity =$Quantity[$i]; 
						
					$InsertIntoStockMoveDetailSQL = "INSERT INTO stockmovedetail (StockMoveID,ProductID,Barcode,Quantity ) 
														VALUES('$stockMoveID','$product_id','$barcode','$quantity')";
					$InsertIntoStockMoveDeatailQuery = $this->db->query($InsertIntoStockMoveDetailSQL);	
							
				}
					
				If ($StockMoveTypeID == $MOVE_STOCK){
							
					$UpdateStockMoveSQL = "UPDATE stockmove INNER JOIN (stockmovedetail INNER JOIN productlocation ON stockmovedetail.ProductID = productlocation.ProductID) 
								ON (stockmove.FromLocationID = productlocation.LocationID) AND (stockmove.StockMoveID = stockmovedetail.StockMoveID) 
								SET productlocation.Quantity = productlocation.Quantity-stockmovedetail.Quantity 
									WHERE stockMovedetail.StockMoveID='$stockMoveID'";
									
					$UpdateStockMoveSQLQuery = $this->db->query($UpdateStockMoveSQL);
			   

					$UpdateBarcodeSQL = "UPDATE stockMove INNER JOIN (productbarcode INNER JOIN stockmovedetail ON productbarcode.Barcode = stockmovedetail.Barcode) 
											ON stockmove.StockMoveID = stockmovedetail.StockMoveID 
											SET productbarcode.LocationID = stockmove.ToLocationID 
											WHERE stockmove.StockMoveID='$stockMoveID' ";
					$UpdateBarcodeSQLQuery =$this->db->query($UpdateBarcodeSQL);		
						
				}else{
						/*PURCHASE_NEW_STOCK when StockMoveTypeId is 1 */
						If($StockMoveTypeID == $PURCHASE_NEW_STOCK ){
							$SelectValueFromStockMoveDeatilSQL = "SELECT stockmovedetail.Barcode,stockmovedetail.ProductID,stockmove.ToLocationID 
																	FROM stockmove INNER JOIN stockmovedetail ON stockmove.StockMoveID = StockMoveDetail.StockMoveID 
																WHERE stockmove.StockMoveID = '$stockMoveID'";
							$SelectValueFromStockMoveDeatilQuery = $this->db->query($SelectValueFromStockMoveDeatilSQL);
							
							$SelectValueFromStockMoveDeatilQueryResult = $SelectValueFromStockMoveDeatilQuery->result();
							
							$BarcodeFromStockMoveDetail = $SelectValueFromStockMoveDeatilQueryResult[0]->Barcode; 
							$ProductIDFromStockMoveDetail = $SelectValueFromStockMoveDeatilQueryResult[0]->ProductID; 

							$LocationIDFromStockMoveDetail = $SelectValueFromStockMoveDeatilQueryResult[0]->ToLocationID; 
							$InsertIntoProductBarcodeSQL = "INSERT INTO productbarcode (Barcode, ProductID, LocationID)
																VALUES ('$BarcodeFromStockMoveDetail','$ProductIDFromStockMoveDetail','$LocationIDFromStockMoveDetail')";
							$InsertIntoProductBarcodeSQLQuery = $this->db->query($InsertIntoProductBarcodeSQL);								
						}else{
					   
							$UpdateStockMoveSQL = "UPDATE stockmove INNER JOIN (stockmovedetail INNER JOIN productlocation ON stockmovedetail.ProductID = productlocation.ProductID) 
									ON stockmove.ToLocationID = productlocation.LocationID AND stockmove.StockMoveID = stockmovedetail.StockMoveID
										SET productlocation.Quantity = productlocation.Quantity+ stockMoveDetail.Quantity
									WHERE stockmovedetail.StockMoveID= '$stockMoveID'";
									
							$UpdateStockMoveSQLQuery = $this->db->query($UpdateStockMoveSQL);
					   
							$SelectValueFromProductionLocationSQL = "SELECT StockMoveDetail.ProductID, '$ToLocationID' AS ToLocationID, StockMoveDetail.Quantity 
																			FROM StockMoveDetail LEFT JOIN (SELECT ProductLocation.ProductLocationID, ProductLocation.ProductID FROM ProductLocation WHERE LocationID = '$ToLocationID' ) AS FilteredProductLocation ON StockMoveDetail.ProductID = FilteredProductLocation.ProductID
																		WHERE FilteredProductLocation.ProductLocationID Is Null AND StockMoveDetail.StockMoveID = '$stockMoveID'";
							$SelectValueFromProductionLocationSQLQuery = $this->db->query($SelectValueFromProductionLocationSQL);
							$SelectValueFromProductionLocationSQLQueryResult = $SelectValueFromProductionLocationSQLQuery->result();
							
							$productIdFromProductionLocation = 	$SelectValueFromProductionLocationSQLQueryResult[0]->ProductID;
							$LocationIDFromProductionLocation = $SelectValueFromProductionLocationSQLQueryResult[0]->ToLocationID;
							$QuantityFromProductionLocation = 	$SelectValueFromProductionLocationSQLQueryResult[0]->Quantity;
							
							$InsertIntoProductLocationSQL  = "INSERT INTO ProductLocation ( ProductID, LocationID, Quantity )
															VALUES ('$productIdFromProductionLocation','$LocationIDFromProductionLocation','$QuantityFromProductionLocation')";
							$InsertIntoProductLocationSQLQuery = $this->db->query($InsertIntoProductLocationSQL); 	
										
						}
					
					}
				}
				$this->load->view('header',$data);
				$this->load->view('navbar');
				$this->load->view('equipment/purchaseOrder');
				$this->load->view('footer'); 
	
	}


	public function ViewLocationsStocks()
	{
		// Idea - get all locations, all products, all productbarcodes. Link them.
		$sqlString = 
		"(SELECT LocationDescription, ProductName, Quantity, NULL FROM productlocation 
			INNER JOIN location ON location.LocationID = productlocation.LocationID 
			INNER JOIN product ON productlocation.ProductID = product.ProductID) 
		UNION 
		(SELECT LocationDescription, ProductName, 1, Barcode FROM productbarcode 
			INNER JOIN location ON productbarcode.LocationID = location.LocationID 
			INNER JOIN product ON productbarcode.ProductID = product.ProductID)";
		$result = $this->db->query($sqlString)->result();
		$data['locationstocks'] = $result; 
		$data['expandMaintainEquip'] = "ExpandMaintainEquip";
		$data['title'] = "Stock Information";
		
		$baseUrl = base_url() ;
		$data['highlightLink'] = $baseUrl . "maintainEquipment/ViewLocationsStocks";
		$this->load->view('header',$data);
		$this->load->view('navbar',$data);
		$this->load->view('equipment/locationstock',$data);
		$this->load->view('footer');
	}
}


?>