<?php
class formapi extends CI_Controller { 

  function __construct(){
     
    parent::__construct();  
    $this->load->library('httpresponses'); 
    $this->load->library('Datatables'); 
  }
 

  /**
   * For datatables
   * @return [type] [description]
   */
  public function educatorsResultsDt($businessName) {  
    //print_r($_POST);
    $this->datatables 
      ->select('
        educator.BusinessName, 
        order.OrderID, 
        order.InvoiceDate, 
        CONCAT(customer.Firstname,\' \',customer.LastName) as \'CustomerName\', 
        hospital.HospitalName, 
        qryOrderListShippedStatus.ShippedStatus, 
        order.RequiredDate', false)
      
      ->from('hospital INNER JOIN (educator INNER JOIN (customer INNER JOIN (`order` 
              INNER JOIN qryOrderListShippedStatus ON order.OrderID = qryOrderListShippedStatus.OrderID) 
              ON customer.CustomerID = order.CustomerID) ON educator.EducatorID = order.EducatorID) 
              ON hospital.HospitalID = order.HospitalID')
      ->where('hospital.HospitalName',urldecode($businessName))
      ->where('educator.BusinessName  IS NOT Null',null);
      
    echo $this->datatables->generate();  
  }
}