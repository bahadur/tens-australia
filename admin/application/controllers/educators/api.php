<?php
class Api extends CI_Controller { 

  function __construct(){
     
    parent::__construct(); 
    $this->load->helper('ssp'); 
    $this->load->library('httpresponses');
    header('Content-Type: application/json'); 
  }

  function getEducatorsSelect()
  {

    $sqlString = "SELECT *, CONCAT( IF(BusinessName IS NULL, '', BusinessName), ' ', 
      IF(Firstname IS NULL, '', Firstname),' ', 
      IF(LastName IS NULL, '', LastName)) AS EducatorFullDetails FROM educator"; 
    $res = $this->getData($sqlString);
    if ($res)
    {
      $resultsArray = array();
      foreach ($res as $educator) 
      {
        $resultsArray[] = array(
          "id" => $educator->EducatorID,
          "text" => $educator->EducatorFullDetails
        );
      }
      echo json_encode($resultsArray);
      return;
    }
    echo "N/A";
  }

    private function getData($sqlString)
    {
      $sqlQuery = $this->db->query($sqlString);
      if ($sqlQuery->num_rows() > 0)
      {
        $results = $sqlQuery->result();
        return $results;
      }
      return null;
    }
}