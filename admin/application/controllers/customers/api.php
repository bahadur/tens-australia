<?php
class Api extends CI_Controller { 

  function __construct(){
     
    parent::__construct(); 
    $this->load->helper('ssp'); 
    $this->load->library('httpresponses');
    header('Content-Type: application/json'); 
  }
 
 
  function addCustomerNote() {
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator'))) {  
    } else {
      
    }
  }

  function updateCustomer(){
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator'))) {  
      $requestBody = json_decode(file_get_contents('php://input'));  
      if ($requestBody) {
        $customerId = null;
        $shippingSameAsBilling = false;
        foreach ($requestBody as $key => $value) {
          if ($key === 'CustomerID') {
            $customerId = $value;
          } if ($key === 'ShippingSameAsBilling') {
            $shippingSameAsBilling = $value;
          } else {
            $updateData[$key] = $value;
          }
        } 
        if ($customerId) {
          if ($shippingSameAsBilling == "1") {
            $updateData['ShippingSameAsBilling'] = true;
            $updateData['ShippingAddress'] = $updateData['BillingAddress'];
            $updateData['ShippingPostcode'] = $updateData['BillingPostcode'];
            $updateData['ShippingState'] = $updateData['BillingState'];
            $updateData['ShippingSuburb'] = $updateData['BillingSuburb'];
            $updateData['ShippingCountry'] = $updateData['BillingCountry'];
          } else { 
            $updateData['ShippingSameAsBilling'] = false;
          }
          $this->db->where('CustomerID', $customerId);
          if ($this->db->update('customer', $updateData)) {
            $this->httpresponses->respondJsonify(array('success' => true), 200);
          } else {
            log_message('error', $this->db->_error_message());
            $this->httpresponses->respondJsonify(array('message' => 'Could not update the customer'), 400);
          }
        }
      }
      return; 
    } else {
      $this->httpresponses->respondJsonify(array('message' => 'Unauthorized'), 401);
    }
  } 

  function updateCustomerContactDetails($dependantOrderId = null){
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator'))) {  
      $requestBody = json_decode(file_get_contents('php://input'));  
      if ($requestBody && $requestBody->CustomerID && $requestBody->BillingAddress && $requestBody->BillingPostcode && $requestBody->BillingState
          && $requestBody->BillingSuburb && $requestBody->BillingCountry && $requestBody->Email && $requestBody->HomePhone
          && $requestBody->MobilePhone && $requestBody->BusinessPhone && $requestBody->Fax) { 

          $updateData['BillingAddress'] = $requestBody->BillingAddress;
          $updateData['BillingPostcode'] = $requestBody->BillingPostcode;
          $updateData['BillingState'] = $requestBody->BillingState;
          $updateData['BillingSuburb'] = $requestBody->BillingSuburb;
          $updateData['BillingCountry'] = $requestBody->BillingCountry;
          $updateData['MobilePhone'] = $requestBody->MobilePhone;
          $updateData['BusinessPhone'] = $requestBody->BusinessPhone;
          $updateData['Fax'] = $requestBody->Fax;
          $updateData['HomePhone'] = $requestBody->HomePhone;
          $updateData['Email'] = $requestBody->Email;
          if ($requestBody->ShippingSameAsBilling == "1") {
            $updateData['ShippingSameAsBilling'] = true;
            $updateData['ShippingAddress'] = $updateData['BillingAddress'];
            $updateData['ShippingPostcode'] = $updateData['BillingPostcode'];
            $updateData['ShippingState'] = $updateData['BillingState'];
            $updateData['ShippingSuburb'] = $updateData['BillingSuburb'];
            $updateData['ShippingCountry'] = $updateData['BillingCountry'];
          } else { 
            $updateData['ShippingSameAsBilling'] = false;
            $updateData['ShippingAddress'] = $requestBody->ShippingAddress;
            $updateData['ShippingPostcode'] = $requestBody->ShippingPostcode;
            $updateData['ShippingState'] = $requestBody->ShippingState;
            $updateData['ShippingSuburb'] = $requestBody->ShippingSuburb;
            $updateData['ShippingCountry'] = $requestBody->ShippingCountry;
          }
          $updateData['Website'] = $requestBody->Website;
          $this->db->where('CustomerID', $requestBody->CustomerID);
          if ($this->db->update('customer',$updateData)) {
            if ($dependantOrderId) {
              unset($updateData['Website']);
              $this->db->where('OrderID', $dependantOrderId);
              if ($this->db->update('order',$updateData)) {
                $this->httpresponses->respondJsonify(array('success' => true, 'updated' => 'Order and customer'), 200);
              } else {
                $this->httpresponses->respondJsonify(array('message' => 'Could not update the order'), 400);
              }
            } else {
              $this->httpresponses->respondJsonify(array('success' => true, 'updated' => 'Customer'), 200);
            }
          } else {
            $this->httpresponses->respondJsonify(array('message' => 'Could not update the customer'), 400);
          }
      } else {
        $this->httpresponses->respondJsonify(array('message' => 'Invalid parameters'), 400);
      }
    }
  }


  function allcustomers()
  {
      echo json_encode($this->db->get("customer")->result_array());
  }
 

  function getCustomersSelect()
  { 
    $sqlString = "SELECT *, 
      CONCAT(
        IF(Firstname IS NULL, '', Firstname), ' ', 
        IF(LastName IS NULL, '', LastName), ' ', 
        IF(MobilePhone IS NULL, '', MobilePhone)) AS CustomerFullDetails FROM customer";
    if ($sqlQuery = $this->db->query($sqlString)) {
      if ($sqlQuery->num_rows() > 0) {
        $results = $sqlQuery->result();
        $resultsArray = array();
        foreach ($results as $customer) 
        {
          $resultsArray[] = array(
            "id" => $customer->CustomerID,
            "text" => $customer->CustomerFullDetails
          );
        }  
        $this->httpresponses->respondJsonify($resultsArray, 200);
        return;
      } 
    } 
    $this->httpresponses->respondJsonify('Nothing', 400);
  }
 

  function getCustomer() {
    $requestBody = file_get_contents("php://input"); 
    $jsonBody = json_decode($requestBody);
    if ($jsonBody && property_exists($jsonBody, 'customerId')) { 
      $customerId = $jsonBody->customerId;
      if ($customerId) {
        $this->db->where('CustomerID', $customerId); 
        if ($q = $this->db->get('customer'))
        {
          if ($q->num_rows() > 0) {
            $r = $q->result();
            if ($r[0]) { 
              $this->httpresponses->respondJsonify($r[0], 200);
              return; 
            }
          }
          $this->httpresponses->respondJsonify('Could not get customer', 400);
        } else {
          log_message('debug', $this->db->_error_code.' -- '.$this->db->_error_message());
          $this->httpresponses->respondJsonify('Could not find customer', 400);
        }
      } else {
          $this->httpresponses->respondJsonify('No customer id', 400);
      }
    } else {
      $this->httpresponses->respondJsonify('Could not get customer', 400);
    } 
  }
   

  public function getCustomers()
  {  
    $this->load->library('Datatables'); 
      $this->datatables->select("CustomerID, Identifiers, BusinessName,Firstname,LastName,BillingAddress,BillingSuburb,BillingState,
        BillingPostcode,BillingCountry,HomePhone,BusinessPhone,MobilePhone, CustomerID, CustomerID, Email");
      $this->datatables->from("customer"); 
    $this->output
    ->set_content_type('application/json')
    ->set_output($this->datatables->generate());   
  }   

}
 