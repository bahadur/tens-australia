<?php
class vc extends CI_Controller {

	function __construct(){ 
		parent::__construct();
    if (!$this->tank_auth->is_logged_in_as(array('staff', 'administrator')))
    {  
    	  redirect(base_url().'auth/login/');
    }
	}


	function index(){
		redirect('customer/find');
	}


	function find(){
		$data['title']='Customer'; 
		$this->load->view('header',$data);  
    $this->load->view('navbar');  
    $this->load->view('customer/find_customer');  
    $this->load->view('footer');   
	}

	function customerDetails($customerid="", $orderId=null){
		if (empty($customerid)){
			redirect("customer/find");
		} 
		$data['customerid'] = $customerid; 
		$this->db->where("CustomerID", $customerid);
		$this->db->join("customerproblemflag", "customer.CustomerProblemFlagID = customerproblemflag.CustomerProblemFlagID"); 
		$customerQuery = $this->db->get("customer"); 
		if ($customerQuery->num_rows() > 0) {
			$customer = $customerQuery->row();
			$data['customer'] = $customer;  
			$data['title'] = "Customer Detail"; 
      if ($orderId) {
        $data['dependantOrderId'] = $orderId;
      }
			$this->load->view('header',$data); 
      $this->load->view('navbar');  
      $this->load->view('customer/customer_detail');  
      $this->load->view('footer');  
    } else  {
    	echo "Could not find the customer";
    }

	}

	function newCustomer(){ 
		if($this->input->post()){ 
			$data = array(
				"CustomerTypeID" => $this->input->post("CustomerTypeID"),
			    "Firstname"  => $this->input->post("Firstname"),
			    "LastName" => $this->input->post("LastName"),
			    "BusinessName"	=> $this->input->post("BusinessName"),
			    "CustomerProblemFlagID"  	=> $this->input->post("CustomerProblemFlagID"),
			    "Comments" => $this->input->post("Comments"),
			    "Active" => 1,
          "ShippingSameAsBilling" => 1,
			    "PriceGroupID" => $this->input->post("PriceGroupID")
			); 
			$this->db->insert("customer",$data);
			$customerID = $this->db->insert_id(); 
			redirect("customers/vc/customerDetails/$customerID");
		} else {
      $data['customerData'] = $this->db->query('SELECT * FROM customertype ORDER BY CustomerType ASC')->result();  
      $data['priceGroups'] = $this->db->query('SELECT * FROM pricegroup ORDER BY PriceGroup ASC')->result(); 
      $data['customerproblemflags'] = $result = $this->db->get("customerproblemflag")->result() ;
			$data['title'] = "New Customer";
			$baseUrl = base_url() ; 
	    $data['defaultcustomerProblemFlag'] = "OK";
	    $data['defaultCustomerType'] = "Retail";
			$this->load->view('header',$data); 
	    $this->load->view('navbar'); 
	    $this->load->view('customer/new-customer');  
	    $this->load->view('footer');  
		}
	}
}

?>