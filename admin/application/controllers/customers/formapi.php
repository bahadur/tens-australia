<?php
class formapi extends CI_Controller { 

  function __construct(){
     
    parent::__construct();  
    $this->load->library('httpresponses'); 
    $this->load->library('Datatables'); 
  }


  function getCustomersSelect() {
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator'))) { 
      $q = $this->input->post('q'); 
      $customers = $this->db
        ->select('CustomerID AS id, 
          CONCAT(
            IF(BusinessName IS NULL, 
              IF(LastName IS NULL, "", LastName), BusinessName), ", ", 
            IF(Firstname IS NULL, "", Firstname), ", ", 
            IF(MobilePhone IS NULL, "", MobilePhone), ", ", 
            IF(BillingAddress IS NULL, "", BillingAddress), ", ", 
            IF(BillingSuburb IS NULL, "", BillingSuburb), ", ", 
            IF(BillingPostcode IS NULL, "", BillingPostcode))
          AS `text`, CustomerProblemFlagID', false)
        ->like('CONCAT(Firstname, \' \', LastName)', $q, false)
        ->from('customer');
      $q = $customers->get();
      if ($q) {
        $this->httpresponses->respondJsonify($q->result(), 200);
      } else {
        log_message('error', $this->db->last_query());
        log_message('error', $this->db->_error_message());
        $this->httpresponses->respondJsonify(array(array('id' => -1, 'text' => 'Error querying for customers')), 200);
      }
    } else {
      $this->httpresponses->respondJsonify(array('id' => -1, 'text' => 'Unauthorized'), 401);
    }
  }


  function customerClasses($customerId = null) {
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator'))) { 
      if ($customerId) { 
        $this->datatables
          ->select('educatorclasscustomer.EducatorClassCustomerID, educatorclasscustomer.EduClassID, 
                  educatorclasscustomer.CustomerID, educatorclasscustomer.NumberAttending, educatorclasscustomer.BookingDateTime, 
                  educatorclasscustomer.Cancelled, educatorclasscustomer.CancelledDateTime,
                  educatorclass.ClassDate, educatorclass.StartTime, educatorlocation.LocName, 
                  educatorlocation.LocSuburb, educator.BusinessName')
          ->from('educatorlocation INNER JOIN (educator INNER JOIN (educatorclass INNER JOIN educatorclasscustomer ON educatorclass.EduClassID = educatorclasscustomer.EduClassID) ON educator.EducatorID = educatorclass.EducatorID) ON educatorlocation.EduLocationID = educatorclass.EduLocationID')
          ->where('educatorclasscustomer.CustomerID = '.$customerId);
        $this->httpresponses->respondJson($this->datatables->generate(), 200);
      } else {
        $this->httpresponses->respondJsonify(array('message' => 'Invalid parameters'), 400);
      }
    } else {
      $this->httpresponses->respondJsonify(array('message' => 'Unauthorized'), 401);
    } 
  }

  function customerOrders($customerId = null) { 
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator'))) {  
      if ($customerId) {
        $this->datatables
        ->select('OrderID, OrderDate, InvoiceDate, CustomerReference')
        ->from('`order`')
        ->where('CustomerID = '.$customerId);
        $this->httpresponses->respondJson($this->datatables->generate(), 200);
      } else {
        $this->httpresponses->respondJsonify(array('message' => 'Invalid parameters'), 400);
      }
    } else {
      $this->httpresponses->respondJsonify(array('message' => 'Unauthorized'), 401);
    } 
  }

  function customerNotes($customerId = null) { 
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator'))) {  
      if ($customerId) {
        $this->datatables
        ->select('customernote.DateTime, Firstname, LastName, customerproblemflag.Flag, customernote.Note, customernote.OrderID, customernote.CustomerID')
        ->from('customerproblemflag RIGHT JOIN (customernote LEFT JOIN staff ON customernote.StaffID = staff.StaffID) 
        ON customerproblemflag.CustomerProblemFlagID = customernote.CustomerProblemFlagID')
        ->where('customernote.CustomerID = '.$customerId);
        $this->httpresponses->respondJson($this->datatables->generate(), 200);
      } else {
        $this->httpresponses->respondJsonify(array('message' => 'Invalid parameters'), 400);
      } 
    } else {
      $this->httpresponses->respondJsonify(array('message' => 'Unauthorized'), 401);
    } 
  }
}