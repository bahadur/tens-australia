<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Options extends CI_Controller{
	
	function __construct (){
		parent::__construct();
	}

	public function index(){

		redirect('options/configuration');
	}

	public function configuration(){

		
		if($this->input->post()){


			$upd_data = array(
				"DefaultPriceGroupID" 			=> $this->input->post("DefaultPriceGroupID"),
			    "GST_Percent" 					=> $this->input->post("GST_Percent"),
			    "DefaultPickLocationID" 		=> $this->input->post("DefaultPickLocationID"),
			    "DefaultHireReturnLocationID" 	=> $this->input->post("DefaultHireReturnLocationID"),
			    "SearchRightCharsInBarcode" 	=> $this->input->post("SearchRightCharsInBarcode"),
			    "ReturnExpectedDuePlusDays" 	=> $this->input->post("ReturnExpectedDuePlusDays"),
			    "SendSMS_Time" 					=> $this->input->post("SendSMS_Time"),
			    "RequiredWarningDueMinusDays" 	=> $this->input->post("RequiredWarningDueMinusDays"),
			    "InvoiceEmailSubject" 			=> $this->input->post("InvoiceEmailSubject"),
			    "AdminPassword" 				=> $this->input->post("AdminPassword"),
			    "InvoiceEmailBody" 				=> $this->input->post("InvoiceEmailBody")
			     );
			    

		$this->db->where("ConfigID",$this->input->post("ConfigID"));

		$this->db->update("config", $upd_data);



		redirect("options/configuration");
		
		}

		
		$data['title']='Configuration';
		
		$data['config'] = $this->db->get('config')->row();

		$this->load->view('header',$data); //dynamic header view loaded
        $this->load->view('navbar',$data); //dynamic sidebar view loaded
        $this->load->view('options/configuration'); //home page contents loaded, however we can provide contents of any page here to be loaded here
        $this->load->view('footer'); //footer view loaded,
	
	}
}