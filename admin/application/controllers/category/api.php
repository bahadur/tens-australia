<?php
class Api extends CI_Controller { 

  function __construct(){
     
    parent::__construct(); 
    $this->load->helper('ssp'); 
    $this->load->library('httpresponses');
    header('Content-Type: application/json'); 
  }
 
 
  function allcategories()
  { 
    $sqlString = "SELECT ProductCategoryID, ProductCategory FROM productcategory"; 
    $res = $this->getData($sqlString);
    if ($res)
    {
      $resultsArray = array();
      foreach ($res as $category) 
      {
        $resultsArray[] = array(
          "id" => $category->ProductCategoryID,
          "text" => $category->ProductCategory
        );
      }
      echo json_encode($resultsArray);
      return;
    }
    echo "N/A";
  }

  private function getData($sqlString)
  {
    $sqlQuery = $this->db->query($sqlString);
    if ($sqlQuery->num_rows() > 0)
    {
      $results = $sqlQuery->result();
      return $results;
    }
    return null;
  }
 

 }