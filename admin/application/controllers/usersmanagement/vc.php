<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class vc extends CI_Controller{

	function __construct (){
		
		parent::__construct();
        if (!$this->tank_auth->is_logged_in_as(array('administrator')))
        { 
              redirect(base_url().'auth/login/');
        }
	
	}

	public function index($addUserTab = false)
	{  
		if ($errors = $this->session->flashdata('form_submission_error')) { 
			$data['errors'] = json_decode($errors);
		} else {
			$data['errors'] = null;
		}
		$data['addusertab'] = $addUserTab; 
		$data['title'] = 'Manage users';
		$this->load-> view('header', $data);
		$this->load->view('navbar');
		$this->load->view('manageUsers/users',$data);
		$this->load->view('footer'); 
	}

	public function edituser(){
		if($username = $this->input->post("username") && $email = $this->input->post("email")){ 
			$data_users = array
			(
			    "username" 		=> $username,
			    "email" 		=> $email 
			);
			$this->db->where('id', $this->input->post("id")); 
			$this->db->update('users', $data_users); 

			$data_profile = array(

				"firstname" 	=> $this->input->post("firstname"),
			    "lastname" 		=> $this->input->post("lastname"), 
			);

			//"group" 		=> $this->input->post("group"),
			$this->db->where('user_id', $this->input->post("id")); 
			if ($this->db->update('user_profiles', $data_profile)) {
				redirect('usersmanagement'); 
			} else {
				echo $this->db->error_code();
				echo $this->db->error_message();
			}
		} else {
			echo 'Invalid parameters';
		}
	}
}