<?php
class formapi extends CI_Controller {

  function __construct() {

    parent::__construct(); 
    $this->load->library('Datatables');
    $this->load->library('httpresponses'); 
  }


  function getStockMoveHistory($orderId = null) {
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator')))
    {
      if ($orderId) {
        $this->datatables
          ->select('orderdetail.OrderID, stockmovetype.StockMoveType, stockmove.MoveDate, 
            FromLocation.LocationDescription AS `FromLocationDescription`, stockmove.FromOrderID, ToLocation.LocationDescription AS `To`, 
            stockmove.ToOrderID, stockmove.Comments, product.ProductName, product.HasBarcodeID, stockmovedetail.Barcode, stockmovedetail.Quantity')
          ->from('stockmovetype 
              INNER JOIN ((location AS ToLocation RIGHT JOIN (location AS FromLocation RIGHT JOIN stockmove 
                ON FromLocation.LocationID=stockmove.FromLocationID) ON ToLocation.LocationID=stockmove.ToLocationID) 
              INNER JOIN (product INNER JOIN (stockmovedetail INNER JOIN orderdetail 
                ON stockmovedetail.OrderDetailID=orderdetail.OrderDetailID) ON product.ProductID=stockmovedetail.ProductID) 
              ON stockmove.StockMoveID=stockmovedetail.StockMoveID) ON stockmovetype.StockMoveTypeID=stockmove.StockMoveTypeID')
          ->where('orderdetail.OrderID = '.$orderId)
          ->group_by('orderdetail.OrderID, stockmovetype.StockMoveType, stockmove.MoveDate, 
            FromLocation.LocationDescription, stockmove.FromOrderID, ToLocation.LocationDescription, 
            stockmove.ToOrderID, stockmove.Comments, product.ProductName, stockmovedetail.Barcode, 
            stockmovedetail.Quantity, stockmove.MoveBy'); 
          $res = $this->datatables->generate();
          if ($res) {
            $this->httpresponses->respondJson($this->datatables->generate(), 200);
          } else {
            log_message('error', $this->db->last_query());
            log_message('error', $this->db->_error_message());
            $this->httpresponses->respondJsonify(array('message' => 'Invalid parameters'), 400);
          }
      } else {
        $this->httpresponses->respondJsonify(array('message' => 'Invalid parameters'), 400);
      }
    } else {
      $this->httpresponses->respondJsonify(array('message' => 'Unauthorized'), 401);
    } 
  }

}
