<?php
class api extends CI_Controller { 

  function __construct(){
    parent::__construct(); 
    $this->load->helper('ssp'); 
    $this->load->library('httpresponses');
    $this->load->library('projectdates');
    header('Content-Type: application/json'); 
  }


  public function updateRefundLine() { 
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator'))) { 
      $requestParams = json_decode(file_get_contents("php://input")); 
      if ($requestParams->OrderDetailID && $requestParams->RefundQty) {
        $orderDetailId = $requestParams->OrderDetailID;   
        $refundQty = $requestParams->RefundQty;
        $query = $this->db->query("SELECT * FROM orderdetail WHERE orderdetail.OrderDetailID = ".$orderDetailId); 
        if ($query && $query->num_rows() > 0) {
          $res = $query->result();
          $orderDetail = $res[0];
          if ($refundQty == 'null') {
            $data['TempRefundQuantity'] = null;
            $data['TempTransactionAmount'] = null;
          } else {
            $data['TempRefundQuantity'] = $refundQty;
            $data['TempTransactionAmount'] = $refundQty * $orderDetail->UnitPrice; 
          }
          $this->db->where('OrderDetailID', $orderDetailId);
          $this->db->update('orderdetail', $data); 
          $this->httpresponses->respondJsonify('', 200); 
        } else { 
          $this->httpresponses->respondJsonify(array('message'=>'- Error updating a refund line'), 200); 
        }
      } else {
        $this->httpresponses->respondJsonify(array('message'=>'-- Invalid parameters'), 400); 
      }
    } else {
        $this->httpresponses->respondJsonify(array('message' => 'Unauthorized'), 401);
    }
  } 
  
  public function confirmRefund() { 
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator'))) { 
      $requestBody = file_get_contents("php://input"); 
      $jsonResponse = json_decode($requestBody);  
      if ($jsonResponse 
        && isset($jsonResponse->OrderID) && $jsonResponse->OrderID 
        && isset($jsonResponse->TransactionTotal) && $jsonResponse->TransactionTotal 
        && isset($jsonResponse->TransactionDate) && $jsonResponse->TransactionDate 
        && isset($jsonResponse->TransactionReference) && $jsonResponse->TransactionReference 
        && isset($jsonResponse->TerminalID) && $jsonResponse->TerminalID 
        && isset($jsonResponse->PaymentMethodID) && $jsonResponse->PaymentMethodID) { 
        $refundDate = $this->projectdates->parseToMySQLDate($jsonResponse->TransactionDate);
        $transactionData['OrderID'] = $jsonResponse->OrderID;
        $transactionData['TransactionDate'] =  $refundDate;
        $transactionData['TransactionTotal'] =  $jsonResponse->TransactionTotal;
        $transactionData['TransactionReference'] = $jsonResponse->TransactionReference;
        $transactionData['Comments'] = $jsonResponse->Comments;  
        $transactionData['PaymentMethodID'] = $jsonResponse->PaymentMethodID;
        $transactionData['TerminalID'] = $jsonResponse->TerminalID; 
        $this->db->trans_start();     
        $transactionData['TransactionCreateDateTime'] = $this->projectdates->mysqlNow();
        $transactionData['TransactionTypeID'] = 2; 
        if ($this->db->insert('transaction', $transactionData)) {  
          $intTransactionID = $this->db->insert_id();     
          $transDetailsQ = $this->db->query('SELECT '.$intTransactionID.' AS TransactionID, orderdetail.OrderDetailID, productaccountcode.AccountCode,
            (-1) * (orderdetail.TempRefundQuantity * orderdetail.UnitPrice) AS RefundSubtotal,
            IF(orderdetail.GST = 1, ((-1) * (orderdetail.TempRefundQuantity * orderdetail.UnitPrice)) * (1 - 1 / (1 + orderdetail.GST_Percent)), 0) AS GST_Amount  
            FROM orderdetail INNER JOIN productaccountcode ON orderdetail.ProductID = productaccountcode.ProductID 
            WHERE (((productaccountcode.TransactionTypeID)=2) AND (orderdetail.OrderID = '.$jsonResponse->OrderID.') 
              AND ((orderdetail.TempRefundQuantity)>0))'); 
          if ($transDetailsQ->num_rows() > 0) {  
            $batchData = array();
            $insertData = $transDetailsQ->result();   
            foreach ($insertData as $row) {
              array_push($batchData, array(
                  "TransactionID" => $intTransactionID, 
                  "OrderDetailID" => $row->OrderDetailID, 
                  "Amount" => $row->RefundSubtotal, 
                  "AccountCode" => $row->AccountCode, 
                  "GST_Amount" => $row->GST_Amount
                ));
            }  
            if ($this->db->insert_batch('transactiondetail', $batchData)) {  
                $updateData['orderdetail.TempTransactionAmount'] = null;
                $updateData['orderdetail.RefundProcessedDate'] = $refundDate;
                $this->db->where('transactiondetail.TransactionID', $intTransactionID);
                if ($this->db->update('orderdetail INNER JOIN transactiondetail ON orderdetail.OrderDetailID = transactiondetail.OrderDetailID', $updateData)) {   
                  $this->db->trans_complete(); 
                  $this->httpresponses->respondJsonify(array('success' => $intTransactionID), 200);
                  return;
              }
            }
          }
        }
        $this->db->trans_rollback();
        log_message('error', $this->db->last_query());
        log_message('error', $this->db->_error_message());
        $this->httpresponses->respondJsonify(array('message' => 'Could not refund.'), 400);
      } else {
          $this->httpresponses->respondJsonify(array('message' => 'Invalid parameters.'), 400);  
      }
    } else {
      $this->httpresponses->respondJsonify(array('message' => 'Unauthorized'), 401);
    }
    
  }
 
}
