<?php
class formapi extends CI_Controller { 

  function __construct(){
    parent::__construct(); 
    $this->load->helper('ssp'); 
    $this->load->library('httpresponses');
    $this->load->library('projectdates');
    $this->load->library('Datatables');
    header('Content-Type: application/json'); 
  }


  public function getRefundLines($orderId) { 
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator'))) {   
      if ($orderId) { 
        $this->datatables
          ->where('((orderdetail.RefundProcessedDate IS Null) AND (orderdetail.OrderID = '.$orderId.'))')
          ->select('orderdetail.OrderID, product.ProductName, OrderDetailID, orderdetail.UnitPrice,
            (SELECT Sum(qryfulfilmentlistqtyshipped.ShippedQuantity) AS Quantity FROM qryfulfilmentlistqtyshipped WHERE OrderDetailID = orderdetail.OrderDetailID) AS ShippedQty, (SELECT Sum(qryhirereturnqty.Quantity) AS Quantity FROM qryhirereturnqty WHERE OrderDetailID = orderdetail.OrderDetailID) AS ReturnQty, orderdetail.ReturnActualDate, orderdetail.TempRefundQuantity, orderdetail.UnitPrice, orderdetail.Hire, orderdetail.TempTransactionAmount, ShowRefundButton AS RefundDeposit', true)
          ->from('product')
          ->join('orderdetail', 'product.ProductID = orderdetail.ProductID');
        $res = $this->datatables->generate();
        if ($res) {
          $this->httpresponses->respondJson($res, 200);
        } else {
          $this->httpresponses->respondJsonify(array('message' => 'Could not get refund lines'), 401);
        }
      } else {
        $this->httpresponses->respondJsonify(array('message' => 'Invalid parameters'), 400);
      }
    } else {
      $this->httpresponses->respondJsonify(array('message' => 'Unauthorized'), 401);
    }
  }


  public function updateRefundLine() { 
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator'))) { 
      $refundQuantity = $this->input->post('value'); 
      if (($this->input->post('value')) && is_numeric($this->input->post('value')) && ($this->input->post('OrderDetailID'))) {
        $orderDetailId = $this->input->post('OrderDetailID');   
        $refundQty = $this->input->post('value');
        $query = $this->db->query("SELECT * FROM orderdetail WHERE orderdetail.OrderDetailID = ".$orderDetailId); 
        if ($query && $query->num_rows() > 0) {
          $res = $query->result();
          $orderDetail = $res[0];
          $data['TempRefundQuantity'] = $refundQty;
          $data['TempTransactionAmount'] = $refundQty * $orderDetail->UnitPrice; 
          $this->db->where('OrderDetailID', $orderDetailId);
          $this->db->update('orderdetail', $data); 
          $this->httpresponses->respondJsonify('', 200); 
        } else { 
          $this->httpresponses->respondJsonify(array('message'=>'- Error updating a refund line'), 200); 
        }
      } else {
        $this->httpresponses->respondJsonify(array('message'=>'-- Invalid parameters'), 400); 
      }
    } else {
        $this->httpresponses->respondJsonify(array('message' => 'Unauthorized'), 401);
    }
  } 
}  