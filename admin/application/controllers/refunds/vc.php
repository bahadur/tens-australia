<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class vc extends CI_Controller{

	function __construct (){ 
		parent::__construct();
    if (!$this->tank_auth->is_logged_in_as(array('staff', 'administrator'))) { 
      redirect(base_url().'auth/login/');
		}
	}

 	public function refund()
 	{ 
		$data['title']='Orders';
 
		$queryString = "SELECT order.OrderID, CONCAT(Firstname, \" \", Lastname, \" (\", BusinessName, \")\") AS Customer, 
			customer.Lastname, order.OrderDate, order.RequiredDate, order.DueDate, MoveDate AS FormatMoveDate, 
			ReturnActualDate AS FormatReturnActualDate FROM (customer INNER JOIN `order` ON customer.CustomerID = order.CustomerID) 
			INNER JOIN (orderdetail LEFT JOIN stockmove ON orderdetail.OrderID = stockmove.ToOrderID) ON order.OrderID = orderdetail.OrderID 
			GROUP BY CONCAT(order.OrderID, Firstname, \" \", Lastname, \" (\", BusinessName, \")\"), customer.Lastname, order.OrderDate, 
			order.RequiredDate, order.DueDate, MoveDate, ReturnActualDate, orderdetail.RefundProcessedDate, orderdetail.Hire, 
			stockmove.StockMoveTypeID HAVING (((ReturnActualDate) <> \"\") AND ((orderdetail.RefundProcessedDate) Is Null) 
			AND ((orderdetail.Hire)=False) AND ((stockmove.StockMoveTypeID)=2));";
		$query = $this->db->query($queryString);
		$refunds = $query->result(); 
		$data['refunds'] = $refunds;
		$this->load->view('header',$data);  
    $this->load->view('navbar'); 
    $this->load->view('orderrefunds/refund');  
    $this->load->view('footer-no-js'); 
 
 	}

  public function refundView($orderId)
  {  
  	$sqlQueryString = "SELECT order.OrderID, order.OrderDate, CONCAT(Firstname, \" \", LastName, \" (\", BusinessName, \")\") AS CustomerName, 
  		CONCAT(`order`.ShippingAddress, \", \", `order`.ShippingSuburb, \", \", `order`.ShippingState, \" \", `order`.ShippingPostcode, \", \", 
  			`order`.ShippingCountry) AS Address, 
  		`order`.HomePhone, `order`.MobilePhone, `order`.Email, `order`.InvoiceDate, `order`.DueDate, orderdetail.ReturnActualDate 
  		FROM (customer INNER JOIN `order` ON customer.CustomerID = `order`.CustomerID) INNER JOIN orderdetail ON 
  		`order`.OrderID = orderdetail.OrderID WHERE `order`.OrderID = ".$orderId."; ";
  	$query = $this->db->query($sqlQueryString); 
  	if ($query->num_rows() > 0) {  
  		  $results = $query->result();
      	$data['title'] = "Refund an order";
      	$data['orderData'] = $results[0];
      	$datediff = strtotime($data['orderData']->ReturnActualDate) - strtotime($data['orderData']->InvoiceDate);
      	if ($datediff != null) {
      		$dayDiff = ceil($datediff/(60*60*24));
        	$data['DayDiff'] = $dayDiff;  
      	} else { 
      		$data['DayDiff'] = 0;
      	} 
      	$productsQueryString = "SELECT orderdetail.OrderID, product.ProductName, OrderDetailID, orderdetail.UnitPrice,
      		(SELECT Sum(qryFulfilmentListQtyShipped.ShippedQuantity) AS Quantity FROM qryFulfilmentListQtyShipped WHERE OrderDetailID = orderdetail.OrderDetailID) AS ShippedQty, 
      		(SELECT Sum(qryHireReturnQty.Quantity) AS Quantity FROM qryHireReturnQty WHERE OrderDetailID = orderdetail.OrderDetailID) AS ReturnQty, 
      		orderdetail.ReturnActualDate, orderdetail.TempRefundQuantity, orderdetail.UnitPrice, orderdetail.Hire, orderdetail.TempTransactionAmount, 
      		IF(IFNULL(ShowRefundButton, 0) = 0 ,'','Refund') AS RefundDeposit FROM product 
      		INNER JOIN orderdetail ON product.ProductID = orderdetail.ProductID 
      		WHERE (((orderdetail.RefundProcessedDate) IS Null) AND (orderdetail.OrderID = ".$orderId.")) ORDER BY orderdetail.OrderDetailID";
      	$productsQuery = $this->db->query($productsQueryString);
      	if ($productsQuery) {
      		$data['productsLines'] = $productsQuery->result(); 
        	$this->load->view('header',$data);  
	        $this->load->view('navbar');  
	        $this->load->view('orderrefunds/orderRefundView'); 
	        $this->load->view('footer-no-js');  
	    }
    } 

  }
 
	
 	public function refundByOrderID()
 	{
		$data['title']='Refund by Order ID'; 
		$queryString = "SELECT order.OrderID, CONCAT(Firstname, \" \", Lastname, \" (\", BusinessName, \")\") AS Customer, 
			customer.Lastname, order.OrderDate, order.RequiredDate, order.DueDate, MoveDate AS FormatMoveDate, 
			ReturnActualDate AS FormatReturnActualDate FROM (customer INNER JOIN `order` ON customer.CustomerID = order.CustomerID) 
			INNER JOIN (orderdetail LEFT JOIN stockmove ON orderdetail.OrderID = stockmove.ToOrderID) ON order.OrderID = orderdetail.OrderID 
			GROUP BY CONCAT(order.OrderID, Firstname, \" \", Lastname, \" (\", BusinessName, \")\"), customer.Lastname, order.OrderDate, 
			order.RequiredDate, order.DueDate, MoveDate, ReturnActualDate, orderdetail.RefundProcessedDate, orderdetail.Hire, 
			stockmove.StockMoveTypeID HAVING (((ReturnActualDate) <> \"\") AND ((orderdetail.RefundProcessedDate) Is Null) 
			AND ((orderdetail.Hire)=False) AND ((stockmove.StockMoveTypeID)=2));";
		$query = $this->db->query($queryString);
		$refund = $query->result();

		$data["refund"] = $refund;
		
		$baseUrl = base_url() ;  
		$this->load->view('header',$data); 
    $this->load->view('navbar',$data);  
    $this->load->view('order/refund');  
    $this->load->view('footer-no-js');  
 	}

	public function refundOrder()
	{ 
		// 1. Create a transaction 
    $this->db->trans_start();  
		$orderId = $this->input->post('orderId');
		$paymentMethodId = explode('##', $this->input->post('paymentMethodTerminalId'))[0];
		$terminalId = explode('##', $this->input->post('paymentMethodTerminalId'))[1];
		$refundCreationDate = date("Y-m-d H:i:s");
		$transactionData['OrderID'] = $orderId;
		$transactionData['TransactionTypeID'] = 2;
		$transactionData['PaymentMethodID'] = $paymentMethodId;
		$transactionData['TransactionDate'] =  $this->input->post('transactionDate');
		$transactionData['TransactionTotal'] =  $this->input->post('transactionTotal');
		$transactionData['TransactionReference'] = $this->input->post('transactionReference');
		$transactionData['TransactionCreateDateTime'] = $refundCreationDate;
		$transactionData['TerminalID'] = $terminalId;
		$transactionData['Comments'] = $this->input->post('comments');  
		if ($this->db->insert('transaction', $transactionData))
		{ 
			// 2.  Get the id 
			$intTransactionID = $this->db->insert_id();
			// 3. Insert transaction details
			$selectDataSQL = "SELECT ".$intTransactionID." AS TransactionID, orderdetail.OrderDetailID, orderdetail.TempTransactionAmount, 
				productaccountcode.AccountCode,  
				(orderdetail.GST * orderdetail.TempTransactionAmount * (1 - 1 / (1 + orderdetail.GST_Percent))) AS GST_Amount 
				FROM orderdetail INNER JOIN productaccountcode ON orderdetail.ProductID = productaccountcode.ProductID 
				WHERE (((productaccountcode.TransactionTypeID)=2) AND (orderdetail.OrderID = ".$orderId.") 
				AND ((orderdetail.TempTransactionAmount)>0))";   
			$query = $this->db->query($selectDataSQL); 
			if ($query->num_rows() > 0)
			{  
				$batchData = array();
				$result = $query->result();  
				foreach ($result as $row)
				{
					array_push($batchData, array(
							"TransactionID" => $intTransactionID, 
							"OrderDetailID" => $row->OrderDetailID, 
							"Amount" => $row->TempTransactionAmount, 
							"AccountCode" => $row->AccountCode, 
							"GST_Amount" => $row->GST_Amount
						));
				}  
				if ($this->db->insert_batch('transactiondetail', $batchData))
				{ 
					// Update Refund Date to now and update Transaction Amounts to 0/null: 
		            $updateData['orderdetail.TempTransactionAmount'] = null;
		            $updateData['orderdetail.RefundProcessedDate'] = $refundCreationDate;
		            $this->db->where('transactiondetail.TransactionID', $intTransactionID);
		            if ($this->db->update('orderdetail INNER JOIN transactiondetail ON orderdetail.OrderDetailID = transactiondetail.OrderDetailID', $updateData))
		            {  
		            	echo '1';
						$this->db->trans_complete();
	        			redirect(base_url() . 'orders/refund');
						return;
		            }
				}
		    }
		}
		echo "Error: ".$this->db->_error_message(); 
		echo "Error: ".$this->db->_error_number(); 
    $this->db->trans_rollback();  
	}
}
?>