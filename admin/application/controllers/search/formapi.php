<?php
class formapi extends CI_Controller {

  function __construct() {

    parent::__construct(); 
    $this->load->library('Datatables');
    $this->load->library('httpresponses'); 
  }
 

  public function searchByLocation($locationId){

    if($this->input->is_ajax_request()){

      
      $this->datatables->select('
        productbarcode.LocationID, 
        product.ProductName, 
        productbarcode.Barcode, 
        qrySearchByLocationLastStockMove.MoveDate')
      
      ->from('product INNER JOIN 
              (locationtype RIGHT JOIN 
              (location INNER JOIN 
              (productbarcode INNER JOIN 
              qrySearchByLocationLastStockMove 
              ON productbarcode.Barcode = qrySearchByLocationLastStockMove.Barcode) 
              ON location.LocationID = productbarcode.LocationID) 
              ON locationtype.LocationTypeID = location.LocationTypeID) 
              ON product.ProductID = productbarcode.ProductID', false)
      
      ->where('location.LocationID',$locationId);

      echo $this->datatables->generate();
      
      


   }

  }


  public function searchByMobile(){

    $mobileNumber = $this->input->post('mobileNumber');
    //echo $mobileNumber; die;
    
      
      $this
        ->datatables
        ->select('
          CustomerID,
          Identifiers,
          BusinessName,
          CONCAT(customer.Firstname,\' \',customer.LastName) as \'CustomerName\', 
          MobilePhone,
          HomePhone,
          BusinessPhone,
          Email
          ', false)
      
        ->from('customer')
      
        ->where('MobilePhone LIKE \'%'.$mobileNumber.'%\'');
        echo $this->datatables->generate();
        //echo $this->datatables->last_query();
        
    

  }

  public function searchByArticleId(){

    
    $articleId = $this->input->post('articleId');
    $this
      ->datatables
      ->select('
          orderdetail.OrderID, 
          hospital.HospitalName, 
          product.ProductName, 
          orderdetail.Quantity, 
          stockmovedetail.Barcode, 
          orderdetail.UnitPrice, 
          orderdetail.GST, 
          stockmove.MoveDate, 
          CONCAT(customer.FirstName, \' \', customer.LastName, \' \', customer.BusinessName) as \'Customer_Name\' ',false)
      ->from('
          (productcategory INNER JOIN product ON productcategory.ProductCategoryID = product.ProductCategoryID) 
          INNER JOIN (((hospital INNER JOIN (customer INNER JOIN `order` ON customer.CustomerID = order.CustomerID) 
          ON hospital.HospitalID = order.HospitalID) INNER JOIN (orderdetail INNER JOIN stockmovedetail 
          ON orderdetail.OrderDetailID = stockmovedetail.OrderDetailID) ON order.OrderID = orderdetail.OrderID) 
          INNER JOIN stockmove ON (stockmove.StockMoveID = stockmovedetail.StockMoveID) 
          AND (order.OrderID = stockmove.ToOrderID)) ON (product.ProductID = stockmovedetail.ProductID) 
          AND (product.ProductID = orderdetail.ProductID)')
      ->where('Barcode LIKE \'%'.$articleId.'%\'');

      echo $this->datatables->generate();


    

  }

  public function byCustomerNotes(){

    

    $this
      ->datatables
      ->select('
        customernote.DateTime, 
        customernote.OrderID,
        CONCAT(customer.Firstname, \' \' , customer.LastName, \' \', customer.BusinessName) AS Customer, 
        CONCAT(staff.FirstName, \' \', staff.LastName) AS Staff, 
        customernote.Note',false)
      ->from('customer INNER JOIN (customernote INNER JOIN staff ON customernote.StaffID = staff.StaffID) ON customer.CustomerID = customernote.CustomerID');
      echo $this->datatables->generate();
    
    

  }
  

  public function searchByBooking()
  { 
     
        $this->datatables->select('
          order.RequiredDate, 
          order.InvoiceDate, 
          order.OrderID, 
          CONCAT(Firstname, \' \', LastName, \' (\', BusinessName, \') \') AS Customer, 
          hospital.HospitalName, 
          shipmethod.ShipMethod, 
          IF(SumofLineTotal = SumofAmount, \'Paid\',\'Unpaid\') AS \'Paid\', 
          qryorderlistshippedstatus.ShippedStatus, 
          order.MobilePhone, 
          order.BillingSuburb, 
          order.BillingPostcode, 
          customerproblemflag.Flag,
          IF(IFNULL((SELECT COUNT(*) FROM customernote WHERE order.OrderID = customernote.OrderID), 0) = 0, \'No\', \'Yes\') AS \'Notes\'', false)
          ->from('order')
          ->join('hospital','hospital.HospitalID = order.HospitalID','left')
          ->join('customer','customer.CustomerID = order.CustomerID','left')
          ->join('customerproblemflag',   'customer.CustomerProblemFlagID = customerproblemflag.CustomerProblemFlagID','left')
          ->join('shipmethod','shipmethod.ShipMethodID = order.ShipMethodID','left')
          ->join('qryfulfilmentlistorderpaid','order.OrderID = qryfulfilmentlistorderpaid.OrderID','left')
          ->join('qryorderlistshippedstatus', 'order.OrderID = qryorderlistshippedstatus.OrderID','left'); 
          echo $this->datatables->generate();
          

  }

  public function searchByBarcode($barcode){
    

    $this->datatables
    ->select('
      orderdetail.OrderID, 
      hospital.HospitalName, 
      product.ProductName, 
      orderdetail.Quantity, 
      stockmovedetail.Barcode, 
      orderdetail.UnitPrice, 
      orderdetail.GST, 
      stockmove.MoveDate, CONCAT(customer.FirstName, \' \', customer.LastName, \' \', customer.BusinessName) as \'Customer_Name\' ',false)
    ->from('
      (productcategory INNER JOIN product ON productcategory.ProductCategoryID = product.ProductCategoryID) 
      INNER JOIN (((hospital INNER JOIN (customer INNER JOIN `order` ON customer.CustomerID = order.CustomerID) 
      ON hospital.HospitalID = order.HospitalID) INNER JOIN (orderdetail INNER JOIN stockmovedetail 
      ON orderdetail.OrderDetailID = stockmovedetail.OrderDetailID) ON order.OrderID = orderdetail.OrderID) 
      INNER JOIN stockmove ON (stockmove.StockMoveID = stockmovedetail.StockMoveID) 
      AND (order.OrderID = stockmove.ToOrderID)) ON (product.ProductID = stockmovedetail.ProductID) 
      AND (product.ProductID = orderdetail.ProductID)')

    //->like('Barcode',$barcode);
    ->where('Barcode LIKE \'%'.$barcode.'%\'');
    echo $this->datatables->generate();

  }

  public function searchByCustomer(){
    
    $this->datatables
      ->select('
        CustomerID,
        BusinessName,
        Identifiers,
        Firstname,
        LastName, 
        MobilePhone,
        Email',false)
    ->from('customer');

    echo $this->datatables->generate();

          

  }
  
 
}


