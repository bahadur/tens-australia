<?php 

class vc extends CI_Controller {


      function __construct(){
            parent::__construct();
        if (!$this->tank_auth->is_logged_in_as(array('staff', 'administrator')))
        { 
            redirect(base_url().'auth/login/');
        }
      }

      public function index()
      { 
      }

      public function byArticleId()
      {  
            //***************************************************
            $data['title']='Search by Article ID';
            $baseUrl = base_url() ; 
            $this->load->view('header',$data);  
            $this->load->view('navbar'); 
            $this->load->view('search/searchByArticleId');
            $this->load->view('footer'); 
      }

      public function byBarcode()
      { 
            //***************************************************
            $data['title']='Search by Barcode';
           
            $baseUrl = base_url() ; 
            $this->load->view('header',$data);  
            $this->load->view('navbar'); 
            $this->load->view('search/searchByBarcode');
            $this->load->view('footer'); 
      }


      public function byMobileNumber()
      { 
            //***************************************************
            $data['title']='Search by Mobile Number';
            $baseUrl = base_url() ; 
            $this->load->view('header',$data); 
            $this->load->view('navbar'); 
            $this->load->view('search/searchByMobileNumber');
            $this->load->view('footer');
      }


      public function byOrderId()
      { 
            //***************************************************
            $data['title']='Search by Order Id';
            $baseUrl = base_url() ; 
            $this->load->view('header',$data);  
            $this->load->view('navbar'); 
            $this->load->view('search/searchByOrderId');
            $this->load->view('footer');
      }

      public function resultsByMobile()
      {  

            $mobileNumber  = $this->input->post('mobileNumber');
            $data['mobileNumber'] = $mobileNumber;
            $data['title']='Search by Mobile';
            $this->load->view('header',$data); 
            $this->load->view('navbar'); 
            $this->load->view('search/searchByMobile');
            $this->load->view('footer');
            
             
      }

      public function byCustomer()
      { 
            // $query = null;
            // if (isset($mobileNumber))
            // {
            //       $query = $this->db->query("SELECT * FROM customer WHERE customer.MobileNumber LIKE %" + $mobileNumber + "%"); 
            // }
            // else 
            // {
            //       $query = $this->db->query("SELECT * FROM customer");
            // }
            // if (!is_null($query) && $query->num_rows() > 0)
            // {
             //     $data['customers'] = $query->result();
                  $data['title']='Search by Customer'; 
                  $this->load->view('header',$data); 
                  $this->load->view('navbar');
                  $this->load->view('search/searchByCustomer');
                  $this->load->view('footer');
           // } 
      }


      public function byCustomerNotes()
      {  
            $data['title']='Search by Customer Notes'; 
            $this->load->view('header',$data); 
            $this->load->view('navbar'); 
            $this->load->view('search/searchByCustomerNotes');
            $this->load->view('footer');
      }


      public function byBookings()
      { 
            //***************************************************
            $data['title']='Search by Bookings';
            $baseUrl = base_url() ; 
            $this->load->view('header',$data);
            $this->load->view('navbar');
            $this->load->view('search/searchByBookings');
            $this->load->view('footer');
      }


      public function byEducator()
      { 
            $data['title']='Search by Educator';
            $baseUrl = base_url() ;
            $this->load->view('header',$data); 
            $this->load->view('navbar');
            $this->load->view('search/searchByEducator');
            $this->load->view('footer');
      }

      public function educatorsResults($educatorName, $educatorId)
      { 
            $educatorName = urldecode($educatorName);    
            $data['title']=''.$educatorName; 
            $this->load->view('header',$data);  
            $this->load->view('navbar');  
            $this->load->view('search/results/educatorsResults');
            $this->load->view('footer');  
      }

      public function byLocation($locationId = null)
      { 
            $locationId = $this->input->post('locationId');
            if ($locationId == null)
            {
                  $sqlQuery = $this->db->query("SELECT location.LocationID, location.LocationDescription 
                        FROM location ORDER BY location.LocationDescription");
                  if ($sqlQuery)
                  {
                        $data['title']= 'Search by location';
                        $data['locations']= $sqlQuery->result();
                        $baseUrl = base_url() ;
                        $data['highlightLink'] = $baseUrl.'searchOrders/byLocation'; 
                        $data ['expandSearch'] = 'expandSearch';
                        $this->load->view('header',$data); //dynamic header view loaded
                        $this->load->view('navbar',$data); //dynamic sidebar view loaded
                        $this->load->view('search/byLocation/chooseLocation');
                        $this->load->view('footer'); //footer view loaded,
                  }
            } else  {
                  $locationQuery = $this->db->query("SELECT CONCAT(
                              IF(location.Address IS NULL,\"\", location.Address), \", \", 
                              IF(location.Suburb IS NULL,\"\", location.Suburb), \", \",
                              IF(location.State IS NULL, \"\",location.State), \", \",
                              IF(location.Postcode IS NULL,\"\", location.Postcode), \", \",
                              IF(location.Country IS NULL,\"\", location.Country)) AS FullAddress,
                              location.LocationID,
                              locationtype.LocationType, location.LocationDescription, 
                              location.PickStock, location.HireReturns
                               FROM location INNER JOIN locationtype ON 
                        locationtype.LocationTypeID = location.LocationTypeID WHERE location.LocationID = ".$locationId);
                 
                  // $findLocationQuery = $this->db->query("SELECT productbarcode.LocationID, product.ProductName, productbarcode.Barcode, 
                  //       qrySearchByLocationLastStockMove.MoveDate 
                  //       FROM product INNER JOIN (locationtype RIGHT JOIN 
                  //             (location INNER JOIN (productbarcode 
                  //                   INNER JOIN qrySearchByLocationLastStockMove 
                  //                   ON productbarcode.Barcode = qrySearchByLocationLastStockMove.Barcode) 
                  //       ON location.LocationID = productbarcode.LocationID) 
                  //       ON locationtype.LocationTypeID = location.LocationTypeID) 
                  //       ON product.ProductID = productbarcode.ProductID WHERE location.LocationID=".$locationId);
                  //if ($findLocationQuery && $locationQuery)
                  if ($locationQuery)
                  {
                        $data['title']= 'Search by location';
                        //$data['productsAtLocation']= $findLocationQuery->result(); 
                        $loc = $locationQuery->result();
                        $data['location'] = $loc[0];
                        $this->load->view('header',$data); 
                        $this->load->view('navbar');  
                        $this->load->view('search/results/locationSearchResults');
                        $this->load->view('footer');  
                  }
            }
      }



      public function articleIdResults($articleId) {  
            


            // $query = $this->db->query("SELECT orderdetail.OrderID, hospital.HospitalName, product.ProductName, 
            //       orderdetail.Quantity, stockmovedetail.Barcode, orderdetail.UnitPrice, orderdetail.GST, 
            //       stockmove.MoveDate, CONCAT(customer.FirstName, \" \", customer.LastName, \" \", customer.BusinessName) as \"Customer_Name\" 
            //       FROM (productcategory INNER JOIN product ON productcategory.ProductCategoryID = product.ProductCategoryID) 
            //       INNER JOIN (((hospital INNER JOIN (customer INNER JOIN `order` ON customer.CustomerID = order.CustomerID) 
            //             ON hospital.HospitalID = order.HospitalID) INNER JOIN (orderdetail INNER JOIN stockmovedetail 
            //             ON orderdetail.OrderDetailID = stockmovedetail.OrderDetailID) ON order.OrderID = orderdetail.OrderID) 
            //                   INNER JOIN stockmove ON (stockmove.StockMoveID = stockmovedetail.StockMoveID) 
            //                   AND (order.OrderID = stockmove.ToOrderID)) ON (product.ProductID = stockmovedetail.ProductID) 
            //             AND (product.ProductID = orderdetail.ProductID) WHERE Barcode LIKE CONCAT('%',".$articleId.") ")->result();
            // $data['orders'] = $query; 
            $data['articleId'] = $articleId;
            
            $data['title']='Orders with Article ID '.$articleId;
            $this->load->view('header',$data); 
            $this->load->view('navbar'); 
            $this->load->view('search/results/articleIdResults');
            $this->load->view('footer');
      }

 
      public function barcodeResults($barcode) {  
            


            
            $data['title']='Orders with Barcode :'.$barcode;
            $data['barcode'] = $barcode;
            $this->load->view('header',$data); 
            $this->load->view('navbar'); 
            $this->load->view('search/results/barcodeResults');
            $this->load->view('footer');
      }
        
}


