<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//main controller: this controller load the main page of the project
class Main extends CI_Controller {

      
	public function index()
	{
            if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator')))
            {
                  redirect(base_url().'confirmorders/vc/index');
            }
            else 
            {
                  redirect(base_url().'auth/login/');
            }
	}
 
        
}
