<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Services extends CI_Controller{

	function __construct(){

		parent::__construct();
		if (!$this->tank_auth->is_logged_in_as(array('staff', 'administrator')))
        { 
              redirect(base_url().'auth/login/');
        }
		
	}

	function delSuiteSupportProgramList(){

		$data = array();
		$data['title'] = 'Delivery Suite Support Program List';

		$suiteSupportProgramListSQL = "SELECT orderdetail.OrderID, BusinessName,Firstname,Lastname AS Customer, 
										product.ProductName, orderdetail.TempBarcode, order.InvoiceDate, Now()-InvoiceDate AS Days, orderdetail.ReturnExpectedDate 
										FROM product INNER JOIN ((customer INNER JOIN `order` ON customer.CustomerID = order.CustomerID) 
											INNER JOIN orderdetail ON order.OrderID = orderdetail.OrderID) ON product.ProductID = orderdetail.ProductID 
										WHERE orderdetail.TempPickQuantity<>0 AND orderdetail.ProductID=68";
		$suiteSupportProgramListSQLQuery = $this->db->query($suiteSupportProgramListSQL);
		$suiteSupportProgramListSQlResult = $suiteSupportProgramListSQLQuery->result();
		
		//$data['suiteProductResult'] = $suiteSupportProgramListSQlResult;
		
		if(sizeof($suiteSupportProgramListSQlResult)>0){

			$data['suiteProductResult'] = $suiteSupportProgramListSQlResult;
			
		
		}
		
		else{

			$data['suiteProductResult'] = [];
		}
		
		$baseUrl = base_url();
		$data['highlightLink'] = $baseUrl . 'Services/delSuiteSupportProgramList';
		$data['expandServices'] = 'expandServices';
		$this->load->view('header',$data);

		$this->load->view('navbar',$data);
		$this->load->view('delSuiteProduct',$data);

		$this->load->view('navbar');
		$this->load->view('services/delSuiteProduct',$data);

		$this->load->view('footer');

	}

	function educatorEquipmentList(){

		$data = array();

		$data['title'] = 'Educator Equipment Program List ';

		$educatorEquipmentProgramListSQL = "SELECT orderdetail.OrderID, BusinessName,Firstname,Lastname AS Customer, product.ProductName, order.InvoiceDate, Now()-InvoiceDate AS Days, orderdetail.ReturnExpectedDate
												FROM product INNER JOIN ((customer INNER JOIN `order` ON customer.CustomerID = order.CustomerID) 
													INNER JOIN orderdetail ON order.OrderID = orderdetail.OrderID) ON product.ProductID = orderdetail.ProductID
											WHERE orderdetail.ProductID=66 AND orderdetail.TempPickQuantity<>0";

		$educatorEquipmentProgramListSQLQuery = $this->db->query($educatorEquipmentProgramListSQL);

		$educatorEquipmentProgramListSQLResult = $educatorEquipmentProgramListSQLQuery->result();

		if(sizeof($educatorEquipmentProgramListSQLResult)>0){

			$data['educatorEquipmentResult'] =  $educatorEquipmentProgramListSQLResult ;

		}else{

			$data ['educatorEquipmentResult'] = [];
		}
		$baseUrl = base_url();
		$data['highlightLink'] = $baseUrl . 'Services/educatorEquipmentList';
		$data['expandServices'] = 'expandServices';
		$this->load-> view('header', $data);

		$this->load->view('navbar',$data);
		$this->load->view('educatorEquipmentProgramList',$data);

		$this->load->view('navbar');
		$this->load->view('services/educatorEquipmentProgramList',$data);

		$this->load->view('footer');


	}

	function eduClassesList(){

		$data = array();

		$data['title'] = 'Available Class List';

		$eduClassesListSQL = "SELECT educatorclass.ClassDate, educatorclass.StartTime, educatorclass.Duration, educatorlocation.locName, educatorlocation.LocAddress, educatorlocation.LocSuburb, educatorlocation.Instructions, educatorclass.EduClassID, educatorclass.EduLocationID, educatorclass.EducatorID, 
									Firstname,Lastname AS Educator, educatorclass.HospitalID, hospital.HospitalName, educatorpatients.PatientsInvited
								FROM hospital INNER JOIN ((educatorlocation INNER JOIN educatorpatients ON educatorlocation.EducatorPatientsID = educatorpatients.EducatorPatientsID) INNER JOIN (educator INNER JOIN educatorclass ON educator.EducatorID = educatorclass.EducatorID) ON educatorlocation.EduLocationID = educatorclass.EduLocationID) ON hospital.HospitalID = educatorclass.HospitalID
							WHERE educatorclass.ClassDate>= (NOW()-INTERVAL 3 DAY) And educatorclass.ClassDate<=(NOW()+INTERVAL 90 DAY)
							ORDER BY educatorclass.ClassDate";
		$eduClassesListSQLQuery = $this->db->query($eduClassesListSQL);
		$eduClassesListSQLResult = $eduClassesListSQLQuery->result();

		if(sizeof($eduClassesListSQLResult)>0){

			$data['eduClassesResult'] = $eduClassesListSQLResult ;


		}else{

			$data['eduClassesResult'] = [];
		}



		$baseUrl = base_url();
		$data['highlightLink'] = $baseUrl. 'Services/eduClassesList';
		$data['expandServices'] = 'expandServices';
		$this->load->view('header',$data);

		$this->load->view('navbar',$data);
		
		$this->load->view('services/educlassesList',$data);

		$this->load->view('footer');


	}


}


?>