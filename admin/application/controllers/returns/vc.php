<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class vc extends CI_Controller{

	function __construct (){
		
		parent::__construct();
		$this->load->helper('date'); 
    if (!$this->tank_auth->is_logged_in_as(array('staff', 'administrator'))) { 
      redirect(base_url().'auth/login/');
    } 
	}


	public function returnHiredProductByOrderID($returned = null)
	{
		if ($orderId = $this->input->post("returnProductOrderID"))
		{
			if ($q = $this->db->query("SELECT OrderID FROM `order` WHERE OrderID = ?", 
				array(
					$orderId
					)
				)) 
			{ 
				if ($q->num_rows() > 0)
				{
					$oId = $q->result();
					/* Reset all quantity to return to null. */
					$updateData['TempReturnQuantity'] = null;
					$this->db->where('orderdetail.OrderID', $orderId);
					/* Step #1. Set all return qty to null */
					if (!$this->db->update('orderdetail', $updateData))
					{
			   			echo "Error: ".$this->db->_error_message(); 
			    		echo "Error: ".$this->db->_error_number(); 
					} 
					else 
					{
						redirect('returnProduct/formReturnEquipmentView/'.$oId[0]->OrderID, 'refresh'); 
						return;
					}
				}
				else 
				{
					$data['alertOnReturnProductBarcode'] = 'The order with the entered order id does not exist';
				}
			}
			else 
			{ 
				$data['alertOnReturnProductBarcode'] = 'The order with the order id \''.$orderId.'\' does not exist';
			}
		}
		else 
		{
			$data['alertOnReturnProductBarcode'] = '';
		}
		$data['title'] = "Return hire by Order ID";
		/**
		* 
		*/
		$this->load->view('header', $data);
		$this->load->view('navbar');
		$this->load->view('returnproduct/returnProductByOrderId');
		$this->load->view('footer');
		
	}

	public function returnHiringProduct($returned = null){
	
		$data = array();
		$data['title'] = "Return Product"; 
		if ($returned)
		{
			$data ['alertOnReturnProductBarcode'] = "Successfully returned.";
		}
		else 
		{
			$data ['alertOnReturnProductBarcode'] = "";
		}
		
		$returnProductBarCode = $this->input->post("returnProductBarCode");
		if ($returnProductBarCode != null)
		{
			$form_check = $this->input->post("form_check");
			
			if($returnProductBarCode == "" && $form_check== 3 )
			{
				$data['alertOnReturnProductBarcode']= "No barcode entered."; 
			} 
			else 
			{ 
				 $orderID = $this->getOrderID($returnProductBarCode);
			
				if($orderID == null || sizeof($orderID) == 0)
				{
					$data['alertOnReturnProductBarcode']= "Can Not Find This Product in Any Order"; 
				} 
				else 
				{ 
					$updateData['TempReturnQuantity'] = null;
					$this->db->where('orderdetail.OrderID', $orderID);
					/* Step #1. Set all return qty to null */
					if ($this->db->update('orderdetail', $updateData))
					{	 
						/* Step #2. Set the return quantity of our product to 1 in the order */
						$orderdetailId = $this->updateReturnQuantity($returnProductBarCode,$orderID); 
						if ($orderdetailId)
						{
							redirect('returns/vc/formReturnEquipmentView/'.$orderID, 'refresh'); 
						} 
						else 
						{
							$data['alertOnReturnProductBarcode']= "Can Not Find This Product in Any Order. "; 
						}
					} 
					else 
					{
						$data['alertOnReturnProductBarcode']= "Could not reset order's return quantities."; 
					}
				}
			}
		} 
		$this->load->view('header',$data);
		$this->load->view('navbar');
		$this->load->view('returnproduct/returnProductView');
		$this->load->view('footer');
				
	}
	
	public function getOrderID($returnProductBarCode){
	
		$countSQL = "SELECT Count(OrderID) AS MatchingOrders FROM productbarcode WHERE Barcode = '$returnProductBarCode'"; 
		$countSQLQuery = $this->db->query($countSQL);
		if ($countSQLQuery && $countSQLQuery->num_rows() > 0)
		{
			$countSQLResult = $countSQLQuery->result_array();
			$count = $countSQLResult[0]['MatchingOrders'];
			
			if($count == 0)
			{
				// Check Max of StockMoveDetailID for the Barcode
				// Which means, it checkes the last "stock movement" of the product with the specified barcode
				$checkMaxStockMoveDetailSQL = 
					"SELECT Max(stockmovedetail.StockMoveID) AS MaxOfStockMoveID 
					FROM location INNER JOIN (product INNER JOIN stockmovedetail ON product.ProductID = stockmovedetail.ProductID) ON 
						location.LocationID = product.LocationID 
						GROUP BY stockmovedetail.Barcode, product.HasBarcodeID, stockmovedetail.StockMoveDetailID, location.HireReturns
						HAVING Not Max(stockmovedetail.StockMoveID Is Null) AND stockmovedetail.Barcode= '$returnProductBarCode'  AND product.HasBarcodeID=2 
						AND location.HireReturns=True 
					ORDER BY stockmovedetail.StockMoveDetailID";
				$checkMaxStockMoveDetailSQlQuery = $this->db->query($checkMaxStockMoveDetailSQL);
				if ($checkMaxStockMoveDetailSQlQuery)
				{
					$checkStockMoveDetailSQLResult = $checkMaxStockMoveDetailSQlQuery->result(); 
					if($checkStockMoveDetailSQLResult == null)
					{ 
						$orderID = "";
						return $orderID;
					}
					else
					{ 
						$stockMoveID = $checkStockMoveDetailSQLResult->MaxOfStockMoveID;
						  //Get OrderID from StockMove
						$orderIDfromStockMoveSQL = "SELECT stockmove.ToOrderID AS OrderID FROM stockmove 
													WHERE stockmove.StockMoveID = '$stockMoveID ' AND stockmove.StockMoveTypeID =2"; 
						$orderIDfromStockMoveSQLQuery = $this->db->query($orderIDfromStockMoveSQL);
						$orderIDfromStockMoveResult = $orderIDfromstockMoveSQLQuery->result(); 
						if($orderIDfromStockMoveResult == null)
						{
							$orderID = ""; 
							return $orderID;
						}
						else
						{ 
							$orderID = $orderIDfromStockMoveResult[0]->OrderID; 
							return $orderID;
						} 
					}
				}
			}
			else
			{
				$orderIdFromProductBarcodeSQL = "SELECT OrderID FROM productbarcode WHERE Barcode = '$returnProductBarCode' ";
				$orderIdFromProductBarcodeSQLQuery = $this->db->query($orderIdFromProductBarcodeSQL);
				if ($orderIdFromProductBarcodeSQLQuery && $orderIdFromProductBarcodeSQLQuery->num_rows() > 0)
				{
					$orderIdFromProductBarcodeResult = $orderIdFromProductBarcodeSQLQuery->result(); 
					$orderID = $orderIdFromProductBarcodeResult[0]->OrderID;
					return $orderID;
				}
			}
		}
		return null;
	}
	
	public function updateReturnQuantity($returnProductBarcode, $orderID)
	{ 		
	   //Get OrderDetailID
		$getOrderDetailIDSQL = 
			"SELECT orderdetail.OrderDetailID 
				FROM (product INNER JOIN orderdetail ON product.ProductID = orderdetail.ProductID) INNER JOIN stockmovedetail ON  
				orderdetail.OrderDetailID = stockmovedetail.OrderDetailID 
			WHERE orderdetail.OrderID=  '$orderID'  AND product.HasBarcodeID=1 AND stockmovedetail.Barcode='$returnProductBarcode' OR  
			orderdetail.OrderID =  '$orderID'  AND product.HasBarcodeID=2 AND stockmovedetail.Barcode='$returnProductBarcode' 
			ORDER BY OrderDetailID"; 
		$getOrderDetailIDSQLQuery = $this->db->query($getOrderDetailIDSQL);
		if ($getOrderDetailIDSQLQuery->num_rows() > 0)
		{
			$getOrderDetailSQLResult = $getOrderDetailIDSQLQuery->result(); 
			$orderDetailID = $getOrderDetailSQLResult[0]->OrderDetailID; 
			if ($getOrderDetailSQLResult == null){
				$data['orderDetailAlertMessage'] = "Can not find Scanned Product in this Order"; 
			}else{  
				$setData['orderdetail.TempReturnQuantity'] = 1;
				$this->db->where('orderdetail.OrderDetailID', $orderDetailID);
				$updateReturnQuantityQuery = $this->db->update('orderdetail', $setData);
			}
			return $orderDetailID;
		}
		else 
		{
			return null;
		}  
	}
 
	public function formReturnEquipmentView($orderId, $orderdetailId = null)
	{ 	
		$data = array(); 
		$data['ReturnLocationErrorMsg'] = $this->session->flashdata('ReturnLocationErrorMsg');
		$data['ReturnHireSuccessMsg'] = $this->session->flashdata('ReturnHireSuccessMsg');
		$data['title'] = "Return Hire Equipment";
		
		$returnLocationSQL = "SELECT location.LocationID, location.LocationDescription FROM location 
								WHERE location.HireReturns= True 
							 ORDER BY location.LocationDescription";
		$rlsq = $this->db->query($returnLocationSQL);
		if ($rlsq)
		{ 
			$data ['returnLocation'] = $rlsq->result();  
			$generalInfoSql = "SELECT order.OrderID, order.OrderDate, CONCAT(FirstName, \" \", LastName, \" (\", BusinessName, \")\") AS CustomerName, 
					CONCAT(order.ShippingAddress, \", \", order.ShippingSuburb, \", \", order.ShippingState, \" \", order.ShippingPostcode, 
						\", \", order.ShippingCountry) AS Address,
					order.HomePhone, order.MobilePhone, order.Email, stockmove.MoveDate FROM 
					(customer INNER JOIN `order` ON customer.CustomerID = order.CustomerID)
					LEFT JOIN stockmove ON order.OrderID = stockmove.ToOrderID WHERE stockmove.StockMoveTypeID = 2 AND order.OrderID = ".$orderId; 
			$genISQ = $this->db->query($generalInfoSql);	
			if ($genISQ) {
				$genInfoRes = $genISQ->result();
				$data['generalInfo'] = $genInfoRes[0]; 
				$getReturnProductsSQL = 
					"SELECT orderdetail.OrderID, product.ProductName, orderdetail.Quantity, stockmovedetail.OrderDetailID, 
					orderdetail.TempReturnQuantity, product.HasBarcodeID, 
					IF(stockmovedetail.OrderDetailID = orderdetail.OrderDetailID, 
						(SELECT Barcode FROM stockmovedetail WHERE OrderDetailID = orderdetail.OrderDetailID LIMIT 1), NULL) AS Barcode,
					(SELECT SumOfQuantity FROM qryreturnformquantityalreadyshipped WHERE stockmovedetail.OrderDetailID LIMIT 1) AS AlreadyShipped
					FROM 
					orderdetail INNER JOIN stockmovedetail ON orderdetail.OrderDetailID = stockmovedetail.OrderDetailID
					INNER JOIN product ON product.ProductID = stockmovedetail.ProductID WHERE orderdetail.ReturnActualDate IS NULL 
					AND orderdetail.OrderID = ".$orderId;   
				$getReturnProductsQuery = $this->db->query($getReturnProductsSQL);  
				if ($getReturnProductsQuery) {
					$data['returnProducts'] = $getReturnProductsQuery->result();  
					$this->load->view('header',$data);
					$this->load->view('navbar');
					$this->load->view('returnproduct/returnHireEquip',$data);
					$this->load->view('footer');
				}
			} 
		} else {
			return;
		}
 
	}


    function orderReturnSubmit()
    {
    	$toLocationId = $this->input->post('ReturnLocation');
    	$orderId = $this->input->post('OrderID'); 
    	$RETURN_HIRE_EQUIPMENT = 3; 
			$date_move = date("Y-m-d H:i:s");
			$this->db->trans_start();
    	$insertStockMove['StockMoveTypeID'] = $RETURN_HIRE_EQUIPMENT;
    	$insertStockMove['MoveDate'] = $date_move;
    	$insertStockMove['ToLocationID'] = $toLocationId;
    	$insertStockMove['FromOrderID'] = $orderId;
    	if ($this->db->insert('stockmove', $insertStockMove))
    	{ 
    		$stockmoveId = $this->db->insert_id();
    		$selectQuery = "SELECT ".$stockmoveId." AS StockMoveID, orderdetail.ProductID, orderdetail.OrderDetailID, 
    			stockmovedetail.Barcode, orderdetail.TempReturnQuantity FROM 
    			(stockmove INNER JOIN stockmovedetail ON stockmove.StockMoveID = stockmovedetail.StockMoveID) 
    			INNER JOIN orderdetail ON stockmovedetail.OrderDetailID = orderdetail.OrderDetailID 
    			WHERE (((stockmove.StockMoveTypeID)=2) AND ((orderdetail.OrderID)= ".$orderId.") AND ((orderdetail.TempReturnQuantity)>0))"; 
			 
			$stockmovesQuery = $this->db->query($selectQuery);
			if ($stockmovesQuery)
			{
				// Create stock move details
				$batchData = array(); 
				$stockmoves = $stockmovesQuery->result();
				foreach ($stockmoves as $row) 
				{
					array_push($batchData, array(
							"StockMoveID" => $stockmoveId, 
							"ProductID" => $row->ProductID, 
							"OrderDetailID" => $row->OrderDetailID, 
							"Barcode" => $row->Barcode, 
							"Quantity" => $row->TempReturnQuantity
						));
				}   
				if ($this->db->insert_batch('stockmovedetail', $batchData))
				{ 
					// Update the barcodes' stored locations
					$updateBarcodeLocations['productbarcode.LocationID'] = $toLocationId;
					$updateBarcodeLocations['productbarcode.OrderID'] = NULL; 
					$this->db->where('stockmove.StockMoveID', $stockmoveId);
					if ($this->db->update('stockmove INNER JOIN (productbarcode INNER JOIN stockmovedetail 
						ON productbarcode.Barcode = stockmovedetail.Barcode) 
						ON stockmove.StockMoveID = stockmovedetail.StockMoveID', 
						$updateBarcodeLocations))
					{ 
						// Update the product quantities for non barcode products
						$this->db->where("stockmovedetail.StockMoveID", $stockmoveId);
						if ($this->db->query(
							"UPDATE stockmove INNER JOIN (stockmovedetail INNER JOIN productlocation ON stockmovedetail.ProductID = productlocation.ProductID) 
							ON (stockmove.FromLocationID = productlocation.LocationID) AND (stockmove.StockMoveID = stockmovedetail.StockMoveID) 
							SET productlocation.Quantity = (productlocation.Quantity + stockmovedetail.Quantity)"))
						{ 
							// Update OrderDetail.ReturnActualDate
							$setDate['orderdetail.ReturnActualDate'] = $date_move;
							$this->db->where("stockmovedetail.StockMoveID", $stockmoveId);
							if ($this->db->update("stockmovedetail INNER JOIN orderdetail 
									ON stockmovedetail.OrderDetailID = orderdetail.OrderDetailID",
								$setDate))
							{ 
								$this->db->trans_complete();
	        					redirect(base_url() . 'returnProduct/returnHiringProduct/1');
								return 1;
							}
						};
					}
				}
			}
		}
    	else 
    	{

    	}
		$this->db->trans_rollback();
		echo "Error: ".$this->db->_error_message(); 
		echo "Error: ".$this->db->_error_number(); 
    }

	
	public function SaveReturnHireEquipment(){
	
		 $OrderID = $this->input->post('OrderID');
		 $OrderDate = $this->input->post('OrderDate');
		 $MoveDate = $this->input->post('mDate');
		 $ShippingAddress = $this->input->post('ShippingAddress');;
		 $HomePhone = $this->input->post('HomePhone');
		$MobilePhone = $this->input->post('MobilePhone'); 
		$Email = $this->input->post('Email');
		$ReturnLocation = $this->input->post('ReturnLocation');
	
		$ReturnDate = $this->input->post('ReturnDate');
		
		$ProductName = $this->input->post('ProductName');
		$AlreadyShipped = $this->input->post('AlreadyShipped');
		$Barcode = $this->input->post('Barcode');
		$TempReturnQuantity = $this->input->post('TempReturnQuantity');
		 
		if($ReturnLocation == null){
			//echo  $ReturnLocationErrorMsg = "You must select a Return Location before saving";
			$this->session->set_flashdata('ReturnLocationErrorMsg', $ReturnLocationErrorMsg);
		}else{ 
			$StockMoveID = $this->MoveOrderToStock($ReturnLocation,$OrderID,$OrderDate,$ReturnDate,$MoveDate);
			
			if($StockMoveID > 0 ){
				
				$ReturnHireSuccessMsg = "Hire Equipment returned successfully.";
				$this->session->set_flashdata('ReturnHireSuccessMsg', $ReturnHireSuccessMsg);
			
			}
		}
	
	}
	
	public function MoveOrderToStock($ReturnLocation,$OrderID,$OrderDate,$ReturnDate,$MoveDate){
	
		$PURCHASE_NEW_STOCK = 1;
		$ORDER_FULFILMENT = 2;
		$RETURN_HIRE_EQUIPMENT = 3;
		$MOVE_STOCK = 4;
		
		$InsertIntoStockMoveSQL = "INSERT INTO stockmove ( StockMoveTypeID, MoveDate, ToLocationID, FromOrderID ) 
										VALUES ('$RETURN_HIRE_EQUIPMENT','$MoveDate','$ReturnLocation','$OrderID')";
										
		$InsertIntoStockMoveSQLQuery = $this->db->query($InsertIntoStockMoveSQL);
	
		$stockMoveIDSQL = "SELECT StockMoveID FROM stockmove WHERE MoveDate = '$MoveDate'";
		$stockMoveIDSQLQuery =$this->db->query($stockMoveIDSQL);
		$stockMoveIDResult = $stockMoveIDSQLQuery->result();
		
		 $StockMoveID  = $stockMoveIDResult[0]->StockMoveID;
		exit();
		
		$createStockMoveDetailSQL = "INSERT INTO stockmovedetail ( StockMoveID, ProductID, OrderDetailID, Barcode, Quantity ) 
										SELECT  '$StockMoveID'  AS StockMoveID, orderdetail.ProductID, orderdetail.OrderDetailID, stockmovedetail.Barcode, orderdetail.TempReturnQuantity 
										FROM (stockmove INNER JOIN stockmovedetail ON stockmove.StockMoveID = stockmovedetail.StockMoveID) INNER JOIN orderdetail ON 
										stockmovedetail.OrderDetailID = orderdetail.OrderDetailID 
									WHERE stockmove.StockMoveTypeID=2 AND orderdetail.OrderID= '$OrderID' AND orderdetail.TempReturnQuantity > 0";
		$createStockMoveDetailSQLQuery = $this->db->query($createStockMoveDetailSQL);
		
		$updateBarcodeRecordLocationSQL = "UPDATE stockmove INNER JOIN (productbarcode INNER JOIN stockmovedetail ON productbarcode.Barcode = stockmovedetail.Barcode) 
												ON stockmove.StockMoveID = stockmovedetail.StockMoveID 
											SET ProductBarcode.LocationID = '$ReturnLocation', productbarcode.OrderID = NULL 
											WHERE stockmove.StockMoveID= ' $StockMoveID' ";
		
		$updateBarcodeRecordLocationSQLQuery = $this->db->query($updateBarcodeRecordLocationSQL);
		
		$updateProductQuantitiesSQL = "UPDATE stockmove INNER JOIN (stockmovedetail INNER JOIN productlocation ON stockmovedetail.ProductID = productlocation.ProductID) ON (stockmove.FromLocationID = productlocation.LocationID) AND (stockmove.StockMoveID = stockmovedetail.StockMoveID) 
										SET productlocation.Quantity = productlocation.Quantity + stockmovedetail.Quantity 
										WHERE stockmovedetail.StockMoveID= '$StockMoveID' ";
		$updateProductQuantitiesSQLQuery = $this->db->query($updateProductQuantitiesSQL);
	
		$updateOrderDetailReturnActualDateSQL = "UPDATE stockmovedetail INNER JOIN orderdetail ON stockmovedetail.OrderDetailID = orderdetail.OrderDetailID SET  
													orderdetail.ReturnActualDate = '$ReturnDate' 
												WHERE stockmovedetail.StockMoveID='$StockMoveID'";
		$updateOrderDetailReturnActualDateSQLQuery = $this->db->query($updateOrderDetailReturnActualDateSQL);
		
		redirect('ReturnProduct/returnHiringProduct','refresh');
	
		return $StockMoveID;
	}
}


?>