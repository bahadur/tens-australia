<?php
class Api extends CI_Controller { 

  function __construct(){
     
    parent::__construct();  
    $this->load->library('httpresponses');
    $this->load->library('Datatables');
    header('Content-Type: application/json'); 
  }


  function getReturnItemsDt() {
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator'))) {  
      $requestBody = json_decode(file_get_contents('php://input')); 
      if ($requestBody && $requestBody->OrderID) { 
        $this->datatables
          ->select('orderdetail.OrderID, product.ProductName, orderdetail.Quantity, stockmovedetail.OrderDetailID, 
          orderdetail.TempReturnQuantity, product.HasBarcodeID, 
          (SELECT Barcode FROM stockmovedetail WHERE OrderDetailID = orderdetail.OrderDetailID LIMIT 1) AS Barcode,
          (SELECT SumOfQuantity FROM qryreturnformquantityalreadyshipped WHERE stockmovedetail.OrderDetailID LIMIT 1) AS AlreadyShipped')
          ->from('orderdetail')
          ->join('stockmovedetail', 'orderdetail.OrderDetailID = stockmovedetail.OrderDetailID','inner')
          ->join('product', 'product.ProductID = stockmovedetail.ProductID','inner')
          ->where('orderdetail.ReturnActualDate IS NULL AND orderdetail.OrderID = '.$requestBody->OrderID);
          
        $res = $this->datatables->generate();
        if ($res) {
          $this->httpresponses->respondJson($res, 200);
        } else {
          log_message('error', $this->db->last_query());
          log_message('error', $this->db->_error_message());
          $this->httpresponses->respondJsonify(array('message' => 'Failed to get data'), 400);
        }
      } else {
        $this->httpresponses->respondJsonify(array('message' => 'Invalid parameters'), 400);
      } 
    } else {
      $this->httpresponses->respondJsonify(array('message' => 'Unauthorized'), 401);
    }   
  }


  function orderReturnSubmit() {
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator'))) { 
      $requestBody = json_decode(file_get_contents('php://input')); 
      if ($requestBody && $requestBody->OrderID && $requestBody->ToLocationID) { 
        $toLocationId = $requestBody->ToLocationID;
        $orderId = $requestBody->OrderID; 
        $RETURN_HIRE_EQUIPMENT = 3; 
        $date_move = date("Y-m-d H:i:s");
        $this->db->trans_start();
        $returnSMInsert['StockMoveTypeID'] = $RETURN_HIRE_EQUIPMENT;
        $returnSMInsert['MoveDate'] = $date_move;
        $returnSMInsert['ToLocationID'] = $toLocationId;
        $returnSMInsert['FromOrderID'] = $orderId;
        if ($this->db->insert('stockmove', $returnSMInsert)) { 
          $returnStockMoveId = $this->db->insert_id();  
          $shippedOrderDetailsQ = $this->db->query('SELECT '.$returnStockMoveId.' AS StockMoveID, orderdetail.ProductID, orderdetail.OrderDetailID, 
              stockmovedetail.Barcode, orderdetail.TempReturnQuantity FROM 
              (stockmove INNER JOIN stockmovedetail ON stockmove.StockMoveID = stockmovedetail.StockMoveID) 
              INNER JOIN orderdetail ON stockmovedetail.OrderDetailID = orderdetail.OrderDetailID 
              WHERE (((stockmove.StockMoveTypeID)=2) AND ((orderdetail.OrderID)= '.$orderId.') AND ((orderdetail.TempReturnQuantity)>0))');
          if ($shippedOrderDetailsQ && $shippedOrderDetailsQ->num_rows() > 0) { 
            $batchData = array(); 
            $stockmoves = $shippedOrderDetailsQ->result();
            foreach ($stockmoves as $row)  {
              array_push($batchData, array(
                  "StockMoveID" => $returnStockMoveId, 
                  "ProductID" => $row->ProductID, 
                  "OrderDetailID" => $row->OrderDetailID, 
                  "Barcode" => $row->Barcode, 
                  "Quantity" => $row->TempReturnQuantity
                ));
            }   
            if ($this->db->insert_batch('stockmovedetail', $batchData)) { 
              // Update the barcodes' stored locations
              $updateBarcodeLocations['productbarcode.LocationID'] = $toLocationId;
              $updateBarcodeLocations['productbarcode.OrderID'] = NULL; 
              $this->db->where('stockmove.StockMoveID', $returnStockMoveId);

              if ($this->db->update('stockmove INNER JOIN (productbarcode INNER JOIN stockmovedetail ON productbarcode.Barcode = stockmovedetail.Barcode) 
                ON stockmove.StockMoveID = stockmovedetail.StockMoveID', $updateBarcodeLocations)) {  
                log_message('debug', $this->db->last_query());
                $this->db->where("stockmovedetail.StockMoveID", $returnStockMoveId);
                // Update the quantity of products at the location just returned to
                if ($this->db->query('UPDATE stockmove INNER JOIN (stockmovedetail INNER JOIN productlocation ON stockmovedetail.ProductID = productlocation.ProductID) 
                              ON (stockmove.FromLocationID = productlocation.LocationID) AND (stockmove.StockMoveID = stockmovedetail.StockMoveID) 
                              SET productlocation.Quantity = (productlocation.Quantity + stockmovedetail.Quantity)')) {  
                  $setDate['orderdetail.ReturnActualDate'] = $date_move;
                  $this->db->where("stockmovedetail.StockMoveID", $returnStockMoveId);
                  if ($this->db->update("stockmovedetail INNER JOIN orderdetail ON stockmovedetail.OrderDetailID = orderdetail.OrderDetailID", $setDate)) {   
                    log_message('debug', $this->db->last_query());
                    $this->httpresponses->respondJsonify(array('success' => true), 200);
                    return;
                  }
                } 
              } 
            } 
          } 
        } 
        log_message('error', $this->db->last_query());
        log_message('error', $this->db->_error_message());
        $this->httpresponses->respondJsonify(array('message' => 'Could not move the stock'), 400);
      }
    } else {
      $this->httpresponses->respondJsonify(array('message' => 'Unauthorized'), 401);
    }
  }



}