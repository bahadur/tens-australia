<?php
class jedapi extends CI_Controller { 

  function __construct(){
     
    parent::__construct();  
    $this->load->library('httpresponses');
    $this->load->library('Datatables'); 
  }

  /**
   * Update temporary quantity (JEditable)
   * @return [type] [description]
   */
  public function updateTempReturnQuantity() {
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator'))) {  
      $orderdetailid = $this->input->post('id');
      $returnQuantity = $this->input->post('value');
      if ($orderdetailid !== null && $returnQuantity !== null) {
        if ((int)$returnQuantity != 0) {
          $returnUpdate['TempReturnQuantity'] = $returnQuantity;
        } else {
          $returnUpdate['TempReturnQuantity'] = null;
        }
        $this->db->where('OrderDetailID', $orderdetailid);
        if ($this->db->update('orderdetail', $returnUpdate)) {
          $this->httpresponses->respondJsonify(array('success' => $this->db->last_query()), 200);
        } else  {
          log_message('error', $this->db->last_query());
          log_message('error', $this->db->_error_message());
          $this->httpresponses->respondJsonify(array('message' => 'Could not update the orderdetail return quantity'), 400);
        }
      } else {
        $this->httpresponses->respondJsonify(array('message' => 'Invalid parameters'), 400);
      }
    } else {
      $this->httpresponses->respondJsonify(array('message' => 'Unauthorized'), 401);
    }
  }

}