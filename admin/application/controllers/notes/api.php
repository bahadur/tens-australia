<?php
class api extends CI_Controller {


  function __construct() {

    parent::__construct();
    header('Content-Type: application/json'); 
    $this->load->library('Datatables');
    $this->load->library('httpresponses'); 
  }
  
  /**
   * Asyncronously allows adding a new note.
   * @param [type] $customerId [description]
   * @param [type] $orderId    [description]
   */
  public function addNote() {
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator'))) {
      $requestBody = json_decode(file_get_contents('php://input'));  
      if ($requestBody->CustomerID && isset($requestBody->OrderID)) {
        if ($requestBody->StaffID) {
          $insertData['CustomerID'] = $requestBody->CustomerID;
          if ( $requestBody->OrderID == 0 ||  $requestBody->OrderID == "0") {
            $insertData['OrderID'] = null;
          } else {
            $insertData['OrderID'] = $requestBody->OrderID;
          }
          $insertData['DateTime'] = $requestBody->DateTime;
          $insertData['StaffID'] = $requestBody->StaffID;
          $insertData['CustomerProblemFlagID'] = $requestBody->CustomerProblemFlagID;
          $insertData['Note'] = $requestBody->Note;
          if ($this->db->insert('customernote', $insertData)) { 
              $this->db->where('customer.CustomerID = '.$requestBody->CustomerID.' AND 
                CustomerProblemFlagID < '.$requestBody->CustomerProblemFlagID);
              if ($this->db->update('customer', array(
                'customer.CustomerProblemFlagID = '.$requestBody->CustomerProblemFlagID
                ))) {
                $this->httpresponses->respondJsonify(array('success' => true, 'CustomerProblemFlagID' => $requestBody->CustomerProblemFlagID), 200);
              } else {
                $this->httpresponses->respondJsonify(array('success' => true), 200);
              };
          } else {
            log_message('error', $this->db->last_query());
            log_message('error', $this->db->_error_message());
            $this->httpresponses->respondJsonify(array('message' => 'Could not add a note'), 400);
          }
        } else {
          $this->httpresponses->respondJsonify(array('message' => 'Staff needs to be specified'), 400);
        }
      } else {
          $this->httpresponses->respondJsonify(array('message' => 'Invalid parameters'), 400);
      }
    } else {
      $this->httpresponses->respondJsonify(array('message' => 'Unauthorized'), 401);
    }
  }
}