<?php
class formapi extends CI_Controller {


  function __construct() {

    parent::__construct(); 
    $this->load->library('Datatables');
    $this->load->library('httpresponses'); 
  } 

  public function orderNotesDt($orderId) { 
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator'))) {
      $this->datatables
        ->select('customernote.DateTime, LastName, Firstname, customerproblemflag.Flag, customernote.Note, customernote.OrderID')
        ->from('customerproblemflag RIGHT JOIN (customernote LEFT JOIN Staff ON customernote.StaffID=Staff.StaffID)
              ON customerproblemflag.CustomerProblemFlagID=customernote.CustomerProblemFlagID')
        ->where('customernote.OrderID = '.$orderId);
      $this->httpresponses->respond($this->datatables->generate(), 200);
    } else {
      $this->httpresponses->respondJson(array('message' => 'Unauthorized'), 401);
    } 
  }
}