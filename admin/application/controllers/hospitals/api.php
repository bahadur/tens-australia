<?php
class Api extends CI_Controller { 

  function __construct(){
     
    parent::__construct(); 
    $this->load->helper('ssp'); 
    $this->load->library('httpresponses');
    header('Content-Type: application/json'); 
  }

  function allhospitals()
  {
      echo json_encode($this->db->get("hospital")->result_array());
  }


  function getHospitalsSelect() 
  { 
    $sqlString = "SELECT * FROM hospital";
    $res = $this->getData($sqlString);
    if ($res)
    {
      $resultsArray = array();
      foreach ($res as $hospital) 
      {
        $resultsArray[] = array(
          "id" => $hospital->HospitalID,
          "text" => $hospital->HospitalName
        );
      }
      echo json_encode($resultsArray);
      return;
    }
    echo "N/A";
  }

  
  private function getData($sqlString)
  {
    $sqlQuery = $this->db->query($sqlString);
    if ($sqlQuery->num_rows() > 0)
    {
      $results = $sqlQuery->result();
      return $results;
    }
    return null;
  }
}