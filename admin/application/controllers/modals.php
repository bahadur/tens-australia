<?php 

/**
* 
*/
class Modals extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();

	}


	 function user_edit($id){

	 	$query = $this->db->query("
	 			SELECT  
	 			`users`.`id` AS  'id', 
	 			`user_profiles`.`firstname` AS  'firstname', 
	 			`user_profiles`.`lastname` AS  'lastname', 
	 			`groups`.`name` AS  'group', 
	 			`users`.`username` AS  'username', 
	 			`users`.`email` AS  'email'
			 FROM `users` INNER JOIN user_profiles ON users.id = user_profiles.user_id INNER JOIN (users_groups INNER JOIN groups 
			ON groups.id = users_groups.group_id) ON users_groups.user_id = users.id
			WHERE users.id = $id");

	 	$data["rec_user"] = $query->row(); 
	 	$this->load->view("modals/user_edit",$data);
	}


	function paymentMethodEdit($paymentMethodID){

		$this->db->where("PaymentMethodID",$paymentMethodID);

		

		$data["rec"] = $this->db->get('paymentmethod')->row();
	 	$this->load->view("modals/payment_method_edit",$data);

	
	}

	function accountCodeEdit($accountcode){
		$this->db->where("AccountCode",$accountcode);

		

		$data["rec"] = $this->db->get('accountcode')->row();
	 	$this->load->view("modals/account_code_edit",$data);
	}


	function customerTypeEdit($customerTypeID){
			$this->db->where("CustomerTypeID",$customerTypeID);

		

		$data["rec"] = $this->db->get('customertype')->row();
	 	$this->load->view("modals/customer_type_edit",$data);
	}


	function educationLocationEdit($EduLocationID){
			$this->db->where("EduLocationID",$EduLocationID);

		

		$data["rec"] = $this->db->get('educatorlocation')->row();
	 	$this->load->view("modals/educator_location_edit",$data);
	}
	
	function hospitalGroupEdit($HospitalGroupID){
			$this->db->where("HospitalGroupID",$HospitalGroupID);

		

		$data["rec"] = $this->db->get('hospitalgroup')->row();
	 	$this->load->view("modals/hospital_group_edit",$data);
	}
	
	function educatorEdit($EducatorID){
			$this->db->where("EducatorID",$EducatorID);

		

		$data["rec"] = $this->db->get('educator')->row();
	 	$this->load->view("modals/educator_edit",$data);
	}

	function reminderEdit($ReminderID){
			$this->db->where("ReminderID",$ReminderID);

		

		$data["rec"] = $this->db->get('reminder')->row();
	 	$this->load->view("modals/reminder_edit",$data);
	}

	function locationTypeEdit($LocationTypeID){
			$this->db->where("LocationTypeID",$LocationTypeID);

		

		$data["rec"] = $this->db->get('locationtype')->row();
	 	$this->load->view("modals/locationtype_edit",$data);
	}
	

	

	function orderSourceEdit($OrderSourceID){
			$this->db->where("OrderSourceID",$OrderSourceID);

		

		$data["rec"] = $this->db->get('ordersource')->row();
	 	$this->load->view("modals/ordersource_edit",$data);
	}

	function priceGroupEdit($PriceGroupID){
			$this->db->where("PriceGroupID",$PriceGroupID);

		

		$data["rec"] = $this->db->get('pricegroup')->row();
	 	$this->load->view("modals/pricegroup_edit",$data);
	}

	function postcodeEdit($PostcodeID){
			$this->db->where("PostcodeID",$PostcodeID);

		

		$data["rec"] = $this->db->get('postcode')->row();
	 	$this->load->view("modals/postcode_edit",$data);
	}
	
	function transactionTypeEdit($TransactionTypeID){
			$this->db->where("TransactionTypeID",$TransactionTypeID);

		

		$data["rec"] = $this->db->get('transactiontype')->row();
	 	$this->load->view("modals/transactiontype_edit",$data);
	}

	function shipMethodEdit($ShipMethodID){
			$this->db->where("ShipMethodID",$ShipMethodID);

		

		$data["rec"] = $this->db->get('shipmethod')->row();
	 	$this->load->view("modals/shipmethod_edit",$data);
	}

	function stockMoveTypeEdit($StockMoveTypeID){
			$this->db->where("StockMoveTypeID",$StockMoveTypeID);

		

		$data["rec"] = $this->db->get('stockmovetype')->row();
	 	$this->load->view("modals/stockmovetype_edit",$data);
	}

	function kitEdit($KitID){
			$this->db->where("KitID",$KitID);

		

		$data["rec"] = $this->db->get('kit')->row();
	 	$this->load->view("modals/kit_edit",$data);
	}

	function kitProductList($KitID){
		
		$this->db->select("product.ProductName,
							kitproduct.Quantity");
		$this->db->where("KitID",$KitID);


		$this->db->join("product","kitproduct.ProductID = product.ProductID");
		$data["rec"] = $this->db->get('kitproduct')->result();
	 	$this->load->view("modals/kit_prduct_list",$data);
	}

	
	
	function hospitalEdit($HospitalID){
			$this->db->where("HospitalID",$HospitalID);

		

		$data["rec"] = $this->db->get('hospital')->row();
	 	$this->load->view("modals/hospital_edit",$data);
	}

	function productEdit($ProductID){
			$this->db->where("ProductID",$ProductID);

		

		$data["rec"] = $this->db->get('product')->row();
		
	 	$this->load->view("modals/product_edit",$data);
	}
	
	
	
	function productCategoryEdit($ProductCategoryID){
			$this->db->where("ProductCategoryID",$ProductCategoryID);

		

		$data["rec"] = $this->db->get('productcategory')->row();
	 	$this->load->view("modals/productcategory_edit",$data);
	}
	
	

	function InsuranceProviderEdit($InsuranceProviderID){
			$this->db->where("InsuranceProviderID",$InsuranceProviderID);

		

		$data["rec"] = $this->db->get('insuranceprovider')->row();
	 	$this->load->view("modals/insuranceprovider_edit",$data);
	}
	

	function LocationEdit($LocationID){
			$this->db->where("LocationID",$LocationID);

		

		$data["rec"] = $this->db->get('location')->row();
	 	$this->load->view("modals/location_edit",$data);
	}
	


	function educatorClassEdit($EduClassID){
			$this->db->where("EduClassID",$EduClassID);

		

		$data["rec"] = $this->db->get('educatorclass')->row();
	 	$this->load->view("modals/educator_class_edit",$data);
	}
	
	

	function areaManagerEdit($areaManager){
		$this->db->where("AreaManagerID",$areaManager);

		

		$data["rec"] = $this->db->get('areamanager')->row();
		$this->db->where("AreaManagerID", $areaManager);
		$codes = array();
		foreach ($this->db->get("areamanagerpostcode")->result() as $row) {
			
			$codes[] = $row->Postcode;
		}

		
		$data["postcodes"] = implode(",", $codes);


	 	$this->load->view("modals/area_manager_edit",$data);
	}
}
 ?>