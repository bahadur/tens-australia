<?php
class Ajax extends CI_Controller {


	function __construct(){
		 
		parent::__construct(); 
		$this->load->helper('ssp'); 
	}


 
	function orderFulfilmentSubmit($orderid, $locationid){
		$date_move = date("Y-m-d H:i:s");
		$comments = $this->input->post('comments');
		$query = $this->db->query("
					INSERT INTO stockMove 
					( 
						StockMoveTypeID, 
						MoveDate, 
						MoveBy, 
						FromPurchaseOrderNo, 
						FromLocationID, 
						ToOrderID, 
						Comments ) 
					VALUES (2, '".$date_move."', '', '', $locationid, $orderid, '$comments')");

		$query = $this->db->query("SELECT StockMoveID from StockMove where MoveDate = '$date_move'");
		$stockMoveId = $query->row()->StockMoveID;

		$query = $this->db->query("
			INSERT INTO StockMoveDetail ( StockMoveID, ProductID, OrderDetailID, Barcode, Quantity )
			SELECT 
			$stockMoveId AS StockMoveID, 
			orderDetail.ProductID, 
			orderDetail.OrderDetailID, 
			orderDetail.TempBarcode, 
			orderDetail.TempPickQuantity 
			FROM OrderDetail
			WHERE OrderID = $orderid
			AND OrderDetail.TempPickQuantity > 0");	
			

		$query = $this->db->query("
		UPDATE stockMove 
		INNER JOIN 
		(stockMoveDetail INNER JOIN productLocation ON stockMoveDetail.ProductID = productLocation.ProductID) 
		ON (stockMove.FromLocationID = productLocation.LocationID) AND (stockMove.StockMoveID = stockMoveDetail.StockMoveID) 
		SET productLocation.Quantity = productLocation.Quantity-stockMoveDetail.Quantity
		WHERE StockMoveDetail.StockMoveID= $stockMoveId ");

		echo json_encode(array("status" => 1));	
	}

	function customers(){

		//print_r($_POST);
		

		$this->db->where("CustomerID", $this->input->post("customerid"));
		$this->db->join("customerproblemflag", "customer.CustomerProblemFlagID = customerproblemflag.CustomerProblemFlagID");

		echo json_encode($this->db->get("customer")->result_array());
		 
		
	}

	function update_customer(){
		
		$data = array();
		$CustomerID = '';
		foreach ($this->input->post() as $key => $rec) {
			if($key == 'CustomerID'){
				$CustomerID = $rec;
			} else {
				$this->db->set($key,$rec);
			}

		}
		$this->db->where('CustomerID', $CustomerID);
		$this->db->update('customer', $data); 
		echo  json_encode(array(1));
	} 
 
	function move_equipment(){
		if($this->input->post()){
			$date = $this->input->post('move_date');
			 $my_date = date('Y-m-d', strtotime($date));
			 
			$data = array('StockMoveTypeID' => 4,
							'MoveDate' =>  $my_date,
							'FromLocationID' => $this->input->post('from_location'),
							'ToLocationID' => $this->input->post('to_location'),
							'Comments' => $this->input->post('comments')

						);

			// $this->db->order_by("StockMoveID","desc");
			// $this->db->limit(1);

			// $stockmoveid = $this->db->get('stockmove')->row();
			// $newstocks_id = $stockmove->StockMoveID + 1;

			if($this->db->insert('stockmove',$data)){
			$newstocks_id = $this->db->insert_id();	

			foreach($this->input->post('products') as $product){

				$this->db->insert('stockmovedetail',array('StockMoveID' => $newstocks_id,
									'ProductID' => $product['productid'],
									'Barcode' 	=> $product['barcode'],
									'Quantity' 	=> $product['qty']));
					

			}
			// decrement qty from
			$query = $this->db->query("
					UPDATE stockMove INNER JOIN (stockMoveDetail INNER JOIN productLocation ON stockMoveDetail.ProductID = productLocation.ProductID) ON (stockMove.FromLocationID = productLocation.LocationID) AND (stockMove.StockMoveID = stockMoveDetail.StockMoveID) 
                    SET productLocation.Quantity = productLocation.quantity-stockMoveDetail.quantity 
                    WHERE stockMoveDetail.StockMoveID=".$newstocks_id);


			// update the barcode records locations
            $query = $this->db->query("
            		UPDATE stockMove INNER JOIN (productBarcode INNER JOIN stockMoveDetail ON productBarcode.Barcode = stockMoveDetail.Barcode) ON stockMove.StockMoveID = stockMoveDetail.StockMoveID
                    SET productBarcode.LocationID = stockMove.ToLocationID 
                    WHERE stockMove.StockMoveID=".$newstocks_id);



            // increment qty to
        	$query = $this->db->query("
        		UPDATE stockMove INNER JOIN (stockMoveDetail INNER JOIN productLocation ON stockMoveDetail.ProductID = productLocation.ProductID) ON (stockMove.ToLocationID = productLocation.LocationID) AND (stockMove.StockMoveID = stockMoveDetail.StockMoveID)
                SET productLocation.Quantity = productLocation.quantity+stockMoveDetail.quantity
                WHERE stockMoveDetail.StockMoveID=".$newstocks_id);
        
   
        	// create product location if required
        	$query = $this->db->query("
        		INSERT INTO productLocation ( ProductID, LocationID, Quantity )
                SELECT stockMoveDetail.ProductID, ".$this->input->post('to_location'). " AS ToLocationID, stockMoveDetail.Quantity
                FROM stockMoveDetail LEFT JOIN (SELECT productLocation.ProductLocationID, productLocation.ProductID FROM productLocation WHERE locationID = " .$this->input->post('to_location'). " ) AS filteredProductLocation ON stockMoveDetail.ProductID = filteredProductLocation.ProductID
                WHERE (((filteredProductLocation.ProductLocationID) Is Null) AND ((stockMoveDetail.StockMoveID)=".$newstocks_id.") AND (Barcode=0))");
        

			echo json_encode(array('status'=>1));
			} 
			 else {
			 	echo json_encode(array('status'=>0));
			 }
			//print_r($this->input->post());
			

			


		}	
	}

	public function barcode_location(){
		$query = $this->db->query("
			SELECT *  FROM productBarcode
			WHERE Barcode='".$this->input->post('barcode')."'
			AND LocationID='".$this->input->post('fromlocationid')."'");
		//echo  $query->num_rows();
		
		echo json_encode(array('status'=> ($query->num_rows()>0)?true:false));
		
	}



	public function shipout(){

        $table = 'shipMethod';
        $primaryKey = 'OrderID';
        $joinTable = 'order';
        
        $joins ="RIGHT JOIN 
        	(
        		`customer` INNER JOIN 
        		(
					(
						`order` LEFT JOIN `qryFulfilmentListOrderPaid` ON `order`.`OrderID` = `qryFulfilmentListOrderPaid`.`OrderID`
					) 	LEFT JOIN `qryFulfilmentListAllShipped` ON `order`.`OrderID` = `qryFulfilmentListAllShipped`.`OrderID`
				) 
				ON `customer`.`CustomerID` = `order`.`CustomerID`
			) 
			ON `shipMethod`.`ShipMethodID` = `order`.`ShipMethodID`";


		$sWhere = "(
				( (`qryFulfilmentListAllShipped`.`OrderID`) Is Null) AND ( (`shipMethod`.`ShipMethodID`)<>27) 
			)";
			
        $columns = array(
            array(
                'db' => 'RequiredDate',
                'dt' => 0,
                'table' => "order",
                'alias' => 'RequiredDate'
            ),
            array(
                'db' => 'OrderID',
                'dt' => 1,
                'table' => "order",
                'alias' => 'OrderID'
            ),
            array(
                'db' => "Firstname",
                'dt' => 2,
                'table' => "",
                'alias' => 'Firstname'
            ),
            array(
                'db' => "Lastname",
                'dt' => 3,
                'table' => "",
                'alias' => 'Lastname'
            ),
            array(
                'db' => "BusinessName",
                'dt' => 4,
                'table' => "",
                'alias' => 'BusinessName'
            ),
            
            array(
                'db' => 'shipMethod',
                'dt' => 5,
                'table' => $table,
                'alias' => 'shipMethod'
            ),
           //if(SumofLineTotal=SumofAmount,'Paid In Full','Not Paid')
            array(
                'db' => "SumofLineTotal",
                'dt' => 6,
                'table' =>  'qryFulfilmentListOrderPaid',
                'alias' => 'SumofLineTotal'
            ),
			//if(SumofLineTotal=SumofAmount,True,False)
            array(
                'db' => "SumofAmount",
                'dt' => 7,
                'table' => 'qryFulfilmentListOrderPaid',
                'alias' => 'SumofAmount'
            ),
            array(
                'db' => "DueDate",
                'dt' => 8,
                'table' => 	'order',
                'alias' => 'DueDate'
            )
        );


        echo json_encode(simple(
        					$_GET, 
        					$table,
        					$primaryKey, 
        					$columns, 
        					$sWhere, 
        					$joins, 
        					$joinTable 
        					
        					
        					)
        				);
  
	}

	function user_accounts(){


		
		$table = 'users';
        $primaryKey = 'id';
        $joinTable = 'user_profiles';
        $joinTable2 = 'users_groups';
        $joinTable3 = 'groups';
        
        $joins ="INNER JOIN $joinTable ON $table.id = $joinTable.user_id INNER JOIN ($joinTable2 INNER JOIN $joinTable3 
		ON $joinTable3.id = $joinTable2.group_id) ON $joinTable2.user_id = $table.id";


       //$sWhere = "";
		
			
        $columns = array(
             array(
                'db' => "id",
                'dt' => 0,
                'table' => $table,
                'alias' => 'id'
            ),
            array(
                'db' => "firstname",
                'dt' => 1,
                'table' => $joinTable,
                'alias' => 'firstname'
            ),
            array(
                'db' => "lastname",
                'dt' => 2,
                'table' => $joinTable,
                'alias' => 'lastname'
            ),
            array(
                'db' => "name",
                'dt' => 3,
                'table' => $joinTable3,
                'alias' => 'group'
            ),
            array(
                'db' => 'username',
                'dt' => 4,
                'table' => $table,
                'alias' => 'username'
            ),

            array(
                'db' => 'email',
                'dt' => 5,
                'table' =>  $table,
                'alias' => 'email'
            ),
            
            
            
        );


        echo json_encode(simple(
        					$_GET, 
        					$table,
        					$primaryKey, 
        					$columns, 
        					$joins, 
        					$joinTable 
        					
        					
        					)
        				);
        
	}


	public function shipout2(){
		
		$aColumns = array( 

		"`order`.`RequiredDate`", 
		"`order`.`OrderID`", 
		"CONCAT(`Firstname`, \" \", `Lastname`, \" (\", `BusinessName`,\")\") AS Customer", 
		"`shipMethod`.`ShipMethod`", 
		"if(`SumofLineTotal`=`SumofAmount`,'Paid In Full','Not Paid') AS PaidText", 
		"if(`SumofLineTotal`=`SumofAmount`,True,False) AS Paid", 
		"datediff(`order`.`DueDate`,CURDATE()) AS DaysToDueDate"
		);



		/* Indexed column (used for fast and accurate table cardinality) */
	$sIndexColumn = "`order`.`OrderID`";
	
	/* DB table to use */
	$sTable = "`shipMethod`";
	/* 
	 * Paging
	 */
	$sLimit = "";
	if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
	{
		$sLimit = "LIMIT ".mysql_real_escape_string( $_GET['iDisplayStart'] ).", ".
			mysql_real_escape_string( $_GET['iDisplayLength'] );
	}
	
	
	/*
	 * Ordering
	 */
	if ( isset( $_GET['iSortCol_0'] ) )
	{
		$sOrder = "ORDER BY  ";
		for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
		{
			if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
			{
				$sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
				 	".mysql_real_escape_string( $_GET['sSortDir_'.$i] ) .", ";
			}
		}
		
		$sOrder = substr_replace( $sOrder, "", -2 );
		if ( $sOrder == "ORDER BY" )
		{
			$sOrder = "";
		}
	}
	
	
	/* 
	 * Filtering
	 * NOTE this does not match the built-in DataTables filtering which does it
	 * word by word on any field. It's possible to do here, but concerned about efficiency
	 * on very large tables, and MySQL's regex functionality is very limited
	 */
	$sWhere = "WHERE (
				( (`qryFulfilmentListAllShipped`.`OrderID`) Is Null) AND ( (`shipMethod`.`ShipMethodID`)<>27) 
			)";
	if ( $_GET['sSearch'] != "" )
	{
		$sWhere = "AND (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= $aColumns[$i]." LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
	}
	
	/* Individual column filtering */
	for ( $i=0 ; $i<count($aColumns) ; $i++ )
	{
		if ( $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
		{
			// if ( $sWhere == "" )
			// {
			// 	$sWhere = "WHERE ";
			// }
			// else
			// {
				$sWhere .= " AND ";
			//}
			$sWhere .= $aColumns[$i]." LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
		}
	}
	
	
	/*
	 * SQL queries
	 * Get data to display
	 */

	
	$sQuery = "
		SELECT SQL_CALC_FOUND_ROWS 
		".str_replace(" , ", " ", implode(", ", $aColumns))."
		FROM $sTable  
		RIGHT JOIN (`customer` INNER JOIN (
				(`order` LEFT JOIN `qryFulfilmentListOrderPaid` ON `order`.`OrderID` = `qryFulfilmentListOrderPaid`.`OrderID`) 
				LEFT JOIN `qryFulfilmentListAllShipped` ON `order`.`OrderID` = `qryFulfilmentListAllShipped`.`OrderID`) 
				ON `customer`.`CustomerID` = `order`.`CustomerID`) 
				ON `shipMethod`.`ShipMethodID` = `order`.`ShipMethodID`
		$sWhere
		$sOrder
		$sLimit";
	$rResult = $this->db->query($sQuery);
	
	/* Data set length after filtering */
	$sQuery = "
		SELECT FOUND_ROWS()
	";
	$rResultFilterTotal = $this->db->query($sQuery);//mysql_query( $sQuery, $gaSql['link'] ) or die(mysql_error());
	$aResultFilterTotal = $rResultFilterTotal->result_array();
	$iFilteredTotal = $aResultFilterTotal[0];
	
	/* Total data set length */
	$sQuery = "
		SELECT COUNT(".$sIndexColumn.")
		FROM   `shipMethod`
		RIGHT JOIN (`customer` INNER JOIN (
		(`order` LEFT JOIN `qryFulfilmentListOrderPaid` ON `order`.`OrderID` = `qryFulfilmentListOrderPaid`.`OrderID`) 
		LEFT JOIN `qryFulfilmentListAllShipped` ON `order`.`OrderID` = `qryFulfilmentListAllShipped`.`OrderID`) 
		ON `customer`.`CustomerID` = `order`.`CustomerID`) 
		ON `shipMethod`.`ShipMethodID` = `order`.`ShipMethodID`
		WHERE (
		( (`qryFulfilmentListAllShipped`.`OrderID`) Is Null) AND ( (`shipMethod`.`ShipMethodID`)<>27) 
		)
	";
	$rResultTotal = $this->db->query($sQuery); //mysql_query( $sQuery, $gaSql['link'] ) or die(mysql_error());
	$aResultTotal = $rResultTotal->result_array();//mysql_fetch_array($rResultTotal);
	$iTotal = $aResultTotal[0];
	
	
	/*
	 * Output
	 */
	$output = array(
		"sEcho" => intval($_GET['sEcho']),
		"iTotalRecords" => $iTotal,
		"iTotalDisplayRecords" => $iFilteredTotal,
		"aaData" => array()
	);
	
	foreach (  $rResult->result_array() as $aRow )
	{
		$row = array();
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( $aColumns[$i] == "version" )
			{
				/* Special output formatting for 'version' column */
				$row[] = ($aRow[ $aColumns[$i] ]=="0") ? '-' : $aRow[ $aColumns[$i] ];
			}
			else if ( $aColumns[$i] != ' ' )
			{
				/* General output */
				$row[] = $aRow[ $aColumns[$i] ];
			}
		}
		$output['aaData'][] = $row;
	}
	
	echo json_encode( $output );


	}




	function postal_codes(){ 
		$row = array();
	    $return_arr = array();
	    $row_array = array();

	    if((isset($_GET['term']) && strlen($_GET['term']) > 0) || (isset($_GET['id']) && is_numeric($_GET['id'])))
	    {


	        if(isset($_GET['term']))
	        {
	            //$getVar = $db->real_escape_string($_GET['term']);
	            $getVar = $_GET['term'];
	            $whereClause =  " Postcode LIKE '" . $getVar ."%' ";
	        }
	        // elseif(isset($_GET['id']))
	        // {
	        //     $whereClause =  " categoryId = $getVar ";
	        // }
	        /* limit with page_limit get */

	        $limit = intval($_GET['page_limit']);

	        $sql = "SELECT Postcode FROM postcode WHERE $whereClause ORDER BY Postcode LIMIT $limit";

	        /** @var $result MySQLi_result */
	        $result = $this->db->query($sql);


	            if($result->num_rows > 0)
	            {

	                foreach($result->result() as $row)
	                {
	                    $row_array['id'] = $row->Postcode;
	                    $row_array['text'] = utf8_encode($row->Postcode);
	                    array_push($return_arr,$row_array);
	                }

	            }
	    }
	    else
	    {
	        $row_array['id'] = 0;
	        $row_array['text'] = utf8_encode('Start Typing....');
	        array_push($return_arr,$row_array);

	    }

	    $ret = array();
	    /* this is the return for a single result needed by select2 for initSelection */
	    if(isset($_GET['id']))
	    {
	        $ret = $row_array;
	    }
	    /* this is the return for a multiple results needed by select2
	    * Your results in select2 options needs to be data.result
	    */
	    else
	    {
	        $ret['results'] = $return_arr;
	    }

	    echo json_encode($ret);
	}
	    //$db->close();

}

?>