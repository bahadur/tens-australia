<?php
class api extends CI_Controller {


  function __construct() {

    parent::__construct();
    header('Content-Type: application/json'); 
    $this->load->library('Datatables');
    $this->load->library('httpresponses');
    $this->load->helper('ssp');
  }
 
 
  /**
   * JEditable for including/excluding a transactiondetail from payment
   * @return [type] [description]
   */
  public function updateTransactionDetail()
  { 
      if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator')))
      {
        $include = $this->input->post('value');
        $data['TempTransactionInclude']       = $include;
        $orderDetailId                = $this->input->post('orderDetailId'); 
        $query = $this->db->query("SELECT * FROM orderdetail WHERE orderdetail.OrderDetailID = ".$orderDetailId);
        if ($query->num_rows() > 0)
        {
          $this->db->where('OrderDetailID', $orderDetailId);
          if ($data['TempTransactionInclude'])
          {
            $result = $query->row(); 
            $data['TempTransactionAmount'] = $result->Quantity * $result->UnitPrice; 
            $this->db->update('orderdetail', $data);
            $this->httpresponses->respondJson('', 200);
          }
          else 
          {
            $data['TempTransactionAmount'] = null;
            $this->db->update('orderdetail', $data);
            $this->httpresponses->respondJson('', 200);
          } 
        } else {
          $this->httpresponses->respondJson('', 400);
        }
    }
    else 
    {
      echo "ERROR Not logged in";
    }
  }

  public function addKitToOrder() {
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator'))) {
      $requestBody = json_decode(file_get_contents('php://input'));  
      if ($requestBody->OrderID && $requestBody->KitID) {
        $this->load->library('orderdetails/productorderdetailfactory');  
        $kitProductsRes = $this->productorderdetailfactory->getAllProductsInAKit($requestBody->OrderID, $requestBody->KitID);
        $item = $kitProductsRes->getItem();
        $error = $kitProductsRes->getError();
        if ($item) {
          $kitProducts = $item;
          $orderdetails = [];
          foreach ($kitProducts as $kitProduct) {
            $saveRes = $kitProduct->save();
            if ($saveRes > 0) {
              $kitProduct->setOrderDetailId($this->db->insert_id()) ;
              array_push($orderdetails, $kitProduct->getJsonReadyArray());
            } else if ($saveRes < 0) {
              $this->httpresponses->respondJsonify(array('message' => 'Product '.$kitProduct->getProductId().' does not have an account code.'), 400);
            }
          } 
          if ($this->db->insert_batch("orderdetail",$orderdetails)) {
            $this->httpresponses->respondJsonify($orderdetails, 200);
          } else {
            $this->httpresponses->respondJsonify('Failed to batch insert.', 400);
          }
        } else {
          return $this->httpresponses->respondJsonify($error, 400); 
        }  
      } else {
        $this->httpresponses->respondJsonify('Invalid parameters.', 400);
      }
    } else {
      $this->httpresponses->respondJsonify('Unauthorized', 401);
    }
  }
 

  public function updateCancelReminders()
  {
        if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator')))
        { 
          $data['CancelReminderMessages'] = $this->input->post("value");
          $data['OrderDetailID'] = $this->input->post('orderDetailId');
          if ($data['OrderDetailID'] && $data['CancelReminderMessages'])
          {
            $this->db->where('OrderDetailID', $data['OrderDetailID']);
            if ($this->db->update('orderdetail', $data))
            {
              echo json_encode($data['CancelReminderMessages']);
            }
            else 
            {
              echo json_encode(array("message" => "Could not update reminder settings"));
            }
          }
          else 
          {
            echo json_encode(array("message" => "Could not update reminder settings - no data"));
          }
        }
  } 


  public function searchByBooking()
  { 
      if($this->input->is_ajax_request())
      { 
        $this->datatables->select("
          `order`.`RequiredDate` AS '0', 
          `order`.`InvoiceDate` AS '1', 
          `order`.`OrderID` AS '2', 
          CONCAT(`Firstname`, ' ', `Lastname`, ' (', `BusinessName`, ')')  AS '3', 
          `hospital`.`HospitalName` AS '4', 
          `shipmethod`.`ShipMethod` AS '5', 
          IF(SumofLineTotal = SumofAmount, 'Paid','Unpaid') AS '6', 
          `qryorderlistshippedstatus`.`ShippedStatus` AS '7', 
          `order`.`MobilePhone` AS '8', 
          `order`.`BillingSuburb` AS '9', 
          `order`.`BillingPostcode` AS '10', 
          `customerproblemflag`.`Flag`  AS '11',
          IF(IFNULL((SELECT COUNT(*) FROM `customernote` WHERE `order`.`OrderID` = `customernote`.`OrderID`), 0) = 0, 'No', 'Yes') AS '12' ", false)
          ->from('order')
          ->join('hospital','hospital.HospitalID = order.HospitalID','left')
          ->join('customer','customer.CustomerID = order.CustomerID','left')
          ->join('customerproblemflag',   'customer.CustomerProblemFlagID = customerproblemflag.CustomerProblemFlagID','left')
          ->join('shipmethod','shipmethod.ShipMethodID = order.ShipMethodID','left')
          ->join('qryfulfilmentlistorderpaid','order.OrderID = qryfulfilmentlistorderpaid.OrderID','left')
          ->join('qryorderlistshippedstatus', 'order.OrderID = qryorderlistshippedstatus.OrderID','left'); 
          echo $this->datatables->generate();
      }
  } 
 

  public function getKits()
  { 
    $this->load->library("orderdetails/kitfactory");
    $allKits = $this->kitfactory->getAllKits();
    $allKitsJson = array();
    foreach ($allKits as $kit)
    { 
      array_push($allKitsJson, $kit->getJsonReadyArray());
    }
    echo json_encode($allKitsJson); 
  } 

  public function getAllProductsJson($priceGroupId)
  { 
    $this->load->library("orderdetails/orderproductfactory"); 
    $allProducts = $this->orderproductfactory->getAllProducts($priceGroupId);
    $allProductsJson = array();
    foreach ($allProducts as $product)
    {
      array_push($allProductsJson, $product->getJsonReadyArray());
    }
    echo json_encode($allProductsJson); 
  }


   public function fulfilOrder()
   { 
    $orderId = $this->input->post("orderId"); 
    $fromLocationId = $this->input->post("fromLocationId");  
    $comments = $this->input->post("comments");   
    $time = date("Y-d-m h:i:s", time()); 
    $ORDER_FULFILLMENT = 2;
    if ($orderId)
    { 
       $this->db->trans_start();
          // 1. Step 1 - Create Stock Move and get the id 
       $stockmoveInsertData = array(
        "StockMoveTypeID" => $ORDER_FULFILLMENT, 
        "FromPurchaseOrderNo" => "",
        "MoveBy" => "",
        "FromLocationID" => $fromLocationId,
        "ToOrderID" => $orderId,
        "Comments" => $comments
        );  
       $this->db->set('MoveDate', 'NOW()', FALSE);
       if ($this->db->insert('stockmove', $stockmoveInsertData))
       {
        $intStockMoveID = $this->db->insert_id();
            // 2. Step 2 - Insert a bunch of stockmovedetails

        $selectQuery = "SELECT ".$intStockMoveID." AS StockMoveID, orderdetail.ProductID, orderdetail.OrderDetailID, 
        orderdetail.TempBarcode, orderdetail.TempPickQuantity FROM orderdetail WHERE OrderID = ".$orderId." 
        AND orderdetail.TempPickQuantity > 0;"; 
        $orderdetails = $this->db->query($selectQuery)->result();
        $batchData = array(); 
        foreach ($orderdetails as $row) 
        {
         array_push($batchData, array(
           "StockMoveID" => $intStockMoveID, 
           "ProductID" => $row->ProductID, 
           "OrderDetailID" => $row->OrderDetailID, 
           "Barcode" => $row->TempBarcode, 
           "Quantity" => $row->TempPickQuantity
           ));
         }  
         if ($this->db->insert_batch('stockmovedetail', $batchData))
         {
              // 3. Step 3. Update the order id of the productt in productbarcode and set location.
           $updateData = array(
            "productbarcode.LocationID" => null, 
            "productbarcode.OrderID" => $orderId
            );
           $this->db->where("stockmove.StockMoveID", $intStockMoveID);
           if ($this->db->update('stockmove INNER JOIN 
            (productbarcode INNER JOIN stockmovedetail ON productbarcode.Barcode = stockmovedetail.Barcode) 
            ON stockmove.StockMoveID = stockmovedetail.StockMoveID', $updateData))
           { 
                  // 4. Step 4 - Update the product quantities for non barcode products
            $updateQuantitiesData = array(
             "productlocation.Quantity" => "productlocation.Quantity - stockmovedetail.Quantity"
             );
            $this->db->where("stockmovedetail.StockMoveID", $intStockMoveID);
            if ($this->db->update('stockmove INNER JOIN 
             (stockmovedetail INNER JOIN productlocation ON stockmovedetail.ProductID = productlocation.ProductID) ON 
             (stockmove.FromLocationID = productlocation.LocationID) AND (stockmove.StockMoveID = stockmovedetail.StockMoveID)', $updateQuantitiesData))
            {
             echo "1";
             $this->db->trans_complete();
             return;
           }
         }
       }
     }
     $this->db->trans_rollback();
     echo "Error: ".$this->db->_error_message(); 
     echo "Error: ".$this->db->_error_number(); 
    } 
  } 

}