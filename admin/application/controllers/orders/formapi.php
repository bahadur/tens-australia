<?php
class formapi extends CI_Controller {

  function __construct() {

    parent::__construct(); 
    $this->load->library('Datatables');
    $this->load->library('httpresponses'); 
  }
 


  public function getOrderDetailsPaymentsDt($orderId) { 
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator'))) {  
      if ($orderId) {  
        $this->datatables->select('
          orderdetail.OrderDetailID, orderdetail.OrderID, product.ProductName, 
          qrypaymenttransactionsdetail.TransactionID, 
          qrypaymenttransactionsdetail.TransactionDate, UnitPrice * Quantity AS LineTotal, orderdetail.TempTransactionInclude, 
          orderdetail.TempTransactionAmount', false)
          ->from('orderdetail')
          ->join('qrypaymenttransactionsdetail','orderdetail.OrderDetailID=qrypaymenttransactionsdetail.OrderDetailID','left')
          ->join('product','product.ProductID=orderdetail.ProductID','inner')
          ->where('orderdetail.OrderID = '.$orderId);
          $this->httpresponses->respondJson($this->datatables->generate(), 200); 
      } else {
         $this->httpresponses->respondJson('Invalid parameters', 400);
      }
    } else {
       $this->httpresponses->respondJson('Unauthorized', 401);
    } 
  }

  public function searchByBooking()
  { 
      if($this->input->is_ajax_request())
      { 
        $this->datatables->select("
          `order`.`RequiredDate`| 
          `order`.`InvoiceDate`| 
          `order`.`OrderID`| 
          CONCAT(`Firstname`, ' ', `LastName`, ' (', `BusinessName`, ') ') AS `Customer`| 
          `hospital`.`HospitalName`| 
          `shipmethod`.`ShipMethod`| 
          IF(SumofLineTotal = SumofAmount, 'Paid','Unpaid') AS 'Paid'| 
          `qryorderlistshippedstatus`.`ShippedStatus`| 
          `order`.`MobilePhone`| 
          `order`.`BillingSuburb`| 
          `order`.`BillingPostcode`| 
          `customerproblemflag`.`Flag`|
          IF(IFNULL((SELECT COUNT(*) FROM `customernote` WHERE `order`.`OrderID` = `customernote`.`OrderID`), 0) = 0, 'No', 'Yes') AS 'Notes'", false, '|')
          ->from('order')
          ->join('hospital','hospital.HospitalID = order.HospitalID','left')
          ->join('customer','customer.CustomerID = order.CustomerID','left')
          ->join('customerproblemflag',   'customer.CustomerProblemFlagID = customerproblemflag.CustomerProblemFlagID','left')
          ->join('shipmethod','shipmethod.ShipMethodID = order.ShipMethodID','left')
          ->join('qryfulfilmentlistorderpaid','order.OrderID = qryfulfilmentlistorderpaid.OrderID','left')
          ->join('qryorderlistshippedstatus', 'order.OrderID = qryorderlistshippedstatus.OrderID','left'); 
          $this->datatables->generate();
          echo $this->datatables->last_query();
      }
  } 
 
}
