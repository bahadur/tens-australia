<?php
class vc extends CI_Controller { 

  function __construct(){ 
    parent::__construct();
    if (!$this->tank_auth->is_logged_in_as(array('staff', 'administrator')))
    { 
        redirect(base_url().'auth/login/');
    }
  }

  function modalOrderFulfilment(){ 
    if ($orderid = $this->input->post('orderid') && $productid = $this->input->post('productid')) {
      
      $query = $this->db->query("
          SELECT 
          orderdetail.OrderID, 
          product.ProductID,
          product.ProductName, 
          product.Hire, 
          orderdetail.Quantity, 
          orderdetail.TempBarcode, 
          orderdetail.TempPicked, 
          orderdetail.TempPickQuantity, 
          (SELECT count(Quantity) from  stockmovedetail WHERE OrderID = orderdetail.OrderID) AS AlreadyShipped,
          product.HasBarcodeID
          FROM product INNER JOIN orderDetail ON product.ProductID = orderDetail.ProductID
          WHERE 
          orderDetail.OrderID = $orderid AND 
          product.ProductID = $productid
          ORDER BY orderDetail.OrderDetailID"); 
      $data['product_detail'] = $query->row(); 
      $this->load->view('modals/order_fulfilment', $data);
    }
  }


	public function details($orderId){  
		$data['title']='Orders'; 
		$data['noBootstrap'] = true; 
		$orderDetails = $this->db->query("SELECT * FROM `order` WHERE `OrderID`=".$orderId)->result();
		if (is_null($orderDetails) || sizeof($orderDetails) == 0) {
			echo "This order doesn't exist";
		} else {	  
			$order = $orderDetails[0]; 
			$data['hospitalsChoices'] = $this->db->query("SELECT * FROM `hospital`")->result();
			$data['orderObject'] = $order;
			$data['educatorsChoices'] = $this->db->query("SELECT * FROM `educator`")->result();
			$data['shippingMethodsChoices'] = $this->db->query("SELECT * FROM `shipmethod`")->result();
			$data['orderSourcesChoices'] = $this->db->query("SELECT * FROM `ordersource`")->result();
			$data['priceGroupChoices'] = $this->db->query("SELECT * FROM `pricegroup`")->result();   
			  
			$data['stockmoverecords'] = $this->db->query(
				'SELECT orderdetail.OrderID, stockmovetype.StockMoveType, stockmove.MoveDate, 
				FromLocation.LocationDescription AS FromLocationDescription, stockmove.FromOrderID, ToLocation.LocationDescription AS `To`, 
				stockmove.ToOrderID, stockmove.Comments, product.ProductName, product.HasBarcodeID, stockmovedetail.Barcode, 
        stockmovedetail.Quantity FROM stockmovetype INNER JOIN ((location AS ToLocation RIGHT JOIN (location AS FromLocation RIGHT JOIN stockmove 
					ON FromLocation.LocationID=stockmove.FromLocationID) ON ToLocation.LocationID=stockmove.ToLocationID) 
				INNER JOIN (product INNER JOIN (stockmovedetail INNER JOIN orderdetail 
					ON stockmovedetail.OrderDetailID=orderdetail.OrderDetailID) ON product.ProductID=stockmovedetail.ProductID) 
				ON stockmove.StockMoveID=stockmovedetail.StockMoveID) ON stockmovetype.StockMoveTypeID=stockmove.StockMoveTypeID 
				 WHERE orderdetail.OrderID = '.$orderId.' GROUP BY orderdetail.OrderID, stockmovetype.StockMoveType, stockmove.MoveDate, 
				FromLocation.LocationDescription, stockmove.FromOrderID, ToLocation.LocationDescription, 
				stockmove.ToOrderID, stockmove.Comments, product.ProductName, stockmovedetail.Barcode, 
				stockmovedetail.Quantity, stockmove.MoveBy;')->result();  
			$data['smshistory'] = $this->db->query('SELECT orderdetail.OrderID, remindersent.ReminderID, remindersent.SentDateTime 
				FROM orderdetail INNER JOIN (reminder INNER JOIN remindersent ON reminder.ReminderID=remindersent.ReminderID) 
				ON orderdetail.OrderDetailID=remindersent.OrderDetailID WHERE orderdetail.OrderID = '.$orderId.'; ')->result();  
 			$customerId = $order->CustomerID;  
			$customerSelected = $this->db->query("SELECT * FROM `customer` WHERE CustomerID = ".$customerId)->result();  
			$data['customerSelected'] = $customerSelected[0]; 
			
		  $this->load->view('header',$data);  
    	$this->load->view('navbar');
      $this->load->view('order/orderdetails'); 
      $this->load->view('footer-no-js'); 
    } 
		
	}


	public function index()
	{  
	}
	
	public function newOrder()
	{ 
		$data['title']='Orders';

		$data['customers'] = $this
              ->db->order_by("LastName, Firstname, BusinessName, MobilePhone, ShippingAddress, ShippingSuburb, ShippingState, ShippingPostcode")
							->get("customer")->result();
		
		$dataFromHospitalSql = "SELECT * FROM hospital ORDER BY HospitalName ASC";
		$dataFromHospitalQuery = $this->db->query($dataFromHospitalSql);
		$dataFromHospitalSqlResult = $dataFromHospitalQuery->result();
		$data['hospitals'] = $dataFromHospitalSqlResult ;
		
		$dataFromPriceGroupsSql = "SELECT * FROM pricegroup ORDER BY PriceGroup ASC";
		$dataFromPriceGroupsQuery = $this->db->query($dataFromPriceGroupsSql);
		$dataFromPriceGroupsResult = $dataFromPriceGroupsQuery->result();
		$data['pricegroups'] = $dataFromPriceGroupsResult;
		

		$data['educators'] = $this->db->get("educator")->result();
		
		$dataFromShippingMethodSql = "SELECT * FROM shipmethod ORDER BY ShipMethod ASC";
		$dataFromShippingMethodQuery = $this->db->query($dataFromShippingMethodSql);
		$dataFromShippingMethodResult = $dataFromShippingMethodQuery->result();
		$data['shipmethods'] = $dataFromShippingMethodResult; 
		$this->load->view('header',$data); 
    $this->load->view('navbar'); 
    $this->load->view('order/new-order');  
    $this->load->view('footer'); 
	}

 

 	public function pickorder($orderId)
 	{ 
		$data['title']='Orders'; 
		$inforesultsQ = $this->db->query(
			"SELECT `order`.OrderID, `order`.OrderDate, CONCAT(Firstname, \" \", LastName, \" (\", BusinessName, \")\") AS CustomerName, 
			CONCAT(`order`.ShippingAddress, \", \", `order`.ShippingSuburb, \", \", `order`.ShippingState, \" \", `order`.ShippingPostcode, \", \", `order`.ShippingCountry) AS Address, 
				`order`.HomePhone, `order`.MobilePhone, `order`.BusinessPhone, `order`.Email, `order`.InvoiceDate FROM Customer INNER JOIN `order` 
				ON customer.CustomerID = `order`.CustomerID WHERE `order`.OrderID=".$orderId);  
		$locationsResultsQ = $this->db->query(
			"SELECT location.LocationID, location.LocationDescription FROM location WHERE (((location.PickStock)=True)) 
			ORDER BY location.LocationDescription;");  
		$productsFulfilledQ = $this->db->query(
			"SELECT firstOrderDetail.OrderID, product.ProductName, product.Hire, firstOrderDetail.Quantity, firstOrderDetail.TempBarcode, 
			firstOrderDetail.TempPicked, firstOrderDetail.TempPickQuantity, (SELECT SUM(Quantity) FROM orderdetail AS otherOrderDetail 
			WHERE otherOrderDetail.OrderDetailID = firstOrderDetail.OrderDetailID) AS AlreadyShipped, product.HasBarcodeID 
			FROM product INNER JOIN orderdetail as firstOrderDetail ON product.ProductID=firstOrderDetail.ProductID 
			WHERE firstOrderDetail.OrderID=".$orderId." ORDER BY firstOrderDetail.OrderDetailID");
    if ($productsFulfilledQ && $locationResultsQ && $inforesultsQ) {
      $info = $inforesultsQ[0];
      $data['shipoutinfo'] = $info;
      $data['locations'] = $locationResultsQ->result(); 
      $data['productsFulfilled'] = $productsFulfilledQ->result();
      $this->load->view('header',$data);  
      $this->load->view('navbar');  
      $this->load->view('shipout/pickorder');  
      $this->load->view('footer-no-js'); 
    }
 	}

 
 	public function updateOrderDetail()
 	{  
        if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator')))
        {
	        $orderDetailID 				= $this->input->post('orderDetailId'); 
	        if ($orderDetailID)
	        {
		        $data['UnitPrice']   		= $this->input->post('price');
		        $data['Quantity']    		= $this->input->post('quantity');
		        $data['ProductID']   		= $this->input->post('productChoice');
		        $data['ReturnExpectedDate'] = date('Y-m-d', strtotime($this->input->post('returnExpectedDate')));
		        if ($this->input->post('cancelSms'))
		        {
		        	$data['CancelReminderMessages'] = 1;
		        }
		        else 
		        {
		        	$data['CancelReminderMessages'] = 0;
		        }
		        $data['returnExpectedDate']  = $this->input->post('returnExpectedDate'); 
		        // Do an update  
		        $this->db->where('OrderDetailID', $orderDetailID);
		        $this->db->update('orderdetail', $data);
		        $query = $this->db->query("SELECT * FROM orderdetail WHERE orderdetail.OrderDetailID=".$orderDetailID);
		        if ($query->num_rows() > 0)
		        {
		        	redirect(base_url() . 'orders/vc/details/' . $query->row()->OrderID);
		        }
		    }
		}
 	}

 	public function updateOrderGeneralDetails()
 	{ 
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator')))
    { 
			$this->load->library('projectdates');  

	 		$orderId = $this->input->post('OrderId'); 

 			$data['OrderDate'] = $this->projectdates->parseToMySQLDate($this->input->post('ApplicationDate')); 
      if ($this->input->post('DueDate')) {
        $data['DueDate'] = $this->projectdates->parseToMySQLDate($this->input->post('DueDate'));  
      }
      if ($this->input->post('RequiredDate')) {
        $data['RequiredDate'] = $this->projectdates->parseToMySQLDate($this->input->post('RequiredDate')); 
      }
 
	 		$data['PriceGroupID'] = $this->input->post('PriceGroup');
	 		$data['OrderSourceID'] = $this->input->post('OrderSource');
	 		$data['CustomerReference'] = $this->input->post('CustomerRef');
	 		$data['HospitalID'] = $this->input->post('Hospital');
	 		$data['EducatorID'] = $this->input->post('Educator');
	 		$data['ShipMethodID'] = $this->input->post('ShippingMethod');
	 		$data['Pharmacy'] = $this->input->post('Pharmacy'); 
	 		$this->db->trans_start();
	 		$this->db->where('OrderID', $orderId);
	 		$this->db->update('order', $data);
			$this->db->trans_complete();  
	 		if ($this->db->trans_status() === FALSE)
	 		{ 
	 			 echo "Could not update the order";
			}
			else 
			{ 
	    		redirect(base_url() . 'orders/vc/details/' . $orderId);
			}
		}
 	}

 	public function updateOrderContactDetails()
 	{   
        if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator')))
        {
	 		$orderId = $this->input->post('ContactDetailsOrderID');
	 		$data['BillingAddress'] = $this->input->post('BillingAddress');
	 		$data['BillingSuburb'] = $this->input->post('BillingSuburb');
	 		$data['BillingState'] = $this->input->post('BillingState');
	 		$data['BillingPostcode'] = $this->input->post('BillingPostcode');
	 		$data['BillingCountry'] = $this->input->post('BillingCountry'); 
	 		if ($this->input->post('ShippingBillingSame') == null)
	 		{
	 			$data['ShippingSameAsBilling'] = 0;
		 		$data['ShippingAddress'] = $this->input->post('ShippingAddress');
		 		$data['ShippingSuburb'] = $this->input->post('ShippingSuburb');
		 		$data['ShippingState'] = $this->input->post('ShippingState');
		 		$data['ShippingPostcode'] = $this->input->post('ShippingPostcode');
		 		$data['ShippingCountry'] = $this->input->post('ShippingCountry');
	 		}
	 		else 
	 		{
	 			$data['ShippingSameAsBilling'] = 1;
		 		$data['ShippingAddress'] = $this->input->post('BillingAddress');
		 		$data['ShippingSuburb'] = $this->input->post('BillingSuburb');
		 		$data['ShippingState'] = $this->input->post('BillingState');
		 		$data['ShippingPostcode'] = $this->input->post('BillingPostcode');
		 		$data['ShippingCountry'] = $this->input->post('BillingCountry');
	 		}
	 		$data['HomePhone'] = $this->input->post('HomePhone');
	 		$data['MobilePhone'] = $this->input->post('MobilePhone');
	 		$data['Fax'] = $this->input->post('Fax');
	 		$data['Email'] = $this->input->post('Email'); 
	 		$data['BusinessPhone'] = $this->input->post('BusinessPhone');   
	 		$this->db->trans_start();
	 		$this->db->where('OrderID', $orderId);
	 		$this->db->update('order', $data);
			$this->db->trans_complete(); 
	 		if ($this->db->trans_status() === FALSE)
	 		{
				$this->session->set_flashdata('message_name', 'Order could not be updated.');
			}
			else 
			{
				$this->session->set_flashdata('message_name', 'Order has been updated.');
	    		redirect(base_url() . 'orders/vc/details/' . $orderId);
			}
		}
 	}



 	public function saveTransactionOld()
 	{  
        if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator')))
        { 
			$data['OrderID'] = $this->input->post('orderId');
	        $data['TransactionTypeID'] = $this->input->post('transactionTypeId');
	        $data['PaymentMethodID'] = $this->input->post('paymentMethodId');
	        $data['TransactionDate'] = date('m/d/Y h:i:s a', time()); 
	        $data['TransactionTotal'] = $this->input->post('transactionTotal');
	        $data['TransactionReference'] = $this->input->post('transactionReference'); 
	        if (isset($data['PaymentMethodID']))
	        {
				$query = $this->db->query("SELECT paymentterminal.PaymentMethodID, paymentterminal.TerminalID 
			                        FROM (paymentterminal INNER JOIN paymentmethod ON paymentterminal.PaymentMethodID = paymentmethod.PaymentMethodID) 
			                        INNER JOIN terminal ON paymentterminal.TerminalID = terminal.TerminalID 
									WHERE paymentterminal.PaymentMethodID = ".$data['PaymentMethodID']."
			                        ORDER BY paymentterminal.PaymentTerminalID DESC , 
		 	                       paymentterminal.TerminalID ");
				if ($query->num_rows() > 0)
				{
					$result = $query->result();
					$data['TerminalID'] = $result[0]->TerminalID; 
			        $this->db->trans_start();  
	        		$this->db->insert('transaction', $data);
			        $transactionId = $this->db->insert_id(); 
			        $selectQuery = "SELECT ".$transactionId." AS TransactionID, orderdetail.OrderDetailID, orderdetail.TempTransactionAmount,
			        	productaccountcode.AccountCode, orderdetail.GST_Amount FROM orderdetail INNER JOIN productaccountcode ON
			        	orderdetail.ProductID = productaccountcode.ProductID 
			        	WHERE (((productaccountcode.TransactionTypeID) = ".$data['TransactionTypeID'].")
			        	AND (orderdetail.TempTransactionInclude = True) AND (orderdetail.OrderID = ".$data['OrderID']."))"; 
					$query = $this->db->query($selectQuery); 
					if ($query->num_rows() > 0)
					{ 
						$batchData = array();
						$result = $query->result();
						foreach ($result as $row)
						{
							array_push($batchData, array(
									"TransactionID" => $row->TransactionID, 
									"OrderDetailID" => $row->OrderDetailID, 
									"Amount" => $row->TempTransactionAmount, 
									"AccountCode" => $row->AccountCode, 
									"GST_Amount" => $row->GST_Amount
								));
						} 
						$this->db->insert_batch('transactiondetail', $batchData);
						$this->db->trans_complete();
				    }
				    else 
				    {
				    	echo "Rollingback!";
				    	$this->db->trans_rollback();
				    }
				}
			}
		}
 
 	}
 	/**
 	* Delete APIs
 	*/

 	public function deleteOrderDetail()
 	{
        
 		
        if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator')))
        {
	        $orderDetailID 	= $this->input->post('orderDetailId'); 
	        $query = $this->db->query("SELECT * FROM orderdetail WHERE orderdetail.OrderDetailID=".$orderDetailID);
	        if ($query->num_rows() > 0)
	        {
		        $orderId = $query->row()->OrderID;
		        $this->db->where('OrderDetailID',$orderDetailID);
		        $this->db->delete('orderdetail');
	        	//redirect(base_url() . 'orders/vc/details/' . $orderId);
	        }
	    }
 	}
 
 	/**
 	*	INSERT APIs
 	*/



 	/*  
 	 * Default source is manual
 	 */
 	public function createOrder()
 	{       
    $this->load->library('projectdates');
 		$sourceId = $this->input->post('orderSourceId') == null ? 1 : $this->input->post('orderSourceId'); 
 		$customerId = $this->input->post('customerId');
    $customerQ = $this->db->query('SELECT * FROM customer WHERE CustomerID = '. $customerId); 
    if ($customerQ && $customerQ->num_rows() > 0) {
      $customerR = $customerQ->result();
      $customer = $customerR[0];
      $data['CustomerID'] = $customerId;
      $data['HospitalID'] = $this->input->post('hospitalId');
      $data['PriceGroupID'] = $customer->PriceGroupID;
      $data['EducatorID'] = $this->input->post('educatorId');
      $data['Pharmacy'] = $this->input->post('pharmacyName');
      $data['OrderSourceID'] = $sourceId;
      $nowDate = $this->projectdates->mysqlNow();
      $data['OrderDate'] = $nowDate; 
      $data['RequiredDate'] = $nowDate; 
      $data['CreatedDate'] = $nowDate; 
      $data['CreatedBy'] = $this->tank_auth->get_username(); 
      $data['ShippingAddress'] = $customer->ShippingAddress; 
      $data['ShippingSuburb'] = $customer->ShippingSuburb; 
      $data['ShippingPostcode'] = $customer->ShippingPostcode; 
      $data['ShippingCountry'] = $customer->ShippingCountry; 
      $data['ShippingState'] = $customer->ShippingState; 
      $data['BillingAddress'] = $customer->BillingAddress; 
      $data['BillingSuburb'] = $customer->BillingSuburb; 
      $data['BillingPostcode'] = $customer->BillingPostcode; 
      $data['BillingCountry'] = $customer->BillingCountry; 
      $data['BillingState'] = $customer->BillingState; 
      $data['ShippingSameAsBilling'] = $customer->ShippingSameAsBilling; 
      $data['HomePhone'] = $customer->HomePhone; 
      $data['Fax'] = $customer->Fax; 
      $data['MobilePhone'] = $customer->MobilePhone; 
      $data['BusinessPhone'] = $customer->BusinessPhone; 
      $data['Email'] = $customer->Email; 
      if ($this->db->insert('order',$data))
      { 
        $lastPrimaryKey = $this->db->insert_id();
        /*if ($this->input->post('unconfirmedOrderId'))
        { 
          $updateData['Confirmed'] = true;
          $this->db->where('UOrderID', $this->input->post('unconfirmedOrderId'));
          $this->db->update('unconfirmedorders', $updateData);
        }*/
        redirect(base_url() . 'orders/vc/details/' . $lastPrimaryKey); 
      } else {
        echo 'Failed to insert a new order.';
      }
    } else {
      echo 'No customer.';
    }
 	}
 

	 public function createCustomerNote()
	 {   
      if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator')))
      {
          $this->load->library('projectdates');  
	        $data['CustomerID'] = $this->input->post('NewNoteCustomerId');
	        $data['OrderID'] = $this->input->post('NewNoteOrderId');
	        $data['DateTime'] = $this->projectdates->parseToMySQLDate($this->input->post('NewNoteDatetime'));
	        $data['StaffID'] = $this->input->post('NewNoteStaff');
	        $data['CustomerProblemFlagID'] = $this->input->post('NewNoteProblems');
	        $data['Note'] = $this->input->post('NewNoteNotes');   
	        $this->db->insert('customernote', $data);  
	    	  redirect(base_url() . 'orders/vc/details/' . $data['OrderID']);
	    }
	 }




	/**
	*	SELECT APIs
	*/

	public function getOrderRefundItems($orderId)
	{
        if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator')))
        { 
	        	$productsQueryString = "SELECT orderdetail.OrderID, product.ProductName, OrderDetailID, orderdetail.UnitPrice,
	        		(SELECT Sum(qryFulfilmentListQtyShipped.ShippedQuantity) AS Quantity FROM qryFulfilmentListQtyShipped WHERE OrderDetailID = orderdetail.OrderDetailID) AS ShippedQty, 
	        		(SELECT Sum(qryHireReturnQty.Quantity) AS Quantity FROM qryHireReturnQty WHERE OrderDetailID = orderdetail.OrderDetailID) AS ReturnQty, 
	        		orderdetail.ReturnActualDate, orderdetail.TempRefundQuantity, orderdetail.UnitPrice, orderdetail.Hire, orderdetail.TempTransactionAmount, 
	        		IF(IFNULL(ShowRefundButton, 0) = 0 ,'','Refund') AS RefundDeposit FROM product 
	        		INNER JOIN orderdetail ON product.ProductID = orderdetail.ProductID 
	        		WHERE (((orderdetail.RefundProcessedDate) IS Null) AND (orderdetail.OrderID = ".$orderId.")) ORDER BY orderdetail.OrderDetailID";
	        	$productsQueryResult = $this->db->query($productsQueryString)->result();
        	$resultJson = array(
				"data" => $productsQueryResult
				);
			echo json_encode($resultJson);
    	}
	}

 

	/* Question - how to accept it in POST body */
	public function getProductsInKitTable($orderId = 0, $date = 0, $kitId = 0, $priceGroupId = 0) 
	{
        if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator')))
        {
			$this->load->library('orderdetails/productsinkitfactory');  
			$allProductsInKit = $this->productsinkitfactory->getAllProductsInAKit($orderId, $date, $kitId, $priceGroupId);  
			$dataArray = array();
			foreach ($allProductsInKit as $product)
			{ 
				array_push($dataArray, $product->getJsonReadyArray());
			}
			$allProductsInKitJson = array(
				'data' => $dataArray
				);
			echo json_encode($allProductsInKitJson);
		}
	} 



    public function getAllOrderDetails($orderId)
    {
        if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator')))
        {
			$this->load->library('orderdetails/productorderdetailfactory');
	        $queryString = "SELECT orderdetail.*, product.ProductName FROM orderdetail INNER JOIN product 
	        		ON orderdetail.ProductId = product.ProductId WHERE orderdetail.OrderID = ".$orderId." ORDER BY orderdetail.OrderDetailID; ";

	        $orderDetailsResults = $this->db->query($queryString)->result();  
	        $allOrderDetails = array();
	     	foreach ($orderDetailsResults as $orderDetailResult)
	     	{
	     		array_push($allOrderDetails, $this->productorderdetailfactory->createObjectFromData($orderDetailResult));
	     	}   
	        return $allOrderDetails;
	    }
    }
}



/*
  public function getAllProducts($priceGroupId)
  {  
    $this->load->library("orderdetails/orderproductfactory"); 
    return $this->orderproductfactory->getAllProducts($priceGroupId); 
  }


  public function getKits()
  { 
    $this->load->library("orderdetails/kitfactory");
    $allKits = $this->kitfactory->getAllKits();
    $allKitsJson = array();
    foreach ($allKits as $kit)
    { 
      array_push($allKitsJson, $kit->getJsonReadyArray());
    }
    echo json_encode($allKitsJson); 
  } 
  public function getProductsInKit($orderId = 0, $date = 0, $kitId = 0, $priceGroupId = 0) 
  {
        if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator')))
        {
      $this->load->library('orderdetails/productsinkitfactory');  
      $allProductsInKit = $this->productsinkitfactory->getAllProductsInAKit($orderId, $date, $kitId, $priceGroupId);  
      $dataArray = array();
      foreach ($allProductsInKit as $product)
      { 
        array_push($dataArray, $product->getJsonReadyArray());
      } 
      return json_encode($dataArray);
    }
  }
 
  public function getAllProductsJson($priceGroupId)
  { 
    $this->load->library("orderdetails/orderproductfactory"); 
    $allProducts = $this->orderproductfactory->getAllProducts($priceGroupId);
    $allProductsJson = array();
    foreach ($allProducts as $product)
    {
      array_push($allProductsJson, $product->getJsonReadyArray());
    }
    echo json_encode($allProductsJson); 
  }

  public function getProduct($priceGroupId, $productId)
  {
        if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator')))
        {
      $this->load->library("orderdetails/orderproductfactory"); 
      $allProducts = $this->orderproductfactory->getAllProducts($priceGroupId);
      foreach ($allProducts as $product)
      {
        if ($product->getProductId() === $productId)
        {
          echo json_encode($product->getJsonReadyArray());
          return;
        }
      }
      return;
    }
  }


  public function getProductsInKitArray($orderId = 0, $date = 0, $kitId = 0, $priceGroupId = 0) 
  {
        if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator')))
        {
      $dataArray = array();
      foreach ($allProductsInKit as $product)
      {
        array_push($dataArray, $product->getDatatablesArray());
      }
      $allProductsInKitJson = array(
        'data' => $dataArray
        );
      echo json_encode($allProductsInKitJson);
    }
  }
*/