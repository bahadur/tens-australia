<?php 


class api extends CI_Controller {


	function __construct(){ 
		parent::__construct(); 
		$this->load->helper('ssp'); 
	}

	function getUnconfirmedOrderItems()
	{ 
        $table = 'unconfirmedorderdetails';
        $primaryKey = 'UOrderDetailID'; 

        $columns = array(
            array(
                'db' => 'UOrderDetailID',
                'dt' => 0,
                'table' => $table,
                'alias' => 'UOrderDetailID'
                ),
            array(
                'db' => "CONCAT(Firstname,' ', LastName)",
                'dt' => 1,
                'table' => "",
                'alias' => 'Customer'
                ),
            array(
                'db' => 'ApplicationDate',
                'dt' => 2,
                'table' => $table,
                'alias' => 'ApplicationDate'
                ),
            array(
                'db' => "RequiredDate",
                'dt' => 3,
                'table' => $table,
                'alias' => 'RequiredDate'
                ),
            array(
                'db' => "DueDate",
                'dt' => 4,
                'table' => $table,
                'alias' => 'DueDate'
                )
            );
        $items = simple(
           $_GET, 
           $table,
           $primaryKey, 
           $columns, 
           "","", ""  
           );
        $items = null;
        if ($items) {
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($items));
        } else {
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(null));
        }
    }


    function getUnconfirmedOrders()
    { 
        try {
            $table = 'unconfirmedorders';
            $primaryKey = 'UOrderID'; 

            $columns = array(
                array(
                    'db' => 'UOrderID',
                    'dt' => 0,
                    'table' => $table,
                    'alias' => 'UOrderID'
                    ),
                array(
                    'db' => 'Confirmed',
                    'dt' => 1,
                    'table' => $table,
                    'alias' => 'Status'
                    ),
                array(
                    'db' => "CONCAT(Firstname,' ', LastName)",
                    'dt' => 2,
                    'table' => "",
                    'alias' => 'Customer'
                    ),
                array(
                    'db' => 'ApplicationDate',
                    'dt' => 3,
                    'table' => $table,
                    'alias' => 'ApplicationDate'
                    ),
                array(
                    'db' => "RequiredDate",
                    'dt' => 4,
                    'table' => $table,
                    'alias' => 'RequiredDate'
                    ),
                array(
                    'db' => "DueDate",
                    'dt' => 5,
                    'table' => $table,
                    'alias' => 'DueDate'
                    )
                );    
            $items = simple(
               $_GET, 
               $table,
               $primaryKey, 
               $columns, 
               "","", "Confirmed = 'Unconfirmed'"  
               );
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($items));
        } catch (Exception $e) {
            log_message('error', $e);
            $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(array( 'message' => 'API call error.')));
        }
    }

}