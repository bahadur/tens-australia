<?php
class vc extends CI_Controller {

  public function index()
  {
    $data['title'] = "Confirm Orders";
    $this->load->view('header',$data); 
    $this->load->view('navbar'); 
    $this->load->view('confirmOrders/confirmOrders'); 
    $this->load->view('footer'); 
  }

  public function confirmOrderDetails($uOrderId)
  {  
    $this->db->where("UOrderID", $uOrderId);
    $querySql = $this->db->get("unconfirmedorders"); 
    if ($querySql && $querySql->num_rows() > 0)
    {
      $res = $querySql->result();
      $data['title'] = "Confirm Order View";
      $data['order'] = $res[0];
      $this->db->where("UOrderID", $uOrderId);
      $this->db->join('product', 'product.ProductID = unconfirmedorderitems.ProductID');
      $data['orderItems'] = $this->db->get("unconfirmedorderitems")->result();
      $this->load->view('header',$data); 
      $this->load->view('navbar'); 
      $this->load->view('confirmOrders/confirmOrderDetails'); 
      $this->load->view('footer'); 
    }
    else 
    {
      log_message('error', '404 page.');
      show_404('page', 'Cannot open the page');
    }
  } 
}