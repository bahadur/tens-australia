<?php
class formapi extends CI_Controller {


  function __construct(){ 
    parent::__construct();  
    $this->load->library('Datatables');
    $this->load->library('httpresponses'); 
    $this->load->library('projectdates');
  }



  public function updateTempNoninvBarcode() {  
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator'))) { 
      $tempBarcode = $this->input->post('value');
      $orderDetailId = $this->input->post('OrderDetailID'); 
      if ($orderDetailId && $tempBarcode) {  
        $data['TempBarcode'] = $tempBarcode;
        $data['TempPicked'] = 1;
        $data['TempPickQuantity'] = 1;
        $this->db->where('OrderDetailID', $orderDetailId);
        if ($this->db->update('orderdetail', $data)) {
          $this->httpresponses->respondJson($tempBarcode, 200);
        } else {
          $this->httpresponses->respondJsonify(array('message' => 'Update db error'), 400);
        }  
      } else  {
        $this->httpresponses->respondJsonify(array('message' => 'Invalid parameters'), 400);
      } 
    } else {
      $this->httpresponses->respondJsonify(array('message' => 'Unauthorized'), 401);
    }
  } 



}