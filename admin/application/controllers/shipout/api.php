<?php
class api extends CI_Controller {


  function __construct(){ 
    parent::__construct(); 
    header('Content-Type: application/json'); 
    $this->load->library('Datatables');
    $this->load->library('httpresponses'); 
    $this->load->library('projectdates');
  }


   public function fulfillOrder()
   { 
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator'))) { 
      $req = json_decode(file_get_contents('php://input')); 
      if ($req->OrderID != null && $req->FromLocationID != null) {  
        $orderId = $req->OrderID; 
        $fromLocationId = $req->FromLocationID;  
        $comments = isset($req->Comments) ? $req->Comments : "";   
        $time = $this->projectdates->mysqlNow(); 
        $ORDER_FULFILLMENT = 2; 
        $stockmoveInsertData = array(
          "StockMoveTypeID" => $ORDER_FULFILLMENT, 
          "FromPurchaseOrderNo" => "",
          "MoveBy" => "",
          "FromLocationID" => $fromLocationId,
          "ToOrderID" => $orderId,
          "Comments" => $comments
          );  
        $this->db->set('MoveDate', 'NOW()', FALSE);
        if ($this->db->insert('stockmove', $stockmoveInsertData)) {
          $intStockMoveID = $this->db->insert_id(); 
          $selectQuery = "SELECT ".$intStockMoveID." AS StockMoveID, orderdetail.ProductID, orderdetail.OrderDetailID, 
          orderdetail.TempBarcode, orderdetail.TempPickQuantity FROM orderdetail WHERE OrderID = ".$orderId." 
          AND orderdetail.TempPickQuantity > 0;"; 
          $orderdetails = $this->db->query($selectQuery)->result();
          $batchData = array(); 
          foreach ($orderdetails as $row) 
          {
           array_push($batchData, array(
             "StockMoveID" => $intStockMoveID, 
             "ProductID" => $row->ProductID, 
             "OrderDetailID" => $row->OrderDetailID, 
             "Barcode" => $row->TempBarcode, 
             "Quantity" => $row->TempPickQuantity
             ));
           }  
           if ($this->db->insert_batch('stockmovedetail', $batchData)) { 
             $updateData = array(
              "productbarcode.LocationID" => null, 
              "productbarcode.OrderID" => $orderId
              );
             $this->db->where("stockmove.StockMoveID", $intStockMoveID);
             if ($this->db->update('stockmove INNER JOIN 
              (productbarcode INNER JOIN stockmovedetail ON productbarcode.Barcode = stockmovedetail.Barcode) 
              ON stockmove.StockMoveID = stockmovedetail.StockMoveID', $updateData)) {  
                $updateQuantitiesData = array(
                 "productlocation.Quantity" => "productlocation.Quantity - stockmovedetail.Quantity"
                 );
                $this->db->where("stockmovedetail.StockMoveID", $intStockMoveID);
                if ($this->db->update('stockmove INNER JOIN (stockmovedetail INNER JOIN productlocation ON stockmovedetail.ProductID = productlocation.ProductID) ON 
                 (stockmove.FromLocationID = productlocation.LocationID) AND (stockmove.StockMoveID = stockmovedetail.StockMoveID)', $updateQuantitiesData)) {
                    $this->httpresponses->respondJsonify(array('success' => true), 200);
                    return;
                } else {
                  $this->httpresponses->respondJsonify(array('message' => 'Could not create stock move data.'), 400);
                }
             } else {
                $this->httpresponses->respondJsonify(array('message' => 'Could not create stock move data.'), 400);
             }
         } else {
            $this->httpresponses->respondJsonify(array('message' => '- Could not create stock move data.'), 400);
         }
       } else {
        $this->httpresponses->respondJsonify(array('message' => '-- Could not create stock move data.'), 400);
       }  
      } else {
        $this->httpresponses->respondJsonify(array('message' => 'Invalid parameters.'), 400);
      }
    } else {
      $this->httpresponses->respondJsonify(array('message' => 'Unauthorized.'), 401);
    }
  } 

  public function getOrderDetails() {
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator'))) { 
      $req = json_decode(file_get_contents('php://input'));
      if ($req->OrderID) {

        $this->datatables->select("orderdetail.OrderID, product.ProductID, product.ProductName, product.Hire, orderdetail.Quantity, 
            orderdetail.TempBarcode, orderdetail.TempPicked, orderdetail.TempPickQuantity, orderdetail.OrderDetailID,
            (SELECT count(Quantity) FROM stockmovedetail WHERE stockmovedetail.OrderDetailID = orderdetail.OrderDetailID) AS AlreadyShipped,
            product.HasBarcodeID", false)
          ->from('product')
          ->join('orderdetail','product.ProductID = orderdetail.ProductID')
          ->where('orderdetail.OrderID = '.$req->OrderID.'');
        $this->httpresponses->respondJson($this->datatables->generate(), 200); 
      } else {
        $this->httpresponses->respondJson('Invalid parameters', 400);
      }
      return;
    } else {
      $this->httpresponses->respondJson('Unauthorized', 401);
    }
  }
 

  public function updateTempPicked() {  
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator'))) {
      $orderdetailId = $this->input->post('OrderDetailID');  
      $tempPicked = $this->input->post('value');
      if ($orderdetailId != null && $tempPicked != null) {
        if ($tempPicked)
        {
          $data['TempPickQuantity'] = 0;
        }
        else 
        {
          $data['TempPickQuantity'] = NULL;
        }
        $data['TempPicked'] = $tempPicked; 
        $this->db->where("OrderDetailID", $orderdetailId); 
        if ($this->db->update("orderdetail", $data)) {
          $this->httpresponses->respond('', 200);
        } else {
          $this->httpresponses->respondJsonify(array('message' => 'Cannot update orderdetail.'), 400);
        } 
      } else {
        $this->httpresponses->respondJsonify(array('message' => 'Invalid parameters.'), 400);
      } 
    } else {
      $this->httpresponses->respondJsonify(array('message' => 'Unauthorized.'), 401);
    }
  }

  public function updateTempQuantity() { 
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator'))) {
      $orderdetailId = $this->input->post('OrderDetailID');  
      $tempQuantity = $this->input->post('value');
      if ($orderdetailId != null && $tempQuantity != null && is_numeric($tempQuantity)) {
        $data['TempPickQuantity'] = $tempQuantity; 
        $this->db->where("OrderDetailID", $orderdetailId); 
        if ($this->db->update("orderdetail", $data)) {
          $this->httpresponses->respond('', 200);
        } else {
          $this->httpresponses->respondJsonify(array('message' => 'Cannot update orderdetail.'), 400);
        } 
      } else {
        $this->httpresponses->respondJsonify(array('message' => 'Invalid parameters.'), 400);
      }
    } else {
      $this->httpresponses->respondJsonify(array('message' => 'Unauthorized.'), 401);
    }
  }


 
  /**
   * JEditable API.
   * @return [type] [description]
   */
  public function updateTempBarcode() {  
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator'))) { 
      $response = file_get_contents('php://input');
      $keyValues = explode('&', $response);
      $jsonReq = array();
      foreach ($keyValues as $keyValue) {
        $kvSplit = explode('=', $keyValue);
        $jsonReq[$kvSplit[0]] = $kvSplit[1]; 
      } 
      if (isset($jsonReq['value']) && isset($jsonReq['OrderDetailID']) && isset($jsonReq['LocationID'])) { 
        $tempBarcode = $jsonReq['value'];
        $orderdetailid = $jsonReq['OrderDetailID'];
        $locationId = $jsonReq['LocationID'];
        $qString = "SELECT Count(`Barcode`) AS barcode_count FROM productbarcode
            WHERE
            Barcode = '".$tempBarcode."'
            AND
            LocationID = ".$locationId." LIMIT 1"; 
        $query = $this->db->query($qString); 
        if ($query) {
          $barcount = $query->row();   
          if ($barcount->barcode_count > 0) {
            $data['TempBarcode'] = $tempBarcode;
            $data['TempPicked'] = 1;
            $data['TempPickQuantity'] = 1;
            $this->db->where('OrderDetailID', $orderdetailid);
            if ($this->db->update('orderdetail', $data)) {
              $this->httpresponses->respondJson($tempBarcode, 200);
            } else {
              $this->httpresponses->respondJsonify(array('message' => 'Update db error'), 400);
            } 
          } else {
            $this->httpresponses->respondJsonify(array('message' => 'Barcode does not exist in the specified location'), 400);
          }
        } else {
          $this->httpresponses->respondJsonify(array('message' => 'Db error'), 400);
        }
      } else  {
        $this->httpresponses->respondJsonify(array('message' => 'Invalid parameters'), 400);
      } 
    } else {
      $this->httpresponses->respondJsonify(array('message' => 'Unauthorized'), 401);
    }
  } 
}