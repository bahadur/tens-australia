<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class vc extends CI_Controller {
 

  function __construct(){ 
        parent::__construct();
        if (!$this->tank_auth->is_logged_in_as(array('staff', 'administrator')))
        { 
            redirect(base_url().'auth/login/');
        }
  }

 	public function shipout(){

 		$sqlQuery = 'SELECT order.RequiredDate, order.OrderID, 
	 		CONCAT(Firstname," ", Lastname, " (", BusinessName, ")") AS Customer, shipmethod.ShipMethod, IF(SumofLineTotal=SumofAmount,True,False) AS Paid,
			`order`.DueDate-Now() AS DaysToDueDate FROM `order` INNER JOIN customer ON customer.CustomerID = order.CustomerID LEFT JOIN qryfulfilmentlistorderpaid 
			ON qryfulfilmentlistorderpaid.OrderID = order.OrderID LEFT JOIN shipmethod ON shipmethod.ShipMethodID = order.ShipMethodID LEFT JOIN 
			qryfulfilmentlistallshipped ON qryfulfilmentlistallshipped.OrderID = order.orderID
			WHERE (shipmethod.ShipMethodID <> 27) AND (qryfulfilmentlistallshipped.OrderID IS NULL)'; 
		$ordersNotShippedQuery = $this->db->query($sqlQuery);
		if ($ordersNotShippedQuery) { 
	 		$data['title']='Ship out';  
	 		$data['ordersNotShipped'] = $ordersNotShippedQuery->result();
	 		$this->load->view('header',$data);
		    $this->load->view('navbar',$data);
		    $this->load->view('shipout/shipout');
		    $this->load->view('footer-no-js');
		}
 	}

 	public function orderFullfillment($orderID){
    $qFulfillmentString = 'SELECT `customer`.CustomerID, `order`.OrderID, `order`.OrderDate, 
        CONCAT(`Firstname`, " ", `LastName`, " (",`BusinessName`,")") AS CustomerName, 
        CONCAT(`order`.`ShippingAddress`,  ", " , `order`.`ShippingSuburb`, ", ", 
          `order`.`ShippingState`, " ", `order`.`ShippingPostcode`, ", " , `order`.`ShippingCountry`) AS Address, 
        `order`.HomePhone, `order`.MobilePhone, `order`.BusinessPhone, `order`.Email, `order`.InvoiceDate
        FROM `customer` INNER JOIN `order` ON `customer`.`CustomerID` = `order`.`CustomerID`
        WHERE `order`.`OrderID` = '.$orderID;  
 		$queryDetailsStr = 'SELECT orderdetail.OrderID, product.ProductID, product.ProductName, product.Hire, orderdetail.Quantity, 
				orderdetail.TempBarcode, orderdetail.TempPicked, orderdetail.TempPickQuantity, orderdetail.OrderDetailID,
				(SELECT count(Quantity) FROM stockmovedetail WHERE stockmovedetail.OrderDetailID = orderdetail.OrderDetailID) AS AlreadyShipped,
				product.HasBarcodeID FROM product INNER JOIN orderdetail ON product.ProductID = orderdetail.ProductID
				WHERE orderdetail.OrderID = '.$orderID.' ORDER BY orderdetail.OrderDetailID';
    $queryLocationsStr = 'SELECT location.LocationID, location.LocationDescription
               FROM location WHERE (((location.PickStock)=True)) ORDER BY location.LocationDescription'; 
    $queryDetails = $this->db->query($queryDetailsStr);
    $queryFulfillment = $this->db->query($qFulfillmentString);
    $queryLocations = $this->db->query($queryLocationsStr);
 		if ($queryDetails && $queryFulfillment && $queryLocations) {
      $defConfig = $this->db->query('SELECT DefaultPickLocationID FROM Config WHERE ConfigID = 1');
      if ($defConfig && $defConfig->num_rows() > 0) {
        $cR = $defConfig->result();
        $cRs = $cR[0];
        $data['DefaultPickLocationID'] = $cRs->DefaultPickLocationID; 
      }
      $rDetails = $queryDetails->result();
      $rFulfillment = $queryFulfillment->row();
      $rLocs = $queryLocations->result();
      $data['orderFulfillment'] = $rFulfillment; 
      $data['orderDetails'] = $rDetails;
      $data['locations'] = $rLocs;
      $data['title']='Order Fullfillment'; 
      $this->load->view('header',$data);
      $this->load->view('navbar');
      $this->load->view('shipout/orderFullfillment');
      $this->load->view('footer-no-js');
      return;
    }  
    echo "Could not open order fulfillment page.";
    echo $this->db->_error_message(); 
    echo $this->db->_error_number();  
 	}
}

/* End of file shipout.php */
/* Location: ./application/controllers/shipout.php */