<?php 

class vc extends CI_Controller {


  function __construct(){ 
    parent::__construct();
    if (!$this->tank_auth->is_logged_in_as(array('administrator')))
    { 
      redirect(base_url().'auth/login/');
    }
  }


  function manageProducts() {
    // load the correct view.
    $data['title']='Manage Products'; 
    $this->load->view('header',$data); //dynamic header view loaded
    $this->load->view('navbar'); //dynamic sidebar view loaded
    $this->load->view('cms/manageProducts'); //home page contents loaded, however we can provide contents of any page here to be loaded here
    $this->load->view('footer'); //footer view loaded,  
  }

  function viewProduct($productId = null) {
    if ($productId) {
      $qProductStr = 'SELECT * FROM product INNER JOIN productcategory ON product.ProductCategoryID = productcategory.ProductCategoryID WHERE product.ProductID = '.$productId;
      $qPriceListsStr = 'SELECT * FROM product INNER JOIN productprice ON product.ProductID = productprice.ProductID';
      $qCategoriesStr = 'SELECT * FROM productcategory';
      $qProduct = $this->db->query($qProductStr);
      $qPrice = $this->db->query($qPriceListsStr);
      $qCategories = $this->db->query($qCategoriesStr);
      if ($qPrice && $qProduct && $qCategories) {
        $product = $qProduct->result();
        $prices = $qPrice->result();
        $categories = $qCategories->result();
        $data['product'] = $product[0];
        $data['prices'] = $prices;
        $data['categories'] = $categories; 
        $data['title']='Edit a product'; 
        $this->load->view('header',$data); //dynamic header view loaded
        $this->load->view('navbar'); //dynamic sidebar view loaded
        $this->load->view('cms/viewProduct'); //home page contents loaded, however we can provide contents of any page here to be loaded here
        $this->load->view('footer'); //footer view loaded,  
      }
    }
  }
}