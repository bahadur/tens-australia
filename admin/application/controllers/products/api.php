<?php
class api extends CI_Controller {


  function __construct() {

    parent::__construct();
    header('Content-Type: application/json'); 
    $this->load->library('Datatables'); 
    $this->load->library('httpresponses');
    
  }
  

  public function add_addon(){
    print_r($this->input->post());
     $data = array(
                   'MainProductID' => $this->input->post('MainProductID'),
                   'AddonProductID' => $this->input->post('AddonProductID'),
                   'FieldTypeID' => $this->input->post('FieldTypeID'),
                   'DefaultValue' => $this->input->post('DefaultValue'),
                   'MinValue' => $this->input->post('MinValue'),
                   'Position' => $this->input->post('Position'),
                   );

     $this->db->insert('productaddons',$data);
  }

  public function upload_image(){

    //$imagePath = "../temp/";

    $imagePath = realpath(APPPATH . '../assets/img/');
    

    $allowedExts = array("gif", "jpeg", "jpg", "png", "GIF", "JPEG", "JPG", "PNG");
    $temp = explode(".", $_FILES["img"]["name"]);
    $extension = end($temp);
    
    //Check write Access to Directory

    if(!is_writable($imagePath)){
      $response = Array(
        "status" => 'error',
        "message" => 'Can`t upload File; no write Access'
      );
      print json_encode($response);
      return;
    }
    
    if ( in_array($extension, $allowedExts))
      {
      if ($_FILES["img"]["error"] > 0)
      {
         $response = array(
          "status" => 'error',
          "message" => 'ERROR Return Code: '. $_FILES["img"]["error"],
        );      
      }
      else
      {
       
        $filename = $_FILES["img"]["tmp_name"];
        
        list($width, $height) = getimagesize( $filename );

        move_uploaded_file($filename,  $imagePath .'\\'. $_FILES["img"]["name"]);

        $response = array(
        "status" => 'success',
        "url" => base_url().'assets/img/'.$_FILES["img"]["name"],
        "width" => $width,
        "height" => $height
        );
        
      }
      }
    else
      {
       $response = array(
        "status" => 'error',
        "message" => 'something went wrong, most likely file is to large for upload. check upload_max_filesize, post_max_size and memory_limit in you php.ini',
      );
      }
      
      print json_encode($response);
  }


  public function crop_image(){
    $imgUrl = $_POST['imgUrl'];
    // original sizes
    $imgInitW = $_POST['imgInitW'];
    $imgInitH = $_POST['imgInitH'];
    // resized sizes
    $imgW = $_POST['imgW'];
    $imgH = $_POST['imgH'];
    // offsets
    $imgY1 = $_POST['imgY1'];
    $imgX1 = $_POST['imgX1'];
    // crop box
    $cropW = $_POST['cropW'];
    $cropH = $_POST['cropH'];
    // rotation angle
    $angle = $_POST['rotation'];

    $jpeg_quality = 100;

    $imagePath = realpath(APPPATH . '../assets/img/');
    //$output_filename = "temp/croppedImg_".rand();
    $tmp = "/croppedImg_".rand();
    $output_filename = $imagePath.$tmp;
    $output_file_url = base_url().'assets/img/'.$tmp;

    // uncomment line below to save the cropped image in the same location as the original image.
    //$output_filename = dirname($imgUrl). "/croppedImg_".rand();

    $what = getimagesize($imgUrl);

    switch(strtolower($what['mime']))
    {
        case 'image/png':
            $img_r = imagecreatefrompng($imgUrl);
        $source_image = imagecreatefrompng($imgUrl);
        $type = '.png';
            break;
        case 'image/jpeg':
            $img_r = imagecreatefromjpeg($imgUrl);
        $source_image = imagecreatefromjpeg($imgUrl);
        error_log("jpg");
        $type = '.jpeg';
            break;
        case 'image/gif':
            $img_r = imagecreatefromgif($imgUrl);
        $source_image = imagecreatefromgif($imgUrl);
        $type = '.gif';
            break;
        default: die('image type not supported');
    }


    //Check write Access to Directory

    if(!is_writable(dirname($output_filename))){
      $response = Array(
          "status" => 'error',
          "message" => 'Can`t write cropped File'
        );  
    }else{

        // resize the original image to size of editor
        $resizedImage = imagecreatetruecolor($imgW, $imgH);
      imagecopyresampled($resizedImage, $source_image, 0, 0, 0, 0, $imgW, $imgH, $imgInitW, $imgInitH);
        // rotate the rezized image
        $rotated_image = imagerotate($resizedImage, -$angle, 0);
        // find new width & height of rotated image
        $rotated_width = imagesx($rotated_image);
        $rotated_height = imagesy($rotated_image);
        // diff between rotated & original sizes
        $dx = $rotated_width - $imgW;
        $dy = $rotated_height - $imgH;
        // crop rotated image to fit into original rezized rectangle
      $cropped_rotated_image = imagecreatetruecolor($imgW, $imgH);
      imagecolortransparent($cropped_rotated_image, imagecolorallocate($cropped_rotated_image, 0, 0, 0));
      imagecopyresampled($cropped_rotated_image, $rotated_image, 0, 0, $dx / 2, $dy / 2, $imgW, $imgH, $imgW, $imgH);
      // crop image into selected area
      $final_image = imagecreatetruecolor($cropW, $cropH);
      imagecolortransparent($final_image, imagecolorallocate($final_image, 0, 0, 0));
      imagecopyresampled($final_image, $cropped_rotated_image, 0, 0, $imgX1, $imgY1, $cropW, $cropH, $cropW, $cropH);
      // finally output png image
      //imagepng($final_image, $output_filename.$type, $png_quality);
      imagejpeg($final_image, $output_filename.$type, $jpeg_quality);
      $response = Array(
          "status" => 'success',
          "url" => $output_file_url.$type
        );
    }
    print json_encode($response);
  }


  public function getKitsSelect()
  { 
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator')))
    { 
      $kitQueryQuery = $this->db->query('SELECT KitID AS id, KitName AS text FROM kit ORDER BY KitName'); 
      if ($kitQueryQuery && $kitQueryQuery->num_rows() > 0) { 
          $this->httpresponses->respondJsonify($kitQueryQuery->result(), 200); 
      } else {
          $this->httpresponses->respondJsonify('No kits', 400); 
      }
    } else {
      $this->httpresponses->respondJsonify('Unauthorized', 401); 
    }
  }

  function getProductPrices($productId = null) {
    if ($productId) { 
      
         
      $this->datatables 
      ->select('pricegroup.PriceGroup,
        productprice.Price,
        productprice.ProductPriceID')
      ->from('productprice')
      ->join('pricegroup','pricegroup.PriceGroupID = productprice.PriceGroupID')
      ->where('productprice.ProductID',$productId);
      echo $this->datatables->generate(); 
      
    } else {
      $this->httpresponses->respondJsonify('No product specified', 400);
    }
  }

  function getProductAddons($productId = null) {
    
    if ($productId) {
     
      $this->datatables 
      ->select('
        productaddons.AddonProductID,
        product.ProductName,
        addonsfieldtypes.FieldType,
        productaddons.DefaultValue,
        productaddons.MinValue,
        productaddons.Position,
        productaddons.MainProductID')
      ->from('product')
      ->join('productaddons','product.ProductID = productaddons.AddonProductID')
      ->join('addonsfieldtypes','productaddons.FieldTypeID = addonsfieldtypes.FieldTypeID')
      ->where('productaddons.MainProductID', $productId);
      echo $this->datatables->generate(); 

    } else {
      $this->httpresponses->respondJsonify('No product specified', 400);
    }
  }
  /**
   * For datatables
   * @return JsonArray A json array for datatables
   */
  function getManageProducts() {  
      
      
      $this->datatables 
      ->select('
        product.ProductID,
        product.ProductCode,
        product.ProductName,
        productcategory.ProductCategory
        ')
      ->from('product')
      ->join('productcategory','product.ProductCategoryID = productcategory.ProductCategoryID');
      echo $this->datatables->generate(); 



   
  }
}