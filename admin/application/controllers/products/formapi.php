<?php
class formapi extends CI_Controller {


  function __construct() {

    parent::__construct(); 
    $this->load->library('httpresponses'); 
  }
 

  public function getProductsSelect() {
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator'))) {  
      $searchQuery = $this->input->post('q');
      $page = $this->input->post('page');
      $q = $this->db->select('ProductID AS `id`, ProductName AS `text`, product.HasBarcodeID')
        ->from('product')
        ->like('ProductName', $searchQuery)->get();
      if ($q && $q->num_rows() > 0) {  
        $this->httpresponses->respondJsonify($q->result(), 200); 
      } else {
        log_message('error', $this->db->last_query());
        log_message('error', $this->db->_error_message());
        $this->httpresponses->respondJsonify(array('message' => 'DB error.'), 400); 
      } 
    } else {
      $this->httpresponses->respondJsonify(array('message' => 'Unauthorized'), 401); 
    }
  }

  /**
   * Get all products available to the order with $orderId based on the order's price group
   * @param  [type] $orderId [description]
   * @return [type]          [description]
   */
  public function getAllProductsSelect($orderId) {  
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator'))) {  
      $searchQuery = $this->input->post('q');
      $page = $this->input->post('page');
      $q = $this->db->query('SELECT * FROM `order` WHERE `order`.OrderID = '.$orderId);
      if ($q && $q->num_rows() > 0) { 
        $order =$q->row();
        $this->load->library("orderdetails/orderproductfactory"); 
        $products = $this->orderproductfactory->getAllProductsSelect($order->PriceGroupID, $searchQuery);
        if ($products) {  
          if ($products->num_rows() > 0) {
            $this->httpresponses->respondJsonify($products->result(), 200);
          } else {
            $this->httpresponses->respondJsonify(array(), 200); 
          }
        } else {
        $this->httpresponses->respondJsonify(array('message' => 'Could not get the products'), 400); 
        }
      } else { 
        $this->httpresponses->respondJsonify(array('message' => 'No such order'), 400); 
      } 
    } else {
      $this->httpresponses->respondJsonify(array('message' => 'Unauthorized'), 401); 
    }
  }




  public function getKitsSelect() { 
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator'))) { 
      $kitsSearchQuery = $this->input->post('q');
      $kitQueryQuery = $this->db->select('KitID AS id, KitName AS text')
        ->from('kit')
        ->like('KitName', $kitsSearchQuery)
        ->order_by('KitName')
        ->get(); 
      if ($kitQueryQuery && $kitQueryQuery->num_rows() > 0) { 
          $this->httpresponses->respondJsonify($kitQueryQuery->result(), 200); 
      } else {
          $this->httpresponses->respondJsonify('No kits', 400); 
      }
    } else {
      $this->httpresponses->respondJsonify('Unauthorized', 401); 
    }
  }

 }