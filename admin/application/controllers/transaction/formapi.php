<?php
class formapi extends CI_Controller {


  function __construct(){
     
    parent::__construct();   
    $this->load->library('Datatables');
    $this->load->library('httpresponses');
  }

  public function getTransactions($orderId) {
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator'))) {  
      $this->datatables
        ->where('`transaction`.OrderID = '.$orderId.'')
        ->select('`transaction`.OrderID, transactiontype.TransactionType, paymentmethod.PaymentMethod, 
        `transaction`.TransactionDate, `transaction`.TransactionTotal, `transaction`.TransactionReference, 
        transactiondetail.OrderDetailID, transactiondetail.Amount, transactiondetail.AccountCode, 
        `transaction`.Comments, `transaction`.TransactionTypeID, `transaction`.TransactionID')
        ->from('transactiontype INNER JOIN ((paymentmethod INNER JOIN `transaction` ON paymentmethod.PaymentMethodID = `transaction`.PaymentMethodID) 
          INNER JOIN transactiondetail ON `transaction`.TransactionID = transactiondetail.TransactionID) 
        ON transactiontype.TransactionTypeID = `transaction`.TransactionTypeID');  
      $this->httpresponses->respondJson($this->datatables->generate(), 200);
    } else {
      $this->httpresponses->respondJsonify(array('message'=>'Unauthorized'), 401);
    }
  }

}