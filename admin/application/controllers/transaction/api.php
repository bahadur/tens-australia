<?php
class api extends CI_Controller {


  function __construct(){
     
    parent::__construct();  
    header('Content-Type: application/json');  
    $this->load->library('httpresponses');
  }


  public function saveTransaction()
  { 
    if ($this->tank_auth->is_logged_in_as(array('staff', 'administrator'))) {
      $requestBody = json_decode(file_get_contents('php://input'));  
      if ($requestBody->OrderID && $requestBody->TransactionTypeID &&  $requestBody->PaymentMethodID && $requestBody->TransactionTotal 
        && $requestBody->TransactionReference) {  
          $this->load->library('projectdates');
          $orderId = $this->input->post('orderId');
          $data['OrderID'] = $requestBody->OrderID;
          $data['TransactionTypeID'] = $requestBody->TransactionTypeID;
          $data['PaymentMethodID'] = $requestBody->PaymentMethodID;
          $data['TransactionDate'] = $this->projectdates->currentMySQLDate(); 
          $data['TransactionTotal'] = $requestBody->TransactionTotal;
          $data['TransactionReference'] = $requestBody->TransactionReference;  
          $pTermQ = $this->db->query('SELECT paymentterminal.PaymentMethodID, paymentterminal.TerminalID 
                                FROM (paymentterminal INNER JOIN paymentmethod ON paymentterminal.PaymentMethodID = paymentmethod.PaymentMethodID) 
                                INNER JOIN terminal ON paymentterminal.TerminalID = terminal.TerminalID 
                                WHERE paymentterminal.PaymentMethodID = '.$data['PaymentMethodID'].'
                                ORDER BY paymentterminal.PaymentTerminalID DESC , 
                                paymentterminal.TerminalID'); 
          if ($pTermQ->num_rows() > 0) {
            $pTermR = $pTermQ->result();
            $pTerm = $pTermR[0];
            $data['TerminalID'] = $pTerm->TerminalID;  
            $qTrDetailsStr = "SELECT qrypaymenttransactionsdetail.TransactionID, orderdetail.OrderDetailID, orderdetail.TempTransactionAmount,
              qrypaymenttransactionsdetail.TransactionID, productaccountcode.AccountCode, orderdetail.GST_Amount 
              FROM (orderdetail LEFT JOIN qrypaymenttransactionsdetail ON qrypaymenttransactionsdetail.OrderDetailID = orderdetail.OrderDetailID)
              LEFT JOIN productaccountcode ON orderdetail.ProductID = productaccountcode.ProductID  WHERE 
              (((productaccountcode.TransactionTypeID = ".$data['TransactionTypeID'].") OR (productaccountcode.TransactionTypeID IS NULL) )
              AND (orderdetail.TempTransactionInclude = True) AND (qrypaymenttransactionsdetail.TransactionID IS NULL) 
              AND (orderdetail.OrderID = ".$data['OrderID']."))";    
            $qTrDetails = $this->db->query($qTrDetailsStr); 
            if ($qTrDetails->num_rows() > 0)
            { 
              $batchData = array();
              $rTrDetails = $qTrDetails->result();   
              if ($this->db->insert('transaction', $data)) {
                $transactionId = $this->db->insert_id();  
                foreach ($rTrDetails as $rTrDetail) {
                  array_push($batchData, array(
                      "TransactionID" => $transactionId, 
                      "OrderDetailID" => $rTrDetail->OrderDetailID, 
                      "Amount" => $rTrDetail->TempTransactionAmount, 
                      "AccountCode" => $rTrDetail->AccountCode, 
                      "GST_Amount" => $rTrDetail->GST_Amount
                    ));
                }  
                if ($t = $this->db->insert_batch('transactiondetail', $batchData)) {
                  $this->httpresponses->respondJson(json_encode(array('message'=>'Success', 'lastSql' => $this->db->last_query(), 'sqlRes' => $t, 'pMethod' => $data['PaymentMethodID'])), 200);
                } else {
                  $this->httpresponses->respondJsonify($this->db->_error_message(), 400);
                }
              } else {
                $this->httpresponses->respondJsonify(array('message' => '- Transaction Failed'), 400);
              }
            } else  {  
                $this->httpresponses->respondJsonify(array('message' => 'Transaction Failed'), 400);
            }
          } else  { 
            $this->httpresponses->respondJsonify(array('message' => 'No transaction'), 400);
          }  
      } else {
        $this->httpresponses->respondJsonify(array('message' => 'Invalid Parameters'), 400);
      }
    } else {
        $this->httpresponses->respondJsonify(array('message' => 'Unauthorized'), 401);
    }
  }

}