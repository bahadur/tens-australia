<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ProjectDates
{
	
    private $_frontEndDateFormat = "d-m-Y"; 
    private $_frontEndTimeFormat = 'h:i:s a';
    private $_mysqlDateFormat = 'Y-m-d H:i:s';
	private $_ci;

 	function __construct()
    { 
    	$this->_ci =& get_instance(); 
    }

    public function parseToMySQLMyobDate($str) {
        $frontEndDateFormat = "d-m-Y H:i:s"; 
        $mysqlDateFormat = 'Y-m-d H:i:s';
        if ($orderDate = date_create_from_format($frontEndDateFormat, $str))
        {
            return $orderDate->format($mysqlDateFormat);
        } 
        else 
        {
            return null;
        }
    }

    public function parseToMySQLDate($str)
    {  
        $frontEndDateFormat = "d/m/Y"; 
        $mysqlDateFormat = 'Y-m-d H:i:s';
        if ($orderDate = date_create_from_format($frontEndDateFormat, $str))
        {
            return $orderDate->format($mysqlDateFormat);
        } 
        else 
        {
            return null;
        }
    } 

    public function mysqlNow()
    {
        return date($this->_mysqlDateFormat);
    }
    public function backendDateToFrontEndTime($mysqlStr)
    { 
        if ($datetime = date_create_from_format($this->_mysqlDateFormat, $mysqlStr))
        {
            return $datetime->format($this->_frontEndTimeFormat);
        } 
        else 
        {
            return null;
        }
    }

    public function backendDateToFrontEndDate($mysqlStr)
    {
        if ($datetime = date_create_from_format($this->_mysqlDateFormat, $mysqlStr))
        {
            return $datetime->format($this->_frontEndDateFormat);
        } 
        else 
        {
            return null;
        }
    }


    public function frontEndDateToBackEndDate($str)
    {
        return $this->parseToMySQLDate($str);
    }

    public function frontEndTimeToBackEndDate($str)
    {
        $frontEndTimeFormat = "H:i:s a"; 
        $mysqlDateFormat = 'Y-m-d H:i:s';
        if ($orderDate = date_create_from_format($frontEndTimeFormat, $str))
        {
            return $orderDate->format($mysqlDateFormat);
        } 
        else 
        {
            return null;
        }
    }

    public function currentMySQLDate()
    {
        return date('Y-m-d H:i:s', time());
    }

}

 
?>