<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class OrderProductFactory
{
	
	private $_ci;

 	function __construct()
    {
    	//When the class is constructed get an instance of codeigniter so we can access it locally
    	$this->_ci =& get_instance();
    	//Include the user_model so we can use it
    	$this->_ci->load->model("orderdetails/orderproduct");
    }


    public function getAllProductsModels($priceGroupId) {
        $this->_ci->db->query("SET @priceGroupIdVar = $priceGroupId");
        $productsResults = $this->_ci->db->query(
           'SELECT product.ProductID, product.ProductName, product.Hire, qryproductprice.Price, taxcode.CalcGST AS GST, 
            IF(taxcode.CalcGST,(SELECT GST_Percent FROM config WHERE ConfigID = 1),0) AS GSTPercent, 
            product.HasBarcodeID FROM taxcode INNER JOIN (product LEFT JOIN qryproductprice ON product.ProductID=qryproductprice.ProductID) 
            ON taxcode.TaxCodeID=product.TaxCodeID ORDER BY product.ProductName')->result();   
        $productsResultsArray = array(); 
        foreach ($productsResults as $row)
        {
            array_push($productsResultsArray, $this->createObjectFromData($row));
        } 
        return $productsResultsArray;
    }

    
    public function getAllProductsSelect($priceGroupId, $filterQuery = null)
    {  
		$this->_ci->db->query("SET @priceGroupIdVar = $priceGroupId"); 
        $this->_ci->db->select('product.ProductID AS id, product.ProductName AS text, product.Hire, qryproductprice.Price, taxcode.CalcGST AS GST, 
            IF(taxcode.CalcGST,(SELECT GST_Percent FROM config WHERE ConfigID = 1),0) AS GSTPercent, 
            product.HasBarcodeID', false)
        ->from('taxcode INNER JOIN (product LEFT JOIN qryproductprice ON product.ProductID=qryproductprice.ProductID) 
            ON taxcode.TaxCodeID=product.TaxCodeID')
        ->like('product.ProductName', 
            $filterQuery)
        ->order_by('product.ProductName');
        $productsResults = $this->_ci->db->get();
		if ($productsResults) {
            log_message('debug', $this->_ci->db->last_query());
            return $productsResults;
        } else {
            log_message('error', $this->_ci->db->last_query());
            log_message('error', $this->_ci->db->_error_message());
            return null;
        } 
    }


    public function createObjectFromData($row) { 
    	$orderProduct = new OrderProduct();
    	$orderProduct->setProductId($row->ProductID);
    	$orderProduct->setProductName($row->ProductName);
    	$orderProduct->setHire($row->Hire);
    	$orderProduct->setPrice($row->Price);
    	$orderProduct->setCalcGst($row->GST);
    	$orderProduct->setGstPercent($row->GSTPercent);
    	$orderProduct->setHasBarcodeId($row->HasBarcodeID);
        return $orderProduct;
    }
}
 
?>