<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ProductOrderDetailFactory
{

	private $_ci;

 	function __construct()
    {
    	//When the class is constructed get an instance of codeigniter so we can access it locally
    	$this->_ci =& get_instance();
    	//Include the user_model so we can use it
    	$this->_ci->load->model("orderdetails/productorderdetail");
        $this->_ci->load->library('errorobject/errorobject');
    }

    /**
     * Creates an order detail model data to be inserted into the database;
     * @param  [type] $orderId   [description]
     * @param  [type] $productId [description]
     * @param  [type] $quantity  [description]
     * @param  [type] $price     [description]
     * @return [ErrorObject]   Returns an error object with a model on success, and an error object with an error if not successful
     */
    public function createOrderDetail($orderId, $productId, $quantity, $price) {   
        $qProduct = 
            $this->_ci->db->query('SELECT selectedorder.OrderID, product.ProductID, 
                        product.ProductName, '.$price.' AS UnitPrice, '.$quantity.' AS Quantity, taxcode.CalcGST AS GST, 
                            CONVERT(ROUND(IF ( taxcode.CalcGST, config.GST_Percent, 0), 2),DECIMAL(16,2)) AS GST_Percent,
                            CONVERT(
                                    ROUND(Price * '.$quantity.' *
                                            (1-
                                                (1/
                                                    (1+ 
                                                        ROUND(
                                                            IF(taxcode.CalcGST = 1, config.GST_Percent, 0),4
                                                            )
                                                    )
                                                )
                                            )*(CalcGST),2
                                        ) 
                                        , DECIMAL(16,2)
                                    ) AS GST_Amount,  
                            product.Hire, product.HasBarcodeID, product.AccountCode,
                            IF(Hire = True, 
                                DATE_ADD(selectedorder.DueDate, 
                                    INTERVAL ReturnExpectedDuePlusDays DAY),Null) AS ReturnExpectedDate 
                            FROM config, taxcode INNER JOIN 
                                ((SELECT * FROM `order` WHERE `order`.OrderID = '.$orderId.' LIMIT 1) AS `selectedorder` 
                                INNER JOIN productprice ON (productprice.PriceGroupID = selectedorder.PriceGroupID AND productprice.ProductID = '.$productId.') 
                                INNER JOIN `product` ON product.ProductID = productprice.ProductID )
                            ON taxcode.TaxCodeID = product.TaxCodeID 
                            WHERE (config.ConfigID = 1);');
        log_message('debug', 'Inserting a product order detail query:');
        log_message('debug', $this->_ci->db->last_query());
        if ($qProduct  && $qProduct->num_rows() > 0) {  
            $productData = $qProduct->row();
            if ($productData->AccountCode) {
                return $this->_ci->errorobject->create(null, $this->createModel($productData)); 
            } else {
                log_message('error', 'CreateOrderDetail() : this product '.$productId.' has no account code.'); 
                return $this->_ci->errorobject
                    ->create('CreateOrderDetail() : this product '.$productId.' has no account code or order id '.$orderId.' does not exist.', null);
            }
        } else {
            log_message('error', $this->_ci->db->last_query());
            log_message('error', $this->_ci->db->_error_message());
            return $this->_ci->errorobject
                ->create('Cannot get the product with id '.$productId.' info. Either it does not have prices for the selected price group or the order is invalid.', 
                    null);
        } 
    }

    public function getProduct($productId)
    {
        $queryString = "SELECT product.ProductName, product.Hire, taxcode.CalcGST, config.GST_Percent
            FROM product, config INNER JOIN taxcode WHERE config.ConfigID = 1 AND
            taxcode.TaxCodeID = product.TaxCodeID AND product.ProductID = ".$productId; 
        $query = $this->_ci->db->query($queryString);
        $rowcount = $query->num_rows();
        if ($query->num_rows() > 0)
        { 
            return $query->row();
        }
        else 
        {
            return null;
        }
    } 

    public function getAllProductsInAKit($orderId, $kitId)
    { 
        $q = $this->_ci->db->query('SELECT DueDate, PriceGroupID FROM `order` WHERE `order`.OrderID = '.$orderId);
        if ($q && $q->num_rows() > 0) {
            $rs = $q->result();
            $r = $rs[0];
            $dueDate = $r->DueDate;
            $priceGroupId = $r->PriceGroupID;
            if ($dueDate && $priceGroupId) {
                $queryString = 'SELECT '.$orderId.' AS OrderID, kitproduct.ProductID, product.ProductName, productprice.Price AS UnitPrice, 
                        kitproduct.Quantity, taxcode.CalcGST AS GST, 
                            CONVERT(ROUND(IF ( taxcode.CalcGST, config.GST_Percent, 0), 2),DECIMAL(16,2)) AS GST_Percent,
                            CONVERT(
                                    ROUND(Price * Quantity *
                                            (1-
                                                (1/
                                                    (1+ 
                                                        ROUND(
                                                            IF(taxcode.CalcGST = 1, config.GST_Percent, 0),4
                                                            )
                                                    )
                                                )
                                            )*(CalcGST),2
                                        ) 
                                        , DECIMAL(16,2)
                                    ) AS GST_Amount,  
                            product.Hire, product.HasBarcodeID, product.AccountCode,
                            IF(Hire = True, 
                                DATE_ADD(\''.$dueDate.'\', 
                                    INTERVAL ReturnExpectedDuePlusDays DAY),Null) AS ReturnExpectedDate 
                            FROM config, taxcode INNER JOIN ((product INNER JOIN kitproduct ON product.ProductID = kitproduct.ProductID) 
                                INNER JOIN productprice ON product.ProductID = productprice.ProductID) ON taxcode.TaxCodeID = product.TaxCodeID 
                            WHERE(((kitproduct.KitID) = '.$kitId.') And ((productprice.PriceGroupID) = '.$priceGroupId.') And ((config.ConfigID) = 1)) 
                            ORDER BY kitproduct.KitProductID;'; 
                $mainQ = $this->_ci->db->query($queryString);
                if ($mainQ && $mainQ->num_rows() > 0) {
                    $kitQueryResult = $mainQ->result();
                    $kitDetailsArray = array();
                    log_message('debug', $this->_ci->db->last_query());
                    foreach ($kitQueryResult as $kitDetailRow) {
                        if ($kitDetailRow->AccountCode) {
                            $kd = $this->createModel($kitDetailRow); 
                            array_push($kitDetailsArray, $kd);
                        } else {
                            return $this->_ci
                                ->errorobject
                                ->create('This product in the kit:'.$kitDetailRow->ProductName.' has no account code.', null); 
                        }
                    }  
                    return $this->_ci
                        ->errorobject
                        ->create(null, $kitDetailsArray); 
                } else {
                    log_message('error', $this->db->last_query());
                    log_message('error', $this->db->_error_message());
                    log_message('error', $this->db->_error_code());
                    return $this->_ci
                        ->errorobject
                        ->create('Database error when adding a kit. Check logs.', $kitDetailsArray); 
                }
            } else {
                return $this->_ci
                    ->errorobject
                    ->create('Due date or price group is missing', $kitDetailsArray); 
            }
        } else {
            log_message('error', $this->db->last_query());
            log_message('error', $this->db->_error_message());
            log_message('error', $this->db->_error_code());
            return $this->_ci
                ->errorobject
                ->create('Database error when adding a kit. Check logs.', $kitDetailsArray); 
        } 
    } 

    public function createObjectFromArray($data)
    { 
        $productInKit = new ProductOrderDetail();  
        $productInKit->setOrderDetailId($data['orderDetailId']);
        $productInKit->setOrderDetailId($data["orderId"]);
        $productInKit->setOrderId($data["orderId"]); 
        $productInKit->setProductId($data["productId"]); 
        $productInKit->setProductName($data["productName"]); 
        $productInKit->setPrice($data["price"]);
        $productInKit->setQuantity($data["quantity"]);
        $productInKit->setCalcGST($data["calcGst"]);
        $productInKit->setGSTPercent($data['gstPercent']);
        $productInKit->setTaxAmount($data['taxAmount']); 
        $productInKit->setHire($data["hire"]); 
        $productInKit->setReturnExpectedDate($data['returnExpectedDate']); 
        $productInKit->setCancelReminderMessages(0); 
        return $productInKit;
    }

    public function createModel($row) {    
    	$productInKit = new ProductOrderDetail();  
        if (isset($row->OrderDetailID))
        {
            $productInKit->setOrderDetailId($row->OrderDetailID);
        }
        log_message('error', $row->HasBarcodeID);
        $productInKit->setAccountCode($row->AccountCode); 
        $productInKit->setHasBarcodeId($row->HasBarcodeID); 
        $productInKit->setOrderId($row->OrderID); 
        $productInKit->setProductId($row->ProductID); 
        $productInKit->setProductName($row->ProductName); 
        $productInKit->setPrice($row->UnitPrice);
        $productInKit->setQuantity($row->Quantity);
        $productInKit->setCalcGST($row->GST);
        $productInKit->setGSTPercent($row->GST_Percent);
        $productInKit->setTaxAmount($row->GST_Amount); 
        $productInKit->setHire($row->Hire); 
        $productInKit->setReturnExpectedDate($row->ReturnExpectedDate); 
        if (isset($row->CancelReminderMessages))
        {
            $productInKit->setCancelReminderMessages($row->CancelReminderMessages); 
        }
        else 
        {
            $productInKit->setCancelReminderMessages(1); 
        }
    	return $productInKit;
    }
} 
?>