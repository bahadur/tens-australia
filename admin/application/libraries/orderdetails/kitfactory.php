<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class KitFactory
{
	
	private $_ci;

 	function __construct()
    {
    	//When the class is constructed get an instance of codeigniter so we can access it locally
    	$this->_ci =& get_instance();
    	//Include the user_model so we can use it
    	$this->_ci->load->model("orderdetails/kitdetails");
    }

    public function getAllKits()
    { 
		$kitDetailsArray = array();
		$kitQueryQuery = $this->_ci->db->query('SELECT KitID, KitName FROM kit ORDER BY KitName'); 
		if (!is_null($kitQueryQuery))
		{
			$kitQueryResult = $kitQueryQuery->result();
			foreach ($kitQueryResult as $kitDetailRow)
			{
				array_push($kitDetailsArray, $this->createObjectFromData($kitDetailRow));
			}
		}
		return $kitDetailsArray;
    }


    public function createObjectFromData($row) { 
    	$kitDetails = new KitDetails(); 
    	$kitDetails->setKitId($row->KitID); 
    	$kitDetails->setKitName($row->KitName); 
    	return $kitDetails;
    }
}

 
?>