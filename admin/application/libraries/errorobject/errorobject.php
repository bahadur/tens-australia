<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ErrorObject 
{ 
  private $_error;
  private $_item;

  function __construct()
  { 
  }

  public function getError() {
    return $this->_error;
  }

  public function setError($val) {
    $this->_error = $val;
  }

  public function getItem() {
    return $this->_item;
  }

  public function setItem($val) {
    $this->_item = $val;
  }

  function create($errorMessage, $successItem) {
    $error = new ErrorObject();
    $error->setError($errorMessage);
    $error->setItem($successItem);
    return $error;
  }
}

