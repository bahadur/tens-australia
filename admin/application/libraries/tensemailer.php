<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class Tensemailer {
    
    function tensemailer()
    {
        require_once APPPATH.'/third_party/PHPMailer-master/PHPMailerAutoload.php';  
        require_once APPPATH.'/third_party/SaveSentEmailer/saveSentEmailer.php';  
    }
  
}