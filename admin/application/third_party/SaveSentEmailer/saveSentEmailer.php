<?php 

class SaveSentMailer extends PHPMailer {
    /**
     * Save email to a folder (via IMAP)
     *
     * This function will open an IMAP stream using the email
     * credentials previously specified, and will save the email
     * to a specified folder. Parameter is the folder name (ie, Sent)
     * if nothing was specified it will be saved in the inbox.
     *
     * @author David Tkachuk <http://davidrockin.com/>
     */
    public function copyToFolder($folderPath = null) {
        $message = $this->MIMEHeader . $this->MIMEBody; 
        $emailHostPath = "{cp-kil-m-001.micron21.com:993/imap/validate-cert/ssl}INBOX.Sent" ;
        $imapStream = imap_open($emailHostPath , $this->Username, $this->Password)  or die('Cannot connect: ' . print_r(imap_errors(), true));
        if ($imapStream)
        { 
            imap_append($imapStream, $emailHostPath, $message) or die('Cannot connect: ' . print_r(imap_errors(), true)); 
            imap_close($imapStream) or die('Cannot connect: ' . print_r(imap_errors(), true));
        }
        else 
        {
            echo "Couldn't open imap";
        }
    }
}

?>