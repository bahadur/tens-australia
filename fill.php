<?php


$hostname = "localhost";  //write your local server
$mysql_login = "root";  //write your mysql login
$mysql_password = "";  //write your mysql password
$database = "tens_australia";  //write name of mysql database

$link = mysqli_connect($hostname,$mysql_login,$mysql_password,$database) or die("Error " . mysqli_error($link));

//    customer        orders


//$ProductID;     // foreign key can be hardcoded. We already have a table of products.

$order_detail_num = (rand(0,1) % 2 == 0 )? 3: 4;


// Testing area
/*echo$order_detail_num;
exit;*/



for($i = 0; $i < 100000; $i++) {

    $OrderSourceID = 1;
    $HospitalID = 1;
    $AreaManagerID = 1;

    $BillingAddress = "Place Street 12";
    $BillingSuburb = "Melbourne";
    $BillingState = "VIC";
    $BillingPostcode = "3000";
    $BillingCountry = "Australia";

    $ShippingSameAsBilling = 1;

    $ShippingAddress = "Place Street 12";
    $ShippingSuburb = "Melbourne";
    $ShippingState = "VIC";
    $ShippingPostcode = "3000";
    $ShippingCountry = "Australia";

    $HomePhone = "400 000 000";
    $Fax = "0303030303";
    $MobilePhone = "0443333443";
    $Email = "incotaku@yahoo.com";
    $Comments = "";
    $Active = "";
    $InsuranceProviderID = 2;
    $InsuranceMembershipNo = "34343434343";
    $PriceGroupID = 1;
    $PriceList = 1;

    $CustomerTypeID = rand(1,15);


    if($i%2 == 0) {
        $BusinessName = 'Abc';
        $ABN = "4443" . $i;
        $BusinessPhone = "0444322" . $i;
        $Website = "www.google.com";
    }
    else {
        $BusinessName = '';
        $ABN = '';
        $BusinessPhone = "";
        $Website = "";
    }

    $Firstname = "Jon" .$i;
    $LastName = "Malkovich" .$i;


    $query = "INSERT INTO customer (`CustomerTypeID`, `BusinessName`, `ABN`, `Firstname`,     `LastName`, `BillingAddress`,  `BillingSuburb`,  `BillingState`,  `BillingPostcode`,  `BillingCountry`,  `ShippingSameAsBilling`,  `ShippingAddress`,
                                      `ShippingSuburb`,`ShippingState`,`ShippingPostcode`,`ShippingCountry`,     `HomePhone`,
                                      `BusinessPhone`,  `Fax`,  `MobilePhone`,  `Email`,  `Website`, `Comments`,  `Active`,
                                      `InsuranceProviderID`, `InsuranceMembershipNo`, `PriceGroupID`, `PriceList` )
                            VALUES ('$CustomerTypeID', '$BusinessName', '$ABN', '$Firstname', '$LastName', '$BillingAddress', '$BillingSuburb', '$BillingState', '$BillingPostcode', '$BillingCountry', '$ShippingSameAsBilling', '$ShippingAddress',
                                      '$ShippingSuburb','$ShippingState','$ShippingPostcode','$ShippingCountry', '$HomePhone',
                                      '$BusinessPhone', '$Fax', '$MobilePhone', '$Email', '$Website', '$Comments', '$Active',
                                      '$InsuranceProviderID', '$InsuranceMembershipNo', '$PriceGroupID', '$PriceList')";

    mysqli_query($link, $query) or die ('Unable to execute query. '. mysqli_error($link));

    $CustomerID = mysqli_insert_id($link);

    $HospitalID = 3;
    $EducatorID  = 2;
    $AreaManagerID  = 2;
    $OrderDate = "2015-05-10 00:00:00";
    $CreatedDate = "2015-05-10 00:00:00";
    $DueDate = "2016-01-01 00:00:00";
    $RequiredDate = "2015-11-01 00:00:00";

    $orders_num = ($CustomerID % 2 == 0)? 4:2;

    for($j = 0; $j < $orders_num; $j++) {

        $query = "INSERT INTO `order` (`CustomerID`,  `PriceGroupID`, `BillingAddress`,   `BillingSuburb`,  `BillingState`,  `BillingPostcode`,  `BillingCountry`,  `ShippingSameAsBilling`,  `ShippingAddress`,
                                 `ShippingSuburb`,`ShippingState`,`ShippingPostcode`,`ShippingCountry`,     `HomePhone`,
                                 `BusinessPhone`,  `Fax`,  `MobilePhone`,  `Email`,  `OrderDate`, `CreatedDate`, `DueDate`,
                                 `RequiredDate`,  `OrderSourceID`,  `HospitalID`,  `EducatorID`,  `AreaManagerID`  )
                         VALUES ('$CustomerID', '$PriceGroupID', '$BillingAddress', '$BillingSuburb', '$BillingState', '$BillingPostcode', '$BillingCountry', '$ShippingSameAsBilling', '$ShippingAddress',
                                 '$ShippingSuburb','$ShippingState','$ShippingPostcode','$ShippingCountry', '$HomePhone',
                                 '$BusinessPhone', '$Fax', '$MobilePhone', '$Email', '$OrderDate',  '$CreatedDate', '$DueDate',
                                 '$RequiredDate', '$OrderSourceID', '$HospitalID', '$EducatorID', '$AreaManagerID'  )";


        mysqli_query($link, $query) or die ('Unable to execute query. '. mysqli_error($link));

        $OrderID = mysqli_insert_id($link);

        if($OrderID % 2 == 0) {
            $order_detail_num = (rand(0,1) % 2 == 0 )? 0: 4;
        }
        else {
            $order_detail_num = (rand(0,1) % 2 == 0 )? 0: 3;
        }


        $GST = 0;
        $Hire = 0;
        $ReturnExpectedDate =  "2016-01-01 00:00:00";
        $CancelReminderMessages =  1;
        $TempTransactionInclude =  0;
        $TempPicked =  0;



        if($order_detail_num != 0) {
            //for Transactions based on oreder

            $TransactionTypeID = 1;
            $PaymentMethodID = 1;
            $TerminalID = 5;
            $TransactionDate = "2015-05-10 00:00:00";
            $TransactionCreateDateTime = "2015-05-10 00:00:00";
            $TransactionTotal = 99;
            $TransactionReference = 'Refer';
            $Comments= '';

            $query = "INSERT INTO `transaction` ( `OrderID`,  `TransactionTypeID`,  `PaymentMethodID`,  `TransactionDate`,  `TransactionTotal`,
                                            `TransactionReference`, `TransactionCreateDateTime`, `TerminalID`, `Comments`  )
                                   VALUES ( '$OrderID', '$TransactionTypeID', '$PaymentMethodID', '$TransactionDate', '$TransactionTotal',
                                           '$TransactionReference', '$TransactionCreateDateTime', '$TerminalID', '$Comments')";

            mysqli_query($link, $query) or die ('Unable to execute query. '. mysqli_error($link));

            $TransactionID = mysqli_insert_id($link);
        }

        for($t = 0; $t < $order_detail_num; $t++) {



            $ProductID = 1;
			$Quantity = 1;
            $query = "INSERT INTO orderdetail (`OrderID`,  `ProductID`,   `Quantity`,  `GST`,   `ReturnExpectedDate`, `CancelReminderMessages`,
                                           `TempTransactionInclude`, `TempPicked`, `Hire`)
                                   VALUES ('$OrderID',  '$ProductID', '$Quantity', '$GST', '$ReturnExpectedDate',  '$CancelReminderMessages',
                                           '$TempTransactionInclude', '$TempPicked', '$Hire')";

            mysqli_query($link, $query) or die ('Unable to execute query. '. mysqli_error($link));

            $OrderDetailID = mysqli_insert_id($link);

            //  Amount : 15.00 (even though it is wrong, just hardcode it)
            //AccountCode : "ABC"
            //GST_AMOUNT : 0.00
            //TransactionID, orderdetailId : I will leave this up to you.

            if($t < 3) {
                $Amount = 15;
                $AccountCode = "ABC";
                $GST_Amount = 0;
                                
                $query = "INSERT INTO transactiondetail (`TransactionID`,   `OrderDetailID`, `Amount`,  `AccountCode`, `GST_Amount`  )
                                                 VALUES ('$TransactionID', '$OrderDetailID', '$Amount', '$AccountCode', '$GST_Amount' )";
                mysqli_query($link, $query) or die ('Unable to execute query. '. mysqli_error($link));
            }

        }

    }




}


