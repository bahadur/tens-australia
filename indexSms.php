<?php


class SmsReminder {

	private $_dbUsername;
	private $_dbPassword;
	private $_dbName;
	private $_db;

	function __construct(){  
		/* Production live setup : */
		$this->_dbUsername = "tensaust_admin";
		$this->_dbPassword = "@dm1nMySQL";
		$this->_dbName = "tensaust_web";
		/* Local setup : 
		$this->_dbUsername = "root";
		$this->_dbPassword = "";
		$this->_dbName = "tens_aust";*/
	}

	function sendReminders()
	{
		/*
			How about allow this only to the cronjob.
			Maybe, I will need to change the active record to core mysql functions.
			Plus, I need to add testing to this whole thing...
			
			Q1. What happens to failed sent SMSes??
		*/  
		$this->_db = new mysqli('localhost', $this->_dbUsername, $this->_dbPassword, $this->_dbName); 
		if ($this->_db )
		{
			if ($this->_db->connect_errno) {
			    printf("Connect failed: %s\n", $this->_db->connect_error);
			    exit();
			}
			if ($this->sendHireReminders() && $this->sendClassesReminders())
			{
				$this->_db->query('UPDATE config SET config.SMS_SentDate = NOW()'); 
			}
			$this->_db->close();
		}
		else 
		{
			echo "Could not open database connection.";
		}
		/*if ($this->sendHireReminders() && $this->sendClassesReminders())
		{
			$reminderData['SMS_SentDate'] = date('Y-m-d H:i:s');	
			$this->db->update('config',$reminderData);	  
		}*/ 
	}


	private function sendClassesReminders()
	{
		if(!$result = $this->_db->query("SELECT ClassReminderMessageText, ClassReminderMinusDays FROM config WHERE ConfigID = 1")){
			die('#1 - There was an error running the query [' . $this->_db->error . ']');
			return false;
		}  
		if ($result->num_rows > 0)
		{
			$row = $result->fetch_assoc();
			$strClassReminderMessageText = $row['ClassReminderMessageText'];
			$intClassReminderMinusDays = $row['ClassReminderMinusDays'];
			$result->free();
			$strSQL = "SELECT customer.MobilePhone, educatorclass.StartTime, educatorclass.ClassDate, educatorlocation.LocName, 
				educatorclasscustomer.EducatorClassCustomerID FROM (educatorlocation INNER JOIN (educatorclass INNER JOIN 
				(customer INNER JOIN educatorclasscustomer ON customer.CustomerID = educatorclasscustomer.CustomerID) 
				ON educatorclass.EduClassID = educatorclasscustomer.EduClassID) ON educatorlocation.EduLocationID = educatorclass.EduLocationID) 
				LEFT JOIN educatorclasscustomerremindersent ON 
				educatorclasscustomer.EducatorClassCustomerID = educatorclasscustomerremindersent.EducatorClassCustomerID  
				WHERE (customer.MobilePhone IS NOT Null) 
				AND (educatorclass.ClassDate = DATE_ADD(DATE(Now()), INTERVAL $intClassReminderMinusDays DAY)) 
				AND ((educatorclasscustomerremindersent.EducatorClassCustomerID) Is Null) 
				AND ((educatorclasscustomer.Cancelled)=False)";
 
			if(!$classResults = $this->_db->query($strSQL))
			{
				die('#2 - There was an error running the query [' . $this->_db->error . ']');
				return false;
			}
			$insertData = array();
			while($classRow = $classResults->fetch_assoc()){   
				$strClassDate = date('d/m/Y', strtotime($classRow['ClassDate'])); 
				$strClassTime = $this->convertDate($classRow['StartTime']);  
				$finalMessage = sprintf($strClassReminderMessageText, $strClassDate, $strClassTime, $classToRemind->LocName);
				if ($this->sendSMSWithReplyToEmail($classToRemind->MobilePhone, $finalMessage))
				{
					$insertData['EducatorClassCustomerID'] = $classToRemind->EducatorClassCustomerID;
					$insertData['SentDateTime'] = date('Y-m-d H:i:s'); 
					print_r($insertData);
				}
			}
			$classResults->free();
			return true;
		}
		else 
		{
			return false;
		} 
	} 
	
	private function sendHireReminders()
	{
		/* 
			Sending reminders about hires
		*/
		
		if(!$durationOne = $this->_db->query("SELECT * FROM reminder WHERE ReminderID = 1 LIMIT 1")){
			die('#13 - There was an error running the query [' . $this->_db->error . ']');
			return false;
		}  
		
		if(!$durationTwo = $this->_db->query("SELECT * FROM reminder WHERE ReminderID = 2 LIMIT 1")){
			die('#4 - There was an error running the query [' . $this->_db->error . ']');
			return false;
		}   
		if ($durationOne->num_rows == 1 && $durationTwo->num_rows == 1)
		{
			$durationOneResults = $durationOne->fetch_assoc();
			$durationTwoResults = $durationTwo->fetch_assoc();
			$intervalOne = $durationOneResults['GraceDays'];
			$intervalSecond = $durationTwoResults['GraceDays']; 
			$firstRemindersToSend = "SELECT orderdetail.OrderDetailID, customer.MobilePhone, order.OrderID, customer.Firstname,
				1 AS ReminderID, ReturnExpectedDate FROM (customer INNER JOIN `order` ON customer.CustomerID = order.CustomerID) 
				INNER JOIN (orderdetail LEFT JOIN remindersent ON orderdetail.OrderDetailID = remindersent.OrderDetailID) ON 
				order.OrderID = orderdetail.OrderID WHERE (customer.MobilePhone IS NOT NULL) AND (orderdetail.hire = true) AND 
				(orderdetail.ReturnActualDate IS NULL) AND (orderdetail.CancelReminderMessages = false) AND 
				(DATE_ADD(ReturnExpectedDate, INTERVAL 1 DAY) < Now()) AND (remindersent.ReminderSentID IS NULL)";

			$secondRemindersToSend = "SELECT orderdetail.OrderDetailID, customer.MobilePhone, order.OrderID, customer.Firstname, 
				2 AS ReminderID, remindersent.SentDateTime FROM (customer INNER JOIN `order` ON customer.CustomerID = order.CustomerID) 
				INNER JOIN (((SELECT ReminderSentID, OrderDetailID, ReminderID FROM remindersent WHERE ReminderID=2)  
				AS Reminder2Sent RIGHT JOIN orderdetail ON Reminder2Sent.OrderDetailID = orderdetail.OrderDetailID) 
				INNER JOIN remindersent ON orderdetail.OrderDetailID = remindersent.OrderDetailID) ON order.OrderID = orderdetail.OrderID
				WHERE (((customer.MobilePhone) Is Not Null) AND ((orderdetail.Hire)=True) AND ((orderdetail.ReturnActualDate) Is Null) 
				AND ((orderdetail.CancelReminderMessages)=False) AND 
				(DATE_ADD(remindersent.SentDateTime, INTERVAL ".$intervalSecond." DAY)<=Now()) AND 
				((remindersent.ReminderID)=1) AND ((Reminder2Sent.ReminderSentID) Is Null))"; 
			$strSQL = "SELECT T1.OrderDetailID, T1.MobilePhone, T1.OrderID, T1.Firstname, T1.ReminderID  
	                    FROM (".$firstRemindersToSend.") T1 
	                    UNION  
	                    SELECT T2.OrderDetailID, T2.MobilePhone, T2.OrderID, T2.Firstname, T2.ReminderID 
	                    FROM (".$secondRemindersToSend.") T2" ;  
	        if (!$query = $this->_db->query($strSQL))
			{
				die('#5 - There was an error running the query [' . $this->_db->error . ']');
				return false;
			}  
			if (!$remindersSent = $this->_db->query("SELECT * FROM remindersent")) 
			{
				die('#6 - There was an error running the query [' . $this->_db->error . ']');
				return false;
			}; 
			if (!$msgOneQuery = $this->_db->query("SELECT MessageText FROM reminder WHERE ReminderID = 1"))
			{
				die('#7 - There was an error running the query [' . $this->_db->error . ']');
				return false;
			}
			if (!$msgTwoQuery = $this->_db->query("SELECT MessageText FROM reminder WHERE ReminderID = 2"))
			{
				die('#8 - There was an error running the query [' . $this->_db->error . ']');
				return false;
			} 
			if ($msgOneQuery->num_rows > 0 && $msgTwoQuery->num_rows > 0)
			{
				$msgOne = $msgOneQuery->fetch_assoc();
				$msgTwo = $msgTwoQuery->fetch_assoc();
				$astrMessageText[1] = $msgOne['MessageText']; 
				$astrMessageText[2] = $msgTwo['MessageText'];    
				while ($toSend = $query->fetch_assoc())
				{ 
					$finalMessage = sprintf($astrMessageText[$toSend['ReminderID']], $toSend['Firstname'], $toSend['OrderID']);
					if ($this->sendSMSWithReplyToEmail($toSend['MobilePhone'], $finalMessage))
					{ 
						$insertData['OrderDetailID'] = $toSend['OrderDetailID'];
						$insertData['ReminderID'] = $toSend['ReminderID'];
						$insertData['SentDateTime'] = date('Y-m-d H:i:s'); 
						print_r($insertData);
						echo "INSERTED<br/>";
						//$this->db->insert('remindersent',$insertData);
						/*
						add to reminder sent table
						*/
					} 
				}
				return true;
			} 
			$msgOneQuery->free();
			$msgTwoQuery->free();

	    }
	    return false;
	}
	
	
	private function convertDate($dateToConvert)
	{
		$dateParts = explode(' ', $dateToConvert);
		$timeString = '';
		if (sizeof($dateParts) > 1)
		{
			$timeParts = explode(':',$dateParts[1]);
			if (sizeof($timeParts) > 2)
			{
				$ampm = 'am';
				$h = (int)$timeParts[0];
				if ($h > 12)
				{
					$h = $h - 12;
					$ampm = 'pm';
				}
				$m = $timeParts[1];
				$timeString = $h.':'.$m.' '.$ampm;
			}
		}
		return $timeString;
	}

	private function sendSMSWithReplyToEmail($mobileNumber, $finalMessage)
	{
	    $strUsername = "TensAustralia";
	    $strPassword = "674429";
	    $strReplyEmailAddress = "info@tensaustralia.com";
	    $finalMessage = urlencode($finalMessage);
	    $strURL = "http://www.messagenet.com.au/dotnet/lodge.asmx/LodgeSMSMessageWithReply?Username=".$strUsername.
	    	"&Pwd=".$strPassword."&PhoneNumber=".$mobileNumber."&PhoneMessage=".$finalMessage."&ReplyType=EMAIL&ReplyPath="
	    	.$strReplyEmailAddress;
	    	echo $strURL."<br/><br/>";
	    /* Script URL */
	    //$url = 'http://www.messagenet.com.au/dotnet/lodge.asmx/LodgeSMSMessageWithReply';
	    $url = "http://tensaustralia.com.au";

	    /* $_GET Parameters to Send */ 
	    $params = array(
	    	'Username' => $strUsername, 
	    	'Pwd' => $strPassword,
	    	'PhoneNumber' => $mobileNumber,
	    	'PhoneMessage' => $finalMessage,
	    	'ReplyType' => 'EMAIL',
	    	'ReplyPath' => $strReplyEmailAddress); 
	}

}

$smsReminder = new SmsReminder();
$smsReminder->sendReminders();

?>