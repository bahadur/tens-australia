'use strict';

function YoutubeApi(jQuery) {
  var apiKey = 'AIzaSyBqtbdTc_cVQvUsmeUIf0xL9liPSBAygnU';
  var videoURL = 'http://www.youtube.com/watch?v='; 
  var pageSize = 5;
  var page = 1;
  var total = 0;
  var videoElements = []; 
  var activePlayVideoElement = null;
  var nextPageButton = null;
  var prevPageButton = null;
  var nextPageToken = null;
  var prevPageToken = null;
  var playListId = null;


  /** Converts seconds to a time string */
  var secondsToTime = function(secs) {
    var hours = Math.floor(secs / (60 * 60));

    var divisor_for_minutes = secs % (60 * 60);
    var minutes = Math.floor(divisor_for_minutes / 60);

    var divisor_for_seconds = divisor_for_minutes % 60;
    var seconds = Math.ceil(divisor_for_seconds);

    if (hours > 0)
      return hours + ':' + minutes + ':' + seconds;
    else
      return minutes + ':' + seconds;
  };

  var getListItem = function(itemData, active) { 
    var view = '';
    if (itemData.snippet) {
      var feedTitle = itemData.snippet.title; 
      var videoID = itemData.snippet.resourceId.videoId;  
      var thumb = '';
      if (itemData.snippet.thumbnails) {
        thumb = itemData.snippet.thumbnails.medium.url;
      } 

      var isActive = function() {
        if (active)
          return 'ibacordotcom-vid-active';
        else
          return '';
      }; 

      view = jQuery('<div class="ibacordotcom-play ' + isActive() + '" data-vvv="' + videoID + '">' +
                        '<div class="ibacordotcom_youtube_thumb"><img src="' + thumb + '" alt="ibacor">' +
                            '<span class="ibacordotcom-vid-tm0"></span>' +
                        '</div>' + feedTitle + '</div>');
      view.click(function() { 
        jQuery('.ibacordotcom-play').removeClass('ibacordotcom-vid-active'); 
        jQuery(this).addClass('ibacordotcom-vid-active');
        activePlayVideoElement.html('<iframe src="http://www.youtube.com/embed/' +
          jQuery(this).attr('data-vvv') + '" allowfullscreen="" frameborder="0" class="ibacordotcom-vid-iframe"></iframe>');
      });

    } 
    return view;
  }; 
 
  var firstTime = true;

  var getYoutubeVideos = function(playListID, pageToken) { 
    var playListURL = 'https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&playlistId=' +
      playListID + '&key=' + apiKey;
    if (pageToken) {
      playListURL += '&pageToken=' + pageToken;
    }
    playListId = playListID;
    jQuery.getJSON(playListURL, function(data) { 
      prevPageToken = data.prevPageToken;
      nextPageToken = data.nextPageToken;
      if (firstTime) { 
        var videoID = data.items[0].snippet.resourceId.videoId;
        activePlayVideoElement.html('<iframe src="http://www.youtube.com/embed/' + 
          videoID + '" allowfullscreen="" frameborder="0" class="ibacordotcom-vid-iframe"></iframe>');
      }
      videoElements.children().remove();
      // Need to append with jQuery, not fucking hardcoding html 
      jQuery.each(data.items, function(i, item) {
        var active = firstTime ? true : false;
        videoElements.append(getListItem(item, active));
        firstTime = false;
      }); 
      //showPage(page);
    });
  };


  /**
   * Call this on the document being ready
   * @return {[type]} [description]
   */
  this.initDom = function(playListID) {
    videoElements = jQuery('.ibacordotcom-vid-bottom'); 
    activePlayVideoElement = jQuery('.ibacordotcom_vid_play');
    nextPageButton = jQuery('.ibacordotcom_vid_next');
    prevPageButton = jQuery('.ibacordotcom_vid_prev'); 

    nextPageButton.on('click', function() { 
      if (nextPageToken) {
        getYoutubeVideos(playListId, nextPageToken);
      }
    });

    prevPageButton.on('click', function() { 
      if (prevPageToken) {
        getYoutubeVideos(playListId, prevPageToken);
      }
    });
    getYoutubeVideos(playListID);
  };
}
  