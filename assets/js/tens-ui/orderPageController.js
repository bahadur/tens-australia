'use strict';

function stopRKey(evt) { 
  var evt = (evt) ? evt : ((event) ? event : null); 
  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null); 
  if ((evt.keyCode == 13) && (node.type=="text"))  {return false;} 
} 

document.onkeypress = stopRKey; 

function orderPageController() {
    var maxIdx = 1;

    var firstTab = jQuery('#collapseOne');
    var firstTabHeading = jQuery('#firstTabHeading');
    var firstTabButton = jQuery('#firstTabButton');

    var secondTab = jQuery('#collapseTwo');
    var secondTabHeading = jQuery('#secondTabHeading');
    var secondTabButton = jQuery('#secondTabButton');

    var thirdTab = jQuery('#collapseThree');
    var thirdTabHeading = jQuery('#thirdTabHeading');
    var thirdTabButton = jQuery('#thirdTabButton');

    var fourthTab = jQuery('#collapseFour');
    var fourthTabHeading = jQuery('#fourthTabHeading');
    var fourthTabButton = jQuery('#fourthTabButton');

    var fifthTab = jQuery('#collapseFive');
    var fifthTabHeading = jQuery('#fifthTabHeading');
    var fifthTabButton = jQuery('#fifthTabButton');

    var sixthTab = jQuery('#collapseSix');
    var sixthTabHeading = jQuery('#sixthTabHeading'); 

    var currentTab = firstTab;

    var isValidEmailAddress = function(emailAddress) {
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        return pattern.test(emailAddress);
    };

    var isValidPhone = function(value) {
        var strippedValue = value.replace(' ','');
        strippedValue = strippedValue.replace('-','');
        strippedValue = strippedValue.replace('+','');
        strippedValue = strippedValue.replace('(','');
        strippedValue = strippedValue.replace(')','');
        return strippedValue.match(/^[0-9]+$/) !== null;
    };

    var anyInputErrors = function(jInput) {
        var rule = jInput.attr('data-validate-type');
        if (rule === 'email') {
            if (!isValidEmailAddress(jInput.val())) {
                return 'Invalid email';
            } else {
                return null;
            }
        } else if (rule === 'phone') {
            if (!isValidPhone(jInput.val())) {
                return 'Invalid phone number';
            } else {
                return null;
            }
        } else if (rule === 'date') {
            if (jInput.val().length === 0) {
                return 'Invalid date';
            } else {
                return null;
            } 
        } else if (rule === 'mobile') {
            if (!isValidPhone(jInput.val())) {
                return 'Invalid mobile number';
            } else {
                return null;
            }
        } else if (jInput.val().length === 0) {
            return 'Please enter value';
        } else {
            return null;
        }
    };

    var validInputs = function(tab) {
        var inputs = tab.find('input'); 
        var validInputs = true; 
        for (var k = 0; k < inputs.length; k++) { 
            var jInput = jQuery(inputs[k]);  
            if (jInput.attr('data-validate') === 'required') { 
                var errormessage = anyInputErrors(jInput);
                if (errormessage) {
                    validInputs = false;
                    // Find the neighbour span
                    var parentDiv = jInput.parent();
                    parentDiv.addClass('validate-has-error');
                    var span = jInput.next('span.error-label'); 
                    if (span.length === 0) {  
                        span = jInput.parent().parent().find('span.error-label');
                    }
                    span.show();
                    span.html(errormessage);  
                } else {
                    // Find the neighbour span
                    var parentDiv = jInput.parent();
                    parentDiv.removeClass('validate-has-error');
                    var span = jInput.next('span.error-label'); 
                    if (span.length === 0) { 
                        span = jInput.parent().parent().find('span.error-label');
                    } 
                    span.hide();
                }
            }
        };
        var selects = tab.find('select');
        if (selects) {
            var jSelect = jQuery(selects);
            if (jSelect.attr('data-validate') === 'required') {
                if (!jSelect.val()) {
                    validInputs = false;
                    var parentDiv = jSelect.parent();
                    parentDiv.addClass('validate-has-error');
                    var span = jSelect.next('span.error-label');  
                    span.show();
                    span.html('Please enter value'); 
                } else {
                    // Find the neighbour span
                    var parentDiv = jSelect.parent();
                    parentDiv.removeClass('validate-has-error');
                    var span = jSelect.next('span.error-label');  
                    // hide it
                    span.hide();
                }
            }
        }
        var agreement = tab.find('input#toc-agreement');
        if (agreement.length > 0) {
            var jAgreement = jQuery(agreement);
            if (jAgreement.is(':checked')) {
                // Find the neighbour span 
                var span = jAgreement.next('p#toc-error');  
                // hide it
                span.hide();
            } else {
                validInputs = false; 
                var span = jAgreement.next('p#toc-error');  
                span.show();
                span.html('You must agree to the terms of conditions to proceed'); 
            }
        }
        return validInputs;
    };

    var firstTabClicked = function() { 
        if (currentTab != firstTab && maxIdx >= 1) { 
            currentTab.collapse('hide');
            currentTab = firstTab;
            firstTab.collapse('show'); 
        } 
    };

    var secondTabClicked = function() {
        if (currentTab != secondTab && maxIdx >= 2) {  
            currentTab.collapse('hide');
            currentTab = secondTab;
            secondTab.collapse('show'); 
        } 
    }

    var thirdTabClicked = function() {
        if (currentTab != thirdTab && maxIdx >= 3) {  
            currentTab.collapse('hide');
            currentTab = thirdTab;
            thirdTab.collapse('show'); 
        } 
    }

    var fourthTabClicked = function() {
        if (currentTab != fourthTab && maxIdx >= 4) { 
            currentTab.collapse('hide');
            currentTab = fourthTab;
            fourthTab.collapse('show'); 
        } 
    }

    var fifthTabClicked = function() {
        if (currentTab != fifthTab && maxIdx >= 5) {  
            currentTab.collapse('hide');
            currentTab = fifthTab;
            fifthTab.collapse('show'); 
        } 
    }

    var sixthTabClicked = function() { 
        if (currentTab != sixthTab && maxIdx >= 6) { 
            currentTab.collapse('hide');
            currentTab = sixthTab;
            sixthTab.collapse('show');
        } 
    }

    var firstButtonClicked = function(e) {
        if (validInputs(firstTab)) {
            if (maxIdx == 1) { 
                secondTabHeading.find('h4').removeClass('panel-title-disabled'); 
                maxIdx++;
            }
            secondTabClicked();
        }
    } 

    var copyBillingToShipping = function() { 
        jQuery(thirdTab.find('input#shippingAddress')).val(
            jQuery(secondTab.find('input#billingAddress1')).val());
        jQuery(thirdTab.find('input#shippingAddress2')).val(
            jQuery(secondTab.find('input#billingAddress2')).val());
        jQuery(thirdTab.find('input#shippingSuburb')).val(
            jQuery(secondTab.find('input#billingSuburb')).val());
        jQuery(thirdTab.find('input#shippingState')).val(
            jQuery(secondTab.find('input#billingState')).val());
        jQuery(thirdTab.find('input#shippingPostcode')).val(
            jQuery(secondTab.find('input#billingPostcode')).val()); 
    }

    var secondButtonClicked = function(e) {
        if (validInputs(secondTab)) { 
            var shipToSameAddress = jQuery(secondTab.find('select')).val();
            if (shipToSameAddress == 1) {
                if (maxIdx == 2 || maxIdx == 3) {
                    thirdTabHeading.find('h4').removeClass('panel-title-disabled'); 
                    fourthTabHeading.find('h4').removeClass('panel-title-disabled');
                    maxIdx+=2;
                    copyBillingToShipping();
                }
                fourthTabClicked();
            } else {
                if (maxIdx == 2 || maxIdx == 3) {
                    thirdTabHeading.find('h4').removeClass('panel-title-disabled'); 
                    maxIdx++;
                }
                thirdTabClicked();
            } 
        }
    } 

    var thirdButtonClicked = function(e) {
        if (validInputs(thirdTab)) {
            if (maxIdx == 3) { 
                fourthTabHeading.find('h4').removeClass('panel-title-disabled'); 
                maxIdx++;
            }
            fourthTabClicked();
        }
    } 

    var fourthButtonClicked = function(e) {
        if (validInputs(fourthTab)) {
            if (maxIdx == 4) { 
                fifthTabHeading.find('h4').removeClass('panel-title-disabled'); 
                maxIdx++;
            }
            fifthTabClicked();
        }
    }
 
    var fifthButtonClicked = function(e) {
        if (validInputs(fifthTab)) {
            if (maxIdx == 5) { 
                sixthTabHeading.find('h4').removeClass('panel-title-disabled'); 
                maxIdx++;
            }
            sixthTabClicked();
        } 
    } 

    function drawHiddenMain() {
        var hiddenMainProduct = $(document).find('#mainProduct');
        if (hiddenMainProduct) {
            var productId = hiddenMainProduct.attr('tens-main-id');
            var productQuantity = hiddenMainProduct.attr('tens-main-quantity');
            var productPrice = hiddenMainProduct.attr('tens-main-price'); 
            var html = $("<input name='orderProducts[0][productid]' value='" + productId + "'' type='hidden'/>" +
                        "<input name='orderProducts[0][productquantity]' value='" + productQuantity + "' type='hidden'/>" +
                        "<input name='orderProducts[0][unitprice]' value='" + productPrice + "' type='hidden'/>");
            hiddenMainProduct.append(html);
        }
    }
    /** Custom form **/
    function drawTable() {
        var table = $(document).find('table.order-products'); 
        var rows = table.find('tr'); 
        for (var i = 1; i < rows.length; i++)
        {
            var index = i;
            var row  = $(rows[i]); 
            var rowClass = row.prop('class'); 
            var code = row.attr('td-data-code');
            var description = row.attr('td-data-description');
            var defaultValue = row.attr('td-default-value');
            var minValue = row.attr('td-min-value');
            var id = row.attr('td-data-id');
            var price = row.attr('td-data-price'); 
            var totalPrice = "$" + (defaultValue * price).toFixed(2);
            price = "$" + (1 * price).toFixed(2);
            if (rowClass.indexOf('number') != -1 && rowClass.indexOf('checkboxnumber') == -1) {
                var rowInner = $('<td><input type="hidden" name=\'orderProducts[' + index + '][productid]\' value="' + id + '"></input></td>' +
                                                '<td>' + code + '</td>' +
                                                '<td>' + description + '</td>' +
                                                '<td style="width:15px"><input type="text" name=\'orderProducts[' + index + '][productquantity]\' class="spinner" style="width:25px" value="' + defaultValue + '"></input></td>' +
                                                '<td>X</td>' +
                                                '<td id="price' + index + '"><input type="hidden" name=\'orderProducts[' + index + '][unitprice]\' value="' + price + '"></input>' + price + '</td>' +
                                                '<td id="totalPrice' + index + '">' + totalPrice + '</td>');  
                row.append(rowInner);   
                row.find('.spinner').spinner({
                    min : minValue,
                    stop: function(event, ui) { 
                        var td = $(this).parent().parent();
                        var tr = td.parent(); 
                        var price = tr.attr('td-data-price');  
                        td.siblings().last().html('$' + (price * $(this).attr('aria-valuenow') * 1).toFixed(2));  
                    },
                    change: function(event, ui) {
                        var td = $(this).parent().parent();
                        var tr = td.parent(); 
                        var price = tr.attr('td-data-price');  
                        td.siblings().last().html('$' + (price * $(this).attr('aria-valuenow') * 1).toFixed(2));  
                    } 
                });
            }
            else if (rowClass.indexOf('checkboxnumber') != -1) { 
                var rowInner = $('<td><input type="checkbox" name=\'orderProducts[' + index + '][productid]\' value="' + id + '"></input></td>' +
                                                '<td>' + code + '</td>' +
                                                '<td>' + description + '</td>' +
                                                '<td style="width:15px"><input type="text" class="spinner" name=\'orderProducts[' + index + '][productquantity]\' style="width:25px" value="' + defaultValue + '"></input></td>' +
                                                '<td>X</td>' +
                                                '<td id="price' + index + '"><input type="hidden" name=\'orderProducts[' + index + '][unitprice]\' value="' + price + '"></input>' + price + '</td>' +
                                                '<td id="totalPrice' + index + '">' + totalPrice + '</td>'); 
                row.append(rowInner);    
                row.find('.spinner').spinner({
                    min : minValue,
                    stop: function(event, ui) {
                        var td = $(this).parent().parent();
                        var tr = td.parent(); 
                        var price = tr.attr('td-data-price');  
                        td.siblings().last().html('$' + (price * $(this).attr('aria-valuenow') * 1).toFixed(2));  
                    },
                    change: function(event, ui) {
                        var td = $(this).parent().parent();
                        var tr = td.parent(); 
                        var price = tr.attr('td-data-price');  
                        td.siblings().last().html('$' + (price * $(this).attr('aria-valuenow') * 1).toFixed(2));  
                    } 

                });
            }
            else if (rowClass.indexOf('fixedcheckbox') != -1) {
                var rowInner = $('<td><input type="checkbox" name="orderProducts[' + index + '][productid]" value="' + id + '"></input></td><td></td>' +
                                                '<td><input type="hidden" name="orderProducts[' + index + '][productquantity]" value="1"></input>' + description + '</td><td></td><td></td>' + 
                                                '<td><input type="hidden" name=\'orderProducts[' + index + '][unitprice]\' value="' + price + '"></input>' + price + '</td><td></td>');
                row.append(rowInner);  
            }
            else if (rowClass.indexOf('tensradio') != -1) {
                
                var rowInner = $('<td><input type="radio" name="orderProducts[' + index + '][productid]" value="' + id + '"></input><input type="hidden" name="orderProducts[' + index + '][productquantity]" value="1"></input></td>' +
                                                '<td>' + code + '</td>' +
                                                '<td>' + description + '</td>' +
                                                '<td style="width:15px"><input type="hidden" name=\'orderProducts[' + index + '][unitprice]\' value="' + price + '"></input></td>' +
                                                '<td></td>' +
                                                '<td></td>' +
                                                '<td></td>');
                row.append(rowInner);  
            }
        }
    }


    this.initController = function() { 
        // Initialize the validation of all elements in all tabs
        // 1. Disable the tab panels depending on the current max Panel  [There is probably a way to optimize this code] 
        firstTabHeading.click(function(e) { firstTabClicked(); });
        secondTabHeading.click(function(e) { secondTabClicked(); });
        thirdTabHeading.click(function(e) { thirdTabClicked(); });
        fourthTabHeading.click(function(e) { fourthTabClicked(); });
        fifthTabHeading.click(function(e) { fifthTabClicked(); });
        sixthTabHeading.click(function(e) { sixthTabClicked(); });

        // 2. Attach events to "Continue" buttons
        firstTabButton.click(firstButtonClicked);
        secondTabButton.click(secondButtonClicked);
        thirdTabButton.click(thirdButtonClicked);
        fourthTabButton.click(fourthButtonClicked);
        fifthTabButton.click(fifthButtonClicked);

        drawHiddenMain();
        drawTable();
        var dd = fourthTab.find('input#dueDate');
        if (dd.length > 0) {
            dd.datepicker({format:'dd/mm/yyyy'});
        }
        fourthTab.find('input#requiredDate').datepicker({format:'dd/mm/yyyy'});


        jQuery('#submit-btn').on('click', function(e) {
            if (validInputs(firstTab) && validInputs(secondTab) && validInputs(thirdTab) && validInputs(fourthTab) && validInputs(fifthTab)) {
                jQuery('#orderForm').submit();
            }
        });
    }; 
}
