'use strict';
/*
*
*/ 

$(document).ready(function() {

	function drawHiddenMain() {
		var hiddenMainProduct = $(document).find('#mainProduct');
		if (hiddenMainProduct) {
			var productId = hiddenMainProduct.attr('tens-main-id');
			var productQuantity = hiddenMainProduct.attr('tens-main-quantity');
			var productPrice = hiddenMainProduct.attr('tens-main-price'); 
			var html = $("<input name='orderProducts[0][productid]' value='" + productId + "'' type='hidden'/>" +
	                    "<input name='orderProducts[0][productquantity]' value='" + productQuantity + "' type='hidden'/>" +
	                    "<input name='orderProducts[0][unitprice]' value='" + productPrice + "' type='hidden'/>");
			hiddenMainProduct.append(html);
		}
	}
	/** Custom form **/
	function drawTable() {
		var table = $(document).find('table.order-products'); 
		var rows = table.find('tr'); 
		for (var i = 1; i < rows.length; i++)
		{
			var index = i;
			var row  = $(rows[i]); 
			var rowClass = row.prop('class'); 
			var code = row.attr('td-data-code');
			var description = row.attr('td-data-description');
			var defaultValue = row.attr('td-default-value');
			var minValue = row.attr('td-min-value');
			var id = row.attr('td-data-id');
			var price = row.attr('td-data-price'); 
			var totalPrice = '$' + (defaultValue * price).toFixed(2);
			price = '$' + (1 * price).toFixed(2);
			if (rowClass.indexOf('number') != -1 && rowClass.indexOf('checkboxnumber') == -1) {
				var rowInner = $('<td><input type="hidden" name=\'orderProducts[' + index + '][productid]\' value="' + id + '"></input></td>' +
	                                            '<td>' + code + '</td>' +
	                                            '<td>' + description + '</td>' +
	                                            '<td style="width:15px"><input type="text" name=\'orderProducts[' + index + '][productquantity]\' class="spinner" style="width:25px" value="' + defaultValue + '"></input></td>' +
	                                            '<td>X</td>' +
	                                            '<td id="price' + index + '"><input type="hidden" name=\'orderProducts[' + index + '][unitprice]\' value="' + price + '"></input>' + price + '</td>' +
	                                            '<td id="totalPrice' + index + '">' + totalPrice + '</td>');  
				row.append(rowInner);   
				row.find('.spinner').spinner({
					min : minValue,
				    stop: function(event, ui) { 
				    	var td = $(this).parent().parent();
				    	var tr = td.parent(); 
							var price = tr.attr('td-data-price');  
				    	td.siblings().last().html('$' + (price * $(this).attr('aria-valuenow') * 1).toFixed(2));  
				    },
			    	change: function(event, ui) {
				    	var td = $(this).parent().parent();
				    	var tr = td.parent(); 
						var price = tr.attr('td-data-price');  
				    	td.siblings().last().html('$' + (price * $(this).attr('aria-valuenow') * 1).toFixed(2));  
			    	} 
				});
			}
			else if (rowClass.indexOf('checkboxnumber') != -1) { 
				var rowInner = $('<td><input type="checkbox" name=\'orderProducts[' + index + '][productid]\' value="' + id + '"></input></td>' +
	                                            '<td>' + code + '</td>' +
	                                            '<td>' + description + '</td>' +
	                                            '<td style="width:15px"><input type="text" class="spinner" name=\'orderProducts[' + index + '][productquantity]\' style="width:25px" value="' + defaultValue + '"></input></td>' +
	                                            '<td>X</td>' +
	                                            '<td id="price' + index + '"><input type="hidden" name=\'orderProducts[' + index + '][unitprice]\' value="' + price + '"></input>' + price + '</td>' +
	                                            '<td id="totalPrice' + index + '">' + totalPrice + '</td>'); 
				row.append(rowInner);    
				row.find('.spinner').spinner({
					min : minValue,
				    stop: function(event, ui) {
				    	var td = $(this).parent().parent();
				    	var tr = td.parent(); 
						var price = tr.attr('td-data-price');  
				    	td.siblings().last().html('$' + (price * $(this).attr('aria-valuenow') * 1).toFixed(2));  
				    },
			    	change: function(event, ui) {
				    	var td = $(this).parent().parent();
				    	var tr = td.parent(); 
						var price = tr.attr('td-data-price');  
				    	td.siblings().last().html('$' + (price * $(this).attr('aria-valuenow') * 1).toFixed(2));  
			    	} 

				});
			}
			else if (rowClass.indexOf('fixedcheckbox') != -1) {
				var rowInner = $('<td><input type="checkbox" name="orderProducts[' + index + '][productid]" value="' + id + '"></input></td><td></td>' +
	                                            '<td><input type="hidden" name="orderProducts[' + index + '][productquantity]" value="1"></input>' + description + '</td><td></td><td></td>' + 
	                                            '<td><input type="hidden" name=\'orderProducts[' + index + '][unitprice]\' value="' + price + '"></input>' + price + '</td><td></td>');
				row.append(rowInner);  
			}
			else if (rowClass.indexOf('tensradio') != -1) {
				
				var rowInner = $('<td><input type="radio" name="orderProducts[' + index + '][productid]" value="' + id + '"></input><input type="hidden" name="orderProducts[' + index + '][productquantity]" value="1"></input></td>' +
	                                            '<td>' + code + '</td>' +
	                                            '<td>' + description + '</td>' +
	                                            '<td style="width:15px"><input type="hidden" name=\'orderProducts[' + index + '][unitprice]\' value="' + price + '"></input></td>' +
	                                            '<td></td>' +
	                                            '<td></td>' +
	                                            '<td></td>');
				row.append(rowInner);  
			}
		}
	}

	drawHiddenMain();
	drawTable();
})