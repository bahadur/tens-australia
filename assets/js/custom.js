

;(function($, window, undefined){

    "use strict";

    $(document).ready(function()
    {
        InitValidator();
        InitDatepicker();
        InitTimepicker();
        InitInputMask();
        // Multi-select
        if($.isFunction($.fn.multiSelect))
        {
            $(".multi-select").multiSelect();
        }

    });

})(jQuery, window);


/* Functions */

// Form Validation
function InitValidator(){
    if($.isFunction($.fn.validate))
    {
        $("form.validate").each(function(i, el)
        {
            var $this = $(el),
                opts = {
                    rules: {},
                    messages: {},
                    errorElement: 'span',
                    errorClass: 'validate-has-error',
                    highlight: function (element) {
                        $(element).closest('.form-group').addClass('validate-has-error');
                    },
                    unhighlight: function (element) {
                        $(element).closest('.form-group').removeClass('validate-has-error');
                    },
                    errorPlacement: function (error, element)
                    {
                        if(element.closest('.has-switch').length)
                        {
                            error.insertAfter(element.closest('.has-switch'));
                        }
                        else
                        if(element.parent('.checkbox, .radio').length || element.parent('.input-group').length)
                        {
                            error.insertAfter(element.parent());
                        }
                        else
                        {
                            error.insertAfter(element);
                        }
                    }
                },
                $fields = $this.find('[data-validate]');


            $fields.each(function(j, el2)
            {
                var $field = $(el2),
                    name = $field.attr('name'),
                    validate = attrDefault($field, 'validate', '').toString(),
                    _validate = validate.split(',');

                for(var k in _validate)
                {
                    var rule = _validate[k],
                        params,
                        message;

                    if(typeof opts['rules'][name] == 'undefined')
                    {
                        opts['rules'][name] = {};
                        opts['messages'][name] = {};
                    }

                    if($.inArray(rule, ['required', 'url', 'email', 'number', 'date', 'creditcard']) != -1)
                    {
                        opts['rules'][name][rule] = true;

                        message = $field.data('message-' + rule);

                        if(message)
                        {
                            opts['messages'][name][rule] = message;
                        }
                    }
                    // Parameter Value (#1 parameter)
                    else
                    if(params = rule.match(/(\w+)\[(.*?)\]/i))
                    {
                        if($.inArray(params[1], ['min', 'max', 'minlength', 'maxlength', 'equalTo']) != -1)
                        {
                            opts['rules'][name][params[1]] = params[2];


                            message = $field.data('message-' + params[1]);

                            if(message)
                            {
                                opts['messages'][name][params[1]] = message;
                            }
                        }
                    }
                }
            });

            console.log( opts );
            $this.validate(opts);
        });
    }
}

function InitDatepicker(){
    // Datepicker
    if($.isFunction($.fn.datepicker))
    {
        $(".datepicker").each(function(i, el)
        {
            var $this = $(el),
                opts = {
                    format: attrDefault($this, 'format', 'mm/dd/yyyy'),
                    startDate: attrDefault($this, 'startDate', ''),
                    endDate: attrDefault($this, 'endDate', ''),
                    daysOfWeekDisabled: attrDefault($this, 'disabledDays', ''),
                    startView: attrDefault($this, 'startView', 0),
                    rtl: rtl()
                },
                $n = $this.next(),
                $p = $this.prev();

            $this.datepicker(opts);

            if($n.is('.input-group-addon') && $n.has('a'))
            {
                $n.on('click', function(ev)
                {
                    ev.preventDefault();

                    $this.datepicker('show');
                });
            }

            if($p.is('.input-group-addon') && $p.has('a'))
            {
                $p.on('click', function(ev)
                {
                    ev.preventDefault();

                    $this.datepicker('show');
                });
            }
        });
    }

}

function InitTimepicker(){

    // Timepicker
    if($.isFunction($.fn.timepicker))
    {
        $(".timepicker").each(function(i, el)
        {
            var $this = $(el),
                opts = {
                    template: attrDefault($this, 'template', false),
                    showSeconds: attrDefault($this, 'showSeconds', false),
                    defaultTime: attrDefault($this, 'defaultTime', 'current'),
                    showMeridian: attrDefault($this, 'showMeridian', true),
                    minuteStep: attrDefault($this, 'minuteStep', 15),
                    secondStep: attrDefault($this, 'secondStep', 15)
                },
                $n = $this.next(),
                $p = $this.prev();

            $this.timepicker(opts);

            if($n.is('.input-group-addon') && $n.has('a'))
            {
                $n.on('click', function(ev)
                {
                    ev.preventDefault();

                    $this.timepicker('showWidget');
                });
            }

            if($p.is('.input-group-addon') && $p.has('a'))
            {
                $p.on('click', function(ev)
                {
                    ev.preventDefault();

                    $this.timepicker('showWidget');
                });
            }
        });
    }
}

function InitInputMask(){
    // Input Mask
    if($.isFunction($.fn.inputmask))
    {
        $("[data-mask]").each(function(i, el)
        {
            var $this = $(el),
                mask = $this.data('mask').toString(),
                opts = {
                    numericInput: attrDefault($this, 'numeric', false),
                    radixPoint: attrDefault($this, 'radixPoint', ''),
                    rightAlignNumerics: attrDefault($this, 'numericAlign', 'left') == 'right'
                },
                placeholder = attrDefault($this, 'placeholder', ''),
                is_regex = attrDefault($this, 'isRegex', '');


            if(placeholder.length)
            {
                opts[placeholder] = placeholder;
            }

            switch(mask.toLowerCase())
            {
                case "phone":
                    mask = "(999) 999-9999";
                    break;

                case "currency":
                case "rcurrency":

                    var sign = attrDefault($this, 'sign', '$');;

                    mask = "999,999,999.99";

                    if($this.data('mask').toLowerCase() == 'rcurrency')
                    {
                        mask += ' ' + sign;
                    }
                    else
                    {
                        mask = sign + ' ' + mask;
                    }

                    opts.numericInput = true;
                    opts.rightAlignNumerics = false;
                    opts.radixPoint = '.';
                    break;

                case "email":
                    mask = 'Regex';
                    opts.regex = "[a-zA-Z0-9._%-]+@[a-zA-Z0-9-]+\\.[a-zA-Z]{2,4}";
                    break;

                case "fdecimal":
                    mask = 'decimal';
                    $.extend(opts, {
                        autoGroup		: true,
                        groupSize		: 3,
                        radixPoint		: attrDefault($this, 'rad', '.'),
                        groupSeparator	: attrDefault($this, 'dec', ',')
                    });
            }

            if(is_regex)
            {
                opts.regex = mask;
                mask = 'Regex';
            }

            $this.inputmask(mask, opts);
        });
    }

}

// Element Attribute Helper
function attrDefault($el, data_var, default_val)
{
    if(typeof $el.data(data_var) != 'undefined')
    {
        return $el.data(data_var);
    }

    return default_val;
}