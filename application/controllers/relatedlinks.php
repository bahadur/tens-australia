<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class RelatedLinks extends CI_Controller{

	function __construct(){

		parent::__construct(); 

	}

	function index(){

        $this->load->view('ver2/blocks/header');
        $this->load->view('ver2/blocks/top-nav');
        $this->load->view("ver2/relatedlinks.php");	
        $this->load->view('ver2/blocks/footer');  
		
	}


}


 