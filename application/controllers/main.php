<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//main controller: this controller load the main page of the project
class Main extends CI_Controller {
	
      public function __construct(){
            parent::__construct();
           
            
            
      }

      function index(){
            //$this->layouts->add_include('assets/style.css');
            //$this->layouts->add_include('assets/jquery.js');
            
            $this->load->view('ver2/blocks/header');
            $this->load->view('ver2/blocks/top-nav');
            $this->load->view('ver2/home');
            $this->load->view('ver2/blocks/footer');
             
	}

      function contactus(){
            
            $data = array();

            if ($message = $this->session->flashdata('message')) {
                  $data["message"] = $message;
            }

            if($this->input->post()){

                  $privatekey = "6LdmLAcTAAAAADpz3BCP5_Fltx3DRk-Tfl_5v_D5";

            // reCaptcha looks for the POST to confirm
            
                  $captcha = $this->input->post("g-recaptcha-response");

                  if(!$captcha){
                     echo json_encode(array("msg" => "Please check the the captcha form."));
                     
                     exit;
                  } 

                  
                  $recaptcha = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$privatekey&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
                  if($recaptcha["success"]){ 
                        $this->load->library('email');


                        $msg = "<ul>";
                        $msg .= "<li><strong>First Name:</strong> "    .$this->input->post("first_name");
                        $msg .= "<li><strong>Last Name:</strong> "     .$this->input->post("last_name");
                        $msg .= "<li><strong>Email:</strong> "         .$this->input->post("email");
                        $msg .= "<li><strong>Company Name:</strong> "  .$this->input->post("company_name");
                        $msg .= "<li><strong>Website:</strong> "       .$this->input->post("website");
                        $msg .= "<li><strong>Phone:</strong> "         .$this->input->post("phone");
                        $msg .= "<li><strong>Fax:</strong> "           .$this->input->post("fax");
                        $msg .= "<li><strong>Street:</strong> "        .$this->input->post("street");
                        $msg .= "<li><strong>Suburb:</strong> "        .$this->input->post("suburb");
                        $msg .= "<li><strong>State:</strong> "         .$this->input->post("state");
                        $msg .= "<li><strong>Post Code:</strong> "     .$this->input->post("postcode");
                        $msg .= "<li><strong>Country:</strong> "       .$this->input->post("country");
                        $msg .= "<li><strong>Comments:</strong> "      .$this->input->post("comments");
                        $msg .= "<ul>";

                       
                        


                        $this->email->from($this->input->post("email"), "Contact Us form");
                        $this->email->reply_to($this->input->post("email"), "Contact Us form");
                        $this->email->to("frankensteinruo@gmail.com");
                        $this->email->subject("Contact us from " . $this->input->post("first_name")."(".$this->input->post("email").")");
                        $this->email->message($msg);
                        //$this->email->set_alt_message($this->load->view('email/'.$type.'-txt', $data, TRUE));
                        if (!$this->email->send())
                        { 
                              $this->email->print_debugger();
                              echo "Naaa";
                              exit;
                        }
                        else 
                        { 
                              print_r($recaptcha);
                              $this->load->view('ver2/blocks/header', $data);
                              $this->load->view('ver2/blocks/top-nav');
                              $this->load->view('ver2/thankyoucontacts');
                              $this->load->view('ver2/blocks/footer');  
                        }
                  }
                  else {
                        print_r($recaptcha);
                  } 
                 // $this->_show_message("Thanks for sending us an enquiry. We will get back in touch with you shortly.","contactUs");
 
            }
            else 
            {
                  $this->load->view('ver2/blocks/header');
                  $this->load->view('ver2/blocks/top-nav');
                  $this->load->view('ver2/contactus', $data);
                  $this->load->view('ver2/blocks/footer'); 
            } 
      }


      function about(){
            $this->load->view('ver2/blocks/header');
            $this->load->view('ver2/blocks/top-nav');
            $this->load->view('ver2/about');
            $this->load->view('ver2/blocks/footer');  
      }
      

      function hireFreeMomTens(){

            $query = $this->db->query("SELECT * FROM product INNER JOIN productprice ON product.ProductID = productprice.ProductID
             WHERE product.ProductID = 1 AND PriceGroupID = 1");
            if ($query->num_rows() > 0)
            {
                  $res = $query->result();
                  $data['product'] = $res[0];
                  $this->load->view('empty',$data);
                  $this->load->library('Layouts');
                  $this->layouts->set_title('Welcome!'); 
                  $this->layouts->view('hireFreeMomTens');
            }
      }

      function _show_message($message, $redirect)
      {
            $this->session->set_flashdata('message', $message);
            redirect("main/$redirect");
            redirect('/auth/');
      }


 
        
}
