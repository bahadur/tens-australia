<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class ApiControllerTestSuite {
  private $apiController;

  public function __construct($apiController) {
      if (ENVIRONMENT === "development") {
        $apiController->load->library("unit_test");
        $this->apiController = $apiController;
      }
  }

  /** Should fail if any of the following fields are empty or null: 
   * [firstname, lastname, addresses]
   */
  public function run() { 
    $this->testOrderSubmissionFailFields();
    $this->testOrderSubmissionSuccessFields();
    $this->testDateFormatConversion();
  }

  private function testOrderSubmissionFailFields() { 
    $failValues = $this->failValues(); 
    $i = 0; 
    foreach ($failValues as $failValue) {
      $_POST = $failValue;
      $res = $this->apiController->verifyOrder(); 
      echo $this->apiController->unit->run($res, false, 'testOrderSubmissionFailFields'.$i++);
    } 
  }

  private function testOrderSubmissionSuccessFields() {
    $successValues = $this->getSuccessValues(); 
    $i = 0;  
    $_POST = $successValues;
    $res = $this->apiController->verifyOrder(); 
    echo $this->apiController->unit->run($res, true, 'testOrderSubmissionSuccessFields'.$i++); 
  }

  private function testDateFormatConversion() { 
    $apiController = new ApiController();
    $reflector = new ReflectionClass('ApiController');
    $reflection_method = $reflector->getMethod('getMySQLDate');
    $reflection_method->setAccessible(true);
    $result = $reflection_method->invoke($apiController, '05-12-2015');
    echo $this->apiController->unit->run($result, '2015-12-05 00:00:00', 'testDateFormat'); 
  }

/**
* Helper functions:
*/
  private function getSuccessValues() {
    return array('firstname' => 'test','lastname' => 'test','billingaddress' => 'test','billingsuburb' => 'test','billingstate' => 'test','billingpostcode' => 'test',
        'shippingaddress' => 'test','shippingsuburb' => 'test','shippingstate' => 'test','shippingpostcode' =>'test','telephone' => 'test','mobile' => 'test',
        'email' => 'test','hospital' => 'test','insuranceprovider' => 'test','insurancenumber' => 'test','educator' => 'test','pharmacy' => 'test', 'requiredDate' =>'22-12-15' 
      );
  }

  private function failValues() { 
    $requiredFields = array('firstname', 'lastname', 'billingaddress', 'billingsuburb', 'billingstate', 'billingpostcode',
      'shippingaddress','shippingsuburb','shippingstate','shippingpostcode','requiredDate');
    $boilerPlate = array('firstname' => 'test','lastname' => 'test','billingaddress' => 'test','billingsuburb' => 'test','billingstate' => 'test','billingpostcode' => 'test',
        'shippingaddress' => 'test','shippingsuburb' => 'test','shippingstate' => 'test','shippingpostcode' =>'test','telephone' => 'test','mobile' => 'test',
        'email' => 'test','hospital' => 'test','insuranceprovider' => 'test','insurancenumber' => 'test','educator' => 'test','pharmacy' => 'test', 'requiredDate' => '22-12-2015' 
      );
    $possiblePostValues = array( 
    );
    foreach ($requiredFields as $requiredToOmit) {
      $newEmptyPost = array();
      foreach($boilerPlate as $key => $value) {
        if ($key === $requiredToOmit) {
          $newEmptyPost[$key] = '';
        } else {
          $newEmptyPost[$key] = $value;
        }
      }
      $possiblePostValues[] = $newEmptyPost;
      $newNullpost = array();
      foreach($boilerPlate as $key => $value) {
        if ($key === $requiredToOmit) {
          $newNullpost[$key] = null;
        } else {
          $newNullpost[$key] = $value;
        }
      }
      $possiblePostValues[] = $newNullpost;
    }
    return $possiblePostValues;
  }
}



class ApiController extends CI_Controller {
	    private $_MySQL_Date_Format = 'Y-m-d H:i:s';
      public function __construct(){
            parent::__construct(); 
      }

      /**
      * Tests
      */ 
      public function testsSuite() {
          if (ENVIRONMENT === "development") { 
            $testSuite = new ApiControllerTestSuite($this);
            $testSuite->run(); 
          }
          else {
            redirect('/');
          }
      } 

      /** Expectation : d-m-Y (like, 01-12-2015 which stands for 1st December 2015) */
      private function getMySQLDate($frontEndDate) { 
        return date($this->_MySQL_Date_Format, strtotime($frontEndDate));
      }

      public function verifyOrder() { 
        $required = array('fname', 'lname', 'billingAddress1', 'billingSuburb', 'billingState', 'billingPostcode',
          'shippingAddress','shippingSuburb','shippingState','shippingPostcode','requiredDate');   
        foreach ($required as $reqField) {   
          if (!empty($this->input->post($reqField)) && $this->input->post($reqField) && $this->input->post($reqField) != '') {
            if ($reqField === 'requiredDate') {
              $reqDate = $this->input->post('requiredDate');
              $dt = DateTime::createFromFormat("d/m/Y", $reqDate);
              if ($dt !== false && !array_sum($dt->getLastErrors())) {
                continue;
              } else {  
                return false;
              }
            }
          } else {  
            return false;
          }
        }
        return true;
      }

      public function submitOrder() {
        if ($this->verifyOrder()) {
        	$insertData['FirstName'] = $this->input->post('fname');
        	$insertData['LastName'] = $this->input->post('lname');

        	$insertData['BillingAddress'] = $this->input->post('billingAddress1').' '.$this->input->post('billingAddress2');
        	$insertData['BillingSuburb'] = $this->input->post('billingSuburb');
        	$insertData['BillingState'] = $this->input->post('billingState');
        	$insertData['BillingPostcode'] = $this->input->post('billingPostcode');
          $insertData['BillingCountry'] = 'Australia';

        	$insertData['ShippingAddress'] = $this->input->post('shippingAddress').' '.$this->input->post('shippingAddress2');
        	$insertData['ShippingSuburb'] = $this->input->post('shippingSuburb');
        	$insertData['ShippingState'] = $this->input->post('shippingState');
        	$insertData['ShippingPostcode'] = $this->input->post('shippingPostcode');
          $insertData['ShippingCountry'] = 'Australia';

        	$insertData['TelephoneNumber'] = $this->input->post('phone');
        	$insertData['MobileNumber'] = $this->input->post('mobile');
        	$insertData['Email'] = $this->input->post('email');
        	$insertData['Hospital'] = $this->input->post('hospital');
        	
        	$insertData['InsuranceProvider'] = $this->input->post('insuranceProvider');
        	$insertData['InsuranceNumber'] = $this->input->post('insuranceNumber');
          $insertData['Educator'] = $this->input->post('educator');
          $insertData['Pharmacy'] = $this->input->post('pharmacy');

          $insertData['DueDate'] = null;//date('Y-m-d H:i:s', strtotime($this->input->post('duedate'))); //
          $insertData['RequiredDate'] = $this->getMySQLDate($this->input->post('requiredDate'));
          $insertData['ApplicationDate'] = date($this->_MySQL_Date_Format, time()); 
          $insertData['ApplicationDate'] = date($this->_MySQL_Date_Format, time()); 
          $insertData['Confirmed'] = 0; 

          $products = array();
          $rawProducts = $this->input->post('orderProducts'); 
 
          if ($this->db->insert('unconfirmedorders',$insertData))
          {    
            $id = $this->db->insert_id();
            $orderItemsToInsert = array();
            foreach ($rawProducts as $rawProduct){  
              if (isset($rawProduct["productid"])) {
                $orderItemInsert['ProductID'] = $rawProduct["productid"];
                $orderItemInsert['UnitPrice'] = str_replace('$','', $rawProduct["unitprice"]); 
                $orderItemInsert['Quantity'] = $rawProduct["productquantity"];  
                $orderItemInsert['UOrderID'] = $id;   
                $orderItemsToInsert[] = $orderItemInsert;
              }
            } 
            if (!$this->db->insert_batch("unconfirmedorderitems", $orderItemsToInsert)) {
              echo json_encode(array("error" => "- Could not create an order")); 
            } else { 
              echo 'Success';
            }
          }
          else 
          {
          	echo json_encode(array("error" => "-- Could not create an order"));
          } 
        } else {
            echo json_encode(array("error" => "--- Could not create an order"));
        }
      }  
}