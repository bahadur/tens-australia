<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


function sum($a, $b) {
	return $a + $b;
}
class UnitTests extends CI_Controller {
	
      public function __construct(){
          parent::__construct();
          $this->load->library("unit_test");
      }
            
      public function testSomething() {
      		$this->unit->run(sum(4,3), 7, "Testing sum function");
      	  echo "Test";
      }
}