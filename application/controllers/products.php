<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products extends CI_Controller{
	
	public function __construct(){
		parent::__construct();
		$this->load->library('breadcrumbs');
	}


	function index(){
		
		redirect("Products/FreeMOMTENS");
	}


	function freeMomTens($param="index", $innerpage=""){ 
		// add breadcrumbs 
		$this->breadcrumbs->push('Home', '/');
		  // $this->breadcrumbs->push('Section', site_url('section') );
		$this->breadcrumbs->push('FreeMOM TENS', '/products/freeMomTens/'); 		
		$data['showBanner'] = true;
		if ($param != "index")
		{
			$title = "";
			if ($param == "faq")
			{
				$title = "F.A.Q.";
			}
			else if ($param == "insurance")
			{
				$title = "Insurance";
			}
			else if ($param == "productcare")
			{
				$title = "Product Care";
			}
			else if ($param == "relatedlinks")
			{
				$title = "Related Links";
			}
			else if ($param == "referencematerial")
			{
				$title = "Reference Material";
			}
			else if ($param == "specification")
			{
				$title = "Specification";
			}
			else if ($param == "hire-info") 
			{
				$data['showBanner'] = false;
				$title = "Hire";
				$data['pageTitle'] = 'Hire FreeMOM TENS Online Application';
			}
			else if ($param == "purchase-info")
			{
				$data['showBanner'] = false;
				$title = "Purchase";
				$data['pageTitle'] = 'Buy FreeMOM TENS Online Application';
			}
			$this->breadcrumbs->push($title, '/products/freeMomTens/$param'); 
		}
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		$this->load->view("ver2/blocks/header", $data);
		$this->load->view("ver2/blocks/top-nav");
    	$this->load->view("ver2/blocks/breadcrumbs.php");	
    	$this->load->view("ver2/product/freeMomTens/blocks/header.php"); 	
    	if ($innerpage != null)
    	{
    		$this->load->view("ver2/product/freeMomTens/$param/$innerpage.php");	
    	}
    	else 
    	{
    		$this->load->view("ver2/product/freeMomTens/$param.php");	
    	}
		$this->load->view("ver2/blocks/footer");
	}

	function freeLadyTens($param="index", $innerpage = null){
		// add breadcrumbs 
		$this->breadcrumbs->push('Home', '/');
		  // $this->breadcrumbs->push('Section', site_url('section') );
		$this->breadcrumbs->push('FreeLady TENS', '/products/freeladyTens/'); 	
		$data['showBanner'] = true;
		if ($param != "index")
		{
			$title = "";
			if ($param == "faq")
			{
				$title = "F.A.Q.";
			}
			else if ($param == "insurance")
			{
				$title = "Insurance";
			}
			else if ($param == "productcare")
			{
				$title = "Product Care";
			}
			else if ($param == "relatedlinks")
			{
				$title = "Related Links";
			}
			else if ($param == "referencematerial")
			{
				$title = "Reference Material";
			}
			else if ($param == "specification")
			{
				$title = "Specification";
			}
			else if ($param == "hire-info") 
			{
				$title = "Hire";
				$data['showBanner'] = false;
				$data['pageTitle'] = 'Hire FREELADY TENS Online Application';
			}
			else if ($param == "purchase-info")
			{
				$title = "Purchase";
				$data['showBanner'] = false;
				$data['pageTitle'] = 'Buy FREELADY TENS Online Application';
			}
			$this->breadcrumbs->push($title, '/products/freeladyTens/$param'); 
		}
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		$this->load->view("ver2/blocks/header", $data);
		$this->load->view("ver2/blocks/top-nav");
    	$this->load->view("ver2/blocks/breadcrumbs.php");	
    	$this->load->view("ver2/product/freeladyTens/blocks/header.php"); 	
    	if ($innerpage != null)
    	{
    		$this->load->view("ver2/product/freeladyTens/$param/$innerpage.php");	
    	}
    	else 
    	{
    		$this->load->view("ver2/product/freeladyTens/$param.php");	
    	}
		$this->load->view("ver2/blocks/footer");

	}

	function compactTens($param="index", $innerpage = null){ // add breadcrumbs 
		$this->breadcrumbs->push('Home', '/');
		  // $this->breadcrumbs->push('Section', site_url('section') );
		$this->breadcrumbs->push('Compact TENS', '/products/compactTens/'); 
		$data['showBanner'] = true;
		if ($param != "index")
		{
			$title = "";
			if ($param == "faq")
			{
				$title = "F.A.Q.";
			}
			else if ($param == "insurance")
			{
				$title = "Insurance";
			}
			else if ($param == "productcare")
			{
				$title = "Product Care";
			}
			else if ($param == "relatedlinks")
			{
				$title = "Related Links";
			}
			else if ($param == "referencematerial")
			{
				$title = "Reference Material";
			}
			else if ($param == "specification")
			{
				$title = "Specification";
			}
			else if ($param == "hire-info") 
			{
				$title = "Hire";
				$data['showBanner'] = false;
				$data['pageTitle'] = 'Hire COMPACT TENS Online Application';
			}
			else if ($param == "purchase-info")
			{
				$title = "Purchase";
				$data['showBanner'] = false;
				$data['pageTitle'] = 'Buy COMPACT TENS Online Application';
			}
			$this->breadcrumbs->push($title, '/products/compactTens/$param'); 
		}
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		$this->load->view("ver2/blocks/header", $data);
		$this->load->view("ver2/blocks/top-nav");
    	$this->load->view("ver2/blocks/breadcrumbs.php");	
    	$this->load->view("ver2/product/compactTens/blocks/header.php"); 	
    	if ($innerpage != null)
    	{
    		$this->load->view("ver2/product/compactTens/$param/$innerpage.php");	
    	}
    	else 
    	{
    		$this->load->view("ver2/product/compactTens/$param.php");	
    	}
		$this->load->view("ver2/blocks/footer");

	}


	public function getAccessories()
	{
		$queryStr = "SELECT product.ProductCode, product.ProductName, product.Notes, product.ProductCategoryID, product.ImageUri
				FROM product WHERE ProductCategoryID = 3 OR ProductCategoryID = 2 OR ProductCategoryID = 2 AND 
				product.OnWebsite = 1 AND product.Active = 1";
		$q = $this->db->query($queryStr);
		if ($q->num_rows() > 0)
		{
			$r = $q->result();
			print_r(json_encode($r));
		}
		else 
		{
			echo "{}";
		}
	}

	function accessories($param="index", $innerpage = null){
		$this->breadcrumbs->push('Home', '/');
		  // $this->breadcrumbs->push('Section', site_url('section') );
		$this->breadcrumbs->push('Accessories', '/products/accessories/');  
		if ($param == "productCare")
		{
			$this->breadcrumbs->push('Product Care', '/products/accessories/productCare'); 
		}
		else if ($param == "purchaseInfo")
		{
			$this->breadcrumbs->push('Purchase', '/products/accessories/purchaseInfo'); 
			if ($innerpage != null)
			{
				$this->breadcrumbs->push('Online', '/products/accessories/purchaseInfo/onlineform'); 
				$data['pageTitle'] = "Buy Accessories";
			}
		}
		else if ($param == "index") 
		{
			$qCount=  $this->db->query("SELECT
					SUM(IF(product.ProductCategoryID = 4 AND product.OnWebsite = 1 AND product.Active = 1, 1, 0)) AS OptionsCount,
				    SUM(IF(product.ProductCategoryID = 2 AND product.OnWebsite = 1 AND product.Active = 1, 1, 0)) AS ElectrodesCount,
				    SUM(IF(product.ProductCategoryID = 3 AND product.OnWebsite = 1 AND product.Active = 1, 1, 0)) AS AccessoriesCount FROM product");
			if ($qCount->num_rows() > 0)
			{
				$rC = $qCount->result();
				$data['accessoriesCount'] = $rC[0];
			}
		}
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		$this->load->view("ver2/blocks/header", $data);
		$this->load->view("ver2/blocks/top-nav");
    	$this->load->view("ver2/blocks/breadcrumbs.php");	 
    	if ($innerpage != null)
    	{
    		$this->load->view("ver2/product/accessories/$param/$innerpage.php");	
    	}
    	else 
    	{
    		$this->load->view("ver2/product/accessories/$param.php");	
    	}
		$this->load->view("ver2/blocks/footer");

	}

	
}

 ?>