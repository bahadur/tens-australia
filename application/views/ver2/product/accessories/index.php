
    <section id="main-content-area">
      <div class="container">

        <div class="row">
          
          <div class="col-md-9">

            <!--<div class="top-pagination">
              <nav>
                <ul class="pagination pagination-lg">
                      <li><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
                      <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">4</a></li>
                      <li><a href="#">5</a></li>
                      <li><a href="#" aria-label="Previous"><span aria-hidden="true">&raquo;</span></a></li>
                </ul>
              </nav>
            </div>-->
            <ul id="accessories-list" class="accessories-list"></ul>
 

          </div>

          <div class="col-md-3">
            <div class="list-group side-group">

                <a href="#" class="list-group-item disabled active">
                    CATEGORIES
                </a> 
                 <a href="#" class="list-group-item">
                    <span class="badge">(<?php echo $accessoriesCount->OptionsCount;?>)</span>
                    Options
                </a>
 
                <a href="#" class="list-group-item">
                    <span class="badge">(<?php echo $accessoriesCount->ElectrodesCount;?>)</span>
                    Electrodes
                </a>   

                <a href="#" class="list-group-item">
                    <span class="badge">(<?php echo $accessoriesCount->AccessoriesCount;?>)</span>
                    Accessories
                </a>      
            </div>            


            <div class="list-group side-group">

                <a href="#" class="list-group-item disabled active">
                    INFORMATION
                </a>

                 <a href="<?= base_url();?>products/accessories/productCare" class="list-group-item">
                    Product Care
                </a>
 
                <a href="<?= base_url();?>products/accessories/purchaseInfo" class="list-group-item">
                    Purchase
                </a>   

            </div>


          </div>
          
        </div>

      </div>
    </section>
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/styles.pagination.css"></link>
<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.quick.pagination.min.js"></script>
    <script type="text/javascript">

    // 1. Get accessories from the API
    $(document).ready(function(e) {
    $.ajax({
        url : "<?=base_url();?>products/getAccessories", 
         success: function(data, status) { 
            var accessories = JSON.parse(data);  
            for (var i = 0; i < accessories.length; i++)
            {
              var accessory = accessories[i];  
              $("#accessories-list").append('<li><div class="row">'+
                  '<div class="single-accessories-product">' +
                    '<div class="col-md-3">' +
                      '<img src="' + accessory['ImageUri'] + '" alt="" class="img-responsive accessories-product-image">' +
                    '</div>' +
                    '<div class="col-md-9">' + 
                      '<h2 class="accessories-product-title">' + accessory['ProductName'] + '</h2>' + 
                        '<div class="row">' + 
                          '<div class="col-md-8 accessories-cost">' +
                            '<p>Box of 50 : AUD $15.00</p>' +
                          '</div>' + 
                          '<div class="col-md-4 accessories-code">' +
                            '<p>(Product Code ' + accessory['ProductCode'] + ')</p>' +
                          '</div></div>' + 
                        '<p>' + accessory['Notes'] + '</p>' + 
                        '<a href="<?=base_url();?>products/accessories/purchaseInfo" class="btn btn-success">Purchase</a>' +
                    '</div></div></div>'+
                '<div class="accessories-line-bottom"></div></li>');
            };
            $("#accessories-list").quickPagination({pagerLocation:"both",pageSize:"5"})
          } 
      });
    })
    // 2. On callback - create UL and LI structure + save the json results into an array

    // 3. On filtering ... You will be able to 
    </script>