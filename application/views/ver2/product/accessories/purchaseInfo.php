
	<br/>
	<div class="main-content container">
		
	<!-- Main content -->
		<div class="col-md-7">
			<a name="toc-1"></a>
			<h2>How do I purchase the Products?</h2>
			<br/>
			<ul class="bullet-points">
				<li>Print customer order form and product price list</li>
				<li>Complete the customer order form</li>
				<li>Fax, mail, scan or email the order form to our office.</li>
				<li>Fax:	1300 913 149</li>
				<li>Mailing address: 7 / 2 Central Avenue, MOORABBIN VICTORIA 3189 Australia</li>
				<li>Email:	info@tensaustralia.com</li>
				<li>Alternatively call our office during business hours and complete the application over the telephone. <br/>Tel: 1300 913 129</li>
			</ul>
			<a name="toc-2"></a>
			<h2>How do I pay for my purchase?</h2>
			<ul class="bullet-points">
				<li>Credit card (Mastercard or Visa )</li>
				<li>Cheque</li>
				<li>Money Order</li>
			</ul> 
			<a name="toc-3"></a>
			<h2>Can I purchase additional electrodes in the future?</h2>
			<br/>
			<p>Yes, repeat the above steps to purchase additional accessories.</p> 
		</div>
		<!-- Main content end -->
		<div class="col-md-4 col-md-offset-1">
			<div class="row col-md-12 green-toc">
				<h3 style="width:100%; margin:0 auto; text-align:center;margin-bottom:15px">Ready to Buy Accessories?</h3>
				<div class="col-md-12"> 
				<a class="btn btn-default btn-tens-action" href="<?= base_url() ?>products/accessories/purchaseInfo/onlineform"  style="width:100%; font-size:18px; text-align:left;" >
					<img src="http://www.graphicsfuel.com/wp-content/uploads/2012/01/shopping-cart-icon-515.png" style="float:left;width:40px; margin-top:5px; height:40px"></img>
				<span style="margin-left:20px; margin-right:15px; text-align:left">Accessories BUY <br/></span>
				<span  style="margin-left:20px; margin-right:15px; text-align:left">ONLINE</a></span>
				
				<a class="btn btn-default btn-tens-action"  style="width:100%; font-size:18px; text-align:left;" >
					<img src="http://tensaustralia.com/images/pdfico2.png" style="float:left;width:40px; height:40px"></img>
				<span style="margin-left:20px; margin-right:15px; text-align:left">Accessories BUY <br/></span>
				<span  style="margin-left:20px; margin-right:15px; text-align:left">APPLICATION FORM</a></span>
				</div>
			</div> 
			<div class="row col-md-12 blue-toc" style="margin-top:55px">
				<ul class="toc-content"> 
					<li><a href="#toc-1" > How do I purchase the Products?</a></li>
					<li><a href="#toc-2" > How do I pay for my purchase?</a></li>
					<li><a href="#toc-3" > Can I purchase additional electrodes in the future?</a></li> 
				</ul>
				
			</div>
		</div>
	</div>
	<div class="clear-footer"></div>	