<p style="text-align:center; font-weight:600;">Tel 1300 913 129 Fax 1300 913 149 </p>
<p style="text-align:center; font-weight:600;">info@tensaustralia.com  www.tensaustralia.com </p>
<p style="text-align:center; font-weight:200; font-size:14px">TENS”R”US PTY LTD ABN 64 130 096 949</p>
<p style="text-align:center; font-weight:600; font-size:18px">
	COMPACT TENS PAIN MANAGEMENT KIT <br/>HIRE TERMS AND CONDITIONS  </p>  
<p>When hiring a COMPACT TENS Pain Management kit (hereby referred to as “the hire kit”), 
	it is the responsibility of the hirer or his/her representative to familiarise themselves with the conditions of the hire.</p> 
<ol class="numbering-list">
<li>The hire kit is to be used in accordance with the instructions provided or as per instructions supplied by a treating clinician. </li>
<li>The hire TENS stimulator is to be used by the client named on the hire application form. </li>
<li>A hire application form is to be completed by the hirer or a representative prior to the supply of a kit. </li>
<li>The hire of a kit must be paid prior to, or when collecting the kit which includes all the accessories outlined on the hire form.</li>
<li>The kit is supplied direct or by Registered Post to the user at an agreed nominated date.  </li>
<li>A refundable deposit is charged as part of the hire which is refunded upon the return of the kit within the guidelines outlined below. </li>
<li>Title to hired equipment supplied to the hirer remains with us the supplier. </li>
<li>Title to equipment and or accessories sold with the hired equipment remain with us the supplier until all monies owing in respect 
	of the equipment and or accessories, and all other money owing to us on any other account are paid by you. </li>
<li>The Supplier must be notified if the kit cannot be returned on time. Please call 1300 913 129 during business hours.</li>
<li>The purchase price of electrodes will be refunded if returned to the supplier unopened and undamaged with the kit. </li>
<li>Return of the kit is the responsibility of the hirer. If the prepaid return satchel has not been purchased and used, the hirer 
	must purchase insured and traceable shipping and advise the supplier with the tracking details. </li>
<li>The hire commences from the date the hire equipment is shipped or supplied direct to the hirer. </li>
<li>The return date is 30 days after the hire commence. </li> <!-- 13 -->
<li>Please ensure the hire kit is returned to the supplier no later than 14 days after your return date.  </li>
<li>If the hire kit is returned to the supplier later than 14 days after your return date the deposit paid by the hirer will not be refunde.</li>
<li>In the event the hired kit is not returned after 30 days from the return date, it is assumed the hirer wishes to purchase the kit. 
	The replacement price is AUD $175. 
	The customer will be charged the replacement price of AUD$175 less any hire fees and deposit paid by the hirer </li>
<li> In the event of damage to the kit while in possession of the hirer, 
	it is the hirer’s responsibility to notify the supplier immediately.</li>
<li>In the event the hired kit is returned to the supplier damaged beyond repair the hirer is liable for the replacement price of AUD$175. 
	The customer will be charged the replacement price of AUD$175 less any hire fees and deposit paid by the hirer. </li>
<li>The supplier takes no responsibility for kits that are lost in transit. </li>
<li>Advice from a Health Professional should be sought prior to using any TENS devices if the hirer is fitted with a Pacemaker, has a heart disease or suffers from epilepsy. </li>


<li>The hirer should verify the contents of the kit and additional items supplied when the unit is collected or received. It is important that the instructions (supplied with the kit) are read prior to use.  </li>
<li>It is recommended that a Health Professional explain how to use the TENS device prior to its use. </li>
<li>The hirer acknowledges that the improper use of the kit may cause damage or physical harm, and that the instruction manual supplied with the kit contains cautions and warnings as well as instructions on the proper use of the device. Hence it is the responsibility of the hirer to read these cautions, warnings and instructions and to abide by them. </li> 
<li>The hirer releases the supplier, their officers, agents and employees from any and all liability for any damage, injury, cost, liability or expense arising from any defect within the device, over and above the hire or purchase cost of the kit. </li>
<li>The hirer is able to purchase the kit following the start of the hire period, by contacting our office on 1300 913 129. The deposit and hire charge will be deducted from the purchase price. </li>
</ol>