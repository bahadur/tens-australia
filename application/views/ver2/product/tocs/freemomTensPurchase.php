<p style="text-align:center; font-weight:600;">Tel 1300 913 129 Fax 1300 913 149 </p>
<p style="text-align:center; font-weight:600;">info@tensaustralia.com  www.tensaustralia.com </p>
<p style="text-align:center; font-weight:200; font-size:14px">TENS”R”US PTY LTD ABN 64 130 096 949</p>
<p style="text-align:center; font-weight:600; font-size:18px">PURCHASE  <br/>HIRE TERMS AND CONDITIONS </p>  
<p>When purchasing a TENS Pain Management kit (hereby referred to as “the kit”) and or TENS Accessories (hereby referred to as “accessories”), it is the responsibility of the purchaser or his/her representative to familiarise themselves with the conditions of purchase.  </p> 
<ol class="numbering-list">
<li>The purchased kit is to be used in accordance with the instructions provided or as per instructions supplied by a treating clinician.</li>
<li>It is recommended that a Health Professional explain how to use the kit and or accessories prior to its use.</li>
<li>The purchased kit and or accessories is to be used by the purchaser. </li>

<li>A purchase or order form is to be completed by the person or his/her representative prior to the supply.</li>
<li>The amount due must be paid in full prior to shipping or when collecting the kit and or accessories. </li>
<li>Title to equipment and or accessories sold to you or your representative remain with us until all monies owing in respect of the equipment and or accessories, and all other money owing to us on any other account are paid by you. </li>

<li>The kit is supplied direct or by Registered Post to you or your representative a mutually convenient nominated date.</li>
<li>The supplier takes no responsibility for kits that are lost in transit. </li>
<li>Advice from a Health Professional should be sought prior to using the kit and or accessories, if the person is fitted with a pacemaker, has a heart disease or suffers from epilepsy. </li>

<li>The purchaser should verify the contents of the kit and additional items supplied when the unit is collected or receive. </li>
<li>It is important that the instructions supplied with TENS devices are read prior to use. </li>
<li>The purchaser acknowledges that the improper use of the kit may cause damage or physical harm, and that the instruction manual supplied with the kit contains cautions and warnings as well as instructions on the proper use of the device. Hence it is the responsibility of the purchaser to read these cautions, warnings and instructions and to abide by them. </li>
<li>The purchaser releases the supplier, their officers, agents and employees from any and all liability for any damage, injury, cost, liability or expense arising from any defect within the device, over and above the hire or purchase cost of the kit. </li>

</ol>