
	<br/>
	<div class="main-content container">
	
	<!-- Main content -->
		<div class="col-md-7">
<a name="toc-1"></a>
			<h3>What is a FREELADY TENS?</h3>
<p>FREELADY is a Transcutaneous Electrical Nerve Stimulation (TENS) device which has been specifically designed to assist in the relief of menstrual and endometriosis pain.</p>

<a name="toc-2"></a>
<h3>How does it Work?</h3>
<p>FREELADY sends small electronic signals into the body via electrodes which are placed onto the skin. The stimulus aids the relief of pain in 3 ways:</p>
<ol class="numbered-list">
	<li>By blocking the pain messages from reaching the brain via the nerves.</li>
<li>By stimulating the release of the body’s own naturally occurring pain-relieving endorphins.</li>
<li>By providing a distraction whilst the pain is occurring.</li></ol>
<br/>
<table>
	<tr>

		<td style="width:10%;text-align:center"> </td>
		<td style="width:40%;text-align:center"> 
			<img src="<?=base_url()?>assets/images/Freelady_with_Electrodes_shown.jpg"/><br/>
			<p>The FREELADY TENS stimulator is shown
			with its adjustable controls on the 
			front of the stimulator.)</p> 
		</td>
		<td style="width:40%;text-align:center"> 
				<img src="<?=base_url()?>assets/images/Freelady_Croped_Top_Mk_2_View.jpg"/><br/>
				<p>FREELADY TENS</p> 
		</td>
		<td style="width:10%;text-align:center"> </td>
	</tr>
	</table> 
<a name="toc-3"></a>
<h3>Types of Menstrual Pain</h3>
<p>During menstruation, the intensity, character and location of pain varies – thus affecting the extent to which menstrual pain influences each woman’s physical and emotional well-being. Moreover, menstrual pain is not the same each and every month, and of course, menstrual pain also varies from woman to woman.</p>
<p>Despite these variables there are at least three distinct categories of menstrual pain that are usually differentiated and identified among most women.</p>
<ul class="bullet-list">
	<li>The first, referred pain – is a clear, well-defined pain, whose location is relatively easy to pinpoint. This type of pain (regardless of its location, intensity and sharpness), is characterised by the fact that practically every woman can isolate and identify “the field of pain” on her body – that is, define and point to its location and boundaries on her body’s surface.</li>
	<li>The second, radiating pain – branches out and affects parts of the body far removed from the source of pain. The areas typically affected include the hips or thighs, the lower back and/or waist, sometimes reaching as far as the shoulders and neck.</li>
	<li>The third is a dull, distant, hard to define pain that is extremely difficult to pinpoint accurately, and is accompanied by a feeling of heaviness, pressure and overall discomfort. Most woman can identify “the field of pain” belonging to the first category of “referred pain”. The pain is strong and sharp and is felt on the surface (the skin). This is the target area that the FREELADY aims at treating.</li>
</ul>
<a name="toc-4"></a>
<h3>Using your FREELADY</h3>
<p>You’ll find your FREELADY extremely easy to use (as outlined below).</p>
<ol class="numbered-list">
	<li>Attach the electrodes to your skin (below the ‘belly-button’, in the supra pubic area).</li>
	<li>Connect the electrodes to the stimulator unit using the supplied cable.</li>
	<li>Turn ON the stimulator and adjust its amplitude (strength).</li>
</ol>
<h3>FREELADY offers the following superior features:</h3>
<ul class="bullet-list">
	<li>FREELADY is a pocket sized device. FREELADY is very simple to use and may be placed on a belt or attached to your briefs. When not in use, it can be carried in your pocket or in a handbag. The FREELADY is comfortable and unobtrusive under your clothes.</li>
	<li>“Set and Forget” stimulation settings. No need to continuously control the intensity.</li>
</ul>
<h3>Benefits of the FREELADY:</h3>
<ul class="bullet-list"> 
<li>FREELADY is simple to use, non-invasive and drug free,</li>
<li>FREELADY initiates the action of the body’s own natural pain relieving mechanisms.</li>
<li>FREELADY is used to provide a background level of ‘distraction’ to which any other form of analgesia may be added if required.</li>
<li>FREELADY may be used at home or work but must be removed prior to a shower, bath etc.</li>
<li>FREELADY allows you to be fully mobile and manage your pain.</li>
<li>FREELADY can be easily be discontinued at any time.</li>
</ul>
		</div>
		<!-- Main content end -->
		<div class="col-md-4 blue-toc col-md-offset-1">
			<ul class="toc-content">
				<li><a href="#toc-1" >  What is FREELADY TENS?</a></li>
				<li><a href="#toc-2" >  How does it Work?</a></li>
				<li><a href="#toc-3" >  Using your FREELADY</a></li>
				<li><a href="#toc-4" >  FREELADY offers the following superior features</a></li> 
				<li><a href="#toc-5" >  Benefits of the FREELADY</a></li> 
			</ul>
		</div>
	</div> 
	
	<div class="clear-footer"></div>