
	<br/>
	<div class="main-content container">
		
	<!-- Main content -->
		<div class="col-md-7">

			<h3>How do I purchase the Products?</h3>
<p>You have the following options</p>
<ol class="numbered-list">
	<li>Purchase ONLINE</li>
	<li>Print customer order form and product price list</li>
	<li>Complete the customer order form</li>
	<li>
		<p>Fax, mail, scan or email the order form to our office.</p>
		<p>Fax:	1300 913 149
		</p>
		<p>Mailing address: 7 / 2 Central Avenue MOORABBIN VICTORIA 3189 Australia
		</p>
		<p>Email:	info@tensaustralia.com
		</p>
	</li>
	<li><p>Alternatively call our office during business hours and complete the application over the telephone.</p>
		<p>Tel: 1300 913 129</p>
	</li>
</ol>

 
<h3>How do I pay for my purchase?</h3>
<ul class="bullet-points">
<li>Credit card (Mastercard or Visa )</li>
<li>Cheque</li>
<li>Money Order</li>
</ul>
<h3>Can I purchase additional electrodes in the future?</h3>
<p>Yes, repeat the above steps to purchase additional accessories.</p>

<h3>How long will my order take to arrive once I pay for my order?</h3>

<table class="table table-striped">
	<tr>
		<th>Next business day</th>
		<th>Two business days</th>
	</tr>
	<tr>
		<td>Adelaide metro</td>
		<td>Brisbane metro</td>
	</tr>
	<tr>
		<td>Canberra metro</td>
		<td>Darwin metro</td>
	</tr>
	<tr>
		<td>Melbourne metro</td>
		<td>Hobart metro</td>
	</tr>
	<tr>
		<td>Sydney metro</td>
		<td>Perth metro</td>
	</tr>
</table> 

<p>International shipping available by Air express</p>

		</div>
		<!-- Main content end -->
		<div class="col-md-4 col-md-offset-1">
			<div class="row col-md-12 green-toc">
			<h3 style="width:100%; margin:0 auto; text-align:center;margin-bottom:15px">Ready to Hire FreeLady?</h3>
				<div class="col-md-12"> 
					<a class="btn btn-default btn-tens-action" href="<?= base_url() ?>products/freeladyTens/purchase-info/onlineform"  style="width:100%; font-size:18px; text-align:left;" >
						<img src="http://www.graphicsfuel.com/wp-content/uploads/2012/01/shopping-cart-icon-515.png" style="float:left;width:40px; margin-top:5px; height:40px"></img>
					<span style="margin-left:20px; margin-right:15px; text-align:left">FreeLady BUY <br/></span>
					<span  style="margin-left:20px; margin-right:15px; text-align:left">ONLINE</a></span>
					
					<a class="btn btn-default btn-tens-action" href="<?= base_url()?>assets/applications/TENS_R_US_Order_Form.pdf" target="_blank" style="width:100%; font-size:18px; text-align:left;" >
						<img src="http://tensaustralia.com/images/pdfico2.png" style="float:left;width:40px; height:40px"></img>
					<span style="margin-left:20px; margin-right:15px; text-align:left">FreeLady BUY <br/></span>
					<span  style="margin-left:20px; margin-right:15px; text-align:left">APPLICATION FORM</a></span>
					</div>
				</div> 
		</div>
	</div>
	<div class="clear-footer"></div>	