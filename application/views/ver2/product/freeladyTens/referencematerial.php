
<br/>

<div class="main-content container">
<br/>
<h2>FreeMOM Tens Reference Material</h2>
<br/>
<table class="table table-striped">
<tr><th>Content</th><th>PDF</th></tr>
<tr><td>Clinical Evaluation of a New Model of a TENS Device for the Management of Primary Dysmenorrhea</td>
<td><a href="<?= base_url()?>assets/pdfs/referencematerials/freeladytens/Clinical_Evaluation_of_a_New_Model_of_a_TENS_Device_for_the_Management_of_Primary_Dysmenorrhea.pdf" target="_blank"><img src="<?= base_url()?>assets/images/pdfico2.png"></img></a></td></tr>
<tr>
<td>TENS as a relief for Dysmenorrhea</td>
<td><a href="<?= base_url()?>assets/pdfs/referencematerials/freeladytens/TENS_as_a_relief_for_Dysmenorrhea.pdf" target="_blank"><img src="<?= base_url()?>assets/images/pdfico2.png"></img></a></img>
</td></tr> 
</table>

<p>You are welcome to call or email our office if you have any questions or require additional information.</p>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
</div>