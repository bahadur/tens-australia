
<br/>
<br/>
<br/>

<div class="main-content container"> 
<table class="table table-striped">
	<tr><td>Size:</td>  	<td>(L) 92mm (W) 74 mm (D) 15 mm</td></tr>
<tr><td>Weight:  </td>	<td>60 gm. (excl. Battery)</td></tr>
<tr><td>Waveform:</td>  	<td>Symmetrical rectangular biphasic</td></tr>
<tr><td>Channels:</td>  	<td>One (1)</td></tr>
<tr><td>Current Output:</td>  	<td>0-50mA Peak into 1K ohm load</td></tr>
<tr><td>Pulse Width: </td> 	<td>100 MicroSec. Preset</td></tr>
<tr><td>Pulse Rate: </td> 	<td>100 Hz.</td></tr>
<tr><td>Modulation:</td>  	<td>3 to 10 Hz. (FM)</td></tr>
<tr><td>Green LED: </td> 	<td>Stays ON when switched ON</td></tr>  
<tr><td>Battery type:</td>  	<td>3 x AAA 1.5V Alkaline</td></tr>
<tr><td>Pulse rate:</td>  	<td>(Approx.) 80 Hours</td></tr>
</table>
<br/>
<br/>
<br/>
<br/>
</div>