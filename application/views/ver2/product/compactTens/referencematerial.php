
<br/>

<div class="main-content container">
<br/>
<h2>Compact TENS Reference Material</h2>
<br/>
<table class="table table-striped">
<tr><th>Content</th><th>PDF</th></tr>
<tr><td>Testing the efficacy of the modulation technique.</td>
<td><a href="<?= base_url()?>assets/pdfs/referencematerials/compacttens/Testing_the_Efficacy_of_the_Modulation_Technique.pdf" target="_blank"><img src="<?= base_url()?>assets/images/pdfico2.png"></img></a></td></tr>
<tr>
<td>The use of TENS in General Practice - Medical Journal of Australia	</td>
<td><a href="<?= base_url()?>assets/pdfs/referencematerials/compacttens/The_use_of_TENS_in_General_Practice_Medical_Journal_of_Australia.pdf" target="_blank"><img src="<?= base_url()?>assets/images/pdfico2.png"></img></a></img>
</td></tr> 
</table>

<p>You are welcome to call or email our office if you have any questions or require additional information.</p>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
</div>