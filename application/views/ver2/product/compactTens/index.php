
	<br/>
	<div class="main-content container">
	
	<!-- Main content -->
		<div class="col-md-7">
			<a name="toc-1"></a>
			<h3>What is the COMPACT TENS?</h3>
			<image src="<?= base_url() ?>assets/images/ICON_Compactens.jpg" style="float:right"></image>
<br/>
			<p>COMPACT TENS is a dual channel, non-invasive Transcutaneous Electrical Nerve Stimulation (T.E.N.S) device which has been specifically designed for the purpose of assisting in the treatment and management of chronic and acute pain.</p>

<br/>
<br/>
			<a name="toc-2"></a>
			<h3>How does it Work?</h3>
<br/>
			<p>The COMPACT TENS emits a small, electrical pulse into the body via electrodes which are placed onto the skin. The stimulus aids the relief of pain in 3 ways: </p>
			<ol>
				<li>By “blocking” the pain messages from reaching the brain via the nerves.</li>
				<li>By stimulating the release of the body’s naturally occurring pain-relieving 	endorphins.</li>
				<li>By providing a distraction while the pain is occurring.</li>
			</ol>

<br/>
<br/>
			<a name="toc-3"></a>
			<h3>Using the COMPACT TENS</h3>
<br/>

			<p>Detailed advice on the application of the COMPACT TENS is available from Health Professionals and Pain Clinics. 
				The COMPACT TENS includes information to assist health professionals and educational information for patients. 
				Regular review of client progress is recommended following the initial assessment and treatment session with 
				the health professional.</p>
			<p>Correct electrode placement is essential for effective treatment. Electrode placement protocol instructions are 
				included with each COMPACT TENS kit supplied.</p>

			
<br/>
<br/>
			<a name="toc-4"></a>
			<h3>Features and Benefits</h3>
<br/>
			<ul class="bullet-list">
				<li>Non-invasive and drug free.</li>
				<li>Can be used with other pain management treatments and modalities.</li>
				<li>Can be used continuously for 16 hours in a 24 hour period.</li>
				<li>Compact, portable and simple to use.</li>
				<li>Enhances the patients feeling of ‘well-being’ by encouraging the release of endorphins.</li>
				<li>May give a patient a sense of control over the pain.</li>
				<li>May give the patient increased autonomy as it can be applied anytime with minimal or no disruption to their normal daily routine.</li>
				<li>No interference with prescribed medications.</li>
				<li>Choice of 3 available waveforms: Continuous, Pulse Burst and Modulated.</li>
				<li>Clinically evaluated with published reports available in Medical and other journals.</li>
				<li>Cost effective option for patient’s pain management.</li>
				<li>Meets AAMI guidelines (Association for the Advancement of Medical Instrumentation).</li>
			</ul>

			
<br/>
<br/>
			<a name="toc-5"></a>  
			<h3>COMPACT TENS Kit contains</h3>
<br/>
			<ul class="bullet-list">
				<li>COMPACT TENS Stimulator unit</li>
				<li>2 x Cables</li>
				<li>1 x 9V Alkaline battery.</li>
				<li>3 x Clean Cote Skin wipes.</li>
				<li>1 x Set of Electrodes</li>
				<li>Written instructions / User Manual</li>
				<li>Protective carry case</li> 
			</ul>

		</div>
		<!-- Main content end -->
		<div class="col-md-4 col-md-offset-1 blue-toc">
			<ul class="toc-content">
				<li><a href="#toc-1" >What is the COMPACT TENS?</a></li>
				<li><a href="#toc-2" >How does it Work?</a></li>
				<li><a href="#toc-3" >Using the COMPACT TENS</a></li>
				<li><a href="#toc-4" >Features and Benefits</a></li> 
				<li><a href="#toc-5" >COMPACT TENS Kit contains</a></li> 
			</ul>
		</div>
	</div>
	
	<div class="clear-footer"></div>