
<br/>

<div class="main-content container">
<h2>Insurance</h2>
<br/>

<p style="text-decoration:underline">Please contact your insurance provider to determine if you are eligible to make a claim. </p>
<p>To assist you with your enquiries please find below a list of insurers with a link to their individual websites. Some insurers require us to supply a Provider number when you make a claim. Where we have been supplied a Provider number it appears below and on your copy of our tax invoice.</p>

<br/>
<h2>Private Health Insurance Providers</h2>
<br/>
<table class="table table-striped">
	<tr>
		<th>Provider Name</th>
		<th>Provider No</th>
	</tr>
	<tr>
		<td>ACA Health Benefits Fund</td>
		<td></td>
	</tr>
	<tr>
		<td>ahm Health Insurance</td>
		<td></td>
	</tr>
	<tr>
		<td>Australian Unity Health Limited</td>
		<td></td>
	</tr>
	<tr>
		<td>Bupa Australia Pty Ltd</td>
		<td>EP08949</td>
	</tr>
	<tr>
		<td>CBHS Health Fund Limited</td>
		<td></td>
	</tr>
	<tr>
		<td>CDH Benefits Fund</td>
		<td></td>
	</tr>
	<tr>
		<td>Central West Health Cover</td>
		<td></td>
	</tr>
	<tr>
		<td>CUA Health Limited</td>
		<td></td>
	</tr>
	<tr>
		<td>Defence Health Limited</td>
		<td></td>
	</tr>
	<tr>
		<td>Doctors Health Fund</td>
		<td></td>
	</tr>
	<tr>
		<td>GMF Health</td>
		<td></td>
	</tr>
	<tr>
		<td>GMHBA Limited</td>
		<td></td>
	</tr>
	<tr>
		<td>Grand United Corporate Health</td>
		<td></td>
	</tr>
	<tr>
		<td>HBF Health Limited</td>
		<td></td>
	</tr>
	<tr>
		<td>HCF</td>
		<td></td>
	</tr>
	<tr>
		<td>Health Care Insurance Limited</td>
		<td></td>
	</tr>
	<tr>
		<td>Health Insurance Fund of Australia Limited</td>
		<td></td>
	</tr>
	<tr>
		<td>Health Partners</td>
		<td></td>
	</tr>
	<tr>
		<td>health.com.au</td>
		<td></td>
	</tr>
	<tr>
		<td>Latrobe Health Services</td>
		<td></td>
	</tr>
	<tr>
		<td>Medibank Private Limited</td>
		<td></td>
	</tr>
	<tr>
		<td>Mildura Health Fund</td>
		<td></td>
	</tr>
	<tr>
		<td>National Health Benefits Australia Pty Ltd (onemedifund)</td>
		<td></td>
	</tr>
	<tr>
		<td>Navy Health Ltd</td>
		<td></td>
	</tr>
	<tr>
		<td>NIB Health Funds Ltd.</td>
		<td>10112588</td>
	</tr>
	<tr>
		<td>Peoplecare Health Insurance</td>
		<td></td>
	</tr>
	<tr>
		<td>Phoenix Health Fund Limited</td>
		<td></td>
	</tr>
	<tr>
		<td>Police Health</td>
		<td></td>
	</tr>
	<tr>
		<td>Queensland Country Health Fund Ltd</td>
		<td></td>
	</tr>
	<tr>
		<td>Railway and Transport Health Fund Limited</td>
		<td></td>
	</tr>
	<tr>
		<td>Reserve Bank Health Society Ltd</td>
		<td></td>
	</tr>
	<tr>
		<td>St.Lukes Health</td>
		<td></td>
	</tr>
	<tr>
		<td>Teachers Health Fund</td>
		<td></td>
	</tr>
	<tr>
		<td>Transport Health Pty Ltd</td>
		<td></td>
	</tr>
	<tr>
		<td>TUH</td>
		<td></td>
	</tr>
	<tr>
		<td>Westfund Limited</td>
		<td></td>
	</tr>
</table>

<br/>
<h2>Workers Compensation Insurance and Agents</h2>
<br/>

<table class="table table-striped">
	<tr>
		<th>Provider name</th>
		<th>Provider No</th>
	</tr>
	<tr>
		<td>Australian Capital Territory</td>
		<td></td>
	</tr>
	<tr>
		<td>New South Wales</td>
		<td></td>
	</tr>
	<tr>
		<td>Northern Territory</td>
		<td></td>
	</tr>
	<tr>
		<td>Queensland</td>
		<td></td>
	</tr>
	<tr>
		<td>South Australia</td>
		<td></td>
	</tr>
	<tr>
		<td>Tasmania</td>
		<td></td>
	</tr>
	<tr>
		<td>Victoria</td>
		<td>APT0983T</td>
	</tr>
	<tr>
		<td>Western Australia</td>
		<td></td>
	</tr>
</table>
<br/>
<h2>Transport Accident Insurance</h2>
<br/>

<table class="table table-striped">
	<tr>
		<th>Provider name</th>
		<th>Provider No</th>
	</tr>
	<tr>
		<td>Traffic Accident Commission (TAC)</td>
		<td>20114140</td>
	</tr>
</table>
<br/>
<br/>
<br/>
<br/>
</div>