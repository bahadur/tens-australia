
	<br/>
	<div class="main-content container">
		
	<!-- Main content -->
		<div class="col-md-7"><!--
			<div class="row">
				<h4>To Hire FreeMOM Tens, choose one of options below:</h4>
				<div class="col-md-12"><a class="btn btn-default btn-tens-action" style="width:48%; font-size:18px">FreeMOM HIRE ONLINE</a>
				<a class="btn btn-default btn-tens-action"  style="width:48%; font-size:18px" >FreeMOM HIRE APPLICATION FORM</a></div>
			</div> --> 
			<a name="toc-1"></a>
			<h3>How do I hire the COMPACT TENS?</h3> 

			<p>Print and complete the hire application form. Fax, mail or email the completed form to our office.</p>

			<a name="toc-2"></a>
			<h3>How do I pay for the COMPACT TENS hire?</h3> 
			<ul class="bullet-list">
				<li>Credit card (MasterCard or Visa )</li>
				<li>Cheque</li>
				<li>Money Order</li>
			</ul>

			<a name="toc-3"></a>
			<h3>How long can I hire the COMPACT TENS kit?</h3> 
			<p>You hire the kit for 1 month. After that trial period you may wish to purchase the COMPACT TENS outright.</p>

			<a name="toc-4"></a>
			<h3>When do I need to return the COMPACT TENS kit?</h3> 

			<p>After you have completed your trial period. If you require the kit for longer, please call us.</p>
			<a name="toc-5"></a>
			<h3>How do I return the hired COMPACT kit?</h3>

			<ul class="bullet-list">
			<li>Purchase an Australia Post Prepaid Pre-addressed satchel when you order your COMPACT TENS.</li>
			<li>Visit your local Post Office and return the kit in a bubble bag to your local COMPACT TENS Supplier office.</li>
			<li>Visit your local FreeMOM TENS Supplier office.</li>
			</ul>
			<a name="toc-6"></a>
			<h3>How do I get my COMPACT TENS Hire deposit refunded?</h3>
			<p>Print and complete the hire application form. Fax, mail or email the completed form to our office.</p>
		</div>
		<!-- Main content end -->
		<div class="col-md-4 col-md-offset-1">
			<div class="row col-md-12 green-toc">
				<h3 style="width:100%; margin:0 auto; text-align:center;margin-bottom:15px">Ready to Hire COMPACT TENS?</h3>
				<div class="col-md-12"> 
				<a class="btn btn-default btn-tens-action" href="<?= base_url() ?>products/compactTens/hire-info/onlineform" style="width:100%; font-size:18px; text-align:left;" >
					<img src="http://www.graphicsfuel.com/wp-content/uploads/2012/01/shopping-cart-icon-515.png" style="float:left;width:40px; margin-top:5px; height:40px"></img>
				<span style="margin-left:20px; margin-right:15px; text-align:left">COMPACT HIRE <br/></span>
				<span  style="margin-left:20px; margin-right:15px; text-align:left">ONLINE</a></span>
				
				<a class="btn btn-default btn-tens-action" href="<?= base_url()?>assets/applications/COMPACT_Home_Hire_Application_w.pdf"  target="_blank" style="width:100%; font-size:18px; text-align:left;" >
					<img src="http://tensaustralia.com/images/pdfico2.png" style="float:left;width:40px; height:40px"></img>
				<span style="margin-left:20px; margin-right:15px; text-align:left">COMPACT HIRE <br/></span>
				<span  style="margin-left:20px; margin-right:15px; text-align:left">APPLICATION FORM</a></span>
				</div>
			</div> 
		</div>
	</div>
	<div class="clear-footer"></div>	