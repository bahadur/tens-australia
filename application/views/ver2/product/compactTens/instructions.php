<div class="container">

    <h2>Instructions</h2>

    <div class="row">
		<div id="ibacordotcom-content">

	        <div class="col-md-8">
	          <div class="ibacordotcom-unit ibacordotcom-vid-kenca">
	            <div class="ibacordotcom_vid_play">
                    <div style="height:750px;width:500px">
                        <img src="<?= base_url()?>assets/images/indicator.gif" style="position:relative;margin:0 auto; top:25%;left:55%"></img>
                    </div>   
                </div>
	          </div>

	          <div class="instruction-pdf">
	            <p>Click the link below to download instructions in pdf format. </p>

	            <div class="instruction-pdf-link">
	              <a href="#"><img src="<?= base_url(); ?>assets/images/pdf.png" alt="pdf download link"></a>
	            </div>
	          </div>

	        </div>

            <div class="col-md-4">
                <div class="ibacordotcom-unit ibacordotcom-vid-katuhu">
                    <div class="ibacordotcom_youtube_channels">
                        <div class="ibacordotcom-vid-top"><span class="ibacordotcom-nav-title">Compact TENS - Instructions video</span><br><span
                                class="ibacordotcom-vid-by">by <a href="http://www.youtube.com/user/tensaustralia"
                                                                  target="_BLANK">tensaustralia</a></span>
                            <hr>
                            <i class="fa fa-fast-backward ibacordotcom_vid_prev" title="Previous videos"></i> <i
                                class="fa fa-fast-forward ibacordotcom_vid_next" title="Next videos"></i></div>
                        <div class="ibacordotcom-vid-bottom"> 
                        </div>
                    </div>
                </div>
            </div>

		</div>
    </div>

    <br/>
    <br/>
    <br/>
    <br/>
</div>



<script src="<?=base_url();?>assets/js/jquery.flexslider-min.js"></script>
<script src="<?=base_url();?>assets/js/jquery.fitvids.js"></script>
<script src="<?=base_url();?>assets/js/youtubeApi/youtubeApi.js"></script>
<script type="text/javascript">

var youtubeApi = new YoutubeApi($); 
$(document).ready(function(){
    youtubeApi.initDom('PL4B6BDC2FF28F75F8');  
});
</script>
 
