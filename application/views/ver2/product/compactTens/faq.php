
<br/>

<div class="main-content container">
<div class="col-md-7">

<a name="toc-1"></a>
	<h3>When should I organise a COMPACT TENS kit?</h3>
<p>Arrange an appointment with your Health Professional to assess your condition and determine if the COMPACT TENS can assist you to manage pain.</p>
<p>We usually hire and supply the kit for 30 days to give you and your Health Professional time to evaluate its effectiveness before you purchase the device. </p>

<a name="toc-2"></a>
<h3>How much is the COMPACT TENS to hire?</h3>

<table class="table table-striped">
	<tr>
		<td>Hire COMPACT TENS Kit</td>
		<td>$50.00</td>
	</tr>
	<tr>
		<td>1 x Packet of Electrodes (A range of shapes and types are available)</td>
		<td>$15.00</td>
	</tr>
	<tr>
		<td>1 x Box of 50 Clean Cote Skin Wipes</td>
		<td>$15.00</td>
	</tr>
	<tr>
		<td>Deposit (refundable)</td>
		<td>$40.00</td>
	</tr>
	<tr>
		<td>Total minimum amount to hire the kit</td>
		<td>$120.00</td>
	</tr>
	<tr>
		<td><strong>Postage options available:	</strong></td>
		<td></td>
	</tr>
	<tr>
		<td>Express Registered Postage to send the kit to you</td>
		<td>$15.00</td>
	</tr>
	<tr>
		<td>Pre-paid Pre-addressed Registered Return Postage satchel</td>
		<td>$15.00</td>
	</tr>
</table>    

<p>The $50.00 hire period is 30 days.</p>
<p>The shape and type of electrodes included are determined by a range of factors including the location or part of the body you wish to place the electrodes. The electrodes do have a limited life of a maximum of 3 weeks of continuous use. Please refer to the Product Care information page for a range of suggestions that can extend the life of the electrodes.</p> 
<p>We ask you to please return the kit in the 2 weeks after you are due to return the kit so we can prepare the kit for other people and ensure you receive your deposit refund. </p>
<a name="toc-3"></a>
<h3>What if I want to purchase the hired COMPACT TENS?</h3>
<p>We deduct the $50 hire and $40 deposit from the purchase price.</p>

<table class="table table-striped">
	<tr>
		<td>COMPACT TENS Kit</td>
		<td></td>
		<td>$175.00</td>
	</tr>
	<tr>
		<td>Less</td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>Hire COMPACT TENS Kit</td>
		<td>$50.00</td>
		<td></td>
	</tr>
	<tr>
		<td>Deposit (refundable)</td>
		<td>$40.00</td>
		<td>$90.00</td>
	</tr>
	<tr>
		<td><strong>Balance owing for the COMPACT TENS</strong></td>
		<td></td>
		<td>$75.00</td>
	</tr>
	<tr>
		<td><strong>Additional items to consider </strong></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>1 x Battery recharger pack (Includes 2 rechargable batteries and a recharger)</td>
		<td></td>
		<td>$40.00</td>
	</tr>
	<tr>
		<td>1 Packet of Electrodes (A range of shapes and types are available)</td>
		<td></td>
		<td>$15.00</td>
	</tr>
	<tr>
		<td>1 Box of 50 Clean Cote Skin Wipes</td>
		<td></td>
		<td>$15.00</td>
	</tr>
</table>  


<a name="toc-4"></a>
<h3>How much is the COMPACT TENS to purchase?</h3>

<table class="table table-striped"> 
	<tr>
		<td>COMPACT TENS Kit</td>
		<td>$175.00</td>
	</tr>
	<tr>
		<td>1 x Packet of Electrodes is included (A range of shapes and types are available)</td>
		<td></td>
	</tr>
	<tr>
		<td>1 x Box of Clean Cote Skin Wipes</td>
		<td>$15.00</td>
	</tr>
	<tr>
		<td>1 x Battery recharger pack (Includes 2 rechargable batteries and a recharger)</td>
		<td>$40.0</td>
	</tr>
	<tr>
		<td><strong>Total minimum amount to purchase the kit</strong></td>
		<td>$230.00</td>
	</tr>
	<tr>
		<td>Postage options available (if applicable):</td>
		<td></td>
	</tr>
	<tr>
		<td>Express Registered Postage to send the kit to you</td>
		<td>$15.00</td>
	</tr>
</table>  	  

<a name="toc-5"></a>
<h3>Payment options?</h3>
<ul>
<li>Credit card – Visa or Mastercard </li>
<li>Direct deposit (BSB 033-095 Account 50-0657)</li>
<li>No Amex or cheques accepted</li>
<li>Money order</li>
</ul>

<a name="toc-6"></a>
<h3>What does the COMPACT TENS kit include?</h3>
<ul>
	<li>COMPACT TENS Stimulator unit</li>
	<li>2 x Cables</li>
	<li>1 x 9V Alkaline battery.</li>
	<li>3 x Clean Cote Skin wipes.</li>
	<li>1 x Set of Electrodes</li>
	<li>Written instructions / User Manual</li>
	<li>Protective carry case</li>
</ul>
<p>Please refer to the Product Information page for more information</p>

<a name="toc-7"></a>
<h3>How do I hire or buy and access a COMPACT TENS kit?</h3>
<ul>
	<li>Complete an online hire application or purchase application.</li>
	<li>Call 1300 913 129 and we can help prepare the hire or purchase application.</li>
	<li>E-mail info@tensaustralia.com a completed hire or purchase application.</li>
	<li>Fax 1300 913 149 a completed hire or purchase application to our office</li>
	<li>Visit, you are very welcome to collect and or return the kit to our head office in Melbourne</li>
</ul>

<a name="toc-8"></a>
<h3>Do Private Health Insurance Funds refund the hire or purchase of a COMPACT TENS kit?</h3>
<p>Please refer to the <a href="insurance">Insurance page</a>. </p>

<a name="toc-9"></a>
<h3>When do I have to return the COMPACT TENS kit?</h3>
<p>We ask you to please return the kit in the 2 weeks after you are due to return the kit so we can prepare the kit for other people and ensure you receive your deposit refund. </p>

<a name="toc-10"></a>
<h3>Where should I return the COMPACT TENS kit?</h3>
<p>It is important that you do not leave the hire kit at a hospital or health professional. The hospital or health professional is not responsible for the hired kit.</p>
<ul>
	<li>Post the kit using the Pre-paid Pre-addressed Registered option supplied with the kit.</li>
	<li>Visit your local Australia Post office and return the kit to our head office in Moorabbin in a bubble bag to protect the equipment during transit.</li>
	<li>You are welcome to visit and return the kit at head office in Moorabbin.</li>
</ul>
<br/>
<br/>
<br/>
<br/>



</div>

<div class="col-md-4 col-md-offset-1 blue-toc">

	<ul class="toc-content">
		<li><a href="#toc-1"> When should I organise a COMPACT TENS kit?</li>
		<li><a href="#toc-2"> How much is the COMPACT TENS to hire?</a></li>
		<li><a href="#toc-3"> What if I want to purchase the hired COMPACT TENS?</a></li>
		<li><a href="#toc-4"> How much is the COMPACT TENS to purchase?</a></li>
		<li><a href="#toc-5"> Payment options</li> 
		<li><a href="#toc-6"> What does the COMPACT TENS kit include?</li> 
		<li><a href="#toc-7"> How do I hire or buy and access a COMPACT TENS kit?</li> 
		<li><a href="#toc-8"> Do Private Health Insurance Funds refund the hire or purchase of a COMPACT TENS kit?</li> 
		<li><a href="#toc-9"> When do I have to return the COMPACT TENS kit?</li> 
		<li><a href="#toc-10"> Where should I return the COMPACT TENS kit?</li>  
		 
	</ul> 
</div>

</div>