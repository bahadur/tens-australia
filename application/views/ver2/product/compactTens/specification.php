
<br/>
<br/>
<br/>

<div class="main-content container"> 
<table class="table table-striped">
	<tr><td>Size:</td>  	<td>(L) 80 mm (W) 60 mm (D) 28 mm</td></tr>
<tr><td>Weight:  </td>	<td>90 gm/3.0 oz. (Excl. Battery)</td></tr>
<tr><td>Waveform:</td>  	<td>Symmetrical rectangular biphasic, with zero net DC current</td></tr>
<tr><td>Channels:</td>  	<td>Two, electrically isolated</td></tr>
<tr><td>Current Output:</td>  	<td>Each channel 0-60mA into 1K ohms</td></tr>
<tr><td>Pulse Width: </td> 	<td>30 - 300 µ/sec - adjustable</td></tr>
<tr><td>Pulse Rate: </td> 	<td>1-200 pps - adjustable</td></tr>
<tr><td>Modes: </td> 	<td></td></tr>
<tr><td>Modulation type:</td>  	<td>(Patented) Width (Patented)</td></tr>
<tr><td>Width: </td> 	<td>50% of width setting +/- 15%</td></tr>
<tr><td>Rate:</td>  	<td>20% of width setting +/- 15%</td></tr>
<tr><td>Modulation Rate:</td>  	<td>8 – 10 Hz</td></tr>
<tr><td>Burst:</td>  	<td>250 µ/sec on, 250 µ/sec off</td></tr>
<tr><td>Battery test:</td>  	<td>Green LED indicates battery OK<br/>
Red LED indicates low battery (5.5 volts)</td></tr>
<tr><td>Battery source:</td>  	<td>9V Alkaline battery</td></tr>
<tr><td>Battery type:</td>  	<td>9V Alkaline battery<br/>
									40 – 50Hrs. (Using a new Alkaline Battery).</td></tr>
<tr><td>Battery life:</td>  	<td>At 30mA into 2K, 2 channels, continuous pulse width <br/>
100 µ/sec Battery life approx. 60 hours</td></tr>
</table>

<br/>
<br/>
<br/>
<br/>
</div>