
<br/>

<div class="main-content container">

<div class="col-md-7">
<a name="toc-1"></a>
<h3>Electrodes</h3>
<p>TENS Electrodes include two characteristics – conductivity and flexibility and so they are made with rubber and carbon fibres. The additional application of an adhesive compound ensures that the electrodes are reusable based on several conditions outlined below.</p>
<p>You will achieve the best possible results and minimise the chances for the development of skin sensitivity to the use of the electrodes by observing the following good practices:</p>
<p>1.	Important to apply a TENS Clean-Cote Skin Wipe to the electrode placement site prior to the application of the electrodes. Clean-Cote Skin Wipes contain Alcohol and Silicone. Cleans and disinfects the application area, and increases electrode conductivity. Shields the the skin from adhesives and possible irritation. Extends the life of the electrodes.</p>

<p>2.	Electrodes have limited usage which is stated on the instructions. If you exceed that period, the skin may become irritated, the skin under the electrode placement site may feel “prickly” and become red.</p>

<p>3.	After the treatment period, remove the electrodes from the skin, place the electrodes on their ‘Teflon’ backing sheet and store the electrodes in their resealable bag.</p>

<p>4.	After the treatment period, wash the skin with water then ‘dab or pat dry’. Apply a mild moisturising cream to the electrode placement skin location(s). For the next treatment period, items 1 to 4 are repeated.</p>

<p>5.	Electrodes can appear a little dull after approx 7 - 10 days of use. Rejuvenate the electrodes by applying a few drops of water onto the adhesive surface/section of the electrode and rubbing this in a circular motion. When the adhesive surface looks “shiny”, place the electrode onto the ‘Teflon’ backing sheet and store the electrodes in their resealable bag. Keep the resealable bag in a cool location overnight (usually a refrigerator) where the electrode will rehydrate.</p>

<p>6.	Please DO NOT apply electrodes to broken skin. If irritation occurs, discontinue the treatment and consult your therapist for advice.</p>

<p>7.	Switch off the TENS and remove the electrodes from your skin prior to going into a bath, shower or swimming pool.</p>

<p>8.	When using TENS stimulation, always ensure that no other person is able to tamper with your stimulator.</p>

<p>9.	If in doubt as to which electrodes to use for either an existing or changing condition, always ask your therapist for advice.</p>

<p>10.	Electrodes, when not in use, should always be kept in a cool place. Spare electrodes should only be opened when required as they may dry out in warm conditions.</p>


<a name="toc-2"></a>
<h3>TENS Cables</h3>
<p>Cables are used to transmit the pulsations between the electrodes and the TENS stimulator. Cables must be looked after to ensure a trouble free performance. This is achieved by following the observations outlined below:</p>
<p>I.	Cables should be periodically wiped with a soft cloth with methylated spirits.</p>
<p>II.	Cables should not be left lying exposed to strong sun-light as the plastic insulation will harden and crack.</p>
<p>III.	DO NOT pull on the cable when removing the cable plugs from the electrode sockets. ALWAYS hold the cable plug and electrode socket and “twist” them together or apart as required.</p>
<p>IV.	Note: the RED and BLACK connectors can be connected to any electrode as the COLOUR plays no part in the treatment.</p>
<p>V.	The Co-axial right angled plug section that goes to the TENS stimulator must not be bent tightly as the copper connection can break IF the cable is harshly treated.</p>


<a name="toc-3"></a>
<h3>Batteries for use with TENS Stimulators</h3>
<p>The batteries that are used with our TENS stimulators have been tested to ensure that they meet the “life-span”, reliability and repeatability when required to operate.</p>
<p>The 9V Alkaline Battery selected has a continuous constant output for the duration of its life and meets our specification requirements. We have selected the Duracell PROCELL, but you may wish to use other 9V Alkaline manufacturers whose products are available in your location.</p>
<p>a)	ALWAYS ensure that you start your treatment with a fresh 9V Alkaline battery.</p>
<p>b)	Do NOT leave batteries exposed in sunlight but keep them under supervision.</p>
<p>c)	Do NOT let children play with your batteries as they may flatten them and then return them to you TENS case without informing you that the battery is now flat.</p>
<p>d)	Ensure that you dispose of used batteries in the prescribed manner.</p>


<a name="toc-4"></a>
<h3>Clean-Cote Wipes</h3>
<p>The Clean-Cote wipe contains an alcohol and Silicon solution that both cleans the skin from any oily products or sweat that may be at the electrode placement sites and covers the site with a protective silicone coating that stops sweat getting under the electrodes and ensures a good conductive interface between the electrode and the skin.</p>
<p>1)	ONE wipe should always be used to clean the skin before electrodes are applied to the skin.</p>
<p>2)	Dispose of the used wipe and its container after use. DO NOT leave the wipe of its container on any polished surface as the alcohol may damage the surface.</p>
<p>3)	If during treatment with the TENS you wish to go for a shower or bath, you must first switch off the TENS stimulator and remove the electrodes from the skin. IF you wish to continue with the TENS treatment after the shower or bath, you will NOT require to wipe the skin again with a Clean-Cote wipe as the Silicon protective coating does not wear off for a period of 24 hours.</p>


<a name="toc-5"></a>
<h3>TENS Stimulator service</h3>
<p>We recommend that your TENS stimulator is cleaned, serviced, calibrated and tested every 12 months. It is prudent to ensure that the TENS stimulator is fully functional at all times.</p>
<p>We have the qualified staff and capabilities to test every feature of all TENS stimulators that we market. The cost for that service is well worth the peace of mind that it brings knowing that your stimulator meets your requirements when and if you need to use it.</p>
 
</div>
<div class="col-md-4 col-md-offset-1 blue-toc">

	<ul class="toc-content">
		<li><a href="#toc-1"> Electrodes</li>
		<li><a href="#toc-2"> TENS Cables</a></li>
		<li><a href="#toc-3"> Batteries for use with TENS Stimulators </a></li>
		<li><a href="#toc-4"> Clean-Cote Wipes</li> 
		<li><a href="#toc-5"> TENS Stimulator service</li>  
		 
	</ul> 
</div>

</div>
<br/>
<br/>
<br/>
<br/>