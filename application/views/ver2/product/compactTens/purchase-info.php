
	<br/>
	<div class="main-content container">
		
	<!-- Main content -->
		<div class="col-md-7">

			<h3>How do I pay for my purchase?</h3>
<ul class="bullet-list">
	<li>Credit card (Mastercard or Visa )</li>
	<li>Cheque</li>
	<li>Money Order</li>
</ul>

<h3>Can I purchase additional electrodes in the future?</h3>
<p>Yes, repeat the above steps to purchase additional accessories.</p>

<h3>How long will my order take to arrive once I pay for my order?</h3>

<table class="table table-striped">
	<tr>
		<td>Adelaide metro</td>
		<td>Next business day</td>
	</tr>
	<tr>
		<td>Brisbane metro</td>
		<td>Two business days</td>
	</tr>

	<tr>
		<td>Canberra metro</td>
		<td>Next business day</td>
	</tr>

	<tr>
		<td>Darwin metro</td>
		<td>Two business days</td>
	</tr>

	<tr>
		<td>Hobart metro</td>
		<td>Two business days</td>
	</tr>

	<tr>
		<td>Melbourne metro</td>
		<td>Next business day</td>
	</tr>

	<tr>
		<td>Perth metro</td>
		<td>Two business days</td>
	</tr>
	  
	<tr>
		<td>Sydney metro</td>
		<td>Next business day</td>
	</tr>
	<tr>
		<td>International</td>
		<td>Available by air express</td>
	</tr> 
</table>
		</div>
		<!-- Main content end -->
		<div class="col-md-4 col-md-offset-1"> 
			<div class="row col-md-12 green-toc">
			<h3 style="width:100%; margin:0 auto; text-align:center;margin-bottom:15px">Ready to buy COMPACT TENS?</h3>
				<div class="col-md-12"> 
				<a class="btn btn-default btn-tens-action" href="<?= base_url() ?>products/compactTens/purchase-info/onlineform"  style="width:100%; font-size:18px; text-align:left;" >
					<img src="http://www.graphicsfuel.com/wp-content/uploads/2012/01/shopping-cart-icon-515.png" style="float:left;width:40px; margin-top:5px; height:40px"></img>
				<span style="margin-left:20px; margin-right:15px; text-align:left">COMPACT BUY <br/></span>
				<span  style="margin-left:20px; margin-right:15px; text-align:left">ONLINE</a></span>
				
				<a class="btn btn-default btn-tens-action" href="<?= base_url()?>assets/applications/TENS_R_US_Order_Form.pdf"  target="_blank" style="width:100%; font-size:18px; text-align:left;" >
					<img src="http://tensaustralia.com/images/pdfico2.png" style="float:left;width:40px; height:40px"></img>
				<span style="margin-left:20px; margin-right:15px; text-align:left">COMPACT BUY <br/></span>
				<span  style="margin-left:20px; margin-right:15px; text-align:left">APPLICATION FORM</a></span>
				</div>
			</div>
		</div>
	</div>
	<div class="clear-footer"></div>	