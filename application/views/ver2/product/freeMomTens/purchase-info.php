
	<br/>
	<div class="main-content container">
		
	<!-- Main content -->
		<div class="col-md-7"><!--
			<div class="row">
				<h4>To Hire FreeMOM Tens, choose one of options below:</h4>
				<div class="col-md-12"><a class="btn btn-default btn-tens-action" style="width:48%; font-size:18px">FreeMOM HIRE ONLINE</a>
				<a class="btn btn-default btn-tens-action"  style="width:48%; font-size:18px" >FreeMOM HIRE APPLICATION FORM</a></div>
			</div> -->
			<a name="toc-1"></a>
			<h3>How much is the <span class="brush-font">Free</span>MOM to buy?</h3>
			
<table class="table table-striped">
	<thead>
	<tr>
		<th style="width:70%"></th>
		<th>AUD</th>
	</tr>
	</thead>
	<tr>
		<td><span class="brush-font">Free</span>MOM TENS Kit (Incl. GST) for Labour Pain Management<br/>
(Product Code T38053)<br/>
Supplied in a case with 2 x 9V Alkaline Batteries, Cable, ‘After-birth’ Electrodes, Written instructions and an instructional DVD.</td>
		<td>$260.00</td>
	</tr>
	<tr>
		<td>Labour Electrodes <br/>(Product Code E30480)</td>
		<td>$35.00</td>
	</tr> 
	<tr>
		<td style="float:right;font-weight:bold">Total</td>
		<td style="font-weight:bold">$295.00</td>
	</tr>
	<tr>
		<td style="font-weight:bold">If applicable, postage or shipping</td>
		<td></td>
	</tr>
	<tr>
		<td>Postage - Australia Post delivery to your address</td>
		<td>$15.00</td>
	</tr>
	<tr>
		<td style="font-weight:bold">If applicable, OPTIONS</td>
		<td></td>
	</tr>
	<tr>
		<td>Back Pain or Pelvic Instability – Butterfly Electrodes with Clean-cote Skin Wipes and battery</td>
		<td>$20.00</td>
	</tr> 
	<tr>
		<td>Neck and Shoulder Pain – Neck and Shoulder Electrodes with Clean-cote Skin Wipes and battery</td>
		<td>$20.00</td>
	</tr>
</table>
			<a name="toc-2"></a>
	<h3>Do Private Health Insurance Funds refund the purchase of a <span class="brush-font">Free</span>MOM TENS kit?</h3>
	<p>If you are a member of a private health insurance provider please consider contacting your fund as you may be entitled to make a claim.</p>
<p>Example:</p>
<p>Bupa Australia Provider Number: EP08949 - HBA Recognised Provider - Mutual Community Recognised Provider - MBF Recognised Provider - MBF Alliances Recognised Provider</p>
			
			<a name="toc-3"></a>
			<h3>How do I purchase the <span class="brush-font">Free</span>MOM TENS?</h3>
			<ol class="numbered-list">
				<li>
					<p> User our <a href="#">Online Form</a></p>
				</li>
				<li>
					<p> Call our office during business hours and we complete the <span class="brush-font">Free</span>MOM TENS hire application form with you over the telephone.</p>
					<p><a href="#">Tel: 1300 913 129</a></p>
					</li>
				<li>
					<p> Fax the completed <span class="brush-font">Free</span>MOM TENS Hire application form to our office </p>
					<p><a href="#">Fax: 1300 913 149</a></p>
				</li>
					<li  >
					<p> Mail the completed <span class="brush-font">Free</span>MOM TENS Hire application form to our office </p>
					<p><a href="#">7 / 2 Central Avenue, MOORABBIN VICTORIA 3189 Australia</a></p>
				</li>
					<li  >
					<p> Scan and email the completed <span class="brush-font">Free</span>MOM TENS Hire application form to our office </p>
					<p><a href="#">info@tensaustralia.com</a></p>
				</li>
			</ol>
			<a name="toc-4"></a>
			<h3>How do I pay for the <span class="brush-font">Free</span>MOM TENS purchase?</h3>
			<ol class="numbered-list">	
				<li>Credit card (Master or Visa )</li>
				<li>Cheque</li>
				<li>Money Order</li>
			</ol>
			<a name="toc-5"></a>
			<h3>Can I purchase additional electrodes in the future?</h3>
			<p>	Yes, repeat the above steps to purchase additional accessories.</p> 
			<a name="toc-6"></a>
			<h3>How long will my order take to arrive once I pay for my order?</h3>
			<table class="table table-striped">
				<tr>
					<td>Adelaide metro</td>
					<td>Next business day</td>
				</tr>
				<tr>
					<td>Brisbane metro</td>
					<td>Two business days</td>
				</tr>
				<tr>
					<td>Canberra metro</td>
					<td>Next business day</td>
				</tr>
				<tr>
					<td>Darwin metro</td>
					<td>Two business days</td>
				</tr>
				<tr>
					<td>Hobart metro</td>
					<td>Two business days</td>
				</tr>
				<tr>
					<td>Melbourne metro</td>
					<td>Next business day</td>
				</tr>
				<tr>
					<td>Adelaide metro</td>
					<td>Next business day</td>
				</tr>
				<tr>
					<td>Perth metro</td>
					<td>Two business days</td>
				</tr>
				<tr>
					<td>Sydney metro</td>
					<td>Next business day</td>
				</tr>
				<tr>
					<td>International</td>
					<td>Available by air express</td>
				</tr>
			</table>
		</div>
		<!-- Main content end -->
		<div class="col-md-4 col-md-offset-1">
			<div class="row col-md-12 green-toc">
				<h3 style="width:100%; margin:0 auto; text-align:center;margin-bottom:15px">Ready to Buy <span class="brush-font">Free</span>MOM?</h3>
				<div class="col-md-12"> 
				<a class="btn btn-default btn-tens-action" href="<?= base_url() ?>products/freeMomTens/purchase-info/onlineform"  style="width:100%; font-size:18px; text-align:left;" >
					<img src="http://www.graphicsfuel.com/wp-content/uploads/2012/01/shopping-cart-icon-515.png" style="float:left;width:40px; margin-top:5px; height:40px"></img>
				<span style="margin-left:20px; margin-right:15px; text-align:left"><span class="brush-font">Free</span>MOM BUY <br/></span>
				<span  style="margin-left:20px; margin-right:15px; text-align:left">ONLINE</a></span>
				
				<a class="btn btn-default btn-tens-action" href="<?= base_url()?>assets/applications/TENS_R_US_Order_Form.pdf" target="_blank" style="width:100%; font-size:18px; text-align:left;" >
					<img src="http://tensaustralia.com/images/pdfico2.png" style="float:left;width:40px; height:40px"></img>
				<span style="margin-left:20px; margin-right:15px; text-align:left"><span class="brush-font">Free</span>MOM BUY <br/></span>
				<span  style="margin-left:20px; margin-right:15px; text-align:left">APPLICATION FORM</a></span>
				</div>
			</div> 
			<div class="row col-md-12 blue-toc" style="margin-top:55px">
				<ul class="toc-content"> 
					<li><a href="#toc-1"> How much is the <span class="brush-font">Free</span>MOM to buy?</a></li>
					<li><a href="#toc-2"> Do Private Health Insurance Funds refund the purchase of a <span class="brush-font">Free</span>MOM TENS kit?</a></li>
					<li><a href="#toc-3"> How do I purchase the <span class="brush-font">Free</span>MOM TENS?</a></li>
					<li><a href="#toc-4"> How do I pay for the <span class="brush-font">Free</span>MOM TENS purchase?</a></li>
					<li><a href="#toc-5"> Can I purchase additional electrodes in the future?</a></li>
					<li><a href="#toc-6"> How long will my order take to arrive once I pay for my order?</a></li> 
				</ul>
				
			</div>
		</div>
	</div>
	<div class="clear-footer"></div>	