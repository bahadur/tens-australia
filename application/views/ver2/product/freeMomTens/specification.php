
<br/>

<div class="main-content container"> 
<table class="table table-striped">
	<tr><td>Size:</td>  	<td>(L) 13 cm (W) 3 cm (D) 2.3 cm</td></tr>
<tr><td>Weight:  </td>	<td>100 gm. (incl. Battery)</td></tr>
<tr><td>Waveform:</td>  	<td>Symmetrical Rectangular Biphasic</td></tr>
<tr><td>Channels:</td>  	<td>One (1)</td></tr>
<tr><td>Current Output:</td>  	<td>60mA Peak into 1K ohm load</td></tr>
<tr><td>Pulse Width: </td> 	<td>50 to 550 MicroSec.</td></tr>
<tr><td>Pulse Rate: </td> 	<td>80 Hz.</td></tr>
<tr><td>Modulation:</td>  	<td>3 to 10 Hz. (FM)</td></tr>
<tr><td>Green LED: </td> 	<td>Flashing when “Boost Mode is OFF”</td></tr>
<tr><td>Green LED:</td>  	<td>Stays ON when “Boost Mode is ON”</td></tr>
<tr><td>RED LED:</td>  	<td>ON when Battery requires changing</td></tr>
<tr><td>Battery type:</td>  	<td>9V Alkaline</td></tr>
<tr><td>Battery Life:</td>  	<td>(Approx.) 30 Hours</td></tr>
</table>
</div>