
<br/>

<div class="main-content container">
	<div class="col-md-7">
	<a name="toc-1"></a>
<h3>When should I organise a <span class="brush-font">Free</span>MOM TENS kit?</h3>
<p>Your due date is an estimate only.</p><p>
We usually hire and supply the kit at week 37 to 38 of your pregnancy. 
</p>
<br/>
<br/>
<a name="toc-2"></a>
<h3>How much is the <span class="brush-font">Free</span>MOM TENS to hire?</h3>

<br/> 
<table class="table table-striped">
	<tr>
		<td><strong>Hire</strong> <span class="brush-font">Free</span>MOM TENS Kit</td>
		<td>$50.00</td>
	</tr>
	<tr>
		<td>Labour Obstetric Electrodes </td>
		<td>$35.00</td>
	</tr>

	<tr>
		<td style="font-color:red">Deposit (refundable)</td>
		<td>$40.00</td>
	</tr>
	<tr>
		<td><span style="float:right">Total minimum amount to <strong>hire</strong> the kit</span></td>
		<td>$125.00</td>
	</tr>
	<tr>
		<td><strong>Postage options available:</strong></td>
		<td></td>
	</tr>
	<tr>
		<td><strong>Express Registered</strong> Postage to send the kit to you</td>
		<td>$15.00</td>
	</tr>
	<tr>
		<td><strong>Pre-paid</strong> Pre-addressed <strong>Registered</strong> Return Postage satchel</td>
		<td>$15.00</td>
	</tr> 
</table>
<br/>
<br/>

<a name="toc-3"></a>
<h3>How much is the <span class="brush-font">Free</span>MOM TENS to purchase?</h3>
<br/> 
<table class="table table-striped">
	<tr>
		<td><span class="brush-font">Free</span>MOM TENS Kit</td>
		<td>$260.00</td>
	</tr>
	<tr>
		<td>Labour Obstetric Electrodes</td>
		<td>$35.00</td>
	</tr>
 
	<tr>
		<td><span style="float:right">Total minimum amount to <strong>purchase</strong> the kit</span></td>
		<td>$295.00</td>
	</tr>
	<tr>
		<td><strong>Postage available (if applicable):</strong></td>
		<td></td>
	</tr>
	<tr>
		<td><strong>Express Registered</strong> Postage to send the kit to you</td>
		<td>$15.00</td>
	</tr> 
</table>

<br/>
<br/>
<a name="toc-4"></a>
<h3>Payment options ?</h3>
<ul>
<li>Credit card – Visa or Mastercard </li>
<li>Direct deposit (BSB 033-095 Account 50-0657)</li>
<li>No Amex or cheques accepted</li>
<li>Money order</li>
</ul>

<br/>
<br/>
<a name="toc-5"></a>
<h3>What does the <span class="brush-font">Free</span>MOM TENS kit include?</h3>
<ul>
<li><span class="brush-font">Free</span>MOM TENS Stimulator unit</li>
<li>Lanyard</li>
<li>Cable</li>
<li>2 x 9V Alkaline batteries</li>
<li>3 x Clean Cote Skin wipes</li>
<li>1 x Set of Labour Obstetric Electrodes (Includes 1 Clean Cote Skin Wipe)</li>
<li>1 x Set of After birth Electrodes</li>
<li>DVD Instructions</li>
<li>Written instructions / User Manual</li>
<li>Protective carry case</li>
</ul>
<br/>
<p>Please refer to the <a href="..">Product Information page</a> for more information</p>

<br/>
<br/>
<a name="toc-6"></a>
<h3>How do I hire or buy and access a <span class="brush-font">Free</span>MOM TENS kit?</h3>
<ul>
<li>Complete an <a href="<?=base_url();?>products/freeMomTens/hire-info">online hire</a> application or <a href="<?=base_url();?>products/freeMomTens/purchase-info">purchase</a> application.</li>
<li>Call 1300 913 129 and we can help prepare the hire or purchase application.</li>
<li>E-mail <a href="email:info@tensaustralia.com">info@tensaustralia.com</a> a completed hire or purchase application.</li>
<li>Fax 1300 913 149 a completed hire or purchase application to our office</li>
<li>Visit, you are very welcome to collect and or return the hired kit to our head office in Moorabbin, Victoria</li>
</ul>

<br/>
<br/>
<a name="toc-7"></a>
<h3>Do Private Health Insurance Funds refund the hire or purchase of a <span class="brush-font">Free</span>MOM TENS kit?</h3>
<p>Please refer to the <a href="./insurance">Insurance page</a>. </p>
<br/>
<br/>
<a name="toc-8"></a>
<h3>When do I have to return the <span class="brush-font">Free</span>MOM TENS kit?</h3>
<p>We ask you to please return the kit in the <strong>2 weeks</strong> after you are due to return the kit so we can prepare the kit for other people and ensure you receive your <strong>deposit refund</strong>. </p>

<br/>
<br/>
<a name="toc-9"></a>
<h3>Where should I return the <span class="brush-font">Free</span>MOM TENS kit?</h3>
<p>It is important that you do not leave the hire kit at a hospital. The hospital is not responsible for the kit.</p>
<p>•	Post the kit using the Pre-paid Pre-addressed Registered option supplied with the kit.</p>
<p>You did not purchase the Pre-paid Pre-addressed Registered return satchel with the hired kit, you can select from the following options.</p>
<p>•	Visit your local Australia Post office and return the kit to our head office in Moorabbin in a bubble bag to protect the equipment during transit.</p>
<p>•	You are welcome to visit and return the kit at head office in Moorabbin.</p>
<br/><br/><br/><br/><br/>
</div>

<div class="col-md-4 col-md-offset-1 blue-toc">

	<ul class="toc-content">
		<li><a href="#toc-1"> When should I organise a <span class="brush-font">Free</span>MOM TENS?</a> kit?</li>
		<li><a href="#toc-2"> How much is the <span class="brush-font">Free</span>MOM TENS to hire?  kit?</a></li>
		<li><a href="#toc-3"> How much is the <span class="brush-font">Free</span>MOM TENS to purchase? </a></li>
		<li><a href="#toc-4"> Payment options</li> 
		<li><a href="#toc-5"> What does the <span class="brush-font">Free</span>MOM TENS kit include?</li> 
		<li><a href="#toc-6"> How do I hire or buy and access a <span class="brush-font">Free</span>MOM TENS kit?</li> 
		<li><a href="#toc-7"> Do Private Health Insurance Funds refund the hire or purchase of a <span class="brush-font">Free</span>MOM TENS kit?</li> 
		<li><a href="#toc-8"> When do I have to return the <span class="brush-font">Free</span>MOM TENS kit?</li> 
		<li><a href="#toc-9"> Where should I return the <span class="brush-font">Free</span>MOM TENS kit?</li>  
		 
	</ul> 
</div>
</div>