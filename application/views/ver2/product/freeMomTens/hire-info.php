
	<br/>
	<div class="main-content container">
		
	<!-- Main content -->
		<div class="col-md-7"><!--
			<div class="row">
				<h4>To Hire FreeMOM Tens, choose one of options below:</h4>
				<div class="col-md-12"><a class="btn btn-default btn-tens-action" style="width:48%; font-size:18px">FreeMOM HIRE ONLINE</a>
				<a class="btn btn-default btn-tens-action"  style="width:48%; font-size:18px" >FreeMOM HIRE APPLICATION FORM</a></div>
			</div> -->
			<a name="toc-1"></a>
			<h3>How much is the FreeMOM to hire?</h3>
			
<table class="table table-striped">
	<thead>
	<tr>
		<th style="width:70%"></th>
		<th>AUD</th>
	</tr>
	</thead>
	<tr>
		<td><span class="brush-font">Free</span>Mom TENS Hire (Incl. GST) - for the term of your confinement</td>
		<td>$50.00</td>
	</tr>
	<tr>
		<td>Labour Electrodes - refundable if not opened, used or damaged</td>
		<td>$35.00</td>
	</tr>
	<tr>
		<td>Deposit - refundable on the return of the <span class="brush-font">Free</span>Mom TENS kit </td>
		<td>$40.00</td>
	</tr>
	<tr>
		<td style="float:right;font-weight:bold">Subtotal</td>
		<td style="font-weight:bold">$125.00</td>
	</tr>
	<tr>
		<td style="font-weight:bold">If applicable, postage or shipping</td>
		<td></td>
	</tr>
	<tr>
		<td>Postage - Australia Post delivery to your address</td>
		<td>$15.00</td>
	</tr>
	<tr>
		<td>Prepaid return Postage Satchel - Australia post delivery to our office</td>
		<td>$15.00</td>
	</tr>
	<tr>
		<td style="font-weight:bold">If applicable, OPTIONS</td>
		<td></td>
	</tr>
	<tr>
		<td>Back Pain or Pelvic Instability – Butterfly Electrodes with Clean-cote Skin Wipes and battery</td>
		<td>$20.00</td>
	</tr>
	<tr>
		<td>Neck and Shoulder Pain – Neck and Shoulder Electrodes with Clean-cote Skin Wipes and battery</td>
		<td>$20.00</td>
	</tr>
</table>
			<a name="toc-2"></a>
			<h3>How do I hire the FreeMOM TENS?</h3>
			<ol class="numbered-list">
				<li>
					<p>User our <a href="#">Online Form</a></p>
				</li>
				<li>
					<p>Call our office during business hours and we complete the FreeMOM TENS hire application form with you over the telephone.</p>
					<p><a href="#">Tel: 1300 913 129</a></p>
					</li>
				<li>
					<p>Fax the completed FreeMOM TENS Hire application form to our office </p>
					<p><a href="#">Fax: 1300 913 149</a></p>
				</li>
					<li>
					<p>Mail the completed FreeMOM TENS Hire application form to our office </p>
					<p><a href="#">7 / 2 Central Avenue, MOORABBIN VICTORIA 3189 Australia</a></p>
				</li>
					<li>
					<p>Scan and email the completed FreeMOM TENS Hire application form to our office </p>
					<p><a href="#">info@tensaustralia.com</a></p>
				</li>
			</ol>
			
			<a name="toc-3"></a>
			<h3>How do I pay for the FreeMOM TENS hire?</h3>
			
			<ol class="numbered-list">	
				<li>Credit card (Master or Visa )</li>
				<li>Cheque</li>
				<li>Money Order</li>
			</ol>
			
			<a name="toc-4"></a>
			<h3>How long can I hire the FreeMOM TENS kit?</h3>
			<p>	You hire the kit for the term of your labour.</p>

			<a name="toc-5"></a>
			<h3>When do I need to return the FreeMOM TENS kit?</h3>
			<p>We ask that you return the kit in the week after you leave the hospital.</p>	
			
			<a name="toc-6"></a>
			<h3>How do I return the hired FreeMOM kit?</h3>
				<ol class="numbered-list">
					<li>Purchase a prepaid pre-addressed return satchel with your hire application </li>
					<li>Visit your local Post Office and send the kit in a bubble bag to our office  </li>
					<li>(Office address: 7 / 2 CENTRAL AVENUE MOORABBIN VICTORIA 3189)  </li>
					<li>Visit your local office - Head Office : 7 / 2 Central Avenue, MOORABBIN VICTORIA 3189 </li>
				</ol>
		</div>
		<!-- Main content end -->
		<div class="col-md-4 col-md-offset-1">
			<div class="row col-md-12 green-toc">
			<h3 style="width:100%; margin:0 auto; text-align:center;margin-bottom:15px">Ready to Hire FreeMOM?</h3>
				<div class="col-md-12"> 
				<a class="btn btn-default btn-tens-action" href="<?= base_url() ?>products/freeMomTens/hire-info/onlineform" style="width:100%; font-size:18px; text-align:left;" >
					<img src="http://www.graphicsfuel.com/wp-content/uploads/2012/01/shopping-cart-icon-515.png" style="float:left;width:40px; margin-top:5px; height:40px"></img>
				<span style="margin-left:20px; margin-right:15px; text-align:left">FreeMOM HIRE <br/></span>
				<span  style="margin-left:20px; margin-right:15px; text-align:left">ONLINE</a></span>
				
				<a class="btn btn-default btn-tens-action" href="<?= base_url()?>assets/applications/TENS_R_US_Freemom_Home_Hire_Application_Web.pdf" target="_blank" style="width:100%; font-size:18px; text-align:left;" >
					<img src="http://tensaustralia.com/images/pdfico2.png" style="float:left;width:40px; height:40px"></img>
				<span style="margin-left:20px; margin-right:15px; text-align:left">FreeMOM HIRE <br/></span>
				<span  style="margin-left:20px; margin-right:15px; text-align:left">APPLICATION FORM</a></span>
				</div>
			</div>
			<div class="row col-md-12 blue-toc">
				<ul class="toc-content"> 
					<li><a href="#toc-1" > How much is the FreeMOM to hire?</a></li>
					<li><a href="#toc-2" > How do I hire the FreeMOM TENS?</a></li>
					<li><a href="#toc-3" > How do I pay for the FreeMOM TENS hire?</a></li>
					<li><a href="#toc-4" > How long can I hire the FreeMOM TENS kit?</a></li>
					<li><a href="#toc-5" > When do I need to return the FreeMOM TENS kit?</a></li>
					<li><a href="#toc-6" > How do I return the hired FreeMOM kit?</a></li> 
				</ul>
				
			</div>
		</div>
	</div>
	<div class="clear-footer"></div>	