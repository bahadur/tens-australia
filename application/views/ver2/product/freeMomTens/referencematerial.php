
<br/>

<div class="main-content container">
<br/>
<h2>FreeMOM Tens Reference Material</h2>
<br/>
<table class="table table-striped">
<tr><th>Content</th><th>PDF</th></tr>
<tr><td>Transcutaneous Electrical Nerve Stimulation(TENS) as a pain-relief device in Obstetrics and Gynecology</td>
<td><a href="<?= base_url()?>assets/pdfs/referencematerials/freemomtens/C_E_FREEMOM_SPEC_REF.pdf" target="_blank"><img src="<?= base_url()?>assets/images/pdfico2.png"></img></a></td></tr>
<tr>
<td>Transcutaneous Electrical Nerve Stimulation(TENS) for adjuvant pain-relief during labor and delivery</td>
<td><a href="<?= base_url()?>assets/pdfs/referencematerials/freemomtens/O_G_FREEMOM_USAGE_REF.pdf" target="_blank"><img src="<?= base_url()?>assets/images/pdfico2.png"></img></a></img>
</td></tr>
<tr><td>FreeMOM TEST Evaluation results</td>
<td><a href="<?= base_url()?>assets/pdfs/referencematerials/freemomtens/2_Years_of_ Freemom_Test_Results_Fairfield_Hospital_NSW_P_Avis_2005_2_.pdf" target="_blank"><img src="<?= base_url()?>assets/images/pdfico2.png"></img></a></img>
</td>
</tr> 
</table>

<p>You are welcome to call or email our office if you have any questions or require additional information.</p>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
</div>