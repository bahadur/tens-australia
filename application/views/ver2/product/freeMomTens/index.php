
	<br/>
	<div class="main-content container">
	
	<!-- Main content -->
		<div class="col-md-7">
			<a name="toc-1"></a>
			<h1>What is the <span class="brush-font">Free</span>MOM TENS?</h1>
			<image height="200px" src="<?=base_url();?>assets/images/freemom-pic-1.png" style="float:right"></image>
<p><span class="brush-font">Free</span>MOM TENS is a Transcutaneous Electrical Nerve Stimulation device which has been specifically designed to assist in the relief of pain during labour, after-birth pain, assistance with sleeping following the delivery and lower back pain.	 </p>
			<a name="toc-2"></a>
			<h1>How does it Work?</h1>
<p><span class="brush-font">Free</span>MOM TENS emits a small, electrical pulse into the body via electrodes which are placed onto the skin. The stimulus aids the relief of pain in 3 ways: </p>
      <ol>
      	<li>By “blocking” the pain messages from reaching the brain via the nerves.</li>
      <li>By stimulating the release of the body’s naturally occurring pain-relieving 	endorphins.</li>
      <li>By providing a distraction while the pain is occurring.</li>
  </ol>
 <br/>
<p style="text-align:center"><image src="<?= base_url()?>assets/images/FreeMOM_Labour_Electrodes_position.jpg"></image></p>
 <br/>
<p>The electrodes are applied to the lower back (see picture above), when labour first commences. The expectant mother sets the <span class="brush-font">Free</span>eMOM TENS to a comfortable sensation level while pressing on the ‘Boost Button’. Once set, the expectant mother simply presses the ‘Boost Button’ and holds it down until the contraction ceases. An instructional DVD and written instructions are included with the <span class="brush-font">Free</span>MOM TENS unit. The video instructions are available on our website. We encourage you and your partner to attend a class on how to use the <span class="brush-font">Free</span>MOM TENS with a Physiotherapist – Women’s Health Specialist (APA) or Midwife – Prenatal Educator (CAPEA).</p>

<a name="toc-3"></a>

<h1>Features and Benefits</h1>
<ul class="bullet-list">
<li>The unit is small and easy to handle.</li>
<li>Non-invasive and drug free.</li>
<li>Initiates the action of the body’s own natural pain-relieving mechanisms.</li>
<li>Provides a background level of pain relief to which any other form of analgesia may be added if required.</li>
<li>May be used at home, or upon arrival at the hospital.</li>
<li>It can easily be interrupted or discontinued at any time during the labour. (it may need to be disconnected during foetal monitoring).</li>
<li>It is simple to use and a lanyard may be placed around the neck for freedom of movement.</li>
<li>“Set and forget” stimulation settings. No need to control the stimulation intensity during each contraction.</li>
<li>DVD instructions supplied with each kit. Video instructions are available on our website.</li>
<li>Used in hospitals throughout Australia and by mother’s world-wide.</li>
</ul>

<a name="toc-4"></a>
<h1><span class="brush-font">Free</span>MOM TENS Kit contains</h1>
<ul class="bullet-list">
<li><span class="brush-font">Free</span>MOM TENS Stimulator unit</li>
<li>Lanyard</li>
<li>Cable</li>
<li>2 x 9V Alkaline batteries.</li>
<li>3 x Clean Cote Skin wipes.</li>
<li>1 x Set of Labour Obstetric Electrodes (includes 1 Clean Cote Skin Wipe)</li>
<li>1 x Set of After birth Electrodes</li>
<li>DVD instructions</li>
<li>Written instructions / User Manual</li>
<li>Protective carry case</li>

</ul>
		</div>
		<!-- Main content end --> 

		<div class="col-md-4 col-md-offset-1 blue-toc">
			<ul class="toc-content">
				<li><a href="#toc-1"> What is the <span class="brush-font">Free</span>MOM TENS?</a></li>
				<li><a href="#toc-2"> How does it Work?</a></li>
				<li><a href="#toc-3"> Features and Benefits</a></li>
				<li><a href="#toc-4"> <span class="brush-font">Free</span>MOM TENS Kit contains</a></li> 
				 
			</ul>
		</div>
	</div>
	
	<div class="clear-footer"></div>