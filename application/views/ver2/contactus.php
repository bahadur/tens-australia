	
<div id="map-canvas" style="background-color: #CCC; width:100%;height:400px"></div>
    <div class="container">
   		<br/>
   		<h2>Our Contact Details	</h2>
   		<div class="row">
   			<div class="col-md-6"> 
   					<div class="row">
		   				<div class="col-md-6 ">
		   					<p><i class="fa fa-clock-o "></i> Office Hours:</p>
		   				</div>
		   				<div class="col-md-6">
		   					<p>10 a.m. – 5 p.m. Mon - Fri (Excluding Public Holidays)</p>
		   				</div>
   					</div>
   					<div class="row">
		   				<div class="col-md-6 ">
		   					<p><i class="fa fa-phone"></i> Telephone Number:</p>
		   				</div>
		   				<div class="col-md-6">
		   					<p>1300 913 129</p>
		   				</div>
   					</div>
   					<div class="row">
		   				<div class="col-md-6 ">
		   					<p><i class="fa fa-phone"></i> Facsimile Number:</p>
		   				</div>
		   				<div class="col-md-6">
		   					<p>1300 913 149</p>
		   				</div>
   					</div> 
   			</div>
   			<div class="col-md-6">

   					<div class="row">
		   				<div class="col-md-6 ">
		   					<p><i class="fa fa-envelope "></i> Email:</p>
		   				</div>
		   				<div class="col-md-6">
		   					<p>info@tensaustralia.com</p>
		   				</div>
   					</div>
   					<div class="row">
		   				<div class="col-md-6 ">
	   					<p><i class="fa fa-skype"></i> Skype:</p> 
		   				</div>
		   				<div class="col-md-6">
		   					<p><a id="skypeButton" href="skype:tensaustralia?call"><img src="<?=base_url()?>assets/images/skypeButton.png" style="height:40px"></img></a></p>
	   				
		   				</div>
   					</div> 
   			</div>
   		</div>
   		<h2>Online Contact Form</h2>
   		<label id="errorLabel" class="errorLabel" style="display:none">Some of required fields (*) need your input</label><br/>
   		<div class="row col-md-12">
   		<form class="form" method="post" action="" id="form-contactus"> 
   			<div class="row">
	   			<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Subject: <i class="field-required"></i></label> 
						<select class="form-control" name="subject" id="subject" >
							<option selected readonly disabled>All products</option>
							<option>COMPACT TENS</option>
							<option>FreeMOM TENS</option>
							<option>FreeLADY TENS</option>
							<option>In-TENS</option>
							<option>TENS Accessories</option>
						</select>  
					</div>

					<div class="form-group">
						<label class="control-label ">First name: <i class="field-required"></i></label>
						<div class="">
							<input type="text" class="form-control" name="first_name" id="first_name" />
						</div>
						<div class="col-md-1"></div>
					</div>

					<div class="form-group">
						<label class="control-label ">Last Name: <i class="field-required"></i></label>
						<div class="">
							<input type="text" class="form-control" name="last_name" id="last_name" />
						</div> 
					</div>

					<div class="form-group">
						<label class="control-label ">Email: <i class="field-required"></i></label>
						<div class="">
							<input type="email" class="form-control" name="email" id="email" />
						</div> 
					</div>

					<div class="form-group">
						<label class="control-label ">Hospital/Company Name:</label>
						<div class="">
							<input type="text" class="form-control" name="company_name" id="company_name" />
						</div>
					</div>

					<div class="form-group">
						<label class="control-label ">Your website address:</label>
						<div class="">
							<input type="url" class="form-control" name="website" id="website" />
						</div>
					</div>

					<div class="form-group">
						<label class="control-label ">Telephone No: <i class="field-required"></i></label>
						<div class="">
							<input type="tel" class="form-control" name="phone" id="phone" />
						</div> 
					</div>

	   			</div>
	   			<div class="col-md-6">

					<div class="form-group">
						<label class="control-label ">Facsimile No:</label>
						<div class="">
							<input type="tel" class="form-control" name="fax" id="fax" />
						</div>
					</div>

					<div class="form-group">
						<label class="control-label ">Street Address:</label>
						<div class="">
							<input type="text" class="form-control" name="street" id="street" />
						</div>
					</div>

					<div class="form-group">
						<label class="control-label ">Suburb:</label>
						<div class="">
							<input type="text" class="form-control" name="suburb" id="suburb" />
						</div>
					</div>

					<div class="form-group">
						<label class="control-label ">State: <i class="field-required"></i></label>
						<div class="">
						<select class="form-control" name="state" id="state">
							<option selected="selected" readonly disabled>*** SELECT STATE ***</option>
							<option>Australian Capital Territory</option>
							<option>New South Wales</option>
							<option>Victoria</option>
							<option>Queensland</option>
							<option>South Australia</option>
							<option>Western Australia</option>
							<option>Tasmania</option>
							<option>Northern Territory </option>
							<option>NOT LISTED</option>
						</select>
							
						</div> 
					</div>

					<div class="form-group">
						<label class="control-label ">Postcode:</label>
						<div class="">
							<input type="text" class="form-control" name="postcode" id="postcode" />
						</div>
					</div>

					<div class="form-group">
						<label class="control-label ">Province/Country: <i class="field-required"></i></label>
						<div class="">
						<select  class="form-control" name="country" id="country">
								<option selected="selected" readonly disabled>*** SELECT COUNTRY ***</option>
								<option value="Australia">Australia</option>
								<option value="New Zealand" >New Zealand</option>
								<option value="United States">United States</option> 
								<option value="Canada">Canada</option> 
								<option value="United Kingdom" >United Kingdom</option>
								<option value="Ireland" >Ireland</option>
								<option value="" >-------------------</option>
								<option value="Afghanistan">Afghanistan</option> 
								<option value="Albania">Albania</option> 
								<option value="Algeria">Algeria</option> 
								<option value="American Samoa">American Samoa</option> 
								<option value="Andorra">Andorra</option> 
								<option value="Angola">Angola</option> 
								<option value="Anguilla">Anguilla</option> 
								<option value="Antarctica">Antarctica</option> 
								<option value="Antigua and Barbuda">Antigua and Barbuda</option> 
								<option value="Argentina">Argentina</option> 
								<option value="Armenia">Armenia</option> 
								<option value="Aruba">Aruba</option> 
								<option value="Australia">Australia</option> 
								<option value="Austria">Austria</option> 
								<option value="Azerbaijan">Azerbaijan</option> 
								<option value="Bahamas">Bahamas</option> 
								<option value="Bahrain">Bahrain</option> 
								<option value="Bangladesh">Bangladesh</option> 
								<option value="Barbados">Barbados</option> 
								<option value="Belarus">Belarus</option> 
								<option value="Belgium">Belgium</option> 
								<option value="Belize">Belize</option> 
								<option value="Benin">Benin</option> 
								<option value="Bermuda">Bermuda</option> 
								<option value="Bhutan">Bhutan</option> 
								<option value="Bolivia">Bolivia</option> 
								<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option> 
								<option value="Botswana">Botswana</option> 
								<option value="Bouvet Island">Bouvet Island</option> 
								<option value="Brazil">Brazil</option> 
								<option value="British Indian Ocean Territory">British Indian Ocean Territory</option> 
								<option value="Brunei Darussalam">Brunei Darussalam</option> 
								<option value="Bulgaria">Bulgaria</option> 
								<option value="Burkina Faso">Burkina Faso</option> 
								<option value="Burundi">Burundi</option> 
								<option value="Cambodia">Cambodia</option> 
								<option value="Cameroon">Cameroon</option> 
								<option value="Canada">Canada</option> 
								<option value="Cape Verde">Cape Verde</option> 
								<option value="Cayman Islands">Cayman Islands</option> 
								<option value="Central African Republic">Central African Republic</option> 
								<option value="Chad">Chad</option> 
								<option value="Chile">Chile</option> 
								<option value="China">China</option> 
								<option value="Christmas Island">Christmas Island</option> 
								<option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option> 
								<option value="Colombia">Colombia</option> 
								<option value="Comoros">Comoros</option> 
								<option value="Congo">Congo</option> 
								<option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option> 
								<option value="Cook Islands">Cook Islands</option> 
								<option value="Costa Rica">Costa Rica</option> 
								<option value="Cote D'ivoire">Cote D'ivoire</option> 
								<option value="Croatia">Croatia</option> 
								<option value="Cuba">Cuba</option> 
								<option value="Cyprus">Cyprus</option> 
								<option value="Czech Republic">Czech Republic</option> 
								<option value="Denmark">Denmark</option> 
								<option value="Djibouti">Djibouti</option> 
								<option value="Dominica">Dominica</option> 
								<option value="Dominican Republic">Dominican Republic</option> 
								<option value="Ecuador">Ecuador</option> 
								<option value="Egypt">Egypt</option> 
								<option value="El Salvador">El Salvador</option> 
								<option value="Equatorial Guinea">Equatorial Guinea</option> 
								<option value="Eritrea">Eritrea</option> 
								<option value="Estonia">Estonia</option> 
								<option value="Ethiopia">Ethiopia</option> 
								<option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option> 
								<option value="Faroe Islands">Faroe Islands</option> 
								<option value="Fiji">Fiji</option> 
								<option value="Finland">Finland</option> 
								<option value="France">France</option> 
								<option value="French Guiana">French Guiana</option> 
								<option value="French Polynesia">French Polynesia</option> 
								<option value="French Southern Territories">French Southern Territories</option> 
								<option value="Gabon">Gabon</option> 
								<option value="Gambia">Gambia</option> 
								<option value="Georgia">Georgia</option> 
								<option value="Germany">Germany</option> 
								<option value="Ghana">Ghana</option> 
								<option value="Gibraltar">Gibraltar</option> 
								<option value="Greece">Greece</option> 
								<option value="Greenland">Greenland</option> 
								<option value="Grenada">Grenada</option> 
								<option value="Guadeloupe">Guadeloupe</option> 
								<option value="Guam">Guam</option> 
								<option value="Guatemala">Guatemala</option> 
								<option value="Guinea">Guinea</option> 
								<option value="Guinea-bissau">Guinea-bissau</option> 
								<option value="Guyana">Guyana</option> 
								<option value="Haiti">Haiti</option> 
								<option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option> 
								<option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option> 
								<option value="Honduras">Honduras</option> 
								<option value="Hong Kong">Hong Kong</option> 
								<option value="Hungary">Hungary</option> 
								<option value="Iceland">Iceland</option> 
								<option value="India">India</option> 
								<option value="Indonesia">Indonesia</option> 
								<option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option> 
								<option value="Iraq">Iraq</option> 
								<option value="Ireland">Ireland</option> 
								<option value="Israel">Israel</option> 
								<option value="Italy">Italy</option> 
								<option value="Jamaica">Jamaica</option> 
								<option value="Japan">Japan</option> 
								<option value="Jordan">Jordan</option> 
								<option value="Kazakhstan">Kazakhstan</option> 
								<option value="Kenya">Kenya</option> 
								<option value="Kiribati">Kiribati</option> 
								<option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option> 
								<option value="Korea, Republic of">Korea, Republic of</option> 
								<option value="Kuwait">Kuwait</option> 
								<option value="Kyrgyzstan">Kyrgyzstan</option> 
								<option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option> 
								<option value="Latvia">Latvia</option> 
								<option value="Lebanon">Lebanon</option> 
								<option value="Lesotho">Lesotho</option> 
								<option value="Liberia">Liberia</option> 
								<option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option> 
								<option value="Liechtenstein">Liechtenstein</option> 
								<option value="Lithuania">Lithuania</option> 
								<option value="Luxembourg">Luxembourg</option> 
								<option value="Macao">Macao</option> 
								<option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option> 
								<option value="Madagascar">Madagascar</option> 
								<option value="Malawi">Malawi</option> 
								<option value="Malaysia">Malaysia</option> 
								<option value="Maldives">Maldives</option> 
								<option value="Mali">Mali</option> 
								<option value="Malta">Malta</option> 
								<option value="Marshall Islands">Marshall Islands</option> 
								<option value="Martinique">Martinique</option> 
								<option value="Mauritania">Mauritania</option> 
								<option value="Mauritius">Mauritius</option> 
								<option value="Mayotte">Mayotte</option> 
								<option value="Mexico">Mexico</option> 
								<option value="Micronesia, Federated States of">Micronesia, Federated States of</option> 
								<option value="Moldova, Republic of">Moldova, Republic of</option> 
								<option value="Monaco">Monaco</option> 
								<option value="Mongolia">Mongolia</option> 
								<option value="Montserrat">Montserrat</option> 
								<option value="Morocco">Morocco</option> 
								<option value="Mozambique">Mozambique</option> 
								<option value="Myanmar">Myanmar</option> 
								<option value="Namibia">Namibia</option> 
								<option value="Nauru">Nauru</option> 
								<option value="Nepal">Nepal</option> 
								<option value="Netherlands">Netherlands</option> 
								<option value="Netherlands Antilles">Netherlands Antilles</option> 
								<option value="New Caledonia">New Caledonia</option> 
								<option value="New Zealand">New Zealand</option> 
								<option value="Nicaragua">Nicaragua</option> 
								<option value="Niger">Niger</option> 
								<option value="Nigeria">Nigeria</option> 
								<option value="Niue">Niue</option> 
								<option value="Norfolk Island">Norfolk Island</option> 
								<option value="Northern Mariana Islands">Northern Mariana Islands</option> 
								<option value="Norway">Norway</option> 
								<option value="Oman">Oman</option> 
								<option value="Pakistan">Pakistan</option> 
								<option value="Palau">Palau</option> 
								<option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option> 
								<option value="Panama">Panama</option> 
								<option value="Papua New Guinea">Papua New Guinea</option> 
								<option value="Paraguay">Paraguay</option> 
								<option value="Peru">Peru</option> 
								<option value="Philippines">Philippines</option> 
								<option value="Pitcairn">Pitcairn</option> 
								<option value="Poland">Poland</option> 
								<option value="Portugal">Portugal</option> 
								<option value="Puerto Rico">Puerto Rico</option> 
								<option value="Qatar">Qatar</option> 
								<option value="Reunion">Reunion</option> 
								<option value="Romania">Romania</option> 
								<option value="Russian Federation">Russian Federation</option> 
								<option value="Rwanda">Rwanda</option> 
								<option value="Saint Helena">Saint Helena</option> 
								<option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option> 
								<option value="Saint Lucia">Saint Lucia</option> 
								<option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option> 
								<option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option> 
								<option value="Samoa">Samoa</option> 
								<option value="San Marino">San Marino</option> 
								<option value="Sao Tome and Principe">Sao Tome and Principe</option> 
								<option value="Saudi Arabia">Saudi Arabia</option> 
								<option value="Senegal">Senegal</option> 
								<option value="Serbia and Montenegro">Serbia and Montenegro</option> 
								<option value="Seychelles">Seychelles</option> 
								<option value="Sierra Leone">Sierra Leone</option> 
								<option value="Singapore">Singapore</option> 
								<option value="Slovakia">Slovakia</option> 
								<option value="Slovenia">Slovenia</option> 
								<option value="Solomon Islands">Solomon Islands</option> 
								<option value="Somalia">Somalia</option> 
								<option value="South Africa">South Africa</option> 
								<option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option> 
								<option value="Spain">Spain</option> 
								<option value="Sri Lanka">Sri Lanka</option> 
								<option value="Sudan">Sudan</option> 
								<option value="Suriname">Suriname</option> 
								<option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option> 
								<option value="Swaziland">Swaziland</option> 
								<option value="Sweden">Sweden</option> 
								<option value="Switzerland">Switzerland</option> 
								<option value="Syrian Arab Republic">Syrian Arab Republic</option> 
								<option value="Taiwan, Province of China">Taiwan, Province of China</option> 
								<option value="Tajikistan">Tajikistan</option> 
								<option value="Tanzania, United Republic of">Tanzania, United Republic of</option> 
								<option value="Thailand">Thailand</option> 
								<option value="Timor-leste">Timor-leste</option> 
								<option value="Togo">Togo</option> 
								<option value="Tokelau">Tokelau</option> 
								<option value="Tonga">Tonga</option> 
								<option value="Trinidad and Tobago">Trinidad and Tobago</option> 
								<option value="Tunisia">Tunisia</option> 
								<option value="Turkey">Turkey</option> 
								<option value="Turkmenistan">Turkmenistan</option> 
								<option value="Turks and Caicos Islands">Turks and Caicos Islands</option> 
								<option value="Tuvalu">Tuvalu</option> 
								<option value="Uganda">Uganda</option> 
								<option value="Ukraine">Ukraine</option> 
								<option value="United Arab Emirates">United Arab Emirates</option> 
								<option value="United Kingdom">United Kingdom</option> 
								<option value="United States">United States</option> 
								<option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option> 
								<option value="Uruguay">Uruguay</option> 
								<option value="Uzbekistan">Uzbekistan</option> 
								<option value="Vanuatu">Vanuatu</option> 
								<option value="Venezuela">Venezuela</option> 
								<option value="Viet Nam">Viet Nam</option> 
								<option value="Virgin Islands, British">Virgin Islands, British</option> 
								<option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option> 
								<option value="Wallis and Futuna">Wallis and Futuna</option> 
								<option value="Western Sahara">Western Sahara</option> 
								<option value="Yemen">Yemen</option> 
								<option value="Zambia">Zambia</option> 
								<option value="Zimbabwe">Zimbabwe</option>
						</select>

							
						</div> 
					</div>

					<div class="form-group">
						<label class="control-label ">Question/Comments:</label>
						<div class="">
							<textarea  class="form-control" name="comments" id="comments" ></textarea>
						</div>
					</div>
	   			</div>
	   		</div>
 
   			<p class=""> 
					<div class="g-recaptcha" data-sitekey="6LdmLAcTAAAAAEthKA6xlRzA9TP8F9V0FcIupl6h"></div>
					<div class="text-danger" id="captcha-error-msg" style="display:none">
						<p style="color:red !important"  class="error-captcha"><span class="glyphicon glyphicon-remove "></span> Please fill up the captcha.</p>
					</div> 
				<input type="submit" name="send" id="send" class="btn btn-primary" data-style="expand-right" value="Send" style="font-size:24px"></input>
				 
				<div class="alert alert-success hidden" id="msg" role="alert"></div>
			</p> 
				</form>
			</div>
	</div>
			<br/><br/><br/><br/><br/>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script type="text/javascript" src="http://www.skypeassets.com/i/scom/js/skype-uri.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script src='<?=base_url()?>assets/js/jquery.min.js'></script>
    <script src='<?=base_url()?>assets/js/jquery.validate.min.js'></script> 
    <script type="text/javascript">
    function initialize() {
	    var mapCanvas = document.getElementById('map-canvas');
	    var mapOptions = {
	      center: new google.maps.LatLng(-37.935361, 145.038193),
	      zoom: 14,
	      mapTypeId: google.maps.MapTypeId.ROADMAP
	    }
	    var map = new google.maps.Map(mapCanvas,mapOptions); 
  		var myLatlng = new google.maps.LatLng(-37.935361, 145.038193);
	    var marker = new google.maps.Marker({
		  position: myLatlng,
		  map: map,
      	  title: 'Hello World!'
		}); 
		var contentString = '<div id="content">'+
	      '<h1 id="firstHeading" class="firstHeading">Our Location</h1>'+
	      '<div id="bodyContent"> 7 / 2 Central Ave MOORABBIN VIC 3189 AUSTRALIA <br/><em>Car parks available at the rear of the building</em><br/>' + 
	      '</div>'+
	      '</div>';

	  var infowindow = new google.maps.InfoWindow({
	      content: contentString
	  });


      infowindow.open(map,marker);  
      //Skype.ui({"name": "dropdown","element": "skypeButton","participants": ["dao-duy-anh"],"imageSize": 32}); 
	  google.maps.event.addListener(marker, 'click', function() {
	    infowindow.open(map,marker);
	  });

	}

	google.maps.event.addDomListener(window, 'load', initialize);

  	$("#form-contactus").validate({
  		
        rules:{ 
        	subject: { required: true }, 
			first_name: { required: true }, 
			last_name: { required: true }, 
			email: { required: true,email: true }, 
			phone: { required: true }, 
			state: { required: true }, 
			country: { required: true },  
        },
 
        errorPlacement: function (error, element) { 
            var elem = jQuery(element);
            error.addClass('errorLabel');
            element.addClass('errorInput');
            $("#errorLabel").show();
            //error.insertAfter(element);
        },
                        
        success: function (label, element) {  
            var elem = jQuery(element);
            $("#errorLabel").hide();
            elem.removeClass('errorInput');
        },

        submitHandler: function(form) {
        	var googleResponse = jQuery('#g-recaptcha-response').val();
		    if (!googleResponse) {
		        $("#captcha-error-msg").show();
		        return false;
		    } else {
		        $("#captcha-error-msg").hide();
		        return true;
		    } 
        }
	});

    </script>
 
 
<?php 
/*
$this->layouts->add_include('assets/js/jquery.validate.min.js');
$this->layouts->add_include('http://www.google.com/recaptcha/api.js', false);

$this->layouts->add_include('assets/css/ladda-themeless.min.css');
$this->layouts->add_include('assets/js/spin.min.js');
$this->layouts->add_include('assets/js/ladda.min.js');



$js = <<< CUSTOM_JS
  
  
  	$("#send").click(function(){
  		var l = Ladda.create(this);
  		l.start();
  		$.ajax({
	    		url: base_url + "main/contactUs",
	    		type: "POST",
	    		async: false,
	    		data: $("#form-contactus").serialize(),
	    		success: function(resp) {
	      			
	      			$("#msg").html(resp.msg);
	      			$("#msg").removeClass("hidden");
	      			$("#msg").addClass("show");
	      			l.stop();
	      			
	    		}
  			});
		return false;
  	});



  	$("#form-contactus").validate({
  		onkeyup: false,
	    onfocusout: false,
	    onclick: false,
        rules:{
            
        	subject: { required: true }, 
			first_name: { required: true }, 
			last_name: { required: true }, 
			email: { required: true,email: true }, 
			phone: { required: true }, 
			state: { required: true }, 
			country: { required: true }
            
            
            
        },
        	
        errorPlacement: function (error, element) { 
            var elem = jQuery(element);
            error.addClass('errorLabel');
            error.insertAfter(element);
        },
                        
        success: function (label, element) {  
            var elem = jQuery(element);
            elem.siblings('label.error').remove();


        },

        submitHandler: function(form) {
             
        	return true;
            
             
            
        }
	});
	
	function validateForm(){ 
        return false;

    }

    

CUSTOM_JS;


$this->layouts->add_js_block($js);*/
?>
