
<section id="main-content-area">
	<div class="container"> 
<h2>Terms and Policies</h2>

<h3>Shipping</h3>

<p>All items shipped are sent registered and tracked requiring a signature upon delivery.</p>
<p>
In Australia, we deliver using Australia Post’s, Express Post Registered service. Deliveries are next business day and this service is available in major metro and regional population centres and requires a signature on delivery.
</p>
<p>
International deliveries are sent using Australia Post’s, Express Post International service. Deliveries for items up to 20kg to over 190 countries. Estimated 3-10 days business days to metropolitan areas of major cities. 
Shipping costs are influenced by the size and weight of the product and your location and weight limits may vary between countries.
</p>

<h3>
Currency and Pricing
</h3>
<p>
All transactions are processed in Australian dollars. All pricing and items that include Goods and Services Tax (GST) will be identified on the hire and purchase pages and tax invoices.
</p>

<h3>Copyright </h3>

<p>
No right, title or interest in any downloaded materials is transferred to you as a result of any such downloading or copying.
</p>

<h3>
Terms and Conditions
</h3>

<p><a target="_blank" href="<?= base_url()?>assets/pdfs/FreeMOM TENS Labour Pain Management Terms and Conditions Hire pdf.pdf">

FreeMOM TENS Hire Terms and Conditions (link to a pdf)</a>
</p>

<p><a target="_blank" href="<?= base_url()?>assets/pdfs/COMPACT TENS Chronic and Acute Pain Management Terms and Conditions Hire pdf.pdf">

COMPACT TENS Hire Terms and Conditions (link to a pdf)</a>
</p>

<p><a target="_blank" href="<?= base_url()?>assets/pdfs/FreeMOM TENS Labour Pain Management Terms and Conditions Purchase pdf.pdf">
FreeMOM TENS Purchase Terms and Conditions (link to a pdf)</a>
</p>
<p><a target="_blank" href="<?= base_url()?>assets/pdfs/COMPACT TENS Chronic and Acute Pain Management Terms and Conditions Purchase pdf.pdf">
COMPACT TENS Purchase Terms and Conditions (link to a pdf)</a>
</p>

<h3>Return Policy</h3>
	<h4>Returning hired equipment</h4>

<p>Please refer to our Hire Terms and Conditions.</p>
<h4>Returning purchased equipment</h4>

<p>You are responsible for returning product(s) to your supplier. If you are sending the product(s) to your supplier it is your responsibility to take reasonable measures to protect the product(s) with appropriate packaging to prevent damage during shipping.</p>
<h3>Refund Policy</h3>
	
<p>When hiring or purchasing equipment please refer to the terms and conditions supplied with the equipment.</p>
<p>We do not refund electrodes that are returned used or opened.</p>
<p>We do not refund product that have been misused in anyway. </p>

<h3>Security Policy</h3>

<p>When hiring or purchasing from TENS Australia, card details are transmitted through a secure server using the latest 128-bit SSL (Secure Sockets Layer) encryption technology. Card data is not hosted by TENS Australia after processing.</p>
<h3>Privacy Policy</h3>

<p>TENS”R”US Pty Ltd (ACN 601 058 995)</p>
<p>Tens R Us is committed to keeping your details private. Any information we collect in relation to you is kept strictly secured and confidential. The primary reason we use this information is to identify and fulfil your order or booking and provide you with assistance.</p>
<p>This privacy policy applies to information collected through our website and off-line.</p>


	</div>
</section>
<br/><br/><br/><br/>
