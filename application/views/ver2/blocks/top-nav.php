
<body>

    
    <header class="top-header" id="main-header">
      
      <div class="container">
        <div class="row">
          
          <!-- HEADER TITLE -->
          <div class="col-md-5 col-sm-5">
            <h2 class="header-title brush">non invasive pain management</h2>
          </div>
          
          <!-- LOGO -->
          <div class="col-md-3 col-sm-3">
            <div class="logo"><a href="<?= base_url();?>"><img src="<?= base_url() ?>assets/images/logo.png" alt="tesns australia"></a></div>
          </div>
          
          <!-- NAVIGATION -->
          <div class="col-md-4 col-sm-4">

            <nav class="navbar navbar-default main-navigation">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">MENU</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand hidden-md hidden-lg hidden-sm " href="#">MENU</a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="<?= base_url()?>main/about">About Us</a></li>
                            <li><a href="<?= base_url()?>main/contactus">Contact Us</a></li>
                        </ul>
                    </div>
                    <!-- //NAVEBAR COLLAPSE -->
                </div>
                <!-- //CONTAINER FLUID -->
            </nav>

          </div>
          <!--//END NAVIGATION -->

        </div>
        <!--//END ROW -->
      </div>
      <!--//END CONTAINER -->


    </header>
    <!--//END TOP-HEADER -->
    