  
    <footer class="footer">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h5 style="color:white">Follow Us</h5>
            <ul class="social">
              <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
              <li><a href="#"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            </ul>
            
            <p><a href="#">Health professionals login</a></p>
            <p class="copy-right-text">Copyright © 2009 TENS “R” US PTY. LTD. All rights reserved.</p>
          </div>
          <div class="col-md-3 col-md-offset-3">
            <br/>
            <p><a href="<?=base_url();?>indications">Indications</a></p>
            <p><a href="<?=base_url();?>termspolicies">Terms & Policies</a></p>
            <p><a href="<?=base_url();?>relatedlinks">Related Links</a></p> 
          </div>
        </div>
      </div>
    </footer>
    <!--//END FOOTER -->



</body>

</html>