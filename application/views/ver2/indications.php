
<section id="main-content-area">
	<div class="container">
<h1>Indications</h1>
<h2>Warning</h2>

<ul class="nav">
	<li>• TENS is not a medication, is non-invasive and has no harmful side effects when used
appropriately.</li>
<li>• TENS is used for pain management and muscle stimulation. TENS does not cure the disease process causing the pain. Pain may serve as a protective mechanism, and 
	can be an important clinical symptom.</li>
<li>• TENS may interfere with the use of Electronic medical devices. Eg pacemakers. (Please refer to your clinician for additional information)</li>
</ul>

<h2>Precaution</h2>

<ul class="nav">
<li>• The TENS device must be OFF before connecting or removing cables or electrodes attached to your skin.</li>
<li>• TENS should not be used to alleviate an undiagnosed pain syndrome, until your clinician has established its cause.</li>
</ul>

<h2>Contra-indications</h2>

<ul class="nav">
<li>• TENS devices can affect the operation of cardiac pacemakers if placed incorrectly on your body.</li>
<li>• The FREELADY should not be used for ovulation pain (mid-cycle pain)</li>
<li>• The FREELADY should not be used while fertility problems are being evaluated, diagnosed or treated.</li>
<li>• The FREELADY should not be used during pregnancy or delivery and preferably not during breast feeding.</li>
<li>• If you have any concerns regarding the use of TENS for your situation, consult your clinician.</li>
</ul>

<h2>Skin Irritation</h2>

<ul class="nav">
<li>• Prolonged stimulation and placement of electrodes on the same area of skin may cause skin irritation.</li>
<li>• Consult your clinician prior to commencement of treatment.</li>
</ul>
	</div>
</section>
<br/><br/><br/><br/>
