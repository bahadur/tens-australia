
    <section id="main-content-area">
    	<div class="container">

        <div class="row">

          <div class="col-md-6 no-margin-padding-right single-product">
              <!--<img class="img-responsive" src="<?= base_url(); ?>assets/images/freemomTens.png" alt="single-product">
                
              <div class="single-product-content">
                <h2><span class="brush">Free</span>MOM Tens</h2>
                 <p>Labour Pain Management</p>
              </div>-->
              <div class="grid">
                <figure class="effect-julia" onclick="window.location='<?= base_url()?>products/freeMomTens'"/>

                <img src="<?= base_url(); ?>assets/images/freemomTens.png" alt="img21" >

                  <figcaption>

                  <h2><span class="brush">Free</span>MOM TENS</h2>

                    <div>
                    <p>Labour Pain Management</p> <br>
                    <p><a href="<?= base_url(); ?>products/freeMomTens" class="btn btn-success">LEARN MORE</a></p>
                    </div>


                  </figcaption>     
                </figure>
              </div>   
          </div>

          <div class="col-md-6 no-margin-padding-left single-product" onclick="window.location='<?= base_url()?>products/compactTens'">
            <!--<img class="img-responsive" src="<?= base_url(); ?>assets/images/compactTens.png" alt="single-product">
              <div class="single-product-content">
                 <h2>Compact Tens</h2>
                 <p>Chronic/Acute Pain Management</p>
              </div>-->
              <div class="grid">
                <figure class="effect-julia" onclick="window.location='<?= base_url()?>products/compactTens'"/>

                <img src="<?= base_url(); ?>assets/images/compactTens.png" alt="img22" >

                  <figcaption>

                  <h2>Compact TENS</h2>

                    <div>
                    <p>Chronic/Acute Pain Management</p> <br>
                    <p><a href="<?= base_url(); ?>products/compactTens" class="btn btn-success">LEARN MORE</a></p>
                    </div>


                  </figcaption>     
                </figure>
              </div>   
          </div>

        </div>
        
        <div class="row">

          <div class="col-md-6 no-margin-padding-right single-product">
            <!--<img class="img-responsive" src="<?= base_url(); ?>assets/images/freeladyTens.png" alt="single-product">
              <div class="single-product-content">
                 <h2>FREELADY Tens</h2>
                 <p>Menstrual/Endometriosis Pain Management</p>
              </div> -->
              <div class="grid">
                <figure class="effect-julia" onclick="window.location='<?= base_url()?>products/freeladyTens'"/>

                <img src="<?= base_url(); ?>assets/images/freeladyTens.png" alt="img23" >

                  <figcaption>

                  <h2>FREELADY TENS</h2>

                    <div>
                    <p>Menstrual/Endometriosis Pain Management</p> <br>
                    <p><a href="<?= base_url(); ?>products/freeladyTens" class="btn btn-success">LEARN MORE</a></p>
                    </div>


                  </figcaption>     
                </figure>
              </div>   

        
          </div>

          <div class="col-md-6 no-margin-padding-left single-product">
            <!--<img class="img-responsive" src="<?= base_url(); ?>assets/images/accessories.png" alt="single-product">
              <div class="single-product-content">
                 <h2>Accessories</h2>
                 <p>Electrodes, Cables, Wipes, etc..</p>
              </div> -->


              <div class="grid">
                <figure class="effect-julia" onclick="window.location='<?= base_url()?>products/accessories'"/>

                <img src="<?= base_url(); ?>assets/images/accessories.png" alt="img24" >

                  <figcaption>

                  <h2>ACCESSORIES</h2>

                    <div>
                    <p>Electrodes, Cables, Wipes, etc..</p> <br>
                    <p><a href="<?= base_url(); ?>products/accessories" class="btn btn-success">LEARN MORE</a></p>
                    </div>


                  </figcaption>     
                </figure>
              </div>   
          </div>
        
        </div>
 <br/><br/><br/>
      </div>
  </section>