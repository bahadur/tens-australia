 
	
	<div class="container">
		<br/>
		<br/>
		<br/>
		<br/>
		<div class="row">
			<div class="col-md-2">
				<div class="well well-sm">
					<img src="<?php echo base_url()?>assets/images/RayKrieser.jpg" alt="Mr. Ray Krieser" class="img-thumbnail"  />
					<small>Mr. Ray Krieser <br />
						MANAGING DIRECTOR</small>
				</div>
			</div>
			<div class="col-md-10">
				<h3>Company Profile</h3>
				
				<p>TENS “R” US Pty Ltd (formerly KRIESLEX Pty Ltd) is an Australian owned company and has been supplying Transcutaneous Electrical Nerve Stimulation (TENS) units and accessories since 1980. </p>
				<p>A chance meeting with a remarkable anaesthetist, Dr. Florella Magora, and a team of scientific researchers at the Hadassah Hospital in Jerusalem, inspired our obsession to develop a pain management system that is non-invasive, drug free and without side effects. </p>
				<p>So started the exciting co-operation between Krieslex, Ginosar Electronics and Titan Electronics, who jointly developed a range of portable effective TENS units. </p>
				<p>Ray Krieser, an Electronic Engineer, is one of the Directors of TENS “R” US. He has been involved in the development of TENS units, electrodes and a variety of TENS accessories for the last 30 years. In 1987, Ray came up with the concept of using TENS for pain relief in Labour.</p>
				<p>TENS “R” US strives to deliver a high standard of service to our clients. This includes the supply of small, modern reliable TENS units, education in the safe use of TENS and a full range of accessories.
Our professional support, warranty and service are world standard.</p>
				<p>All our stimulators meet the strict guidelines of the Association for the Advancement of Medical Instrumentation (AAMI), the Australian Therapeutic Goods Administration (TGA) and the EC (European Community). </p>
				<p>From our very beginning, TENS, and its clinical applications have always been an integral part of TENS “R” US. We conduct educational seminars in the use of TENS and stimulator selection, to find the appropriate TENS unit to meet your requirements. These sessions are given unconditionally and free of charge. We conduct regular workshops for midwifery staff and prenatal educators. We value our reputation and pursue ongoing developments in TENS pain management systems. </p>
			</div>
		</div>
		<br/>
		<br/>
		<br/>
		<br/>

	</div> 