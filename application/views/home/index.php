  <!--//END TOP-HEADER -->
    
    <section id="main-content-area">
      <div class="container">

        <div class="row">

          <div class="col-md-6 no-margin-padding-right single-product">
              <img class="img-responsive" src="images/ten-1.png" alt="single-product">
                
              <div class="single-product-content">
                <h2><span class="brush">Free</span>MOM Tens</h2>
                 <p>Labour Pain Management</p>
              </div>
          </div>

          <div class="col-md-6 no-margin-padding-left single-product">
            <img class="img-responsive" src="images/ten-2.png" alt="single-product">
              <div class="single-product-content">
                 <h2>Compact Tens</h2>
                 <p>Chronic/Acute Pain Management</p>
              </div>
          </div>

        </div>
        
        <div class="row">

          <div class="col-md-6 no-margin-padding-right single-product">
            <img class="img-responsive" src="images/ten-3.png" alt="single-product">
              <div class="single-product-content">
                 <h2>FREELADY Tens</h2>
                 <p>Menstrual/Endometriosis Pain Management</p>
              </div>            
          </div>

          <div class="col-md-6 no-margin-padding-left single-product">
            <img class="img-responsive" src="images/ten-4.png" alt="single-product">
              <div class="single-product-content">
                 <h2>Accessories</h2>
                 <p>Electrodes, Cables, Wipes, etc..</p>
              </div>
          </div>
        
        </div>

        <div class="main-bottom-content">
          Non Invasive Pain Management
        </div>

      </div>
    </section>
    <!--//END MAIN-CONTENTNT-AREA -->