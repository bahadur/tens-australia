<section class="product-information">
	
	
	<div class="container">

		<div class="row">
			
			<div class="col-md-12">
				<h2>Servics</h2>
				
				
				<ul>

					<li>
						<h4>TENS Consultation regarding musculo-skeletal pain management</h4>
						<p>TENS has been shown to be an effective, non-invasive technique for managing musculo-skeletal pain.</p>
					</li>

					<li>
						<h4>TENS Education, Service and Support</h4>
						<p>TEN ‘R’ US provide support and comprehensive educational programs to both private clients and hospital staff.</p>
					</li>

					<li>
						<h4>TENS Technical support</h4>
						<p>TEN ‘R’ US undertake quality testing of all our TENS products to ensure safe and effective use.</p>
					</li>

					<li>
						<h4>TENS Stimulator Maintenance and Repair services</h4>
						<p>TENS stimulators should be tested and serviced on a regular basis to ensure safe and effective use.</p>
					</li>
				</ul>

			</div>
		</div>

	</div>
</section>