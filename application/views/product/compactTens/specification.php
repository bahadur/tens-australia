<?php $this->load->view("product/compactTens/header") ?>


    <section class="product-information"> 
      <div class="container">
        <h2>Specification</h2>
      </div>
    </section>


    <section class="product-information">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            

            <table class="table table-bordered">
              
              <tr>
                <th>Size:</th>
                <td>(L) 80 mm (W) 60 mm (D) 28 mm</td>
              </tr>

              <tr>
                <th>Weight:</th>
                <td>90 gm/3.0 oz. (Excl. Battery)</td>
              </tr>


              <tr>
                <th>Waveform:</th>
                <td>Symmetrical rectangular biphasic, with zero net DC current</td>
              </tr>


              <tr>
                <th>Channels:</th>
                <td>Two, electrically isolated</td>
              </tr>


              <tr>
                <th>Current Output:</th>
                <td>Each channel 0-60mA into 1K ohms</td>
              </tr>


              <tr>
                <th>Pulse Width:</th>
                <td>30 - 300 µ/sec - adjustable</td>
              </tr>


              <tr>
                <th>Pulse Rate:</th>
                <td>1-200 pps - adjustable</td>
              </tr>

              <tr>
                <th>Modes:</th>
                <td></td>
              </tr>


              <tr>
                <th>Modulation type:</th>
                <td>(Patented) Width (Patented)</td>
              </tr>


              <tr>
                <th>Width:</th>
                <td>50% of width setting +/- 15%</td>
              </tr>


              <tr>
                <th>Rate:</th>
                <td>20% of width setting +/- 15%</td>
              </tr>


              <tr>
                <th>Modulation Rate:</th>
                <td>8 – 10 Hz.</td>
              </tr>


              <tr>
                <th>Burst:</th>
                <td>250 µ/sec on, 250 µ/sec off</td>
              </tr>


              <tr>
                <th>Battery test:</th>
                <td>Green LED indicates battery OK
              Red LED indicates low battery (5.5 volts)</td>
              </tr>

              <tr>
                <th>Battery source:</th>
                <td>9V Alkaline battery</td>
              </tr>


              <tr>
                <th>Battery type:</th>
                <td>9V Alkaline battery</td>
              </tr>


              <tr>
                <th>Battery life:</th>
                <td>40 – 50Hrs. (Using a new Alkaline Battery).
At 30mA into 2K, 2 channels, continuous pulse width 
100 µ/sec Battery life approx. 60 hours</td>
              </tr>

             
             
            </table>

            
            
            
            
            
            
          
          </div>
        </div>
      </div>
    </section>

?>