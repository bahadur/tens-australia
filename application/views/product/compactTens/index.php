<?php 
$this->layouts->add_include('assets/css/normalize.css');
$this->layouts->add_include('assets/css/flexslider.css');
?>

<?php $this->load->view("product/compactTens/header") ?>



    <section class="product-information"> 
      <div class="container">
        <h2>Product Information</h2>
        <div class="row produxt-content">
          <p>Chronic Pain Management</p>
          <div class="col-md-4">
            <img src="<?php echo base_url()?>assets/images/single.png" alt="">
          </div>
          <div class="col-md-8">
 
          
          
          
            <h3><a href="#whatIsCompactTens">What is a COMPACT TENS?</a></h3>
            
            <h3><a href="#howDoesItwork">How does it Work?</a></h3>

            <h3><a href="#usingYourCompactTens">Using your COMPACT TENS</a></h3>

            <h3><a href="#compactTensBenefits">COMPACT TENS offers the following superior features</a></h3>
            
            <h3><a href="#testimonials">Testimonials</a></h3>

            <h3><a href="#references">References</a></h3>
            
          </div>
        </div>
      </div>
    </section>



    <section class="testimonial-section">
    <a name="testimonials"></a><h3 class="text-center">Testimonials</h3>
      <hr class="testimonial-line">
    <div class="container">
 
        <div class="flexslider">
          <ul class="slides">
            <li>
              <p>The COMPACT TENS Units - What a relief for my back pain 

                To whom it may concern,
                I have been suffering from MS for a number of years and am now wheelchair bound.
                I developed severe back pain and was lucky enough to see a consultant that recommended that I trial the COMPACT TENS for pain relief as he wanted to minimize my consumption of pain relieving medication. I have now been using the COMPACT TENS daily for over 2 years and am just grateful that this non invasive pain relief device was available. Its effectiveness gives me relief and allows me to live a more productive life without the daily need to focus on my back pain. 
                Yours sincerely,</p>
              
              <div class="person">
                <h6>N. N. (Melbourne)</h6>
              </div>

            </li>
            <li>
              <p>Dear Mr. Krieslex

              I have been trying for some years now to manage my increasing levels of pain in my legs and in my back. My medication intake has caused my physician to look for alternatives as I have reacted badly to medication clashes that simply cannot be tolerated. As a result, I was sent to a pain clinic where the COMPACT TENS was trialed and I regarded myself among the fortunate clients who discovered this wonderful device. I have managed to reduce my pain relieving medication to a minimum while using the TENS stimulator for the relief of muscle wastage – another problem that was prevalent due to my inability to keep physically active. The TENS is used daily for both pain relief and “application to exercise” my leg and arm muscles. I have found that using the COMPACT TENS daily has improved the “quality” of my daily routine and I am able to join in many more activities than I was able to do previously. Thank you so much for your assistance from an educational point and for your telephonic support that is greatly appreciated.
              </p>
              
              <div class="person">
                <h6>Sam H. (Melbourne)</h6>
              </div>

            </li>
            
            
          </ul>
        </div>
      </div>
    </section>


    <section class="product-information">
      <div class="container">
        <div class="row">
          <div class="col-md-12">

          <a name="whatIsCompactTens"></a><h3>What is a COMPACT TENS?</h3>
          <p>The COMPACT TENS is a dual channel, non-invasive Transcutaneous Electrical Nerve Stimulator (TENS) a medical device, designed specifically for the purpose of assisting in the treatment and management of chronic and acute pain.</p>
          <br>

          <a name="howDoesItwork"></a><h3>How does it Work?</h3>
          <p>The COMPACT TENS sends small, electronic signals via electrodes which are placed of the skin. The electrical signals stimulate the nerves under the skin, providing relief from pain in three main ways:
              1.  Preventing pain signals reaching the brain thus reducing the sensation of pain in the affected area.
              2.  Stimulating the brain to produce and release the body's naturally occurring pain-relieving endorphins.
              3.  Creating sensations which act as powerful distractions from the pain.</p>
          <br>

          <a name="usingYourCompactTens"></a><h3>Using your COMPACT TENS</h3>
          <p>Detailed advice on the application of the COMPACT TENS is available from Health Professionals and Pain Clinics. The COMPACT TENS includes information to assist health professionals and educational information for patients. Regular review of client progress is recommended following the initial assessment and treatment session with the health professional.

            Correct electrode placement is essential for effective treatment. Electrode placement protocol instructions are included with each COMPACT TENS kit supplied.
          </p>
          <br>

          <a name="compactTensBenefits"></a><h3>COMPACT TENS benefits:</h3>
          <p>
          Non-invasive.
          • Can be applied with other pain management treatments and modalities.
          • Can be applied continuously for 16 hours in a 24 hour period.
          • Compact, portable and simple to use.
          • Enhances the patients feeling of 'well being’ by encouraging the release of endorphins.
          • May give the patient a sense of control over the pain.
          • May give the patient increased autonomy as it can be applied anytime with minimal or no disruption to their normal daily routine.
          • No interference with prescribed medications.
          • Choice of 3 available waveforms: Continuous, Pulse Burst and Modulated.
          • Clinically evaluated with published reports available in Medical and other journals.
          • Cost effective option for patient’s pain management.
          • Meets AAMI guidelines (Association for the Advancement of Medical Instrumentation).
          </p>
          <br>

          <a name="references"></a><h3>references</h3>
                <ol type="1">
                  <li>1.  Melzack R and Wall PD. “Pain Mechanisms”, “A New Theory” Science. (150). 971-979. 1965</li>
                  <li>2.  Sjolund B.H. and Ericksson E.B. “Endorphins and Analgesia Produced by Peripheral Conditioning Stimulation”. Advances in Pain Research and Therapy Vol.3 5870592, 1979.</li>
                  <li>3.  Augstinsson et al – Pain 4. 59065, 1977.</li>
                  <li>4.  Thurin, E. Meehan, P.F., Gilbert B.S. “Treatment of Pain by Transcutaneous Electrical Nerve Stimulation in General Practice.” Medical Journal of Australia; 1 (2) 70-71, 1980.</li>
                  <li>5.  Schuster, G.D. and Marsden B., “Treatment of Pain with TENS”, Fifth International Symposium on Electrico-therapeutic Sleep and Electro-anaesthesia, Graz Austria: 221-225, 1978.</li>
                <ol>
              <p>*Pain Management & Research Centre, Melbourne Australia.</p>

          </div>
        </div>
      </div>
    </section>

<?php 
$this->layouts->add_include('assets/js/jquery.flexslider-min.js');
$js = <<< CUSTOM_JS
  
  $('.flexslider').flexslider({
          animation: "flase",            
          easing: "swing",               
          direction: "horizontal",       
          reverse: false,                
          animationLoop: true,           
          smoothHeight: false,           
          startAt: 0,                    
          slideshow: true,               
          slideshowSpeed: 7000,          
          animationSpeed: 600,           
          initDelay: 0,                  
          randomize: false,              
          controlNav: true,              
          directionNav: true,            
          prevText: "&nbsp;",           
          nextText: "&nbsp;",            
        });
    

CUSTOM_JS;


$this->layouts->add_js_block($js);
?>