<?php $this->load->view("product/compactTens/header") ?>
<section class="instruction-page-full">
      
      <div class="container">
      
        <h2 class="text-color">Compact Tens - Instructions</h2>

        <div class="row">
    			<div id="ibacordotcom-content">
            <div class="col-md-8">
              <div class="ibacordotcom-unit ibacordotcom-vid-kenca">
                <div class="ibacordotcom_vid_play"></div>
              </div>

              <div class="instruction-pdf">
                <p>Click the link below to download instructions in pdf format. </p>

                <div class="instruction-pdf-link">
                  <a href="#"><img src="<?php echo base_url()?>assets/images/pdf.png" alt="pdf download link"></a>
                </div>
              </div>

            </div>        


            <div class="col-md-4">
              <div class = "ibacordotcom-unit ibacordotcom-vid-katuhu">
                <div class="ibacordotcom_youtube_channels"></div>
              </div>
            </div>

    			</div>
        </div>

      </div>    
  
    </section>
<?php  
$this->layouts->add_include('assets/js/jquery.fitvids.js');
$this->layouts->add_include('assets/js/ycp.js');


 $js = <<< CUSTOM_JS
  
    
  $(".container").fitVids();
    

CUSTOM_JS;


$this->layouts->add_js_block($js);
?>