<?php $this->load->view("product/compactTens/header") ?>

<section class="testimonial-full">
      <div class="container">

        <h2 class="text-color">FreeMOM Tens - Customer Testimonials</h2>

          <div class="testimonial-wrap">
            <div class="row">
              <div class="col-md-6">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/2trs2GMG3NY" frameborder="0" allowfullscreen></iframe>
              </div>

              <div class="col-md-6">
                <h3 class="text-color"><i class="fa fa-quote-left fa-2x"></i>Thank you Tens. I was using it last month and it has been a saviour. I will be usning it again.<i class="fa fa-quote-right fa-2x"></i></h3>


                <p class="testimonail-content-comment">“ Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ”</p>

                <div class="testimonial-person-full">
                  <h5>Victoriya Ambrosia</h5>
                  <p>Melbourne, Australia</p>
                </div>
              </div>
            </div>
          </div>          

          <hr class="testimonial-full-line">

          <div class="testimonial-wrap">
            <div class="row">

              <div class="col-md-6">
                <h3 class="text-color"><i class="fa fa-quote-left fa-2x"></i>Thank you Tens. I was using it last month and it has been a saviour. I will be usning it again.<i class="fa fa-quote-right fa-2x"></i></h3>


                <p class="testimonail-content-comment">“ Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ”</p>

                <div class="testimonial-person-full">
                  <h5>Victoriya Ambrosia</h5>
                  <p>Melbourne, Australia</p>
                </div>
              </div>

              <div class="col-md-6">
                <iframe src="https://player.vimeo.com/video/125235739?portrait=0&badge=0" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
              </div>

            </div>
          </div>

          <hr class="testimonial-full-line">

          <div class="testimonial-wrap">
            <div class="row">
              <div class="col-md-6">

                <iframe src="https://player.vimeo.com/video/125235739?portrait=0&badge=0" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

              </div>

              <div class="col-md-6">
                <h3 class="text-color"><i class="fa fa-quote-left fa-2x"></i>Thank you Tens. I was using it last month and it has been a saviour. I will be usning it again.<i class="fa fa-quote-right fa-2x"></i></h3>


                <p class="testimonail-content-comment">“ Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ”</p>

                <div class="testimonial-person-full">
                  <h5>Victoriya Ambrosia</h5>
                  <p>Melbourne, Australia</p>
                </div>
              </div>
            </div>
          </div>                      


      </div>  
    </section>