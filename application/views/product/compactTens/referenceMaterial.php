<?php $this->load->view("product/compactTens/header") ?>


    <section class="product-information"> 
      <div class="container">
        <h2>Reference Material</h2>
      </div>
    </section>


    <section class="product-information">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            

            <table class="table table-bordered">
              <tr>
                <th>CONTENT</th>
                <th>PDF</th>
              </tr>
              <tr>
                <td>
                  Testing the efficacy of the modulation technique.
                </td>
                <td>
                  <i class="fa fa-file-pdf-o"></i>
                </td>
              </tr>
              <tr>
                <td>
                  The use of TENS in General Practice - Medical Journal of Australia
                </td>
                <td>
                  <i class="fa fa-file-pdf-o"></i>
                </td>
              </tr>
            </table>

            
            <p><small>You are welcome to call or email our office if you have any questions or require additional information.</small></p>      
            
            
            
            
          
          </div>
        </div>
      </div>
    </section>

?>