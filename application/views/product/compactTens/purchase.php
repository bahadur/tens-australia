
<section class="product-information"> 
      <div class="container">
      	<h2>Purchase Compact Tens<h2>
			<p><a href="<?php echo base_url()?>products/compactTens/orderform" class="btn slider-button" style="color:white; width:30%">Purchase online</a>
			<a href="#" class="btn slider-button" style="color:white; width:30%">Print a form</a></p>
			<h4>How much is the FreeMOM to buy?</h4>
			<table>
				<thead>
					<tr>
						<th></th>
						<th>AUD</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>FreeMOM TENS Kit (Incl. GST) for Labour Pain Management<br/>
							(Product Code T38053)<br/>
							Supplied in a case with 2 x 9V Alkaline Batteries, Cable, ‘After-birth’ Electrodes, Written instructions and an instructional DVD.<br/><br/></td>
						<td>$260.00</td>
					</tr>
					<tr>
						<td>Labour Electrodes <br/>
(Product Code E30480)<br/><br/></td>
						<td>$35.00</td>
					</tr>
					<tr>
						<td><strong>Total</strong></td>
						<td><strong>$295.00</strong></td>
					</tr>
					<tr>
						<td><strong>If applicable, postage or shipping</strong></td>
						<td><strong></strong></td>
					</tr>
					<tr>
						<td>Postage - Australia post delivery to your address</td>
						<td>$15.00</td>
					</tr>
					<tr>
						<td><strong>If applicable, OPTIONS</strong></td>
						<td><strong></strong></td>
					</tr>
					<tr>
						<td>Back Pain or Pelvic Instability – Butterfly Electrodes with Clean-cote Skin Wipes and battery</td>
						<td>$20.00</td>
					</tr>
					<tr>
						<td>Neck and Shoulder Pain – Neck and Shoulder Electrodes with Clean-cote Skin Wipes and battery</td>
						<td>$20.00</td>
					</tr>
				</tbody>
			</table>
			<br/><br/>
			<h4>Do Private Health Insurance Funds refund the purchase of a FreeMOM TENS kit?</h4>
			<p>If you are a member of a private health insurance provider please consider contacting your fund as you may be entitled to make a claim.</p>
			<p>Example:</p>
			<p>Bupa Australia Provider Number: EP08949 - HBA Recognised Provider - Mutual Community Recognised Provider - MBF Recognised Provider - MBF Alliances Recognised Provider</p>

			<h4>How do I purchase the FreeMOM TENS?</h4>
			<p>Purchase ONLINE</p>
<br/>
			<p>OR</p>
<br/>
<p>Call our office during business hours and we complete the FreeMOM TENS purchase with you over the telephone.</p>
<p>Tel: 1300 913 129</p>
<br/>
<p>OR</p>
<br/>
<p>Print the customer order form and product price list pdf from our website</p>
<p>Fax the completed Order form to our office.</p>
<p>Fax: 1300 913 149</p>
<br/>
<p>OR</p>
<br/>
<p>Mail the completed order form to our office</p>
<p>7 / 2 Central Avenue, MOORABBIN VICTORIA 3189 Australia</p>
<br/>
<p>OR</p>
<br/>
<p>Scan and email the completed Order form to our office</p>
<p>info@tensaustralia.com</p>
<br/>
<h4>How do I pay for the FreeMOM TENS purchase?</h4>
<br/>
<ul>
	<li>•	Credit card (Master or Visa )</li>	
<li>•	Cheque</li>	
<li>•	Money Order</li>
</ul>
<br/>
<h4>Can I purchase additional electrodes in the future?</h4>
<br/>
<p>•	Yes, repeat the above steps to purchase additional accessories.</p>
<br/>
<h4>How long will my order take to arrive once I pay for my order?</h4> 
	<br/>
<table>
	<tr>
		<td>Adelaide metro</td>
		<td>Next business day</td>
	</tr>

	<tr>
		<td>Brisbane metro</td>
		<td>Two business days</td>
	</tr>

	<tr>
		<td>Canberra metro</td>
		<td>Next business day</td>
	</tr>

	<tr>
		<td>Darwin metro</td>
		<td>Two business days</td>
	</tr>

	<tr>
		<td>Hobart metro</td>
		<td>Two business days</td>
	</tr>

	<tr>
		<td>Melbourne metro   <span style="color:white">.........</span></td>
		<td>Next business day</td>
	</tr>

	<tr>
		<td>Perth metro</td>
		<td>Two business days</td>
	</tr>

	<tr>
		<td>Sydney metro</td>
		<td>Next business day</td>
	</tr>
	<tr>
		<td>International</td>
		<td>Available by air express</td>
	</tr>
	</table>
	<br/>
	<br/>
<br/>
		</div>
</div>