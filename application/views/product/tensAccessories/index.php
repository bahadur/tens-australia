<section class="product-information">
	
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3>Tens Accessories</h3>

				<table class="table table-bordered table-condensed table-striped">
					<tr>
						<th>Product Description</th>
						<th>Price</th>
						<th>Image</th>

					</tr>

					<tr>
						<td>
							<h4>CLEAN-COTE Skin Wipes</h4>
							<p>Special TENS skin preparation single use – contains Alcohol and Silicon.
								Cleans and disinfects the application area, and increases electrode conductivity. Shields the the skin from adhesives and possible irritation. Extends the life of the electrodes.
							</p>
							<p>(Product Code A30220)</p>
						</td>
						<td>
							<strong>Box of 50</strong>
							<p>AUD $15.00</p>
						</td>
						<td>
							<img src="<?php echo base_url() ?>assets/images/CLEAN-COTE_WIPE.jpg" alt="img21"/>
						</td>
					</tr>
					<tr>
						<td>
							<h4>GEL – “TAC GEL”</h4>
							<p>
								Electrically conductive adhesive for TENS & EMS – 50gm tube
							</p>
							<p>(Product Code A38018)</p>
						</td>
						<td>
							<strong>Ea</strong>
							<p>AUD $12.00</p>
						</td>
						<td>
							<img src="<?php echo base_url() ?>assets/images/TAC_GEL.JPG" alt="img21"/>
						</td>
					</tr>

					<tr>
						<td>
							<h4>AFTER TENS Care Cream</h4>
							<p>
								For sensitive skin, used after TENS treatment - 60ml tube
							</p>
							<p>(Product Code A38020)</p>
						</td>
						<td>
							<strong>Ea</strong>
							<p>AUD $12.00</p>
						</td>
						<td>
							<img src="<?php echo base_url() ?>assets/images/AFTER-TENS_GEL.JPG" alt="img21"/>
						</td>
					</tr>

					<tr>
						<td>
							<h4>PRE-TAC Conductive TENS Skin Preparation</h4>
							<p>
								Conductive Skin Preparation for sensitive skin, prevents electrodes from slipping during conditions of perspiration and humidity - 15ml bottle
							</p>
							<p>(Product Code A30231)</p>
						</td>
						<td>
							<strong>Ea</strong>
							<p>AUD $12.00</p>
						</td>
						<td>
							<img src="<?php echo base_url() ?>assets/images/PRETAC.JPG" alt="img21"/>
						</td>
					</tr>

					<tr>
						<td>
							<h4>CABLE</h4>
							<p>
								Available for COMPACT TENS(80cm), FreeMOM TENS(120cm) and FREELADY (50cm)
							</p>
							<p>(Product Code A38021)</p>
						</td>
						<td>
							<strong>Ea</strong>
							<p>AUD $15.00</p>
						</td>
						<td>
							<img src="<?php echo base_url() ?>assets/images/TENS_Cable.JPG" alt="img21"/>
						</td>
					</tr>

					<tr>
						<td>
							<h4>BATTERY 9V ALKALINE</h4>
							<p>
								(suit FreeMOM TENS, COMPACT TENS and IN-TENS
								Stimulators for approx. 30 – 40 hrs)
							</p>
							<p>(Product Code A38054)</p>
						</td>
						<td>
							<strong>Ea</strong>
							<p>AUD $5.00</p>
						</td>
						<td>
							<img src="<?php echo base_url() ?>assets/images/BATTERY.JPG" alt="img21"/>
						</td>
					</tr>

					<tr>
						<td>
							<h4>BATTERY 9V LITHIUM</h4>
							<p>
								(suit COMPACT TENS for approx. 100+ hrs)
							</p>
							<p>(Product Code A38055)</p>
						</td>
						<td>
							<strong>Ea</strong>
							<p>AUD $19.50</p>
						</td>
						<td>
							<img src="<?php echo base_url() ?>assets/images/BATTERY2.JPG" alt="img21"/>
						</td>
					</tr>

					<tr>
						<td>
							<h4>9V RECHARGEABLE BATTERIES PACK</h4>
							<p>
								(suit COMPACT TENS and IN-TENS)
								2 x 780 mAh Lithium li-ion Rechargeable batteries and 1 x Recharger
							</p>
							<p>(Product Code A38058)</p>
						</td>
						<td>
							<strong>Ea</strong>
							<p>AUD $40.00</p>
						</td>
						<td>
							<img src="<?php echo base_url() ?>assets/images/BATTERY_NiMH.JPG" alt="img21"/>
						</td>
					</tr>

					<tr>
						<td>
							<h4>ELECTRODES - Standard Soft Rubber</h4>
							<p>
								46mm x 35mm (Pin Connection)
							</p>
							<p>(Product Code E38005)</p>
						</td>
						<td>
							<strong>Set of 2</strong>
							<p>AUD $8.00</p>
						</td>
						<td>
							<img src="<?php echo base_url() ?>assets/images/RUBBER_ELECTRODES_SMALL.JPG" alt="img21"/>
						</td>
					</tr>

					<tr>
						<td>
							<h4>ELECTRODES - Large Soft Rubber</h4>
							<p>
								148mm x 50mm (Pin Connection)
							</p>
							<p>(Product Code E38007)</p>
						</td>
						<td>
							<strong>Set of 2</strong>
							<p>AUD $10.00</p>
						</td>
						<td>
							<img src="<?php echo base_url() ?>assets/images/RUBBER_ELECTRODES_LARGE.JPG" alt="img21"/>
						</td>
					</tr>

					<tr>
						<td>
							<h4>ELECTRODES - ROUND</h4>
							<p>
								<strong>32mm Diameter - Long Life Reusable for 3 weeks </strong>
								Supplied in a resealable bag
							</p>
							<p>(Product Code E32811)</p>
							<p>(Ideal for Arthritic, Chronic Pain at specific locations to be outlined by your clinician).</p>
						</td>
						<td>
							<strong>4 per pack</strong>
							<p>AUD $15.00</p>
						</td>
						<td>
							<img src="<?php echo base_url() ?>assets/images/ELECTRODE_ROUND.JPG" alt="img21"/>
						</td>
					</tr>

					<tr>
						<td>
							<h4>ELECTRODES – SMALL BUTTERFLY</h4>
							<p>
								<strong>30mm X 50mm - Long Life Reusable for 3 Weeks.</strong>
								Supplied in a resealable bag
							</p>
							<p>(Product Code E32833)</p>
							<p>(Ideal for Arthritic, Chronic Pain at specific locations to be outlined by your clinician).</p>
						</td>
						<td>
							<strong>4 per pack</strong>
							<p>AUD $15.00</p>
						</td>
						<td>
							<img src="<?php echo base_url() ?>assets/images/ELECTRODES_SMALL_BUTTERFLY.JPG" alt="img21"/>
						</td>
					</tr>

					<tr>
						<td>
							<h4>ELECTRODES - SQUARE</h4>
							<p>
								<strong>50mm X 50mm - Long Life Reusable for 3 Weeks </strong>
								Supplied in a resealable bag
							</p>
							<p>(Product Code E32835)</p>
							<p>(Ideal for Arthritic, Skeletal Pains at specific locations to be outlined by your clinician)</p>
						</td>
						<td>
							<strong>4 per pack</strong>
							<p>AUD $15.00</p>
						</td>
						<td>
							<img src="<?php echo base_url() ?>assets/images/ELECTRODES_SQUARE.JPG" alt="img21"/>
						</td>
					</tr>

					<tr>
						<td>
							<h4>ELECTRODES – RECTANGULAR</h4>
							<p>
								<strong>90mm x 50mm - Long Life Reusable for 3 Weeks</strong>
								Supplied in a resealable bag
							</p>
							<p>(Product Code E32837)</p>
							<p>(Ideal for Arthritic, Chronic Pain and specific locations to be outlined by your clinician)</p>
						</td>
						<td>
							<strong>4 per pack</strong>
							<p>AUD $15.00</p>
						</td>
						<td>
							<img src="<?php echo base_url() ?>assets/images/ELECTRODE_RECTANGULAR.JPG" alt="img21"/>
						</td>
					</tr>

					<tr>
						<td>
							<h4>ELECTRODES – OVAL </h4>
							<p>
								<strong>34mm x 63mm - Long Life Reusable for 3 Weeks</strong>
								Supplied in a resealable bag
							</p>
							<p>(Product Code E36160)</p>
							<p>(Ideal for Chronic Pain, Interferential applications as outlined by your clinician)</p>
						</td>
						<td>
							<strong>4 per pack</strong>
							<p>AUD $15.00</p>
						</td>
						<td>
							<img src="<?php echo base_url() ?>assets/images/ELECTRODE_OVAL.JPG" alt="img21"/>
						</td>
					</tr>

					<tr>
						<td>
							<h4>ELECTRODES – LARGE BUTTERFLY </h4>
							<p>
								<strong>105mm x 150mm - Long Life Reusable for 3 Weeks</strong>
								Supplied in a resealable bag
							</p>
							<p>(Product Code E34041)</p>
							<p>(Ideal for Low Back Pain, Pelvic Instability applications and Back Pain associated with Pregnancy- use from 37 weeks onwards (if required) or as specified by your clinician)</p>
						</td>
						<td>
							<strong>One pair</strong>
							<p>AUD $15.00</p>
						</td>
						<td>
							<img src="<?php echo base_url() ?>assets/images/Butterfly_Large_Electrodes_K34041R.JPG" alt="img21"/>
						</td>
					</tr>

					<tr>
						<td>
							<h4>ELECTRODES LABOUR/OBSTETRICS  </h4>
							<p>
								<strong>225mm x 40mm –Reusable for one birth experience.</strong>
								Supplied in a sealed satchel
							</p>
							<p>(Product Code E30480)</p>
							<p>(Ideal for Labour Pain Management - once opened, the electrodes can be reused up to 30 applications within a 1 month period)</p>
						</td>
						<td>
							<p><strong>One pair</strong>
							<p>AUD $35.00</p>

						</td>
						<td>
							<img src="<?php echo base_url() ?>assets/images/FreeMOM_LABOUR_ELECTRODES.JPG" alt="img21"/>
						</td>
					</tr>
				</table>	
			</div>
		</div>
	</div>
</section>