<?php 
$this->layouts->add_include('assets/css/normalize.css');
$this->layouts->add_include('assets/css/flexslider.css');
?>

<?php $this->load->view("product/freeMomTens/header") ?>


    <section class="product-information"> 
      <div class="container">
        <h2>Product Information</h2>
        <div class="row produxt-content">
          <p>"We owe it to ourselves to be educated and armed, so that we are more able to enjoy what is such a special day."</p>
          <div class="col-md-4">
            <img src="<?php echo base_url()?>assets/images/single.png" alt="">

          </div>
          <div class="col-md-8">

            <h3><a href="#whatIsFreeMOMTENS">What is FreeMOM TENS?</a></h3>
            <h3><a href="#doPrivate">Do Private Health Insurance Funds refund the hire of a FreeMOM TENS kit?</a></h3>
            <h3><a href="#howMuchCost">How much is the FreeMOM to hire?</a></h3>
            <h3><a href="#howDoesItWork">How does it Work?</a></h3>
            <h3><a href="#importantInformation">Important Information</a></h3>
            <h3><a href="#isFreeMOMEffective">Is FreeMOM effective?</a></h3>
            <h3><a href="#freeMOMeasyToUse">Is FreeMOM easy to use?</a></h3>
            <h3><a href="#isTheHireFreeMOMExpensive">Is the hire of the FreeMOM expensive?</a></h3>
            <h3><a href="#freeMOMFeatures">FreeMOM offers the following superior features</a></h3>
            <h3><a href="#benefitsOfFreeMOM">Benefits of the FreeMOM</a></h3>
            <h3><a href="#testimonials">Testimonials</a></h3>
            <h3><a href="#references">References</a></h3>

          </div>
        </div>
      </div>
    </section>



    <section class="testimonial-section">
    <a name="testimonials"></a><h3 class="text-center">Testimonials</h3>
    <hr class="testimonial-line">
    <div class="container">
 
        <div class="flexslider">
          <ul class="slides">
            <li>
              <p>“You may be entitled to make a claim with your Health Insurance Provider.

                Bupa Australia Provider Number: EP08949
                HBA Recognised Provider
                Mutual Community Recognised Provider
                MBF Recognised Provider
                MBF Alliances Recognised Provider
                NIB Provider No: 10112588”</p>
              
              <div class="person">
                <h6>Do Private Health Insurance Funds refund the hire of a FreeMOM TENS kit?</h6>
              </div>

            </li>
            <li>
              <p>“ Voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet ”</p>
              
              <div class="person">
                <h6>Quoted Person</h6>
              </div>

            </li>
            <li>
              <p>“ Cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.”</p>

              <div class="person">
                <h6>Quoted Person</h6>
              </div>

            </li>
            <li>
              <p>“ Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.”</p>

              <div class="person">
                <h6>Quoted Person</h6>
              </div>
              
            </li>
          </ul>
        </div>
      </div>
    </section>

    <section class="product-information">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            

            <a name="whatIsFreeMOMTENS"></a><h3>What is FreeMOM TENS?</h3>
            <p>FreeMOM is a Transcutaneous Electrical Nerve Stimulation (T.E.N.S) device which has been specifically designed to assist in the relief of pain during labour, after-birth pain, assist with sleeping after the delivery and lower back pain.</p>      
            
            <br>  

            <a name="doPrivate"></a><h3>Do Private Health Insurance Funds refund the hire of a FreeMOM TENS kit?</h3>
            <p>You may be entitled to make a claim with your Health Insurance Provider.</p>
            <p>Bupa Australia Provider Number: EP08949</p>
            <br>
              <ul>
                <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HBA Recognised Provider</li>
                <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mutual Community Recognised Provider</li>
                <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MBF Recognised Provider</li>
                <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MBF Alliances Recognised Provider</li>
              </ul>
            <br>
            <p>NIB Provider No: 10112588</p>

            <br>
            
            <a name="howMuchCost"></a><h3>How much is the FreeMOM to hire?</h3>
            <br>

            <table width="100%">
              <tr>
                <th></th>
                <th>AUD</th>
              </tr>
              <tr>
                <td>FreeMOM TENS Hire (Incl. GST) - for the term of your confinement</td>
                <td>$50.00</td>
              </tr>
              <tr>
                <td>Labour Electrodes - refundable if not opened, used or damaged</td>
                <td>$35.00</td>
              </tr>
              <tr>
                <td>Deposit - refundable on the return of the FreeMOM TENS kit</td>
                <td>$40.00</td>
              </tr>
              <tr>
                <th>Sub Total</th>
                <th>$125.00</th>
              </tr>
            </table>

            <br>

            <a name="howDoesItWork"></a><h3>How does it Work?</h3>
            <p>FreeMOM sends small, electrical signals into the body via electrodes which are placed onto the skin. The stimulus aids the relief of pain in 3 ways:
            <br>
              <ul>
                <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.  By blocking the pain messages from reaching the brain via the nerves.</li>
                <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.  By stimulating the release of the body’s naturally occurring pain-relieving endorphins.</li>
                <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3.  By providing a distraction whilst the pain is occurring.</li>
              </ul>
            </p>


            <br>

            
            <a name="importantInformation"></a><h3>Read Important Imformation</h3>
            <p>Warning: It is recommended that patients with heart and/or circulation problems, cardio-vascular problems or implanted cardiac pacemakers seek medical advice of their practitioner prior to using the FreeMOM.</p>


            <br>

            <a name="isFreeMOMEffective"></a><h3>Is FreeMOM effective?</h3>
            <p>Women who received TENS pre-natal education prior to the use of the FreeMOM TENS for their labour, rated it as 92% effective for pain relief with 34% of all users requiring nothing else but the FreeMOM TENS as their only source of pain relief during labour.</p>


            <br>

            <a name="freeMOMeasyToUse"></a><h3>Is FreeMOM easy to use?</h3>
            <p>Very!<br>
The electrodes are applied to the lower back (see picture below), when labour first commences. The expectant mother sets the FreeMOM TENS to a comfortable sensation level while pressing on the ‘Booster Button’. Once set, at every contraction the expectant mother simply presses the ‘Booster Button’ and holds it down until the contraction ceases. A full instructional DVD is included with the FreeMOM TENS unit. We can also arrange an appointment with one of our qualified health professionals for a more detailed demonstration.</p>


            <br>

            <a name="isTheHireFreeMOMExpensive"></a><h3>Is the hire of the FreeMOM expensive?</h3>
            <p>FreeMOM rental is cheaper than the charge for using an epidural during birth. In addition, most health funds give a rebate for the hire and /or purchase of the FreeMOM TENS units. (depending on your health fund and level of cover).</p>

            <br>

            <a name="freeMOMFeatures"></a><h3>FreeMOM offers the following superior features:</h3>
            <ul>
              <li>  FreeMOM is the smallest, easiest to handle unit. FreeMOM is very simple to use and may be placed around the neck for freedom of movement.</li>
              <li>“Set and Forget” stimulation settings. No need to control the stimulation intensity during each contraction.</li>
              <li>Two long electrodes cover the spinal nerves to the uterus and cervix resulting in greater area coverage and less wires to get in the way or tangled.</li>
              <li>High efficacy rate (effective in 92% of natural births*).</li>
              <li>Menu driven instructional DVD with each unit.</li>
              <li>Training on the use of FreeMOM by qualified health care professionals if required.</li>
              <li>Used in hospitals throughout Australia and by mothers world-wide.</li>
              <li>All FreeMOM units are less than one year old to reduce risk of failure.</li>
            </ul>

            <br>

            <a name="benefitsOfFreeMOM"></a><h3>Benefits of the FreeMOM:</h3>
              <ul>
                <li>FreeMOM is simple and drug free</li>
                <li>FreeMOM initiates the action of the body’s own natural pain –relieving mechanisms.</li>
                <li>FreeMOM is used to provide a background level of pain relief to which any other form of analgesia may be added if required.</li>
                <li>FreeMOM may be used whilst still at home, or from the time you arrive at the hospital.</li>
                <li>FreeMOM may shorten the 1st stage of labour, but otherwise does not alter the natural course of labour.</li>
                <li>FreeMOM allows the mother to be fully mobile and “in control” during her labour, unlike other methods of pain relief.</li>
                <li>FreeMOM can be easily interrupted or discontinued at any time during the labour. (It may need to be disconnected during foetal monitoring).</li>
                <li>Benefits of greater comfort during early stages of labour are carried right throughout the birth process.</li>
                <li>FreeMOM can also be used for after-birth pain, restless leg syndrome and other pain e.g. lower back pain and shoulder pain.</li>

              </ul>

              <br>

              <a name="references"></a><h3>references</h3>
                <ol type="1">
                  <li>1. Metzack Rand Wall PD. “Pain Mechanisms”, “A New Theory” Science. (150). 971-979. 1965</li>
                  <li>2. B. Kaplan, D.Rubinerson, J. Pardo, U.R.Krieser*, A. Neri “Transcutaneous electrical nerve stimulation (TENS) as a pain-relief device in obstetrics and gynecology”. Clinical and Experimental Obstetrics and Gynecology. Volume XXIV. No. 3, March, 1997.</li>
                  <li>3. B. Kaplan, D. Rabinerson, L. Lurie, J. Bar, U.R. Krieser*, A. Neri “Transcutaneous electrical nerve stimulation (TENS) for adjuvant pain relief during labor and delivery”. International Journal of Gynecology & Obstetrics, Volume 60, No3, March 1998.</li>
                  <li>4. Sjolund B.H. and Ericksson E.B. “Endorphins and Analgesia Produced by Peripheral Conditioning Stimulation”. Advances in Pain Research and Therapy Vol.3 5870592, 1979.</li>
                  <li>5. Bundsen et al – Acta Obstetric Gyne Scan 60 1981.</li>
                  <li>6. Augstinsson et al – Pain 4. 59065, 1977.</li>
                  <li>7. Evron S. Postoperative Analgesia by Percutaneous Electrical Nerve Stimulation in Gynacology and Obstetrics. Europe. J. Gyne. 12 (1981). 305-313</li>
                  <li>8. Kaplan Boris, Rabinerson David, Peled Yoav, Hiirsch Michael, Pardo Joseph, Neri Alexander “T.E.N.S. As A Pain Relief Method During Deliver An Overlooked Modality”. Department of Obestetrics & Gynaecology, Rabin Medical Centre, Israel.</li>
                </ol>
              <p>*Pain Management & Research Centre, Melbourne Australia.</p>




          
          </div>
        </div>
      </div>
    </section>

<?php 
$this->layouts->add_include('assets/js/jquery.flexslider-min.js');
$js = <<< CUSTOM_JS
  
  $('.flexslider').flexslider({
          animation: "flase",            
          easing: "swing",               
          direction: "horizontal",       
          reverse: false,                
          animationLoop: true,           
          smoothHeight: false,           
          startAt: 0,                    
          slideshow: true,               
          slideshowSpeed: 7000,          
          animationSpeed: 600,           
          initDelay: 0,                  
          randomize: false,              
          controlNav: true,              
          directionNav: true,            
          prevText: "&nbsp;",           
          nextText: "&nbsp;",            
        });
    

CUSTOM_JS;


$this->layouts->add_js_block($js);
?>