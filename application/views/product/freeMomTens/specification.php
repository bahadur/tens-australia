<?php $this->load->view("product/freeMomTens/header") ?>


    <section class="product-information"> 
      <div class="container">
        <h2>Specification</h2>
      </div>
    </section>


    <section class="product-information">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            

            <table class="table table-bordered">
              
              <tr>
                <th>Size:</th>
                <td>(L) 13 cm (W) 3 cm (D) 2.3 cm</td>
              </tr>

              <tr>
                <th>Weight:</th>
                <td>100 gm. (incl. Battery)</td>
              </tr>


              <tr>
                <th>Waveform:</th>
                <td>Symmetrical Rectangular Biphasic</td>
              </tr>


              <tr>
                <th>Channels:</th>
                <td>One (1)</td>
              </tr>


              <tr>
                <th>Current Output:</th>
                <td>60mA Peak into 1K ohm load</td>
              </tr>


              <tr>
                <th>Pulse Width:</th>
                <td>50 to 550 MicroSec.</td>
              </tr>


              <tr>
                <th>Pulse Rate:</th>
                <td>80 Hz.</td>
              </tr>


              <tr>
                <th>Modulation:</th>
                <td>3 to 10 Hz. (FM)</td>
              </tr>


              <tr>
                <th>Green LED:</th>
                <td>Flashing when “Boost Mode is OFF”</td>
              </tr>


              <tr>
                <th>Green LED:</th>
                <td>Stays ON when “Boost Mode is ON”</td>
              </tr>


              <tr>
                <th>RED LED:</th>
                <td>ON when Battery requires changing</td>
              </tr>


              <tr>
                <th>Battery type:</th>
                <td>9V Alkaline</td>
              </tr>


              <tr>
                <th>Battery Life:</th>
                <td>(Approx.) 30 Hours</td>
              </tr>


             
             
            </table>

            
            
            
            
            
            
          
          </div>
        </div>
      </div>
    </section>

?>