
<section class="product-information"> 
      <div class="container">
      	<h2>Hire Compact Tens<h2>
			<p><a href="<?php echo base_url()?>products/FreeMOMTENS/orderform" class="btn slider-button" style="color:white; width:30%">Purchase online</a>
			<a href="#" class="btn slider-button" style="color:white; width:30%">Print a form</a></p>
			<h4>How do I hire the COMPACT TENS??</h4><br/>
			 <p>Print and complete the hire application form. Fax, mail or email the completed form to our office.
</p>
			<br/><br/>

			<h4>How do I pay for the COMPACT TENS hire?</h4>
			<ul>
				<li>
				•	Credit card (MasterCard or Visa )
				</li>
				<li>
				•	Cheque
				</li>
				<li>
				•	Money Order
				</li>
			</ul>
			<h4>How long can I hire the COMPACT TENS kit?</h4>
			<p>•	You hire the kit for 1 month. After that trial period you may wish to purchase the COMPACT TENS outright.</p> 
			<br/>
			<h4>When do I need to return the COMPACT TENS kit?</h4>
			<br/>
			<ul>
				<li>
•	Purchase an Australia Post Prepaid Pre-addressed satchel when you order your COMPACT TENS.</li>
<li>•	Visit your local Post Office and return the kit in a bubble bag to your local COMPACT TENS Supplier office.</li>
<li>•	Visit your local FreeMOM TENS Supplier office.</li>
</ul>

<br/>
			<h4>How do I get my COMPACT TENS Hire deposit refunded?</h4>
			<br/>
<p>•	When the COMPACT TENS kit is returned, please include your contact details and we will refund your deposit as soon as possible.</p>
<br/><br/><br/>
		</div>
</div>