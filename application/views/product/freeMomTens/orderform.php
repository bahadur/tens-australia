<section class="order-full">
      <div class="container">
        <h2 class="text-color">Hire FreeMom Tens</h2>

          <form id="rootwizard-2" method="post" action="" class="form-wizard validate">

            <div class="steps-progress">
              <div class="progress-indicator"></div>
            </div>
            
            <ul>
              <li class="active">
                <a href="#tab2-1" data-toggle="tab"><span>1</span>PERSONAL INFORMATION</a>
              </li>
              <li>
                <a href="#tab2-2" data-toggle="tab"><span>2</span>BILLING</a>
              </li>
              <li>
                <a href="#tab2-3" data-toggle="tab"><span>3</span>SHIPPING</a>
              </li>
              <li>
                <a href="#tab2-4" data-toggle="tab"><span>3</span>PRODUCTS DETAILS</a>
              </li>
              <li>
                <a href="#tab2-5" data-toggle="tab"><span>4</span>PAYMENT INFORMATION</a>
              </li> 
            </ul>
            
            <div class="tab-content">
              <div class="tab-pane active" id="tab2-1">

                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label for="first_name" class="control-label">First Name</label>
                              <input class="form-control" name="first_name" id="first_name" data-validate="required" placeholder="Please enter full name">
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="form-group">
                              <label for="surname" class="control-label">Surname</label>
                              <input class="form-control" name="surname" id="surname" data-validate="required" placeholder="Please enter surname">
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label for="telephone" class="control-label">Telephone No</label>
                              <input type="tel" class="form-control" name="telephone" id="telephone" data-rule-number="true"  data-validate="required" placeholder="Telephone No">
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="form-group">
                              <label for="mobile" class="control-label">Mobile no</label>
                              <input type="text" class="form-control" name="mobile" id="mobile" data-rule-number="true"  data-validate="required" placeholder="Mobile no">
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label for="email" class="control-label">Email</label>
                              <input type="email" class="form-control" name="email" id="email" data-validate="required" data-rule-email="true" data-rule-regex="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}" placeholder="Email">
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="form-group">
                              <label for="hospital" class="control-label">Maternity Hospital</label>
                              <input type="text" class="form-control" name="hospital" id="hospital" data-validate="required" placeholder="Maternity Hospital">
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label for="hospital1" class="control-label">Private Health Insurance provider name (If applicable)</label>
                              <input type="text" class="form-control" name="hospital1" id="hospital1" data-validate="required" placeholder="Please enter Private Health Insurance provider name">
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="form-group">
                              <label for="hospital2" class="control-label">Private Health Insurance provider name (If applicable)</label>
                              <input type="text" class="form-control" name="hospital2" id="hospital2" data-validate="required" placeholder="Please enter Private Health Insurance provider name">
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label for="educator" class="control-label">Your pre-natal Educator</label>
                              <input type="text" class="form-control" name="educator" id="educator" data-validate="required" placeholder="Your pre-natal Educator">
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="form-group">
                              <label for="pharmacy" class="control-label">Pharmacy</label>
                              <input type="text" class="form-control" name="pharmacy" id="pharmacy" data-validate="required" placeholder="Pharmacy">
                          </div>
                      </div>
                  </div>

                <div class="form-group">
<!--
                  <div class="col-md-6">
                    <label for="address" class="control-label">Address</label>
                    <input type="text" class="form-control" id="address" placeholder="Please enter address">
                  </div>

                  <div class="col-md-6">
                    <label for="suburb" class="control-label">Suburb</label>
                    <input type="text" class="form-control" id="suburb" placeholder="Please enter suburb...">
                  </div>

                  <div class="col-md-6">
                    <label for="state" class="control-label">State</label>
                    <input type="text" class="form-control" id="state" placeholder="State">
                  </div>

                  <div class="col-md-6">
                    <label for="postcode" class="control-label">Postcode</label>
                    <input type="text" class="form-control" id="postcode" placeholder="Postcode">
                  </div>             -->
                </div>


              </div>
              
              <div class="tab-pane" id="tab2-2">
                  <h2 class="form-note-ten text-color">Billing details</h2>
                  <div class="row">
                      <div class="col-md-12">
                          <div class="form-group">
                              <label for="address" class="control-label">Address</label>
                              <input type="text" class="form-control" name="billingAddress" id="billingAddress" data-validate="required" placeholder="Please enter address">
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-12">
                          <div class="form-group">
                              <label for="suburb" class="control-label">Suburb</label>
                              <input type="text" class="form-control" name="billingSuburb" id="billingSuburb" data-validate="required" placeholder="Please enter suburb...">
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-12">
                          <div class="form-group">
                              <label for="state" class="control-label">State</label>
                              <input type="text" class="form-control" name="billingState" id="billingState" data-validate="required" placeholder="State">
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-12">
                          <div class="form-group">
                              <label for="postcode" class="control-label">Postcode</label>
                              <input type="text" class="form-control" name="billingPostcode" id="billingPostcode" data-validate="required" placeholder="Postcode">
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-12">
                          <div class="form-group">
                              <label for="postcode" class="control-label">Ship to the same address? Please check:</label>
                              <select class="form-control" name="ddlShip" id="ddlShip" data-validate="required">
                                  <option></option>
                                  <option>Ship to the same address</option>
                                  <option>Ship to a different address</option>
                              </select>
                          </div>
                      </div>
                  </div>
              </div>
              
              <div class="tab-pane" id="tab2-3">
                
               <div class="col-md-12">
				  <h2 class="form-note-ten text-color">Shipping details</h2>
				  <p id="shippingMessage" style="font-size:18px;">You have opted to ship to a different address</p>
                  <br />
                  <p id="shippingMessage2" style="font-size:18px;display:none;color: red">You have chosen to ship to
                      the same address. If you wish to change this, go back to “Billing” step
                      and change your option.</p>
                   <div class="row">
                       <div class="col-md-12">
                           <div class="form-group">
                               <label for="address" class="control-label">Address</label>
                               <input type="text" class="form-control" name="shippingAddress" id="shippingAddress" data-validate="required" placeholder="Please enter address">
                           </div>
                       </div>
                   </div>
                   <div class="row">
                       <div class="col-md-12">
                           <div class="form-group">
                               <label for="suburb" class="control-label">Suburb</label>
                               <input type="text" class="form-control" name="shippingSuburb" id="shippingSuburb" data-validate="required" placeholder="Please enter suburb...">
                           </div>
                       </div>
                   </div>
                   <div class="row">
                       <div class="col-md-12">
                           <div class="form-group">
                               <label for="state" class="control-label">State</label>
                               <input type="text" class="form-control" name="shippingState" id="shippingState" data-validate="required" placeholder="State">
                           </div>
                       </div>
                   </div>
                   <div class="row">
                       <div class="col-md-12">
                           <div class="form-group">
                               <label for="postcode" class="control-label">Postcode</label>
                               <input type="text" class="form-control" name="shippingPostcode" id="shippingPostcode" data-validate="required" placeholder="Postcode">
                           </div>
                       </div>
                   </div>

			  </div>
			  
			  
                
              </div>

              <div class="tab-pane" id="tab2-4">
                
				<h2 class="form-note-ten text-color">Important Dates</h2>

                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label for="imagePicker" class="control-label">Due Date</label>
                              <div class="input-group date" data-date-format="dd-mm-yyyy">
                                  <input type="text" class="form-control" name="date" id="date" data-validate="required"><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                              </div>
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="form-group">
                              <label for="imagePicker" class="control-label">Require Date</label>
                              <div class="another input-group date" data-date-format="dd-mm-yyyy">
                                  <input type="text" class="form-control" name="date2" id="date2" data-validate="required"><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-12">
                          <div class="form-group">
                              <br>
                              <h5 class="form-note-ten text-color" style="color:red">
                                  <strong>Important - </strong>The FreeMOM TENS stimulator kit is usually supplied at week 37 to 38 of your pregnancy.
                              </h5>
                          </div>
                      </div>
                  </div>
			<h2 class="form-note-ten text-color">Product Details</h2>
                <table class="table">
                  <thead>
                    <tr>
                      <th><h5 class="text-color">Please select item(s) required by ticking the boxes:</h5></th>
                      <th></th>
                    </tr>
                  </thead>

                  <thead>
                    <tr>
                      <th><strong>ITEM</strong></th>
                      <th class="text-right"><strong>AUD</strong></th>
                    </tr>
                  </thead>

                  <tbody>

                    <tr>
                      <td><input type="checkbox" value=""> FreeMOM TENS Hire (Incl. GST) - for the term of your confinement</td>
                      <td class="text-right">$50.00</td>
                    </tr>                                  

                    <tr>
                      <td><input type="checkbox" value=""> Labour Electrodoes - refundable if not opened used or damaged </td>
                      <td class="text-right">$35.00</td>
                    </tr>                     

                    <tr>
                      <td><input type="checkbox" value="">  Deposit - on the return of the FreeMOM TENS kit </td>
                      <td class="text-right">$40.00</td>
                    </tr>                                     

                    <tr>
                      <td><input type="checkbox" value=""> Postage - Australia Post delivery to your address</td>
                      <td class="text-right">$15.00</td>
                    </tr>                     

                    <tr>
                      <td><input type="checkbox" value=""> Prepaid return Postage Satchel - Australia post delivery to our office </td>
                      <td class="text-right">$15.00</td>
                    </tr>                                  
                      
                  </tbody>

                </table>

                <table class="table">

                  <thead>

                    <tr>
                      <th><strong>If applicable, OPTIONS - refundable if returned not opened, usged or damaged</strong></th>
                      <th></th>
                    </tr>

                  </thead>

                  <tbody>

                    <tr>
                      <td><input type="checkbox" value=""> Back pain or pelvic instability - Electrodes with Clean-cote Skin Wipes and battery </td>
                      <td class="text-right">$20.00</td>
                    </tr>                    

                    <tr>
                      <td><input type="checkbox" value=""> Nick and shoulder pain - Electrodes with Clean-cote Skin Wipes and battery</td>
                      <td class="text-right">$20.00</td>
                    </tr> 

                    <tr>
                      <td>&nbsp;</td>
                      <td class="text-right">Total inc. = $125.00</td>
                    </tr>

                  </tbody>

                </table>

              </div>
              
              <div class="tab-pane" id="tab2-5">

                  <div class="row">
                      <div class="col-md-12">
                          <div class="form-group">
                              <label class="control-label">Debit or credit card:</label>
                              <select class="form-control" name="ddlCardType" id="ddlCardType" data-validate="required">
                                  <option></option>
                                  <option value="1">Debit</option>
                                  <option value="2">Credit</option>
                              </select>
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-12">
                          <div class="form-group">
                              <label class="control-label">Card number</label>
                              <input type="text" class="form-control" name="cardNumber" id="cardNumber" data-validate="required" data-rule-creditcard="true" placeholder="Please enter card number">
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-12">
                          <div class="form-group">
                              <label class="control-label">Expiry Date</label>
                              <input type="text" class="form-control" name="expDate" id="expDate" data-validate="required" placeholder="Please enter expiry date">
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-12">
                          <div class="form-group">
                              <label class="control-label">CVC/CVV</label>
                              <input type="text" class="form-control" name="cvc" id="cvc" data-rule-required="true" data-rule-number="true" data-rule-minlength="3" data-msg-minlength="CVC/CVV must have at least 3 digits" placeholder="Please enter CVC/CVV">
                          </div>
                      </div>
                  </div>

                <strong>ORDERS ARE DELAYED IF CARD NUMBER, EXPIRY DATE OR CVC NUMBERS ARE INCORRECT. PLEASE DOUBLE CHECK THEM BEFORE CLICKING THE ORDER BUTTON. THANK YOU.</strong>
                
              </div>
              
              <div class="tab-pane" id="tab2-6">

                <div class="form-group"> 

                  <div class="col-md-12 from-text-area">
                    <label for="inputEmail3" class="control-label">Comments</label>
                    <textarea type="text" class="form-control" id="inputEmail3" rows="10"></textarea>
                  </div>

                  <div class="col-md-12">
                    <button type="submit" class="btn btn-main">Submit Order</button>
                  </div>

                </div>  
                
              </div>
              
              <ul class="pager wizard">
                
                <li class="next">
                  <a href="#">Next <i class="entypo-right-open"></i></a>
                </li>
              </ul>
            </div>

          </form>

      </div>  
    </section>

    <script>
      $('.input-group.date').datepicker({
      autoclose: true,
      todayHighlight: true
    });       


    $('.another.input-group.date').datepicker({
      autoclose: true,
      todayHighlight: true
    }); 
  </script>

  <script>
  $(document).ready(function(){
    // Target your .container, .wrapper, .post, etc.
    $(".container").fitVids();
  });
  </script>
  <script src="js/jquery.flexslider-min.js"></script>
  <script>
      // Can also be used with $(document).ready()
      $(window).load(function() {
        $('.flexslider').flexslider({
          animation: "flase",              //String: Select your animation type, "fade" or "slide"
          easing: "swing",               //{NEW} String: Determines the easing method used in jQuery transitions. jQuery easing plugin is supported!
          direction: "horizontal",        //String: Select the sliding direction, "horizontal" or "vertical"
          reverse: false,                 //{NEW} Boolean: Reverse the animation direction
          animationLoop: true,             //Boolean: Should the animation loop? If false, directionNav will received "disable" classes at either end
          smoothHeight: false,            //{NEW} Boolean: Allow height of the slider to animate smoothly in horizontal mode  
          startAt: 0,                     //Integer: The slide that the slider should start on. Array notation (0 = first slide)
          slideshow: true,                //Boolean: Animate slider automatically
          slideshowSpeed: 7000,           //Integer: Set the speed of the slideshow cycling, in milliseconds
          animationSpeed: 600,            //Integer: Set the speed of animations, in milliseconds
          initDelay: 0,                   //{NEW} Integer: Set an initialization delay, in milliseconds
          randomize: false,               //Boolean: Randomize slide order
          controlNav: true,               //Boolean: Create navigation for paging control of each clide? Note: Leave true for manualControls usage
          directionNav: true,             //Boolean: Create navigation for previous/next navigation? (true/false)
          prevText: "&nbsp;",           //String: Set the text for the "previous" directionNav item
          nextText: "&nbsp;",               //String: Set the text for the "next" directionNav item
        });
    });
  </script>
    <script>
        $(document).ready(function(){
            InitializeFormWizard();
            SetInputMasks();
            AddEmailValidatorMethod();
        });
    function InitializeFormWizard(){
         $(".form-wizard").each(function(i, el)
         {
             maxIdx = 0;
             var $this = $(el),
                     $progress = $this.find(".steps-progress div"),
                     _index = $this.find('> ul > li.active').index();

             // Validation
             var checkFormWizardValidaion = function(tab, navigation, index)
             {
                 maxIdx = index;
                 var isValidFrom =  ValidateFrom($this);
                 if(!isValidFrom) return false;
                 //if($this.hasClass('validate'))
                 //{
                 //    var $valid = $this.valid();

                 //    if( ! $valid)
                 //    {
                 //        $this.data('validator').focusInvalid();
                 //        return false;
                 //    }
                 //}
                 if(index == 2 ){
                     var ddl = $this.find('#ddlShip')[0];
                     var idx = ddl.selectedIndex;
                     debugger;
                     if(idx == 1){
                         CopyBillingDataToShipping();
                         $this.bootstrapWizard('show', 2);
                     }
                     else{
                         ClearShippingData();
                     }
                 }
                 return true;
             };

             var tabClicked = function(tab,naviagtion,index,clicked)
             {
                 debugger;
                 if (clicked > maxIdx) {
                     return false;
                 } else if(clicked == maxIdx) {
                     var isValidFrom =  ValidateFrom($this);
                     if(!isValidFrom) return false;
                     return true;
                 }
                 else{
                     return true;
                 }
             };
             $this.bootstrapWizard({
                 tabClass: "",
                 onTabShow: function($tab, $navigation, index)
                 {
                     setCurrentProgressTab($this, $navigation, $tab, $progress, index);
                 },

                 onNext: checkFormWizardValidaion,
                 onTabClick: tabClicked
             });

             $this.data('bootstrapWizard').show( _index );

             /*$(window).on('neon.resize', function()
              {
              $this.data('bootstrapWizard').show( _index );
              });*/
         });
     }
    function ValidateFrom($form){
         if($form.hasClass('validate'))
         {
             var $valid = $form.valid();

             if( ! $valid)
             {
                 $form.data('validator').focusInvalid();
                 return false;
             }
             else{
                 return true;
             }
         }
     }
    function SetInputMasks(){
        $('#expDate').inputmask({
            mask: "99/99",
            placeholder: "MM/DD",
            alias: "date"
        });
    }
    function AddEmailValidatorMethod(){
        $.validator.addMethod(
                "regex",
                function(value, element, regexp) {
                    var re = new RegExp(regexp);
                    return this.optional(element) || re.test(value);
                },
                "Incorrect format, Please check your input."
        );
    }
    function CopyBillingDataToShipping(){
        $('#shippingMessage2').css("display","block");
        $('#shippingAddress').val($('#billingAddress').val());
        $('#shippingSuburb').val($('#billingSuburb').val());
        $('#shippingState').val($('#billingState').val());
        $('#shippingPostcode').val($('#billingPostcode').val());
    }
    function ClearShippingData(){
        $('#shippingMessage2').css("display","none");
        $('#shippingMessage').css("display","block");
        $('#shippingAddress').val('');
        $('#shippingSuburb').val('');
        $('#shippingState').val('');
        $('#shippingPostcode').val('');
        }
    </script>