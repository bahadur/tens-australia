<?php $this->load->view("product/freeLady/header") ?>


    <section class="product-information"> 
      <div class="container">
        <h2>Specification</h2>
      </div>
    </section>


    <section class="product-information">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            

            <table class="table table-bordered">
              
              <tr>
                <th>Size:</th>
                <td>(L) 92mm (W) 74 mm (D) 15 mm</td>
              </tr>

              <tr>
                <th>Weight:</th>
                <td>60 gm. (excl. Battery)</td>
              </tr>


              <tr>
                <th>Waveform:</th>
                <td>Symmetrical rectangular biphasic</td>
              </tr>


              <tr>
                <th>Channels:</th>
                <td>One (1)</td>
              </tr>


              <tr>
                <th>Current Output:</th>
                <td>0-50mA Peak into 1K ohm load</td>
              </tr>


              <tr>
                <th>Pulse Width:</th>
                <td>100 MicroSec. Preset</td>
              </tr>


              <tr>
                <th>Pulse Rate:</th>
                <td>100 Hz.</td>
              </tr>


              <tr>
                <th>Modulation:</th>
                <td>3 to 10 Hz. (FM)</td>
              </tr>


              <tr>
                <th>Green LED:</th>
                <td>Stays ON when switched ON</td>
              </tr>

              
              <tr>
                <th>Battery Life:</th>
                <td>3 x AAA 1.5V Alkaline</td>
              </tr>

              <tr>
                <th>Pulse Rate:</th>
                <td>(Approx.) 80 Hours</td>
              </tr>
             
             
            </table>

            
            
            
            
            
            
          
          </div>
        </div>
      </div>
    </section>

?>