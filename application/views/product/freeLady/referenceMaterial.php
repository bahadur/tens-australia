<?php $this->load->view("product/freeLady/header") ?>


    <section class="product-information"> 
      <div class="container">
        <h2>Reference Material</h2>
      </div>
    </section>


    <section class="product-information">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            

            <table class="table table-bordered">
              <tr>
                <th>CONTENT</th>
                <th>PDF</th>
              </tr>
              <tr>
                <td>
                  Clinical Evaluation of a New Model of a TENS Device 
for the Management of Primary Dysmenorrhea
                </td>
                <td>
                  <i class="fa fa-file-pdf-o"></i>
                </td>
              </tr>
              <tr>
                <td>
                  TENS as a relief for Dysmenorrhea
                </td>
                <td>
                  <i class="fa fa-file-pdf-o"></i>
                </td>
              </tr>
            </table>

            
            <p><small>You are welcome to call or email our office if you have any questions or require additional information.</small></p>      
            
            
            
            
          
          </div>
        </div>
      </div>
    </section>

?>