<?php $this->load->view("product/freeLady/header") ?>


    <section class="product-information"> 
      <div class="container">
        <h2>Frequently Asked Questions</h2>
        <div class="row produxt-content">
          
          <!-- <div class="col-md-4">
            <img src="<?php echo base_url()?>assets/images/single.png" alt="">

          </div> -->
          <div class="col-md-8">

            <h3><a href="#whatIsTENS">What is Tens?</a></h3>
            <h3><a href="#howDoesTensWork">How Does Tens Work?</a></h3>
            <h3><a href="#howCanIuseTens">Can I use Tens in the shower or bath?</a></h3>
            <h3><a href="#howLongHasTensBeenUsed">How long has Tens been used?</a></h3>
          </div>
        </div>
      </div>
    </section>


    <section class="product-information">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            

            <a name="whatIsTENS"></a><h3>WHAT IS TENS?</h3>
            <p>TENS stands for Transcutaneous Electrical Nerve Stimulation. TENS is a drug free, non-invasive pain management tool that has been shown to be effective for many applications. Low-voltage electrical pulses are used to stimulate the nervous system. This is achieved via soft conductive pads that are attached to the skin.</p>      
            
            <br>  

            <a name="howDoesTensWork"></a><h3>HOW DOES TENS WORK? </h3>
            <p>Pain is the body’s warning system that registers in the brain as an unpleasant sensation. Pain messages are triggered by receptors in our skin, muscles, ligaments, back, etc. These messages are transported via nerves to the spinal cord and then to the brain where the sensation is registered. </p>
            <p>The electrical signal generated by the TENS device can partially block pain signals and reduce the severity of pain experienced by the individual. </p>
            <p>This 'partially shuts the gate on the pain messages' and prevents them from reaching the brain. 
            TENS stimulates the release of endorphins from the brain. Endorphins are our bodies own natural pain relieving compounds. 
            As an additional feature, the patented TENS ‘R’ US electrical waveform, promotes the ongoing release of endorphins with prolonged TENS use. </p>
            
           

            <br>
            
            <a name="howCanIuseTens"></a><h3>CAN I USE TENS IN THE SHOWER or BATH?</h3>
            <p>No. Water will damage the TENS stimulator and electrodes.</p>


            <br>

            <a name="howLongHasTensBeenUsed"></a><h3>HOW LONG HAS TENS BEEN USED?</h3>
            <p>The principles of “electro pain relief” have been dated back to 2500BC. The Egyptians, Romans and the Greeks were known for using electric eels during their baths to treat painful ailments. 
            </p>
            <p>The modern development of TENS is the result of years of research and development. TENS has been used for over 50 years in the management of chronic and acute pain. </p>
            <p>The large scale application of TENS stimulation today, stems from theoretical discoveries in human neurophysiology, the most important being the 'gate theory' of pain transmission by 'Melzack and Wall', published in Science, in 1965. </p>


            <br>

            

          
          </div>
        </div>
      </div>
    </section>

?>