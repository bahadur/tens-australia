<section class="top-slider-section">

      <div class="slider-heading">
        <div class="container">
        
          <h2>FREELADY TENS - <small>Menstrual/Endometriosis Pain Management</small></h2>

        </div>
      </div>

      <div class="container">
        
        <div class="slider-content">
            <div class="row">
            
                <div class="col-md-8">
                  <div class="slider-left-content">

                    <div class="button-container"> 
                      <ul class="button-container-list">
                        <!--li><a href="" class="btn slider-button"><i class="fa fa-cart-plus"></i>&nbsp;&nbsp;Hire</a></li -->
                        <li><a href="" class="btn slider-button"><i class="fa fa-cart-arrow-down"></i>&nbsp;&nbsp;purchase</a></li>
                        <!--li><a href="" class="btn slider-button"><i class="fa fa-play-circle-o"></i>&nbsp;&nbsp;Watch instructions</a></li -->
                      </ul>
                    </div>

                  </div>      
                </div>
                
                <div class="col-md-4">
                  <div class="slider-right-content">
                      <ul class="slider-right-content-link-list">
                        <li><a href="<?php echo base_url()?>"><i class="fa fa-angle-double-right"></i>&nbsp;&nbsp;Product Information</a></li>
                        <li><a href="<?php echo base_url()?>products/freeLady/instructions"><i class="fa fa-angle-double-right"></i>&nbsp;&nbsp;Instructions</a></li>
                        <li><a href="<?php echo base_url()?>products/freeLady/testimonial"><i class="fa fa-angle-double-right"></i>&nbsp;&nbsp;Testimonial</a></li>
                        <li><a href="<?php echo base_url()?>products/freeLady/faqs"><i class="fa fa-angle-double-right"></i>&nbsp;&nbsp;FAQs</a></li>
                        <li><a href="<?php echo base_url()?>products/freeLady/insurance"><i class="fa fa-angle-double-right"></i>&nbsp;&nbsp;Insurance</a></li>
                        <li><a href="<?php echo base_url()?>products/freeLady/productCare"><i class="fa fa-angle-double-right"></i>&nbsp;&nbsp;Product Care</a></li>
                        <li><a href="<?php echo base_url()?>products/freeLady/relatedLinks"><i class="fa fa-angle-double-right"></i>&nbsp;&nbsp;Related Links</a></li>
                        <li><a href="<?php echo base_url()?>products/freeLady/referenceMaterial"><i class="fa fa-angle-double-right"></i>&nbsp;&nbsp;Reference Material</a></li>
                        <li><a href="<?php echo base_url()?>products/freeLady/specification"><i class="fa fa-angle-double-right"></i>&nbsp;&nbsp;Specification</a></li>
                      </ul>
                  </div>
                </div>
            
            </div>
        </div>

      </div>

    </section>