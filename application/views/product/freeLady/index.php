<?php 
$this->layouts->add_include('assets/css/normalize.css');
$this->layouts->add_include('assets/css/flexslider.css');
?>


<?php $this->load->view("product/freeLady/header") ?>


    <section class="product-information"> 
      <div class="container">
        <h2>Product Information</h2>
        <div class="row produxt-content">
          <div class="col-md-4">
            <img src="<?php echo base_url()?>assets/images/single.png" alt="">
          </div>
          <div class="col-md-8">

             <h3><a href="#whatIsfreeLadyTens">What is FREELADY TENS?</a></h3>
            
            <h3><a href="#howDoesItwork">How does it Work?</a></h3>

            <h3><a href="#usingYourfreeLady">Using your FREELADY</a></h3>

            <h3><a href="#freeLadyTensFeatures">FREELADY offers the following superior features</a></h3>

            <h3><a href="#benefitsFreeLady">Benefits of the FREELADY</a></h3>
            
            <h3><a href="#testimonials">Testimonials</a></h3>

            <h3><a href="#references">References</a></h3>

          </div>
        </div>
      </div>
    </section>



    <section class="testimonial-section">
    <a name="testimonials"></a><h3 class="text-center">Testimonials</h3>
      <hr class="testimonial-line">
    <div class="container">
 
        <div class="flexslider">
          <ul class="slides">
            <li>
              <p>Eternally grateful for the FREELADY TENS unit 

                I have been using the FREELADY TENS stimulator for over a year now and it has been my lucky day when I first heard about its existence and started using it on a regular basis every month. I have suffered all my life from severe menstrual pain (Dysmenorhea) which have stopped me on a monthly basis from completing a full month’s work. The use of the FREELADY TENS unit has [provided menstrual pain relief] and literally added an extra 32 days a year to my life, days that I used to spend at home in utter despair! 
                Over the last year I have not missed a day from work due to my condition. For that I am eternally grateful for the arrival in Australia of the FREELADY TENS unit. </p>
              
              <div class="person">
                <h6>H.E.</h6>
              </div>

            </li>
            
           
            
          </ul>
        </div>
      </div>
    </section>


    <section class="product-information">
      <div class="container">
        <div class="row">
          <div class="col-md-12">


          <a name="whatIsfreeLadyTens"></a><h3>What is FREELADY TENS?</h3>
          <p>FREELADY is a Transcutaneous Electrical Nerve Stimulation (TENS) device which has been specifically designed to assist in the relief of menstrual and endometriosis pain.</p>
          <br>

          <a name="howDoesItwork"></a><h3>How does it Work?</h3>
          <p>FREELADY sends small electronic signals into the body via electrodes which are placed onto the skin. The stimulus aids the relief of pain in 3 ways:
            1.  By blocking the pain messages from reaching the brain via the nerves.
            2.  By stimulating the release of the body’s own naturally occurring pain-relieving endorphins.
            3.  By providing a distraction whilst the pain is occurring.</p>
          <br>

          <a name="usingYourfreeLady"></a><h3>Using your FREELADY</h3>
          <p>
          You’ll find your FREELADY extremely easy to use (as outlined below).
            1.  Attach the electrodes to your skin (below the ‘belly-button’, in the supra pubic area).
            2.  Connect the electrodes to the stimulator unit using the supplied cable.
            3.  Turn ON the stimulator and adjust its amplitude (strength).
          </p>
          <br>

          <a name="freeLadyTensFeatures"></a><h3>FREELADY offers the following superior features:</h3>
          <p>
              •  FREELADY is a pocket sized device. FREELADY is very simple to use and may be placed on a belt or attached to your briefs. When not in use, it can be carried in your pocket or in a handbag. The FREELADY is comfortable and unobtrusive under your clothes.
              • “Set and Forget” stimulation settings. No need to continuously control the intensity.
          </p>
          <br>

          <a name="benefitsFreeLady"></a><h3>Benefits of the FREELADY:</h3>
          <p>
              •  FREELADY is simple to use, non-invasive and drug free,
              • FREELADY initiates the action of the body’s own natural pain relieving mechanisms.
              • FREELADY is used to provide a background level of ‘distraction’ to which any other form of analgesia may be added if required.
              • FREELADY may be used at home or work but must be removed prior to a shower, bath etc.
              • FREELADY allows you to be fully mobile and manage your pain.
              • FREELADY can be easily be discontinued at any time.
          </p>
          <br>


          <a name="references"></a><h3>references</h3>
                <ol type="1">
                  <li>1.  Metzack Rand Wall PD. “Pain Mechanisms”, “A New Theory” Science. (150). 971-979. 1965</li>
                  <li>2.  Sjolund B.H. and Ericksson E.B. “Endorphins and Analgesia Produced by Peripheral Conditioning Stimulation”. Advances in Pain Research and Therapy Vol.3 5870592, 1979.</li>
                  <li>3.  Augstinsson et al – Pain 4. 59065, 1977.</li>
                  
                <ol>
              <p>*Pain Management & Research Centre, Melbourne Australia.</p>

          </div>
        </div>
      </div>
    </section>


<?php 
$this->layouts->add_include('assets/js/jquery.flexslider-min.js');
$js = <<< CUSTOM_JS
  
  $('.flexslider').flexslider({
          animation: "flase",            
          easing: "swing",               
          direction: "horizontal",       
          reverse: false,                
          animationLoop: true,           
          smoothHeight: false,           
          startAt: 0,                    
          slideshow: true,               
          slideshowSpeed: 7000,          
          animationSpeed: 600,           
          initDelay: 0,                  
          randomize: false,              
          controlNav: true,              
          directionNav: true,            
          prevText: "&nbsp;",           
          nextText: "&nbsp;",            
        });
    

CUSTOM_JS;


$this->layouts->add_js_block($js);
?>