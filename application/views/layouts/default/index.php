<!DOCTYPE html>

  <html lang="en">

    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

      <!-- SITE TITLE -->
      <title>Tens Australia<?php echo $title_for_layout ?></title>
      
      <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700,400italic|Raleway:500,600,700,400' rel='stylesheet' >

      <!-- STYLESHEET -->
      <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/font-awesome.min.css" rel="stylesheet">
      <!-- link href="<?php echo base_url() ?>assets/css/normalize.css" rel="stylesheet" -->
      

      
      
      



      <?php echo $this->layouts->print_css(); ?> 
      <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
      <link href="<?php echo base_url() ?>assets/css/responsive.css" rel="stylesheet">
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
      <![endif]-->

    </head>
  <body>

 
<?php $this->load->view("layouts/default/header"); ?>

<?php echo $content_for_layout; ?> 


<?php $this->load->view("layouts/default/footer");?>

<!-- JQUERY -->
<script src="<?php echo base_url() ?>assets/js/jquery-1.11.2.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
  
<?php echo $this->layouts->print_js(); ?> 

<script type="text/javascript">

jQuery(document).ready(function () {
  var base_url = "<?php echo base_url()?>";
  <?php echo $this->layouts->js_block() ?>
  

});

</script>
</body> 
</html>