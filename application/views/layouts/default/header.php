   <!-- HEADER -->
    <header id="header" class="header">
      <div class="container">
      
        <!-- NAVIGATION -->
        <nav class="navbar navbar-default">

          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#tens-australia-nav-menu">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo base_url()?>"><img src="<?php echo base_url() ?>assets/tempassets/images/logo.png" alt="Tens Australia"></a>
            </div>

            <!-- Toggling Links For Responsive Navigation-->
            <div class="collapse navbar-collapse" id="tens-australia-nav-menu">
              <ul class="nav navbar-nav navbar-right">
                <li><a href="<?php echo base_url()?>"><i class="fa fa-home"></i><?=lang('home')?></a></li>
                <li><a href="#"><i class="fa fa-cart-plus"></i><?=lang('products')?></a></li>
                <li><a href="<?php echo base_url()?>services"><i class="fa fa-folder-open-o"></i><?=lang('services')?></a></li>
                <li><a href="<?php echo base_url()?>indicators"><i class="fa fa-circle-o-notch"></i><?=lang('indicators')?></a></li>
                <li><a href="<?php echo base_url()?>main/aboutUs"><i class="fa fa-file-archive-o"></i><?=lang('about_us')?></a></li>
                <li><a href="<?php echo base_url()?>main/contactUs"><i class="fa fa-map-marker"></i><?=lang('contact_us')?></a></li>
                <!--li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-language"></i> English <span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                  <li>
                    
                    <?php //echo anchor('','<i class="fa fa-angle-right"></i>English') ?>
                  </li>
                  <li>
                    
                    <?php // echo anchor('/ch','<i class="fa fa-angle-right"></i>中国') ?>
                  </li>
                  <li>
                    
                    <?php //echo  anchor('/jp','<i class="fa fa-angle-right"></i>日本人') ?>
                  </li>
                  </ul>
                </li -->
                <!--li class="font-size-maintain"><a class="font-size-small" href="#">A-</a></li>
                <li class="font-size-maintain"><a class="font-size-big" href="#">A+</a></li -->

              </ul> 

            </div><!-- //navbar-collapse -->
          </div><!-- //container-fluid -->

        </nav>
        <!-- //END NAVIGATION -->

      </div>
      <!-- //END CONTAINER -->
    </header>
    <!-- //END HEADER -->